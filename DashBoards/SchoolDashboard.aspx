﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SchoolDashboard.aspx.cs" Inherits="DashBoards_SchoolDashboard" %>

<%@ Register src="NationaltyChart.ascx" tagname="NationaltyChart" tagprefix="uc1" %>

<%@ Register src="StudentsPerDay.ascx" tagname="StudentsPerDay" tagprefix="uc2" %>

<%@ Register src="FamilyUseChart.ascx" tagname="FamilyUseChart" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" Runat="Server">

    <uc1:NationaltyChart ID="NationaltyChart1" runat="server" />

    <uc2:StudentsPerDay ID="StudentsPerDay1" runat="server" />

    <uc3:FamilyUseChart ID="FamilyUseChart1" runat="server" />

</asp:Content>

