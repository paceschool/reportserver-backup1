﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//[PartialCaching(3600, "none", "none", "none", true)]
public partial class Charting_FamilyUseChart : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var builder = new FamilyUseChartBuilder(_chart);
        builder.BuildChart();
    }
}