﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Student_AddComplaint : BaseEnrollmentControl
{
    protected Int32 _studentId;
    protected StudentComplaint _complaint;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            if (GetValue<Int32?>("StudentComplaintId").HasValue)
            {
                Int32 studentcomplaintId =GetValue<Int32>("StudentComplaintId");
                _complaint = (from comp in entity.StudentComplaint.Include("Student").Include("Xlk_Severity").Include("Users")
                              where comp.StudentComplaintId == studentcomplaintId
                              select comp).FirstOrDefault();
            }
        }

    }

    protected string GetStudentId
    {
        get
        {
            if (_complaint != null)
                return _complaint.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetStudentComplaintId
    {
        get
        {
            if (_complaint != null)
                return _complaint.StudentComplaintId.ToString();

            if (Parameters.ContainsKey("StudentComplaintId"))
                return Parameters["StudentComplaintId"].ToString();

            return "0";
        }
    }

    protected string GetComplaintTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_complaint != null && _complaint.Xlk_ComplaintType != null) ? _complaint.Xlk_ComplaintType.ComplaintTypeId : (byte?)null;

        foreach (Xlk_ComplaintType item in LoadComplaintTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ComplaintTypeId, item.Description, (_current.HasValue && item.ComplaintTypeId == _current) ? "selected" :string.Empty);
        }

        return sb.ToString();
    }

    protected string GetSeverityList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_complaint != null && _complaint.Xlk_Severity != null) ? _complaint.Xlk_Severity.SeverityId : (byte?)null;

        foreach (Xlk_Severity item in LoadSeverity())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.SeverityId, item.Description, (_current.HasValue && item.SeverityId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetComplaintUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_complaint != null && _complaint.Users != null) ? _complaint.Users.UserId : (Int32?)null;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetComplaint
    {
        get
        {
            if (_complaint != null)
                return _complaint.Complaint.ToString();

            return null;
        }
    }

    protected string GetAction
    {
        get
        {
            if (_complaint != null)
                if (_complaint.ActionNeeded == true)
                    return "checked";

            return string.Empty;
        }
    }


}
