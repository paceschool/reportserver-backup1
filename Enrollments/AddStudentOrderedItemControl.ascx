﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentOrderedItemControl.ascx.cs"
    Inherits="Student_AddStudentOrderedItem" %>
<script type="text/javascript">
    $(function () {

        var unitCosts = {};

        getForm = function () {
            return $("#addstudentordereditem");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudentOrderedItem") %>';
        }

        getRateCardUrl = function () {
            return '<%= ToVirtual("~/Agent/ViewQuotePage.aspx","GetStudentRateCardCostings") %>';
        }

        jQuery.validator.addMethod('selectcheck', function (value) {
            return (value > 0);
        }, "Please select an option");

        getLocalRules = function () {
            var arr = ["85", "86","87","88"];
            var localrules = {};
            localrules = {
                Qty: {
                    required: true,
                    minlength: 1,
                    number: true,
                    minStrict: 0
                },
                ActiveDate: {
                    required: true,
                    minlength: 6
                },
                AmountPaid: {
                    number: true,
                    minlength: 1,
                    required: function () {
                        return $("#PaymentMethodId").val() > 1;
                    },
                },
                TicketNumberFrom: {
                    required: function () {
                        return $.inArray($("#ItemId").val(), arr) > 0;
                    },
                },
                OrderableItem: {
                    selectcheck: true
                },
                Xlk_PaymentMethod: {
                    selectcheck: true
                }
            }
            return localrules;
        }

        getLocalMessages = function () {
            var localmessages = {};
            localmessages = {
                Qty: {
                    required: "Please provide a quantity",
                    minlength: "The quantity must be greater than 0"
                },
                ActiveDate: {
                    required: "Please select start date for ticket",
                    minlength: "The date must be valid"
                },
                TicketNumberFrom: {
                    required: "Please enter the ticket number",
                    minlength: "The ticket number must be part of a batch"
                },
                TicketNumberTo: {
                    required: "Please enter the ticket number",
                    minlength: "The ticket number must be part of a batch"
                },
                AmountPaid: {
                    required: "Please enter the amount paid",
                    minlength: "This value must be at greater than 0"
                },
                ItemId: {
                    required: "Please select a payment method",
                    minlength: "Please select a payment method"
                },
                Xlk_PaymentMethod: {
                    required: "Please select a payment method",
                    minlength: "Please select a payment method"
                }
            }
            return localmessages;
        }


        getRateCards = function (studentid, itemid, qty) {
            var params = '{studentid:' + studentid + ', itemid:' + itemid + ', qty:' + qty + '}';
            $.ajax({
                type: "POST",
                url: getRateCardUrl(),
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    items = null;

                    if (msg.d != null && !msg.d.Success) {
                        alert(msg.d.ErrorMessage);
                        return;
                    }

                    if (msg.d != null && msg.d.Success && msg.d.Payload.length > 0) {
                        items = msg.d.Payload;
                        $('#UnitId').find('option').remove();
                        if (items.length == 0)
                            $('#UnitId').find('option').end().append('<option value="">Not available</option>');
                        if (items.length >= 1) {
                            $('#UnitId').find('option').end().append('<option value="">Please Select</option>');

                            $(items).each(function () {
                                $('#UnitId').append($("<option></option>")
                                .attr("value", this.UnitId)
                                .text(this.Description)
                                .attr("selected", (this.DefaultUnitId == this.UnitId)));
                            });

                            if (items.length >= 1)
                                setItemPriceData(0);
                        }

                    }
                }
            });
        }

        $('.watcher').change(function () {
            getRateCards($('#StudentId').val(), $('#ItemId option:selected').val(), $('#Qty').val());
        });

        $('#Qty').keyup(function () {
            getRateCards($('#StudentId').val(), $('#ItemId option:selected').val(), $('#Qty').val());
        });


        $('.qwatcher').keyup(function () {
            setNettPrice();
        });




        assignValues = function (object) {
            if (object != null) {
                unitCosts = object.Costs;
                $("#UnitId").html('').addItems(object.Costs).change();
            }

        }
        $.fn.addItems = function (data) {
            return this.each(function () {
                var list = this;
                $.each(data, function (index, itemData) {
                    var option = new Option(itemData.Description, itemData.UnitId);
                    list.add(option);
                });
            });
        };

        setItemPriceData = function (index) {
            if (items.length > 0) {
                $('#RRPrice').val(items[index].Cost);
                $('#CalculationTypeId').val(items[index].CalculationTypeId);
                $('#Discount').val(items[index].Discount);
                $('#RateCardId').val(items[index].RateCardId);
                setNettPrice();
            }
        }

        setNettPrice = function () {
            var nett = 0;
            if ($('#CalculationTypeId').val() == 1)
                nett = (($('#RRPrice').val() * $('#Qty').val())) * (($('#Discount').val() > 0) ? (1 - $('#Discount').val() / 100) : 1);
            else
                nett = (($('#RRPrice').val())) * (($('#Discount').val() > 0) ? (1 - $('#Discount').val() / 100) : 1);

            $('#TotalLinePrice').val(nett);
        }

        $("#ActiveDate").datepicker({ dateFormat: 'dd/mm/yy' });
    });
    
</script>
<style>
    label.error
    {
        float: right;
        color: red;
        padding-left: .5em;
        vertical-align: top;
    }
    p
    {
        clear: both;
    }
    .submit
    {
        margin-left: 12em;
    }
    em
    {
        font-weight: bold;
        padding-right: 1em;
        vertical-align: top;
    }
</style>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addstudentordereditem" name="addstudentordereditem" method="post" action="ViewAccommodationPage.aspx/SaveStudentOrderedItem">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="OrderedItemId" value="<%= GetOrderedItemId %>" />
        <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
        <input type="hidden" id="PaidByTransactionId" value="<%= GetPaidByTransactionId %>" />
        <input type="hidden" id="RefundByTransactionId" value="<%= GetRefundByTransactionId %>" />
        <input type="hidden" id="CalculationTypeId" />
        <label>
            Ordered Item
        </label>
        <select  <%= IsEditMode %> class="watcher" type="text" name="OrderableItem" id="ItemId">
            <%= GetItemList() %>
        </select>
        <label for="Qty">
            Qty
        </label>
        <input <%= IsEditMode %> type="text" name="Qty" id="Qty" value="<%= GetQty %>" />
        <label>
            By Unit
        </label>
        <select <%= IsEditMode %> class="lwatcher" type="text" name="Xlk_Unit" id="UnitId">
             <%= GetQtyUnitList() %>
        </select>
        <label>
            Starting Date
        </label>
        <input type="datetext" name="ActiveDate" id="ActiveDate" value="<%= GetActiveDate %>"  />
        <label>
            Reference From
        </label>
        <input type="text" name="TicketNumberFrom" id="TicketNumberFrom" />
        <div class="togglevis" style="display: none">
            <label>
                Reference To
            </label>
            <input type="text" name="TicketNumberTo" id="TicketNumberTo" /></div>
    </fieldset>
    <fieldset>
        <label>
            Recommended Retail Price
        </label>
       <input class="qwatcher" disabled type="text" id="RRPrice" value="<%= GetRRPrice %>" />
        <label>
            Discount (default: 0%)
        </label>
        <input c <%= IsEditMode %> lass="qwatcher" type="text" id="Discount" value="<%= GetDiscount %>" />
        <label>
            <b>Total Expected</b>
        </label>
          <input  <%= IsEditMode %> type="text" name="TotalLinePrice" id="TotalLinePrice" value="0" />
        <label>
            Amount Received
        </label>
        <input  <%= IsEditMode %> type="text" name="AmountPaid" id="AmountPaid" />
        <label>
            Type
        </label>
        <select  <%= IsEditMode %> name="Xlk_PaymentMethod" id="PaymentMethodId">
            <%= GetPaymentTypeList()%>
        </select>
    </fieldset>
    <fieldset>
      <label>
            Computer
        </label>
        <input type="text" name="Computer" id="Computer" value="<%= GetComputer %>" />
        <label>
            Description
        </label>
        <textarea name="Description" id="Description"><%= GetDescription %></textarea>
        <label>
            Comment
        </label>
        <textarea name="Comment" id="Comment"><%= GetComment %></textarea>
        <label>
            Special Instructions
        </label>
        <textarea name="SpecialInstructions" id="SpecialInstructions"><%= GetSpecialInstructions%></textarea>
    </fieldset>
</div>
</form>
