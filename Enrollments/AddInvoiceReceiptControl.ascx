﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddInvoiceReceiptControl.ascx.cs"
    Inherits="Finance_ReceiptInvoiceControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addinvoicereceipt");
        }

        getTargetUrl = function () {

            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveInvoiceReceipt") %>';
        }

        getLocalRules = function () {
            var localrules = {};
            localrules = {
                AmountPaid: {
                    number: true,
                    minlength: 1,
                    minStrict: 1,
                    required: true
                },
                InvoiceNumber: {
                    number: true,
                    minlength: 1,
                    minStrict: 0,
                    required: true
                },
                Xlk_PaymentMethod: {
                    minStrict:1,
                    required: true
                }
            }
            return localrules;
        }

        getLocalMessages = function () {
            var localmessages = {};
            localmessages = {
                InvoiceNumber: {
                    required: "Please enter an invoice number",
                    minlength: "The invoice number must be greater than zero"
                },
                AmountPaid: {
                    required: "Please enter the amount paid",
                    minlength: "This value must be at greater than 0"
                },
                Xlk_PaymentMethod: {
                    required: "Please select a payment method",
                    minlength: "Please select a payment method"
                },
            }
            return localmessages;
        }

        $.widget("custom.catcomplete", $.ui.autocomplete, {
            _renderMenu: function (ul, items) {
                var that = this,
                currentCategory = "";
                $.each(items, function (index, item) {
                    if (item.category != currentCategory) {
                        ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                        currentCategory = item.category;
                    }
                    that._renderItemData(ul, item);
                });
            }
        });


        var invoices = <%= GetInvoices %>

        $("#SageInvoiceRef").catcomplete({                                                                                                                                                          
            delay: 0,
            source: invoices
        });
    });
</script>
<style>
  .ui-autocomplete-category {
    font-weight: bold;
    padding: .2em .4em;
    margin: .8em 0 .2em;
    line-height: 1.5;
  }
</style>
<p id="message" style="display: none;">
    All fields must be completed<a href="~/Enrollments/AddStudentComplaintControl.ascx">~/Enrollments/AddStudentComplaintControl.ascx</a>
</p>
<form id="addinvoicereceipt" name="addfamilypayment" method="post" action="ReceiptPaymentPage.aspx/SaveInvoiceReceipt"
style="width: 450px">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="InvoiceReceiptId" value="<%= GetInvoiceReceiptId %>" />
        <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
        <label>
            Invoice Number
        </label>
        <input type="text" id="SageInvoiceRef" name="SageInvoiceRef" value=<%= GetInvoiceNumber %> />
        <label>
            Amount Received
        </label>
        <input type="text" <%= IsEditMode %> class="watcher" name="AmountPaid" id="AmountPaid" value="<%= GetAmountPaid %>" />
        <label>
            Type
        </label>
        <select  <%= IsEditMode %> name="Xlk_PaymentMethod" id="PaymentMethodId">
            <%= GetPaymentTypeList()%>
        </select>
        <label>
            Comment
        </label>
        <textarea name="Comment" id="Comment"><%= GetComment %></textarea>
    </fieldset>
</div>
</form>
