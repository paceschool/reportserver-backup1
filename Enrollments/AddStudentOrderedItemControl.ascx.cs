﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Student_AddStudentOrderedItem : BaseEnrollmentControl
{
    protected Int32 _studentId;
    protected OrderedItem _orderedItem;
    protected Int16? _filterproductid;
    protected byte? _filtercategoryid;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            if (GetValue<Int32?>("OrderedItemId").HasValue)
            {
                Int32 orderedItemId = GetValue<Int32>("OrderedItemId");
                _orderedItem = (from comp in entity.OrderedItems.Include("Xlk_Unit").Include("OrderableItem").Include("Student").Include("Xlk_PaymentMethod")
                                where comp.OrderedItemId == orderedItemId
                              select comp).FirstOrDefault();
            }

            if (GetValue<Int16?>("FilterProductId") != null)
                _filterproductid = GetValue<Int16>("FilterProductId");

            if (GetValue<byte?>("FilterCategoryId") != null)
                _filtercategoryid = GetValue<byte>("FilterCategoryId");
        }

    }

    protected string GetOrderedItemId
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.OrderedItemId.ToString();

            if (Parameters.ContainsKey("OrderedItemId"))
                return Parameters["OrderedItemId"].ToString();

            return "0";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string IsEditMode
    {
        get
        {
            if (_orderedItem == null)
                return String.Empty;

            return "disabled";
        }
    }

    protected string GetComputer
    {
        get
        {
            return GetComputerName(Request.ServerVariables["remote_addr"]);
        }
    }

    protected string GetCategoryList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_Category item in LoadCategories())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.CategoryId, item.Description);
        }

        return sb.ToString();
    }

    protected string GetPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"-1\">Please Select</option>");


        short? _current = (_orderedItem != null && _orderedItem.Xlk_PaymentMethod != null) ? _orderedItem.Xlk_PaymentMethod.PaymentMethodId : (short?)null;


        foreach (Xlk_PaymentMethod item in LoadPaymentMethods())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.PaymentMethodId, item.Description, (_current.HasValue && item.PaymentMethodId == _current) ? "selected" : string.Empty);

        }

        return sb.ToString();
    }

    protected string GetDiscount
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.Discount.ToString();

            return "0.00";
        }
    }

    protected string GetRRPrice
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.RRPrice.ToString();

            return "0.00";
        }
    }
       
    protected string GetActiveDate
    {
        get
        {
            if (_orderedItem != null && _orderedItem.ActiveDate.HasValue)
                return _orderedItem.ActiveDate.Value.ToString("dd/MM/yy");

            return DateTime.Now.ToString("dd/MM/yy");
        }
    }

    
    protected string GetProductTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_ProductType item in LoadProductTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.ProductTypeId, item.Description);
        }

        return sb.ToString();
    }

    protected string GetItemList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"-1\">Please Select</option>");

        short? _current = (_orderedItem != null && _orderedItem.OrderableItem != null) ? _orderedItem.OrderableItem.ItemId : (short?)null;


        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var items =
                from o in entity.Xlk_ProductType
                orderby o.Description
                select new { ProductType = o.Description, Items = o.OrderableItems };


            foreach (var type in items)
            {

                sb.AppendFormat("<optgroup label=\"{0}\">", type.ProductType);

                foreach (var item in type.Items)
                {
                    sb.AppendFormat("<option {3} value={0}>{2} ({1})</option>", item.ItemId, item.SageRef, item.Title, (_current.HasValue && item.ItemId == _current) ? "selected" : string.Empty);
                }

                sb.Append("</optgroup>");

            }
        }
        return sb.ToString();
    }

    protected string GetQtyUnitList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_orderedItem != null && _orderedItem.Xlk_Unit != null) ? _orderedItem.Xlk_Unit.UnitId : (byte?)null;

        if (!_current.HasValue)
            return "<option>loading...</option>";

        foreach (Xlk_Unit item in LoadUnits())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UnitId, item.Description, (_current.HasValue && item.UnitId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetDescription
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.Description;

            return null;
        }
    }

    protected string GetQty
    {
        get
        {
            if (_orderedItem != null && _orderedItem.Qty.HasValue)
                return _orderedItem.Qty.Value.ToString();

            return "1";
        }
    }

    protected string GetSpecialInstructions
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.SpecialInstructions;

            return string.Empty;
        }
    }

    protected string GetComment
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.Comment;

            return string.Empty;
        }
    }

    protected string GetPaidByTransactionId
    {
        get
        {
            if (_orderedItem != null && _orderedItem.PaidByTransactionId.HasValue)
                return _orderedItem.PaidByTransactionId.Value.ToString();

            return string.Empty;
        }
    }
    protected string GetRefundByTransactionId
    {
        get
        {
            if (_orderedItem != null && _orderedItem.RefundByTransactionId.HasValue)
                return _orderedItem.RefundByTransactionId.Value.ToString();

            return string.Empty;
        }
    }

}
