﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentHostingControl.ascx.cs"
    Inherits="Student_AddHosting" %>
<script type="text/javascript">
    $(function () {

        $("#ArrivalDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#DepartureDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function () {
            return $("#addhosting");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveHosting") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }


        $('.target').change(function () {
            findFamilies($(this).val())
        });

        findFamilies = function (typeid) {
            var params = '{typeid:' + typeid + '}';
            $.ajax({
                type: "POST",
                url: "ViewStudentPage.aspx/FilterFamilies",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $("#FamilyId").html(msg.d.Payload);
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        }
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addhosting" name="addhosting" method="post">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
        <input type="hidden" id="HostingId" value="<%= GetHostingId %>" />
        <label for="family">
            Family</label>
        <select class="target" type="ignore" <%= IsFamilyLocked() %>>
            <%= GetFamilyTypeList() %>
        </select>
        <label for="family">
            Family</label>
        <select type="text" name="Family" id="FamilyId" <%= IsFamilyLocked() %>>
            <%= GetFamilyList() %>
        </select>
        <label for="ArrivalDate">
            Start Date</label>
        <input type="datetext" name="ArrivalDate" id="ArrivalDate" value="<%= GetArrivalDate %>" />
        <label for="DepartureDate">
            End Date</label>
        <input type="datetext" name="DepartureDate" id="DepartureDate" value="<%= GetDepartureDate %>" />
        <label for="WeeklyRate">
            Weekly Rate</label>
        <input type="text" id="WeeklyRate" value="<%= GetWeeklyRate %>" />
        <label for="SupplementaryPayment">
            Supplementary Payment</label>
        <input type="text" id="SupplementaryPayment" value="<%= GetSupplementaryPayment %>" />
        <label for="SupplementaryPaymentComment">
            Supplementary Payment Comment</label>
        <input type="text" id="SupplementaryPaymentComment" value="<%= GetSupplementaryPaymentComment %>" />
    </fieldset>
</div>
</form>
