﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Student_AddHosting : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected  Hosting _hosting;
    protected Student _student;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("HostingId").HasValue)
            {
                Int32 hostingId = GetValue<Int32>("HostingId");
                _hosting = (from host in entity.Hostings.Include("Student").Include("Family")
                            where host.HostingId == hostingId
                            select host).FirstOrDefault();
            }
            if (GetValue<Int32?>("StudentId").HasValue)
            {
                Int32 studentId = GetValue<Int32>("StudentId");
                _student = (from host in entity.Student
                            where host.StudentId == studentId
                            select host).FirstOrDefault();
            }
        }
    }


    private List<Pace.DataAccess.Accommodation.Family> GetFamilies(string filter)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Pace.DataAccess.Accommodation.Family> familyQuery = from f in entity.Families
                                                                           where f.CampusId == BasePage.GetCampusId
                                                                       select f;
            if (!string.IsNullOrEmpty(filter))
                familyQuery = familyQuery.Where(f => f.FirstName.StartsWith(filter) || f.SurName.StartsWith(filter));

            return familyQuery.ToList();
        }
    }

    protected string GetFamilyTypeList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">Any</option>");

        using (AccomodationEntities entity = new AccomodationEntities())
        {
            foreach (var item in entity.Xlk_FamilyType.ToList())
            {
                sb.AppendFormat("<option value={0}>{1}</i></option>", item.FamilyTypeId, item.Description);
            }
        }
        return sb.ToString();

    }

    protected string IsFamilyLocked()
    {
        DateTime _now = DateTime.Now.Date;

        if (_hosting != null && _hosting.ArrivalDate <= _now && _hosting.DepartureDate >= _now)
            return "disabled";

        return string.Empty;


    }

    protected string GetFamilyList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_hosting != null && _hosting.Family != null) ? _hosting.Family.FamilyId : (int?)null;

        var familyQuery = GetFamilies(string.Empty).Select(f => new { FamilyId = f.FamilyId,Address=f.Address, Name = f.SurName.ToString() + " " + f.FirstName.ToString() }).OrderBy(f => f.Name);
        
        foreach (var item in familyQuery.ToList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}      {3}</option>", item.FamilyId, item.Name, (_current.HasValue && item.FamilyId == _current) ? "selected" : string.Empty, item.Address);
        }

        return sb.ToString();

    }

    protected string GetHostingId
    {
        get
        {
            if (_hosting != null)
                return _hosting.HostingId.ToString();
            return "0";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_hosting != null)
                return _hosting.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetArrivalDate
    {
        get
        {
            if (_hosting != null)
                return _hosting.ArrivalDate.ToString("dd/MM/yy");

            if (_student != null)
                return _student.ArrivalDate.ToString("dd/MM/yy");

            return null;
        }
    }
    protected string GetDepartureDate
    {
        get
        {
            if (_hosting != null && _hosting.DepartureDate != null)
                return _hosting.DepartureDate.ToString("dd/MM/yy");

            if (_student != null && _student.DepartureDate.HasValue)
                return _student.DepartureDate.Value.ToString("dd/MM/yy");

            return null;

        }
    }

    protected string GetWeeklyRate
    {
        get
        {
            if (_hosting != null)
                return _hosting.WeeklyRate.ToString();

            return null;
        }
    }

    protected string GetSupplementaryPayment
    {
        get
        {
            if (_hosting != null)
                return _hosting.SupplementaryPayment.ToString();

            return null;
        }
    }

    protected string GetSupplementaryPaymentComment
    {
        get
        {
            if (_hosting != null)
                return _hosting.SupplementaryPaymentComment;

            return null;
        }
    }
}
