﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Student_AddTransfer : BaseEnrollmentControl
{
    protected StudentTransfer _studentTransfer;
    protected void Page_Load(object sender, EventArgs e)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            if (GetValue<Int32?>("StudentTransferId").HasValue)
            {
                Int32 studentTransferId = GetValue<Int32>("StudentTransferId");
                _studentTransfer = (from st in entity.StudentTransfer.Include("Student").Include("Xlk_TransferType").Include("Xlk_BusinessEntity")
                                    where st.StudentTransferId == studentTransferId
                                    select st).FirstOrDefault();

            }
        }
    }

    protected string GetLocationList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_studentTransfer != null && _studentTransfer.Xlk_Location != null) ? _studentTransfer.Xlk_Location.LocationId : (short?)null;

        foreach (Xlk_Location item in LoadLocations())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current.HasValue && item.LocationId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetTransferTypeList()
    {
        StringBuilder sb = new StringBuilder();
        byte? _current = (_studentTransfer != null && _studentTransfer.Xlk_TransferType != null) ? _studentTransfer.Xlk_TransferType.TransferTypeId : (byte?)null;

        foreach (Xlk_TransferType item in LoadTransferTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TransferTypeId, item.Description, (_current.HasValue && item.TransferTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetBusinessEntityList()
    {
        StringBuilder sb = new StringBuilder();
        byte? _current = (_studentTransfer != null && _studentTransfer.Xlk_BusinessEntity != null) ? _studentTransfer.Xlk_BusinessEntity.EntityId : (byte?)null;

        foreach (Xlk_BusinessEntity item in LoadBusinessEntity())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.EntityId, item.Description, (_current.HasValue && item.EntityId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetStudentId
    {
        get
        {
            if (_studentTransfer != null)
                return _studentTransfer.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetStudentTransferId
    {
        get
        {
            if (_studentTransfer != null)
                return _studentTransfer.StudentTransferId.ToString();
            return "0";
        }
    }

    protected string GetComment
    {
        get
        {
            if (_studentTransfer != null)
                return _studentTransfer.Comment;

            return null;
        }
    }

    protected string GetDate
    {
        get
        {
            if (_studentTransfer != null)
                return _studentTransfer.Date.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetTransportBookingId
    {
        get
        {
            if (_studentTransfer != null)
                return _studentTransfer.TransportBookingId.ToString();

            return null;
        }
    }

    protected string GetFlightNumber
    {
        get
        {
            if (_studentTransfer != null)
                return _studentTransfer.FlightNumber;

            return null;
        }
    }

    protected string GetTime
    {
        get
        {
            if (_studentTransfer != null && _studentTransfer.Time.HasValue)
                return _studentTransfer.Time.ToString();

            return null;
        }
    }

}
