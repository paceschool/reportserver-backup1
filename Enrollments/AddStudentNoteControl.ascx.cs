﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Student_AddNote : BaseEnrollmentControl
{
    protected Int32 _familyId;
    protected StudentNote _note;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            if (GetValue<Int32?>("StudentNoteId") != null)
            {
                Int32 studentnoteId = GetValue<Int32>("StudentNoteId");
                _note = (from n in entity.StudentNote.Include("Student").Include("Users").Include("Xlk_NoteType")
                         where n.StudentNoteId == studentnoteId
                         select n).FirstOrDefault();
            }
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_note != null)
                return _note.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetStudentNoteId
    {
        get
        {
            if (_note != null)
                return _note.StudentNoteId.ToString();

            if (Parameters.ContainsKey("StudentNoteId"))
                return Parameters["StudentNoteId"].ToString();

            return "0";
        }
    }

    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_note != null && _note.Xlk_NoteType != null) ? _note.Xlk_NoteType.NoteTypeId : (byte?)null;

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NoteTypeId, item.Description, (_current.HasValue && item.NoteTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNoteUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_note != null && _note.Users != null) ? _note.Users.UserId : GetCurrentUser().UserId;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNote
    {
        get
        {
            if (_note != null)
                return _note.Note.ToString();

            return null;
        }
    }
}
