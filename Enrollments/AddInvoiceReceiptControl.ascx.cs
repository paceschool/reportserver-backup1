﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using Pace.DataAccess.Enrollment;



public partial class Finance_ReceiptInvoiceControl : BaseEnrollmentControl
{
    protected Int32? _studentId;
    protected Int32? _invoiceReceiptId;
    protected InvoiceReceipt _receipt = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("InvoiceReceiptId").HasValue)
        {
            _invoiceReceiptId = GetValue<Int32>("InvoiceReceiptId");

            using (EnrollmentsEntities entity = new EnrollmentsEntities())
            {
                _receipt = (from i in entity.InvoiceReceipts.Include("Student").Include("Xlk_PaymentMethod")
                                     where i.InvoiceReceiptId == _invoiceReceiptId
                                     select i).FirstOrDefault();
            }
        }

        if (GetValue<Int32?>("StudentId").HasValue)
            _studentId =GetValue<Int32>("StudentId");
    }

    protected string IsEditMode
    {
        get
        {
            if (_receipt == null)
                return String.Empty;

            return "disabled";
        }
    }

    protected string GetInvoiceReceiptId
    {
        get
        {
            if (_receipt != null)
                return _receipt.InvoiceReceiptId.ToString();

            if (_invoiceReceiptId.HasValue)
                return _invoiceReceiptId.ToString();

            if (Parameters.ContainsKey("InvoiceReceiptId"))
                return Parameters["InvoiceReceiptId"].ToString();

            return "0";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_receipt != null)
                return   _receipt.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetInvoices
    {
        get
        {
            using (Pace.DataAccess.Sage.SageEntities entity = new Pace.DataAccess.Sage.SageEntities())
            {
                var studentinvoices = ((from sd in entity.Invoices
                                       join s in entity.StudentInvoices on sd.InvoiceNumber equals s.SageInvoiceRef
                                where s.StudentId == _studentId.Value
                                        select new { s.SageInvoiceRef, sd.NetAmount }).ToList().Select(x => new { value = x.SageInvoiceRef, label = (x.NetAmount.HasValue) ? string.Format("{0} for {1}", x.SageInvoiceRef, x.NetAmount.Value.ToString("C")) : x.SageInvoiceRef.ToString(), category = "Student Invoice" })).ToList();
               
                return new JavaScriptSerializer().Serialize(studentinvoices);
            }
        }
    }

    protected string GetInvoiceNumber
    {
        get
        {
            if (_receipt != null)
                return _receipt.SageInvoiceRef.ToString();

            return "Type here";
        }
    }

    protected string GetAmountPaid
    {
        get
        {
            if (_receipt != null)
                return _receipt.AmountPaid.ToString();

            return "0.0";
        }
    }

    protected string GetComment
    {
        get
        {
            if (_receipt != null)
                return _receipt.Comment;

            return "";
        }
    }

    protected string GetPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_receipt != null && _receipt.Xlk_PaymentMethod != null) ? _receipt.Xlk_PaymentMethod.PaymentMethodId : (byte?)null;


        foreach (Pace.DataAccess.Enrollment.Xlk_PaymentMethod item in LoadPaymentMethods())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.PaymentMethodId, item.Description, (_current.HasValue && item.PaymentMethodId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}