﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Enrollments_AddStudentControl : BaseEnrollmentControl
{
    protected Student _student;
    protected Int32 _studentId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            if (GetValue<Int32?>("StudentId").HasValue)
            {
                Int32 studentId = GetValue<Int32>("StudentId");
                _student = (from st in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_StudentType").Include("Xlk_Status").Include("Xlk_Gender").Include("Exam")
                            where st.StudentId == studentId
                            select st).FirstOrDefault();
            }
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_student != null)
                return _student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }
    protected string GetGroupingBlockId
    {
        get
        {
            if (_student != null && _student.GroupingBlockId.HasValue)
                return _student.GroupingBlockId.Value.ToString();

            return "";
        }
    }

    protected string GetFirstName
    {
        get
        {
            if (_student != null)
                return _student.FirstName.ToString();

            return null;
        }
    }

    protected string GetSurName
    {
        get
        {
            if (_student != null)
                return _student.SurName.ToString();

            return null;
        }
    }

    protected string GetGender()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (_student != null) ? _student.Xlk_Gender.GenderId : (Byte?)null;

        foreach (Xlk_Gender item in LoadGender())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.GenderId, item.Description, (_current.HasValue && item.GenderId == _current) ? "selected" : string.Empty);   
        }
        return sb.ToString();
    }

    protected string GetDateOfBirth
    {
        get
        {
            if (_student != null && _student.DateOfBirth.HasValue)
                return _student.DateOfBirth.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetNationality()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_student != null) ? _student.Xlk_Nationality.NationalityId : (Int16?)null;

        foreach (Xlk_Nationality item in LoadNationality())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NationalityId, item.Description, (_current != null && item.NationalityId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetExam()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_student != null && _student.Exam != null) ? _student.Exam.ExamId : (Int16?)null;

        sb.AppendFormat("<option value={0}>{1}</option>", -1, "");

        foreach (Exam item in LoadExam())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExamId, item.ExamName, (_current != null && item.ExamId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetArrivalDate
    {
        get
        {
            if (_student != null)
                return _student.ArrivalDate.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetDepartureDate
    {
        get
        {
            if (_student != null && _student.DepartureDate.HasValue)
                return _student.DepartureDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetAgent()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_student != null && _student.Agency.AgencyId != null) ? _student.Agency.AgencyId : (Int32?)null;

        foreach (Agency item in LoadAgency())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.AgencyId, item.Name, (_current != null && item.AgencyId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStatus()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (_student != null) ? _student.Xlk_Status.StatusId : (Byte?)null;

        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current != null && item.StatusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetPlacement
    {
        get
        {
            if (_student != null && _student.PlacementTestResult.HasValue)
                return _student.PlacementTestResult.Value.ToString();

            return null;
        }
    }

    protected string GetStudentType()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (_student != null) ? _student.Xlk_StudentType.StudentTypeId : (Byte?)null;

        foreach (Xlk_StudentType item in LoadStudentType())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StudentTypeId, item.Description, (_current != null && item.StudentTypeId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetCampus()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_student != null) ? _student.CampusId : GetCampusId;

        foreach (Campus item in LoadCampus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.CampusId, item.Name, (_current != null && item.CampusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetPrepaidBook
    {
        get
        {
            if (_student != null && _student.PrepaidBook)
                return "checked";

            return string.Empty;
        }
    }

    protected string GetPrepaidExam
    {
        get
        {
            if (_student != null && _student.PrepaidExam)
                return "checked";

            return string.Empty;
        }
    }


    protected string GetOwnAccommodation
    {
        get
        {
            if (_student != null && _student.OwnAccommodation.HasValue && _student.OwnAccommodation.Value)
                return "checked";

            return string.Empty;
        }
    }

    protected string GetHomeAddress
    {
        get
        {
            if (_student != null && _student.HomeAddress != null)
                return _student.HomeAddress.ToString();

            return string.Empty;
        }
    }

    protected string GetStudentEmail
    {
        get
        {
            if (_student != null && _student.StudentEmail != null)
                return _student.StudentEmail.ToString();

            return string.Empty;
        }
    }

    protected string GetEmergencyContactNumber
    {
        get
        {
            if (_student != null && !string.IsNullOrEmpty(_student.EmergencyContactNumber))
                return _student.EmergencyContactNumber.ToString();

            return string.Empty;
        }
    }

    protected string GetGroupList()
    {
        StringBuilder sb = new StringBuilder("<option value=0>None</option>");

        Int32? _current = (_student != null) ? _student.GroupId : (Byte?)null;

        DateTime arrivaldate = (_student != null) ? _student.ArrivalDate.AddMonths(-1) : DateTime.MinValue;

        using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
        {

            var groups = from g in gentity.Group
                         where g.ArrivalDate >= arrivaldate
                         select new { g.GroupId, g.GroupName };

            foreach (var item in groups)
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.GroupId, item.GroupName, (_current.HasValue && item.GroupId == _current) ? "selected" : string.Empty);
            }
        }
        return sb.ToString();
    }


}