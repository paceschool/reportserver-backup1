﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Data;
using Tuition = Pace.DataAccess.Tuition;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Globalization;

public partial class Enrollments_ViewStudentPage : BaseEnrollmentPage
{
    protected Int32 _studentId;


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["StudentId"]))
            _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!Page.IsPostBack)
        {
            DisplayStudent(LoadStudent(new EnrollmentsEntities(),_studentId));
            LoadClassHistory(_studentId);

        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected List<Pace.DataAccess.Enrollment.Student> Get_DeleteStudent()
    {
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Status")
                                     where f.StudentId == _studentId
                                     select f;
            return StudentSearchQuery.ToList();
        }
    }

    
    
    private Student LoadStudent(EnrollmentsEntities entity, Int32 studentId) //Load Student if passed from Enrollment page
    {

        IQueryable<Student> studentQuery = from s in entity.Student.Include("Agency").Include("Xlk_Nationality").Include("Xlk_Gender").Include("Xlk_Status").Include("Exam").Include("Xlk_StudentType")
                                           where s.StudentId == studentId
                                           select s;
                                           //select new { s ,ImageId = s.StudentImages.Where(x=>x.Xlk_ImageType.ImageTypeId == 1).Select(y=>y.ImageId).FirstOrDefault()};

        if (studentQuery.ToList().Count() > 0)
            return studentQuery.ToList().First();

        return null;
    }



    private void DisplayStudent(Student student) //Display loaded Student details
    {
        if (student != null)
        {
            StudentId.Text = student.StudentId.ToString();
 
            Name.Text = String.Format("{0} {1}", student.FirstName.ToTitleCase(), student.SurName.ToTitleCase());

            CourseStart.Text = student.ArrivalDate.ToString("d");
            CourseFinish.Text = student.DepartureDate.Value.ToString("d");
            Weeks.Text = CalculateTimePeriod(student.ArrivalDate, student.DepartureDate, 'W');
            gendertext.Text = student.Xlk_Gender.Description;

            if (student.Xlk_Gender.Description == "Female")
            {
                gender.ImageUrl = "~/Content/img/female.png";
                gender.ToolTip = "Female";
            }
            else
            {
                gender.ImageUrl = "~/Content/img/male.png";
                gender.ToolTip = "Male";
            }
            if (student.DateOfBirth.HasValue)
                DOB.Text = student.DateOfBirth.Value.ToString("dd/MM/yyyy");
            Nationality.Text = student.Xlk_Nationality.Description;
            //Agent.Text = "Agent: " + student.Agency.Name;
            if (student.PlacementTestResult.HasValue)
                Placement.Text = (student.PlacementTestResult.Value/100).ToString("0.0%");
            if (student.PrepaidBook)
                ppbook.Text = "YES";
            else
                ppbook.Text = "NO";

            if (student.Exam != null)
                exam.Text = student.Exam.ExamName;
            else
                exam.Text = "NO";

            if (student.OwnAccommodation.HasValue && student.OwnAccommodation.Value)
                ownaccomodation.Text = "YES";
            else
                ownaccomodation.Text = "NO";


            if (student.GroupId.HasValue)
            {
                using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
                {

                    group.Text = GroupLink(student.GroupId, "~/Groups/ViewGroupPage.aspx", entity.Group.Single(g => g.GroupId == student.GroupId.Value).GroupName);
                }
            }

            agency.Text = student.Agency.Name.ToString();
            status.Text = student.Xlk_Status.Description.ToString();
            email.Text = student.StudentEmail;

            isCancelled.Visible = (student.Xlk_Status.StatusId == 5);
            notCancelled.Visible = !(student.Xlk_Status.StatusId == 5);
        }
    }

    private Student GetStudent(EnrollmentsEntities entity) //Load Student or Create new Student
    {
        return LoadStudent(entity,_studentId);
    }

    private void SaveStudent(EnrollmentsEntities entity, Student student) //Final Save stage & Redirect
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayStudent(LoadStudent(entity,_studentId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }

    private void LoadClassHistory(Int32 studentId) //Display Class History for Student
    {
        using (Tuition.TuitionEntities entity = new Tuition.TuitionEntities())
        {
            var studentClassHistoryQuery = from e in entity.Enrollment.Include("Xlk_CourseType").Include("Teacher").Include("ClassRoom")
                                           from s in e.ClassSchedule
                                           where e.Student.StudentId == studentId
                                           select new { s.StartDate, TuitionLevel = s.Class.TuitionLevel.Description, CEFR = s.Class.TuitionLevel.CEFR, Teacher = s.Class.Teacher.Name, CourseType = s.Class.Xlk_CourseType.Description, ClassId = s.Class.ClassId };


            classHistory.DataSource = studentClassHistoryQuery.ToList();
            classHistory.DataBind();
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> FilterFamilies(int typeid)
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
            {
                var familyQuery = (from f in entity.Families
                                   orderby new { f.SurName, f.FirstName }
                                   select new { FamilyId = f.FamilyId, Address = f.Address, Name = f.SurName + " " + f.FirstName, FamilyTypeId = f.Xlk_FamilyType.FamilyTypeId });
                if (typeid > 0)
                    familyQuery = familyQuery.Where(f => f.FamilyTypeId == typeid);

                foreach (var item in familyQuery.ToList())
                {
                    sb.AppendFormat("<option value={0}>{1}      {2}</option>", item.FamilyId, item.Name, item.Address);
                }
            }
            return new AjaxResponse<string>(sb.ToString());
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetCourseTypes(int programmetypeid)
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                var lessonblockQuery = (from lb in entity.Xlk_CourseType
                                        from ct in lb.Xlk_ProgrammeType
                                        where ct.ProgrammeTypeId == programmetypeid
                                        orderby lb.CourseTypeId
                                        select lb);


                foreach (var item in lessonblockQuery.ToList())
                {
                    sb.AppendFormat("<option value={0}>{1}</option>", item.CourseTypeId, item.Description);
                }
            }
            return new AjaxResponse<string>(sb.ToString());
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetLessonBlocks(int courestypeid)
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                var lessonblockQuery = (from lb in entity.Xlk_LessonBlock
                                        from ct in lb.Xlk_CourseType
                                        where ct.CourseTypeId == courestypeid
                                        orderby lb.Description
                                        select lb);

                
                foreach (var item in lessonblockQuery.ToList())
                {
                    sb.AppendFormat("<option value={0}>{1}</option>", item.LessonBlockId, item.Description);
                }
            }
            return new AjaxResponse<string>(sb.ToString());
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    
}
