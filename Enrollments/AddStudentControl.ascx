﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentControl.ascx.cs"
    Inherits="Enrollments_AddStudentControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addstudent");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudent") %>'
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $(".datetext").datepicker({ dateFormat: 'dd/mm/yy' });

        $('.horizontalcheckboxes').change(function () {

            var classname = $(this).find('input:checkbox').attr('class');
            var total = 0;

            $("." + classname + ":checked").each(function () {
                total += parseInt($(this).val(), 10);
            });

            $('#' + classname + '').val(total);

        });

    });
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addstudent" name="addstudent" method="post" action="ViewStudentPage.aspx/SaveStudent">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="StudentId" value="<%= GetStudentId %>" />
        <input type="hidden" id="GroupingBlockId" value="<%= GetGroupingBlockId %>" />
        <label>
            Student First Name(s)
        </label>
        <input type="text" name="FirstName" id="FirstName" value="<%= GetFirstName %>" />
        <label>
            Student Surname(s)
        </label>
        <input type="text" name="SurName" id="SurName" value="<%= GetSurName %>" />
        <label>
            Gender
        </label>
        <select name="Xlk_Gender" id="GenderId">
            <%= GetGender() %>
        </select>
        <label>
            Date of Birth
        </label>
        <input class="datetext" type="datetext" name="DateOfBirth" id="DateOfBirth" value="<%= GetDateOfBirth %>" />
        <label>
            Nationality
        </label>
        <select name="Xlk_Nationality" id="NationalityId">
            <%= GetNationality() %>
        </select>
        <label>
            Exam
        </label>
        <select name="Exam" id="ExamId">
            <%= GetExam() %>
        </select>
        <label>
            Group
        </label>
        <select id="GroupId">
            <%= GetGroupList() %>
        </select>
    </fieldset>
    <fieldset>
        <label>
            Arrival Date
        </label>
        <input class="datetext" type="datetext" name="ArrivalDate" id="ArrivalDate" value="<%= GetArrivalDate %>" />
        <label>
            Departure Date
        </label>
        <input class="datetext" type="datetext" name="DepartureDate" id="DepartureDate" value="<%= GetDepartureDate %>" />
        <label>
            Agent
        </label>
        <select name="Agency" id="AgencyId">
            <%= GetAgent() %>
        </select>
        <label>
            Status
        </label>
        <select name="Xlk_Status" id="StatusId">
            <%= GetStatus() %>
        </select>
        <label>
            Placement Test score
        </label>
        <input type="text" id="PlacementTestResult" value="<%= GetPlacement %>" />
        <label>
            Student Type
        </label>
        <select name="Xlk_StudentType" id="StudentTypeId">
            <%= GetStudentType() %>
        </select>
    </fieldset>
    <fieldset>
        <label>
            Campus
        </label>
        <select id="CampusId">
            <%= GetCampus() %>
        </select>
        <label>
            Prepaid Book?
        </label>
        <input type="checkbox" id="PrepaidBook" value="true" <%= GetPrepaidBook %> />
        <label>
            Prepaid Exam?
        </label>
        <input type="checkbox" id="PrepaidExam" value="true" <%= GetPrepaidExam %> />
        <label>
            Own Accommodation?
        </label>
        <input type="checkbox" id="OwnAccommodation" value="true" <%= GetOwnAccommodation %> />
        <label>
            Home Address
        </label>
        <input type="text" name="HomeAddress" id="HomeAddress" value="<%= GetHomeAddress %>" />
        <label>
            Email
        </label>
        <input type="text" name="StudentEmail" id="StudentEmail" value="<%= GetStudentEmail %>" />
        <label>
            Emergency No.
        </label>
        <input type="text" id="EmergencyContactNumber" value="<%= GetEmergencyContactNumber %>" />
    </fieldset>
</div>
</form>
