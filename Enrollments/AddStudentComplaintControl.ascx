﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentComplaintControl.ascx.cs"
    Inherits="Student_AddComplaint" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addstudentcomplaint");
        }

        getTargetUrl = function() {
            return return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudentComplaint") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addstudentcomplaint" name="addstudentcomplaint" method="post" action="ViewAccommodationPage.aspx/SaveFamilyComplaint">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
        <input type="hidden" id="StudentComplaintId" value="<%= GetStudentComplaintId %>" />
                <label>
            Severity
        </label>
        <select type="text" name="Xlk_Severity" id="SeverityId">
            <%= GetSeverityList() %>
        </select>
        <label>
            Type
        </label>
        <select type="text" name="Xlk_ComplaintType" id="ComplaintTypeId">
            <%= GetComplaintTypeList() %>
        </select>
        <label>
            Complaint
        </label>
        <input type="text" id="Complaint" value="<%=GetComplaint %>" />
        <label>
            Action Needed
        </label>
        <input type="checkbox" id="ActionNeeded" <%=GetAction %>  value="true"/>
        <label>
            Raised By
        </label>
        <select type="text" name="Users" id="UserId">
            <%= GetComplaintUserList() %>
        </select>
    </fieldset>
</div>
</form>
