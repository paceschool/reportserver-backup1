﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewStudentPage.aspx.cs" Inherits="Enrollments_ViewStudentPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/Js/Silverlight.js") %>"></script>
    <script type="text/javascript">
        $(function () {

            var Id = <%= _studentId %>;

            $("table").tablesorter();

            refreshCurrentPage = function () {
                location.reload();
            }


            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }

            refreshPage = function () {
                location.reload();
            }


            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });

            showPrintOptions = function () {
                $("#dialog-print").dialog("open");
            };

            $("#dialog-print").css({ 'background-color': '#E0FFFF' }).dialog({
                autoOpen: false,
                modal: true
            });

            function checkLength(o, n, min, max) {
                if (o.val().length > max || o.val().length < min) {
                    o.addClass("ui-state-error");
                    updateTips("Length of " + n + " must be between " + min + " and " + max + ".");
                    return false;
                } else {
                    return true;
                }
            }

            deleteHosting = function (hostingid) {
                $("#dialog-message").html("<p class='validateTips'>All form fields are required.</p><form><div class='inputArea'><fieldset><input type='hidden' id='HostingId' value=" + hostingid + "></input><label for='folder'>Reason for cancellation</label><textarea rows='4' id='CancelledReason'></textarea></fieldset></div></form>").dialog("open");
            };

            $("#dialog-message").html("").dialog({
                autoOpen: false,
                height: 300,
                width: 350,
                modal: true,
                buttons: {
                    "Cancel Hosting": function () {
                        var bValid = true,
                        cancelledreason = $("#CancelledReason");
                        hostingid = $("#HostingId");
                        bValid = bValid && checkLength(cancelledreason, "CancelledReason", 3, 200);
                        if (bValid) {
                            $.ajax({
                                type: "POST",
                                url: "ViewStudentPage.aspx/DeleteHosting",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: "{hostingid:" + hostingid.val() + ",cancelledreason:'" + cancelledreason.val() + "'}",
                                success: function (msg) {
                                if (msg.d != null) {
                                    if (msg.d.Success) {
                                        refreshCurrentTab();
                                        $("#dialog-message").dialog("close");
                                    }
                                    else
                                        alert(msg.d.ErrorMessage);
                                }
                                else
                                    alert("No Response, please contact admin to check!");
                                }
                            });
                        }
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    $([]).add(folder).val("").removeClass("ui-state-error");
                }
            });

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Student Details Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="imgdiv" style="position: absolute; top: 100px; right: 200px; z-index: 2;
        border: solid 1px black; visibility: hidden;">
    </div>
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col" style="width: 7%">
                        <label>
                            Student ID
                        </label>
                    </th>
                    <td>
                        <a style="height: 50px; width: 50px;" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentImageControl.ascx',{'StudentId': <%=_studentId %>}, refreshCurrentPage );">
                            <img src="../Handler/ImageHandler.ashx?id=<%=_studentId %>&type=student&thumbnail=true" /></a>
                        <asp:Label ID="StudentId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Name
                        </label>
                    </th>
                    <td style="text-align: left;">
                        <asp:Label ID="Name" runat="server" Font-Bold="true" Font-Size="Larger">
                        </asp:Label>
                        <%= PopUpLink("StudentId", _studentId, "~/Enrollments/ViewStudentPage.aspx", "", "PopUpFullStudent")%>
                    </td>
                    <th scope="col">
                        <label>
                            Gender
                        </label>
                    </th>
                    <td style="text-align: center; font-size: larger">
                        <asp:Image ID="gender" runat="server" />
                        <asp:Label ID="gendertext" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Arrival Date
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="CourseStart" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Departure Date
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="CourseFinish" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            #Weeks
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Weeks" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Nationality
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Nationality" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            DOB
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="DOB" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Placement
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Placement" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                                <tr>
                    <th scope="col">
                        <label>
                            Agency
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="agency" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Status
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="status" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Email Address
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="email" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Prepaid Book/Exam/Own Accom?
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="ppbook" runat="server">
                        </asp:Label>/
                        <asp:Label ID="exam" runat="server">
                        </asp:Label>/
                        <asp:Label ID="ownaccomodation" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Group
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="group" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Actions
                        </label>
                    </th>
                    <td style="text-align: center">
                    <span id="isCancelled" runat="server">
                        <a href="#" title="Delete Student">
                            <img src="../Content/img/actions/delete.png" title="Print Options" />
                        </a></span>
                         <span id="notCancelled" runat="server">
                        <a title="Edit Student" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentControl.ascx',{'StudentId':<%=_studentId %>}, refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a> <a href="#" title="Create Tasks"
                                onclick="ajaxMessageBox('ViewStudentPage.aspx','ProcessTasks','All tasks complete or incomplete for this Student will be deleted, are you sure you wish to continue?', <%=_studentId %>)">
                                <img src="../Content/img/actions/tools.png" title="Create Tasks" /></a>&nbsp;
                        <a href="#" title="Print Options" onclick="showPrintOptions();">
                            <img src="../Content/img/actions/printer.png" title="Print Options" />
                        </a>
                        </span>
                    </td>
                </tr>
            </tbody>

            <%--        <table class="box-table-a">
            <thead>
                <tr>
                    <th scope="col" colspan="2" style="width: 7%">
                        <label>
                            Student ID
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Name
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Arrival Date
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Departure Date
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            #Weeks
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Gender
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Nationality
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            DOB
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Placement
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Prepaid Book/Exam?
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Group
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Actions
                        </label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a style="height: 50px; width: 50px;" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentImageControl.ascx',{'StudentId': <%=_studentId %>}, refreshCurrentPage );">
                            <img src="../Handler/ImageHandler.ashx?id=<%=_studentId %>&type=student&thumbnail=true" /></a>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="StudentId" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: left; background-color: Yellow">
                        <asp:Label ID="Name" runat="server" Font-Bold="true" Font-Size="Larger">
                        </asp:Label>
                        <%= PopUpLink("StudentId", _studentId, "~/Enrollments/ViewStudentPage.aspx", "", "PopUpFullStudent")%>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="CourseStart" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="CourseFinish" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="Weeks" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: center; font-size: larger">
                        <asp:Image ID="gender" runat="server" />
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="Nationality" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="DOB" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="Placement" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="ppbook" runat="server">
                        </asp:Label>/
                        <asp:Label ID="exam" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:Label ID="group" runat="server">
                        </asp:Label>
                    </td>
                    <% foreach (Pace.DataAccess.Enrollment.Student item in Get_DeleteStudent())
                       {
                           if (item.Xlk_Status.StatusId == 5)
                           {
                    %>
                    <td style="text-align: center">
                        <a href="#" title="Delete Student">
                            <img src="../Content/img/actions/delete.png" title="Print Options" />
                        </a>
                    </td>
                    <%}
                           else
                           {%>
                    <td style="text-align: center">
                        <a title="Edit Student" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentControl.ascx',{'StudentId':<%=_studentId %>}, refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a> <a href="#" title="Create Tasks"
                                onclick="ajaxMessageBox('ViewStudentPage.aspx','ProcessTasks','All tasks complete or incomplete for this Student will be deleted, are you sure you wish to continue?', <%=_studentId %>)">
                                <img src="../Content/img/actions/tools.png" title="Create Tasks" /></a>&nbsp;
                        <a href="#" title="Print Options" onclick="showPrintOptions();">
                            <img src="../Content/img/actions/printer.png" title="Print Options" />
                        </a>
                    </td>
                    <%}
                       }%>
                </tr>
            </tbody>--%>
        </table>
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="ViewStudentPage.aspx/LoadEnrollments?id=<%= _studentId %>">Academic</a></li>
                <li><a href="ViewStudentPage.aspx/LoadHostings?id=<%= _studentId %>">Accomodation</a></li>
                <li><a href="ViewStudentPage.aspx/LoadStudentTransfers?id=<%= _studentId %>">Transfers</a></li>
                <li><a href="ViewStudentPage.aspx/LoadOrderedItems?id=<%= _studentId %>">Ordered Items</a></li>
                <li><a href="ViewStudentPage.aspx/LoadInvoiceReceipts?id=<%= _studentId %>">Receipts</a></li>
                <li><a href="#tabs-1">Class History</a></li>
                <li><a href="ViewStudentPage.aspx/LoadStudentComplaints?id=<%= _studentId %>">Complaints</a></li>
                <li><a href="ViewStudentPage.aspx/LoadNotes?id=<%= _studentId %>">Notes</a></li>
                <li><a href="ViewStudentPage.aspx/LoadPreTestByStudent?id=<%= _studentId %>">Results</a></li>
                <li><a href="ViewStudentPage.aspx/LoadStudentInvoice?id=<%= _studentId %>">Finance Refs</a></li>
                <li><a href="ViewStudentPage.aspx/LoadStudentTasks?id=<%= _studentId %>">Tasks</a></li>
                <li><a href="ViewStudentPage.aspx/LoadNewStudentDocs?id=<%= _studentId %>">Documents</a></li>
            </ul>
            <div id="tabs-1">
                <table id="box-table-a" class="tablesorter">
                    <thead>
                        <tr>
                            <th scope="col">
                                Date Started
                            </th>
                            <th scope="col">
                                Level
                            </th>
                            <th scope="col">
                                CEFR
                            </th>
                            <th scope="col">
                                Teacher
                            </th>
                            <th scope="col">
                                Course Type
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="classHistory" runat="server" EnableViewState="True">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "StartDate", "{0:D}")%>
                                    </td>
                                    <td style="text-align: left">
                                        <a href="../Tuition/ViewClassPage.aspx?ClassId=<%#DataBinder.Eval(Container.DataItem, "ClassId") %>">
                                            <%# DataBinder.Eval(Container.DataItem, "TuitionLevel")%>
                                        </a>
                                    </td>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "CEFR")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "Teacher")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "CourseType")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="dialog-print" title="Print Options" style="display: none;">
        <fieldset>
            <a href="<%=ReportPath("EnrollmentReports","StudentForms","PDF", new Dictionary<string, object> { {"StudentId",_studentId}}) %>"
                title="Print First Day Forms">
                <img src="../Content/img/actions/report-paper.png" title="Print First Day Forms" />
                Print First Day Forms </a>
            <br />
            <a href="<%=ReportPath("AcademicReports","NewIndividualStudentCert","HTML4.0", new Dictionary<string, object> { {"StudentId",_studentId}}) %>"
                title="Print Certificate">
                <img src="../Content/img/actions/printer2.png" title="Print Certificate" />
                Print Certificate </a>
            <br />
            <a href="<%=ReportPath("EnrollmentReports","StudentEnrollmentLetter","HTML4.0", new Dictionary<string, object> { {"StudentId",_studentId}}) %>"
                title="Print Enrollment Letter">
                <img src="../Content/img/actions/printer.png" title="Print Enrollment Letter" />
                Print Enrollment Letter </a>
            <br />
            <a href="<%=ReportPath("EnrollmentReports","StudentReport","HTML4.0", new Dictionary<string, object> { {"StudentId",_studentId}}) %>"
                title="Print Report Form">
                <img src="../Content/img/actions/accept.png" title="Print Report Form" />
                Print Report Form </a>
            <br />
            <a href="<%=ReportPath("EnrollmentReports","EOCAppraisal","PDF", new Dictionary<string, object> { {"StudentId",_studentId}}) %>"
                title="Print Appraisal Form">
                <img src="../Content/img/actions/printer.png" title="Print Appraisal Form" />
                Print Appraisal Form </a>
            <br />
            <a href="<%=ReportPath("EnrollmentReports","BusStopDetails","PDF", new Dictionary<string, object> { {"StudentId",_studentId}}) %>"
                title="Private Bus Routes">
                <img src="../Content/img/actions/printer.png" title="Private Bus Route" />
                Private Bus Route</a>
            <br />
            <a href="<%=ReportPath("TuitionReports", "Tuition_HomeTuitionInterimReport", "PDF", new Dictionary<string, object> { {"StudentId", _studentId} }) %>"
                title="Home Tuition Interim Report">
                <img src="../Content/img/actions/printer.png" title="Home Tuition Interim Report" />
                Home Tuition Interim Report</a>
            <br/>
        </fieldset>
    </div>
</asp:Content>
