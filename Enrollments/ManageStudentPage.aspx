﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManageStudentPage.aspx.cs" Inherits="AddStudentPage"  %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
        $(function() {
            $("table").tablesorter()
            $("#<%= newarrdate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<%= newdepdate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
             $("#<%= dob.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
 
            $('#<%= cancel.ClientID %>').click(function() {
                parent.history.back();
                return false;
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">   
<%--    <div id="pagename">
        <span style="font-style:italic; font-weight:bolder; font-size:larger">
            Manage Student Page
        </span>
    </div>--%>
    <div class="resultscontainer">
        <div id="errorcontainer" name="errorcontainer" class="error" runat="server" visible="false">
            <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
        </div>
        <div class="inputArea">
            <fieldset>
                <label>
                    Student First Name</label>
                <asp:TextBox ID="newfname" runat="server">
                </asp:TextBox>
                <label>
                    Student Surname</label>
                <asp:TextBox ID="newsname" runat="server">
                </asp:TextBox>
                <label>
                    Gender
                </label>
                <asp:DropDownList ID="gender" runat="server" DataTextField="Description" DataValueField="GenderId">
                </asp:DropDownList>
                <label>
                    Date of Birth
                </label>
                <asp:TextBox ID="dob" runat="server">
                </asp:TextBox>
                <label>
                    Student Nationality</label>
                <asp:DropDownList ID="newnationality" runat="server" DataTextField="Description"
                    DataValueField="NationalityId">
                </asp:DropDownList>
                <label>
                    Exam
                </label>
                <asp:DropDownList ID="newexam" runat="server"  DataTextField="ExamName" 
                    DataValueField="ExamId" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value="0">
                    </asp:ListItem>
                </asp:DropDownList>
            </fieldset> 
            <fieldset>
                <label>
                    Arrival Date
                </label>
                <asp:TextBox ID="newarrdate" runat="server" OnTextChanged="CheckArrival" EnableViewState="true" on >
                </asp:TextBox>
                <label>
                    Departure Date
                </label>
                <asp:TextBox ID="newdepdate" runat="server"></asp:TextBox>
               <label>
                    Agent</label>
                <asp:DropDownList ID="newagent" runat="server" DataTextField="Name" DataValueField="AgencyId"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="Choose Agency" Value="0">
                    </asp:ListItem>
                </asp:DropDownList>
                <label>
                    Status
                </label>
                <asp:DropDownList ID="newstatus" runat="server" DataTextField="Description" DataValueField="StatusId">
                </asp:DropDownList>
                <label>
                    Placement Test Score
                </label>
                <asp:TextBox ID="newplacement" runat="server" Text="0.0"></asp:TextBox>
               <label>
                    Campus
                </label>
                <asp:DropDownList ID="listcampus" runat="server"  DataValueField="CampusId" DataTextField="Name">
                </asp:DropDownList>
            </fieldset> 
            <fieldset>
                <label>
                    Prepaid Book?
                </label>
                <asp:CheckBox ID="chkbook" runat="server" Checked="false" />
                <label>
                    Prepaid Exam?
                </label>
                <asp:CheckBox ID="chkexam" runat="server" Checked="false" />
                <label>
                    Student Type
                </label>
                <asp:DropDownList ID="stype" runat="server" DataTextField="Description" DataValueField="StudentTypeId">
                </asp:DropDownList>
                <asp:LinkButton ID="newsave" runat="server" class="button" OnClick="Click_Save"><span>Save</span></asp:LinkButton>
                <asp:LinkButton ID="cancel" runat="server" class="button" OnClientClick=""><span>Cancel</span></asp:LinkButton>
            </fieldset>
        </div>
    </div>
</asp:Content>
