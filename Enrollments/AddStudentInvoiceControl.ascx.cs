﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Student_AddStudentInvoiceControl : BaseEnrollmentControl
{
    protected Int32 _groupInvoiceId;
    protected StudentInvoice _invoice;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("StudentInvoiceId").HasValue)
        {
            _groupInvoiceId = GetValue<Int32>("StudentInvoiceId");
            using (EnrollmentsEntities entity = new EnrollmentsEntities())
            {
                _invoice = (from invoices in entity.StudentInvoice.Include("Student").Include("Xlk_SageDataType")
                            where invoices.StudentInvoiceId == _groupInvoiceId
                            select invoices).FirstOrDefault();
            }
        }
    }

    protected string GetSageRef
    {
        get
        {
            if (_invoice != null)
                return _invoice.SageInvoiceRef.ToString();

            return "";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_invoice != null)
                return _invoice.Student.StudentId.ToString();

            if (GetValue<Int32?>("StudentId") != null)
                return GetValue<Int32>("StudentId").ToString();
            return "0";
        }
    }
    protected string GetStudentInvoiceId
    {
        get
        {
            if (_invoice != null)
                return _invoice.StudentInvoiceId.ToString();


            if (GetValue<Int32?>("StudentInvoiceId").HasValue)
                return GetValue<Int32>("StudentInvoiceId").ToString();

            return "0";
        }
    }

    protected string GetDataType()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_invoice != null && _invoice.Xlk_SageDataType != null) ? _invoice.Xlk_SageDataType.SageDataTypeId : (byte?)null;

        foreach (Xlk_SageDataType item in LoadDataTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.SageDataTypeId, item.Description, (_current.HasValue && item.SageDataTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}