﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Tuition;
using System.Text;
using System.Web.Services;

public partial class Student_AddStudentResultControl : BaseEnrollmentControl
{
    protected Int32 StudentId;
    protected Int32 PreTestId;
    protected PreTest _test;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string GetStudentId
    {
        get
        {
            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

}