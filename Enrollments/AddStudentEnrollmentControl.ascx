﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentEnrollmentControl.ascx.cs"
    Inherits="Student_AddEnrollment" %>

<script type="text/javascript">
    $(function () {

        $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#EndDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function () {
            return $("#addenrollment");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveEnrollment") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        loadcourseTypes = function (programmetypeid) {
            $("#CourseTypeId").html('');
            $.ajax({
                type: "POST",
                url: "ViewStudentPage.aspx/GetCourseTypes",
                data: "{programmetypeid:" + programmetypeid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $("#CourseTypeId").html(msg.d.Payload).change();
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        }

        loadLessonBlocks = function (courestypeid) {
            $("#LessonBlockId").html('');
            $.ajax({
                type: "POST",
                url: "ViewStudentPage.aspx/GetLessonBlocks",
                data: "{courestypeid:" + courestypeid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $("#LessonBlockId").html(msg.d.Payload);
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        }

        $("#ProgrammeTypeId").change(function (e) {
            loadcourseTypes($(this).val());

        });

        $("#CourseTypeId").change(function (e) {
            loadLessonBlocks($(this).val());

        });
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addenrollment" name="addenrollment" method="post" action="ViewAccommodationPage.aspx/SaveFamilyNote">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Student" id="StudentId" value="<%=GetStudentId %>" />
        <input type="hidden" id="EnrollmentId" value="<%= GetEnrollmentId %>" />
        <label>
            Programme Type
        </label>
        <select name="Xlk_ProgrammeType" id="ProgrammeTypeId">
            <%= GetProgrammeTypeList() %>
        </select>
        <label>
            Course Type
        </label>
        <select name="Xlk_CourseType" id="CourseTypeId">
            <%= GetCourseTypeList() %>
        </select>
        <label>
            Hours
        </label>
        <select name="Xlk_LessonBlock" id="LessonBlockId">
            <%= GetLessonBlockTypeList() %>
        </select>
        <label for="StartDate">
            Start Date</label>
        <input type="datetext" name="StartDate" id="StartDate" value="<%= GetStartDate %>" />
        <label for="EndDate">
            End Date</label>
        <input type="datetext" name="EndDate" id="EndDate" value="<%=GetEndDate %>" />
    </fieldset>
</div>
</form>
