﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentImageControl.ascx.cs" 
    Inherits="Student_AddStudentResultControl" %>

    <script type="text/javascript">
        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }

    $(function () {

        getForm = function () {
            return $("#addstudentpretest");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudentProfileImage") %>';
        }

        getLocalRules = function () {
        var localrules = {};
            localrules = {
                EncodedString: {
                    required: true,
                    minlength: 1,
                    minStrict: 0
                }
                            }
            return localrules;
        }

        getLocalMessages = function () {
         var localmessages = {};
            localmessages = {
                   EncodedString: {
                    required: "Please take a picture",
                    minlength: "Click tke photo"
                }
            }
            return localmessages;
        }

    });
</script>

<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addstudentpretest" name="addstudentpretest" method="post" action="ViewStudentPage.aspx/SaveStudentProfileImage">
        <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
        <input type="hidden" id="EncodedString" value="" />
        <input type="hidden" name="Xlk_ImageType" id="ImageTypeId" value="1" />
        <input type="hidden" name="Xlk_MimeType" id="MimeTypeId" value="1" />
        <input type="hidden" id="Title" value="Profile Picture" />

    <div id="silverlightControlHost">
        <object data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="400" height="300">
		  <param name="source" value="../ClientBin/Pace.Silverlight.Controls.xap"/>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="5.0.61118.0" />
		  <param name="autoUpgrade" value="true" />
		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
	    </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe></div>

</form>