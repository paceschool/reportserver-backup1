﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentInvoiceControl.ascx.cs" 
    Inherits="Student_AddStudentInvoiceControl" %>

<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addstudentsageref");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudentInvoice") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addstudentsageref" name="addstudentsageref" method="post" action="ViewStudentPage.aspx/SaveStudentInvoice">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Student" id="StudentId" value="<%=GetStudentId %>" />
        <input type="hidden" id="StudentInvoiceId" value="<%= GetStudentInvoiceId %>" />
                <label>
            Sage Reference
        </label>
        <input type="text" id="SageInvoiceRef"  value="<%= GetSageRef %>" />
        <label>
            Type
        </label>
        <select type="text" name="Xlk_SageDataType" id="SageDataTypeId">
            <%= GetDataType() %>
        </select>
    </fieldset>
</div>
</form>