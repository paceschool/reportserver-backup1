﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentResultControl.ascx.cs" 
    Inherits="Student_AddStudentResultControl" %>

<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addstudentpretest");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudentPreTest") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });
</script>

<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addstudentpretest" name="addstudentpretest" method="post" action="ViewStudentPage.aspx/SaveStudentPreTest">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="PreTestId" value="<%= GetPreTestId %>" />
            <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
            <label>
                Select Exam Date
            </label>
            <select type="text" id="ExamDateId">
                <%= GetExamDates() %>
            </select>
            <label> 
                Enter Score
            </label>
            <input type="text" id="score" value="<%= GetScore %>" />
        </fieldset>
    </div>
</form>