﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentTransferControl.ascx.cs"
    Inherits="Student_AddTransfer" %>

<script type="text/javascript">
    $(function() {

        $("#Date").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function() {
            return $("#addstudenttransfer");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudentTransfer") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addstudenttransfer" name="addstudenttransfer" method="post" action="ViewStudentPage.aspx/SaveStudentTransfer">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Student" id="StudentId" value="<%=GetStudentId %>" />
        <input type="hidden" id="StudentTransferId" value="<%= GetStudentTransferId %>" />
         <input type="hidden" id="TransportBookingId" value="<%= GetTransportBookingId %>" />
        <label>
            Location
        </label>
        <select type="text" name="Xlk_Location" id="LocationId">
            <%= GetLocationList() %>
        </select>
        <label>
            Transfer Type
        </label>
        <select type="text" name="Xlk_TransferType" id="TransferTypeId">
            <%= GetTransferTypeList() %>
        </select>
               <label>
            Completed By
        </label>
        <select type="text" name="Xlk_BusinessEntity" id="EntityId">
            <%= GetBusinessEntityList()%>
        </select>
        <label for="Date">
           Transfer Date</label>
        <input type="datetext" name="Date" id="Date" value="<%= GetDate %>" />
        <label for="FlightNumber">
            Flight Number</label>
        <input type="text" name="FlightNumber" id="FlightNumber" value="<%= GetFlightNumber %>"/>
        <label for="Time">
            Time</label>
        <input type="text" name="Time" id="Time" value="<%= GetTime %>"/>
        <label>
            Comment
        </label>
        <textarea id="Comment"><%= GetComment %></textarea>
    </fieldset>
</div>
</form>
