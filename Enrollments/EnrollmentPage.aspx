﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EnrollmentPage.aspx.cs" Inherits="EnrollmentPage" EnableViewState="true" EnableEventValidation="false" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter();

            $("#dialog:ui-dialog").dialog("destroy");

            refreshCurrentTab = function () {
                location.reload();
            }

            $('#<%= lookupString.ClientID %>').bind('keypress', function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) { //Enter keycode
                    eval($("#<%=searchButton.ClientID %>").attr('href'));
                }
            });

            $("#<%= repeat.ClientID %>").buttonset();

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Enrollments Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Nationality
                    </td>
                    <td>
                        <asp:DropDownList ID="listnationality" runat="server" DataTextField="Description"
                            DataValueField="NationalityId" AppendDataBoundItems="true">
                            <asp:ListItem Text="All Nationalities" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        or by Agent
                    </td>
                    <td>
                        <asp:DropDownList ID="listagent" runat="server" DataTextField="Name" DataValueField="AgencyId"
                            AppendDataBoundItems="true" EnableViewState="true">
                            <asp:ListItem Text="All Agents" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        or by Programme Type
                    </td>
                    <td>
                        <asp:DropDownList ID="listprogrammes" runat="server" DataTextField="Description"
                            DataValueField="ProgrammeTypeId" AppendDataBoundItems="true" EnableViewState="true">
                            <asp:ListItem Text="All Programmes" Value="-1">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Find By Name/Invoice Ref
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="X-Large" OnTextChanged="lookupStudent"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="searchButton" PostBackUrl="~/Enrollments/EnrollmentPage.aspx"
                runat="server" OnClick="lookupStudent"><span>Search Results</span>
            </asp:LinkButton>
            <a href="?Action=lastentered">Last Entered</a> 
            <a href="?Action=current">Current</a>
            <a href="?Action=arriving">Next 30 Days</a>
            <a href="?Action=allarriving">All Arriving</a> 
            <a href="?Action=departing">Departing</a>
            <a href="?Action=junior">Junior Programme</a> 
            <a href="?Action=allnotgroups">All</a>
<%--            <a href="?Action=birthdays">Birthdays this month</a> --%>
        </span>
        <span style="float: right;">
         <a title="Add Student" href="#" onclick="loadEditArrayControl('#dialog-form','EnrollmentPage.aspx','Enrollments/AddStudentControl.ascx',{})"><img src="../Content/img/actions/add.png">Add Student</a>&nbsp;
            <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
            </asp:Label>
        </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Name
                    </th>
                    <th scope="col">
                        Programme Type
                    </th>
                    <th scope="col">
                        Arrival
                    </th>
                    <th scope="col">
                        Departure
                    </th>
                    <th scope="col">
                        #Weeks
                    </th>
                    <th scope="col">
                        Nationality
                    </th>
                    <th scope="col">
                        DOB
                    </th>
                    <th scope="col">
                        Agent
                    </th>
                    <th scope="col">
                        Placement
                    </th>
                    <th scope="col">
                        Exam
                    </th>
                    <th scope="col">
                        Prepaid Book/Exam
                    </th>
                    <th scope="col">
                        Finance Refs
                    </th>
                    <th scope="col">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody style="font-size: smaller">
                <asp:Repeater ID="results" runat="server" EnableViewState="true" OnItemCommand="results_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center">
                                <%#DataBinder.Eval(Container.DataItem, "StudentId") %>
                            </td>
                            <td style="text-align: left">
                            <%# StudentLink(DataBinder.Eval(Container.DataItem, "StudentId"), "~/Enrollments/ViewStudentPage.aspx", CreateName(DataBinder.Eval(Container.DataItem, "FirstName"), DataBinder.Eval(Container.DataItem, "SurName")))%>
                            </td>
                            <td style="text-align: left">
                                <%# ShowProgrammeType(DataBinder.Eval(Container.DataItem, "Enrollment"))%>
                            </td>
                            <td style="text-align: left; font-size: small">
                                <%#DataBinder.Eval(Container.DataItem, "ArrivalDate", "{0:dd/MM/yy}") %>
                            </td>
                            <td style="text-align: left; font-size: small">
                                <%#DataBinder.Eval(Container.DataItem, "DepartureDate", "{0:dd/MM/yy}")%>
                            </td>
                            <td style="text-align: left">
                                <%# CalculateTimePeriod(DataBinder.Eval(Container.DataItem, "ArrivalDate"), DataBinder.Eval(Container.DataItem, "DepartureDate"),'W')%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Xlk_Nationality.Description") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "DateOfBirth", "{0:dd/MM}") %>
                            </td>
                            <td style="text-align: left; font-size: small">
                             <%# AgencyLink(DataBinder.Eval(Container.DataItem, "Agency.AgencyId"), "~/Agent/ViewAgentPage.aspx", DataBinder.Eval(Container.DataItem, "Agency.Name").ToString())%>

                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "PlacementTestResult", "{0:0.0}") %>%
                            </td>
                            <td style="text-align: center">
                                <%#DataBinder.Eval(Container.DataItem, "Exam.ExamName") %>
                            </td>
                            <td style="text-align: center">
                                <asp:Image ID="bookyesno" runat="server" Visible='<%#Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "PrepaidBook")) %>'
                                    ImageUrl="~/Content/img/book_open.png" ToolTip="Prepaid Book" />
                                <%--                            </td>
                            <td style="text-align:left">--%>
                                <asp:Image ID="examyesno" runat="server" Visible='<%#Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "PrepaidExam")) %>'
                                    ImageUrl="~/Content/img/report_pencil.png" ToolTip="Prepaid Exam" />
                            </td>
                            <td style="text-align: right">
                                <%# CreateStudentInvoiceList(DataBinder.Eval(Container.DataItem, "StudentInvoice"))%>
                            </td>
                            <td style="text-align: center">
                                <a href="<%# ReportPath("EnrollmentReports","StudentEnrollmentLetter", new Dictionary<string, object> { { "StudentId", DataBinder.Eval(Container.DataItem, "StudentId")}}) %>"
                                    title="Hosting">
                                    <img alt="Print Enrollment Letter" title="Print Enrollment Letter" src="../Content/img/actions/printer2.png" /></a>&nbsp;
                                <asp:ImageButton CommandName="DeleteStudent" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "StudentId") %>'
                                    ToolTip="Delete Student" Visible='<%# IsCancelled(DataBinder.Eval(Container.DataItem, "Xlk_Status.StatusId"),"5")%>'
                                    ID="deletestudent" runat="server" ImageUrl="../Content/img/actions/bin_closed.png" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <!-- Areas for showing in the dialog -->
    <div id="dialog-form" title="Add new Sage Reference" style="display: none;">
        <p class="validateTips">
            All form fields are required.</p>
        <form>
        <fieldset>
            <label for="addsageref">
                Reference</label>
            <input type="text" name="addsageref" id="addsageref" class="text ui-widget-content ui-corner-all" />
        </fieldset>
        </form>
    </div>
    <div id="delete-sageref-form" title="Delete Sage Reference" style="display: none;">
        <p class="validateTips">
            All form fields are required.</p>
        <form>
        <fieldset>
            <label for="deletesageref">
                Reference</label>
            <select name="deletesageref" id="deletesageref" class="text ui-widget-content ui-corner-all">
            </select>
        </fieldset>
        </form>
    </div>
</asp:Content>
