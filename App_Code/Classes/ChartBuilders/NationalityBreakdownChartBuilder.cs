﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using Pace.DataAccess.Enrollment;

/// <summary>
/// Summary description for NationalityBreakdownChartBuilder
/// </summary>
public class NationalityBreakdownChartBuilder : ChartBuilder
{
    public NationalityBreakdownChartBuilder(Chart chart)
        :base(chart, 1)
    {
     
    }

    protected override void  CustomizeChartSeries(IList<Series> seriesList)
    {
        DateTime _startdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        DateTime _enddate = _startdate.AddMonths(1).AddDays(-1);

        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var query = entity.Student.Where(h => (!h.GroupId.HasValue) && (h.ArrivalDate <= _startdate && h.DepartureDate >= _startdate) || (h.ArrivalDate <= _enddate && h.DepartureDate >= _enddate) || (h.ArrivalDate <= _startdate && h.DepartureDate >= _enddate))
                                  .GroupBy(student => student.Xlk_Nationality.Description)
                                  .OrderBy(group => group.Key);

            var nationalitySeries = seriesList.First();
            BuildNationalitySeries(nationalitySeries);


            var xValues = query.Select(group => group.Key).ToList();

            nationalitySeries.Points.DataBindXY(
                    xValues,
                    query.Select(
                        group => group.Count()).ToList());
        }
    }

    private static void BuildNationalitySeries(Series weatherDelaySeries)
    {
        weatherDelaySeries.Name = "Nationality";
        weatherDelaySeries.Palette = ChartColorPalette.None;
        weatherDelaySeries.Color = Color.Magenta;
        weatherDelaySeries.ChartType = SeriesChartType.Pie;
    }


    protected override void  CustomizeChartTitle(Title title)
    {
        title.Text = "Delays By Day";
    }
}