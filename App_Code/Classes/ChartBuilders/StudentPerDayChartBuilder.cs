﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using Pace.DataAccess.Enrollment;

/// <summary>
/// Summary description for NationalityBreakdownChartBuilder
/// </summary>
public class StudentPerDayChartBuilder : ChartBuilder
{
    public StudentPerDayChartBuilder(Chart chart)
        :base(chart, 1)
    {
     
    }

    protected override void  CustomizeChartSeries(IList<Series> seriesList)
    {

        //var query = BaseEnrollmentPage.Entities.Student
        //                      .GroupBy(student => student..Description)
        //                      .OrderBy(group => group.Key);

        //var nationalitySeries = seriesList.First();
        //BuildNationalitySeries(nationalitySeries);
        DateTime _startdate = DateTime.Now.AddDays(-7);
        DateTime _enddate = DateTime.Now.AddDays(49);

        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            List<Enrollment> enrollments = (from e in entity.Enrollment
                                            where (e.StartDate <= _startdate && e.EndDate >= _startdate) || (e.StartDate <= _enddate && e.EndDate >= _enddate) || (e.StartDate <= _startdate && e.EndDate >= _enddate) || (e.StartDate >= _startdate && e.EndDate <= _enddate)
                                            select e).ToList();

            var result = from e in enrollments
                         from d in (Enumerable.Range(0, (e.EndDate.Value - e.StartDate).Days).Select(days => e.StartDate.AddDays(days)))
                         where (d >= _startdate && d <= _enddate)
                         group e by d.Date into group_enroll
                         select new { Date = group_enroll.Key, Qty = group_enroll.Count() };

            var res = from e in enrollments
                      group e by new { Day = Enumerable.Range(0, 56).Select(days => e.StartDate.AddDays(days)).Select(day => day.Date) } into groups
                      select new { groups.Key, Qty = groups.Count() };
        }

        //var xValues = query.Select(group => group.Key).ToList();

        //nationalitySeries.Points.DataBindXY(
        //        xValues,
        //        query.Select(
        //            group => group.Count()).ToList());
    }

    private static void BuildNationalitySeries(Series weatherDelaySeries)
    {
        weatherDelaySeries.Name = "Students";
        weatherDelaySeries.Palette = ChartColorPalette.None;
        weatherDelaySeries.Color = Color.Magenta;
        weatherDelaySeries.ChartType = SeriesChartType.Bar;
    }


    protected override void  CustomizeChartTitle(Title title)
    {
        title.Text = "Student By Day";
    }
}