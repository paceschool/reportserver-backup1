﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using System.ServiceModel;
using Pace.Common;
using System.Transactions;
using System.Web.Script.Serialization;
using Pace.DataAccess.WebSite;
using System.Net.Mail;

/// <summary>
/// Summary description for BaseEnrollmentPage
/// </summary>
public partial class BaseWebSitePage : BasePage
{
    public WebSiteEntities _entities { get; set; }

    public BaseWebSitePage()
    {
    }

    #region Events

    public static WebSiteEntities CreateEntity
    {
        get
        {
            return new WebSiteEntities();
        }
    }


    public IList<ServiceType> LoadServiceTypes()
    {
        List<ServiceType> values = null;

        if (CacheManager.Instance.GetFromCache<List<ServiceType>>("AllWebSiteServiceType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<ServiceType>>("AllWebSiteServiceType", GetServiceTypes().ToList());
    }
    public IList<RequestType> LoadRequestTypes()
    {
        List<RequestType> values = null;

        if (CacheManager.Instance.GetFromCache<List<RequestType>>("AllWebSiteRequestType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<RequestType>>("AllWebSiteRequestType", GetRequestTypes().ToList());
    }



    #endregion


    #region Private Methods


    protected string ShowCreateStudentString(object siteRequestId, object requestTypeId)
    {

        int[] ids = new[] { 4, 5, 6 };

        if (ids.Contains(Convert.ToInt32(requestTypeId)))
            return string.Format("<a title=\"Create Student\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewSiteRequestPage.aspx','WebContent/AddStudentControl.ascx',{{'SiteRequestId':{0}}})\"><img src=\"../Content/img/actions/user_accept.png\"></a>&nbsp;", siteRequestId);

        return string.Empty;


    }

    private IList<ServiceType> GetServiceTypes()
    {
        using (WebSiteEntities entity = new WebSiteEntities())
        {
            IQueryable<ServiceType> lookupQuery =
                from c in entity.ServiceTypes
                select c;
            return lookupQuery.ToList();
        }
    }

    private IList<RequestType> GetRequestTypes()
    {
        using (WebSiteEntities entity = new WebSiteEntities())
        {
            IQueryable<RequestType> lookupQuery =
                from c in entity.RequestTypes
                select c;
            return lookupQuery.ToList();
        }
    }



    #endregion



    #region Javascript Enabled Methods

    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopUpSiteRequestStrings(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder().AppendFormat("<table  id=\"box-table-a\"><thead><tr><th>Form</th><th>Value</th></tr></thead><tbody>",id);

            using (WebSiteEntities entity = new WebSiteEntities())
            {
                var _filters = (from s in entity.SiteRequestStrings
                                   where s.SiteRequestId == id
                                   select new { s.StringType.Description, s.Value}).ToList();

                if (_filters != null && _filters.Count() > 0)
                {

                    foreach (var filter in _filters)
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", filter.Description, filter.Value);
                    }
                }
                else
                    sb.Append("<tr><td colspan=\"2\"></td></tr>");

                sb.Append("</tbody></table>");
                return sb.ToString();
            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveSiteRequestEmail(SiteRequestEmail newObject)
    {
        try
        {
            SiteRequestEmail newSiteRequestEmail = new SiteRequestEmail();

            using (WebSiteEntities entity = new WebSiteEntities())
            {


                try
                {
                    MailMessage email = new MailMessage();
                    email.To.Add(new MailAddress(newObject.ToAddress));
                    email.Bcc.Add(new MailAddress(newObject.BCCAddress));
                    email.CC.Add(new MailAddress(newObject.CCAddress));
                    email.Body = newObject.Body;
                    email.Subject = newObject.Subject;
                    email.From = new System.Net.Mail.MailAddress(GetCurrentUser().Email, GetCurrentUser().Name);
                    Pace.Common.EmailHelper.SendEmail(email);
                }

                catch (Exception)
                {
                    throw;
                }

                var siteRequest = (from s in entity.SiteRequests
                                  where s.SiteRequestId == newObject.SiteRequest.SiteRequestId
                                  select s).SingleOrDefault();

                siteRequest.OwnerUserId = GetCurrentUser().UserId;
                

                newSiteRequestEmail.Body = newObject.Body;
                newSiteRequestEmail.CCAddress = newObject.CCAddress;
                newSiteRequestEmail.CreatedByUserId = GetCurrentUser().UserId;
                newSiteRequestEmail.DateCreated = DateTime.Now;
                newSiteRequestEmail.Subject = newObject.Subject;
                newSiteRequestEmail.ToAddress = newObject.ToAddress;
                newSiteRequestEmail.FromAddress = newObject.FromAddress;
                newSiteRequestEmail.BCCAddress = newObject.BCCAddress;
                newSiteRequestEmail.SiteRequestId = newObject.SiteRequest.SiteRequestId;


                entity.AddToSiteRequestEmails(newSiteRequestEmail);

                if (entity.SaveChanges() > 0)
                {
                    return new AjaxResponse();
                }
            }

            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    #endregion

}
