﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Excursion;
using System.Globalization;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using Pace.Common;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;

/// <summary>
/// Summary description for BaseExcursionPage
/// </summary>
public partial class BaseExcursionPage : BasePage
{
    public ExcursionEntities _entities { get; set; }
    
    public BaseExcursionPage()
	{
	}

    public static ExcursionEntities CreateEntity
    {
        get
        {
            return new ExcursionEntities();
        }
    }

    public IList<Xlk_ExcursionType> LoadExcursionTypes()
    {
        IList<Xlk_ExcursionType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_ExcursionType>>("All_ExcursionType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_ExcursionType>>("All_ExcursionType", GetExcursionType());
    }

    public static IList<Excursion> LoadExcursions()
    {
        return  GetExcursion();
    }

    public static IList<Booking> LoadBookings()
    {
        IList<Booking> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Booking>>("AllBooking", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Booking>>("AllBooking", GetBookings());
    }

    public IList<Xlk_TransportType> LoadTransportTypes()
    {
        IList<Xlk_TransportType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_TransportType>>("AllExcursionTransportType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_TransportType>>("AllExcursionTransportType", GetTransportTypes());
    }

    public IList<Xlk_SupplierType> LoadSupplierTypes()
    {
        IList<Xlk_SupplierType> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Xlk_SupplierType>>("AllExcursionSupplierType", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Xlk_SupplierType>>("AllExcursionSupplierType", GetSupplierTypes());
    }

    public IList<Supplier> LoadContactDetails()
    {
        IList<Supplier> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Supplier>>("AllExcursionSupplierDetail", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Supplier>>("AllExcursionSupplierDetail", GetSuppliers());
    }

    public IList<Pace.DataAccess.Excursion.Xlk_Status> LoadExcursionStatus()
    {
        IList<Pace.DataAccess.Excursion.Xlk_Status> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Excursion.Xlk_Status>>("AllExcursionStatus", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Excursion.Xlk_Status>>("AllExcursionStatus", GetExcursionStatus());
    }
    public IList<Xlk_Location> LoadLocations()
    {
        IList<Pace.DataAccess.Excursion.Xlk_Location> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Xlk_Location>>("AllExcursionXlk_Location", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Xlk_Location>>("AllExcursionXlk_Location", GetLocations());
    }
    public IList<Xlk_ContactMethod> LoadContactMethods()
    {
        IList<Pace.DataAccess.Excursion.Xlk_ContactMethod> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Xlk_ContactMethod>>("AllExcursionContactMethods", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Xlk_ContactMethod>>("AllExcursionContactMethods", GetContactMethods());
    }
    #region Private Methods

    private IList<Xlk_ContactMethod> GetContactMethods()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_ContactMethod> lookupQuery =
                from s in entity.Xlk_ContactMethod
                select s;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_Location> GetLocations()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_Location> lookupQuery =
                from s in entity.Xlk_Location
                orderby s.Description ascending
                select s;
            return lookupQuery.ToList();
        }
    }


    private IList<Pace.DataAccess.Excursion.Xlk_Status> GetExcursionStatus()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Pace.DataAccess.Excursion.Xlk_Status> lookupQuery =
                from s in entity.Xlk_Status
                select s;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_ExcursionType> GetExcursionType()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_ExcursionType> lookupQuery =
                 from p in entity.Xlk_ExcursionType
                 select p;
            return lookupQuery.ToList();
        }
    }

    private static IList<Excursion> GetExcursion()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Excursion> lookupQuery =
                from p in entity.Excursions
                orderby p.Title
                select p;
            return lookupQuery.ToList();
        }
    }

    private static IList<Booking> GetBookings()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Booking> lookupQuery =
                from b in entity.Bookings
                orderby b.ExcursionDate
                select b;
            return lookupQuery.ToList();
        }
    }

    public static IList<Booking> TodaysExcursions()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Booking> lookupQuery =
                from bk in entity.Bookings
                where bk.ExcursionDate == DateTime.Today && bk.Xlk_Status.StatusId < 3
                select bk;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_TransportType> GetTransportTypes()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_TransportType> lookupQuery =
                from p in entity.Xlk_TransportType
                select p;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_SupplierType> GetSupplierTypes()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_SupplierType> lookupQuery =
                from a in entity.Xlk_SupplierType
                select a;
            return lookupQuery.ToList();
        }
    }

    public IList<Supplier> GetSuppliers()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Supplier> lookupQuery =
                from b in entity.Suppliers
                orderby b.Title ascending
                select b;
            return lookupQuery.ToList();
        }
    }

    #endregion

    #region Protected Methods

    protected DateTime FirstDateOfWeek(int year, int weekNum, CalendarWeekRule rule)
    {

        DateTime jan1 = new DateTime(year, 1, 1);

        int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        DateTime firstMonday = jan1.AddDays(daysOffset);

        var cal = CultureInfo.CurrentCulture.Calendar;
        int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

        if (firstWeek <= 1 || firstWeek >= 52)
        {
            weekNum -= 1;
        }

        DateTime result = firstMonday.AddDays(weekNum * 7);

        return result;
    }

    protected DateTime GetFirstMonday(DateTime seedDate)
    {
        DateTime dt = new DateTime(seedDate.Year, seedDate.Month, 1);
        while (dt.DayOfWeek != DayOfWeek.Monday)
        {
            dt = dt.AddDays(1);
        }
        return dt;
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveLnkStudentExcursion(Lnk_Student_Booking newObject)
    {
        Lnk_Student_Booking newmember = new Lnk_Student_Booking();

        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                if (newObject.StudentBookingId > 0)
                    newmember = (from e in entity.Lnk_Student_Booking where e.StudentBookingId == newObject.StudentBookingId select e).FirstOrDefault();

                newmember.BookingId = newObject.BookingId;
                newmember.StudentId = newObject.StudentId;
                newmember.AmountPaid = newObject.AmountPaid;
                
                if (newObject.AmountPaid > 0 && !newmember.DatePaid.HasValue)
                    newmember.DatePaid = DateTime.Now;

                if (newmember.StudentBookingId == 0)
                {
                    entity.AddToLnk_Student_Booking(newmember);

                }

                entity.SaveChanges();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadConfirmed()
    {
        DateTime _now = DateTime.Now.Date;

        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\"> Id </th><th scope=\"col\"> Title </th><th scope=\"col\"> Date - Time </th><th scope=\"col\"> Transport Type </th><th scope=\"col\"> Meet Time </th><th scope=\"col\"> Meet Location </th><th scope=\"col\"> Confirmed Date <th scope=\"col\"> Confirmed By </th><th scope=\"col\"> Voucher Id </th><th scope=\"col\"> Qty Pace Guides </th><th scope=\"col\"> Actions </th></tr></thead><tbody >");

            using (ExcursionEntities entity = new ExcursionEntities())
            {

                List<Booking> query = (from n in entity.Bookings.Include("Xlk_TransportType").Include("Xlk_Status")
                                       where n.CampusId == GetCampusId &&  n.Xlk_Status.StatusId == 2 && n.Excursion.Xlk_ExcursionType.ExcursionTypeId == 1 && n.ExcursionDate > _now//excursion typeid
                                       orderby n.ConfirmedDate
                                       select n).ToList();
                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td>{2} - {3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td></td><td>{9}</td><td style=\"text-align: center\"><a title=\"Delete Booking\" href=\"#\" onclick=\"deleteBooking({0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>&nbsp;<a title=\"Edit Booking\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','PlanBookingsPage.aspx','Excursions/AddBookingControl.ascx',{{'BookingId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Confirm Booking\" href=\"#\" onclick=\"confirmBooking({0},'conmfirmbooked')\"><img src=\"../Content/img/actions/accept.png\"></a></td></tr>", booking.BookingId, TrimText(booking.Title, 60), booking.ExcursionDate.ToString("dd/MM/yyyy"), booking.ExcursionTime, booking.Xlk_TransportType.Description, booking.MeetTime, booking.MeetLocation, (booking.ConfirmedDate.HasValue) ? booking.ConfirmedDate.Value.ToString("dd/MM/yyyy") : "Not Confirmed", booking.ConfirmedBy, booking.QtyGuides);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadToBeConfirmed()
    {
        DateTime _now = DateTime.Now.Date;

        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\"> Id </th><th scope=\"col\"> Title </th><th scope=\"col\"> Date - Time </th><th scope=\"col\"> Transport Type </th><th scope=\"col\"> Meet Time </th><th scope=\"col\"> Meet Location </th><th scope=\"col\"> Confirmed Date <th scope=\"col\"> Confirmed By </th><th scope=\"col\"> Voucher Id </th><th scope=\"col\"> Qty Pace Guides </th><th scope=\"col\"> Actions </th></tr></thead><tbody >");

            using (ExcursionEntities entity = new ExcursionEntities())
            {

                List<Booking> query = (from n in entity.Bookings.Include("Xlk_TransportType")
                                       where n.CampusId == GetCampusId && n.Xlk_Status.StatusId == 1 && n.Excursion.Xlk_ExcursionType.ExcursionTypeId == 1 //excursion typeid
                                       orderby n.ExcursionDate
                                       select n).ToList();
                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td>{2} - {3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td></td><td>{9}</td><td style=\"text-align: center\"><a title=\"Delete Booking\" href=\"#\" onclick=\"deleteBooking({0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>&nbsp;<a title=\"Edit Booking\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','PlanBookingsPage.aspx','Excursions/AddBookingControl.ascx',{{'BookingId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Confirm Booking\" href=\"#\" onclick=\"confirmBooking({0},'conmfirmbooked')\"><img src=\"../Content/img/actions/accept.png\"></a></td></tr>", booking.BookingId, TrimText(booking.Title, 60), booking.ExcursionDate.ToString("dd/MM/yyyy"), booking.ExcursionTime, booking.Xlk_TransportType.Description, booking.MeetTime, booking.MeetLocation, (booking.ConfirmedDate.HasValue) ? booking.ConfirmedDate.Value.ToString("dd/MM/yyyy") : "Not Confirmed", booking.ConfirmedBy, booking.QtyGuides);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadAll()
    {
        DateTime _now = DateTime.Now.Date;

        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\"> Id </th><th scope=\"col\"> Title </th><th scope=\"col\"> Date - Time </th><th scope=\"col\"> Transport Type </th><th scope=\"col\"> Meet Time </th><th scope=\"col\"> Meet Location </th><th scope=\"col\"> Confirmed Date <th scope=\"col\"> Confirmed By </th><th scope=\"col\"> Voucher Id </th><th scope=\"col\"> Qty Pace Guides </th><th scope=\"col\"> Actions </th></tr></thead><tbody >");

            using (ExcursionEntities entity = new ExcursionEntities())
            {

                List<Booking> query = (from n in entity.Bookings.Include("Xlk_TransportType")
                                       where n.CampusId == GetCampusId
                                       orderby n.ExcursionDate
                                       select n).Take(200).ToList();
                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td>{2} - {3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td></td><td>{9}</td><td style=\"text-align: center\"><a title=\"Delete Booking\" href=\"#\" onclick=\"deleteBooking({0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>&nbsp;<a title=\"Edit Booking\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','PlanBookingsPage.aspx','Excursions/AddBookingControl.ascx',{{'BookingId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Confirm Booking\" href=\"#\" onclick=\"confirmBooking({0},'conmfirmbooked')\"><img src=\"../Content/img/actions/accept.png\"></a></td></tr>", booking.BookingId, TrimText(booking.Title, 60), booking.ExcursionDate.ToString("dd/MM/yyyy"), booking.ExcursionTime, booking.Xlk_TransportType.Description, booking.MeetTime, booking.MeetLocation, (booking.ConfirmedDate.HasValue) ? booking.ConfirmedDate.Value.ToString("dd/MM/yyyy") : "Not Confirmed", booking.ConfirmedBy, booking.QtyGuides);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadRequested()
    {
        DateTime _now = DateTime.Now.AddDays(-14).Date;

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Group Excursion Id</th><th scope=\"col\">Excursion</th><th scope=\"col\">Preferred Date</th><th scope=\"col\">Preferred Time</th><th scope=\"col\">Qty Leaders/Students/Guides</th><th scope=\"col\">Private Transport?</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>"));

            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                var query = entity.ExcursionToBeBooked(GetCampusId).ToList().Where(n => n.PreferredDate > _now);

                if (query.Count() > 0)
                {
                    foreach (var group in query)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1} - {2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5} / {6} / {7}</td><td style=\"text-align: left; font-size:large\"><img src=\"{10}\" /></td><td style=\"text-align: left; font-size:large\"><a href=\"#\" class=\"information\">{8}<span>{9}</span></a></td><td style=\"text-align: center\"><a title=\"Create Booking\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','PlanBookingsPage.aspx','Excursions/AddBookingControl.ascx',{{'GroupExcursionId':{0}}})\"><img src=\"../Content/img/actions/next.png\"></a></td></tr>", group.GroupExcursionId, group.GroupName, group.Title, group.PreferredDate.ToString("D"), group.PreferredTime, group.QtyStudents, group.QtyLeaders, group.QtyGuides, TrimText(group.Comment, 20), group.Comment, (group.TransportRequired.HasValue && group.TransportRequired.Value) ? "../Content/img/actions/accept.png" : "../Content/img/actions/delete.png");
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadDeleted()
    {
        DateTime _now = DateTime.Now.Date;

        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\"> Id </th><th scope=\"col\"> Title </th><th scope=\"col\"> Date - Time </th><th scope=\"col\"> Transport Type </th><th scope=\"col\"> Meet Time </th><th scope=\"col\"> Meet Location </th><th scope=\"col\"> Confirmed Date <th scope=\"col\"> Confirmed By </th><th scope=\"col\"> Voucher Id </th><th scope=\"col\"> Qty Pace Guides </th><th scope=\"col\"> Actions </th></tr></thead><tbody >");
            
            using (ExcursionEntities entity = new ExcursionEntities())
            {

                List<Booking> query = (from n in entity.Bookings.Include("Xlk_TransportType")
                                       where n.Xlk_Status.StatusId == 3 && n.CampusId == GetCampusId
                                       orderby n.ExcursionDate
                                       select n).ToList();
                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td>{2} - {3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td></td><td>{9}</td><td style=\"text-align: center\"><a title=\"Edit Booking\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','PlanBookingsPage.aspx','Excursions/AddBookingControl.ascx',{{'BookingId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Confirm Cancelled\" href=\"#\" onclick=\"confirmBooking({0},'conmfirmcancelled')\"><img src=\"../Content/img/actions/accept.png\"></a></td></tr>", booking.BookingId, TrimText(booking.Title, 60), booking.ExcursionDate.ToString("dd/MM/yyyy"), booking.ExcursionTime, booking.Xlk_TransportType.Description, booking.MeetTime, booking.MeetLocation, (booking.ConfirmedDate.HasValue) ? booking.ConfirmedDate.Value.ToString("dd/MM/yyyy") : "Not Confirmed", booking.ConfirmedBy, booking.QtyGuides);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadBookingTasks(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Support/ViewTaskPage.aspx','Support/AddTaskControl.ascx','Ref',{'BookingId:" + id + "}')\"><img src=\"../Content/img/actions/add.png\" />Add New Task</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Assigned To</th><th scope=\"col\">Priority</th><th scope=\"col\">Status</th><th scope=\"col\">Description</th><th scope=\"col\">Action Needed</th><th scope=\"col\">Date Due</th><th scope=\"col\">Action Taken</th><th scope=\"col\">Date Completed</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            string reference = "BookingId:" + id.ToString();

            using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
            {

                var tasks = (from t in entity.Task.Include("Xlk_Status").Include("Xlk_Priority").Include("RaisedByUser").Include("AssignedToUser")
                             where t.Ref == reference
                             orderby t.DateDue
                             select t).ToList();


                if (tasks != null && tasks.Count > 0)
                {
                    foreach (Pace.DataAccess.Support.Task item in tasks)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{8}</td><td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: center\">", item.TaskId, item.Xlk_Priority.Description, item.Xlk_Status.Description, TrimTextA(item.Description, 40), TrimTextA(item.ActionNeeded, 20), item.DateDue.ToString("dd MMM yyyy"), item.ActionTaken, item.DateCompleted, item.AssignedToUser.Name);

                        if (item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Edit Task\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Support/ViewTaskPage.aspx','Support/AddTaskControl.ascx',{{'TaskId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;", item.TaskId);

                        if (item.AssignedToUser.UserId == GetCurrentUser().UserId | item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Close Task\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Support/ViewTaskPage.aspx','Support/AddTaskControl.ascx',{{ TaskId: {0}, Close: {0}}} )\"><img src=\"../Content/img/actions/user_accept.png\"></a>&nbsp;", item.TaskId);

                        if (item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Delete Item\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('../Support/ViewTaskPage.aspx','Task',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>", item.TaskId);

                        sb.Append("</td></tr>");
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"10\">No Task Records to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveLnkGroupExcursion(Lnk_GroupExcursion_Booking newObject)
    {
        Lnk_GroupExcursion_Booking newmember = new Lnk_GroupExcursion_Booking();

        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                bool exists = (from e in entity.Lnk_GroupExcursion_Booking where e.Booking.BookingId == newObject.Booking.BookingId && e.GroupExcursionId == newObject.GroupExcursionId select e).FirstOrDefault() != null;

                newmember.GroupExcursionId = newObject.GroupExcursionId;
                newmember.Booking.BookingId = newObject.Booking.BookingId;

                if (!exists)
                {
                    entity.AddToLnk_GroupExcursion_Booking(newmember);

                }

                entity.SaveChanges();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveBookingGuide(BookingStaff newObject)
    {
        BookingStaff newmember = new BookingStaff();

        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                BookingStaff _value = (from s in entity.BookingStaffs where s.BookingId == newObject.BookingId && s.ContactId == newObject.ContactId select s).FirstOrDefault();

                if (_value != null)
                    newmember = _value;
                else
                    entity.AddToBookingStaffs(newmember);

                newmember.ContactId = newObject.ContactId;
                newmember.BookingId = newObject.BookingId;

                entity.SaveChanges();
            }
            return new AjaxResponse();
        }

        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveLnkGroup(Lnk_Group_Booking newObject)
    {
        Lnk_Group_Booking newmember = new Lnk_Group_Booking();

        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                Lnk_Group_Booking _value = (from e in entity.Lnk_Group_Booking where e.BookingId == newObject.BookingId && e.GroupId == newObject.GroupId select e).FirstOrDefault();

                if (_value != null)
                    newmember = _value;
                else
                    entity.AddToLnk_Group_Booking(newmember);

                newmember.GroupId = newObject.GroupId;
                newmember.BookingId = newObject.BookingId;
                newmember.QtyGuides = newObject.QtyGuides;
                newmember.QtyLeaders = newObject.QtyLeaders;
                newmember.QtyStudents = newObject.QtyStudents;


                entity.SaveChanges();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetMeetTime(int excursionid,string excursiontime, int transporttypeid)
    {

        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
               ExcursionTransportType transportTypes = entity.ExcursionTransportTypes.Where(t=>t.ExcursionId == excursionid && t.TransportTypeId == transporttypeid).FirstOrDefault();

               if (transportTypes != null && !string.IsNullOrEmpty(excursiontime))
                    return new AjaxResponse<string>(TimeSpan.Parse(excursiontime).Add(new TimeSpan(- ((transportTypes.TransportDurationHours.HasValue) ? transportTypes.TransportDurationHours.Value : 60), 0, 0)).ToString("hh\\:mm"));

               return new AjaxResponse<string>(TimeSpan.Parse(excursiontime).Add(new TimeSpan(- 60, 0, 0)).ToString("hh\\:mm"));
            }
            //return new AjaxResponse<string>(new Exception("No data found in the database, please contact admin"));

        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }
    
   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveExcursionCost(ExcursionCost newObject)
    {
        ExcursionCost newcost;

        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {

                newcost = entity.ExcursionCosts.SingleOrDefault(x => x.ExcursionId == newObject.Excursion.ExcursionId && x.AttendeeTypeId == newObject.Xlk_AttendeeType.AttendeeTypeId);

                if (newcost != null)
                {
                    newcost.CostPerStudent = newObject.CostPerStudent;
                    newcost.MarkupMultiplier = newObject.MarkupMultiplier;
                    newcost.DefaultAdvertisedCost = newObject.DefaultAdvertisedCost;
                    newcost.FreePer = newObject.FreePer;
                }
                else
                {
                    newcost = new ExcursionCost();
                    newcost.ExcursionId = newObject.Excursion.ExcursionId;
                    newcost.AttendeeTypeId = newObject.Xlk_AttendeeType.AttendeeTypeId;
                    newcost.CostPerStudent = newObject.CostPerStudent;
                    newcost.MarkupMultiplier = newObject.MarkupMultiplier;
                    newcost.DefaultAdvertisedCost = newObject.DefaultAdvertisedCost;
                    newcost.FreePer = newObject.FreePer;
                    entity.AddToExcursionCosts(newcost);
                }

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }


   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public static AjaxResponse SaveExcursionDocument(ExcursionDocument newObject)
   {
       ExcursionDocument newcost = new ExcursionDocument();

       try
       {
           using (ExcursionEntities entity = new ExcursionEntities())
           {

               newcost.ExcursionId = newObject.Excursion.ExcursionId;
               newcost.DocId = newObject.DocId;

               entity.AddToExcursionDocuments(newcost);


               if (entity.SaveChanges() > 0)
                   return new AjaxResponse();
           }
           return new AjaxResponse();

       }
       catch (Exception ex)
       {
           return new AjaxResponse(ex);
       }
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public static AjaxResponse SaveBooking(Booking newObject)
    {
        Booking newbooking = new Booking();
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {


                if (newObject.BookingId > 0)
                {
                    newbooking = (from e in entity.Bookings where e.BookingId == newObject.BookingId select e).FirstOrDefault();
                    newbooking.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);
                }
                else
                {
                    entity.AddToBookings(newbooking);
                    newbooking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)1);
                }

                newbooking.BookingId = newObject.BookingId;
                newbooking.Title = newObject.Title;
                newbooking.BookingRef = newObject.BookingRef;
                newbooking.ExcursionDate = newObject.ExcursionDate;
                newbooking.ExcursionTime = newObject.ExcursionTime;
                newbooking.IsClosed = newObject.IsClosed;
                newbooking.MeetLocation = newObject.MeetLocation;
                newbooking.MeetTime = newObject.MeetTime;
                newbooking.QtyGuides = newObject.QtyGuides;
                newbooking.QtyLeaders = newObject.QtyLeaders;
                newbooking.QtyStudents = newObject.QtyStudents;
                newbooking.VoucherId = newObject.VoucherId;
                newbooking.ExcursionReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Excursions", newObject.Excursion);
                newbooking.Xlk_TransportTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_TransportType", newObject.Xlk_TransportType);


                if (entity.SaveChanges(SaveOptions.AcceptAllChangesAfterSave) > 0)
                {

                    if (newObject.GroupExcursionId > 0)
                    {
                        Lnk_GroupExcursion_Booking excurs = new Lnk_GroupExcursion_Booking();
                        excurs.BookingId = newbooking.BookingId;
                        excurs.GroupExcursionId = newObject.GroupExcursionId;
                        entity.AddToLnk_GroupExcursion_Booking(excurs);
                        entity.SaveChanges();
                    }
                }

                if (newObject.BookingId == 0)
                {
                    if (newbooking.BookingId > 0)
                        return new AjaxResponse(string.Format("{0}?BookingId={1}", VirtualPathUtility.ToAbsolute("~/Excursions/ViewBookingPage.aspx"), newbooking.BookingId));
                    else
                        return new AjaxResponse(new Exception("Could not find the new booking Identifier, please search for this booking before trying to add again!"));
                }
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public static AjaxResponse SaveExcursion(Excursion newObject)
   {
       Excursion newexcursion = new Excursion();
       try
       {
           using (ExcursionEntities entity = new ExcursionEntities())
           {


               if (newObject.ExcursionId > 0)
               {
                   newexcursion = (from e in entity.Excursions where e.ExcursionId == newObject.ExcursionId select e).FirstOrDefault();
               }
               else
               {
                   entity.AddToExcursions(newexcursion);
               }

               newexcursion.Comment = newObject.Comment;
               newexcursion.Description = newObject.Description;
               newexcursion.DurationHours = newObject.DurationHours;
               newexcursion.ExcursionImage = newObject.ExcursionImage;
               newexcursion.ImageUrl = newObject.ImageUrl;
               newexcursion.MaxCapacity = newObject.MaxCapacity;
               newexcursion.MinCapacity = newObject.MinCapacity;
               newexcursion.PromoText = newObject.PromoText;
               newexcursion.PromoURI = newObject.PromoURI;
               newexcursion.Title = newObject.Title;
               if (newObject.TransportRequired != null && newObject.TransportRequired)
                   newexcursion.TransportRequired = true;
               else
                   newexcursion.TransportRequired = false;

               if (newObject.VenueGuideSupplied != null && newObject.VenueGuideSupplied)
                   newexcursion.VenueGuideSupplied = true;
               else
                   newexcursion.VenueGuideSupplied = false;

               newexcursion.Xlk_ContactMethodReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_ContactMethod", newObject.Xlk_ContactMethod);
               newexcursion.Xlk_ExcursionTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_ExcursionType", newObject.Xlk_ExcursionType);
               newexcursion.Xlk_LocationReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Location", newObject.Xlk_Location);
               newexcursion.SupplierReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Suppliers", newObject.Supplier);


               entity.SaveChanges();

               if (newObject.ExcursionId == 0)
               {
                   if (newexcursion.ExcursionId > 0)
                       return new AjaxResponse(string.Format("{0}?ExcursionId={1}", VirtualPathUtility.ToAbsolute("~/Excursions/ViewExcursionPage.aspx"), newexcursion.ExcursionId));
                   else
                       return new AjaxResponse(new Exception("Could not find the new booking Identifier, please search for this excursion before trying to add again!"));
               }
           }
           return new AjaxResponse();

       }
       catch (Exception ex)
       {
           return new AjaxResponse(ex);
       }
   }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse AddTransportType(ExcursionTransportType newObject)
    {
        ExcursionTransportType newobj;

        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                newobj = (from e in entity.ExcursionTransportTypes where e.ExcursionId == newObject.ExcursionId && e.TransportTypeId == newObject.TransportTypeId select e).FirstOrDefault();
                
                if (newobj == null)
                {
                    newobj = new ExcursionTransportType();
                    newobj.ExcursionId = newObject.ExcursionId;
                    newobj.TransportCostPerSeat = newObject.TransportCostPerSeat;
                    newobj.TransportDurationHours = newObject.TransportDurationHours;
                    newobj.TransportTypeId = newObject.Xlk_TransportType.TransportTypeId;

                    newobj.Xlk_TransportTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_TransportType", newObject.Xlk_TransportType);
                    if (newObject.Supplier != null && newObject.Supplier.SupplierId > 0)
                        newobj.SupplierReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Supplier", newObject.Supplier);

                    entity.AddToExcursionTransportTypes(newobj);
                }
                else
                {
                    newobj.ExcursionId = newObject.ExcursionId;
                    newobj.TransportCostPerSeat = newObject.TransportCostPerSeat;
                    newobj.TransportDurationHours = newObject.TransportDurationHours;
                    newobj.Xlk_TransportTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_TransportType", newObject.Xlk_TransportType);
                    if (newObject.Supplier != null && newObject.Supplier.SupplierId > 0)
                        newobj.SupplierReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Supplier", newObject.Supplier);
                }

                entity.SaveChanges();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    public static string ConfirmBookingStaff(int bookingid, string action)
    {
        BookingStaff staff;
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                if (bookingid > 0)
                {
                    staff = (from s in entity.BookingStaffs where s.BookingId == bookingid select s).FirstOrDefault();

                    //staff.ConfirmedDate = DateTime.Now;

                    if (entity.SaveChanges() > 0)
                        return string.Empty;
                }
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse ConfirmBooking(int bookingid, string action)
    {
        Booking booking;
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                if (bookingid > 0)
                {
                    booking = (from e in entity.Bookings.Include("Xlk_Status") where e.BookingId == bookingid select e).FirstOrDefault();

                    switch (action)
                    {
                        case "conmfirmcancelled":
                            booking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)4); // confirmed cancelled statusid is 4
                            break;
                        case "conmfirmbooked":
                            booking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)2); // confirmed statusid is 2
                            break;
                        default:
                            break;
                    }
                    booking.ConfirmedDate = DateTime.Now;



                    if (entity.SaveChanges() > 0)
                        return new AjaxResponse();
                }
            }
            return new AjaxResponse(new Exception("Nothing saved, plaese check with admin"));

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse DeleteBooking(int bookingid)
    {
        Booking booking;
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                if (bookingid > 0)
                {
                    booking = (from e in entity.Bookings.Include("Xlk_Status") where e.BookingId == bookingid select e).FirstOrDefault();


                    booking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)3); // tobecancelled statusid is 2

                    if (entity.SaveChanges() > 0)
                        return new AjaxResponse();
                }
            }
            return new AjaxResponse(new Exception("Nothing saved please contact admin"));

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetStudentList(int programmetypeid, int bookingid)
    {
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                Booking _booking = entity.Bookings.Include("Lnk_Student_Booking").Single(b => b.BookingId == bookingid);

                StringBuilder sb = new StringBuilder();
                DateTime _bookingDate = _booking.ExcursionDate;
                List<int> _currentIds = _booking.Lnk_Student_Booking.Select(e => e.StudentId).ToList();


                using (Pace.DataAccess.Enrollment.EnrollmentsEntities eentity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
                {
                    IQueryable<Pace.DataAccess.Enrollment.Student> studentsQuery = (from s in eentity.Student where !s.GroupId.HasValue && !_currentIds.Contains(s.StudentId) && s.ArrivalDate < _bookingDate && s.DepartureDate > _bookingDate select s);

                    if (programmetypeid > 0)
                       studentsQuery = studentsQuery.Where(x => x.Enrollment.Any(y => y.Xlk_ProgrammeType.ProgrammeTypeId == programmetypeid));

                    foreach (Pace.DataAccess.Enrollment.Student item in studentsQuery.ToList())
                    {
                        sb.AppendFormat("<option value={0}>{1} {2}</option>", item.StudentId, item.FirstName, item.SurName);
                    }

                }

                return new  AjaxResponse<string>(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupBooking(int id)
    {
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                var booking = (from b in entity.Bookings.Include("Excursion")
                               where b.BookingId == id
                               select b).SingleOrDefault();

                if (booking != null)
                {
                    return string.Format("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th scope=\"col\">Booking Id:</th><td style=\"text-align:left\">{0}</td></tr><tr><th scope=\"col\">Excursion:</th><td style=\"text-align:left\">{1}</td></tr><tr><th>Meet Location:</th><td>{2}</td></tr><tr><th>Date:</th><td>{3}</td></tr><tr><th>Time:</th><td>{4}</td></tr><tr><th>Leaders/Students:</th><td>{5}/{6}</td></tr></tbody></table>", booking.BookingId, booking.Excursion.Title, booking.MeetLocation, booking.ExcursionDate.ToLongDateString(), booking.ExcursionTime, booking.QtyLeaders, booking.QtyStudents);
                }

                return string.Empty;
            }
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupExcursion(int id)
    {
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                var excursion = (from s in entity.Excursions
                             where s.ExcursionId == id
                             select new { s.ExcursionId, s.Comment, Location = s.Xlk_Location.Description,ExcursionType= s.Xlk_ExcursionType.Description,Supplier= s.Supplier.Title, s.DurationHours,s.MinCapacity,s.Title,s.TransportRequired,s.VenueGuideSupplied, s.MaxCapacity }).SingleOrDefault();

                if (excursion != null)
                {
                    return string.Format("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>ExcursionId</th><td>{0}</td></tr><tr><th>Title</th><td>{1}</td></tr><tr><th>Location</th><td>{2}</td></tr><tr><th>ExcursionType</th><td>{3}</td></tr><tr><th>Capacity Min/Max</th><td>{4}/{5}</td></tr><tr><th>Supplier</th><td>{6}</td></tr><tr><th>Duration</th><td>{7}</td></tr></tbody></table>", excursion.ExcursionId, excursion.Title, excursion.Location, excursion.ExcursionType, excursion.MinCapacity, excursion.MaxCapacity, excursion.Supplier, excursion.DurationHours);
                }

                return string.Empty;

            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool ProcessTasks(int id)
    {
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {

                var excursion = entity.Bookings.Single(x => x.BookingId == id);
                return BaseSupportPage.ProcessTasks(1, excursion.BookingId, excursion.ExcursionDate, excursion.ExcursionDate);
            }


        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

}

