﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
using Pace.Common;
using Pace.DataAccess.Finance;
using System.Text;
using Pace.DataAccess.Sage;
using System.Data.Entity.Core.Objects;

/// <summary>
/// Summary description for BaseFinancePage
/// </summary>
public partial class BaseFinancePage : BasePage
{
    public FinanceEntities _entities { get; set; }
    
    public BaseFinancePage()
	{
		
	}

    public static FinanceEntities CreateEntity
    {
        get
        {
            return new FinanceEntities();
        }
    }

    #region Private Transctions

    public static long PostTransaction(string reference, bool isManualAdjustment, byte? postingTypeId, long refundingTransactionId, byte paymentMethodId, string deviceIdentifier, decimal? amount)
    {
        try
        {
            using (FinanceEntities entity = new FinanceEntities())
            {
                ObjectResult<Transaction> _transction = entity.PostTransaction(reference, GetCurrentUser().UserId, isManualAdjustment, postingTypeId, refundingTransactionId, paymentMethodId, deviceIdentifier, amount);

                return _transction.First().TransactionId;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }


    #endregion



    #region Javascript Enabled Methods


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, object id)
    {
        try
        {
            var page = new BaseFinancePage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse RefundTransaction(RefundTransaction newObject)
    {
        try
        {
            Transaction newitem = new Transaction();

            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                using (FinanceEntities entity = new FinanceEntities())
                {

                    newitem = (from t in entity.Transactions.Include("PostingType").Include("Xlk_PaymentMethod").Include("Postings").Include("Postings.Account").Include("Postings.Account.AccountType")
                               where t.TransactionId == newObject.TransactionId
                               select t).FirstOrDefault();

                    if (newitem != null)
                    {
                        newitem.RefundingTransactionId = BaseFinancePage.PostTransaction(string.Format("Refunding : - {0}",newitem.Reference), false, newitem.PostingType.PostingTypeId, 0, newitem.Xlk_PaymentMethod.PaymentMethodId, GetComputerName(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]), newitem.Postings.Where(p => p.Account.AccountTypeId == 1).Select(t => t.PostingAmount).FirstOrDefault());

                        if (newObject.StudentOrderedItemId.HasValue && newObject.StudentOrderedItemId.Value > 0)
                        {
                            using (Pace.DataAccess.Enrollment.EnrollmentsEntities eEntity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
                            {
                                var _orderedItem = eEntity.OrderedItems.Where(i => i.OrderedItemId == newObject.StudentOrderedItemId.Value).FirstOrDefault();
                                if (_orderedItem != null)
                                {
                                    _orderedItem.RefundByTransactionId = newitem.RefundingTransactionId;
                                    if (eEntity.SaveChanges() == 0)
                                    {
                                        scope.Dispose();
                                        return new AjaxResponse(new Exception("Cannot save refunding transaction to Students ordered item"));
                                    }
                                }
                            }
                        }

                        if (newObject.GroupOrderedItemId.HasValue && newObject.GroupOrderedItemId.Value > 0)
                        {
                            using (Pace.DataAccess.Group.GroupEntities gEntity = new Pace.DataAccess.Group.GroupEntities())
                            {
                                var _orderedItem = gEntity.OrderedItems.Where(i => i.OrderedItemId == newObject.StudentOrderedItemId.Value).FirstOrDefault();
                                if (_orderedItem != null)
                                {
                                    _orderedItem.RefundByTransactionId = newitem.RefundingTransactionId;
                                    if (gEntity.SaveChanges() == 0)
                                    {
                                        scope.Dispose();
                                         return new AjaxResponse(new Exception("Cannot save refunding transaction to Group ordered item"));
                                    }
                                }
                            }
                        }

                        if (entity.SaveChanges() > 0)
                        {
                            scope.Complete();
                            return null;
                        }
                    }
                    scope.Dispose();
                    return new AjaxResponse(new Exception("Cannot find transaction to refund!"));
                } 
            }
        }
        catch (System.Transactions.TransactionAbortedException ex)
        {
            return new AjaxResponse(ex);
        }
        catch (ApplicationException ex)
        {
            return new AjaxResponse(ex);
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }



    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadDiscountHistory(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Reference</th><th scope=\"col\">Amount</th><th scope=\"col\">Date Created</th><th scope=\"col\">Action</th></tr></thead><tbody>");

            using (SageEntities entity = new SageEntities())
            {
                var discounts = from n in entity.InvoiceDiscounts
                               where n.Invoice.InvoiceNumber == id
                               select n;

                if (discounts.Count() > 0)
                {
                    foreach (InvoiceDiscount discount in discounts.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td></tr>", discount.Reference, discount.AmountDiscounted, discount.DiscountDate.ToString("dd/MM/yy"));

                    }
                }
                if (discounts.Count() == 0 && discounts.Count() == 0)
                {
                    sb.Append("<tr><td colspan=\"3\">No discounts to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadPaymentHistory(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Type</th><th scope=\"col\">Amount</th><th scope=\"col\">Date Created</th><th scope=\"col\">Action</th></tr></thead><tbody>");

            using (SageEntities entity = new SageEntities())
            {
                var receipts = from n in entity.Lnk_Receipt_Invoice.Include("Receipt")
                               where n.InvoiceNumber == id
                               select n;

                if (receipts.Count() > 0)
                {
                    foreach (Lnk_Receipt_Invoice pmy in receipts.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">Receipt</td><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td><a target=\"_blank\" href=\"{2}\" title=\"Print Receipt\"><img src=\"../Content/img/actions/printer2.png\" /></a></td></tr>", pmy.AmountPaid, pmy.Receipt.ReceiptDate,   ReportPath("FinanceReports","Receipt","HTML4.0", new Dictionary<string, object> { { "InvoiceNumber",  pmy.InvoiceNumber }, {"ReceiptNumber", pmy.ReceiptNumber} }));

                    }
                }
                var credits = from n in entity.Lnk_Credit_Invoice.Include("Credit")
                              where n.InvoiceNumber == id
                              select n;

                if (credits.Count() > 0)
                {
                    foreach (Lnk_Credit_Invoice credit in credits.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">Credit</td><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td></tr>", credit.AmountPaid, credit.Credit.InvoiceDate);

                    }
                }
                if (credits.Count() == 0 && receipts.Count() == 0)
                {
                    sb.Append("<tr><td colspan=\"3\">No payments to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    #endregion

}