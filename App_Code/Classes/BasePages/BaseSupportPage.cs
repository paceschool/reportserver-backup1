﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Support;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Text;
using Pace.Common;
using System.Data.Entity.Core;

/// <summary>
/// Summary description for BaseSupportPage
/// </summary>
public partial class BaseSupportPage : BasePage
{
    public SupportEntities _entities { get; set; }
    
    public BaseSupportPage()
	{
	}

    public static SupportEntities CreateEntity
    {
        get
        {
            return new SupportEntities();
        }
    }

    public static IList<Template> LoadTemplates()
    {
        List<Template> values = null;

        if (CacheManager.Instance.GetFromCache<List<Template>>("AllSupportTemplates", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Template>>("AllSupportTemplates", GetTemplates().ToList());
    }

    private static IList<Template> GetTemplates()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Template> lookupQuery =
                from p in entity.Templates
                select p;
            return lookupQuery.ToList();
        }
    }

    public static IList<Xlk_Priority> LoadPriority()
    {
        List<Xlk_Priority> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Priority>>("AllSupportPriority", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Priority>>("AllSupportPriority",GetPriority().ToList());
    }

    private static IList<Xlk_Priority> GetPriority()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Xlk_Priority> lookupQuery =
                from p in entity.Xlk_Priority
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_BusTicketType> LoadBusTicketTypes()
    {
        IList<Xlk_BusTicketType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_BusTicketType>>("AllBusTicketType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_BusTicketType>>("AllBusTicketType", GetBusTicketTypes());
    }

    private static IList<Xlk_BusTicketType> GetBusTicketTypes()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Xlk_BusTicketType> lookupQuery =
                from t in entity.Xlk_BusTicketType
                select t;
            return lookupQuery.ToList();
        }
    }
    public static IList<Xlk_Status> LoadStatus()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllSupportStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllSupportStatus", GetStatus().ToList());
    }

    private static IList<Xlk_Status> GetStatus()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from s in entity.Xlk_Status
                select s;
            return lookupQuery.ToList();
        }
    }

    public static IList<Xlk_TicketType> LoadTicketTypes()
    {
        List<Xlk_TicketType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_TicketType>>("AllTicketType", out values))
        return values;

        return CacheManager.Instance.AddToCache<List<Xlk_TicketType>>("AllTicketType", GetTicketTypes().ToList());
    }

    private static IList<Xlk_TicketType> GetTicketTypes()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Xlk_TicketType> lookupQuery =
                from t in entity.Xlk_TicketType
                select t;
            return lookupQuery.ToList();
        }
    }

    public static IList<Users> LoadUsers()
    {
        List<Users> values = null;

        if (CacheManager.Instance.GetFromCache<List<Users>>("AllSupportUsers", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Users>>("AllSupportUsers", GetUsers().ToList());
    }

    private static IList<Users> GetUsers()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Users> lookupQuery =
                from u in entity.Users
                select u;
            return lookupQuery.ToList();
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupTask(int id)
    {
        try
        {
            using (SupportEntities entity = new SupportEntities())
            {
                var task = (from s in entity.Task
                             where s.TaskId == id
                             select new { s.Ref, s.Description, AssignedTo= s.AssignedToUser,RaisedBy = s.RaisedByUser,s.Xlk_Status,s.Xlk_Priority }).SingleOrDefault();

                if (task != null)
                {
                    return string.Format("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>Ref</th><td>{0}</td></tr><tr><th>Description:</th><td>{1}</td></tr><tr><th>Raised By:</th><td>{2}</td></tr><tr><th>Assigned To:</th><td>{3}</td></tr><tr><th>Status:</th><td>{4}</td></tr><tr><th>Priority:</th><td>{5}</td></tr><tr id=\"release-notes\">	<th>Read the release notes:</th>	<td><a href=\"./releasenotes.html\" title=\"Read the release notes\">release notes</a></td></tr></tbody></table>", task.Ref, task.Description, task.RaisedBy.Name, task.AssignedTo.Name, task.Xlk_Status.Description, task.Xlk_Priority.Description);
                }

                return string.Empty;

            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupAssignments(int id)
    {
        StringBuilder sb = new StringBuilder("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>User</th><th>Date Assigned</th><th>Sequence</th></tr>");

        try
        {
            using (SupportEntities entity = new SupportEntities())
            {
                var assignments = (from s in entity.TaskAssignments
                             where s.TaskId == id
                             select new { s.TaskId,s.Sequence,s.AssignedDate,s.AssignedTo });

                if (assignments != null)
                {
                    foreach (var item in assignments.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2} / {3}</td><td>{4}</td></tr>", item.AssignedTo.Name, item.AssignedDate.ToString("dd MMM yy"), item.Sequence.ToString());
                    }
                }

                return string.Empty;
            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupTicket(int id)
    {
        try
        {
            using (SupportEntities entity = new SupportEntities())
            {
                //var agent = (from s in entity.Task
                //             where s.AgencyId == id
                //             select new { s.Name, s.SageRef, s.Address, s.ContactName, s.Email, Nationality = s.Xlk_Nationality.Description }).SingleOrDefault();

                //if (agent != null)
                //{
                //    return string.Format("<table class=\"popup-contents\"><tbody><tr><th>Name</th><td>{0}</td></tr><tr><th>Address:</th><td>{1}</td></tr><tr><th>SageRef:</th><td>{2}</td></tr><tr><th>Email:</th><td>{3}</td></tr><tr><th>Nationality:</th><td>{4}</td></tr><tr id=\"release-notes\">	<th>Read the release notes:</th>	<td><a href=\"./releasenotes.html\" title=\"Read the release notes\">release notes</a></td></tr></tbody></table>", agent.Name, agent.Address, agent.SageRef, agent.Email, agent.Nationality);
                //}

                return string.Empty;

            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, object> entitykeys)
    {
        try
        {
            var page = new BaseSupportPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveTemplateTask(TemplateTask newObject)
    {
        try
        {
            TemplateTask newtask = new TemplateTask();
            using (SupportEntities entity = new SupportEntities())
            {
                if (newObject.TemplateTaskId > 0)
                    newtask = (from c in entity.TemplateTasks.Include("AssignedToUser") where c.TemplateTaskId == newObject.TemplateTaskId select c).FirstOrDefault();

                newtask.Description = newObject.Description;
                newtask.ActionNeeded = newObject.ActionNeeded;
                newtask.Title = newObject.Title;
                newtask.Days = newObject.Days;
                newtask.Xlk_PriorityReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Priority", newObject.Xlk_Priority);
                
                if (newObject.Xlk_UseDate != null)
                    newtask.Xlk_UseDateReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_UseDate", newObject.Xlk_UseDate);

                if (newObject.Xlk_Status != null)
                    newtask.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);

                if (newObject.ParentTask != null && newObject.ParentTask.TemplateTaskId > 0)
                    newtask.ParentTaskReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".TemplateTasks", newObject.ParentTask);

                if (newObject.AssignedToUser != null)
                    newtask.AssignedToUserReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", newObject.AssignedToUser);

                if (newObject.Template != null)
                    newtask.TemplateReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Templates", newObject.Template);


                if (newtask.TemplateTaskId == 0)
                    entity.AddToTemplateTasks(newtask);

                entity.SaveChanges();



            }

            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveTask(Task newObject)
    {
        try
        {
            Task newtask = new Task();
            using (SupportEntities entity = new SupportEntities())
            {
                if (newObject.TaskId > 0)
                    newtask = (from c in entity.Task.Include("ParentTask").Include("ParentTask.Xlk_Status").Include("AssignedToUser") where c.TaskId == newObject.TaskId select c).FirstOrDefault();
                else
                {

                }

                if (newObject.Xlk_Status == null || newObject.Xlk_Status.StatusId != 3)
                {
                    newtask.RaisedDate = newObject.RaisedDate;
                    newtask.Description = newObject.Description;
                    newtask.ActionNeeded = newObject.ActionNeeded;
                    newtask.ActionTaken = newObject.ActionTaken;
                    newtask.DateDue = newObject.DateDue;
                    newtask.Ref = newObject.Ref;

                    newtask.Xlk_PriorityReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Priority", newObject.Xlk_Priority);
                    if (newObject.TaskId > 0 && newObject.Xlk_Status != null)
                        newtask.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);
                    else
                        newtask.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)1);

                    if (newObject.TaskId == 0)
                        newtask.RaisedByUserReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", GetCurrentUser());

                    if (newObject.ParentTask != null && newObject.ParentTask.TaskId > 0)
                        newtask.ParentTaskReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Task", newObject.ParentTask);

                    if (newtask.AssignedToUser != null && newtask.AssignedToUser.UserId != newObject.AssignedToUser.UserId)
                    {
                        TaskAssignments newAssignment = new TaskAssignments();
                        newAssignment.AssignedDate = DateTime.Now;
                        newAssignment.AssignedTo = newtask.AssignedToUser;
                        newAssignment.TaskId = newtask.TaskId;
                        newtask.TaskAssignments.Add(newAssignment);
                    }

                    if (newObject.AssignedToUser != null)
                        newtask.AssignedToUserReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", newObject.AssignedToUser);
                }
                else
                {
                    if (newtask.ParentTask != null && newtask.ParentTask.Xlk_Status.StatusId != 3)
                        return new AjaxResponse(new Exception("The parent task is still open"));

                    newtask.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)3);
                    newtask.ActionTaken = newObject.ActionTaken;
                    newtask.DateCompleted = DateTime.Now;
                }

                if (newtask.TaskId == 0)
                    entity.AddToTask(newtask);

                entity.SaveChanges();



            }

            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveSchedule(Scheduler newObject)
    {
        try
        {
            Scheduler newschedule = new Scheduler();
            using (SupportEntities entity = new SupportEntities())
            {
                if (newObject.ScheduleId > 0)
                    newschedule = (from c in entity.Schedulers.Include("AssignedToUser") where c.ScheduleId == newObject.ScheduleId select c).FirstOrDefault();

                newschedule.ActionNeeded = newObject.ActionNeeded;
                newschedule.AssignedToRoleName = newObject.AssignedToRoleName;
                newschedule.CycleDays = newObject.CycleDays;
                newschedule.Ref = newObject.Ref;

                newschedule.ScheduleDescription = newObject.ScheduleDescription;
                newschedule.ScheduleTitle = newObject.ScheduleTitle;
                newschedule.TaskDescription = newObject.TaskDescription;
                newschedule.TaskTitle = newObject.TaskTitle;

                newschedule.Xlk_PriorityReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Priority", newObject.Xlk_Priority);
                newschedule.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);
                newschedule.AssignedToUserReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", newObject.AssignedToUser);
                newschedule.CreatedByUserReference.EntityKey = new  EntityKey(entity.DefaultContainerName + ".Users","UserId", GetCurrentUser().UserId);
                newschedule.LastRunDate = newObject.LastDueDate.AddDays(-newObject.CycleDays) ;
                newschedule.LastDueDate = newObject.LastDueDate;

                if (newschedule.ScheduleId == 0)
                    entity.AddToSchedulers(newschedule);


                entity.SaveChanges();



            }

            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }
    

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadChildTasks(int id)
    {
        List<long> ids;
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewTaskPage.aspx','Support/AddTaskControl.ascx',{'ParentTaskId':" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Task</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Priority</th><th scope=\"col\">Status</th><th scope=\"col\">Description</th><th scope=\"col\">Action Needed</th><th scope=\"col\">Date Due</th><th scope=\"col\">Action Taken</th><th scope=\"col\">Date Completed</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (SupportEntities entity = new SupportEntities())
            {
                ids = entity.LoadAllChildTasks(id).Select(x => x.TaskId).ToList();

                var query = from n in entity.Task.Include("Xlk_Status").Include("Xlk_Priority").Include("RaisedByUser").Include("AssignedToUser")
                            where ids.Contains(n.TaskId)
                            select n;

                if (query.Count() > 0)
                {
                    foreach (Task item in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: center\">", item.TaskId, item.Xlk_Priority.Description, PopUpLink("TaskId", item.TaskId, "~/Support/ViewTaskPage.aspx", item.Xlk_Status.Description, "PopUpTask"), TrimTextA(item.Description, 20), TrimTextA(item.ActionNeeded,20), item.DateDue.ToString("dd MMM yyyy"), TrimTextA(item.ActionTaken,20),(item.DateCompleted.HasValue) ? item.DateCompleted.Value.ToString("dd MMM yyyy"): string.Empty);

                        if (item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Edit Task\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewTaskPage.aspx','Support/AddTaskControl.ascx',{{'TaskId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;", item.TaskId);

                        if (item.AssignedToUser.UserId == GetCurrentUser().UserId | item.RaisedByUser.UserId == GetCurrentUser().UserId)
                        sb.AppendFormat("<a title=\"Close Task\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewTaskPage.aspx','Support/AddTaskControl.ascx',{{ TaskId: {0}, Close: {0}}} )\"><img src=\"../Content/img/actions/user_accept.png\"></a>&nbsp;", item.TaskId);

                        if (item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Delete Item\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','Task',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>", item.TaskId);
                        
                        sb.Append("</td></tr>");
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"9\">No Items to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadTaskAssignments(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">User</th><th scope=\"col\">Date Assigned</th><th scope=\"col\">Sequence</th></tr></thead><tbody>");
            using (SupportEntities entity = new SupportEntities())
            {
                var query = from  s in entity.TaskAssignments
                            where s.TaskId == id
                            select new { s.TaskId,s.Sequence,s.AssignedDate,s.AssignedTo };

                if (query.Count() > 0)
                {
                    foreach (var item in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2} / {3}</td><td>{4}</td></tr>", item.AssignedTo.Name, item.AssignedDate.ToString("dd MMM yy"), item.Sequence.ToString());
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Items to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool ProcessTasks(short templateid,int localid, DateTime? startdate, DateTime? enddate) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        try
        {
            using (SupportEntities entity = new SupportEntities())
            {
                string result = entity.CreateTasksFromTemplate(templateid, localid, startdate, enddate).First();

               return !string.IsNullOrEmpty(result);
            }
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    
}
