﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Supplier;
using Pace.Common;


/// <summary>
/// Summary description for BaseDocumentPage
/// </summary>
public partial class BaseSupplierPage : BasePage
{
    public SupplierEntities _entities { get; set; }

    public BaseSupplierPage()
    {
    }

    public static SupplierEntities Entities
    {
        get
        {  
            //SupplierEntities value;

            //if (CacheManager.Instance.GetFromCache<SupplierEntities>("PaymentsEntitiesObject", out value))
            //    return value;

            //return CacheManager.Instance.AddToCache<SupplierEntities>("PaymentsEntitiesObject", new SupplierEntities());
            return new SupplierEntities();
        }
    }


    public IList<Xlk_PaymentMethod> LoadPaymentMethods()
    {
        IList<Xlk_PaymentMethod> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_PaymentMethod>>("All_PaymentMethod", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_PaymentMethod>>("All_PaymentMethod", GetPaymentMethods());
    }

    public IList<Xlk_SupplierType> LoadSupplierTypes()
    {
        IList<Xlk_SupplierType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_SupplierType>>("All_SupplierType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_SupplierType>>("All_SupplierType", GetSupplierTypes());
    }

    public IList<Supplier> LoadSuppliers()
    {
        IList<Supplier> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Supplier>>("All_Supplier", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Supplier>>("All_Supplier", GetSupplier());
    }

    public IList<Supplier> LoadSuppliers(int SupplierTypeId)
    {
        IEnumerable<Supplier> values = from t in LoadSuppliers()
                                       where t.Xlk_SupplierType.SupplierTypeId == SupplierTypeId
                                       select t;

        return values.ToList();
    }

    public Supplier LoadSupplier(int SupplierId)
    {
        IEnumerable<Supplier> value = from t in LoadSuppliers()
                        where t.SupplierId == SupplierId
                        select t;

        return value.First();
    }

    #region Private Methods

    private IList<Supplier> GetSupplier()
    {
        using (Pace.DataAccess.Supplier.SupplierEntities entity = new SupplierEntities())
        {
            IQueryable<Supplier> lookupQuery =
                 from p in entity.Suppliers.Include("Xlk_SupplierType")
                 select p;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_SupplierType> GetSupplierTypes()
    {
        using (Pace.DataAccess.Supplier.SupplierEntities entity = new SupplierEntities())
        {
            IQueryable<Xlk_SupplierType> lookupQuery =
                 from p in entity.Xlk_SupplierType
                 select p;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_PaymentMethod> GetPaymentMethods()
    {
        using (Pace.DataAccess.Supplier.SupplierEntities entity = new SupplierEntities())
        {
            IQueryable<Xlk_PaymentMethod> lookupQuery =
                 from p in entity.Xlk_PaymentMethod
                 select p;
            return lookupQuery.ToList();
        }
    }

    #endregion
}

