﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Contacts;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
using Pace.Common;


/// <summary>
/// Summary description for BaseContactPage
/// </summary>
public partial class BaseContactPage : BasePage
{
    public ContactsEntities _entities { get; set; }

    public BaseContactPage()
    {
    }


    public static ContactsEntities CreateEntity
    {
        get
        {
            return new ContactsEntities();
        }
    }

    public static IList<Pace.DataAccess.Contacts.Xlk_Category> LoadCategories()
    {
        IList<Pace.DataAccess.Contacts.Xlk_Category> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Contacts.Xlk_Category>>("All_ContactsCategory", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Contacts.Xlk_Category>>("All_ContactsCategory", GetCategories());
    }

    public static IList<Pace.DataAccess.Contacts.Xlk_Type> LoadTypes()
    {
        IList<Pace.DataAccess.Contacts.Xlk_Type> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Contacts.Xlk_Type>>("All_ContactsType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Contacts.Xlk_Type>>("All_ContactsType", GetTypes());
    }

    public IList<Xlk_Type> LoadTypes(int categoryId)
    {
        IEnumerable<Xlk_Type> values = from t in LoadTypes()
                                       where t.Xlk_Category.CategoryId == categoryId
                                       select t;

        return values.ToList();
    }

    #region Private Methods

    private static IList<Xlk_Type> GetTypes()
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Xlk_Type> lookupQuery =
                 from p in entity.Xlk_Type.Include("Xlk_Category")
                 select p;
            return lookupQuery.ToList();
        }
    }

    private static IList<Xlk_Category> GetCategories()
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Xlk_Category> lookupQuery =
                 from p in entity.Xlk_Category
                 select p;
            return lookupQuery.ToList();
        }
    }

    protected string TRClass(object statusId)
    {
        if (statusId != null && statusId.ToString() == "2")
            return "disabled";

        return string.Empty;
    }

    protected string CreateName(object firstName, object secondName)
    {

        if (firstName != null && secondName != null)
        {
            return string.Format("{0} {1}", firstName, secondName);
        }

        else
        {
            return (firstName != null) ? firstName.ToString() : secondName.ToString();
        }
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseContactPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("ContactId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, object> entitykeys)
    {
        try
        {
            var page = new BaseContactPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveContact(ContactDetails  newObject)
    {
        ContactDetails newContactDetails = new ContactDetails();

        try
        {
            using (ContactsEntities entity = new ContactsEntities())
            {
                if (newObject.ContactId > 0)
                    newContactDetails = (from e in entity.ContactDetails where e.ContactId == newObject.ContactId select e).FirstOrDefault();

                newContactDetails.CompanyName = newObject.CompanyName;
                newContactDetails.ContactAddress = newObject.ContactAddress;
                newContactDetails.ContactEmail = newObject.ContactEmail;
                newContactDetails.ContactWebsite = newObject.ContactWebsite;
                newContactDetails.FaxNumber = newObject.FaxNumber;
                newContactDetails.FirstName = newObject.FirstName;
                newContactDetails.MobileNumber = newObject.MobileNumber;
                newContactDetails.PhoneNumber = newObject.PhoneNumber;
                newContactDetails.SurName = newObject.SurName;
                newContactDetails.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);
                newContactDetails.Xlk_TypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Type", newObject.Xlk_Type);

                if (newContactDetails.ContactId == 0)
                    entity.AddToContactDetails(newContactDetails);

                entity.SaveChanges();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    #endregion
}



