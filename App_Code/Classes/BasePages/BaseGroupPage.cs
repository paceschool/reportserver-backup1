﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using Pace.DataAccess.Group;
using Pace.DataAccess.Excursion;
using Accommodation = Pace.DataAccess.Accommodation;
using Enrollments = Pace.DataAccess.Enrollment;
using Tuition = Pace.DataAccess.Tuition;
using System.ServiceModel;
using Pace.Common;
using System.Transactions;
using System.Web.Script.Serialization;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core;

/// <summary>
/// Summary description for BaseEnrollmentPage
/// </summary>
public partial class BaseGroupPage : BasePage
{
    public GroupEntities _entities { get; set; }

    public BaseGroupPage()
    {
    }

    #region Events

    public static GroupEntities CreateEntity
    {
        get
        {
            return new GroupEntities();
        }
    }

    public IList<Xlk_Nationality> LoadNationalities()
    {
        IList<Xlk_Nationality> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Nationality>>("AllGroupNationalities", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Nationality>>("AllGroupNationalities", GetNationalities());
    }

    public IList<Xlk_LessonBlock> LoadLessonBlocks()
    {
        IList<Xlk_LessonBlock> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_LessonBlock>>("AllGroupLessonBlocks", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_LessonBlock>>("AllGroupLessonBlocks", GetLessonBlocks());
    }

    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        IList<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_ProgrammeType>>("AllGroupProgrammeTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_ProgrammeType>>("AllGroupProgrammeTypes", GetProgrammeTypes());
    }

    public IList<Agency> LoadAgents()
    {
        return GetAgents();
    }

    public IList<Pace.DataAccess.Group.Xlk_Status> LoadStatus()
    {
        IList<Pace.DataAccess.Group.Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Group.Xlk_Status>>("AllGroupStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Group.Xlk_Status>>("AllGroupStatus", GetStatus());
    }

    public IList<Pace.DataAccess.Accommodation.Xlk_Status> LoadFamilyStatus()
    {
        IList<Pace.DataAccess.Accommodation.Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Accommodation.Xlk_Status>>("AllFamilyStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Accommodation.Xlk_Status>>("AllFamilyStatus", GetFamilyStatus());
    }
    public IList<Pace.DataAccess.Accommodation.Xlk_FamilyType> LoadFamilyType()
    {
        IList<Pace.DataAccess.Accommodation.Xlk_FamilyType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Accommodation.Xlk_FamilyType>>("AllFamilyType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Accommodation.Xlk_FamilyType>>("AllFamilyType", GetFamilyType());
    }
    public IList<Pace.DataAccess.Accommodation.Xlk_RoomType> LoadRoomList()
    {
        IList<Pace.DataAccess.Accommodation.Xlk_RoomType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Accommodation.Xlk_RoomType>>("AllRoomType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Accommodation.Xlk_RoomType>>("AllRoomType", GetRoomList());
    }
    #endregion


    #region Private Methods

    private IList<Xlk_Nationality> GetNationalities()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_Nationality> lookupQuery =
                from a in entity.Xlk_Nationality
                select a;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_LessonBlock> GetLessonBlocks()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_LessonBlock> lookupQuery =
                from a in entity.Xlk_LessonBlock.Include("Xlk_CourseType")
                select a;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_ProgrammeType> lookupQuery =
                from b in entity.Xlk_ProgrammeType
                select b;
            return lookupQuery.ToList();
        }
    }


    private IList<Pace.DataAccess.Group.Xlk_Status> GetStatus()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Pace.DataAccess.Group.Xlk_Status> lookupQuery =
                from c in entity.Xlk_Status
                select c;
            return lookupQuery.ToList();
        }
    }

    private IList<Pace.DataAccess.Accommodation.Xlk_Status> GetFamilyStatus()
    {
        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
        {
            IQueryable<Pace.DataAccess.Accommodation.Xlk_Status> lookupQuery =
                from f in entity.Xlk_Status
                select f;
            return lookupQuery.ToList();
        }
    }

    private IList<Pace.DataAccess.Accommodation.Xlk_FamilyType> GetFamilyType()
    {
        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
        {
            IQueryable<Pace.DataAccess.Accommodation.Xlk_FamilyType> lookupQuery =
                from f in entity.Xlk_FamilyType
                select f;
            return lookupQuery.ToList();
        }
    }


    private IList<Pace.DataAccess.Accommodation.Xlk_RoomType> GetRoomList()
    {
        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {
            IQueryable<Pace.DataAccess.Accommodation.Xlk_RoomType> lookupQuery =
                from r in entity.Xlk_RoomType
                select r;
            return lookupQuery.ToList();
        }
    }

    private IList<Agency> GetAgents()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Agency> lookupQuery =
                from d in entity.Agency
                orderby d.Name
                select d;
            return lookupQuery.ToList();
        }
    }

    protected static Group GetGroup(int groupId)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Group> groupQuery = from g in entity.Group.Include("Agency").Include("Xlk_Status").Include("Xlk_Nationality")
                                           where g.GroupId == groupId
                                           select g;

            if (groupQuery.Count() > 0)
            {
                return groupQuery.First();

            }

        }
        return null;
    }

    #endregion



    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveInvoiceReceipt(InvoiceReceipt newObject)
    {
        try
        {
            InvoiceReceipt newinvoicereceipt = new InvoiceReceipt();
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                using (GroupEntities entity = new GroupEntities())
                {

                    byte? postingTypeId = 8; //posting type for invoices

                    if (newObject.InvoiceReceiptId > 0)
                        newinvoicereceipt = (from c in entity.InvoiceReceipts where c.InvoiceReceiptId == newObject.InvoiceReceiptId select c).FirstOrDefault();
                    else
                    {
                        newinvoicereceipt.DateRecorded = DateTime.Now;
                    }

                    newinvoicereceipt.AmountPaid = newObject.AmountPaid;
                    newinvoicereceipt.Comment = newObject.Comment;
                    newinvoicereceipt.SageInvoiceRef = newObject.SageInvoiceRef;
                    newinvoicereceipt.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                    newinvoicereceipt.Xlk_PaymentMethodReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_PaymentMethod", newObject.Xlk_PaymentMethod);


                    if (newinvoicereceipt.InvoiceReceiptId == 0)
                    {
                        entity.AddToInvoiceReceipts(newinvoicereceipt);

                        if (entity.SaveChanges() > 0)
                        {
                            newinvoicereceipt.PaidByTransactionId = BaseFinancePage.PostTransaction(string.Format("InvoiceReceipt:{0}", newinvoicereceipt.InvoiceReceiptId), false, postingTypeId, 0, newObject.Xlk_PaymentMethod.PaymentMethodId, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], newObject.AmountPaid);

                            if (entity.SaveChanges() > 0)
                                scope.Complete();

                            return new AjaxResponse();
                        }
                    }
                    else
                    {
                        if (entity.SaveChanges() > 0)
                            scope.Complete();

                        return new AjaxResponse();
                    }
                }
                scope.Dispose();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetGroupedStudents(Int32 id)
    {
        StringBuilder sb = new StringBuilder("<table cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;\" id=\"ctl00_results\">");
        try
        {
            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                var students = (from g in entity.Student
                                where g.GroupId == id && (g.GroupingBlockId.HasValue)
                                group g by g.GroupingBlockId into groups
                                select new { Key = groups.Key, Students = groups }); //Select the correct group students

                if (students != null && students.ToList().Count > 0)
                {
                    StringBuilder ss = new StringBuilder();
                    int counter = 0;

                    foreach (var record in students)
                    {

                        ss.Append("<td class=\"classtd\"><ul style=\"float: left; list-style-type: none;\" class=\"classlist\">");
                        ss.AppendFormat("<div class=\"droppable\" id=\"{0}\"><li style=\"text-align:center; width:200px;\">Student Grouping  <a href=\"#\" onclick=\"deleteStudentGrouping({0}, {1})\"><img src=\"../Content/img/actions/delete.png\" alt=\"Delete Grouping\" style=\"float:right\" id=\"DeleteGrouping\"/></a>No.{0}</li><li>", record.Key, id);

                        foreach (Pace.DataAccess.Enrollment.Student student in record.Students)
                        {
                            ss.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}</div>", CreateName(student.FirstName, student.SurName), student.StudentId);
                        }

                        ss.Append("</li></div></ul></td>");
                        counter++;

                        if (counter == 3)
                        {
                            counter = 0;
                            sb.AppendFormat("<tr>{0}</tr>", ss.ToString());
                            ss = new StringBuilder();
                        }
                    }

                    if ( ss.Length > 0)
                        sb.AppendFormat("<tr>{0}</tr>", ss.ToString());
                }
                return new AjaxResponse<string>(sb.Append("</table>").ToString());

            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetGroupStudents(Int32 id)
    {
        StringBuilder sb = new StringBuilder();

        try
        {
            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                var students = (from g in entity.Student
                                where g.GroupId == id && (!g.GroupingBlockId.HasValue)
                                select g); //Select the correct group students

                if (students != null && students.ToList().Count > 0)
                {
                    foreach (Pace.DataAccess.Enrollment.Student student in students)
                    {
                        sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}</div>", CreateName(student.FirstName, student.SurName), student.StudentId);
                    }
                }
                else
                    return new AjaxResponse<string>("<div></div>");

                return new AjaxResponse<string>(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupGroupedStudents(int id)
    {
        try
        {
            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                int[,] sharing = new int[50,3]; //Number of sharings by gender
                int msing = 0; int mdoub = 0; int mtrip = 0; //Breakdown of male sharing types
                int fsing = 0; int fdoub = 0; int ftrip = 0; //Breakdown of female sharing types
                Int32 groupings = 0;

                var lookupQuery = from s in entity.Student
                                  where s.GroupId == id
                                  select s; //Get students list tied to this group
                groupings = Convert.ToInt32(lookupQuery.Max(x => x.GroupingBlockId)); //Get the last GroupingBlockId in this group

                for (var j = 1; j < 3; j++) //Loop through the genders
                {
                    for (var i = 1; i < groupings + 1; i++) //Loop through each GroupingBlockId
                    {
                        foreach (Pace.DataAccess.Enrollment.Student item in lookupQuery.Where(x => x.Xlk_Gender.GenderId == j)) //Look for students with current GroupingBlockId
                        {
                            if (item.GroupingBlockId == i)
                                sharing[i,j] = sharing[i,j] + 1;
                        }
                    }
                }
                    for (var a = 1; a < groupings + 1; a++)
                    {
                        if (sharing[a,1] == 1)
                            msing = msing + 1; //Add up the Male Singles
                        if (sharing[a, 2] == 1)
                            fsing = fsing + 1; //Add up the Female Singles
                        if (sharing[a,1] == 2)
                            mdoub = mdoub + 1; //Add up the Male Doubles
                        if (sharing[a, 2] == 2)
                            fdoub = fdoub + 1; //Add up the Female Doubles
                        if (sharing[a,1] == 3)
                            mtrip = mtrip + 1; //Add up the Male Triples
                        if (sharing[a, 2] == 3)
                            ftrip = ftrip + 1; //Add up the Female Triples
                    }

                if (lookupQuery != null)
                {
                    return string.Format("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>Breakdown of sharing requirements</th></tr><tr><td>.</td></tr><tr><th>Male Singles: </th><td>{0}</td></tr><tr><th>Male Doubles: </th><td>{1}</td></tr><tr><th>Male Triples: </th><td>{2}</td></tr><tr><td>.</td></tr><tr><th>Female Singles:</th><td>{4}</td></tr><tr><th>Female Doubles: </th><td>{5}</td></tr><tr><th>Female Triples: </th><td>{6}</td></tr><tr><td>.</td></tr><tr><th>Total shares: </th><td>{3}</td></tr></tbody></table>", msing, mdoub, mtrip, groupings, fsing, fdoub, ftrip);
                }
                return string.Empty;
            }
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupRooms(int id)
    {
        try
        {
            using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                var sbeds = 0; var dbeds = 0; var cantake = 0;
                var rooms = (from r in entity.Room
                            where r.Family.FamilyId == id
                            select r);

                foreach (Accommodation.Room item in rooms)
                {
                    if (item.NumberofSingleBeds == 1)
                        sbeds++;
                    if (item.NumberofDoubleBeds == 1)
                        dbeds++;
                }

                cantake = sbeds + (dbeds * 2);

                if (rooms != null && (sbeds != 0 || dbeds != 0))
                {
                    return string.Format("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>Single Rooms: </th><td>{0}</td></tr><tr><th>Double Rooms: </th><td>{1}</td></tr><tr><th>Total Availability:</th><td>{2}</td></tr></tbody></table>", sbeds, dbeds, cantake);
                }
                else
                {
                    return string.Format("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>No Rooms Data</th></tr></tbody></table>");
                }

                //return string.Empty;
            }
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupOrderedItem(int id)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                var item = (from i in entity.OrderedItems
                            where i.OrderedItemId == id
                            select new { i.OrderableItem.Title, i.Qty, i.RRPrice, i.AmountPaid, i.DatePurchased, i.Discount, i.ReferenceFrom, ProductType = i.OrderableItem.Xlk_ProductType.Description, Unit = i.Xlk_Unit.Description, PaymentMethod = i.Xlk_PaymentMethod.Description }).SingleOrDefault();

                if (item != null)
                {

                    return string.Format("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>Product</th><td>{0}</td></tr><tr><th>Type</th><td>{1}</td></tr><tr><th>RRP Price</th><td>{2}</td></tr><tr><th>Qty</th><td>{3}</td></tr><tr><th>Discount</th><td>{4}</td></tr><tr><th>AmountPaid</th><td>{5}</td></tr><tr><th>Payment Method</th><td>{6}</td></tr><tr><th>Reference</th><td>{7}</td></tr><tr><th>Date Purchased</th><td>{8}</td></tr></tbody></table>", item.Title, item.ProductType, item.RRPrice, item.Qty, item.Discount, item.AmountPaid, item.PaymentMethod, item.ReferenceFrom, item.DatePurchased.Value.ToString("dd MMM yy"));
                }

                return string.Empty;
            }


        }
        catch (Exception)
        {
            return string.Empty;
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopUpGroup(int id)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                var _group = (from s in entity.Group
                              where s.GroupId == id
                              select new { s.GroupName, s.GroupId, Agency = s.Agency.Name, AgencyEmail = s.Agency.Email, s.GroupCurfew, s.LeaderMobile, s.ArrivalInfo, s.DepartureInfo, s.NoOfStudents, s.NoOfLeaders, s.ArrivalDate, s.DepartureDate, Nationality = s.Xlk_Nationality.Description, Status = s.Xlk_Status.Description }).SingleOrDefault();

                if (_group != null)
                {

                    StringBuilder sb = new StringBuilder();

                    sb.AppendFormat("<table  id=\"matrix-table-a\" class=\"popup-contents\"><tbody><tr><th>GroupId</th><td>{0}</td></tr><tr><th>Name</th><td>{1}</td></tr><tr><th>Agency</th><td>{2}</td></tr><tr><th>No of Students/Leaders:</th><td>{3}/{4}</td></tr><tr><th>Arrival:</th><td>{5}</td></tr><tr><th>Departure:</th><td>{6}</td></tr><tr><th>Status</th><td>{7}</td></tr><tr><th>Nationality</th><td>{8}</td></tr><tr><th>Agency Email</th><td><a href=\"MailTo:{9}\">{9}</a></td></tr><tr><th>Curfew</th><td>{10}</td></tr><tr><th>Leader's Mobile</th><td>{11}</td></tr><tr><th>Arrival Info</th><td>{12}</td></tr><tr><th>Departure Info</th><td>{13}</td></tr></tbody></table>", _group.GroupId, _group.GroupName, _group.Agency, _group.NoOfStudents, _group.NoOfLeaders, _group.ArrivalDate.ToString("dd/MMM/yyyy"), _group.DepartureDate.ToString("dd/MMM/yyyy"), _group.Status, _group.Nationality, _group.AgencyEmail, _group.GroupCurfew, _group.LeaderMobile, _group.ArrivalInfo, _group.DepartureInfo);

                    var _groupinvoices = (from gi in entity.GroupInvoice.Include("Xlk_SageDataType")
                                  where gi.Group.GroupId == id
                                  select gi);


                    if (_groupinvoices.Count() > 0)
                    {
                        sb.Append("<table  id=\"box-table-a\"><thead><tr><th>Type</th><th>Invoice No</th><th>Date Raised</th><th>Net Amount</th></tr></thead><tbody>");
                        int[] typeids = new[] { 1, 2, 3 };

                        if (_groupinvoices.Any(u => typeids.Contains(u.Xlk_SageDataType.SageDataTypeId)))
                        {
                            using (Pace.DataAccess.Sage.SageEntities sentity = new Pace.DataAccess.Sage.SageEntities())
                            {

                                var invoices = (from i in sentity.Invoices
                                                join s in _groupinvoices.Where(p => typeids.Contains(p.Xlk_SageDataType.SageDataTypeId)).Select(x => x.SageInvoiceRef).ToList() on i.InvoiceNumber equals s
                                                select new { i.InvoiceNumber, i.InvoiceDate, i.NetAmount, Type = i.Xlk_InvoiceType.Description });

                                foreach (var invoice in invoices)
                                {
                                    sb.AppendFormat("<tr><td>{3}</td><td>{0}</td><td>{1}</td><td>{2}</td></tr>", invoice.InvoiceNumber, invoice.InvoiceDate.Value.ToString("dd/MMM/yy"), invoice.NetAmount, invoice.Type);
                                }
                            }
                        } 
                        
                        typeids = new[] { 4 };
                        if (_groupinvoices.Any(u => typeids.Contains(u.Xlk_SageDataType.SageDataTypeId)))
                        {
                            using (Pace.DataAccess.Agent.AgentEntities aentity = new Pace.DataAccess.Agent.AgentEntities())
                            {

                                var invoices = (from i in aentity.Quotes
                                                join s in _groupinvoices.Where(p => typeids.Contains(p.Xlk_SageDataType.SageDataTypeId)).Select(x => x.SageInvoiceRef).ToList() on i.QuoteId equals s
                                                select new { i.QuoteId, i.DateCreated, NetAmount = i.QuoteItems.Sum(p => p.LineNetPrice), Type = "Quote" });

                                foreach (var invoice in invoices)
                                {
                                    sb.AppendFormat("<tr><td>{3}</td><td>{0}</td><td>{1}</td><td>{2}</td></tr>", invoice.QuoteId, invoice.DateCreated.ToString("dd/MMM/yy"), invoice.NetAmount, invoice.Type);
                                }
                            }
                        }

                        sb.Append("</tbody></table>");
                    }

                    return sb.ToString();
                }

                return string.Empty;

            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadOrderedItems(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupOrderedItemControl.ascx',{ GroupId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Item</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Type</th><th scope=\"col\">Description</th><th scope=\"col\">Qty</th><th scope=\"col\">By</th><th scope=\"col\">Comment</th><th scope=\"col\">Special Instructions</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (GroupEntities entity = new GroupEntities())
            {
                var query = from n in entity.OrderedItems.Include("Xlk_Unit").Include("OrderableItem")
                            where n.Group.GroupId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (OrderedItem item in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: center\"><a title=\"Edit Note\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupOrderedItemControl.ascx',{{'OrderedItemId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Item\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupOrderedItem',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td> </tr>", item.OrderedItemId, item.OrderableItem.Title, item.Description, item.Qty, item.Xlk_Unit.Description, item.Comment, item.SpecialInstructions);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Items to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroupOrderedItem(OrderedItem newObject)
    {
        try
        {
            OrderedItem newitem = new OrderedItem();
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                using (GroupEntities entity = new GroupEntities())
                {

                    byte? postingTypeId = (from p in entity.OrderableItems
                                           where p.ItemId == newObject.OrderableItem.ItemId
                                           select p.Xlk_ProductType.PostingTypeId).FirstOrDefault();


                    if (!postingTypeId.HasValue)
                        return new AjaxResponse(new Exception("No associated posting type conntect with this product, maybe this should be processed against an invoice"));

                    if (newObject.OrderedItemId > 0)
                        newitem = (from c in entity.OrderedItems where c.OrderedItemId == newObject.OrderedItemId select c).FirstOrDefault();


                    newitem.Comment = newObject.Comment;
                    newitem.Description = newObject.Description;
                    newitem.Qty = newObject.Qty;
                    newitem.SpecialInstructions = newObject.SpecialInstructions;
                    newitem.AmountPaid = newObject.AmountPaid;
                    newitem.DatePurchased = DateTime.Now;
                    newitem.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                    newitem.Xlk_UnitReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Unit", newObject.Xlk_Unit);
                    newitem.OrderableItemReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".OrderableItems", newObject.OrderableItem);
                    newitem.Xlk_PaymentMethodReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_PaymentMethod", newObject.Xlk_PaymentMethod);

                    if (newitem.OrderedItemId == 0)
                        entity.AddToOrderedItems(newitem);

                    if (entity.SaveChanges() > 0)
                    {
                        newitem.PaidByTransactionId = BaseFinancePage.PostTransaction(string.Format("GroupOrderedItemId:{0}", newitem.OrderedItemId), false, postingTypeId, 0, newObject.Xlk_PaymentMethod.PaymentMethodId, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], newObject.AmountPaid);

                        if (entity.SaveChanges() > 0)
                            scope.Complete();

                        return new AjaxResponse();
                    }

                }
                scope.Dispose();
            }
            return new AjaxResponse(new Exception("There was a problem"));
        }
        catch (TransactionAbortedException ex)
        {
            return new AjaxResponse(ex);
        }
        catch (ApplicationException ex)
        {
            return new AjaxResponse(ex);
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupEnrollments(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupEnrollmentControl.ascx',{ GroupId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Enrollment</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Enrollment Id</th><th scope=\"col\">Course Start Date</th><th scope=\"col\">Course Finish Date</th><th scope=\"col\">#Weeks</th><th scope=\"col\">Programme Type</th><th scope=\"col\">Course Type</th><th scope=\"col\">Lesson Block</th><th scope=\"col\">Tuition Days</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (GroupEntities entity = new GroupEntities())
            {
                var query = from n in entity.Enrollment.Include("Xlk_ProgrammeType").Include("Xlk_LessonBlock").Include("Xlk_CourseType").Include("Group")
                            where n.Group.GroupId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (Pace.DataAccess.Group.Enrollment enrollment in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{8}</td> <td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{9} {10} {11}</td><td style=\"text-align: center\"><a title=\"Edit Enrollment\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupEnrollmentControl.ascx',{{'EnrollmentId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Enrollment\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupEnrollment',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>&nbsp;<a href=\"{7}\" title=\"Print Group Certificates\"><img src=\"../Content/img/actions/printer.png\" /></a>&nbsp;<a href=\"{7}?/PaceManagerReports/GroupEOCAppraisalNew&GroupId={6}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\" title=\"Print Group Appraisals\"><img src=\"../Content/img/actions/printer2.png\" /></a></td></tr>", enrollment.EnrollmentId, enrollment.StartDate.ToString("D"), (enrollment.EndDate.HasValue) ? enrollment.EndDate.Value.ToString("D") : String.Empty, CalculateTimePeriod(enrollment.StartDate, enrollment.EndDate, 'W'), enrollment.Xlk_ProgrammeType.Description, enrollment.Xlk_LessonBlock.Description, id, ReportPath("GroupReports", "TestGroupCert", "HTML4.0", new Dictionary<string, object> { { "GroupId", id } }), enrollment.Xlk_CourseType.Description, (enrollment.MorningLessonDays.HasValue && enrollment.MorningLessonDays.Value > 0) ? BasePage.TuitionDaysLink(enrollment.MorningLessonDays, "~/Groups/ViewGroupPage.aspx", "Morning") : String.Empty, (enrollment.AfternoonLessonDays.HasValue && enrollment.AfternoonLessonDays.Value > 0) ? BasePage.TuitionDaysLink(enrollment.AfternoonLessonDays, "~/Groups/ViewGroupPage.aspx", "Afternoon") : String.Empty, (enrollment.EveningLessonDays.HasValue && enrollment.EveningLessonDays.Value > 0) ? BasePage.TuitionDaysLink(enrollment.EveningLessonDays, "~/Groups/ViewGroupPage.aspx", "Evening") : string.Empty, ReportPath("EnrollmentReports", "TestGroupCert", "HTML4.0", new Dictionary<string, object> { { "StudentId", id } }));
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"9\">No Enrollments to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupExcursions(int id)
    {
        List<GroupExcursion> groupquery;
        List<Lnk_GroupExcursion_Booking> bookingquery;

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}\"><img src=\"../Content/img/actions/printer.png\" />Print Timetable</a>&nbsp;<a href=\"{2}\" title=\"Print Group Excursion List\"><img src=\"../Content/img/actions/printer.png\" title=\"Print Group Excursion List\" />Print Group Excursion List</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupExcursionControl.ascx',{{ GroupId:{0}}})\"><img src=\"../Content/img/actions/add.png\" />Add Excursion</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Group Excursion Id</th><th scope=\"col\">Excursion</th><th scope=\"col\">Preferred Date</th><th scope=\"col\">Preferred Time</th><th scope=\"col\">Qty Leaders/Students/Guides</th><th scope=\"col\">Private Transport?</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, ReportPath("GroupReports", "GroupExcursion", "PDF", new Dictionary<string, object> { { "GroupId", id } }), ReportPath("GroupReports", "GroupExcursionList", "PDF", new Dictionary<string, object> { { "GroupId", id } })));
           
            using (GroupEntities entity = new GroupEntities())
            {
                groupquery = (from e in entity.GroupExcursion.Include("Excursion")
                            where e.Group.GroupId == id
                            orderby e.PreferredDate, e.PreferredTime
                            select e).ToList();

                var ids = groupquery.Select(x => x.GroupExcursionId).ToList();


                using (Pace.DataAccess.Excursion.ExcursionEntities eentity = new ExcursionEntities())
                {
                    bookingquery = (from e in eentity.Lnk_GroupExcursion_Booking.Include("Booking")
                                    where e.Booking.Xlk_Status.StatusId != 4 && ids.Contains(e.GroupExcursionId)
                                    select e).ToList();
                }

                if (groupquery.Count() > 0)
                {
                    foreach (var group in (from g in groupquery
                                           select new { GroupExcursion = g, Booking = bookingquery.Where(x=>x.GroupExcursionId==g.GroupExcursionId)}).ToList())
                    {
                        if (group.Booking.Count() > 0)
                        {
                            foreach (Lnk_GroupExcursion_Booking booking in group.Booking)
                            {
                                sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\"><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4} / {5} / {6}</td><td style=\"text-align: left; font-size:large\"><img src=\"{9}\" /></td><td style=\"text-align: left; font-size:large\"><a href=\"#\" class=\"information\">{7}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Edit Excursion\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Excursions/AddBookingControl.ascx',{{'BookingId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Excursion\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewGroupPage.aspx','ClosedGroupExcursionBooking', {0},{10})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", booking.Booking.BookingId, booking.Booking.Title, booking.Booking.ExcursionDate.ToString("D"), booking.Booking.ExcursionTime, booking.Booking.QtyStudents, booking.Booking.QtyLeaders, booking.Booking.QtyGuides, TrimText(group.GroupExcursion.Comment, 20), group.GroupExcursion.Comment, (group.GroupExcursion.TransportRequired.HasValue && group.GroupExcursion.TransportRequired.Value) ? "../Content/img/actions/accept.png" : "../Content/img/actions/delete.png", booking.GroupExcursionId);
                            }
                        }
                        else

                            sb.AppendFormat("<tr class=\"{10}\"><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4} / {5} / {6}</td><td style=\"text-align: left; font-size:large\"><img src=\"{9}\" /></td><td style=\"text-align: left; font-size:large\"><a href=\"#\" class=\"information\">{7}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Edit Excursion\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupExcursionControl.ascx',{{'GroupExcursionId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Excursion\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupExcursion', {0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", group.GroupExcursion.GroupExcursionId, group.GroupExcursion.Excursion.Title, group.GroupExcursion.PreferredDate.ToString("D"), group.GroupExcursion.PreferredTime, group.GroupExcursion.QtyStudents, group.GroupExcursion.QtyLeaders, group.GroupExcursion.QtyGuides, TrimText(group.GroupExcursion.Comment, 20), group.GroupExcursion.Comment, (group.GroupExcursion.TransportRequired.HasValue && group.GroupExcursion.TransportRequired.Value) ? "../Content/img/actions/accept.png" : "../Content/img/actions/delete.png", (group.GroupExcursion.IsDeleted.HasValue && group.GroupExcursion.IsDeleted.Value) ? "disabled" : String.Empty);
                    }
                }
                else
                {
                    using (Pace.DataAccess.Excursion.ExcursionEntities eentity = new ExcursionEntities())
                    {
                        var openbookingquery = (from e in eentity.Bookings
                                                from g in e.Lnk_Group_Booking
                                                where g.GroupId == id
                                                select e).ToList();

                        if (openbookingquery.Count > 0)
                        {
                            foreach (Booking booking in openbookingquery.ToList())
                            {
                                sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\"><a href=\"\">{1}</a></td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4} / {5} / {6}</td><td style=\"text-align: left; font-size:large\"><img src=\"{9}\" /></td><td style=\"text-align: left; font-size:large\"><a href=\"#\" class=\"information\">{7}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Edit Excursion\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Excursions/AddBookingControl.ascx',{{'BookingId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Excursion\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupExcursion', {0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", booking.BookingId, booking.Title, booking.ExcursionDate.ToString("D"), booking.ExcursionTime, booking.QtyStudents, booking.QtyLeaders, booking.QtyGuides, string.Format("Meet at {0} @ {1}", booking.MeetLocation, booking.MeetTime), "", (booking.Xlk_TransportType != null && booking.Xlk_TransportType.TransportTypeId == 4) ? "../Content/img/actions/accept.png" : "../Content/img/actions/delete.png");
                            }
                        }
                        else
                            sb.Append("<tr><td colspan=\"9\">No Excursions to Show!</td></tr>");
                    }
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupClasses(int id)
    {

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"ajaxMessageBox('ViewGroupPage.aspx', 'CreateClasses','The current classes and their assignments will be permanently deleted and cannot be recovered. Are you sure?' ," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Create Classes</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Class Id</th><th scope=\"col\">Course Type</th><th scope=\"col\">Room Number</th><th scope=\"col\">Teacher</th><th scope=\"col\">Dates</th><th scope=\"col\">Tuition Times</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (Tuition.TuitionEntities entity = new Tuition.TuitionEntities())
            {
                var query = ((from c in entity.Class
                              from g in c.Groups
                              where g.GroupId == id
                              orderby c.Label
                              select c) as ObjectQuery<Pace.DataAccess.Tuition.Class>).Include("Teacher").Include("ClassRoom").Include("Xlk_CourseType");

                if (query.Count() > 0)
                {
                    foreach (Pace.DataAccess.Tuition.Class _class in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1} - {2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5} - {6}</td> <td style=\"text-align: left\">{7} - {8}</td><td style=\"text-align: center\"><a title=\"Edit Class\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Tuition/ViewClassPage.aspx','Tuition/AddClassControl.ascx',{{'ClassId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"View Class\" href=\"../Tuition/ViewClassPage.aspx?ClassId={0}#viewTabs-2\"><img src=\"../Content/img/actions/arrow_right.png\"></a></td></tr>", _class.ClassId, _class.Label, _class.Xlk_CourseType != null ? _class.Xlk_CourseType.Description : "Not Specified", (_class.ClassRoom != null) ? _class.ClassRoom.Label : "Not Assigned", (_class.Teacher != null) ? _class.Teacher.Name : "Not Assigned", _class.StartDate.ToString("D"), _class.EndDate.ToString("D"), _class.DailyStartTime, _class.DailyEndTime);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"9\">No Classes to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string CreateClasses(int id)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            int qty = entity.CreateGroupClasses(id);
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupComplaints(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupComplaintControl.ascx',{ GroupId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Complaint</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">StudentComplaintId</th><th scope=\"col\">Severity</th><th scope=\"col\">Type</th><th scope=\"col\">Complaint</th><th scope=\"col\">Date Created</th><th scope=\"col\">Raised By</th><th scope=\"col\">Action Needed</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (GroupEntities entity = new GroupEntities())
            {
                var query = from n in entity.GroupComplaint.Include("Xlk_Severity").Include("Xlk_ComplaintType").Include("Users")
                            where n.Group.GroupId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (GroupComplaint complaint in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align:left\">{5}</td><td style=\"text-align:left\">{6}</td><td style=\"text-align: center\"><a title=\"Edit Complaint\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupComplaintControl.ascx',{{'GroupComplaintId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Complaint\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupComplaint',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", complaint.GroupComplaintId, complaint.Xlk_Severity.Description, complaint.Xlk_ComplaintType.Description, complaint.Complaint, complaint.DateCreated.ToString("D"), complaint.Users.Name, (complaint.ActionNeeded ? "YES" : "NO"));
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Complaints to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupInvoice(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupInvoiceControl.ascx',{ GroupId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Finance Ref</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Group Invoice Id</th><th scope=\"col\">Ref</th><th scope=\"col\">Type</th><th scope=\"col\">Date Recorded</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (GroupEntities entity = new GroupEntities())
            {
                var query = from n in entity.GroupInvoice.Include("Xlk_SageDataType")
                            where n.Group.GroupId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (GroupInvoice item in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\"><a href=\"{4}{1}\">{1}</a></td> <td style=\"text-align: left\">{1}</td><td style=\"text-align:left\">{3}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: center\"><a title=\"Edit Invoice\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupInvoiceControl.ascx',{{'GroupInvoiceId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Ref\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupInvoice',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td> </tr>", item.GroupInvoiceId, item.SageInvoiceRef, (item.DateRecorded.HasValue) ? item.DateRecorded.Value.ToString("D") : string.Empty, item.Xlk_SageDataType.Description, item.Xlk_SageDataType.SageDataTypeId == 4 ? VirtualPathUtility.ToAbsolute("~/Agent/ViewQuotePage.aspx?QuoteId=") : VirtualPathUtility.ToAbsolute("~/Finance/ViewSageInvoicePage.aspx?InvoiceNumber="));
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"5\">No Refs to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupNotes(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupNoteControl.ascx',{ GroupId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Note</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">NoteId</th><th scope=\"col\">Note Type</th><th scope=\"col\">Note</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (GroupEntities entity = new GroupEntities())
            {
                var query = from n in entity.GroupNote.Include("Xlk_NoteType")
                            where n.Group.GroupId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (GroupNote note in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: center\"><a title=\"Edit Note\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupNoteControl.ascx',{{'GroupNoteId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Note\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupNote',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td> </tr>", note.GroupNoteId, note.Xlk_NoteType.Description, note.Note, note.DateCreated.Value.ToString("D"), note.GroupNoteId);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"5\">No Notes to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SendStudentInfoEmailToFamilies(Dictionary<string,string> newObject)
    {
        try
        {
           
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                //GroupService service = ServiceFactory<IGroupService, GroupService>.Instance.GetService(null);
                
                //ResultMessage message = service.SendEmailToFamilies(Convert.ToInt32(newObject["GroupId"]), Convert.ToInt32(newObject["UserId"]), Convert.ToInt32(newObject["MessageId"]), newObject["Subject"], newObject["Body"]);
                //((IClientChannel)service).Close();
            }

            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }
    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupReservedFamilies(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}\"><img src=\"../Content/img/actions/printer.png\" />Print Listing</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupFamilyReservationControl.ascx',{{'GroupId':{0}}})\"><img src=\"../Content/img/actions/add.png\" />Reserve Families</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Family Id</th><th scope=\"col\">Family</th><th scope=\"col\">Assigned By</th><th scope=\"col\">Assigned Date</th><th scope=\"col\">Confirmed By</th><th scope=\"col\">Confirmed Date</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, ReportPath("GroupReports", "ReservedGroupFamilyListing", "HTML4.0", new Dictionary<string, object> { { "GroupId", id } })));
            using (GroupEntities entity = new GroupEntities())
            {
                string userName = ""; string userConf = "";
                var query = from n in entity.ReservedFamilies.Include("Family")
                            where n.GroupId == id
                            orderby n.Family.SurName
                            select n;
                var userQuery = (from u in entity.Users
                                select u).ToList();

                if (query.Count() > 0)
                {
                    foreach (ReservedFamily family in query.ToList())
                    {
                        userName = ""; userConf = "";
                        foreach (Users user in userQuery)
                        {
                            if (user.UserId == family.AssignedBy)
                                userName = user.Name;
                            if (family.ConfirmedBy.HasValue && user.UserId == family.ConfirmedBy)
                                userConf = user.Name;
                        }
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{3}</td><td><img src=\"../Content/img/actions/bin_closed.png\" title=\"Remove Reservation\"/></td></tr>", BasePage.FamilyLink(family.Family.FamilyId, "~/Accommodation/ViewAccommodationPage.aspx", BasePage.CreateName(family.Family.FirstName, family.Family.SurName)), family.Family.FamilyId, (family.AssignedDate.HasValue) ? family.AssignedDate.Value.ToString("D") : string.Empty, (family.ConfirmedDate.HasValue) ? family.ConfirmedDate.Value.ToString("D") : string.Empty, userName, userConf);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"7\">No Reserved Families to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupHostings(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}\"><img src=\"../Content/img/actions/printer.png\" />Print Listing</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/EmailGroupFamilyControl.ascx',{{'GroupId':{0}}})\"><img src=\"../Content/img/actions/mail_send.png\" />Email Student Details to Families</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Hosting Id</th><th scope=\"col\">Family</th><th scope=\"col\">Type</th><th scope=\"col\">Group Member</th><th scope=\"col\">Arrival</th><th scope=\"col\">Departure</th><th scope=\"col\">Weekly Rate</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, ReportPath("GroupReports", "GroupFamilyListing", "HTML4.0", new Dictionary<string, object> { { "GroupId", id } })));
            
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                var query = from n in entity.Hostings.Include("Family").Include("Student").Include("Student.Agency").Include("Student.Xlk_StudentType")
                            where n.Student.GroupId == id
                            orderby  n.Family.SurName
                            select n;

                if (query.Count() > 0)
                {
                    foreach (Pace.DataAccess.Accommodation.Hosting hosting in query.ToList())
                    {
                        string uri = String.Empty;


                        //if (!string.IsNullOrEmpty(hosting.Family.SurName))
                        // uri = string.Format("http://pace-web01/Reviews/{0}/{1} {2}/{3}.pdf", hosting.Family.SurName[0], hosting.Family.FirstName, hosting.Family.SurName, hosting.HostingId);


                        if (!string.IsNullOrEmpty(hosting.Family.SurName))
                            uri = string.Format("http://pace-web01/Reviews/{0}/{1}/{2}.pdf", hosting.Family.SurName[0], (!string.IsNullOrEmpty(hosting.Family.CustomFolderName)) ? hosting.Family.CustomFolderName.Trim() : string.Format("{0} {1}", hosting.Family.FirstName.Trim(), hosting.Family.SurName.Trim()), hosting.HostingId);


                        sb.AppendFormat("<tr><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{10}</td><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{9}</td><td style=\"text-align: center\">{11}<a title=\"Edit Hosting\" onclick=\"loadEditArrayControl('#dialog-form','../Enrollments/ViewStudentPage.aspx','Enrollments/AddStudentHostingControl.ascx',{{'HostingId':{6}}})\" href=\"#\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a target=\"blank\" href=\"{8}?/PaceManagerReports/FamilyDetails&GroupId={7}&AgencyId=0&StudentId={1}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\" title=\"Print Hosting Info\"><img src=\"../Content/img/actions/printer2.png\" title=\"Print Hosting Details\" alt=\"Print Hosting Details\"/></a>&nbsp;<a title=\"Delete Hosting\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','StudentHosting',{6})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", BasePage.StudentLink(hosting.Student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(hosting.Student.FirstName, hosting.Student.SurName)), hosting.Student.StudentId, BasePage.FamilyLink(hosting.Family.FamilyId, "~/Accommodation/ViewAccommodationPage.aspx", BasePage.CreateName(hosting.Family.FirstName, hosting.Family.SurName)), hosting.Family.FamilyId, hosting.ArrivalDate.ToString("D"), hosting.DepartureDate.ToString("D"), hosting.HostingId, id, ReportPath("GroupReports", "FamilyDetails", "PDF", new Dictionary<string, object> { { "GroupId", hosting.Student.GroupId }, { "AgencyId", hosting.Student.Agency.AgencyId }, { "StudentId", hosting.Student.StudentId } }), hosting.WeeklyRate, hosting.Student.Xlk_StudentType.Description, (UriExists(uri)) ? string.Format("<a target=\"new\" href=\"{0}\"><img src=\"{1}\"></a>", uri, System.Web.VirtualPathUtility.ToAbsolute("~/Content/img/actions/pdf.png")) : string.Empty);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"6\">No Hosting Records to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupStudents(int id)
    {
        string leader = "";
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddStudentGroupingControl.ascx',{{ GroupId:{0}}})\"><img src=\"../Content/img/actions/add.png\" />Student Grouping</a><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupStudentControl.ascx',{{GroupId: {0}}})\"><img src=\"../Content/img/actions/add.png\" />Add New Member</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">No.</th><th scope=\"col\">Type</th><th scope=\"col\">Name</th><th scope=\"col\">Gender</th><th scope=\"col\">Age</th><th scope=\"col\">DOB</th><th scope=\"col\">Sharing with 1</th><th scope=\"col\">Sharing with 2</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (GroupEntities entity = new GroupEntities())
            {
                using (Enrollments.EnrollmentsEntities eentity = new Enrollments.EnrollmentsEntities())
                {
                    var query = from n in eentity.Student.Include("Xlk_Gender").Include("Xlk_StudentType")
                                where n.GroupId == id
                                orderby n.Xlk_StudentType.Description, n.Xlk_Gender.Description, n.DateOfBirth.HasValue ? n.DateOfBirth.Value : DateTime.Now descending, n.FirstName, n.SurName
                                select n;

                    if (query.Count() > 0)
                    {
                        var counter = 1; 
                        foreach (Pace.DataAccess.Enrollment.Student student in query.ToList())
                        {
                            foreach (Group group in entity.Group)
                                if (group.GroupId == student.GroupId && group.GroupName == student.SurName)
                                {
                                    leader = group.GroupName.ToString();
                                    sb.AppendFormat("<tr><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">.</td><td style=\"text-align: left\">.</td><td style=\"text-align: center\"><a href=\"{7}\" title=\"Print Certificate\"><img src=\"../Content/img/actions/printer2.png\" title=\"Print Certificate\" alt=\"Print Certificate\" /></a>&nbsp;<a title=\"Remove Student from Group\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupStudent',{4})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(student.FirstName, student.SurName)), student.Xlk_Gender.Description, BasePage.CalculateAge(student.DateOfBirth), (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("D") : string.Empty, student.StudentId, student.Xlk_StudentType.Description, counter, ReportPath("EnrollmentReports", "IndividualStudentCert", "HTML4.0", new Dictionary<string, object> { { "StudentId", student.StudentId } }));
                                    counter = counter + 1;
                                }
                        }

                        var maxGrouping = 0; string first = "";
                        foreach (Pace.DataAccess.Enrollment.Student student in query.ToList())
                        {
                            if (leader != student.SurName)
                            {
                                maxGrouping = 0; first = "";
                                sb.AppendFormat("<tr><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td>", BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(student.FirstName, student.SurName)), student.Xlk_Gender.Description, BasePage.CalculateAge(student.DateOfBirth), (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("D") : string.Empty, student.StudentId, student.Xlk_StudentType.Description, counter);

                                foreach (Pace.DataAccess.Enrollment.Student share in query.ToList())
                                {
                                    if (share.GroupingBlockId == student.GroupingBlockId && (share.SurName != student.SurName || (share.SurName == student.SurName && share.FirstName.Substring(0,1) != student.FirstName.Substring(0,1))) && first == "" && student.GroupingBlockId > 0)
                                    {
                                        first = share.SurName; maxGrouping = 1;
                                        sb.AppendFormat("<td style=\"text-align: left\">{7}</td>", BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(student.FirstName, student.SurName)), student.Xlk_Gender.Description, BasePage.CalculateAge(student.DateOfBirth), (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("D") : string.Empty, student.StudentId, student.Xlk_StudentType.Description, counter, share.SurName);
                                    }
                                    if (share.GroupingBlockId == student.GroupingBlockId && (share.SurName != student.SurName && share.SurName != first) && student.GroupingBlockId > 0)
                                    {
                                        maxGrouping = 2;
                                        sb.AppendFormat("<td style=\"text-align: left\">{7}</td>", BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(student.FirstName, student.SurName)), student.Xlk_Gender.Description, BasePage.CalculateAge(student.DateOfBirth), (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("D") : string.Empty, student.StudentId, student.Xlk_StudentType.Description, counter,  share.SurName);
                                    }
                                }
                                if (maxGrouping == 0)
                                {
                                    sb.AppendFormat("<td style=\"text-align: left\">&nbsp;</td><td style=\"text-align: left\">&nbsp;</td>");
                                }
                                if (maxGrouping == 1)
                                {
                                    sb.AppendFormat("<td style=\"text-align: left\">&nbsp;</td>");
                                }
                                sb.AppendFormat("<td style=\"text-align: center\"><a href=\"{7}\" title=\"Print Certificate\"><img src=\"../Content/img/actions/printer2.png\" title=\"Print Certificate\" alt=\"Print Certificate\"/></a>&nbsp;<a title=\"Remove Student from Group\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupStudent',{4})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(student.FirstName, student.SurName)), student.Xlk_Gender.Description, BasePage.CalculateAge(student.DateOfBirth), (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("D") : string.Empty, student.StudentId, student.Xlk_StudentType.Description, counter, ReportPath("AcademicReports", "NewIndividualStudentCert", "HTML4.0", new Dictionary<string, object> { { "StudentId", student.StudentId } }));
                                counter = counter + 1;
                            }
                        }
                    }
                    else
                    {
                        sb.Append("<tr><td colspan=\"9\">No Previous Students to Show!</td></tr>");
                    }
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupTransfers(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}\"><img src=\"../Content/img/actions/printer.png\" />Print Group Details</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupTransferControl.ascx',{{ GroupId:{0}}})\"><img src=\"../Content/img/actions/add.png\" />Add New Transfer</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Group TransferId</th><th scope=\"col\">Type</th><th scope=\"col\">Transfer Date</th><th scope=\"col\">Location </th><th scope=\"col\">Flight Number</th><th scope=\"col\">Completed By</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, ReportPath("GroupReports", "GroupDetailsReport", "HTML4.0", new Dictionary<string, object> { { "GroupId", id } })));
            using (GroupEntities entity = new GroupEntities())
            {
                var query = from h in entity.GroupTransfer.Include("Xlk_Location").Include("Xlk_TransferType").Include("Xlk_BusinessEntity")
                            where h.Group.GroupId == id
                            select h;


                if (query.Count() > 0)
                {
                    foreach (GroupTransfer transfer in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2} @ {7}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\"><a href=\"#\" class=\"information\" style=\"font-size: x-small\">{6}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Edit Transfer\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupTransferControl.ascx',{{'GroupTransferId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Transfer\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupTransfer',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", transfer.GroupTransferId, transfer.Xlk_TransferType.Description, transfer.Date.ToString("D"), transfer.Xlk_Location.Description, transfer.FlightNumber, transfer.Xlk_BusinessEntity.Description, TrimText(transfer.Comment, 20), (transfer.Time.HasValue) ? transfer.Time.Value.ToString() : string.Empty, transfer.Comment);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupTransport(int id)
    {

        List<Int32?> _bookingIdList = new List<Int32?>();

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">BookingId</th><th scope=\"col\">Title</th><th scope=\"col\">Date/Time</th><th scope=\"col\">Status</th><th scope=\"col\">Type</th><th scope=\"col\">Pickup</th><th scope=\"col\">Dropoff</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, ReportPath("GroupReports", "GroupDetailsReport", "HTML4.0", new Dictionary<string, object> { { "GroupId", id } })));
            using (Pace.DataAccess.Transport.TransportEntities tentity = new Pace.DataAccess.Transport.TransportEntities())
            {
                _bookingIdList = tentity.FindGroupTransportBookingIds(id).ToList();

            

            if (_bookingIdList.Count > 0)
            {
                var query = from h in tentity.Bookings
                            where _bookingIdList.Contains(h.BookingId)
                            select new {h.BookingId, h.Title, h.RequestedDate, h.RequestedTime, Status = h.Xlk_Status.Description, TripType = h.Xlk_TripType.Description, PickupLocation = h.PickupLocation.Description, DropoffLocation=h.DropoffLocation.Description };


                    if (query.Count() > 0)
                    {
                        foreach (var transport in query.ToList())
                        {
                            sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\"><a href=\"../Transport/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td style=\"text-align: left\">{2} @ {3}</td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: center\"><a title=\"Edit Transport\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Transport/ViewBookingPage.aspx','Transport/AddTransportBookingControl.ascx',{{'BookingId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Transport Booking\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('../Transport/BookingsPage.aspx','TransportBooking',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", transport.BookingId, TransportBookingLink(transport.BookingId, "~/Transport/ViewBookingPage.aspx", transport.Title), transport.RequestedDate.ToString("D"), transport.RequestedTime, transport.Status, transport.TripType, transport.PickupLocation, transport.DropoffLocation);
                        }
                    }
                    else
                    {
                        sb.Append("<tr><td colspan=\"8\">No Transports to Show!</td></tr>");
                    }
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

   

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupTasks(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Support/ViewTaskPage.aspx','Support/AddTaskControl.ascx','Ref',{'GroupId':" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Task</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Assigned To</th><th scope=\"col\">Priority</th><th scope=\"col\">Status</th><th scope=\"col\">Description</th><th scope=\"col\">Action Needed</th><th scope=\"col\">Date Due</th><th scope=\"col\">Action Taken</th><th scope=\"col\">Date Completed</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            string reference = "GroupId:" + id.ToString();

            using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
            {

                var tasks = (from t in entity.Task.Include("Xlk_Status").Include("Xlk_Priority").Include("RaisedByUser").Include("AssignedToUser")
                             where t.Ref == reference
                             orderby t.DateDue
                             select t).ToList();


                if (tasks != null && tasks.Count > 0)
                {
                    foreach (Pace.DataAccess.Support.Task item in tasks)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{8}</td><td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: center\">", item.TaskId, item.Xlk_Priority.Description, item.Xlk_Status.Description, TrimTextA(item.Description, 40), TrimTextA(item.ActionNeeded, 20), item.DateDue.ToString("dd MMM yyyy"), item.ActionTaken, item.DateCompleted, item.AssignedToUser.Name);

                        if (item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Edit Task\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Support/ViewTaskPage.aspx','Support/AddTaskControl.ascx',{{'TaskId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;", item.TaskId);

                        if ((item.Xlk_Status.StatusId == 1) && item.AssignedToUser.UserId == GetCurrentUser().UserId | item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Close Task\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../Support/ViewTaskPage.aspx','Support/AddTaskControl.ascx',{{ TaskId: {0}, Close: {0}}} )\"><img src=\"../Content/img/actions/user_accept.png\"></a>&nbsp;", item.TaskId);

                        if (item.RaisedByUser.UserId == GetCurrentUser().UserId)
                            sb.AppendFormat("<a title=\"Delete Item\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('../Support/ViewTaskPage.aspx','Task',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>", item.TaskId);

                        sb.Append("</td></tr>");
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"10\">No Task Records to Show!</td></tr>");
                }
            }

           sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, object> entitykeys)
    {
        try
        {
            var page = new BaseGroupPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool ProcessTasks(int id)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                var group = entity.Group.Single(x => x.GroupId == id);
                return BaseSupportPage.ProcessTasks(1, group.GroupId, group.ArrivalDate, group.DepartureDate);
            }
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroupNote(GroupNote newObject)
    {
        try
        {
            GroupNote newnote = new GroupNote();
            using (GroupEntities entity = new GroupEntities())
            {
                if (newObject.GroupNoteId > 0)
                    newnote = (from c in entity.GroupNote where c.GroupNoteId == newObject.GroupNoteId select c).FirstOrDefault();

                newnote.DateCreated = DateTime.Now;
                newnote.Note = newObject.Note;
                newnote.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                newnote.Xlk_NoteTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_NoteType", newObject.Xlk_NoteType);
                newnote.UsersReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users","UserId", GetCurrentUser().UserId);

                if (newnote.GroupNoteId == 0)
                    entity.AddToGroupNote(newnote);

                entity.SaveChanges();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroup(Group newObject)
    {
        try
        {
            Group newgroup = new Group();
            using (GroupEntities entity = new GroupEntities())
            {
                if (newObject.GroupId > 0)
                    newgroup = (from c in entity.Group where c.GroupId == newObject.GroupId select c).FirstOrDefault();
                else
                    entity.AddToGroup(newgroup);

                newgroup.ArrivalDate = newObject.ArrivalDate;
                newgroup.ArrivalInfo = newObject.ArrivalInfo;
                newgroup.BusTickets = newObject.BusTickets;

                newgroup.CampusId = newObject.CampusId;
                newgroup.DepartureDate = newObject.DepartureDate;
                newgroup.DepartureInfo = newObject.DepartureInfo;
                newgroup.GroupCurfew = newObject.GroupCurfew;
                newgroup.GroupName = newObject.GroupName;

                if (newObject.IsClosed != null && newObject.IsClosed)
                    newgroup.IsClosed = true ;
                else
                    newgroup.IsClosed = false;

                newgroup.LeaderMobile = newObject.LeaderMobile;
                newgroup.NoOfClasses = newObject.NoOfClasses;
                newgroup.NoOfLeaders = newObject.NoOfLeaders;
                newgroup.NoOfStudents = newObject.NoOfStudents;

                if (newObject.BusTickets != null && newObject.BusTickets.HasValue && newObject.BusTickets.Value)
                    newgroup.BusTickets = newObject.BusTickets.Value;
                else
                    newgroup.BusTickets = false;

                newgroup.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);
                newgroup.Xlk_NationalityReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Nationality", newObject.Xlk_Nationality);
                newgroup.AgencyReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Agency", newObject.Agency);

                entity.SaveChanges();

                if (newObject.GroupId == 0)
                {
                    if (newgroup.GroupId > 0)
                        return new AjaxResponse(string.Format("{0}?GroupId={1}", VirtualPathUtility.ToAbsolute("~/Groups/ViewGroupPage.aspx"), newgroup.GroupId));
                    else
                        return new AjaxResponse(new Exception("Could not find the new group Identifier, please search for this group before trying to add again!"));
                }
            }

            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroupComplaint(GroupComplaint newObject)
    {
        try
        {
            GroupComplaint newcomplaint = new GroupComplaint();
            using (GroupEntities entity = new GroupEntities())
            {
                if (newObject.GroupComplaintId > 0)
                    newcomplaint = (from c in entity.GroupComplaint where c.GroupComplaintId == newObject.GroupComplaintId select c).FirstOrDefault();

                newcomplaint.ActionNeeded = newObject.ActionNeeded;
                newcomplaint.Complaint = newObject.Complaint;
                newcomplaint.DateCreated = DateTime.Now;
                newcomplaint.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                newcomplaint.Xlk_ComplaintTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_ComplaintType", newObject.Xlk_ComplaintType);
                newcomplaint.Xlk_SeverityReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Severity", newObject.Xlk_Severity);
                newcomplaint.UsersReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", newObject.Users);

                if (newcomplaint.GroupComplaintId == 0)
                    entity.AddToGroupComplaint(newcomplaint);

                entity.SaveChanges();
            }

            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroupExcursion(GroupExcursion newObject)
    {
        GroupExcursion newexcursion = new GroupExcursion();

        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                if (newObject.GroupExcursionId > 0)
                    newexcursion = (from e in entity.GroupExcursion where e.GroupExcursionId == newObject.GroupExcursionId select e).FirstOrDefault();


                newexcursion.Comment = newObject.Comment;
                newexcursion.PreferredDate = newObject.PreferredDate;
                newexcursion.PreferredTime = newObject.PreferredTime;
                newexcursion.QtyGuides = newObject.QtyGuides;
                newexcursion.QtyLeaders = newObject.QtyLeaders;
                newexcursion.QtyStudents = newObject.QtyStudents;
                newexcursion.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                newexcursion.ExcursionReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Excursion", newObject.Excursion);

                if (newObject.IsDeleted.HasValue)
                    newexcursion.IsDeleted = true;
                else
                    newexcursion.IsDeleted = false;

                if (!newObject.TransportRequired.HasValue)
                    newexcursion.TransportRequired = true;
                else
                    newexcursion.TransportRequired = false;

                if (newexcursion.GroupExcursionId == 0)
                    entity.AddToGroupExcursion(newexcursion);

                entity.SaveChanges();
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroupInvoice(GroupInvoice newObject)
    {
        GroupInvoice newinvoice = new GroupInvoice();

        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                if (newObject.GroupInvoiceId > 0)
                    newinvoice = (from i in entity.GroupInvoice where i.GroupInvoiceId == newObject.GroupInvoiceId select i).FirstOrDefault();

                newinvoice.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                newinvoice.SageInvoiceRef = newObject.SageInvoiceRef;
                newinvoice.Xlk_SageDataTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_SageDataType", newObject.Xlk_SageDataType);
                newinvoice.DateRecorded = DateTime.Now;

                if (newinvoice.GroupInvoiceId == 0)
                    entity.AddToGroupInvoice(newinvoice);

                entity.SaveChanges();
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveEnrollment(Enrollment newObject)
    {
        Enrollment newenrollment = new Enrollment();
        
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                if (newObject.EnrollmentId > 0)
                    newenrollment = (from e in entity.Enrollment where e.EnrollmentId == newObject.EnrollmentId select e).FirstOrDefault();

                newenrollment.EndDate = newObject.EndDate;
                newenrollment.StartDate = newObject.StartDate;
                newenrollment.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                newenrollment.Xlk_LessonBlockReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_LessonBlock", newObject.Xlk_LessonBlock);
                newenrollment.Xlk_ProgrammeTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_ProgrammeType", newObject.Xlk_ProgrammeType);
                newenrollment.Xlk_CourseTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_CourseType", newObject.Xlk_CourseType);
                newenrollment.MorningLessonDays = newObject.MorningLessonDays;
                newenrollment.AfternoonLessonDays = newObject.AfternoonLessonDays;
                newenrollment.EveningLessonDays = newObject.EveningLessonDays;

                if (newenrollment.EnrollmentId == 0)
                    entity.AddToEnrollment(newenrollment);

                entity.SaveChanges();
            }
            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroupStudent(Pace.DataAccess.Enrollment.Student newObject)
    {
        try
        {

            Group _group = GetGroup(newObject.GroupId.Value);
            Pace.DataAccess.Enrollment.Xlk_Status status = (from s in BaseEnrollmentPage.LoadStatus()
                                                        where s.StatusId == Convert.ToByte((_group.Xlk_Status.StatusId == 1) ? 0 : 1)
                                                        select s).FirstOrDefault();

            if (_group != null)
            {
                using (Enrollments.EnrollmentsEntities entity = new Enrollments.EnrollmentsEntities())
                {
                    Pace.DataAccess.Enrollment.Student newstudent = new Pace.DataAccess.Enrollment.Student();

                    newstudent.DateOfBirth = newObject.DateOfBirth;
                    newstudent.ArrivalDate = _group.ArrivalDate;
                    newstudent.DepartureDate = _group.DepartureDate;
                    newstudent.FirstName = newObject.FirstName;
                    newstudent.SurName = newObject.SurName;
                    newstudent.GroupId = newObject.GroupId;
                    newstudent.CampusId = _group.CampusId;

                    newstudent.Xlk_GenderReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Gender", newObject.Xlk_Gender);
                    newstudent.Xlk_StudentTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_StudentType", newObject.Xlk_StudentType);

                    newstudent.Xlk_NationalityReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Nationality", "NationalityId", (newObject.Xlk_Nationality == null) ? _group.Xlk_Nationality.NationalityId : newObject.Xlk_Nationality.NationalityId);// BaseEnrollmentPage.Entities.CreateEntityKey(BaseEnrollmentPage.Entities.DefaultContainerName + ".Xlk_Nationality", (newObject.Xlk_Nationality == null) ? group.Xlk_Nationality : newObject.Xlk_Nationality);
                    newstudent.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(1)); 
                    newstudent.AgencyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Agency", "AgencyId", _group.Agency.AgencyId);

                    if (newstudent.StudentId == 0)
                        entity.AddToStudent(newstudent);

                    if (entity.SaveChanges() > 0)
                    {
                        using (GroupEntities gentity = new GroupEntities())
                        {
                            int qty = gentity.CreateGroupEnrollments(_group.GroupId, newstudent.StudentId);
                        }
                    }
                }
                return new AjaxResponse();

            }
            else
                return new AjaxResponse(new Exception("The Group could not be found"));

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveGroupTransfer(GroupTransfer newObject)
    {
        GroupTransfer newtransfer = new GroupTransfer();

        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                if (newObject.GroupTransferId > 0)
                    newtransfer = (from t in entity.GroupTransfer where t.GroupTransferId == newObject.GroupTransferId select t).FirstOrDefault();

                newtransfer.Comment = newObject.Comment;
                newtransfer.Date = newObject.Date;
                newtransfer.FlightNumber = newObject.FlightNumber;
                newtransfer.Time = newObject.Time;
                newtransfer.NumberOfPassengers = newObject.NumberOfPassengers;
                newtransfer.GroupTransferId = newObject.GroupTransferId;

                newtransfer.TransportBookingId = newObject.TransportBookingId;
                newtransfer.GroupReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Group", newObject.Group);
                newtransfer.Xlk_LocationReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Location", newObject.Xlk_Location);
                newtransfer.Xlk_TransferTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_TransferType", newObject.Xlk_TransferType);
                newtransfer.Xlk_BusinessEntityReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_BusinessEntity", newObject.Xlk_BusinessEntity);

                newtransfer.FamilyCollectionTime = newObject.FamilyCollectionTime;
                newtransfer.FamilyCollectionLocation = newObject.FamilyCollectionLocation;
                newtransfer.FamilyDropOffTime = newObject.FamilyDropOffTime;
                newtransfer.FamilyDropOffLocation = newObject.FamilyDropOffLocation;


                if (newtransfer.GroupTransferId == 0)
                    entity.AddToGroupTransfer(newtransfer);

                entity.SaveChanges();
            }

            return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    private static bool RemoveHostingForGroupingBlock(int groupid, short groupingblockid)
    {
        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {
            var hostings = (from s in entity.Student
                            join h in entity.Hostings on s.StudentId equals h.Student.StudentId
                            where s.GroupId == groupid && s.GroupingBlockId == groupingblockid
                            select new { h,h.HostingConfirmations }).ToList();

            if (hostings != null && hostings.Count > 0)
            {
                foreach (var item in hostings)
                {
                    foreach (Accommodation.HostingConfirmation confirm in item.HostingConfirmations.ToList())
                    {
                        entity.DeleteObject(confirm);
                    }


                    entity.DeleteObject(item.h);

                }

                if (entity.SaveChanges() > 0)
                    return true;
            }


        }
        return false;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse AssignStudentToGroupingBlock(short groupingblockid, int studentid)
    {
        try
        {
            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {

                if (groupingblockid == -1)
                {
                    groupingblockid = ((from m in entity.Student
                                        where m.GroupId == (from s in entity.Student where s.StudentId == studentid select s.GroupId).FirstOrDefault()
                                        select m.GroupingBlockId).Max().GetValueOrDefault(0));
                    groupingblockid++;
                }

                Pace.DataAccess.Enrollment.Student student = (from s in entity.Student
                                                              where s.StudentId == studentid
                                                              select s).First();

                RemoveHostingForGroupingBlock(student.GroupId.Value, Convert.ToInt16(groupingblockid));

                if (student != null && student.GroupingBlockId == null)
                    student.GroupingBlockId = Convert.ToInt16(groupingblockid);
                

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();

                else
                    return new AjaxResponse(new Exception("Could not save this move"));
            }
        }

        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse DeleteGrouping(int groupingblockid, int groupid)
    {
        try
        {
            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Enrollments.EnrollmentsEntities())
            {
                RemoveHostingForGroupingBlock(groupid, Convert.ToInt16(groupingblockid));

                var student = from d in entity.Student
                                where d.GroupingBlockId == groupingblockid && d.GroupId == groupid
                                select d;

                foreach (Pace.DataAccess.Enrollment.Student item in student)
                {
                    item.GroupingBlockId = null;
                }

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();
                else
                    return new AjaxResponse(new Exception("Could not save this move"));
            }
        }

        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadInvoiceReceipts(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddInvoiceReceiptControl.ascx',{ GroupId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Receipt</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Amount Receipted</th><th scope=\"col\">Receipt Type</th><th scope=\"col\">Sage Invoice Ref</th><th scope=\"col\">Date</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (GroupEntities entity = new GroupEntities())
            {
                var query = from n in entity.InvoiceReceipts.Include("Xlk_PaymentMethod")
                            where n.Group.GroupId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (InvoiceReceipt item in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: center\"><a title=\"Edit Note\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/AddInvoiceReceiptControl.ascx',{{'InvoiceReceiptId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a></td> </tr>", item.InvoiceReceiptId, item.AmountPaid, item.Xlk_PaymentMethod.Description, item.SageInvoiceRef, item.DateRecorded.ToString("dd/MMM/yy"), item.Comment);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Items to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetOrderableItem(int groupid, short itemid)
    {
        Int32? agencyid = null;

        try
        {
            using (Pace.DataAccess.Group.GroupEntities eentity = new Pace.DataAccess.Group.GroupEntities())
            {
                agencyid = (from s in eentity.Group where s.GroupId == groupid select s.Agency.AgencyId).FirstOrDefault();
            }
            using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
            {
                var item = from r in entity.GetAgentRateCards(agencyid, itemid)
                           from c in entity.GetAgencyRateCardCostings(agencyid, itemid)
                           group c by r into grouping
                           select new { grouping.Key.RateCardId, grouping.Key.OrderableItem.Description, grouping.Key.Xlk_Unit.UnitId, Costs = grouping.ToList() };

                return new AjaxResponse<string>(new JavaScriptSerializer().Serialize(item));
            }

        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }



    #endregion

}
