﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AjaxResponse
/// </summary>

public class AjaxResponse : BaseResponse
{

    public string RedirectUrl { get; set; }


    public AjaxResponse(string redirectUrl):this()
    {
        RedirectUrl = redirectUrl;
    }

    public AjaxResponse()
    {
        Success = true;
    }
    public AjaxResponse(Exception ex)
    {
        ErrorMessage = ex.ToString();
        Success = false;
    }

}

public class AjaxResponse<T> : AjaxResponse
{

    public T Payload { get; set; }

    public AjaxResponse(T payload,string redirectUrl)
    {
        RedirectUrl = redirectUrl;
        Payload = payload;
        Success = true;
    }

	public AjaxResponse(T payload)
	{
        Payload = payload;
        Success = true;
	}
    public AjaxResponse()
    {
        Success = true;
    }
    public AjaxResponse(Exception ex)
    {
        ErrorMessage = ex.ToString();
        Success = false;
    }

}