﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GridResponse
/// </summary>
public class GridResponse<t>
{

        public int page { get; set; }
        public int total { get; set; }
        public int records { get; set; }
        public List<t> rows { get; set; }

        public GridResponse()
        {
            rows = new List<t>();
        }
}