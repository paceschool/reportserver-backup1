﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FileParser
/// </summary>
public class FileParser
{

    public string Path { get; set; }
    public string FileName { get; set; }

	public FileParser(string path, string filename)
	{
        Path = path;
        FileName = filename;
	}
}