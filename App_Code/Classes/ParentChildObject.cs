﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ParentChildObject
/// </summary>
public class ParentChildObject<P,C>
{
    public P ParentId { get; set; }
    public C Child { get; set; }
    public string RelationshipSet { get; set; }

    public ParentChildObject()
    {

    }
}
