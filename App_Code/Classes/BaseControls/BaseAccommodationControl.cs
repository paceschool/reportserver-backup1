﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Accommodation;
using Pace.Common;


    /// <summary>
    /// Summary description for BaseAccommodationControl
    /// </summary>
public class BaseAccommodationControl : BaseControl
{
    public AccomodationEntities _entities { get; set; }

    public BaseAccommodationControl()
    {

    }

    public static AccomodationEntities CreateEntity
    {
        get
        {
            return new AccomodationEntities();
        }
    }

    public IList<Xlk_FamilyMemberType> LoadFamilyMemberTypes()
    {
        List<Xlk_FamilyMemberType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_FamilyMemberType>>("AllAccommodationFamilyMemberType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_FamilyMemberType>>("AllAccommodationFamilyMemberType", GetFamilyMemberType().ToList());
    }
    private IList<Xlk_FamilyMemberType> GetFamilyMemberType()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_FamilyMemberType> lookupQuery =
                from p in entity.Xlk_FamilyMemberType
                select p;
            return lookupQuery.ToList();
        }
    }


    public IList<Xlk_Status> LoadStatus()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllAccommodationStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllAccommodationStatus", GetStatus().ToList());
    }
    private IList<Xlk_Status> GetStatus()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from p in entity.Xlk_Status
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_PaymentMethod> LoadPaymentMethods()
    {
        List<Xlk_PaymentMethod> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_PaymentMethod>>("AllAccommodationPaymentMethod", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_PaymentMethod>>("AllAccommodationPaymentMethod", GetPaymentMethod().ToList());
    }
    private IList<Xlk_PaymentMethod> GetPaymentMethod()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_PaymentMethod> lookupQuery =
                from p in entity.Xlk_PaymentMethod
                select p;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_FamilyType> LoadFamilyTypes()
    {
        List<Xlk_FamilyType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_FamilyType>>("AllAccommodationFamilyType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_FamilyType>>("AllAccommodationFamilyType", GetFamilyType().ToList());
    }
    private IList<Xlk_FamilyType> GetFamilyType()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_FamilyType> lookupQuery =
                from p in entity.Xlk_FamilyType
                select p;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_Zone> LoadZones()
    {
        List<Xlk_Zone> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Zone>>("AllAccommodationZones", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Zone>>("AllAccommodationZones", GetZones().ToList());
    }
    private IList<Xlk_Zone> GetZones()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_Zone> lookupQuery =
                from p in entity.Xlk_Zone
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_NoteType> LoadNoteTypes()
    {
        List<Xlk_NoteType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_NoteType>>("AllAccommodationNoteType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_NoteType>>("AllAccommodationNoteType", GetNoteType().ToList());
    }
    private IList<Xlk_NoteType> GetNoteType()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_NoteType> lookupQuery =
                from p in entity.Xlk_NoteType
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_ComplaintType> LoadComplaintTypes()
    {
        List<Xlk_ComplaintType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ComplaintType>>("AllAccommodationComplaintType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ComplaintType>>("AllAccommodationComplaintType", GetComplaintType().ToList());
    }
    private IList<Xlk_ComplaintType> GetComplaintType()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_ComplaintType> lookupQuery =
                from p in entity.Xlk_ComplaintType
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Severity> LoadSeverity()
    {
        List<Xlk_Severity> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Severity>>("AllAccommodationSeverity", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Severity>>("AllAccommodationSeverity", GetSeveity().ToList());
    }
    private IList<Xlk_Severity> GetSeveity()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_Severity> lookupQuery =
                from p in entity.Xlk_Severity
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_AvailabilityType> LoadFamilyAvailabilityTypes()
    {
        List<Xlk_AvailabilityType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_AvailabilityType>>("AllAccommodationFamilyAvailabilityType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_AvailabilityType>>("AllAccommodationFamilyAvailabilityType", GetFamilyAvailabilityTypes().ToList());
    }
    private IList<Xlk_AvailabilityType> GetFamilyAvailabilityTypes()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_AvailabilityType> lookupQuery =
                from q in entity.Xlk_AvailabilityType
                select q;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_FamilyVisitType> LoadFamilyVisitTypes()
    {
        List<Xlk_FamilyVisitType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_FamilyVisitType>>("AllAccommodationFamilyVisitType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_FamilyVisitType>>("AllAccommodationFamilyVisitType", GetFamilyVisitTypes().ToList());
    }
    private IList<Xlk_FamilyVisitType> GetFamilyVisitTypes()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_FamilyVisitType> lookupQuery =
                from p in entity.Xlk_FamilyVisitType
                select p;
            return lookupQuery.ToList();
        }

    }

    public IList<Xlk_RoomType> LoadRoomTypes()
    {
        List<Xlk_RoomType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_RoomType>>("AllAccommodationRoomType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_RoomType>>("AllAccommodationRoomType", GetRoomTypes().ToList());
    }
    private IList<Xlk_RoomType> GetRoomTypes()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Xlk_RoomType> lookupQuery =
                from p in entity.Xlk_RoomType
                select p;
            return lookupQuery.ToList();
        }

    }

    public IList<Tag> LoadTagList()
    {
        List<Tag> values = null;

        if (CacheManager.Instance.GetFromCache<List<Tag>>("AllAccommodationTag", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Tag>>("AllAccommodationTag", GetTagList().ToList());
    }
    private IList<Tag> GetTagList()
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Tag> lookupQuery =
                from t in entity.Tag
                select t;

            return lookupQuery.ToList();
        }
    }
}

