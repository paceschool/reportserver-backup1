﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Pace.DataAccess.Group;
using Enrollments = Pace.DataAccess.Enrollment;
using Pace.Common;

/// <summary>
/// Summary description for BaseEnrollmentControl
/// </summary>
public class BaseGroupControl : BaseControl
{
    public GroupEntities _entities { get; set; }

    public BaseGroupControl()
    {
       
    }

    public static GroupEntities CreateEntity
    {
        get
        {
            return new GroupEntities();
        }
    }

    public IList<Xlk_ProductType> LoadProductTypes()
    {
        List<Xlk_ProductType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProductType>>("AllGroupProductType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProductType>>("AllGroupProductType", GetProductTypes().ToList());
    }
    private static IList<Xlk_PaymentMethod> GetPaymentMethods()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_PaymentMethod> lookupQuery =
                from b in entity.Xlk_PaymentMethod
                select b;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_PaymentMethod> LoadPaymentMethods()
    {
        IList<Xlk_PaymentMethod> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_PaymentMethod>>("AllGroupPaymentMethod", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_PaymentMethod>>("AllGroupPaymentMethod", GetPaymentMethods());
    }

    private IList<Xlk_ProductType> GetProductTypes()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_ProductType> lookupQuery =
                from p in entity.Xlk_ProductType
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_NoteType> LoadNoteTypes()
    {
        List<Xlk_NoteType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_NoteType>>("AllGroupNoteType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_NoteType>>("AllGroupNoteType", GetNoteType().ToList());
    }
    private IList<Xlk_NoteType> GetNoteType()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_NoteType> lookupQuery =
                from p in entity.Xlk_NoteType
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_SageDataType> LoadDataTypes()
    {
        List<Xlk_SageDataType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_SageDataType>>("AllGroupDataType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_SageDataType>>("AllGroupDataType", GetDataType().ToList());
    }

    private IList<Xlk_SageDataType> GetDataType()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_SageDataType> lookupQuery =
                from t in entity.Xlk_SageDataType
                select t;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Unit> LoadUnits()
    {
        IList<Xlk_Unit> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Unit>>("AllGroupUnits", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Unit>>("AllGroupUnits", GetUnits());
    }


    private IList<Xlk_Unit> GetUnits()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_Unit> lookupQuery =
                from b in entity.Xlk_Unit
                select b;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_ComplaintType> LoadComplaintTypes()
    {
        List<Xlk_ComplaintType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ComplaintType>>("AllGroupComplaintType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ComplaintType>>("AllGroupComplaintType", GetComplaintType().ToList());
    }
    private IList<Xlk_ComplaintType> GetComplaintType()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_ComplaintType> lookupQuery =
                from p in entity.Xlk_ComplaintType
                select p;
            return lookupQuery.ToList();
        }

    }
    public IList<Xlk_Severity> LoadSeverity()
    {
        List<Xlk_Severity> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Severity>>("AllGroupSeverity", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Severity>>("AllGroupSeverity", GetSeveity().ToList());
    }
    private IList<Xlk_Severity> GetSeveity()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_Severity> lookupQuery =
                from p in entity.Xlk_Severity
                select p;
            return lookupQuery.ToList();
        }

    }
    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllGroupProgrammeTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllGroupProgrammeTypes", GetProgrammeTypes().ToList());
    }

    private IList<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_ProgrammeType> lookupQuery =
                from b in entity.Xlk_ProgrammeType
                select b;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_LessonBlock> LoadLessonBlocks()
    {
        List<Xlk_LessonBlock> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_LessonBlock>>("AllGroupEnrollmentLessonBlocks", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_LessonBlock>>("AllGroupEnrollmentLessonBlocks", GetLessonBlocks().ToList());
    }

    private IList<Xlk_LessonBlock> GetLessonBlocks()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_LessonBlock> lookupQuery =
                from a in entity.Xlk_LessonBlock.Include("Xlk_CourseType")
                select a;
            return lookupQuery.ToList();
        }
    }


    protected string GetComplaintTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_ComplaintType item in LoadComplaintTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.ComplaintTypeId, item.Description);
        }

        return sb.ToString();

    }
    protected string GetSeverityList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_Severity item in LoadSeverity())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.SeverityId, item.Description);
        }

        return sb.ToString();

    }
    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.NoteTypeId, item.Description);
        }

        return sb.ToString();

    }
    public IList<Xlk_Location> LoadLocations()
    {
        List<Xlk_Location> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Location>>("AllGroupLocationTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Location>>("AllGroupLocationTypes", GetLocations().ToList());
    }

    private IList<Xlk_Location> GetLocations()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_Location> lookupQuery =
                from b in entity.Xlk_Location
                select b;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_TransferType> LoadTransferTypes()
    {
        List<Xlk_TransferType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_TransferType>>("AllGroupTransferTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_TransferType>>("AllGroupTransferTypes", GetTransferTypes().ToList());
    }

    private IList<Xlk_TransferType> GetTransferTypes()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_TransferType> lookupQuery =
                from b in entity.Xlk_TransferType
                select b;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_Nationality> LoadNationalities()
    {
        List<Xlk_Nationality> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Nationality>>("AllGroupNationalities", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Nationality>>("AllGroupNationalities", GetNationalities().ToList());
    }
    public IList<Xlk_Status> LoadStatuses()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllGroupStatuses", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllGroupStatuses", GetStatuses().ToList());
    }
    public IList<Agency> LoadAgencies()
    {
        List<Agency> values = null;

        if (CacheManager.Instance.GetFromCache<List<Agency>>("AllGroupAgency", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Agency>>("AllGroupAgency", GetAgencies().ToList());
    }
    public IList<Pace.DataAccess.Enrollment.Xlk_StudentType> LoadStudentTypes()
    {
        List<Pace.DataAccess.Enrollment.Xlk_StudentType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Pace.DataAccess.Enrollment.Xlk_StudentType>>("AllEnrollmentStudentType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Pace.DataAccess.Enrollment.Xlk_StudentType>>("AllEnrollmentStudentType", GetStudentType().ToList());
    }

    public IList<Pace.DataAccess.Enrollment.Xlk_Gender> LoadGender()
    {
        List<Pace.DataAccess.Enrollment.Xlk_Gender> values = null;

        if (CacheManager.Instance.GetFromCache<List<Pace.DataAccess.Enrollment.Xlk_Gender>>("AllGroupGender", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Pace.DataAccess.Enrollment.Xlk_Gender>>("AllGroupGender", GetGender().ToList());
    }
    private IList<Pace.DataAccess.Enrollment.Xlk_Gender> GetGender()
    {
        using (Enrollments.EnrollmentsEntities entity = new Enrollments.EnrollmentsEntities())
        {
            IQueryable<Pace.DataAccess.Enrollment.Xlk_Gender> lookupQuery =
                from e in entity.Xlk_Gender
                select e;
            return lookupQuery.ToList();
        }
    }


    private IList<Xlk_Nationality> GetNationalities()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_Nationality> lookupQuery =
                from a in entity.Xlk_Nationality
                select a;
            return lookupQuery.ToList();
        }
    }
    private IList<Xlk_Status> GetStatuses()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from a in entity.Xlk_Status
                select a;
            return lookupQuery.ToList();
        }
    }
    private IList<Agency> GetAgencies()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Agency> lookupQuery =
                from a in entity.Agency
                orderby a.Name ascending
                select a;
            return lookupQuery.ToList();
        }
    }
    private IList<Pace.DataAccess.Enrollment.Xlk_StudentType> GetStudentType()
    {
        using (Enrollments.EnrollmentsEntities entity = new Enrollments.EnrollmentsEntities())
        {
            IQueryable<Pace.DataAccess.Enrollment.Xlk_StudentType> lookupQuery =
                from a in entity.Xlk_StudentType
                select a;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_BusinessEntity> LoadBusinessEntity()
    {
        List<Xlk_BusinessEntity> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_BusinessEntity>>("AllGroupBusinessEntity", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_BusinessEntity>>("AllGroupBusinessEntity", GetBusinessEntity().ToList());
    }

    private IList<Xlk_BusinessEntity> GetBusinessEntity()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<Xlk_BusinessEntity> lookupQuery =
                from a in entity.Xlk_BusinessEntity
                select a;
            return lookupQuery.ToList();
        }
    }

    

}

