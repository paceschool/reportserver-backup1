﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Pace.DataAccess.Documents;
using Pace.Common;


/// <summary>
/// Summary description for BaseContactControl
/// </summary>
public class BaseDocumentControl : BaseControl
{
	public DocumentsEntities _entities {get; set; }

    public BaseDocumentControl()
	{

	}
    public static DocumentsEntities CreateEntity
    {
        get
        {
            return new DocumentsEntities();
        }
    }

    public IList<Xlk_Status> LoadStatus()
    {
        IList<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Status>>("AllXlk_Status", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Status>>("AllXlk_Status", GetStatus());
    }

    private IList<Xlk_Status> GetStatus()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from p in entity.Xlk_Status
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Type> LoadTypes()
    {
        IList<Xlk_Type> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Type>>("AllDocumentType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Type>>("AllDocumentType", GetTypes());
    }


    private IList<Xlk_Type> GetTypes()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_Type> lookupQuery =
                from p in entity.Xlk_Type
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Server> LoadServers()
    {
        IList<Server> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Server>>("AllDocumentServer", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Server>>("AllDocumentServer", GetServers());
    }


    private IList<Server> GetServers()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Server> lookupQuery =
                from p in entity.Server
                select p;
            return lookupQuery.ToList();
        }
    }


    public IList<Xlk_FileType> LoadFileTypes()
    {
        IList<Xlk_FileType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_FileType>>("AllDocumentFileType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_FileType>>("AllDocumentFileType", GetFileTypes());
    }


    private IList<Xlk_FileType> GetFileTypes()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_FileType> lookupQuery =
                from p in entity.Xlk_FileType
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Category> LoadCategories()
    {
        IList<Xlk_Category> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Xlk_Category>>("AllDocumentCategories", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Xlk_Category>>("AllDocumentCategories", GetCategories());
    }


    private IList<Xlk_Category> GetCategories()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_Category> lookupQuery =
                from s in entity.Xlk_Category
                select s;
            return lookupQuery.ToList();
        }
    }
}
