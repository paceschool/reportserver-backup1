﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Security;
using System.Text;
using System.Configuration;
using System.Globalization;
using Pace.Common;

/// <summary>
/// Summary description for BaseControl
/// </summary>
public abstract class BaseControl : System.Web.UI.UserControl
{
    public Dictionary<string, object> Parameters;

	public BaseControl()
	{
        Parameters = new Dictionary<string, object>();
	}

    protected virtual T GetValue<T>(string key)
    {
        object value = null;

        if (Parameters.ContainsKey(key))
            value = Parameters[key];

        var t = typeof(T);

        if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
        {
            if (value == null || value.ToString() == "null")
            {
                return default(T);
            }

            t = Nullable.GetUnderlyingType(t); ;
        }

        return (T)Convert.ChangeType(value, t);

    }

    public static Int16 GetCampusId
    {
        get
        {
            Int16 _campusId = Pace.Common.Session.Manager.Get<Int16>("CampusId");
            if (_campusId == default(Int16))
            {
                _campusId = (from b in BasePage.LoadCampus where b.IsDefault select b.CampusId).FirstOrDefault();
                SetCampusId(_campusId);
            }
            return _campusId;
        }
    }

    public static string SetCampusId(short campusid)
    {
        try
        {
            Pace.Common.Session.Manager.Set<Int16>("CampusId", campusid);
            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    public static string GetComputerName(string remoteaddress)
    {

        var computer_name = System.Net.Dns.GetHostEntry(remoteaddress).HostName.Split('.');

        if (computer_name.Length > 0)
            return computer_name[0].ToString();

        return string.Empty;

    }

    protected string ToVirtual(string path, string method)
    {
        return string.Format("{0}/{1}", VirtualPathUtility.ToAbsolute(path), method);
    }

    protected string GetUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32 _currentUser = GetCurrentUser().UserId;


        foreach (Pace.DataAccess.Security.Users user in LoadUserList())
        {
            sb.AppendFormat("<option value={0} {2}>{1}</option>", user.UserId, user.Name, (user.UserId == _currentUser)?"selected":string.Empty);
        }

        return sb.ToString();

    }

    public static SecurityEntities SecurityEntities
    {
        get
        {
            SecurityEntities value;

            if (CacheManager.Instance.GetFromCache<SecurityEntities>("SecurityObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<SecurityEntities>("SecurityObject", new SecurityEntities());
        }
    }
    public List<Users> LoadUserList()
    {
        List<Users> values = null;

        if (CacheManager.Instance.GetFromCache<List<Users>>("AllUsers", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Users>>("AllUsers", GetSecurityUserList().ToList());
    }

    private IQueryable<Users> GetSecurityUserList()
    {
        IQueryable<Users> lookupQuery =
            from p in SecurityEntities.Users
            select p;
        return lookupQuery;

    }

    protected static string ReportServerURL
    {
        get
        {
            return ConfigurationManager.AppSettings["ReportServerURL"];
        }
    }
    public static string TuitionDays(string idtag,int? bitstring,string checkboxclassname)
    {
        StringBuilder sb = new StringBuilder();

        foreach (var item in Enum.GetValues(typeof(TuitionDays)).Cast<TuitionDays>())
        {
            sb.AppendFormat("<label style=\"display:block;\" for=\"{4}{2}\"><input id=\"{4}{2}\" class=\"{5}\" type=\"checkbox\" value=\"{3}\" style=\"width:15px;float:left;\" {0}></input>{1}</label>", (bitstring.HasValue) ? ((TuitionDays)Enum.ToObject(typeof(TuitionDays), bitstring.Value)).HasFlag(item) ? "checked" : string.Empty : string.Empty, Enum.GetName(typeof(TuitionDays), item), item, (int)item, idtag, checkboxclassname);
        }

        return sb.ToString();
    }

    protected static string CreateName(object firstName, object secondName)
    {
        if (firstName != null && secondName != null)
        {
            return string.Format("{0} {1}", firstName, secondName);
        }
        else
        {
            return (firstName != null) ? firstName.ToString() : secondName.ToString();
        }
    }

    public static DateTime FirstDayOfWeek(DateTime date, CalendarWeekRule rule)
    {

        int weeknumber = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, rule, DayOfWeek.Monday);

        DateTime jan1 = new DateTime(date.Year, 1, 1);

        int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        DateTime firstMonday = jan1.AddDays(daysOffset);

        var cal = CultureInfo.CurrentCulture.Calendar;
        int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

        if (firstWeek <= 1)
        {
            weeknumber -= 1;
        }

        DateTime result = firstMonday.AddDays((weeknumber) * 7);

        return result;
    }

    protected static Users GetCurrentUser()
    {
        System.Security.Principal.WindowsIdentity p = System.Security.Principal.WindowsIdentity.GetCurrent();
        if (p != null)
        {
            using (Pace.DataAccess.Security.SecurityEntities entity = new SecurityEntities())
            {
                return (from s in entity.Users where s.DomainUserName == p.Name select s).FirstOrDefault();
            }
        }
        return null;

    }

}
