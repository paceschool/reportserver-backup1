﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Support;
using Pace.Common;

/// <summary>
/// Summary description for BaseSupportControl
/// </summary>
public class BaseSupportControl : BaseControl
{
    public SupportEntities _entities { get; set; }
    
    public BaseSupportControl()
	{
		
	}

    public static SupportEntities CreateEntity
    {
        get
        {
            return  new SupportEntities();
        }
    }

    public IList<Xlk_Priority> LoadPriority()
    {
        List<Xlk_Priority> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Priority>>("AllTaskPriorityTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Priority>>("AllTaskPriorityTypes", GetPriorityType().ToList());
    }
    private IList<Xlk_Priority> GetPriorityType()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Xlk_Priority> lookupQuery =
                from p in entity.Xlk_Priority
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Status> LoadStatus()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllTaskStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllTaskStatus", GetStatus().ToList());
    }
    private IList<Xlk_Status> GetStatus()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from s in entity.Xlk_Status
                select s;
            return lookupQuery.ToList();
        }
    }

    public IList<Users> LoadUsers()
    {
        List<Users> values = null;

        if (CacheManager.Instance.GetFromCache<List<Users>>("AllTaskUsers", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Users>>("AllTaskUsers", GetUsers().ToList());
    }
    private IList<Users> GetUsers()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Users> lookupQuery =
                from u in entity.Users
                select u;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_UseDate> LoadDateUsed()
    {
        List<Xlk_UseDate> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_UseDate>>("AllGetUseDate", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_UseDate>>("AllGetUseDate", GetUseDate().ToList());
    }
    private IList<Xlk_UseDate> GetUseDate()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Xlk_UseDate> lookupQuery =
                from u in entity.Xlk_UseDate
                select u;
            return lookupQuery.ToList();
        }
    }
}