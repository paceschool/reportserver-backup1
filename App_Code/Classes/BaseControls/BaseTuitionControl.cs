﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Tuition;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Text;
using Pace.Common;

/// <summary>
/// Summary description for BaseTuitionPage
/// </summary>
public partial class BaseTuitionControl : BaseControl
{
    public TuitionEntities _entities { get; set; }

    public BaseTuitionControl()
	{
    }

    public static IList<Xlk_Building> LoadBuildings()
    {
        List<Xlk_Building> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Building>>("AllBuilding", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Building>>("AllBuilding", GetBuildings().ToList());
    }

    private static IList<Xlk_Building> GetBuildings()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Xlk_Building> lookupQuery =
                from c in entity.Xlk_Building
                select c;
            return lookupQuery.ToList();
        }
    }

    #region Events

    public static TuitionEntities CreateEntity
    {
        get
        {
            return new TuitionEntities();
        }
    }

    public IList<TuitionLevel> LoadTuitionLevels()
    {
        List<TuitionLevel> values = null;

        if (CacheManager.Instance.GetFromCache<List<TuitionLevel>>("AllTuitionLevel", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<TuitionLevel>>("AllTuitionLevel", GetTuitionLevels().ToList());
    }
    private IList<TuitionLevel> GetTuitionLevels()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<TuitionLevel> lookupQuery =
                from l in entity.TuitionLevel
                select l;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_CourseType> LoadCourseTypes()
    {
        List<Xlk_CourseType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_CourseType>>("AllTuitionCourseType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_CourseType>>("AllTuitionCourseType", GetCourseTypes().ToList());
    }
    private IList<Xlk_CourseType> GetCourseTypes()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Xlk_CourseType> lookupQuery =
                from p in entity.Xlk_CourseType.Include("Xlk_ProgrammeType")
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllTuitionProgrammeType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllTuitionProgrammeType", GetProgrammeTypes().ToList());
    }
    private IList<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Xlk_ProgrammeType> lookupQuery =
                from p in entity.Xlk_ProgrammeType
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Teacher> LoadTeachers()
    {
        List<Teacher> values = null;

        if (CacheManager.Instance.GetFromCache<List<Teacher>>("AllTeacher", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Teacher>>("AllTeacher", GetTeachers().ToList());
    }
    private IList<Teacher> GetTeachers()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Teacher> lookupQuery =
                from t in entity.Teacher
                select t;
            return lookupQuery.ToList();
        }
    }

    public static IList<Xlk_ExamType> LoadExamTypes()
    {
        IList<Xlk_ExamType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_ExamType>>("AllExamType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_ExamType>>("AllExamType", GetExamTypes());
    }

    private static IList<Xlk_ExamType> GetExamTypes()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Xlk_ExamType> lookupQuery =
                from et in entity.Xlk_ExamType
                select et;
            return lookupQuery.ToList();
        }
    }

    public static IList<Exam> LoadExams()
    {
        List<Exam> values = null;

        if (CacheManager.Instance.GetFromCache<List<Exam>>("AllExam", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Exam>>("AllExam", GetExams().ToList());
    }

    private static IList<Exam> GetExams()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Exam> lookupQuery =
                from ex in entity.Exam
                select ex;
            return lookupQuery.ToList();
        }
    }

    public static IList<ExamDates> LoadExamDates()
    {
        List<ExamDates> values = null;

        if (CacheManager.Instance.GetFromCache<List<ExamDates>>("AllExamDates", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<ExamDates>>("allExamDates", GetExamDates().ToList());
    }
    private static IList<ExamDates> GetExamDates()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<ExamDates> lookupQuery =
                from e in entity.ExamDates.Include("Exam")
                orderby e.ExamDate ascending
                select e;
            return lookupQuery.ToList();
        }
    }

    public IList<Student> LoadStudents()
    {
        List<Student> values = null;

        if (CacheManager.Instance.GetFromCache<List<Student>>("AllStudent", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Student>>("AllStudent", GetStudents().ToList());
    }
    private IList<Student> GetStudents()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Student> lookupQuery =
                from s in entity.Student
                where s.GroupId == null && s.DepartureDate > DateTime.Now
                orderby s.FirstName, s.SurName
                select s;
            return lookupQuery.ToList();
        }
    }

    public IList<Student> LoadPastStudents()
    {
        List<Student> values = null;

        if (CacheManager.Instance.GetFromCache<List<Student>>("AllStudent", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Student>>("AllStudent", GetPastStudents().ToList());
    }
    private IList<Student> GetPastStudents()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Student> lookupQuery =
                from s in entity.Student
                where s.GroupId == null && s.DepartureDate < DateTime.Now
                orderby s.FirstName, s.SurName
                select s;
            return lookupQuery.ToList();
        }
    }

    public IList<ClassRoom> LoadClassRooms()
    {
        List<ClassRoom> values = null;

        if (CacheManager.Instance.GetFromCache<List<ClassRoom>>("AllClassRoom", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<ClassRoom>>("AllClassRoom", GetClassRooms().ToList());
    }
    private IList<ClassRoom> GetClassRooms()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<ClassRoom> lookupQuery =
                from c in entity.ClassRoom
                select c;
            return lookupQuery.ToList();
        }
    }
    #endregion
}
