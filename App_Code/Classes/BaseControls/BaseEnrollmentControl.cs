﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Enrollment;
using System.Text;
using Pace.Common;
using Pace.DataAccess.Security;

/// <summary>
/// Summary description for BaseEnrollmentControl
/// </summary>
public class BaseEnrollmentControl : BaseControl
{
    public EnrollmentsEntities _entities { get; set; }

    public BaseEnrollmentControl()
    {
       
    }

    public static EnrollmentsEntities CreateEntity
    {
        get
        {
            return new EnrollmentsEntities();
        }
    }

    public IList<Xlk_NoteType> LoadNoteTypes()
    {
        List<Xlk_NoteType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_NoteType>>("AllEnrollmentNoteType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_NoteType>>("AllEnrollmentNoteType", GetNoteType().ToList());
    }
    private IList<Xlk_NoteType> GetNoteType()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_NoteType> lookupQuery =
                from p in entity.Xlk_NoteType
                select p;
            return lookupQuery.ToList();
        }

    }
    public IList<Xlk_ComplaintType> LoadComplaintTypes()
    {
        List<Xlk_ComplaintType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ComplaintType>>("AllEnrollmentComplaintType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ComplaintType>>("AllEnrollmentComplaintType", GetComplaintType().ToList());
    }

    public static IList<Xlk_Nationality> LoadNationality()
    {
        List<Xlk_Nationality> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Nationality>>("EnrollmentNationality", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Nationality>>("EnrollmentNationality", GetNationality().ToList());
    }

    private static IList<Xlk_Nationality> GetNationality()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_Nationality> lookupQuery =
                from n in entity.Xlk_Nationality
                orderby n.Description ascending
                select n;
            return lookupQuery.ToList();
        }
    }

    public static IList<Exam> LoadExam()
    {
        List<Exam> values = null;

        if (CacheManager.Instance.GetFromCache<List<Exam>>("EnrollmentExam", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Exam>>("EnrollmentExam", GetExam().ToList());
    }

    private static IList<Exam> GetExam()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Exam> lookupQuery =
                from e in entity.Exam
                select e;
            return lookupQuery.ToList();
        }
    }

    public static IList<Agency> LoadAgency()
    {
        List<Agency> values = null;

        if (CacheManager.Instance.GetFromCache<List<Agency>>("EnrollmentAgency", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Agency>>("EnrollmentAgency", GetAgency().ToList());
    }

    private static IList<Agency> GetAgency()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Agency> lookupQuery =
                from a in entity.Agency
                orderby a.Name
                select a;
            return lookupQuery.ToList();
        }
    }

    public static IList<Xlk_Status> LoadStatus()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("EnrollmentStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("EnrollmentStatus", GetStatus().ToList());
    }

    private static IList<Xlk_Status> GetStatus()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_Status> lookupQuery = 
                from s in entity.Xlk_Status
                select s;
            return lookupQuery.ToList();
        }
    }

    public static IList<Xlk_StudentType> LoadStudentType()
    {
        List<Xlk_StudentType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_StudentType>>("EnrollmentStudentType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_StudentType>>("EnrollmentStudentType", GetStudentType().ToList());
    }

    private static IList<Xlk_StudentType> GetStudentType()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_StudentType> lookupQuery =
                from s in entity.Xlk_StudentType
                select s;
            return lookupQuery.ToList();
        }
    }

    public static IList<Campus> LoadCampus()
    {
        List<Campus> values = null;

        if (CacheManager.Instance.GetFromCache<List<Campus>>("EnrollmentCampus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Campus>>("EnrollmentCampus", GetCampus().ToList());
    }

    private static IList<Campus> GetCampus()
    {
        using (SecurityEntities entity = new SecurityEntities())
        {
            IQueryable<Campus> lookupQuery =
                from c in entity.Campuses
                select c;
            return lookupQuery.ToList();
        }
    }

    public static IList<Xlk_Gender> LoadGender()
    {
        List<Xlk_Gender> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Gender>>("EnrollmentGender", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Gender>>("EnrollmentGender", GetGender().ToList());
    }

    private static IList<Xlk_Gender> GetGender()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_Gender> lookupQuery =
                from g in entity.Xlk_Gender
                select g;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Category> LoadCategories()
    {
        List<Xlk_Category> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Category>>("EnrollmentEnrollmentCategory", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Category>>("EnrollmentEnrollmentCategory", GetCategoryies().ToList());
    }
    private IList<Xlk_Category> GetCategoryies()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_Category> lookupQuery =
                from p in entity.Xlk_Category
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_ProductType> LoadProductTypes()
    {
        List<Xlk_ProductType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProductType>>("AllEnrollmentProductType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProductType>>("AllEnrollmentProductType", GetProductTypes().ToList());
    }
    private static IList<Xlk_PaymentMethod> GetPaymentMethods()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_PaymentMethod> lookupQuery =
                from b in entity.Xlk_PaymentMethod
                select b;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_PaymentMethod> LoadPaymentMethods()
    {
        IList<Xlk_PaymentMethod> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_PaymentMethod>>("AllEnrollmentPaymentMethod", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_PaymentMethod>>("AllEnrollmentPaymentMethod", GetPaymentMethods());
    }

    private IList<Xlk_ProductType> GetProductTypes()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_ProductType> lookupQuery =
                from p in entity.Xlk_ProductType
                select p;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_ComplaintType> GetComplaintType()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_ComplaintType> lookupQuery =
                from p in entity.Xlk_ComplaintType
                select p;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_Severity> LoadSeverity()
    {
        List<Xlk_Severity> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Severity>>("AllEnrollmentSeverity", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Severity>>("AllEnrollmentSeverity", GetSeveity().ToList());
    }
    private IList<Xlk_Severity> GetSeveity()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_Severity> lookupQuery =
                from p in entity.Xlk_Severity
                select p;
            return lookupQuery.ToList();
        }

    }
    public static IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllEnrollmentProgrammeTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllEnrollmentProgrammeTypes", GetProgrammeTypes().ToList());
    }

    public static IList<Xlk_CourseType> LoadCourseTypes()
    {
        List<Xlk_CourseType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_CourseType>>("EnrollmentCourseType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_CourseType>>("EnrollmentCourseType", GetCourseTypes().ToList());
    }

    public IList<Xlk_Unit> LoadUnits()
    {
        IList<Xlk_Unit> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Unit>>("AllEnrollmentUnits", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Unit>>("AllEnrollmentUnits", GetUnits());
    }


    private IList<Xlk_Unit> GetUnits()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_Unit> lookupQuery =
                from b in entity.Xlk_Unit
                select b;
            return lookupQuery.ToList();
        }
    }

    private static IList<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_ProgrammeType> lookupQuery =
                from b in entity.Xlk_ProgrammeType
                select b;
            return lookupQuery.ToList();
        }
    }

    private static IList<Xlk_CourseType> GetCourseTypes()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_CourseType> lookupQuery = 
            from c in entity.Xlk_CourseType.Include("Xlk_ProgrammeType")
            select c;
        return lookupQuery.ToList();
    }
    }

    public IList<Xlk_Location> LoadLocations()
    {
        List<Xlk_Location> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Location>>("AllEnrollmentLocationTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Location>>("AllEnrollmentLocationTypes", GetLocations().ToList());
    }

    private IList<Xlk_Location> GetLocations()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_Location> lookupQuery =
            from b in entity.Xlk_Location
            select b;
        return lookupQuery.ToList();
    }
    }
    public IList<Xlk_TransferType> LoadTransferTypes()
    {
        List<Xlk_TransferType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_TransferType>>("AllEnrollmentTransferTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_TransferType>>("AllEnrollmentTransferTypes", GetTransferTypes().ToList());
    }

    private IList<Xlk_TransferType> GetTransferTypes()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_TransferType> lookupQuery =
            from b in entity.Xlk_TransferType
            select b;
        return lookupQuery.ToList();
    }
    }
    public IList<Xlk_LessonBlock> LoadLessonBlocks()
    {
        List<Xlk_LessonBlock> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_LessonBlock>>("AllEnrollmentLessonBlocks", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_LessonBlock>>("AllEnrollmentLessonBlocks", GetLessonBlocks().ToList());
    }

    private IList<Xlk_LessonBlock> GetLessonBlocks()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_LessonBlock> lookupQuery =
            from a in entity.Xlk_LessonBlock.Include("Xlk_CourseType")
            select a;
        return lookupQuery.ToList();
    }
    }

    public IList<Xlk_BusinessEntity> LoadBusinessEntity()
    {
        List<Xlk_BusinessEntity> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_BusinessEntity>>("AllEnrollmentBusinessEntity", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_BusinessEntity>>("AllEnrollmentBusinessEntity", GetBusinessEntity().ToList());
    }

    private IList<Xlk_BusinessEntity> GetBusinessEntity()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_BusinessEntity> lookupQuery =
            from a in entity.Xlk_BusinessEntity
            select a;
        return lookupQuery.ToList();
    }
    }

    public IList<Xlk_BusTicketType> LoadBusTicketType()
    {
        List<Xlk_BusTicketType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_BusTicketType>>("EnrollmentBusTicketType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_BusTicketType>>("EnrollmentBusTicketType", GetBusTicketType().ToList());
    }

    private IList<Xlk_BusTicketType> GetBusTicketType()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_BusTicketType> lookupQuery =
            from b in entity.Xlk_BusTicketType
            select b;
        return lookupQuery.ToList();
    }
    }




    public IList<Xlk_CourseBooks> LoadCourseBooks()
    {
        List<Xlk_CourseBooks> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_CourseBooks>>("EnrollmentCourseBook", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_CourseBooks>>("EnrollmentCourseBook", GetCourseBooks().ToList());
    }

    private IList<Xlk_CourseBooks> GetCourseBooks()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_CourseBooks> lookupQuery =
            from b in entity.Xlk_CourseBooks
            select b;
        return lookupQuery.ToList();
    }
    }

    protected IList<Xlk_CourseBookPaymentType> LoadCourseBookPaymentTypes()
    {
        List<Xlk_CourseBookPaymentType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_CourseBookPaymentType>>("EnrollmentCourseBookPaymentType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_CourseBookPaymentType>>("EnrollmentCourseBookPaymentType", GetCourseBookPaymentTypes().ToList());
    }

    private IList<Xlk_CourseBookPaymentType> GetCourseBookPaymentTypes()
    {        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
        IQueryable<Xlk_CourseBookPaymentType> lookupQuery =
            from t in entity.Xlk_CourseBookPaymentType
            select t;
        return lookupQuery.ToList();
    }
    }

    protected string GetComplaintTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_ComplaintType item in LoadComplaintTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.ComplaintTypeId, item.Description);
        }

        return sb.ToString();

    }

    protected string GetSeverityList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_Severity item in LoadSeverity())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.SeverityId, item.Description);
        }

        return sb.ToString();
    }

    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.NoteTypeId, item.Description);
        }

        return sb.ToString();

    }

    public IList<Xlk_SageDataType> LoadDataTypes()
    {
        List<Xlk_SageDataType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_SageDataType>>("AllStudentDataType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_SageDataType>>("AllStudentDataType", GetDataType().ToList());
    }

    private IList<Xlk_SageDataType> GetDataType()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_SageDataType> lookupQuery =
                from t in entity.Xlk_SageDataType
                select t;
            return lookupQuery.ToList();
        }
    }
}

