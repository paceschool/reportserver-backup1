﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Pace.DataAccess.Content;
using Pace.Common;

/// <summary>
/// Summary description for BaseContactControl
/// </summary>
public class BaseWebContentControl : BaseControl
{
	public ContentEntities _entities {get; set; }

    public BaseWebContentControl()
	{

	}
    public static ContentEntities CreateEntity
    {
        get
        {
            return new ContentEntities();
        }
    }

    public IList<Pace.DataAccess.Content.Xlk_AttributeType> LoadAttributeTypes()
    {
        IList<Pace.DataAccess.Content.Xlk_AttributeType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Content.Xlk_AttributeType>>("AllWebContentAttributeType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Content.Xlk_AttributeType>>("AllWebContentAttributeType", GetAttributeTypes());
    }


    public IList<Pace.DataAccess.Content.Xlk_ElementType> LoadElementTypes()
    {
        IList<Pace.DataAccess.Content.Xlk_ElementType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Content.Xlk_ElementType>>("AllWebContentElementType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Content.Xlk_ElementType>>("AllWebContentElementType", GetElementTypes());
    }


    public IList<Pace.DataAccess.Content.Xlk_LinkType> LoadLinkTypes()
    {
        IList<Pace.DataAccess.Content.Xlk_LinkType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Content.Xlk_LinkType>>("AllWebContentLinkType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Content.Xlk_LinkType>>("AllWebContentLinkType", GetLinkTypes());
    }


    public static IList<Xlk_LanguageCode> LoadLanguageCodes()
    {
        List<Xlk_LanguageCode> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_LanguageCode>>("AllWebContentLanguageCode", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_LanguageCode>>("AllWebContentLanguageCode", GetLanguageCodes().ToList());
    }

    private static IList<Xlk_LanguageCode> GetLanguageCodes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_LanguageCode> lookupQuery =
                from c in entity.Xlk_LanguageCode
                select c;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_AttributeType> GetAttributeTypes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_AttributeType> lookupQuery =
                from a in entity.Xlk_AttributeType
                select a;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_ElementType> GetElementTypes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_ElementType> lookupQuery =
                from a in entity.Xlk_ElementType
                select a;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_LinkType> GetLinkTypes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_LinkType> lookupQuery =
                from b in entity.Xlk_LinkType
                select b;
            return lookupQuery.ToList();
        }
    }
}
