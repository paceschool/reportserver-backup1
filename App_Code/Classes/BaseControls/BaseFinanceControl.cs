﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Finance;
using Pace.Common;
using System.Text;


    /// <summary>
    /// Summary description for BaseFinanceControl
    /// </summary>
public class BaseFinanceControl : BaseControl
{
    public FinanceEntities _entities { get; set; }

    public BaseFinanceControl()
    {

    }

    public static FinanceEntities CreateEntity
    {
        get
        {
            return new FinanceEntities();
        }
    }

}

