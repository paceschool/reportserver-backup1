﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Transport;
using System.Text;
using Pace.Common;

/// <summary>
/// Summary description for BaseTransportControl
/// </summary>
public class BaseTransportControl : BaseControl
{
    public TransportEntities _entities { get; set; }
    
    public BaseTransportControl()
	{
		
	}

    public static TransportEntities CreateEntity
    {
        get
        {
            return  new TransportEntities();
        }
    }

    protected IList<Xlk_TripType> LoadTripTypes()
    {
        List<Xlk_TripType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_TripType>>("AllTripType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_TripType>>("AllTripType", GetTripTypes().ToList());
    }

    private IList<Xlk_TripType> GetTripTypes()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_TripType> lookupQuery =
                from s in entity.Xlk_TripType
                select s;
            return lookupQuery.ToList();
        }
    }
    
    protected IList<Xlk_Status> LoadStatusTypes()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllStatusType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllStatusType", GetStatusType().ToList());
    }

    protected IList<VehiclePricePlan> LoadVehiclePricePlans()
    {
        List<VehiclePricePlan> values = null;

        if (CacheManager.Instance.GetFromCache<List<VehiclePricePlan>>("AllPricePlan", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<VehiclePricePlan>>("AllPricePlan", GetVehiclePricePlans().ToList());
    }

    protected IList<Vehicle> LoadVehicles()
    {
        List<Vehicle> values = null;

        if (CacheManager.Instance.GetFromCache<List<Vehicle>>("AllVehicle", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Vehicle>>("AllVehicle", GetVehicleList().ToList());
    }

    protected IList<User> LoadUsers()
    {
        List<User> values = null;

        if (CacheManager.Instance.GetFromCache<List<User>>("AllUser", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<User>>("AllUser", GetUsers().ToList());
    }

    protected IList<Xlk_Location> LoadLocations()
    {
        List<Xlk_Location> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Location>>("AllLocations", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Location>>("AllLocations", GetLocations().ToList());
    }

    private IList<Xlk_Location> GetLocations()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_Location> lookupQuery =
                from l in entity.Xlk_Location
                select l;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_Status> GetStatusType()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from s in entity.Xlk_Status
                select s;
            return lookupQuery.ToList();
        }
    }

    private IList<User> GetUsers()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<User> lookupQuery =
                from u in entity.Users
                select u;
            return lookupQuery.ToList();
        }
    }

    private IList<VehiclePricePlan> GetVehiclePricePlans()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<VehiclePricePlan> lookupQuery =
                from v in entity.VehiclePricePlans
                select v;
            return lookupQuery.ToList();
        }
    }

    private IList<Vehicle> GetVehicleList()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Vehicle> lookupQuery =
                from vl in entity.Vehicles
                select vl;
            return lookupQuery.ToList();
        }
    }
}