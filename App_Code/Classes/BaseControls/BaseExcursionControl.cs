﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Pace.DataAccess.Excursion;
using Pace.Common;

/// <summary>
/// Summary description for BaseExcursionControl
/// </summary>
public class BaseExcursionControl : BaseControl
{
	public ExcursionEntities _entities {get; set; }
    
    public BaseExcursionControl()
	{

	}
    public static ExcursionEntities CreateEntity
    {
        get
        {
            return new ExcursionEntities();
        }
    }

    public IList<Booking> LoadGroupExcursions()
    {
        List<Booking> values = null;

        if (CacheManager.Instance.GetFromCache<List<Booking>>("AllGroupExcursion", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Booking>>("AllGroupExcursion", GetGroupExcursions().ToList());
    }

    private IQueryable<Booking> GetGroupExcursions()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Booking> lookupQuery =
                from x in entity.Bookings
                select x;

            return lookupQuery;
        }

    }

    public IList<Xlk_ExcursionType> LoadExcursionTypes()
    {
        IList<Xlk_ExcursionType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_ExcursionType>>("All_ExcursionType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_ExcursionType>>("All_ExcursionType", GetExcursionType());
    }

    private IList<Xlk_ExcursionType> GetExcursionType()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_ExcursionType> lookupQuery =
                 from p in entity.Xlk_ExcursionType
                 select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Excursion> LoadExcursions()
    {
        IList<Excursion> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Excursion>>("AllExcursions", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Excursion>>("AllExcursions", GetExcursions());
    }

    private IList<Excursion> GetExcursions()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Excursion> lookupQuery =
                from p in entity.Excursions
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Status> LoadStatus()
    {
        IList<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Status>>("AllXlk_Status", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Status>>("AllXlk_Status", GetStatus());
    }

    private IList<Xlk_Status> GetStatus()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from p in entity.Xlk_Status
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_TransportType> LoadTransportTypes()
    {
        IList<Xlk_TransportType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_TransportType>>("AllExcursionTransportType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_TransportType>>("AllExcursionTransportType", GetTransportTypes());
    }

    public IList<Xlk_AttendeeType> LoadAttendeeTypes()
    {
        IList<Xlk_AttendeeType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_AttendeeType>>("AllExcursionAttendeeType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_AttendeeType>>("AllExcursionAttendeeType", GetAttendeeTypes());
    }


    private IList<Xlk_AttendeeType> GetAttendeeTypes()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_AttendeeType> lookupQuery =
                from p in entity.Xlk_AttendeeType
                select p;
            return lookupQuery.ToList();
        }
    }
    private IList<Xlk_TransportType> GetTransportTypes()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_TransportType> lookupQuery =
                from p in entity.Xlk_TransportType
                select p;
            return lookupQuery.ToList();
        }
    }
    public IList<Xlk_ContactMethod> LoadContactMethods()
    {
        IList<Pace.DataAccess.Excursion.Xlk_ContactMethod> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Xlk_ContactMethod>>("AllExcursionContactMethods", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Xlk_ContactMethod>>("AllExcursionContactMethods", GetContactMethods());
    }

    public IList<Pace.DataAccess.Excursion.Supplier> LoadSuppliers()
    {
        IList<Pace.DataAccess.Excursion.Supplier> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Excursion.Supplier>>("AllExcursionSuppliers", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Excursion.Supplier>>("AllExcursionSuppliers", GetSuppliers());
    }

    private IList<Xlk_Location> GetLocations()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_Location> lookupQuery =
                from s in entity.Xlk_Location
                orderby s.Description ascending
                select s;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Location> LoadLocations()
    {
        IList<Pace.DataAccess.Excursion.Xlk_Location> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Xlk_Location>>("AllExcursionXlk_Location", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Xlk_Location>>("AllExcursionXlk_Location", GetLocations());
    }

    private IList<Xlk_ContactMethod> GetContactMethods()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_ContactMethod> lookupQuery =
                from s in entity.Xlk_ContactMethod
                select s;
            return lookupQuery.ToList();
        }
    }


    private IList<Supplier> GetSuppliers()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Supplier> lookupQuery =
                from p in entity.Suppliers
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Pace.DataAccess.Excursion.Xlk_Status> LoadExcursionStatus()
    {
        IList<Pace.DataAccess.Excursion.Xlk_Status> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Excursion.Xlk_Status>>("AllExcursionStatus", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Excursion.Xlk_Status>>("AllExcursionStatus", GetExcursionStatus());
    }


    private IList<Pace.DataAccess.Excursion.Xlk_Status> GetExcursionStatus()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Pace.DataAccess.Excursion.Xlk_Status> lookupQuery =
                from s in entity.Xlk_Status
                select s;
            return lookupQuery.ToList();
        }
    }
}
