﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Pace.DataAccess.Contacts;
using Pace.Common;

/// <summary>
/// Summary description for BaseContactControl
/// </summary>
public class BaseContactControl : BaseControl
{
    public ContactsEntities _entities { get; set; }
    
    public BaseContactControl()
	{

	}
    public static ContactsEntities CreateEntity
    {
        get
        {
            return new ContactsEntities();
        }
    }

    public IList<Xlk_Status> LoadStatus()
    {
        IList<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Status>>("AllContactsEntitiesXlk_Status", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Status>>("AllContactsEntitiesXlk_Status", GetStatus());
    }

    private IList<Xlk_Status> GetStatus()
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from p in entity.Xlk_Status
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_Type> LoadTypes()
    {
        IList<Xlk_Type> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Type>>("AllContactsEntitiesContactType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Type>>("AllContactsEntitiesContactType", GetTypes());
    }


    private IList<Xlk_Type> GetTypes()
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Xlk_Type> lookupQuery =
                from p in entity.Xlk_Type.Include("Xlk_Category")
                select p;
            return lookupQuery.ToList();
        }
    }


    public IList<Xlk_Category> LoadCategories()
    {
        IList<Xlk_Category> values = null;
        if (CacheManager.Instance.GetFromCache<IList<Xlk_Category>>("AllContactsEntitiesContactCategories", out values))
            return values;
        return CacheManager.Instance.AddToCache<IList<Xlk_Category>>("AllContactsEntitiesContactCategories", GetCategories());
    }


    private IList<Xlk_Category> GetCategories()
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Xlk_Category> lookupQuery =
                from s in entity.Xlk_Category
                select s;
            return lookupQuery.ToList();
        }
    }
}
