﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

using Pace.DataAccess.Transport;


public partial class Transport_AddNewPricePlanControl : BaseTransportControl
{
    protected VehiclePricePlan _vehiclePricePlan;
    protected Int32? _transportSupplierId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("VehiclePricePlanId").HasValue)
        {
            using (TransportEntities entity = new TransportEntities())
            {
                Int32 vehiclePricePlanId = GetValue<Int32>("VehiclePricePlanId");

                _vehiclePricePlan = (from priceplan in entity.VehiclePricePlans.Include("Xlk_TripType").Include("Vehicle").Include("Vehicle.Xlk_VehicleType").Include("PickupLocation").Include("DropoffLocation")
                                     where priceplan.VehiclePricePlanId == vehiclePricePlanId
                                     select priceplan).FirstOrDefault();
            }

        }
        if (GetValue<Int32?>("TransportSupplierId").HasValue)
            _transportSupplierId = GetValue<Int32>("TransportSupplierId");
    }
    

    protected string GetVehicleList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclePricePlan != null) ? _vehiclePricePlan.Vehicle.VehicleId : (Int32?)null;

        using (TransportEntities entity = new TransportEntities())
        {
            var vehicleQuery = (from s in entity.Vehicles 
                                                 select new {Vehicle=s,Type=s.Xlk_VehicleType.Description });

            if (_transportSupplierId.HasValue)
                vehicleQuery = vehicleQuery.Where(s => s.Vehicle.TransportSupplier.TransportSupplierId == _transportSupplierId.Value);


            foreach (var item in vehicleQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.Vehicle.VehicleId, item.Type,(_current.HasValue && item.Vehicle.VehicleId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetTripTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclePricePlan != null) ? _vehiclePricePlan.Xlk_TripType.TripTypeId : (Int32?)null;


        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_TripType> studentsQuery = (from s in entity.Xlk_TripType select s);
            foreach (Xlk_TripType item in studentsQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TripTypeId, item.Description, (_current.HasValue && item.TripTypeId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetDropOffLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclePricePlan != null) ? _vehiclePricePlan.DropoffLocation.LocationId : (Int32?)null;


        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_Location> Xlk_LocationQuery = (from s in entity.Xlk_Location select s);
            foreach (Xlk_Location item in Xlk_LocationQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current.HasValue && item.LocationId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetPickUpLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclePricePlan != null) ? _vehiclePricePlan.PickupLocation.LocationId : (Int32?)null;


        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_Location> Xlk_LocationQuery = (from s in entity.Xlk_Location select s);
            foreach (Xlk_Location item in Xlk_LocationQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current.HasValue && item.LocationId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetMaxCapacity
    {
        get
        {
            if (_vehiclePricePlan != null)
                return _vehiclePricePlan.MaxCapacity.ToString();

            return "0";
        }
    }

    protected string GetMinCapacity
    {
        get
        {
            if (_vehiclePricePlan != null)
                return _vehiclePricePlan.MinCapacity.ToString();

            return "0";
        }
    }

    protected string GetCost
    {
        get
        {
            if (_vehiclePricePlan != null)
                return _vehiclePricePlan.Cost.ToString();

            return "0";
        }
    }


    protected string GetVehiclePricePlanId
    {
        get
        {
            if (_vehiclePricePlan != null)
                return _vehiclePricePlan.VehiclePricePlanId.ToString();

            if (Parameters.ContainsKey("VehiclePricePlanId"))
                return Parameters["VehiclePricePlanId"].ToString();

            return "0";
        }
    }

}