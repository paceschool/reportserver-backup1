﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;

public partial class Transport_BookingsPage : BaseTransportPage
{

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            listtransportsuppliers.DataSource = this.LoadTransportSuppliers();
            listtransportsuppliers.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
        }    
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void lookupBooking(object sender, EventArgs e)
    {
        Click_LoadSearch();
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadSearch();
                break;
            case "future":
                Click_LoadFuture();
                break;
            case "past":
                Click_LoadPast();
                break;
            case "today":
                Click_LoadToday();
                break;
            case "thisweek":
                Click_LoadThisWeek();
                break;
            case "nextweek":
                Click_LoadNextWeek();
                break;
            case "nextmonth":
                Click_LoadNextMonth();
                break;
            default:
                Click_LoadAll();
                break;
        }
    }

    protected void Click_LoadSearch()
    {
        string reffield = reference.Text;
        DateTime? _fromDate = (!string.IsNullOrEmpty(fromDate.Text))?  Convert.ToDateTime(fromDate.Text) : (DateTime?)null;
        DateTime? _toDate = (!string.IsNullOrEmpty(toDate.Text)) ? Convert.ToDateTime(toDate.Text) : (DateTime?)null;
        int _transportsupplierid = Convert.ToInt32(listtransportsuppliers.SelectedItem.Value);

        using (TransportEntities entity = new TransportEntities())
        {
            var bookingSearchQuery = from f in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings").Include("PickupLocation").Include("DropoffLocation").Include("DropoffLocation")
                                   select f;

            if (_fromDate.HasValue || _toDate.HasValue || _transportsupplierid > 0 || !string.IsNullOrEmpty(reffield))
            {
                if (_fromDate.HasValue &&  _toDate.HasValue)
                    bookingSearchQuery = bookingSearchQuery.Where(s => s.RequestedDate >_fromDate.Value && s.RequestedDate < _toDate.Value);

                if (!string.IsNullOrEmpty(reffield))
                    bookingSearchQuery = bookingSearchQuery.Where(s => s.VehicleBookings.Any(z=>z.Reference==reffield ));


                if (_transportsupplierid > 0)
                    bookingSearchQuery = bookingSearchQuery.Where(s => s.VehicleBookings.Count(x=>x.VehiclePricePlan.Vehicle.TransportSupplier.TransportSupplierId == _transportsupplierid) > 0);


                LoadResults(bookingSearchQuery.OrderBy(g => g.RequestedDate).ToList());
            }
            else
                LoadResults(bookingSearchQuery.OrderBy(g => g.RequestedDate).Take(50).ToList());
        }

    }

    protected void Click_LoadAll()
    {
        DateTime today = DateTime.Now.Date;
        using (TransportEntities entity = new TransportEntities())
        {
            var GroupSearchQuery = from f in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings").Include("PickupLocation").Include("DropoffLocation").Include("DropoffLocation")
                                   orderby f.RequestedDate ascending
                                   select f;

            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_LoadToday()
    {
        DateTime today = DateTime.Now.Date;
        using (TransportEntities entity = new TransportEntities())
        {
            var GroupSearchQuery = from f in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings")
                                   where f.RequestedDate == today
                                   orderby f.RequestedDate ascending
                                   select f;

            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_LoadPast()
    {
        DateTime today = DateTime.Now.Date;
        using (TransportEntities entity = new TransportEntities())
        {
            var GroupSearchQuery = from f in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings").Include("PickupLocation").Include("DropoffLocation").Include("DropoffLocation")
                                   where f.RequestedDate < today
                                   orderby f.RequestedDate ascending
                                   select f;

            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_LoadFuture()
    {
        DateTime today = DateTime.Now.Date;
        using (TransportEntities entity = new TransportEntities())
        {
            var GroupSearchQuery = from f in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings").Include("PickupLocation").Include("DropoffLocation").Include("DropoffLocation")
                                   where f.RequestedDate > today
                                   orderby f.RequestedDate ascending
                                   select f;

            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_LoadNextMonth()
    {
        DateTime today = DateTime.Now;
        DateTime later = DateTime.Now.AddDays(28);
        using (TransportEntities entity = new TransportEntities())
        {
            var GroupSearchQuery = from n in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings").Include("PickupLocation").Include("DropoffLocation")
                                   where n.RequestedDate >= today && n.RequestedDate <= later
                                   orderby n.RequestedDate ascending
                                   select n;

            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_LoadThisWeek()
    {
        DateTime monday = FirstDayOfWeek(DateTime.Today.Date, System.Globalization.CalendarWeekRule.FirstFullWeek);
        DateTime _end = FirstDayOfWeek(DateTime.Today.Date.AddDays(7), System.Globalization.CalendarWeekRule.FirstFullWeek);

        using (TransportEntities entity = new TransportEntities())
        {
            var SearchQuery = from t in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings").Include("PickupLocation").Include("DropoffLocation")
                              where t.RequestedDate >= monday && t.RequestedDate <= _end
                              orderby t.RequestedDate ascending
                              select t;
            LoadResults(SearchQuery.ToList());
        }
    }

    protected void Click_LoadNextWeek()
    {
        DateTime monday = FirstDayOfWeek(DateTime.Today.Date.AddDays(7), System.Globalization.CalendarWeekRule.FirstFullWeek);
        DateTime _end = FirstDayOfWeek(DateTime.Today.Date.AddDays(14), System.Globalization.CalendarWeekRule.FirstFullWeek);

        using (TransportEntities entity = new TransportEntities())
        {
            var SearchQuery = from t in entity.Bookings.Include("Xlk_TripType").Include("VehicleBookings").Include("PickupLocation").Include("DropoffLocation")
                              where t.RequestedDate >= monday && t.RequestedDate <= _end
                              orderby t.RequestedDate ascending
                              select t;
            LoadResults(SearchQuery.ToList());
        }
    }

    private void LoadResults(List<Booking> bookings)
    {
        //Set the datasource of the repeater
        results.DataSource = bookings;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", bookings.Count().ToString());
    }

}
