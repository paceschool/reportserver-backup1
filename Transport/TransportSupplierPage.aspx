﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TransportSupplierPage.aspx.cs" Inherits="Transport_TransportSupplierPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" Runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Transport Supplier Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Browse By
                    </td>
                    <td>

                    </td>
                    <td>
                        Or Filter by Month
                    </td>
                    <td>
                    </td>
                    <td>
                        and Year
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span style="float:right">
            <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0' >
            </asp:Label>
        </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col" >
                        Id
                    </th>
                    <th scope="col" >
                        Name
                    </th>
                    <th scope="col" >
                        Description
                    </th>
                    <th scope="col" >
                        Voucher Ref
                    </th>
                 </tr>
            </thead>
            <tbody >
                <asp:Repeater ID="results" runat="server" EnableViewState="True">
                    <ItemTemplate>
                        <tr>
                            <td >
                                <%#DataBinder.Eval(Container.DataItem, "TransportSupplierId")%>
                            </td>
                            <td>
                             <a href="ViewTransportSupplierPage.aspx?TransportSupplierId=<%#DataBinder.Eval(Container.DataItem, "TransportSupplierId")%>">
                                   <%#DataBinder.Eval(Container.DataItem, "Supplier.Title")%>
                                </a>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "Supplier.Description")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "Supplier.VoucherRef")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                
            </tbody>
        </table>
    </div>
</asp:Content>

