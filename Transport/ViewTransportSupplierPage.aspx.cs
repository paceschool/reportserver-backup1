﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;

public partial class Transport_SearchBookingsPage : BaseTransportPage
{
    protected Int32 _transportSupplierId;
    protected string suppName;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["TransportSupplierId"]))
            _transportSupplierId = Convert.ToInt32(Request["TransportSupplierId"]);

        if (!Page.IsPostBack)
        {
            DisplayTransportSupplier(LoadTransportStudent(new TransportEntities(), _transportSupplierId));
        }
    }

    public override string TextValue() // Page Name
    {
        newPageName.Text = "View Transport SUpplier - " + suppName;
        return newPageName.Text;
    }

    private void DisplayTransportSupplier(TransportSupplier transportSupplier) //Display loaded Student details
    {
        if (transportSupplier != null)
        {
            suppName = transportSupplier.Supplier.Title;
            Id.Text = transportSupplier.Supplier.SupplierId.ToString();
            Name.Text = transportSupplier.Supplier.Title;
            VoucherRef.Text = transportSupplier.Supplier.VoucherRef;
            Description.Text = transportSupplier.Supplier.Description;

            newname.Text = transportSupplier.Supplier.Title;
            newdescription.Text = transportSupplier.Supplier.Description;
            newvoucherref.Text = transportSupplier.Supplier.VoucherRef;

        }
    }

    private TransportSupplier LoadTransportStudent(TransportEntities entity, Int32 transportSupplierId) //Display loaded Student details
    {
        IQueryable<TransportSupplier> transportSupplierQuery = from s in entity.TransportSuppliers.Include("Supplier")
                                           where s.TransportSupplierId == transportSupplierId
                                           select s;

        if (transportSupplierQuery.ToList().Count() > 0)
            return transportSupplierQuery.ToList().First();

        return null;
    }
    
    protected void Click_Save(object sender, EventArgs e)
    {
        //string _message = ""; errorcontainer.Visible = false;
        //using (EnrollmentsEntities entity = new EnrollmentsEntities())
        //{
        //    if (!string.IsNullOrEmpty(newfname.Text) || !string.IsNullOrEmpty(newsname.Text) || !string.IsNullOrEmpty(newarrdate.Text) ||
        //        !string.IsNullOrEmpty(newdepdate.Text))
        //    {
        //        Student _student = GetStudent(entity);

        //        _student.FirstName = newfname.Text;
        //        _student.SurName = newsname.Text;

        //        _student.PrepaidBook = chkbook.Checked;
        //        _student.PrepaidExam = chkexam.Checked;
        //        _student.HomeAddress = homeaddress.Text;
        //        _student.StudentEmail = stemail.Text;
        //        _student.Xlk_GenderReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Gender", "GenderId", Convert.ToByte(newgender.SelectedItem.Value));
        //        if (!string.IsNullOrEmpty(newdob.Text))
        //            _student.DateOfBirth = Convert.ToDateTime(newdob.Text);
        //        _student.Xlk_NationalityReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
        //        if (!string.IsNullOrEmpty(newarrdate.Text))
        //        _student.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
        //        if (!string.IsNullOrEmpty(newdepdate.Text))
        //            _student.DepartureDate = Convert.ToDateTime(newdepdate.Text);
        //        if (!string.IsNullOrEmpty(newplacement.Text))
        //            _student.PlacementTestResult = Convert.ToDecimal(newplacement.Text);
        //        _student.AgencyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
        //        _student.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));
        //        if (newexam.SelectedIndex > 0)
        //            _student.ExamReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Exam", "ExamId", Convert.ToInt16(newexam.SelectedItem.Value));
        //        else
        //            _student.ExamReference.EntityKey = null;

        //        SaveStudent(entity,_student);

        //    }
        //    else
        //    {
        //        _message = "Not all fields are complete. Please check.";
        //        errorcontainer.Visible = true;
        //        errormessage.Text = _message;
        //    }
    }


}
