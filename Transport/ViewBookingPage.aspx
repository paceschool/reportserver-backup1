﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewBookingPage.aspx.cs" Inherits="Transport_ViewBookingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });

            showPrintOptions = function () {
                $("#dialog-print").dialog("open");
            };

            $("#dialog-print").css({ 'background-color': '#E0FFFF' }).dialog({
                autoOpen: false,
                modal: true
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="View Transport Booking Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Title
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curTitle" runat="server">
                        </asp:Label><%= BookingLink(_bookingId, "~/Transport/ViewBookingPage.aspx", "More") %>
                    </td>
                    <th scope="col">
                        Date / Time
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curDate" runat="server">
                        </asp:Label>
                        @
                        <asp:Label ID="curTime" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Trip Type
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curTripType" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        No.Passengers
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curNoOfPasengers" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Rep Pickup Location
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curRepPickupLocation" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Pickup Location
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curPickupPoint" runat="server">
                        </asp:Label>
                        <asp:Label ID="curPickupLocation" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Dropoff Location
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curDropoffPoint" runat="server">
                        </asp:Label>
                        <asp:Label ID="curDropoffLocation" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Label3" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Pickup Info
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curCollectionInfo" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Drop off Info
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="currDropoffInfo" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Actions
                    </th>
                    <td style="text-align: center">
                        <a title="Edit Booking" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewBookingPage.aspx','Transport/AddTransportBookingControl.ascx',{'BookingId':<%=_bookingId %>},refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a> <a href="#" title="Create Tasks"
                                onclick="ajaxMessageBox('ViewBookingPage.aspx','ProcessTasks','All tasks complete or incomplete for this Transport Booking will be deleted, are you sure you wish to continue?', <%=_bookingId %>)">
                                <img src="../Content/img/actions/tools.png" title="Create Tasks" /></a>&nbsp;
                        <a href="#" title="Print Options" onclick="showPrintOptions();">
                            <img src="../Content/img/actions/printer.png" title="Print Options" />
                        </a>
                        &nbsp
                        &nbsp
                        &nbsp
                        &nbsp
                        <a title="Back to Planner" href="PlanBookingsPage.aspx">
                            <img src="../Content/img/actions/back.png" title="Back to Planner" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id="tabs" style="font-size: smaller">
            <ul>
                <li><a href="ViewBookingPage.aspx/LoadBookingVehicles?id=<%= _bookingId %>">Vehicles
                    In Use</a></li>
                <li><a href="ViewBookingPage.aspx/LoadBookingExcursions?id=<%= _bookingId %>">Excursions</a></li>
                <li><a href="ViewBookingPage.aspx/LoadBookingGroupTransfers?id=<%= _bookingId %>">Group
                    Transfers</a></li>
                <li><a href="ViewBookingPage.aspx/LoadBookingStudentTransfers?id=<%= _bookingId %>">
                    Student Transfers</a></li>
                <li><a href="ViewBookingPage.aspx/LoadBookingTasks?id=<%= _bookingId %>">Tasks</a></li>
            </ul>
        </div>
    </div>
    <div id="dialog-print" title="Print Options" style="display: none">
        <a href="<%= ReportPath("SupportReports","Support_TransportBooking","HTML4.0", new Dictionary<string, object> { {"BookingId",_bookingId}}) %>"
            title="Show Booking Report">
            <img src="../Content/img/actions/report-paper.png" title="Show Booking Report" />
            Show Booking Report </a>
        <br />
        <br />
    </div>
</asp:Content>
