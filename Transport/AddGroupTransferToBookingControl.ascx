﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupTransferToBookingControl.ascx.cs" 
    Inherits="Transport_AddGroupTransferToBookingControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addtransfertobooking");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Transport/ViewBookingPage.aspx","SaveGroupTransferToBooking") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>

<p id="message" style="display: none">
    All fields must be completed
</p>

<form id="addtransfertobooking" name="addtransfertobooking" method="post" action="ViewBookingPage/SaveGroupTransferToBooking">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="TransportBookingId" value="<%= GetBookingId %>" />
            <label>
                Select Transfer to attach
            </label>
            <select id="GroupTransferId">
                <%= GetTransferList() %>
            </select>
        </fieldset>
    </div>
</form>