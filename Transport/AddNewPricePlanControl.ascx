﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddNewPricePlanControl.ascx.cs"
    Inherits="Transport_AddNewPricePlanControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addpriceplan");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Transport/ViewTransportSupplierPage.aspx","SavePricePlan") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addpriceplan" name="addpriceplan" method="post" action="ViewTransportSupplierPage.aspx/SavePricePlan">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="VehiclePricePlanId" value="<%= GetVehiclePricePlanId %>" />
        <label>
            Vehicle
        </label>
        <select name="Vehicle" id="VehicleId">
            <%= GetVehicleList()%>
        </select>
        <label>
            Pickup Point
        </label>
        <select name="PickupLocation" id="LocationId">
            <%= GetPickUpLocationList()%>
        </select>
        <label>
            Drop Off Point
        </label>
        <select name="DropoffLocation" id="LocationId">
            <%= GetDropOffLocationList()%>
        </select>
        <label>
            Max Capacity
        </label>
        <input type="text" name="Cost" id="MaxCapacity" value="<%= GetMaxCapacity %>" />
        <label>
            Min Capacity
        </label>
        <input type="text" name="Cost" id="MinCapacity" value="<%= GetMinCapacity %>" />
        <label>
            Cost
        </label>
        <input type="text" name="Cost" id="Cost" value="<%= GetCost %>" />
        <label>
            Trip Type
        </label>
        <select name="Xlk_TripType" id="TripTypeId">
            <%= GetTripTypeList()%>
        </select>

    </fieldset>
</div>
</form>
