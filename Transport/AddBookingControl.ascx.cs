﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using System.Text;
using System.Web.Services;

public partial class AddBookingControl : BaseTransportControl
{
    protected Int32 _bookingId;
    protected Booking _booking;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("BookingId").HasValue)
        {
            Int32 BookingId = GetValue<Int32>("BookingId");
            using (TransportEntities entity = new TransportEntities())
            {
                _booking = (from b in entity.Bookings.Include("Xlk_TripType").Include("Xlk_Status").Include("PickupLocation").Include("DropoffLocation").Include("User")
                            where b.BookingId == BookingId
                            select b).FirstOrDefault();
            }
        }
    }

    protected string GetBookingId
    {
        get
        {
            if (_booking != null)
                return _booking.BookingId.ToString();

            return null;
        }
    }

    protected string GetTitle
    {
        get
        {
            if (_booking != null)
                return _booking.Title.ToString();

            return null;
        }
    }

    protected string GetTripTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (_booking != null) ? _booking.Xlk_TripType.TripTypeId : (Byte?)null;

        foreach (Xlk_TripType item in LoadTripTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TripTypeId, item.Description, (_current != null && item.TripTypeId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    //protected string GetVehicleTypeList()
    //{
    //    StringBuilder sb = new StringBuilder();

    //    Byte? _current = (_booking != null) ? _booking.VehicleList
    //}

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();
        
        Byte? _current = (_booking != null) ? _booking.Xlk_Status.StatusId : (Byte?)null;

        foreach (Xlk_Status item in LoadStatusTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current != null && item.StatusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetRequestedDate
    {
        get
        {
            if (_booking != null)
                return _booking.RequestedDate.ToString("dd/mm/yy");

            return null;
        }
    }

    protected string GetRequestedTime
    {
        get
        {
            if (_booking != null)
                return _booking.RequestedTime.ToString();

            return null;
        }
    }

    protected string GetNoOfPassengers
    {
        get
        {
            if (_booking != null)
                return _booking.NumberOfPassengers.ToString();

            return null;
        }
    }

    protected string GetPickupLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_booking != null) ? _booking.PickupLocation.LocationId : (Int16?)null;

        foreach (Xlk_Location item in LoadLocations())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current != null && item.LocationId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetPickupLocationInfo
    {
        get
        {
            if (_booking != null && _booking.PickupLocationInfo != null)
                return _booking.PickupLocationInfo.ToString();

            return null;
        }
    }

    protected string GetDropoffLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_booking != null) ? _booking.DropoffLocation.LocationId : (Int16?)null;

        foreach (Xlk_Location item in LoadLocations())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current != null && item.LocationId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetDropoffLocationInfo
    {
        get
        {
            if (_booking != null && _booking.DropoffLocationInfo != null)
                return _booking.DropoffLocationInfo.ToString();

            return null;
        }
    }

    protected string GetRepPickupLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_booking != null) ? _booking.RepCollectionLocation.LocationId : (Int16?)null;

        foreach (Xlk_Location item in LoadLocations())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current != null && item.LocationId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetCollectionInfo
    {
        get
        {
            if (_booking != null)
                return _booking.CollectionInfo.ToString();

            return null;
        }
    }

    protected string GetDropoffInfo
    {
        get
        {
            if (_booking != null)
                return _booking.DropoffInfo.ToString();

            return null;
        }
    }

    protected string GetConfirmedDate
    {
        get
        {
            if (_booking != null && _booking.ConfirmedDate.HasValue)
                return _booking.ConfirmedDate.ToString();

            return null;
        }
    }

    protected string GetUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_booking != null) ? _booking.User.UserId : (Int32?)null;

        foreach (User item in LoadUsers())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current != null && item.UserId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }
}