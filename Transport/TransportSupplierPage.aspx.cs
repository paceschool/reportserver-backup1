﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;

public partial class Transport_TransportSupplierPage : BaseTransportPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            SearchTransportSupplier();

        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    #endregion
    #region Private Methods

    private void LoadResults(List<TransportSupplier> suppliers)
    {
        //Set datasource of the repeater
        results.DataSource = suppliers;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", suppliers.Count().ToString());
    }

    private void SearchTransportSupplier()
    {
        using (TransportEntities entity = new TransportEntities())
        {

            List<TransportSupplier> supplierIdList = (from s in entity.TransportSuppliers.Include("Supplier")
                                           select s).ToList();

            LoadResults(supplierIdList);

        }
    }


    #endregion

}
