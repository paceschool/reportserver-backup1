﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="ManageBookingPage.aspx.cs" Inherits="Transport_ManageBookingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {

            $("table").tablesorter();


            $("#<%= reqdate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" Runat="Server">
<%--    <div id="pagename">
        <span style="font-style: italic; font-weight: bolder; font-size: larger">
            Manage Transport Bookings Page 
        </span>
    </div>--%>
    <div class="resultscontainer">
        <div id="errorcontainer" name="errorcontainer" class="error" runat="server" visible="false">
            <asp:Label ID="errormessage" runat="server" Text="">
            </asp:Label>
        </div>
        <div class="inputArea" style="font-size:small">
            <fieldset>
                <label>
                    Trip Type
                </label>
                <asp:DropDownList ID="triptypelist" runat="server" name="triptypelist" DataTextField="Description" DataValueField="TripTypeId"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="Select Trip Type" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <label>
                    Vehicle Type
                </label>
                <asp:DropDownList ID="vehicletypelist" runat="server" name="vehicletypelist" DataTextField="Description" DataValueField="VehicleTypeId"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="Select Vehicle Type" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <label>
                    Status
                </label>
                <asp:DropDownList ID="statustypelist" runat="server" name="statustypelist" DataTextField="Description" DataValueField="StatusId"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="Select Status" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <label>
                    Requested Date
                </label>
                <asp:TextBox ID="reqdate" runat="server" name="reqdate">
                </asp:TextBox>
                <label>
                    Requested Time
                </label>
                <asp:TextBox ID="reqtime" runat="server" name="reqtime">
                </asp:TextBox>
            </fieldset>
            <fieldset>
                <label>
                    Number of Passengers
                </label>
                <asp:TextBox ID="nopax" runat="server" name="nopax">
                </asp:TextBox>
                <label>
                    Pickup Location
                </label>
                <asp:DropDownList ID="pickupzone" runat="server" name="pickupzone" DataTextField="Description" DataValueField="LocationId"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="Select Pickup Location" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <label>
                    Dropoff Location
                </label>
                <asp:DropDownList ID="dropoffzone" runat="server" name="dropoffzone" DataTextField="Description" DataValueField="LocationId"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="Select Dropoff Location" Value="-1"></asp:ListItem>
                </asp:DropDownList>
                <label>
                    Pick up Rep?
                </label>
                <asp:CheckBox ID="reppickup" runat="server" />
                <label id="checkrep">
                    Location for Rep Pickup
                </label>
                <asp:DropDownList ID="repzone" runat="server" name="repzone" DataTextField="Description" DataValueField="LocationId"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="Select Rep Pickup" Value="-1"></asp:ListItem>
                </asp:DropDownList>
            </fieldset>
            <fieldset>
                <label>
                    Pickup Information
                </label>
                <asp:TextBox ID="pickupinfo" runat="server" name="pickupinfo">
                </asp:TextBox>
                <label>
                    Dropoff Information
                </label>
                <asp:TextBox ID="dropoffinfo" runat="server" name="dropoffinfo">
                </asp:TextBox>
                <label>
                    Enter Voucher Id?
                </label>
                <asp:TextBox ID="voucherid" runat="server" name="voucherid">
                </asp:TextBox>
            </fieldset>
            <fieldset>
                <label>
                    Confirmed Date
                </label>
                <asp:TextBox ID="confirmeddate" runat="server" name="confirmeddate">
                </asp:TextBox>
                <label>
                    Confirmed By
                </label>
                <asp:DropDownList ID="userlist" runat="server" DataTextField="Name" DataValueField="UserId" AppendDataBoundItems="true">
                    <asp:ListItem Text="Select User" Value="0"></asp:ListItem>
                </asp:DropDownList>
                <asp:LinkButton ID="savebooking" runat="server" class="button" OnClick="Click_SaveBooking"><span>Save</span>
                </asp:LinkButton>
            </fieldset>
        </div>
    </div>
</asp:Content>

