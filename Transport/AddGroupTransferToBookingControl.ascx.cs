﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;


public partial class Transport_AddGroupTransferToBookingControl : BaseTransportControl
{
    protected Int32 _bookingId = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("BookingId").HasValue)
        {
            _bookingId = GetValue<Int32>("BookingId");
        }
        
    }

    protected string GetBookingId
    {
        get
        {
            if (_bookingId != null)
                return _bookingId.ToString();

            return "0";
        }
    }

    protected string GetTransferList()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            foreach (Pace.DataAccess.Group.GroupTransfer item in LoadGroupTransfers())
            {
                sb.AppendFormat("<option alt={0} value={0}>{1}</option>", item.GroupTransferId, item.Group.GroupName + " - " + item.Xlk_TransferType.Description);
            }
        }
        return sb.ToString();
    }

    protected void Click_ChangeTransfers(object sender, EventArgs e)
    {
        GetTransferList();
    }


    private IList<Pace.DataAccess.Group.GroupTransfer> LoadGroupTransfers()
    {
        using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
        {
            var lookupQuery = (from t in gentity.GroupTransfer.Include("Group").Include("Xlk_TransferType").Include("Xlk_Location")
                               where !t.TransportBookingId.HasValue && t.Date > DateTime.Now
                               orderby t.Group.GroupName ascending, t.Xlk_TransferType.Description ascending
                               select t).AsEnumerable(); // Get all Transfers that are later than today


            return lookupQuery.ToList();
        }

    }


}