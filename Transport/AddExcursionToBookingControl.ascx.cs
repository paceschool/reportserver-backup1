﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;


public partial class Transport_AddExcursionToBookingControl : BaseTransportControl
{
    protected Int32 _transportBookingId;
    protected Int32 _excursionId;
    protected Int32 _excursionBookingId;
    //protected Lnk_ExcursionBooking _booking;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("TransportBookingId").HasValue)
        {
            using (TransportEntities entity = new TransportEntities())
            {
                Int32 TransportBookingId = GetValue<Int32>("TransportBookingId");
                Int32 ExcursionBookingId = GetValue<Int32>("ExcursionBookingId");
                
                //_booking = (from l in entity.Lnk_ExcursionBooking
                //            where l.TransportBookingId == TransportBookingId && l.ExcursionBookingId == ExcursionBookingId
                //            select l).FirstOrDefault();
            }
        }
    }


    protected string GetExcursionBookingId
    {
        get
        {
            //if (_booking != null)
            //    return _booking.ExcursionBookingId.ToString();

            if (Parameters.ContainsKey("ExcursionBookingId"))
                return Parameters["ExcursionBookingId"].ToString();

            return "0";
        }
    }

    # region GetLists

    protected string GetExcursionList()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
        {
            foreach (Pace.DataAccess.Excursion.Booking item in LoadBookings())
            {
                sb.AppendFormat("<option alt={0} value={0}>{1}</option>", item.BookingId, item.Title);
            }
        }
        return sb.ToString();
    }

    private IList<Pace.DataAccess.Excursion.Booking> LoadBookings()
    {
        using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
        {
            var lookupQuery = (from b in entity.Bookings.Include("Excursion").Include("Xlk_TransportType")
                               where !b.TransportBookingId.HasValue && b.Xlk_TransportType.TransportTypeId != 1 && b.ExcursionDate > DateTime.Now
                               orderby b.Title
                               select b).AsEnumerable(); // Get all Excursion Bookings that need transport and are later than today

            return lookupQuery.ToList();
        }
    }

    #endregion
}