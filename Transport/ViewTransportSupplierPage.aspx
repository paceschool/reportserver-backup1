﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ViewTransportSupplierPage.aspx.cs" Inherits="Transport_SearchBookingsPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(function () {

            $("table").tablesorter()

            $("#viewTabs").tabs({ spinner: 'Retrieving data...' });


            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            showPrintOptions = function () {
                $("#dialog-print").dialog("open");
            };

            $("#dialog-print").css({ 'background-color': '#E0FFFF' }).dialog({
                autoOpen: false,
                modal: true
            });

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); }, 
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" Runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="View Transport Supplier Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <div id="viewTabs">
            <ul>
                <li><a href="#viewTabs-1">View</a></li>
                <li><a href="#viewTabs-2">Edit</a></li>
            </ul>
            <div id="viewTabs-1">
                <table class="box-table-a">
                    <thead>
                        <tr>
                            <th scope="col" style="width:7%">
                                <label >
                                    ID
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Name
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Description-
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Voucher Ref
                                </label>
                            </th>
                         </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Id" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left; background-color:Yellow">
                                <asp:Label ID="Name" runat="server" Font-Bold="true" Font-Size="Larger">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Description" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="VoucherRef" runat="server">
                                </asp:Label>
                            </td>
                          </tr>
                    </tbody>
                </table>
                
            </div>
            <div id="viewTabs-2">
                <div id="errorcontainer" name="errorcontainer" class="error" runat="server" visible="false">
                    <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
                </div>
                <div class="inputArea">
                    <fieldset>
                        <label>
                            Name</label>
                        <asp:TextBox ID="newname" runat="server">
                        </asp:TextBox>
                        <label>
                            Description</label>
                        <asp:TextBox ID="newdescription" runat="server">
                        </asp:TextBox>
                       <label>
                            Voucher Ref</label>
                        <asp:TextBox ID="newvoucherref" runat="server">
                        </asp:TextBox>

                        <asp:LinkButton ID="newsave" runat="server" class="button" OnClick="Click_Save"><span>Save</span></asp:LinkButton>
                        <asp:LinkButton ID="cancel" runat="server" class="button" OnClientClick=""><span>Cancel</span></asp:LinkButton>
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="examinfo" runat="server" Visible="false">
                        </asp:Label>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
        <div class="resultscontainer">
        <div id="tabs">
            <ul>
                 <li><a href="ViewTransportSupplierPage.aspx/LoadBookings?id=<%= _transportSupplierId  %>&type='current'">Future Bookings</a></li>
                <li><a href="ViewTransportSupplierPage.aspx/LoadBookings?id=<%= _transportSupplierId %>&type='past'">Past Bookings</a></li>
                <li><a href="ViewTransportSupplierPage.aspx/LoadVehicles?id=<%= _transportSupplierId  %>">Vehicles</a></li>
                <li><a href="ViewTransportSupplierPage.aspx/LoadPricePlans?id=<%= _transportSupplierId %>">Price Plans</a></li>
            </ul>
        </div>
    </div>
</asp:Content>

