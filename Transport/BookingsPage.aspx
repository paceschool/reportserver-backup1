﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BookingsPage.aspx.cs" Inherits="Transport_BookingsPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            $("#<%= repeat.ClientID %>").buttonset();
            $("#<%= fromDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<%= toDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

        });
    </script>
    <style type="text/css">
        #toolbar
        {
            padding: 10px 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Transport Bookings Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td style="text-align:right">
                        From Date
                    </td>
                    <td>
                        <asp:TextBox ID="fromDate" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="Large"></asp:TextBox>
                    </td>
                    <td style="text-align:right">
                        To Date
                    </td>
                    <td>
                        <asp:TextBox ID="toDate" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="Large"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:right">
                        Ref
                    </td>
                    <td>
                        <asp:TextBox ID="reference" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="Large"></asp:TextBox>
                    </td>
                    <td style="text-align:right">
                        Transport Supplier
                    </td>
                    <td>
                        <asp:DropDownList ID="listtransportsuppliers" runat="server" DataTextField="Value"
                            DataValueField="Key" AppendDataBoundItems="true" Font-Size="Small">
                            <asp:ListItem Text="All Transport Suppliers" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="LinkButton2" PostBackUrl="~/Transport/BookingsPage.aspx" runat="server"
                OnClick="lookupBooking"><span>Search Results</span>
            </asp:LinkButton>
            <a href="?Action=today">Today</a> 
            <a href="?Action=thisweek">This Week</a>
            <a href="?Action=nextweek">Next Week</a>
            <a href="?Action=nextmonth">Next 28 days</a>
            <a href="?Action=future">Future</a> 
            <a href="?Action=past">Past</a> 
            <a href="?Action=all">All</a> 
        </span>
        <span style="float: right;">
            <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
            </asp:Label>
        </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Title
                    </th>
                    <th scope="col">
                        Type
                    </th>
                    <th scope="col">
                        Date / Time
                    </th>
                    <th scope="col">
                        Trip Type
                    </th>
                    <th scope="col">
                        No of Passengers
                    </th>
                    <th scope="col">
                        Pickup Location
                    </th>
                    <th scope="col">
                        Dropoff Location
                    </th>
                    <th scope="col">
                        Pickup Rep?
                    </th>
                    <th scope="col">
                        Rep Pickup Location
                    </th>
                    <th scope="col">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody style="font-size: smaller">
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "BookingId")%>
                            </td>
                            <td style="text-align: left">
                              <a href="ViewBookingPage.aspx?BookingId=<%#DataBinder.Eval(Container.DataItem, "BookingId")%>">
                                <%#TrimText(DataBinder.Eval(Container.DataItem, "Title"), 80)%></a>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Xlk_TripType.Description") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "RequestedDate", "{0:D}")%>/
                                <%#DataBinder.Eval(Container.DataItem, "RequestedTime")%>
                            </td>
                            <td style="text-align:left">
                                <%#DataBinder.Eval(Container.DataItem, "Xlk_TripType.Description") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "NumberOfPassengers")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "PickupLocation.Description") %>
                            </td>
                             <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "DropoffLocation.Description") %>
                            </td>
                            <td style="text-align:left">
                                
                             </td>
                             <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "RepCollectionLocation.Description") %>
                            </td>
                            <td style="text-align: center">
                                <a onclick="deleteObjectFromAjaxTab('BookingsPage.aspx','TransportBooking',<%#DataBinder.Eval(Container.DataItem, "BookingId")%>)" href="#" title="Delete Booking">
                                    <img src="../Content/img/actions/bin_closed.png">
                                </a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
