﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddVehicleBookingControl.ascx.cs" 
    Inherits="Transport_AddVehicleBookingControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addvehiclebooking");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Transport/ViewBookingPage.aspx","SaveVehicleBooking") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#ConfirmedDate").datepicker({ dateFormat: 'dd/mm/yy' });

        $('.watcher').live("change", function () {
            $('#Cost').val($(this).find("option:selected").attr("alt"));
            $('#VehicleId').val($(this).find("option:selected").attr("title"));
        });

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addvehiclebooking" name="addvehiclebooking" method="post" action="ViewBookingsPage.aspx/SaveVehicleBooking">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="BookingId" name="Booking" value="<%= GetBookingId %>" />
            <input type="hidden" id="VehicleBookingId" value="<%= GetVehicleBookingId %>" />
            <label>
                Price Plan
            </label>
            <select name="VehiclePricePlan" id="VehiclePricePlanId" class="watcher">
                <%= GetPricePlanList() %>
            </select>
            <label>
                Status
            </label>
            <select name="Xlk_Status" id="StatusId">
                <%= GetStatusList() %>
            </select>
            <label>
                Driver Name
            </label>
            <input type="text" id="DriverName" value="<%= GetDriver %>" />
            <label>
                Driver Number
            </label>
            <input type="text" id="DriverContactNumber" value="<%= GetPhone %>" />
            <label>
                Vehicle Cost (if different from above)
            </label>
            <input type="text" id="Cost" value="<%= GetCost %>" />
            <label>
                Reference
            </label>
            <input type="text" id="Reference" value="<%= GetReference %>" />
        </fieldset>
        <fieldset>
            <label>
                Confirmed Date
            </label>
            <input type="datetext" id="ConfirmedDate" value="<%= GetVehicleDate %>" />
            <label>
                <b>Total passengers: <%=GetPassengers %></b>
            </label><br />
            <label>
                <b>Vehicle options: <%= GetVehicleOptions %></b>
            </label>
        </fieldset>
    </div>
</form>