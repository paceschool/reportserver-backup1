﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddExcursionToBookingControl.ascx.cs" 
    Inherits="Transport_AddExcursionToBookingControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $('#addexcursiontobooking');
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Transport/ViewBookingPage.aspx","SaveExcursionToBooking") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
        
    });
</script>

<p id="message" style="display: none">
    All fields must be completed
</p>

<form id="addexcursiontobooking" name="addexcursiontobooking" method="post" action="ViewBookingPage/SaveExcursionToBooking">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="TransportBookingId" value="<%= GetValue<Int32>("BookingId") %>" />
            <label>
                Select Excursion Booking
            </label>
            <select id="BookingId" >
                <%= GetExcursionList() %>
            </select>
        </fieldset>
    </div>
</form>