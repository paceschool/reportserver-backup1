﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using Pace.DataAccess.Security;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;
using System.Data.Entity.Core;


public partial class Transport_ManageBookingPage : BaseTransportPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadTypes();
            LoadUsers();
        }
    }

    protected void LoadTypes()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            var ttypes = from t in entity.Xlk_TripType
                        select t;
            triptypelist.DataSource = ttypes.ToList();
            triptypelist.DataBind();

            var vtypes = from v in entity.Xlk_VehicleType
                         select v;
            vehicletypelist.DataSource = vtypes.ToList();
            vehicletypelist.DataBind();

            var stypes = from s in entity.Xlk_Status
                         select s;
            statustypelist.DataSource = stypes.ToList();
            statustypelist.DataBind();

            var places = from l in entity.Xlk_Location
                         select l;
            pickupzone.DataSource = places.ToList();
            pickupzone.DataBind();
            dropoffzone.DataSource = places.ToList();
            dropoffzone.DataBind();
            repzone.DataSource = places.ToList();
            repzone.DataBind();
        }
    }

    protected void LoadUsers()
    {
        using (SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
        {
            var users = from u in entity.Users
                        select u;
            userlist.DataSource = users.ToList();
            userlist.DataBind();
        }
    }

    protected void Click_SaveBooking(object sender, EventArgs e)
    {
        try
        {
            using (TransportEntities entity = new TransportEntities())
            {
                Booking newbooking = new Booking();
                Vehicle newvehicle = new Vehicle();

                newbooking.CollectionInfo = pickupinfo.Text;
                newbooking.UserReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", Convert.ToInt32(userlist.SelectedItem.Value));
                if (confirmeddate.Text != "")
                    newbooking.ConfirmedDate = Convert.ToDateTime(confirmeddate.Text);
                newbooking.DropoffInfo = dropoffinfo.Text;
                newbooking.DropoffLocationReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Location", "LocationId", Convert.ToInt16(dropoffzone.SelectedItem.Value));
                newbooking.NumberOfPassengers = Convert.ToInt32(nopax.Text);
                newbooking.PickupLocationReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Location", "LocationId", Convert.ToInt16(pickupzone.SelectedItem.Value));
                if (repzone.SelectedItem.Value != "-1")
                    newbooking.RepCollectionLocationReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Location", "LocationId", Convert.ToInt16(repzone.SelectedItem.Value));
                if (reqdate.Text != null)
                    newbooking.RequestedDate = Convert.ToDateTime(reqdate.Text);
                if (reqtime.Text != null)
                {
                    newbooking.RequestedTime = TimeSpan.Parse(reqtime.Text);
                }

                newbooking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(statustypelist.SelectedItem.Value));
                newbooking.Xlk_TripTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_TripType", "TripTypeId", Convert.ToByte(triptypelist.SelectedItem.Value));

                //newvehicle.Xlk_VehicleTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_VehicleType", "VehicleTypeId", Convert.ToByte(vehicletypelist.SelectedItem.Value));
                
                SaveBooking(entity, newbooking);
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    private void SaveBooking(TransportEntities entity, Booking transportbooking)
    {
        entity.AddToBookings(transportbooking);

        if (entity.SaveChanges() > 0)
        {
            Response.Redirect(string.Format("~/Transport/ViewBookingPage.aspx?BookingId=" + transportbooking.BookingId));
        }
    }
}
