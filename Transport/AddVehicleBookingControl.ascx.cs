﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Transport_AddVehicleBookingControl : BaseTransportControl
{
    protected Int32 _bookingId;
    protected Int32 _vehicleId;
    protected Int32 _number;
    protected Int32 _plocation;
    protected Int32 _dlocation;
    protected VehicleBooking _vehiclebooking;
    protected VehicleBooking _allvehiclesbooked;
    protected Booking _booking;
    protected Int32? _vbook = 0;
    protected bool exists;
    protected string buscost = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (TransportEntities entity = new TransportEntities())
        {
            if (GetValue<Int32?>("VehicleBookingId").HasValue && GetValue<Int32?>("BookingId").HasValue)
            {
                Int32 VehicleBookingId = GetValue<Int32>("VehicleBookingId");
                Int32 BookingId = GetValue<Int32>("BookingId");

                _vehiclebooking = (from v in entity.VehicleBookings.Include("VehiclePricePlan").Include("Xlk_Status").Include("VehiclePricePlan.Vehicle.Xlk_VehicleType")
                            where v.VehicleBookingId == VehicleBookingId && v.Booking.BookingId == BookingId
                            select v).FirstOrDefault();
                _booking = (from b in entity.Bookings
                            where b.BookingId == BookingId
                            select b).FirstOrDefault();
                _number = _booking.NumberOfPassengers.Value;
                _plocation = _booking.PickupLocation.LocationId;
                _dlocation = _booking.DropoffLocation.LocationId;

                _allvehiclesbooked = (from v in entity.VehicleBookings.Include("VehiclePricePlan").Include("Xlk_Status").Include("VehiclePricePlan.Vehicle.Xlk_VehicleType")
                                     where v.Booking.BookingId == BookingId
                                     select v).FirstOrDefault();
            }

            if (!GetValue<Int32?>("VehicleBookingId").HasValue && GetValue<Int32?>("BookingId").HasValue)
            {
                Int32 VehicleBookingId = 0;
                Int32 BookingId = GetValue<Int32>("BookingId");

                _booking = (from v in entity.Bookings.Include("VehicleBookings").Include("Xlk_Status")
                            where v.BookingId == BookingId
                            select v).FirstOrDefault();
                _number = _booking.NumberOfPassengers.Value;
                _plocation = _booking.PickupLocation.LocationId;
                _dlocation = _booking.DropoffLocation.LocationId;
            }
        }
    }

    protected string GetBookingId
    {
        get
        {
            if (_booking != null)
                return _booking.BookingId.ToString();

            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }

    protected string GetVehicleBookingId
    {
        get
        {
            if (_vehiclebooking != null)
                return _vehiclebooking.VehicleBookingId.ToString();

            if (Parameters.ContainsKey("VehicleBookingId"))
                return Parameters["VehicleBookingId"].ToString();

            return "0";
        }
    }

    #region Get strings

    protected string GetReference
    {
        get
        {
            if (_vehiclebooking != null)
                return _vehiclebooking.Reference;

            return string.Empty;
        }
    }

    protected string GetCost
    {
        get
        {
            if (_vehiclebooking != null)
                return _vehiclebooking.Cost.Value.ToString("00.00");

            LoadFirstPricePlan(_number, _plocation, _dlocation);
            return buscost;
        }
    }

    protected string GetDriver
    {
        get
        {
            if (_vehiclebooking != null && _vehiclebooking.DriverName != null)
                return _vehiclebooking.DriverName.ToString();

            return string.Empty;
        }
    }

    protected string GetPhone
    {
        get
        {
            if (_vehiclebooking != null && _vehiclebooking.DriverContactNumber != null)
                return _vehiclebooking.DriverContactNumber.ToString();

            return string.Empty;
        }
    }

    protected string GetVehicleDate
    {
        get
        {
            if (_vehiclebooking != null && _vehiclebooking.ConfirmedDate.HasValue)
                return _vehiclebooking.ConfirmedDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetPassengers
    {
        get
        {
            if (_booking != null)
                return _booking.NumberOfPassengers.ToString();

            return "0";
        }
    }

    //protected string GetAssigned
    //{
    //    get
    //    {
    //        if (_vehiclebooking != null)
    //        {
    //            return (_booking.NumberOfPassengers.Value - _vehiclebooking.Vehicle.MaxCapacity).ToString();
    //        }

    //        return "0";
    //    }
    //}

    protected string GetVehicleOptions
    {
        get
        {
            if (_booking != null)
            {
                StringBuilder sb = new StringBuilder();
                Int32[] choice = new Int32[10]; Int32 counter = 1; Int32[,] final = new Int32[10,10];
                TransportEntities ventity = new TransportEntities();
                var lookup = from v in ventity.Vehicles.Include("Xlk_VehicleType")
                             orderby v.MaxCapacity ascending
                             select v;

                var blookup = from b in ventity.Bookings
                              select b;

                foreach (var item in lookup)
                {
                    choice[counter] = item.MinCapacity.Value;
                    counter++;
                }

                for (var x = 0; x < counter; x++)
                {
                    for (var y = 1; y < counter; y++)
                    {
                        final[x, y] = choice[x] + choice[y];
                    }
                }

                if (_booking.NumberOfPassengers > 0)
                {
                    foreach(var item in lookup)
                    {
                        if (item.MaxCapacity >= _booking.NumberOfPassengers && item.MinCapacity <= _booking.NumberOfPassengers)
                            sb.AppendFormat("<br><label><i>{1} - {2} pax</i></label>", item.Xlk_VehicleType.VehicleTypeId, item.Xlk_VehicleType.Description, item.MaxCapacity);
                        
                    }
                    return sb.ToString();
                }
            }

            return "0";
        }
    }

    #endregion

    #region Get Lists

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (_vehiclebooking != null && _vehiclebooking.Xlk_Status.StatusId != null) ? _vehiclebooking.Xlk_Status.StatusId : (Byte?)null;

        foreach (Xlk_Status stat in LoadStatusTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", stat.StatusId, stat.Description, (_current.HasValue && stat.StatusId == _current) ? "Selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetVehiclesList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclebooking != null && _vehiclebooking.VehiclePricePlan.Vehicle != null) ? _vehiclebooking.VehiclePricePlan.Vehicle.VehicleId : (Int32?)null;

        foreach (Vehicle item in LoadVehicles())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.VehicleId, item.Xlk_VehicleType.Description, (_current.HasValue && item.VehicleId == _current) ? "Selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetPricePlanList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclebooking != null && _vehiclebooking.VehiclePricePlan != null) ? _vehiclebooking.VehiclePricePlan.VehiclePricePlanId : (Int32?)null;
        
        foreach (VehiclePricePlan item in LoadPricePlans(_number, _plocation, _dlocation))
        {
            sb.AppendFormat("<option {2} value={0} alt={1} title={5}>{4}, {3} - €{1}</option>", item.VehiclePricePlanId, item.Cost.Value.ToString("00.00"), (_current.HasValue && item.VehiclePricePlanId == _current) ? "Selected" : string.Empty, item.Vehicle.Xlk_VehicleType.Description, item.Vehicle.TransportSupplier.Supplier.Title, item.Vehicle.VehicleId);
        }

        return sb.ToString();
    }

    #endregion

    private IList<VehiclePricePlan> LoadPricePlans(int num, int ploc, int dloc)
    {
        using (TransportEntities entity = new TransportEntities())
        {
            var vehicleLookup = from v in entity.VehiclePricePlans.Include("Vehicle").Include("Vehicle.Xlk_VehicleType").Include("Vehicle.TransportSupplier.Supplier")
                            where v.PickupLocation.LocationId == ploc && v.DropoffLocation.LocationId == dloc && (v.MaxCapacity >= num)
                            select v;

        if (vehicleLookup.Count() > 0)
            return vehicleLookup.ToList();
        else
            return entity.VehiclePricePlans.Include("Vehicle").Include("Vehicle.Xlk_VehicleType").Include("Vehicle.TransportSupplier.Supplier").ToList();

        }
    }

    private IList<VehiclePricePlan> LoadFirstPricePlan(int num, int ploc, int dloc)
    {
        TransportEntities entity = new TransportEntities();

        var vehicleLookup = (from v in entity.VehiclePricePlans
                             where v.PickupLocation.LocationId == ploc && v.DropoffLocation.LocationId == dloc
                             select v);

        if (vehicleLookup.Count() > 0)
        {
            buscost = vehicleLookup.FirstOrDefault().Cost.Value.ToString();
            return vehicleLookup.ToList();
        }
        return null;
    }
}