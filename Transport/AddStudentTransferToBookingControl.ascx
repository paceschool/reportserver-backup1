﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentTransferToBookingControl.ascx.cs" 
    Inherits="Transport_AddStudentTransferToBookingControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addtransfertobooking");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Transport/ViewBookingPage.aspx","SaveStudentTransferToBooking") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>

<p id="message" style="display: none">
    All fields must be completed
</p>

<form id="addtransfertobooking" name="addtransfertobooking" method="post" action="ViewBookingPage/SaveStudentTransferToBooking">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="TransportBookingId" value="<%= GetBookingId %>" />
            <label>
                Select Transfer to attach
            </label>
            <select id="StudentTransferId">
                <%= GetTransferList() %>
            </select>
        </fieldset>
    </div>
</form>