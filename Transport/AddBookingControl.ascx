﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddBookingControl.ascx.cs" 
Inherits="AddBookingControl" %>

<script type="text/javascript">
    $(function () {
        
        $("#RequestedDate").datepicker({ dateFormat : 'dd/mm/yy' });
        $("#ConfirmedDate").datepicker({ dateFormat : 'dd/mm/yy' });
        
        getForm = function () {
            return $("addbooking");
        }
        
        getTargetUrl = function () {
            return "ViewBookingPage/SaveBooking";
            
        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
        
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addbooking" name="addbooking" method="post" action="ViewBookingPage/SaveBooking">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="BookingId" value="<%= GetBookingId %>" />
            <label>
                Title
            </label>
            <input type="text" id="Title" value="<%= GetTitle %>" />
            <label>
                Trip Type
            </label>
            <select name="Xlk_TripType" id="TripTypeId">
                <%= GetTripTypeList() %>
            </select>
            <label>
                Vehicle Type
            </label>
            <%--<select name="Xlk_VehicleType" id="VehicleTypeId">
                <%= GetVehicleTypeList() %>
            </select>--%>
            <label>
                Status
            </label>
            <select name="Xlk_Status" id="StatusId">
                <%= GetStatusList() %>
            </select>
            <label>
                Requested Date
            </label>
            <input type="datetext" id="RequestedDate" value="<%= GetRequestedDate %>" />
            <label>
                Requested Time
            </label>
            <input type="datetext" id="RequestedTime" value="<%= GetRequestedTime %>" />
        </fieldset>
        <fieldset>
            <label>
                Number of Passengers
            </label>
            <input type="text" id="NoOfPassengers" value="<%= GetNoOfPassengers %>" />
            <label>
                Pickup Point
            </label>
            <selct id="PickupLocationId" >
                <%= GetPickupLocationList() %>
            </selct>
            <label>
                Pickup Location
            </label>
            <input type="text" id="PickupLocationInfo" value="<%= GetPickupLocationInfo %>" />
            <label>
                Drop Off Point
            </label>
            <select id="DropoffLocationId">
                <%= GetDropoffLocationList() %>
            </select>
            <label>
                Drop Off Location
            </label>
            <input type="text" id="DropoffLocationInfo" value="<%= GetDropoffLocationInfo %>" />
            <label>
                Rep Pickup Point
            </label>
            <select id="RepPickupLocationId">
                <%= GetRepPickupLocationList() %>
            </select>
        </fieldset>
        <fieldset>
            <label>
                Collection Info
            </label>
            <input type="text" id="CollectionInfo" value="<%= GetCollectionInfo %>" />
            <label>
                Dropoff Info
            </label>
            <input type="text" id="DropoffInfo" value="<%= GetDropoffInfo %>" />
            <label>
                Confirmed Date
            </label>
            <input type="datetext" id="ConfirmedDate" value="<%= GetConfirmedDate %>" />
            <label>
                Confirmed By
            </label>
            <select id="ConfirmedByUserId">
                <%= GetUserList() %>
            </select>
        </fieldset>
    </div>
</form>