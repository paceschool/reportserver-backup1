﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyPaymentControl.ascx.cs" 
    Inherits="Accommodation_AddFamilyPaymentControl" %>

<script type="text/javascript">
    $(function () {

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        getForm = function () {
            return $("#addfamilypayment");
        }

        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#ToDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#ProcessedDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getTargetUrl = function () {

            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyPayment") %>';
        }

        function daydiff(first, second) {
            return (second - first) / (1000 * 60 * 60 * 24) + 1;
        }

        loadStudents = function (familyid,startdate, enddate) {
         var params = '{familyid:' + familyid + ',startdate:"' + startdate.toDateString() + '", enddate:"' + enddate.toDateString() + '"}';
            $.ajax({
                type: "POST",
                url: '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","GetStudentCheckboxList") %>',
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                      if (msg.d != null) {
                        if (msg.d.Success) {
                               $("#students").html(msg.d.Payload);
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                        
                }
            });
        }



        $('.watcher').change(function () {
            loadStudents(<%= GetFamilyId %>,$("#FromDate").datepicker('getDate'), $("#ToDate").datepicker('getDate'));
            $('#DaysCovered').val(daydiff($("#FromDate").datepicker('getDate').getDate(), $("#ToDate").datepicker('getDate').getDate()));
        
        });
    });
</script>

<p id="message" style="display:none;">
    All fields must be completed
</p>

<form id="addfamilypayment" name="addfamilypayment" method="post" action="ViewAccommodationPage.aspx/SaveFamilyPayment" style="width:450px">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" name="Family" id="FamilyId" value="<%= GetFamilyId %>" />
            <input type="hidden" id="PaymentId" value="<%= GetPaymentId %>" />
            <label>
                Amount
            </label>
            <input type="text" name="Amount" id="Amount" value="<%= GetAmount %>" />
            <label>
                From Date
            </label>
            <input type="datetext" class="watcher" name="FromDate" id="FromDate" value="<%= GetFromDate %>" />
            <label>
                To Date
            </label>
            <input type="datetext" class="watcher" name="ToDate" id="ToDate" value="<%= GetToDate %>" />
                        <label>
                Days Covered
            </label>
            <input type="text"  id="DaysCovered" value="<%= GetDaysCovered %>" />
            <% if (!EditMode)
               {%>
            <label>
                Payment for the following students:
            </label>
            <div id="students">
                <%= GetStudentCheckboxList(DateTime.Now.ToString(), DateTime.Now.AddDays(-31).ToString())%>
            </div>
            <% }%>
            <label>
                Payment Reference
            </label>
            <input type="text" name="PaymentRef" id="PaymentRef" value="<%= GetPaymentRef %>" />
            <label>
                Comment
            </label>
            <textarea name="Comment" id="Comment"><%= GetComment %></textarea>
        </fieldset>
    </div>
</form>