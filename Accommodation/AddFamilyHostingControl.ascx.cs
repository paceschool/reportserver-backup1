﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Accommodation_AddFamilyHosting : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected FamilyBank _bank;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyId").HasValue)
            {
                Int32 _familyId = GetValue<Int32>("FamilyId");
            }
        }
    }

    protected string GetFamilyId
    {
        get
        {
            if (_bank != null && _bank.Family != null)
                return _bank.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }


    protected string GetArrivalDate
    {
        get
        {
            return DateTime.Now.ToString("dd/MM/yy");
        }
    }


    protected string GetDepartureDate
    {
        get
        {
            return DateTime.Now.AddDays(14).ToString("dd/MM/yy");
        }
    }
}
