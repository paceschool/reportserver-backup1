﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyMember : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected FamilyMember _member;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyMemberId").HasValue)
            {
                Int32 familyMemberId = GetValue<Int32>("FamilyMemberId");
                _member = (from members in entity.FamilyMember.Include("Xlk_FamilyMemberType").Include("Family")
                           where members.FamilyMemberId == familyMemberId
                           select members).FirstOrDefault();
            }
        }
    }


    protected string GetFamilyMemberTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_member != null && _member.Xlk_FamilyMemberType != null) ? _member.Xlk_FamilyMemberType.FamilyMemberTypeId : (byte?)null;

        foreach (Xlk_FamilyMemberType item in LoadFamilyMemberTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.FamilyMemberTypeId, item.Description, (_current.HasValue && item.FamilyMemberTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetFamilyMemberId
    {
        get
        {
            if (_member != null)
                return _member.FamilyMemberId.ToString();
            return "0";
        }
    }

    protected string GetFamilyId
    {
        get
        {
            if (_member != null)
                return _member.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();
            
            return "0";
        }
    }

    protected string GetMemberName
    {
        get
        {
            if (_member != null)
                return _member.Name.ToString();
            return null;
        }
    }

    protected string GetMemberDob
    {
        get
        {
            if (_member != null && _member.DOB.HasValue)
                return _member.DOB.Value.ToString("dd/MM/yy");
            return null;
        }
    }

    protected string GetMemberProfession
    {
        get
        {
            if (_member != null)
                return _member.Profession;
            return null;
        }
    }

    protected string GetMemberHobbies
    {
        get
        {
            if (_member != null)
                if (_member.Hobbies != null)
                    return _member.Hobbies.ToString();
            return null;
        }
    }

    protected string GetMemberMobile
    {
        get
        {
            if (_member != null)
                if (_member.MobileNumber != null)
                    return _member.MobileNumber.ToString();
            return null;
        }
    }

    protected string GetMemberIsOccupant
    {
        get
        {
            if (_member != null)
                if (_member.IsOccupant == true)
                    return "checked";

            return string.Empty;
        }
    }
}
