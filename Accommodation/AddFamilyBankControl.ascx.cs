﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyBank : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected FamilyBank _bank;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyBankBankId") .HasValue)
            {
                Int32 bankId = GetValue<Int32>("FamilyBankBankId");
                _bank = (from banks in entity.FamilyBank.Include("Family")
                         where banks.FamilyBankBankId == bankId
                         select banks).FirstOrDefault();
            }
        }
    }


    protected string GetFamilyId
    {
        get
        {
            if (_bank != null && _bank.Family != null)
                return _bank.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }

    protected string GetFamilyBankBankId
    {
        get
        {
            if (_bank != null)
                return _bank.FamilyBankBankId.ToString();
            return "0";
        }
    }

    protected string GetBankName
    {
        get
        {
            if (_bank != null)
                return _bank.BankName;
            return null;
        }
    }

    protected string GetBankBranch
    {
        get
        {
            if (_bank != null)
                return _bank.Branch;
            return null;
        }
    }

    protected string GetBankAccNo
    {
        get
        {
            if (_bank != null)
                return _bank.AccountNumber;
            return null;
        }
    }

    protected string GetBankSortCode
    {
        get
        {
            if (_bank != null)
                return _bank.SortCode;
            return null;
        }
    }

    protected string GetIBAN
    {
        get
        {
            if (_bank != null)
                return _bank.IBAN;
            return null;
        }
    }

    protected string GetBIC
    {
        get
        {
            if (_bank != null)
                return _bank.BIC;
            return null;
        }
    }
    protected string GetBankAccName
    {
        get
        {
            if (_bank != null)
                return _bank.AccountName;
            return null;
        }
    }

    protected string GetBankDateCreated
    {
        get
        {
            if (_bank != null)
                return _bank.DateCreated.ToString("dd/MM/yy");
            return DateTime.Now.ToString("dd/MM/yy");
        }
    }
}
