﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewAccommodationPage.aspx.cs" Inherits="Accommodation_ViewAccomodationPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {

            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                select: function (event, ui) { $('#calendar').fullCalendar('render'); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });

            start: $.now();
            end: $.now() + 15;

            checkLength = function (o, n, min, max) {
                if (o.val().length > max || o.val().length < min) {
                    o.addClass("ui-state-error");
                    updateTips("Length of " + n + " must be between " + min + " and " + max + ".");
                    return false;
                } else {
                    return true;
                }
            }

            deleteHosting = function (hostingid) {
                $("#dialog-message").html("<p class='validateTips'>All form fields are required.</p><form><div class='inputArea'><fieldset><input type='hidden' id='HostingId' value=" + hostingid + "></input><label for='folder'>Reason for cancellation</label><textarea rows='4' id='CancelledReason'></textarea></fieldset></div></form>").dialog("open");
            };

            $("#dialog-message").html("").dialog({
                autoOpen: false,
                height: 300,
                width: 350,
                modal: true,
                buttons: {
                    "Cancel Hosting": function () {
                        var bValid = true,
                        cancelledreason = $("#CancelledReason");
                        hostingid = $("#HostingId");
                        bValid = bValid && checkLength(cancelledreason, "CancelledReason", 3, 200);

                        if (bValid) {
                            $.ajax({
                                type: "POST",
                                url: "ViewAccommodationPage.aspx/DeleteHosting",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: "{hostingid:" + hostingid.val() + ",cancelledreason:'" + cancelledreason.val() + "'}",
                                success: function (msg) {
                                    if (msg.d != null) {
                                        if (msg.d.Success) {
                                            refreshCurrentTab();
                                            $("#dialog-message").dialog("close");
                                        }
                                        else
                                            alert(msg.d.ErrorMessage);
                                    }
                                    else
                                        alert("No Response, please contact admin to check!");
                                }
                            });
                        }
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                }
            });


        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="View Accommodation Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th>
                        Family Id
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curFamId" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Name
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curFamName" runat="server">
                        </asp:Label>
                        <%= PopUpLink("EnrollmentId", _familyId, "~/Accommodation/ViewAccommodationPage.aspx", "", "PopUpFullFamily")%>
                    </td>
                    <th>
                        Address
                    </th>
                    <td style="text-align: left">
                        <asp:HyperLink ID="curFamAdd" runat="server" Target="_blank" />
                    </td>
                </tr>
                <tr>
                    <th>
                        Zone
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curZone" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Phone
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curPhone" runat="server">
                        </asp:Label>
                        /
                        <asp:Label ID="curMob" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Email
                    </th>
                    <td id="Td1" style="text-align: left" runat="server">
                        <asp:HyperLink ID="curEmail" runat="server">
                        </asp:HyperLink>
                        <asp:Label ID="curemailaddr" runat="server">
                        </asp:Label>
                    </td>
                </tr>
             <tr>
                    <th>
                        Family Type
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curFamilyType" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Description
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curDescription" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Payment Method
                    </th>
                    <td id="Td2" style="text-align: left" runat="server">
                        <asp:Label ID="curPaymentMethod" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th>
                        Sage Ref
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curSage" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Status
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curStatus" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Actions
                    </th>
                    <td style="text-align: center">
                        <a title="Edit Family" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','AccommodationPage.aspx','Accommodation/AddFamilyControl.ascx',{'FamilyId':<%=_familyId %>},refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a> <a href="#" title="Print Options" onclick="showPrintOptions('#dialog-print');">
                            <img src="../Content/img/actions/bus.gif" title="Transport Options" /></a> <a href="<%= ReportPath("AccommodationReports","FamilySummary","HTML4.0", new Dictionary<string, object> { { "FamilyId", _familyId  } })%>"
                                title="Family Summary">
                                <img alt="Family Summary" src="../Content/img/actions/printer2.png" />
                            </a>
                    </td>
                </tr>
            </tbody>
        </table>
         </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="ViewAccommodationPage.aspx/LoadCurrentHostings?id=<%= _familyId %>">Students</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadPreviousStudents?id=<%= _familyId %>">Previous
                    Students</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyRooms?id=<%= _familyId %>">Rooms</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyMembers?id=<%= _familyId %>">Family
                    Members</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyComplaints?id=<%= _familyId %>">Complaints</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadNotes?id=<%= _familyId %>">Notes</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyVisits?id=<%= _familyId %>">Visits</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyAvailability?id=<%= _familyId %>">
                    Availability</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyBank?id=<%= _familyId %>">Bank Details</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyPayments?id=<%= _familyId %>">Payments</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyTags?id=<%= _familyId %>">Tags</a></li>
                <li><a href="ViewAccommodationPage.aspx/LoadFamilyFiles?id=<%= _familyId %>">Documents</a></li>
            </ul>
        </div>
    </div>
      <!-- Areas for showing in the dialog -->
    <div id="dialog-print" title="Print Option" style="display: none;">
        <fieldset>
            <a target="_blank" href="<%= AddressLink("PACE Language Institute,29 Dublin Rd, Bray, Wicklow, Ireland") %>"
                title="To Dart Station"><img src="../Content/img/actions/bus.gif" title="Transport Options" />To School
                List</a><br />
            <a target="_blank" href="<%= AddressLink("Wilton Hotel Bray (formerly the Ramada Hotel) Southern Cross Rd, Bray, Wicklow, Ireland") %>"
                title="To Wilton"><img src="../Content/img/actions/bus.gif" title="Transport Options" />To Wilton
                List</a><br />
            <a target="_blank" href="<%= AddressLink("Bray Train Station, Bray") %>"
                title="To Dart Station"><img src="../Content/img/actions/bus.gif" title="Transport Options" />To Dart Station
                List</a><br />
           <a target="_blank" href="<%= AddressLink("") %>"
                title="To Other"><img src="../Content/img/actions/bus.gif" title="Transport Options" />To Other
                List</a><br />
            <br />
        </fieldset>
    </div>
</asp:Content>
