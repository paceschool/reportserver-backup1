﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamily : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected Family _family;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyId").HasValue)
            {
                Int32 familyId = GetValue<Int32>("FamilyId");
                _family = (from s in entity.Families.Include("Xlk_FamilyType").Include("Xlk_PaymentMethod").Include("Xlk_Status").Include("Xlk_Zone")
                           where s.FamilyId == familyId
                           select s).FirstOrDefault();
            }
        }
    }

    protected string GetCampusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_family != null) ? _family.CampusId : GetCampusId;

        foreach (Pace.DataAccess.Security.Campus item in BasePage.LoadCampus)
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.CampusId, item.Name, (_current.HasValue && item.CampusId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetFamilyTypeList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_family != null && _family.Xlk_FamilyType != null) ? _family.Xlk_FamilyType.FamilyTypeId : (short?)null;

        foreach (Xlk_FamilyType item in LoadFamilyTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.FamilyTypeId, item.Description, (_current.HasValue && item.FamilyTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetPaymentMethodList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_family != null && _family.Xlk_PaymentMethod != null) ? _family.Xlk_PaymentMethod.PaymentMethodId : (short?)null;
        foreach (Xlk_PaymentMethod item in LoadPaymentMethods())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.PaymentMethodId, item.Description, (_current.HasValue && item.PaymentMethodId == _current) ? "selected" : string.Empty);

        }

        return sb.ToString();
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_family != null && _family.Xlk_Status != null) ? _family.Xlk_Status.StatusId : (int?)null;

        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetZoneList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_family != null && _family.Xlk_Zone != null) ? _family.Xlk_Zone.ZoneId : (int?)null;

        foreach (Xlk_Zone item in LoadZones())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ZoneId, item.Description, (_current.HasValue && item.ZoneId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }


    protected string GetFamilyId
    {
        get
        {
            if (_family != null)
                return _family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();
            
            return "0";
        }
    }

    protected string GetAddress
    {
        get
        {
            if (_family != null)
                return _family.Address;
            return null;
        }
    }


    protected string GetComment
    {
        get
        {
            if (_family != null)
                return _family.Comments;
            return null;
        }
    }
    protected string GetDescription
    {
        get
        {
            if (_family != null)
                return _family.Description;
            return null;
        }
    }
    protected string GetEmailAddress
    {
        get
        {
            if (_family != null)
                return _family.EmailAddress;
            return null;
        }
    }
    protected string GetFirstName
    {
        get
        {
            if (_family != null)
                return _family.FirstName;
            return null;
        }
    }
    protected string GetGeoCode
    {
        get
        {
            if (_family != null && _family.GeoCode != null)
                return _family.GeoCode;
            return null;
        }
    }

    protected string GetCustomFolderName
    {
        get
        {
            if (_family != null && _family.CustomFolderName != null)
                return _family.CustomFolderName;
            return null;
        }
    }
    protected string GetLandLine
    {
        get
        {
            if (_family != null)
                return _family.LandLine;
            return null;
        }
    }
    protected string GetMobile
    {
        get
        {
            if (_family != null)
                return _family.Mobile;
            return null;
        }
    }

    protected string GetSageRef
    {
        get
        {
            if (_family != null)
                return _family.SageRef;
            return null;
        }
    }    
    protected string GetSurName
    {
        get
        {
            if (_family != null)
                return _family.SurName;
            return null;
        }
    }
  
}
