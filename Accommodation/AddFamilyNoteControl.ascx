﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyNoteControl.ascx.cs"
    Inherits="Accommodation_AddNote" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addfamilynote");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyNote") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addfamilynote" name="addfamilynote" method="post" action="ViewAccommodationPage.aspx/SaveFamilyNote">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Family" id="FamilyId" value="<%= GetFamilyId %>" />
        <input type="hidden" id="FamilyNoteId" value="<%= GetFamilyNoteId %>" />
        <label>
            Type
        </label>
        <select type="text" name="Xlk_NoteType" id="NoteTypeId">
            <%= GetFamilyNoteTypeList()%>
        </select>
        <label>
            Created By
        </label>
        <select name="Users" id="UserId">
            <%= GetNoteUserList() %>
        </select>
        <label>
            Note
        </label>
        <textarea rows="4" type="text" id="Note"><%= GetNote %></textarea>
    </fieldset>
</div>
</form>
