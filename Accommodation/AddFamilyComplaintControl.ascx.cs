﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Accommodation_AddFamilyVisit : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected FamilyComplaint _complaint;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyComplaintId").HasValue)
            {
                Int32 complaintId = GetValue<Int32>("FamilyComplaintId");
                _complaint = (from complaints in entity.FamilyComplaint.Include("Xlk_ComplaintType").Include("Xlk_Severity").Include("Family").Include("Users")
                              where complaints.FamilyComplaintId == complaintId
                              select complaints).FirstOrDefault();

            }
        }
    }


    protected string GetComplaintTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_complaint != null && _complaint.Xlk_ComplaintType != null) ? _complaint.Xlk_ComplaintType.ComplaintTypeId : (byte?)null;

        foreach (Xlk_ComplaintType item in LoadComplaintTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ComplaintTypeId, item.Description, (_current.HasValue && item.ComplaintTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }
    protected string GetSeverityList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_complaint != null && _complaint.Xlk_Severity != null) ? _complaint.Xlk_Severity.SeverityId : (byte?)null;

        foreach (Xlk_Severity item in LoadSeverity())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.SeverityId, item.Description, (_current.HasValue && item.SeverityId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetComplaintUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_complaint != null && _complaint.Users != null) ? _complaint.Users.UserId : (Int32?)null;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetFamilyId
    {
        get
        {
            if (_complaint != null)
                return _complaint.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }

    protected string GetFamilyComplaintId
    {
        get
        {
            if (_complaint != null)
                return _complaint.FamilyComplaintId.ToString();
            return "0";
        }
    }

    protected string GetComplaintText
    {
        get
        {
            if (_complaint != null)
                return _complaint.Complaint;
            return null;
        }
    }

    protected string GetAction
    {
        get
        {
            if (_complaint != null)
                if (_complaint.ActionNeeded == true)
                    return "checked";

            return string.Empty;
        }
    }
}
