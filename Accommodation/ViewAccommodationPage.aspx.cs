﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using Pace.DataAccess.Accommodation;
using System.Text;

public partial class Accommodation_ViewAccomodationPage : BaseAccommodationPage
{
    protected Int32 _familyId;
    protected string _address;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["FamilyId"]))
            _familyId = Convert.ToInt32(Request["FamilyId"]);

        if (!Page.IsPostBack)
        {

            DisplayFamily(LoadFamily(new AccomodationEntities(),_familyId));

            if (Request.UrlReferrer != null)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected string AddressLink(string toaddress)
    {
        if (!string.IsNullOrEmpty(toaddress))
            return string.Format("http://hittheroad.ie/#from={0}&to={1}", _address, toaddress);
        else
            return string.Format("http://hittheroad.ie/#from={0}", _address);

    }

    private void SaveFamily(AccomodationEntities entity,Family family)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayFamily(LoadFamily(entity,_familyId));
        }
    }

    private void DisplayFamily(Family family)
    {
        if (family != null)
        {
            _address = (!string.IsNullOrEmpty(family.GeoCode)) ? family.GeoCode  : family.Address;
            curFamId.Text = family.FamilyId.ToString();
            curFamName.Text = family.FirstName + " " + family.SurName;
            curFamAdd.Text = TrimText(family.Address, 40);
            curFamAdd.ToolTip = family.Address;
            curFamAdd.NavigateUrl = String.Format("http://maps.google.com/maps?q=" + family.Address + "&output=embed");
            if (family.Xlk_Zone != null)
                curZone.Text = family.Xlk_Zone.Description;
            curPhone.Text = family.LandLine;
            curMob.Text = family.Mobile;
            if (!string.IsNullOrEmpty(family.EmailAddress))
            {
                curEmail.NavigateUrl = String.Format("MailTo:" + family.EmailAddress);
                curEmail.ImageUrl = "~/Content/img/actions/email_go.png";
                curEmail.Target = "_blank";
                curEmail.ToolTip = "Click to open email message window";
                curemailaddr.Text = family.EmailAddress;
            }
            else
            {
                curEmail.ImageUrl = "~/Content/img/email_not_available.jpg";
                curEmail.ToolTip = "None Available";
            }
            curFamilyType.Text = family.Xlk_FamilyType.Description;
            if (family.Xlk_PaymentMethod != null)
                curPaymentMethod.Text = family.Xlk_PaymentMethod.Description;
            curDescription.Text = TrimText(family.Description, 40);
            curSage.Text = family.SageRef;
            curStatus.Text = family.Xlk_Status.Description;
            
        }
    }

    private Family LoadFamily(AccomodationEntities entity,Int32 familyId)
    {
        IQueryable<Family> familyQuery = from f in entity.Families.Include("FamilyMembers").Include("FamilyMembers.Xlk_FamilyMemberType").Include("Xlk_FamilyType").Include("Xlk_Zone").Include("Xlk_Status").Include("Xlk_PaymentMethod")
                                         where f.FamilyId == familyId
                                         select f;

        if (familyQuery.ToList().Count() > 0)
            return familyQuery.ToList().First();

        return null;


    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> ListCalendarEvents(DateTime start, DateTime end, int id)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            var studentQuery = from h in entity.Hostings
                               where h.Family.FamilyId == id && ((h.ArrivalDate <= start && h.DepartureDate >= end) || (h.ArrivalDate >= start && h.DepartureDate <= end) || (h.ArrivalDate >= start && h.ArrivalDate <= end) || (h.DepartureDate >= start && h.DepartureDate <= end))
                               select new { Title = h.Student.FirstName + " " + h.Student.SurName, ArrivalDate = h.ArrivalDate, DepartureDate = h.DepartureDate };

            foreach (var item in studentQuery.ToList())
            {
                yield return new { Title = item.Title, ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString() };
            }
        }
    }

  

}
