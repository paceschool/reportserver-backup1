﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyControl.ascx.cs"
    Inherits="Accommodation_AddFamily" %>
<script type="text/javascript">
    $(function () {


        getForm = function () {
            return $("#addfamily");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamily") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addfamily" name="addfamily" method="post" action="ViewAccommodationPage.aspx/SaveFamily">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="FamilyId" value="<%= GetFamilyId %>" />
        <label>
            Family Type
        </label>
        <select type="text" name="Xlk_FamilyType" id="FamilyTypeId">
            <%= GetFamilyTypeList() %>
        </select>
        <label>
            Family First Name
        </label>
        <input id="FirstName" value="<%= GetFirstName %>" />
        <label>
            Family SurName
        </label>
        <input id="SurName" value="<%= GetSurName %>" />
        <label>
            Address
        </label>
        <input id="Address" value="<%= GetAddress %>" />
        <label>
            Zone
        </label>
        <select type="text" name="Xlk_Zone" id="ZoneId">
            <%= GetZoneList()%>
        </select>
    </fieldset>
    <fieldset>
        <label>
            email
        </label>
        <input id="EmailAddress" value="<%= GetEmailAddress %>" />
        <label>
            Landline
        </label>
        <input id="Landline" value="<%= GetLandLine %>" />
        <label>
            Mobile
        </label>
        <input id="Mobile" value="<%= GetMobile %>" />
        <label>
            SAGE Reference
        </label>
        <input id="SageRef" value="<%= GetSageRef %>" />
        <label>
            Status
        </label>
        <select type="text" name="Xlk_Status" id="StatusId">
            <%= GetStatusList()%>
        </select>
    </fieldset>
    <fieldset>
        <label>
            Campus
        </label>
        <select type="text" id="CampusId">
            <%= GetCampusList()%>
        </select>
        <label>
            GeoCode
        </label>
        <input id="GeoCode" value="<%= GetGeoCode %>" />
        <label>
            Payment Method
        </label>
        <select type="text" name="Xlk_PaymentMethod" id="PaymentMethodId">
            <%= GetPaymentMethodList()%>
        </select>
        <label>
            Custom Folder Name
        </label>
        <input id="CustomFolderName" value="<%= GetCustomFolderName %>" />
    </fieldset>
    <fieldset>
        <label>
            Description
        </label>
        <textarea rows="5" id="Description"><%= GetDescription %></textarea>
        <label>
            Comment
        </label>
        <textarea rows="5" id="Comments" ><%= GetComment %></textarea>
    </fieldset>
</div>
</form>
