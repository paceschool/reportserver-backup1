﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyMemberControl.ascx.cs"
    Inherits="Accommodation_AddFamilyMember" %>

<script type="text/javascript">
    $(function() {


        getForm = function() {
            return $("#addfamilymember");
        }

        $("#DOB").datepicker({ dateFormat: 'dd/mm/yy' });

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyMember") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display:none;">
    All fields must be completed</p>
<form id="addfamilymember" name="addfamilymember" method="post" action="ViewAccommodationPage.aspx/SaveFamilyMember">
<div class="inputArea">
    <fieldset>
      <input  type="hidden" name="Family" id="FamilyId" value="<%= GetFamilyId %>" />
      <input type="hidden" id="FamilyMemberId" value="<%= GetFamilyMemberId %>" />
        <label>
            First Name
        </label>
        <input type="text" id="Name" validation="required" value="<%= GetMemberName %>"/>
        <label>
            DOB
        </label>
        <input type="datetext" id="DOB" value="<%= GetMemberDob %>"/>
        <label>
            Profession
        </label>
        <input type="text" id="Profession" value="<%= GetMemberProfession%>"/>
        <label>
            Type
        </label>
        <select type="text" name="Xlk_FamilyMemberType" id="FamilyMemberTypeId">
        <%= GetFamilyMemberTypeList() %>
        </select>
        <label>
            Hobbies
        </label>
        <input type="text" id="Hobbies" value="<%= GetMemberHobbies %>" />
        <label>
            Mobile No.
        </label>
        <input type="text" id="MobileNumber" value="<%= GetMemberMobile %>" />
        <label>
            Is Family Member living in the House?
        </label>
        <input type="checkbox" id="IsOccupant" value="true"  <%= GetMemberIsOccupant %> />
    </fieldset>
</div>
</form>
