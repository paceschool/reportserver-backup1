﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyTag : BaseAccommodationControl
{
    protected Int32 _familyId;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string GetTagList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Tag item in LoadTagList())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.TagId, item.Text);
        }

        return sb.ToString();
    }
}
