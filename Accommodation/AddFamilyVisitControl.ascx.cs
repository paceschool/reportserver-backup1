﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyVisit : BaseAccommodationControl
{
    protected Int32 _familyId;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string GetFamilyVisitTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_FamilyVisitType item in LoadFamilyVisitTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>",item.FamilyVisitTypeId,item.Description );
        }

        return sb.ToString();

    }

}
