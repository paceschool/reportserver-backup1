﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyAvailability : BaseAccommodationControl
{

    protected Int32 _familyId;
    protected Availability _availability;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyAvailabilityId").HasValue)
            {
                Int32 availabilityId = GetValue<Int32>("FamilyAvailabilityId");
                _availability = (from avails in entity.Availability.Include("Xlk_AvailabilityType").Include("Family")
                                 where avails.AvailabilityId == availabilityId
                                 select avails).FirstOrDefault();
            }
        }
    }


    protected string GetFamilyAvailabilityTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_availability != null && _availability.Xlk_AvailabilityType != null) ? _availability.Xlk_AvailabilityType.AvailabilityTypeId : (byte?)null;

        foreach (Xlk_AvailabilityType item in LoadFamilyAvailabilityTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.AvailabilityTypeId, item.Description, (_current.HasValue && item.AvailabilityTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetAvailabilityId
    {
        get
        {
            if (_availability != null)
                return _availability.AvailabilityId.ToString();
            return "0";
        }
    }

    protected string GetFamilyId
    {
        get
        {
            if (_availability != null)
                return _availability.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }

    protected string GetFromDate
    {
        get
        {
            if (_availability != null)
                return _availability.FromDate.Value.ToString("dd/MM/yy");
            return null;
        }
    }

    protected string GetToDate
    {
        get 
        {
            if (_availability != null && _availability.ToDate.HasValue)
                return _availability.ToDate.Value.ToString("dd/MM/yy");
            return null;
        }
    }

    protected string GetAvailComment
    {
        get
        {
            if (_availability != null)
                return _availability.Comment;
            return null;
        }
    }
}
