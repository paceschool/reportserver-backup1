﻿#region Copyright
    //Copyright 2014, Hugh McDonnell, All rights reserved.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Web.Services;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;


public partial class Accommodation : BaseAccommodationPage
{

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            familytype.DataSource = this.LoadFamilyTypes();
            familytype.DataBind();

            famstatus.DataSource = this.LoadStatus();
            famstatus.DataBind();

        }

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
    }

    public override string TextValue()
    {
        return newPageName.Text;
    }

    protected void familytype_SelectedIndexChanged(object sender, EventArgs e)
    {
            Int32 _familyTypeId = Convert.ToInt32(familytype.SelectedItem.Value);
            using (AccomodationEntities entity = new AccomodationEntities())
            {
                IQueryable<Family> familySearchQuery = from f in entity.Families.Include("Xlk_Zone").Include("Xlk_Status")
                                                       where f.Xlk_FamilyType.FamilyTypeId == _familyTypeId && f.Xlk_Status.StatusId != 3 && f.CampusId == GetCampusId
                                                       orderby f.SurName, f.FirstName
                                                       select f;

                LoadResults(familySearchQuery.ToList());
            }
    }  


    #endregion


    #region Private Methods

    private void LoadResults(List<Family> family)
    {
            results.DataSource = family;
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", family.Count().ToString());
    }

    protected void familytype_DataBound(object sender, EventArgs e)
    {
        TypeChanged();
    }

    protected void Search_Click(object sender, EventArgs e)
    {
        Click_SearchFamilies();
    }

    private void Click_SearchFamilies()
    {

        int statusId = Convert.ToInt32(famstatus.SelectedItem.Value);
        int familytypeid = Convert.ToInt32(familytype.SelectedItem.Value);
        string searchString = RemoveDiacritics(lookupString.Text.Trim());

        using (AccomodationEntities entity = new AccomodationEntities())
        {

            List<Int32?> ids = entity.SearchFamilies((statusId > 0) ? statusId : (int?)null, (familytypeid > 0) ? familytypeid : (int?)null, searchString).ToList();


            IQueryable<Family> familySearchQuery = from f in entity.Families.Include("FamilyNotes").Include("FamilyNotes.Xlk_NoteType").Include("Xlk_Zone").Include("Xlk_Status")
                                                   where ids.Contains(f.FamilyId) && f.CampusId == GetCampusId
                                                   orderby f.SurName ascending, f.FirstName ascending
                                                   select f;


            LoadResults(familySearchQuery.OrderBy(s=>s.Xlk_Status.StatusId).Take(300).ToList());
            lookupString.Attributes.Add("onfocus", "this.select();"); 
        }
    }

    private void Click_LoadFamiliesInUse(int familyTypeid)
    {
        string _family = lookupString.Text;
        DateTime _now = DateTime.Now.Date;
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Family> familySearchQuery = ((from f in entity.Families.Include("FamilyNotes").Include("FamilyNotes.Xlk_NoteType").Include("Xlk_Zone").Include("Xlk_Status").Include("Xlk_FamilyType")
                                                     from h in f.Hostings
                                                     where f.Xlk_FamilyType.FamilyTypeId == familyTypeid && f.Xlk_Status.StatusId != 3//paying families type 
                                    && ((h.ArrivalDate <= _now && h.DepartureDate >= _now) || h.ArrivalDate >= _now) && f.CampusId == GetCampusId
                                                     orderby f.SurName ascending, f.FirstName ascending
                                                     select f).Distinct() as ObjectQuery<Family>).Include("FamilyNotes").Include("FamilyNotes.Xlk_NoteType").Include("Xlk_Zone").Include("Xlk_Status").Include("Xlk_FamilyType");

            LoadResults(familySearchQuery.ToList());
        }
    }

    protected void ShowAll_Click()
    {

        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Family> familySearchQuery = from f in entity.Families.Include("FamilyNotes").Include("FamilyNotes.Xlk_NoteType").Include("Xlk_Zone").Include("Xlk_Status").Include("Xlk_FamilyType")
                                                   where f.Xlk_Status.StatusId != 3 && f.CampusId == GetCampusId
                                                   orderby f.FirstName ascending, f.SurName ascending
                                                   select f;

            LoadResults(familySearchQuery.ToList());
        }
    }   
    
    private void TypeChanged()
    {
        Int32 _type = Convert.ToInt32(familytype.SelectedItem.Value);
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            var familyQuery = from f in entity.Families.Include("FamilyNotes").Include("FamilyNotes.Xlk_NoteType").Include("Xlk_Zone").Include("Xlk_Status")
                              where f.Xlk_FamilyType.FamilyTypeId == _type && f.Xlk_Status.StatusId != 3 && f.CampusId == GetCampusId
                              orderby f.FirstName ascending, f.SurName ascending
                              select f;

            LoadResults(familyQuery.ToList());
        }
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_SearchFamilies();
                break;
            case "wsinuse":
                Click_LoadFamiliesInUse(2);
                break;
            case "payinginuse":
                Click_LoadFamiliesInUse(1);
                break;
            case "all":
                ShowAll_Click();
                break;
            default:
                Click_SearchFamilies();
                break;
        }

    }

    protected int GetNumber(object notes)
    {
        if (notes != null)
            return ((EntityCollection<FamilyNote>)notes).Count;

        return 0;
    }

    #endregion



}

