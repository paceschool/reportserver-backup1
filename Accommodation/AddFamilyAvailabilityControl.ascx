﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyAvailabilityControl.ascx.cs" 
Inherits="Accommodation_AddFamilyAvailability" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addfamilyavailability");
        }

        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#ToDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyAvailability") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>

<p id="message" style="display:none;">
    All fields must be completed
</p>

<form id="addfamilyavailability" name="addfamilyavailability" method="post" action="ViewAccommodationPage.aspx/SaveFamilyAvailability">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" name="Family" id="FamilyId" value="<%= GetFamilyId %>" />
            <input type="hidden" id="AvailabilityId" value="<%= GetAvailabilityId %>" />
            <label>
                Availability Type
            </label>
            <select name="Xlk_AvailabilityType" id="AvailabilityTypeId">
                <%= GetFamilyAvailabilityTypeList() %>
            </select>
            <label>
                From Date
            </label>
            <input type="datetext" name="FromDate" id="FromDate" value="<%= GetFromDate %>"/>
            <label>
                To Date
            </label>
            <input type="datetext" name="ToDate" id="ToDate" value="<%= GetToDate %>"/>
            <label>
                Comment
            </label>
            <textarea id="Comment" cols="1" ><%= GetAvailComment %></textarea>
        </fieldset>
    </div>
</form>