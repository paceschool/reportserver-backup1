﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyHostingControl.ascx.cs"
    Inherits="Accommodation_AddFamilyHosting" %>
<script type="text/javascript">
    $(function () {


        getForm = function () {
            return $("#addfamilyhosting");
        }

        findStudents = function (startdate, enddate) {
            var params = '{fromdate:"\\\/Date(' + startdate.getTime() + ')\\\/", todate:"\\\/Date(' + enddate.getTime() + ')\\\/"}';
            $.ajax({
                type: "POST",
                url: "ViewAccommodationPage.aspx/GetStudentList",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $("#studentslist").html(msg.d.Payload);
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        }

        $("#ArrivalDate").datepicker({
            dateFormat: 'dd/mm/yy',
            onSelect: function (dateText) {
                findStudents($("#ArrivalDate").datepicker("getDate"), $("#DepartureDate").datepicker("getDate"));
            }
        });

        $("#DepartureDate").datepicker({
            dateFormat: 'dd/mm/yy',
            onSelect: function (dateText) {
                findStudents($("#ArrivalDate").datepicker("getDate"), $("#DepartureDate").datepicker("getDate"));
            }
        });

        findStudents($("#ArrivalDate").datepicker("getDate"), $("#DepartureDate").datepicker("getDate"));

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyhosting") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
           
</script>
<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addfamilyhosting" name="addfamilyhosting" method="post" action="ViewAccommodationPage.aspx/SaveFamilyhosting">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Family" id="FamilyId" value="<%= GetFamilyId %>" />
        <label for="WeeklyRate">
            Weekly Rate</label>
        <input type="text" id="WeeklyRate" value="0" />
        <label for="ArrivalDate">
            Start Date</label>
        <input type="datetext" name="ArrivalDate" id="ArrivalDate" value="<%= GetArrivalDate %>" />
        <label for="DepartureDate">
            End Date</label>
        <input type="datetext" name="DepartureDate" id="DepartureDate" value="<%= GetDepartureDate %>" />
        <label for="family">
            Family</label>
        <div id="studentslist">
            <table class="box-table-a">
                <thead>
                    <tr>
                        <th scope="col">
                        </th>
                        <th scope="col">
                            Name
                        </th>
                        <th scope="col">
                            Age
                        </th>
                        <th scope="col">
                            DOB
                        </th>
                        <th scope="col">
                            Gender
                        </th>
                        <th scope="col">
                            Nationalities
                        </th>
                    </tr>
                </thead>
                <tbody>
                <tr><td colspan="6">No Students to Show!</td></tr>
                </tbody>
            </table>
        </div>
    </fieldset>
</div>
</form>
