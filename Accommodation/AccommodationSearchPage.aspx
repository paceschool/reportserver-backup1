﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AccommodationSearchPage.aspx.cs" Inherits="Accommodation_AccommodationSearchPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {

            var showLoader = false;

            $('button.updateButton').hide();

            $("#accordionMenu").accordion({
                header: "a",
                active: false,
                autoHeight: false,
                collapsible: true,
                changestart: function (event, ui) {
                    var clicked = $(this).find('.ui-state-active').attr('id');

                }
            });

            $("#ArrivalDate").datepicker({
                dateFormat: 'dd/mm/yy'
            });

            $("#DepartureDate").datepicker({
                dateFormat: 'dd/mm/yy'
            });

            $("button#btnKeywordSearch").click(function (e) {
                submitForm();
                return false;
            });

            $("button#drawerUpdateButton.updateButton").click(function (e) {
                submitForm();
                return false;
            });

            $(".watcher").change(function (e) {

                $(e.currentTarget.parentNode.parentNode.parentNode).find("button#drawerUpdateButton.updateButton").show();

            });

            getForm = function () {
                return $('#refinements');
            }

            getTargetUrl = function () {
                return "AccommodationSearchPage.aspx/GetFamiliesWithFilter";
            }

            function submitForm() {
                showLoader = true;
                $.ajax({
                    type: "POST",
                    url: getTargetUrl(),
                    data: JSON.stringify(buildDTO()),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                $('#filterresultscontainer').html(msg.d.Payload);
                                createPopUp();
                                $('button.updateButton').hide();
                                showLoader = false;
                            }
                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");

                    },
                    complete: function (data) {
                        showLoader = false;
                    }
                });
            }

            $('#loadingDiv').hide()  // hide it initially
                .ajaxStart(function () {
                    if (showLoader)
                        $(this).show();
                })
                .ajaxStop(function () {
                    $(this).hide();
                });


        });
    </script>
    <style type="text/css">
        .DL-styled-module .DL-module-content-wrapper
        {
            margin: 2px;
        }
        .DL-non-content-region .DL-styled-module, .DL-non-content-region .DL-styled-module h3.DL-module-title, .DL-non-content-region .DL-styled-module h4.DL-subtitle, .DL-non-content-region .DL-styled-module .DL-module-content-wrapper
        {
            border: 0 none;
            clear: none;
            margin: 0;
            padding: 0;
        }
        
        .DL-topic-timeline ol.DL-timelines
        {
            height: 20px;
            margin: 0 auto;
            padding: 0 0 5px 2px;
            width: auto;
        }
        
        .DL-topic-timeline-page .DL-topic-timeline ol.DL-timelines, .DL-topic-timeline-header ol.DL-timelines
        {
            height: 160px;
        }
        .DL-topic-timeline ol.DL-timelines li
        {
            display: block;
            float: left;
            height: 20px;
            position: relative;
            width: 3px;
        }
        .DL-topic-timeline-page .DL-topic-timeline ol.DL-timelines li, .DL-topic-timeline-header ol.DL-timelines li
        {
            height: 160px;
            width: 10px;
        }
        .DL-topic-timeline ol.DL-timelines li.DL-last
        {
            width: 5px;
        }
        .DL-topic-timeline ol.DL-timelines li.DL-last a
        {
            border-right: 0 none;
        }
        
        .DL-topic-timeline ol.DL-timelines li a
        {
            border-right: 0 none;
            display: block;
            height: 20px;
            padding-right: 2px;
            width: 1px;
        }
        
        .DL-topic-timeline-page .DL-topic-timeline ol.DL-timelines li a, .DL-topic-timeline-header ol.DL-timelines li a
        {
            border-right: 0 none;
            height: 160px;
            padding-right: 5px;
        }
        .DL-topic-timeline ol.DL-timelines li span.top
        {
            background: none repeat scroll 0 0 #E5F1F4;
            cursor: pointer;
            display: block;
        }
        .DL-topic-timeline ol.DL-timelines li span.hols
        {
            background: none repeat scroll 0 0 #F59671;
            display: block;
        }
        .DL-topic-timeline ol.DL-timelines li span.bottom
        {
            background: none repeat scroll 0 0 #3399CC;
            cursor: pointer;
            display: block;
        }
        .DL-topic-timeline ol.DL-timelines li .DL-timeline-zoom
        {
            background: url("http://diacache.daylife.com/_static/v2/img/baseline/timeline_popup.png") repeat scroll 0 0 transparent;
            display: none;
            font-size: 10px;
            height: 33px;
            line-height: 10px;
            margin-left: 0;
            padding: 2px 0 0 33px;
            position: absolute;
            top: -16px;
            width: 127px;
            z-index: 99;
        }
        .DL-topic-timeline ol.DL-timelines li .DL-timeline-zoom-arrow
        {
            background: url("http://diacache.daylife.com/_static/v2/img/baseline/timeline_popup_arrow.png") no-repeat scroll 0 0 transparent;
            display: none;
            height: 6px;
            left: -2px;
            position: absolute;
            top: 8px;
            width: 9px;
            z-index: 100;
        }
        .DL-topic-timeline ol.DL-timelines li .DL-timeline-date
        {
            color: #547289;
            display: none;
            font-size: 11px;
            font-weight: bold;
            height: auto;
            line-height: 11px;
            position: absolute;
            text-align: center;
            top: 163px;
            width: 100px;
            z-index: 100;
        }
        .DL-topic-timeline ol.DL-timelines li .DL-timeline-count
        {
            display: none;
        }
        .DL-topic-timeline ol.DL-timelines li.DL-active
        {
            z-index: 1;
        }
        .DL-topic-timeline ol.DL-timelines li a:hover span.top
        {
            background: none repeat scroll 0 0 #C6DAE0;
            display: block;
        }
        .DL-topic-timeline ol.DL-timelines li a:hover span.hols
        {
            background: none repeat scroll 0 0 #FA0000;
            display: block;
        }
        .DL-topic-timeline ol.DL-timelines li.DL-active span.bottom
        {
            background: none repeat scroll 0 0 #297CA5;
            display: block;
        }
        .DL-topic-timeline ol.DL-timelines li.DL-active .DL-timeline-zoom, .DL-topic-timeline ol.DL-timelines li.DL-active .DL-timeline-zoom-arrow, .DL-topic-timeline-widget ol.DL-timelines li.DL-active:hover .DL-timeline-zoom, .DL-topic-timeline-widget ol.DL-timelines li.DL-active:hover .DL-timeline-zoom-arrow
        {
            display: block;
        }
        .DL-topic-timeline ol.DL-timelines li.DL-query-date .DL-timeline-zoom, .DL-topic-timeline ol.DL-timelines li.DL-query-date .DL-timeline-zoom-arrow
        {
            display: none;
        }
        .DL-topic-timeline #timeline_articles_wrapper
        {
            float: left;
            font-size: 12px;
        }
        .DL-topic-timeline #timeline_articles_wrapper #timeline_articles
        {
            color: #547289;
            font-size: 12px;
            font-weight: bold;
        }
        .DL-topic-timeline #timeline_date
        {
            color: #547289;
            float: right;
            font-size: 12px;
            font-weight: bold;
        }
        .DL-topic-timeline .DL-timeline-header
        {
        }
        .DL-topic-timeline .DL-timeline-header img
        {
            float: left;
            margin-right: 10px;
        }
        .DL-topic-timeline .DL-timeline-header
        {
            color: #333333;
            font-size: 28px;
            font-weight: bold;
            position: relative;
            width: 70%;
            z-index: 100;
        }
        .DL-topic-timeline .DL-timeline-header span.DL-timeline-tagline
        {
            color: #666666;
            display: block;
            font-size: 16px;
            margin-bottom: 1px;
        }
        .DL-topic-timeline .DL-timeline-header span.DL-topic-name
        {
            color: #0071BC;
            display: block;
            line-height: 26px;
            width: 525px;
        }
        .DL-topic-timeline .DL-timeline-header .DL-share-insert
        {
            margin-top: 3px;
        }
        .DL-topic-timeline-header #timeline_articles_wrapper
        {
            color: #666666;
            font-size: 14px;
            margin-top: 7px;
        }
        
        button.updateButton
        {
            background: url("../Content/img/filter/btnUpdateResults.png") no-repeat scroll 0 0 transparent;
            border: medium none;
            height: 18px;
            outline: medium none;
            position: absolute;
            right: -25px;
            text-indent: -999em;
            top: 6px;
            width: 47px;
        }
        #keywordSearchArea button#btnKeywordSearch
        {
            background: url("../Content/img/filter/btnKeywordSearch.gif") no-repeat scroll 0 0 transparent;
            border: 0 none;
            color: #FFFFFF;
            font-family: tahoma,Helvetica,sans-serif;
            font-size: 11px;
            font-weight: bold;
            height: 22px;
            margin: 0 0 0 4px;
            padding: 0 2px;
            text-align: center;
            width: 47px;
        }
        
        table a, table a:link, table a:visited
        {
            border: medium none;
        }
        img
        {
            border: 0 none;
            margin-top: 0.5em;
        }
        .accommodationresultstable table
        {
            border-collapse: collapse;
            border-right: 1px solid #E5EFF8;
            border-top: 1px solid #E5EFF8;
            margin: 1em auto;
            width: 90%;
        }
        .accommodationresultstable caption
        {
            caption-side: top;
            color: #9BA9B4;
            font-size: 0.94em;
            letter-spacing: 0.1em;
            margin: 1em 0 0;
            padding: 0;
            text-align: center;
        }
        .accommodationresultstable tr.odd td
        {
            background: none repeat scroll 0 0 #F7FBFF;
        }
        .accommodationresultstable tr.odd .column1
        {
            background: none repeat scroll 0 0 #F4F9FE;
        }
        .accommodationresultstable .column1
        {
            background: none repeat scroll 0 0 #F9FCFE;
        }
        .accommodationresultstable td
        {
            border-bottom: 1px solid #E5EFF8;
            border-left: 1px solid #E5EFF8;
            color: #678197;
            text-align: center;
        }
        .accommodationresultstable th
        {
            border-bottom: 1px solid #E5EFF8;
            border-left: 1px solid #E5EFF8;
            color: #678197;
            font-weight: normal;
            text-align: left;
        }
        .accommodationresultstable thead th
        {
            background: none repeat scroll 0 0 #F4F9FE;
            color: #66A3D3;
            font: bold 1.2em/2em "Century Gothic" , "Trebuchet MS" ,Arial,Helvetica,sans-serif;
            text-align: center;
        }
        .accommodationresultstable tfoot th
        {
            background: none repeat scroll 0 0 #F4F9FE;
            text-align: center;
        }
        .accommodationresultstable tfoot th strong
        {
            color: #66A3D3;
            font: bold 1.2em "Century Gothic" , "Trebuchet MS" ,Arial,Helvetica,sans-serif;
            margin: 0.5em 0.5em 0.5em 0;
        }
        .accommodationresultstable tfoot th em
        {
            color: #F03B58;
            font-size: 1.1em;
            font-style: normal;
            font-weight: bold;
        }
        .inputContainer
        {
            padding-left: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Advanced Page" BorderStyle="None" Visible="false"></asp:Label><!-- Page Name -->
    <br /> 
    <div id="filtercontainer" style="width: 230px;">
        <div id="breadcrumbArea">
            <h3>
                You Have Chosen:</h3>
            <ul>
                <em></em>
            </ul>
            <a id="clearBreadCrumbs" onclick="javascript:return false;" href="#">Clear All Selections</a>
            <div class="breadcrumbFooter">
            </div>
            <input type="submit" id="submit" name="submit" value="submit" disabled="disabled" /><br />
        </div>
        <div id="refinements">
            <div id="keywordSearchArea">
                <h3>
                    Keyword Search</h3>
                <p id="searchInput">
                    <input id="keywordSearch" type="text" onkeyup="checkCR(event)" maxlength="30" value=""
                        name="keywords">
                    <button id="btnKeywordSearch">
                        Search</button>
                    <span>eg. family name, tag etc.</span>
                </p>
            </div>
            <ul id="accordionMenu">
                <li id="dateDrawer"><a href="#" id="dates" class="navBlock ui-accordion-header" tabindex="0">
                    Dates</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <label for="ArrivalDate">
                                Start Date</label>
                            <input class="watcher" type="datetext" name="ArrivalDate" id="ArrivalDate" value="<%= LoadOptions("arrivaldate")%>" />
                            <label for="DepartureDate">
                                End Date</label>
                            <input class="watcher" type="datetext" name="DepartureDate" id="DepartureDate" value="<%= LoadOptions("departuredate")%>" />
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="familytype"><a href="#" id="typeid" class="navBlock ui-accordion-header"
                    tabindex="0">Family Type</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("familytype")%>
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="nationalities"><a href="#" id="years" class="navBlock ui-accordion-header"
                    tabindex="0">Nationalities</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("nationalities")%>
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="Li1"><a href="#" id="A1" class="navBlock ui-accordion-header" tabindex="0">Gender</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("gender")%>
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="Li4"><a href="#" id="A3" class="navBlock ui-accordion-header" tabindex="0">Status</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("status")%>
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="Li2"><a href="#" id="groups" class="navBlock ui-accordion-header" tabindex="0">
                    Groups</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("groups") %>
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="priceRangeDrawer"><a href="#" id="priceRange" class="navBlock ui-accordion-header"
                    tabindex="0">Zones</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("zones")%>
                            <!-- /priceRangeSliderContainer -->
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="Li3"><a href="#" id="A2" class="navBlock ui-accordion-header" tabindex="0">Family
                    Member Age</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="transmissionTypesDrawer"><a href="#" id="transmission" class="navBlock ui-accordion-header"
                    tabindex="0">Family Member Type</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("familymembertype")%>
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
                <li id="colourDrawer"><a href="#" id="colour" class="navBlock ui-accordion-header"
                    tabindex="0">Room Types</a>
                    <div class="refinementContainer">
                        <div class="inputContainer">
                            <%= LoadOptions("roomtypes")%>
                        </div>
                    </div>
                    <!-- refinementContainer -->
                    <button class="updateButton" id="drawerUpdateButton">
                        Update</button>
                </li>
            </ul>
        </div>
    </div>
    <div id="filterresultscontainer">
        <table summary="Filter families list" class="accommodationresultstable">
            <caption>
                From Aug 2012 to Oct 2012</caption>
            <thead>
                <tr class="odd">
                    <td class="column1">
                        Month
                    </td>
                    <th scope="col" abbr="8">
                        8
                    </th>
                    <th scope="col" abbr="9">
                        9
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="column1" scope="row">
                        <a rel="451,/PaceManager/Accommodation/ViewAccommodationPage.aspx/PopupFamily" class="popupTrigger"
                            href="/PaceManager/Accommodation/ViewAccommodationPage.aspx?FamilyId=451">Anne Maher</a>
                        Cap : 2
                    </th>
                    <td colspan="2">
                        <div class="DL-topic-timeline DL-topic-timeline-widget DL-styled-module DL-section DL-module DL-css">
                            <div class="DL-module-content-wrapper">
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
