﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyVisitControl.ascx.cs"
    Inherits="Accommodation_AddFamilyVisit" %>

<script type="text/javascript">
    $(function() {


        getForm = function() {
            return $("#addfamilyvisit");
        }

        $("#DateVisited").datepicker({ dateFormat: 'dd/mm/yy' });

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyVisit") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addfamilyvisit" name="addfamilyvisit" method="post" action="ViewAccommodationPage.aspx/SaveFamilyMember">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Family" id="FamilyId" value="<%= GetValue<Int32>("FamilyId") %>" />
        <label>
            Type
        </label>
        <select name="Xlk_FamilyVisitType" id="FamilyVisitTypeId">
            <%= GetFamilyVisitTypeList()%>
        </select>
        <label>
            Visited By
        </label>
        <select name="Users" id="UserId">
            <%= GetUserList() %>
        </select>
                <label>
            Date Visited
        </label>
        <input type="datetext" name="DateVisited" id="DateVisited" />
        <label>
            Comment
        </label>
        <textarea id="Comment" cols="1" />
    </fieldset>
</div>
</form>
