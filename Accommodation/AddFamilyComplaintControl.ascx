﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyComplaintControl.ascx.cs"
    Inherits="Accommodation_AddFamilyVisit" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addfamilycomplaint");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyComplaint") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addfamilycomplaint" name="addfamilycomplaint" method="post" action="ViewAccommodationPage.aspx/SaveFamilyComplaint">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Family" id="FamilyId" value="<%= GetFamilyId %>" />
        <input type="hidden" id="FamilyComplaintId" value="<%= GetFamilyComplaintId %>" />
                <label>
            Severity
        </label>
        <select type="text" name="Xlk_Severity" id="SeverityId">
            <%= GetSeverityList() %>
        </select>
        <label>
            Type
        </label>
        <select type="text" name="Xlk_ComplaintType" id="ComplaintTypeId">
            <%= GetComplaintTypeList() %>
        </select>
        <label>
            Complaint
        </label>
        <textarea id="Complaint" ><%= GetComplaintText %></textarea>
        <label>
            Action Needed
        </label>
        <input type="checkbox" id="ActionNeeded" <%= GetAction %> value="true"/>
                <label>
            Raised By
        </label>
        <select type="text" name="Users" id="UserId">
            <%= GetComplaintUserList() %>
        </select>
    </fieldset>
</div>
</form>
