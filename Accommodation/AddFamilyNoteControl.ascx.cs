﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddNote : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected FamilyNote _note;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyNoteId").HasValue)
            {
                Int32 familynoteId = GetValue<Int32>("FamilyNoteId");
                _note = (from n in entity.FamilyNote.Include("Family").Include("Users").Include("Xlk_NoteType")
                         where n.FamilyNoteId == familynoteId
                         select n).FirstOrDefault();
            }
        }
    }

    protected string GetFamilyId
    {
        get
        {
            if (_note != null)
                return _note.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }

    protected string GetFamilyNoteId
    {
        get
        {
            if (_note != null)
                return _note.FamilyNoteId.ToString();

            if (Parameters.ContainsKey("FamilyNoteId"))
                return Parameters["FamilyNoteId"].ToString();

            return "0";
        }
    }

    protected string GetFamilyNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_note != null && _note.Xlk_NoteType != null) ? _note.Xlk_NoteType.NoteTypeId : (byte?)null;

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option{2} value={0}>{1}</option>",item.NoteTypeId,item.Description, (_current.HasValue && item.NoteTypeId == _current ) ? "current" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNoteUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_note != null && _note.Users != null) ? _note.Users.UserId : (Int32?)null;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNote
    {
        get
        {
            if (_note != null)
                return _note.Note.ToString();

            return null;
        }
    }
}
