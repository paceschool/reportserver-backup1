﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyRoom : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected Room _room;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("RoomId").HasValue)
            {
                Int32 roomId = GetValue<Int32>("RoomId");
                _room = (from rooms in entity.Room.Include("Xlk_RoomType").Include("Family")
                         where rooms.RoomId == roomId
                         select rooms).FirstOrDefault();

            }
        }
    }


    protected string GetRoomTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_room != null && _room.Xlk_RoomType != null) ? _room.Xlk_RoomType.RoomTypeId : (byte?)null;

        foreach (Xlk_RoomType item in LoadRoomTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.RoomTypeId, item.Description, (_current.HasValue && item.RoomTypeId == _current) ? "selected" :string.Empty);
        }

        return sb.ToString();

    }

    protected string GetRoomId
    {
        get
        {
            if (_room != null)
                return _room.RoomId.ToString();
            return "0";
        }
    }

    protected string GetFamilyId
    {
        get
        {
            if (_room != null)
                return _room.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }

    protected string GetRoomDescription
    {
        get
        {
            if (_room != null)
                return _room.Description;

                return null;
        }
    }

    protected string GetNoOfDoubleBeds
    {
        get
        {
            if (_room != null)
                return _room.NumberofDoubleBeds.ToString();

            return null;
        }
    }

    protected string GetNoOfSingleBeds
    {
        get
        {
            if (_room != null)
                return _room.NumberofSingleBeds.ToString();

            return null;
        }
    }

    protected string GetEnsuite
    {
        get
        {
            if (_room != null)
                if (_room.Ensuite == true)
                    return "checked";

            return string.Empty;
        }
    }
}
