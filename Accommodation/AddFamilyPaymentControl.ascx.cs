﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyPaymentControl : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected FamilyPayment _payment;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("FamilyPaymentId").HasValue)
            {
                Int32 _paymentId = GetValue<Int32>("FamilyPaymentId");
                _payment = (from p in entity.FamilyPayments.Include("Family")
                            where p.PaymentId == _paymentId
                            select p).FirstOrDefault();
            }
            _familyId = Convert.ToInt32(GetFamilyId);
        }
    }

    public string GetStudentCheckboxList(object startdate, object enddate)
    {
        DateTime _startDate = DateTime.Parse(startdate.ToString());
        DateTime _endDate = DateTime.Parse(enddate.ToString());
        StringBuilder _reposnse = new StringBuilder();
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            var studentQuery = from s in entity.Hostings.Include("Student")
                               where s.Family.FamilyId == _familyId && s.DepartureDate > _endDate && s.ArrivalDate < _startDate
                               select new { hostingid = s.HostingId, studentname = s.Student.FirstName + " " + s.Student.SurName, s.Student.StudentId };

            foreach (var hosting in studentQuery.ToList())
            {
                _reposnse.AppendFormat("<label style=\"display:block;\"><input name=\"Hostingids\" id=\"HostingId\" style=\"width:50px;float:left;\" type=\"checkbox\" value=\"{0}\" />{1}</label><br />", hosting.hostingid, hosting.studentname);
            }
        }
        return _reposnse.ToString();
    }

    protected bool EditMode
    {
        get
        {
            if (_payment != null)
                return _payment.PaymentId > 0;

            if (Parameters.ContainsKey("PaymentId"))
                return ((int)Parameters["PaymentId"]) > 0;

            return false; ;
        }
    }

    //protected ListItem GetCheckedStudents()
    //{
    //    List<ListItem> checkedItems = new List<ListItem>();

    //    // Loop through the list items in the control.
    //    foreach (ListItem item in StudentList.Items)
    //    {
    //        if (item.Selected)
    //            checkedItems.Add(item);
    //    }

    //    return checkedItems;
    //}


    protected string GetFamilyId
    {
        get
        {
            if (_payment != null)
                return _payment.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }

    protected string GetPaymentId
    {
        get
        {
            if (_payment != null)
                return _payment.PaymentId.ToString();

            if (Parameters.ContainsKey("PaymentId"))
                return Parameters["PaymentId"].ToString();

            return "0";
        }
    }

    protected string GetProcessedDate
    {
        get
        {
            if (_payment != null)
                return _payment.ProcessedDate.ToString("dd/MM/yy");

            return null;
        }
    }
    protected string GetDaysCovered
    {
        get
        {
            if (_payment != null && _payment.DaysCovered.HasValue)
                return _payment.DaysCovered.Value.ToString();

            return null;
        }
    }
    
    protected string GetAmount
    {
        get
        {
            if (_payment != null)
                return _payment.Amount.ToString("00.00");

            return "0";
        }
    }

    protected string GetFromDate
    {
        get
        {
            if (_payment != null)
                return _payment.FromDate.ToString("dd/MM/yy");

            return DateTime.Now.AddDays(-31).ToString("dd/MM/yy");
        }
    }

    protected string GetToDate
    {
        get
        {
            if (_payment != null)
                return _payment.ToDate.ToString("dd/MM/yy");

            return DateTime.Now.ToString("dd/MM/yy");
        }
    }

    protected string GetPaymentRef
    {
        get
        {
            if (_payment != null)
                return _payment.PaymentRef;

            return null;
        }
    }

    protected string GetComment
    {
        get
        {
            if (_payment != null)
                return _payment.Comment;

            return null;
        }
    }

    protected string GetStudentList()
    {
        if (_payment != null)
        {
            
        }

        return null;
    }
}