﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyTagControl.ascx.cs" 
Inherits="Accommodation_AddFamilyTag" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addfamilytag");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveFamilyTag") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>

<p id="message" style="Display:none;">
    All fields must be completed
</p>
<form id="addfamilytag" name="addfamilytag" method="post" action="ViewAccommodationPage.aspx/SaveFamilyTag">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="ParentId" value="<%= GetValue<Int32>("FamilyId") %>" />
            <label>
                Select Tag
            </label>
            <select name="Child" id="TagId">
                <%= GetTagList() %>
            </select>
        </fieldset>
    </div>
</form>