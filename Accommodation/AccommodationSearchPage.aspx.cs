﻿#region Copyright
//Copyright 2014, Hugh McDonnell, All rights reserved.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Accommodation_AccommodationSearchPage : BaseAccommodationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected string LoadOptions(string sender)
    {

        switch (sender.ToLower())
        {
            case "nationalities":
                return LoadNationalities();
            case "groups":
                return LoadGroups();
            case "zones":
                return LoadZones();
            case "hostingtype":
                return LoadNationalities();
            case "roomtypes":
                return LoadRoomTypes();
            case "gender":
                return LoadGenders();
            case "familymembertype":
                return LoadFamilyMemberTypes();
            case "familytype":
                return LoadFamilyTypes();
            case "status":
                return LoadStatus();
            case "arrivaldate":
                return new DateTime(DateTime.Now.Year,DateTime.Now.Month,1).Date.ToString("dd/MM/yy") ;
            case "departuredate":
                return DateTime.Now.AddDays(14).ToString("dd/MM/yy");
            default:
                return String.Empty;
        }
    }

    private string LoadStatus()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {

            var list = from x in entity.Xlk_Status
                       select x;

            foreach (var item in list.ToList())
            {
                sb.AppendFormat("<input class=\"watcher\" id=\"status{0}\" name=\"status\" type=\"checkbox\" value=\"{0}\"/>{1}<br />", item.StatusId, item.Description);
            }


        }
        return sb.ToString();
    }

    private string LoadFamilyTypes()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {

            var list = from x in entity.Xlk_FamilyType
                       select x;

            foreach (var item in list.ToList())
            {
                sb.AppendFormat("<input class=\"watcher\" id=\"familytype{0}\" name=\"familytype\" type=\"checkbox\" value=\"{0}\" />{1}<br />", item.FamilyTypeId, item.Description);
            }
        }
        return sb.ToString();
    }

    private string LoadFamilyMemberTypes()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {

            var list = from x in entity.Xlk_FamilyMemberType
                       select x;

            foreach (var item in list.ToList())
            {
                sb.AppendFormat("<input class=\"watcher\" id=\"familymembertype{0}\" name=\"familymembertype\" type=\"checkbox\" value=\"{0}\" />{1}<br />", item.FamilyMemberTypeId, item.Description);
            }
        }
        return sb.ToString();
    }

    private string LoadGenders()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {

            var list = from x in entity.Xlk_Gender
                       select x;

            foreach (var item in list.ToList())
            {
                sb.AppendFormat("<input class=\"watcher\" id=\"gender{0}\" name=\"gender\" type=\"checkbox\" value=\"{0}\"/>{1}<br />", item.GenderId, item.Description);
            }


        }
        return sb.ToString();
    }

    private string LoadRoomTypes()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {

            var list = from x in entity.Xlk_RoomType
                       orderby x.Description
                       select x;

            foreach (var item in list.ToList())
            {
                sb.AppendFormat("<input class=\"watcher\" id=\"roomtype{0}\" name=\"roomtype\" type=\"checkbox\" value=\"{0}\"/>{1}<br />", item.RoomTypeId, item.Description);
            }


        }
        return sb.ToString();
    }

    private string LoadZones()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {

            var list = from x in entity.Xlk_Zone
                       orderby x.Description
                       select x;

            foreach (var item in list.ToList().Distinct())
            {
                sb.AppendFormat("<input class=\"watcher\"  id=\"zone{0}\" name=\"zone\" type=\"checkbox\" value=\"{0}\"/>{1}<br />", item.ZoneId, item.Description);
            }


        }
        return sb.ToString();
    }

    private string LoadGroups()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {

            var list = from x in entity.Group
                                where x.DepartureDate> DateTime.Now
                                orderby x.GroupName
                                select x;

            foreach (var item in list.ToList().Distinct())
            {
                sb.AppendFormat("<input class=\"watcher\" id=\"group{0}\" name=\"group\" type=\"checkbox\" value=\"{0}\"/>{1}<br />", item.GroupId, item.GroupName);
            }


        }
        return sb.ToString();
    }

    private string LoadNationalities()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {

            var nationalities = (from x in entity.Student
                                where x.DepartureDate < DateTime.Now
                                orderby x.Xlk_Nationality.Description
                                select x.Xlk_Nationality);

            foreach (var item in nationalities.ToList().Distinct())
            {
                sb.AppendFormat("<input class=\"watcher nationalitieslist\" id=\"nationality{0}\" name=\"nationality\" type=\"checkbox\" value=\"{0}\"/>{1}<br />", item.NationalityId, item.Description);
            }


        }
        return sb.ToString();
    }

    private static T GetValue<T>(Dictionary<string, object> newObject,string key)
    {

        if (newObject.Keys.Contains(key))
        {
            return (T)Convert.ChangeType(newObject[key], typeof(T));
        }

        return default(T);
    }

    private static void BuildHeader(SpecialObject newObject, ref StringBuilder sb)
    {
        DateTime startDate = newObject.ArrivalDate;

        sb.AppendFormat("<table class=\"accommodationresultstable\" summary=\"Filter families list\"><caption>From {0} to {1}</caption><thead><tr class=\"odd\"><td class=\"column1\">{2}</td>", newObject.ArrivalDate.ToString("MMM yyyy"), newObject.DepartureDate.ToString("MMM yyyy"), (newObject.ByWeek) ? "Week" : "Month");

        while (startDate <= newObject.DepartureDate)
        {
            sb.AppendFormat("<th abbr=\"{1} {0}\" scope=\"col\">{1} {0}</th>", (newObject.ByWeek) ? (startDate.DayOfYear / 7).ToString() : startDate.ToString("MMM"), (newObject.ByWeek) ? "W" : string.Empty);
            startDate = (newObject.ByWeek) ? startDate.AddDays(7) : startDate.AddMonths(1);
        }

        sb.Append("</tr></thead>");
        BuildBody(newObject, ref sb);
        sb.Append("</table>");
    }

    private static string BuildBody(SpecialObject newObject, ref StringBuilder sb)
    {
        sb.Append("<tbody>");

        using (Pace.DataAccess.Accommodation.AccomodationEntities aentity = new Pace.DataAccess.Accommodation.AccomodationEntities())
        {

            var families = from f in aentity.Families
                           select f;

            if (newObject.FamilyTypeIds.Count > 0)
                families = families.Where(f => newObject.FamilyTypeIds.Contains(f.Xlk_FamilyType.FamilyTypeId));

            if (newObject.FamilyMemberTypeIds.Count > 0)
                families = families.Where(f => f.FamilyMembers.Any(x => newObject.FamilyMemberTypeIds.Contains(x.Xlk_FamilyMemberType.FamilyMemberTypeId)));

            if (newObject.RoomTypeIds.Count > 0)
                families = families.Where(f => f.Rooms.Any(x => newObject.RoomTypeIds.Contains(x.Xlk_RoomType.RoomTypeId)));

            if (newObject.StatusIds.Count > 0)
                families = families.Where(f => newObject.StatusIds.Contains(f.Xlk_Status.StatusId));

            if (newObject.ZoneIds.Count > 0)
                families = families.Where(f => newObject.ZoneIds.Contains(f.Xlk_Zone.ZoneId));

            if (!string.IsNullOrEmpty(newObject.keywordSearch))
                families = families.Where(f => f.FirstName.Contains(newObject.keywordSearch) || f.SurName.Contains(newObject.keywordSearch));

            var hostings = from h in aentity.Hostings
                           where h.ArrivalDate <= newObject.DepartureDate && newObject.ArrivalDate <= h.DepartureDate
                           select h;

            if (newObject.NationalityIds.Count > 0)
                hostings = hostings.Where(x => newObject.NationalityIds.Contains(x.Student.Xlk_Nationality.NationalityId));

            if (newObject.GenderIds.Count > 0)
                hostings = hostings.Where(x => newObject.GenderIds.Contains(x.Student.Xlk_Gender.GenderId));

            var result = from fa in families
                          join h in hostings on fa.FamilyId equals h.Family.FamilyId
                          group h by fa into Groupings
                          select new { Family = Groupings.Key, Rooms = (Groupings.Key.Rooms.Count >0) ? Groupings.Key.Rooms.Sum(a => ((a.NumberofDoubleBeds.HasValue) ? a.NumberofDoubleBeds.Value : 0) + ((a.NumberofSingleBeds.HasValue) ? a.NumberofSingleBeds.Value: 0)) : 0, Hostings = Groupings };

            foreach (var family in result.ToList())
            {
                sb.AppendFormat("<tr><th scope=\"row\" class=\"column1\">{0}</th>", BasePage.FamilyLink(family.Family.FamilyId, "~/Accommodation/ViewAccommodationPage.aspx", family.Family.SurName));
                sb.Append("<td><div class=\"DL-topic-timeline DL-topic-timeline-widget DL-styled-module DL-section D7L-module DL-css\"><div class=\"DL-module-content-wrapper\"><ol class=\"DL-timelines\">");

                for (int i = 0; i <= (newObject.DepartureDate - newObject.ArrivalDate).Days; i++)
                {
                    if ((i != 0) && ((newObject.ByWeek && newObject.ArrivalDate.AddDays(i).DayOfWeek == DayOfWeek.Monday) || (!newObject.ByWeek && newObject.ArrivalDate.AddDays(i).Day == 1)))
                        sb.Append("<td><div class=\"DL-topic-timeline DL-topic-timeline-widget DL-styled-module DL-section DL-module DL-css\"><div class=\"DL-module-content-wrapper\"><ol class=\"DL-timelines\">");

                    List<long> ids = (from s in family.Hostings.Where(h => BasePage.IsBetween(h.ArrivalDate, h.DepartureDate, newObject.ArrivalDate.AddDays(i))) select s.HostingId).ToList();
                    bool hols =  family.Family.Availabilities.Any(h => h.Xlk_AvailabilityType.AvailabilityTypeId == 3 && BasePage.IsBetween(h.FromDate.Value, h.ToDate.Value, newObject.ArrivalDate.AddDays(i)));

                    decimal percent = (ids.Count == 0) ? 0 : ((decimal)ids.Count / ((family.Rooms == 0) ? 1 :family.Rooms)) * 100;

                    sb.AppendFormat("<li style=\"height: 10px;\" class=\"DL-timeline DL-calculated\"><a href=\"#\"  class=\"{2}\" title=\"{0} students on {3}\" rel=\"'{1}',ViewAccommodationPage.aspx/PopupHosting\">", ids.Count, string.Join(";", ids.ToArray()), (ids.Count > 0) ? "popupTrigger" : string.Empty, newObject.ArrivalDate.AddDays(i).ToString("dd MMM"));
                    sb.AppendFormat("<span class=\"{2}\" style=\"height: {0}%\"></span><span class=\"bottom\" style=\"height: {1}%;\"></span></a></li>", (percent > 100) ? 0 : (100 - percent), (percent > 100) ? 100 : percent, (hols) ? "hols" : "top");
                    if ((i != 0) && ((newObject.ByWeek && newObject.ArrivalDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday) || (!newObject.ByWeek &&  newObject.ArrivalDate.AddDays(i).Month < newObject.ArrivalDate.AddDays(i + 1).Month)))
                        sb.Append("</td>");
                }

                sb.Append("</tr>");
            }

            return sb.Append("</tbody>").ToString();

   
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ListCalendarEvents(DateTime start, DateTime end, int id)
    {
        return string.Empty;
    }

    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> GetFamiliesWithFilter(SpecialObject newObject)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            BuildHeader(newObject, ref sb);


            return new AjaxResponse<string>(sb.ToString());
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }
}

public class SpecialObject
{
    public DateTime ArrivalDate { get; set; }
    public DateTime DepartureDate { get; set; }
    public Dictionary<object, int>[] nationality { get; set; }
    public Dictionary<object, int>[] gender { get; set; }
    public Dictionary<object, int>[] group { get; set; }
    public Dictionary<object, int>[] zone { get; set; }
    public Dictionary<object, int>[] roomtype { get; set; }
    public Dictionary<object, int>[] familytype { get; set; }
    public Dictionary<object, int>[] familymembertype { get; set; }
    public Dictionary<object, int>[] status { get; set; }
    public string keywordSearch { get; set; }

    public List<int> StatusIds
    {
        get
        {
            if (status != null && status.Count() > 0)
                return status.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }

    public List<int> FamilyTypeIds
    {
        get
        {
            if (familytype != null && familytype.Count() > 0)
                return familytype.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }

    public List<int> FamilyMemberTypeIds
    {
        get
        {
            if (familymembertype != null && familymembertype.Count() > 0)
                return familymembertype.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }

    public List<int> RoomTypeIds
    {
        get
        {
            if (roomtype != null && roomtype.Count() > 0)
                return roomtype.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }

    public List<int> NationalityIds {
        get
        {
            if (nationality != null && nationality.Count() > 0)
                return nationality.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }

    public List<int> GenderIds
    {
        get
        {
            if (gender != null && gender.Count() > 0)
                return gender.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }

    public List<int> GroupIds
    {
        get
        {
            if (group != null && group.Count() > 0)
                return group.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }

    public List<int> ZoneIds
    {
        get
        {
            if (zone != null && zone.Count() > 0)
                return zone.SelectMany(s => s.Select(y => y.Value)).ToList();

            return new List<int>();
        }
    }
    public bool ByWeek { get { return false; } } // (((DepartureDate - ArrivalDate).Days) / 7) < 4; } }
    
}