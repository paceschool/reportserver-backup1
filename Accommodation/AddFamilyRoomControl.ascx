﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddFamilyRoomControl.ascx.cs"
    Inherits="Accommodation_AddFamilyRoom" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addroom");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Accommodation/ViewAccommodationPage.aspx","SaveRoom") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display:none;">
    All fields must be completed</p>
<form id="addroom" name="addroom" method="post" action="ViewAccommodationPage.aspx/SaveRoom">
<div class="inputArea">
    <fieldset>
          <input type="hidden" name="Family" id="FamilyId" value="<%= GetFamilyId %>" />
          <input type="hidden" id="RoomId" value="<%= GetRoomId %>" />
        <label>
            Type
        </label>
        <select type="text" name="Xlk_RoomType" id="RoomTypeId">
            <%= GetRoomTypeList()%>
        </select>
        <label>
            Description
        </label>
        <textarea name="Description" id="Description" validation="required"><%=GetRoomDescription %></textarea>
        <label>
            Double Beds
        </label>
        <input type="text" name="NumberofDoubleBeds" id="NumberofDoubleBeds" value="<%= GetNoOfDoubleBeds %>"/>
        <label>
            Single Beds
        </label>
        <input type="text" name="NumberofSingleBeds" id="NumberofSingleBeds" value="<%= GetNoOfSingleBeds %>"/>
        <label>
            Is Ensuite?
        </label>
        <input type="checkbox" value="true" id="Ensuite" <%= GetEnsuite %> />
    </fieldset>
</div>
</form>
