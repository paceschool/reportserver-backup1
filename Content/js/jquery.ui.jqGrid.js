﻿$(function () {

    buildGridFunction = function (element, pager, url, params, caption, colname, colmodel) {

        return $(element).jqGrid({
            url: url,
            mtype: "POST",
            ajaxGridOptions: { contentType: "application/json; charset=utf-8" },
            datatype: "json",
            serializeGridData: function (postData) {
                return JSON.stringify({ gridData:postData});
            },
            postData: params,// { ActionPage: 'ClearanceCost', Action: 'Fill' },
            jsonReader: {
                root: function (obj) { return obj.d.rows; },
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                records: function (obj) { return obj.d.length; },
                id: "0",
                cell: "",
                repeatitems: false
            },
            datatype: "json",
            pager: pager,
            height: '100%',
            colName: colname, // should be in the form of ['Description', 'ProductName', 'UnitsInStock'],
            colModel: colmodel, //[{ name: 'Description', index: 'Description', width: 100 },{ name: 'Comment', width: 100 },{ name: 'ArrivalDate', width: 100}],
            caption: caption,
            editurl:"/HelpPage.aspx/SaveRecord",
        });
    }
});