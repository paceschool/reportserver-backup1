﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contacts = Pace.DataAccess.Contacts;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;
using Pace.DataAccess.Enrollment;
using System.Web.Script.Services;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects.DataClasses;

public partial class AddContactPage : BaseContactPage
{
    private Int32? _contactId, _cat, _typ;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["ContactId"]))
        {
            _contactId = Convert.ToInt32(Request["ContactId"]);
            _cat = Convert.ToInt32(Request["Category"]);
            _typ = Convert.ToInt32(Request["Type"]);
        }
        
        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            category.DataSource = LoadCategories();
            category.DataBind();

            if (_contactId.HasValue)
            {
                DisplayContact(LoadContact(_contactId.Value));
                category.SelectedIndex = Convert.ToInt32(_cat);
                LoadTypesToDropDown();
                type.SelectedIndex = Convert.ToInt32(_typ);
            }
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void Save_Click(object sender, EventArgs e)
    {
        string _message = String.Empty;
        errorcontainer.Visible = false;
        bool isAcceptable = true; // Check for validity before saving

        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {

            if (!string.IsNullOrEmpty(firstname.Text))
            {

                Contacts.ContactDetails _contact = GetContact();

                var InitialStatus = 1;
                _contact.FirstName = firstname.Text;
                _contact.SurName = surname.Text;
                _contact.MobileNumber = mobilenumber.Text;
                _contact.PhoneNumber = landline.Text;
                _contact.ContactAddress = contactaddress.Text;

                //Check for valid email if any
                if (contactemail.Text.Contains("@"))
                    _contact.ContactEmail = contactemail.Text;
                else if (contactemail.Text.Equals(""))
                    _contact.ContactEmail = contactemail.Text;
                else
                {
                    _message = "The email you have entered, " + contactemail.Text + ", is invalid.  Please ensure you have the '@' sign";
                    isAcceptable = false;
                }

                //Check for valid website if any
                if (ContactWebsite.Text.StartsWith("http://"))
                    _contact.ContactWebsite = ContactWebsite.Text;
                else if (ContactWebsite.Text.StartsWith("www."))
                    _contact.ContactWebsite = "http://" + ContactWebsite.Text;
                else if (ContactWebsite.Text.Equals(""))
                    _contact.ContactWebsite = ContactWebsite.Text;
                else
                {
                    _message = "The website entered, '" + ContactWebsite.Text + "' is invalid. Please try again.";
                    isAcceptable = false;
                }
                // if the above checks are acceptable, try a save
                if (!isAcceptable == false)
                {
                    _contact.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(InitialStatus));

                    if (_contact.FirstName == null)
                        _message = "No Type selected.  Please ensure you select a Type";

                    _contact.Xlk_TypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Type", "TypeId", Convert.ToByte(type.SelectedItem.Value));

                    SaveContact(_contact);
                }

            }
            else
                _message = "First Name field is blank. You must have a value in the First Name field.";

            if (!string.IsNullOrEmpty(_message))
            {
                errorcontainer.Visible = true;
                errormessage.Text = _message;
            }
        }
    }

    protected void category_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTypesToDropDown();

    }

    protected void category_DataBound(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }

    protected void type_DataBound(object sender, EventArgs e)
    {
        TypeChanged();
    }

    protected void type_SelectedIndexChanged(object sender, EventArgs e)
    {
        TypeChanged();
    }

    /*protected void cDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        ContactList();
    }*/

    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        //ConfirmMessageBox(this, "Are you sure?");

        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            object todelete = null;

            if (!string.IsNullOrEmpty(((ImageButton)sender).ToolTip))
            {
                Int32 _contactId = Convert.ToInt32(((ImageButton)sender).ToolTip);
                if (entity.TryGetObjectByKey(new EntityKey(entity.DefaultContainerName + ".ContactDetails", "ContactId", _contactId), out todelete))
                {
                    entity.DeleteObject(todelete);
                    if (entity.SaveChanges() > 0)
                    {
                        MessageBoxShow(this, "Record no." + Convert.ToInt32(((ImageButton)sender).ToolTip) + " deleted.");
                        TypeChanged();
                    }
                    else MessageBoxShow(this, "Unable to delete Record No." + Convert.ToInt32(((ImageButton)sender).ToolTip) + ".  Please contact Admin.");
                }

            }
        }
    }

    #region Private Methods

    private void LoadTypesToDropDown()
    {
        //Set datasource of the Type DropDown
        ListItem _category = category.SelectedItem;
        type.DataSource = this.LoadTypes(Convert.ToInt32(_category.Value));
        type.DataBind();
    }

    private void TypeChanged()
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            if (type.SelectedItem != null)
            {
                Int32 _typeId = Convert.ToInt32(type.SelectedItem.Value);

                var contactSearchQuery = from d in entity.ContactDetails.Include("Xlk_Type").Include("Xlk_Status")
                                         where d.Xlk_Type.TypeId == _typeId
                                         orderby d.FirstName ascending
                                         select d;

                LoadResults(contactSearchQuery.ToList());
            }
        }
    }

    private void ContactList()
    {
        /*if (type.SelectedItem != null)
        {
            Int32 _contactId = Convert.ToInt32(type.SelectedItem.Value);

            var contactDropDown = from e in Entities.ContactDetails.Include("Xlk_Type")
                                  where e.Xlk_Type.TypeId == _contactId
                                  orderby e.FirstName
                                  select e;
            cDropDown.DataSource = contactDropDown;
        }*/
    }

    private void LoadResults(List<Contacts.ContactDetails> contacts)
    {
        //Set datasource of the repeater
        results.DataSource = contacts;
        results.DataBind();
    }

    protected string CreateTagList(object tags)
    {
        System.Text.StringBuilder tagsString = new System.Text.StringBuilder();

        var s = from st in (EntityCollection<Pace.DataAccess.Contacts.Tag>)tags
                select st.Tag1;

        return String.Join("/", s.ToArray());
    }

    protected string CreateId(object id)
    {
        return String.Format("tagId{0}", id);
    }

    private Contacts.ContactDetails LoadContact(Int32 contactId)
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            IQueryable<Contacts.ContactDetails> contactQuery = from c in entity.ContactDetails
                                                               where c.ContactId == contactId
                                                               select c;
            if (contactQuery.ToList().Count() > 0)
                return contactQuery.ToList().First();

            return null;
        }
    }

    private void DisplayContact(Contacts.ContactDetails contact)
    {
        if (contact != null)
        {
            firstname.Text = contact.FirstName;
            surname.Text = contact.SurName;
            landline.Text = contact.PhoneNumber;
            mobilenumber.Text = contact.MobileNumber;
            contactemail.Text = contact.ContactEmail;
            contactaddress.Text = contact.ContactAddress;
            ContactWebsite.Text = contact.ContactWebsite;

        }
    }

    private Contacts.ContactDetails GetContact()
    {
        if (_contactId.HasValue)
            return LoadContact(_contactId.Value);

        return new Contacts.ContactDetails();
    }

    private void SaveContact(Contacts.ContactDetails contact)
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            if (!_contactId.HasValue)
                entity.AddToContactDetails(contact);

            if (entity.SaveChanges() > 0)
                Response.Redirect(string.Format("ContactsPage.aspx"));
        }
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    public static string AddContactTag(int id, string tag)
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            try
            {
                if (!TagExists(id, tag))
                {
                    Contacts.Tag newtag = new Contacts.Tag();
                    newtag.ContactDetailsReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ContactDetails", "ContactId", id);
                    newtag.ContactId = id;
                    newtag.TagId = NextTagId(id, 1);
                    newtag.Tag1 = tag;

                    entity.AddToTag(newtag);

                    if (entity.SaveChanges() > 0)
                        return string.Empty;

                    return "Could not add the new tag; there was a problem";

                }
                else
                    return "Could not add the new tag; tag already exists";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    [WebMethod]
    public static string LoadTags(int id)
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            StringBuilder sb = new StringBuilder();

            IQueryable<Contacts.Tag> tagQuery = from d in entity.Tag
                                                where d.ContactId == id
                                                select d;

            foreach (Contacts.Tag tag in tagQuery.ToList())
            {
                sb.AppendFormat("<option>{0}</option>", tag.Tag1);
            }
            return sb.ToString();
        }
    }

    [WebMethod]
    public static string DeleteTag(int id, string tag)
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            IQueryable<Contacts.Tag> tagObject = from d in entity.Tag
                                                 where d.Tag1 == tag && d.ContactId == id
                                                 select d;

            if (tagObject.Count() > 0)
                entity.DeleteObject(tagObject.First());
            else
                return "Could not delete tag; tag not found";

            if (entity.SaveChanges() > 0)
                return string.Empty;

            return "Could not add the new tag; there was a problem";
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse ToggleStatus(int id)
    {
        try
        {
            using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
            {
                IQueryable<Contacts.ContactDetails> contactQuery = from d in entity.ContactDetails.Include("Xlk_Status")
                                                                   where d.ContactId == id
                                                                   select d;

                if (contactQuery.Count() > 0)
                {
                    Contacts.ContactDetails con = contactQuery.First();
                    con.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte((con.Xlk_Status.StatusId == 1) ? 2 : 1));
                }
                else
                    return new AjaxResponse(new Exception("Could not toggle the status, Contact not found"));

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();

                return new AjaxResponse(new Exception("Could not add the new tag; there was a problem"));
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    private static bool TagExists(int id, string tag)
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            var i = from d in entity.Tag
                    where d.Tag1 == tag && d.ContactId == id
                    select d.ContactId;

            return (i.Count() > 0);
        }
    }

    private static byte NextTagId(int id, byte step)
    {
        using (Contacts.ContactsEntities entity = new Contacts.ContactsEntities())
        {
            IQueryable<Contacts.Tag> tagQuery = from t in entity.Tag
                                                where t.ContactId == id
                                                select t;
            if (tagQuery.Count() > 0)
                return (byte)(tagQuery.Max(t => t.TagId) + step);

            return step;
        }
    }

    private void MessageBoxShow(Page page, string message)
    {
        Literal ltr = new Literal();
        ltr.Text = @"<script type='text/javascript'> alert('" + message + "') </script>";
        page.Controls.Add(ltr);
    }

    private void ConfirmMessageBox(Page page, string message1)
    {
        Literal lit = new Literal();
        lit.Text = @"<script type = 'text/javascript'> confirm('" + message1 + "') </script>";
        page.Controls.Add(lit);
        
    }

    #endregion
}
