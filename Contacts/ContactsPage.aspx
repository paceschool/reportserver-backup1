﻿<%@ Page Title="" Language="C#" MasterPageFile="../MasterPage.master" AutoEventWireup="true"
    CodeFile="ContactsPage.aspx.cs" Inherits="ContactsPage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter();

            $("#<%= repeat.ClientID %>").buttonset();


            refreshCurrentTab = function () {
                location.reload();
            }

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Browse By
                    </td>
                    <td>
                        <asp:DropDownList ID="Category" runat="server" DataTextField="Description" DataValueField="CategoryId"
                            OnSelectedIndexChanged="category_SelectedIndexChanged" AutoPostBack="true" OnDataBound="category_DataBound">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="type" runat="server" DataTextField="Description" DataValueField="TypeId"
                            AutoPostBack="True" OnSelectedIndexChanged="type_SelectedIndexChanged" OnDataBound="type_DataBound">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Or Filter By
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" OnTextChanged="search_Click" runat="server">
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="search" runat="server" OnClick="search_Click" class="button"><span>Search</span>
                        </asp:LinkButton>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="LinkButton2" PostBackUrl="~/Contacts/ContactsPage.aspx" runat="server"
                OnClick="search_Click"><span>Search Results</span></asp:LinkButton>
            <% foreach (Pace.DataAccess.Contacts.Xlk_Category category in LoadCategories())
               { %>
            <a href="?Action=<%= category.Description.ToLower() %>">
                <%= category.Description %></a>
            <% } %>
        </span><span style="float: right;"><a onclick="loadEditArrayControl('#dialog-form','ContactsPage.aspx','Contacts/AddContactControl.ascx',null)"
            href="#">
            <img src="../Content/img/actions/add.png">
            Add New Contact </a>
            <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
            </asp:Label></span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Name
                    </th>
                    <th scope="col">
                        Mobile Number
                    </th>
                    <th scope="col">
                        Land Line
                    </th>
                    <th scope="col">
                        Email
                    </th>
                    <th scope="col">
                        Address
                    </th>
                    <th scope="col">
                        Company
                    </th>
                    <th scope="col">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="results" runat="server" EnableViewState="True">
                    <ItemTemplate>
                        <tr class="<%#TRClass(DataBinder.Eval(Container.DataItem, "Xlk_Status.StatusId")) %>">
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "ContactId")%>
                            </td>
                            <td style="text-align: left">
                                <asp:HyperLink ID="lnkWebsite" runat="server" NavigateUrl='<%#DataBinder.Eval(Container.DataItem, "ContactWebsite") %>'
                                    Target="_blank" ToolTip='<%#"Click to visit website " + DataBinder.Eval(Container.DataItem, "ContactWebsite") %>'>
                            <%# CreateName(DataBinder.Eval(Container.DataItem, "FirstName"), DataBinder.Eval(Container.DataItem, "SurName"))%>
                                </asp:HyperLink>
                            </td>
                            <td style="font-weight: bold; text-align: left">
                                <asp:HyperLink ID="lnkMobile" runat="server" NavigateUrl=<%# String.Format("http://{1}/adr.htm?adrnumber={0}&outgoing=line-1", DataBinder.Eval(Container.DataItem, "MobileNumber"), Phone)%> 
                                    Target="_blank" ToolTip="Click to call">
                                    <%#DataBinder.Eval(Container.DataItem, "MobileNumber")%>
                                </asp:HyperLink>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "PhoneNumber") %>
                            </td>
                            <td style="text-align: center">
                                <asp:HyperLink ID="lnkEmail" runat="server" NavigateUrl='<%#DataBinder.Eval(Container.DataItem, "ContactEmail", "MailTo:{0}") %>'
                                    ImageUrl="~/Content/img/actions/email_go.png" Target="_blank" ToolTip="Click to open email message window">
                                </asp:HyperLink>
                            </td>
                            <td style="text-align: left">
                                <asp:HyperLink runat="server" Text='<%# TrimText(DataBinder.Eval(Container.DataItem, "ContactAddress"),35)%>'
                                    NavigateUrl='<%#String.Format("http://maps.google.com/maps?q="+ DataBinder.Eval(Container.DataItem, "ContactAddress")+"&output=embed") %>'
                                    ToolTip='<%#DataBinder.Eval(Container.DataItem, "ContactAddress") %>' Target="_blank" />
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "CompanyName") %>
                            </td>
                            <td style="text-align: center">
                                <a onclick="loadEditArrayControl('#dialog-form','ContactsPage.aspx','Contacts/AddContactControl.ascx',{'ContactId':<%#DataBinder.Eval(Container.DataItem, "ContactId")%>})"
                                    href="#" title="Edit Contact">
                                    <img src="../Content/img/actions/edit.png"></a>
                                    <a onclick="deleteObjectFromAjaxTab('ContactsPage.aspx','Contact',<%#DataBinder.Eval(Container.DataItem, "ContactId")%>)"
                                        href="#" title="Delete Contact"><img src="../Content/img/actions/bin_closed.png"></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
