﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="ContactsHolidays.aspx.cs" Inherits="Contacts_ContactsHolidays" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="pagename">
        <span style="font-style:italic; font-weight:bolder; font-size:larger">
            Holidays Page
        </span>
    </div>
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Browse By
                    </td>
                    <td>
                        <asp:DropDownList ID="Category" runat="server" DataTextField="Description" DataValueField="CategoryId"
                            OnSelectedIndexChanged="category_SelectedIndexChanged" AutoPostBack="true" OnDataBound="category_DataBound">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="type" runat="server" DataTextField="Description" DataValueField="TypeId"
                            AutoPostBack="True" OnSelectedIndexChanged="type_SelectedIndexChanged" OnDataBound="type_DataBound">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Or Filter By
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" runat="server">
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="search" runat="server" OnClick="search_Click" class="button"><span>Search</span>
                        </asp:LinkButton>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span style="float: right;">
                       <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
            </asp:Label>
        </span>

        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Name
                    </th>
                   <th scope="col">
                       Holiday Records
                    </th>
                    <th scope="col">
                        View Records
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="results" runat="server" EnableViewState="True">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align:left">
                                <%#DataBinder.Eval(Container.DataItem, "ContactId")%>
                            </td>
                            <td style="text-align:left">
                            <asp:HyperLink ID="lnkWebsite" runat="server" NavigateUrl='<%#DataBinder.Eval(Container.DataItem, "ContactWebsite") %>'
                            Target="_blank" ToolTip='<%#"Click to visit website " + DataBinder.Eval(Container.DataItem, "ContactWebsite") %>'>
                            <%# CreateName(DataBinder.Eval(Container.DataItem, "FirstName"), DataBinder.Eval(Container.DataItem, "SurName"))%>
                            </asp:HyperLink>
                            </td>

                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
