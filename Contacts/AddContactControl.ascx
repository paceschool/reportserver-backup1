﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddContactControl.ascx.cs"
    Inherits="Contacts_AddContactControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addcontact");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Contacts/ContactsPage.aspx","SaveContact") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addcontact" name="addcontact" method="post" action="ContactsPage.aspx/SaveContact">
<div class="inputArea">
    <fieldset>
         <input type="hidden" id="ContactId" value="<%= GetContactId %>" />
        <label>
            Status
        </label>
        <select name="Xlk_Status" id="StatusId">
            <%= GetStatusList()%>
        </select>
        <label>
            Type
        </label>
        <select name="Xlk_Type" id="TypeId">
            <%= GetTypeList()%>
        </select>
        <label>
            First Name
        </label>
        <input type="text" id="FirstName" value="<%= GetFirstName %>" />
        <label>
            Surname
        </label>
        <input type="text" id="SurName" value="<%= GetSurName %>" />
        <label>
            Landline Number
        </label>
        <input type="text" id="PhoneNumber" value="<%= GetPhoneNumber %>" />
    </fieldset>
    <fieldset>
        <label>
            Mobile Number
        </label>
        <input type="text" id="MobileNumber" value="<%= GetMobileNumber %>" />
        <label>
            email
        </label>
        <input type="text" id="ContactEmail" value="<%= GetContactEmail %>" />
        <label>
            address
        </label>
        <input type="text" id="ContactAddress" value="<%= GetContactAddress %>" />
        <label>
            Company Name
        </label>
        <input type="text" id="CompanyName" value="<%= GetCompanyName %>" />
        <label>
            Website (http://........)
        </label>
        <input type="text" id="ContactWebsite" value="<%= GetContactWebsite %>" />
    </fieldset>
</div>
</form>
