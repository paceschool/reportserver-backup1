﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Contacts;
using System.Web.Services;
using System.Text;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects.DataClasses;

public partial class Contacts_ViewContactPage : BaseContactPage
{
    protected Int32 _contactId;
    protected Int32 UID = GetCurrentUser().UserId;
    protected string Phone = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["ContactId"]))
            _contactId = Convert.ToInt32(Request["ContactId"]);

        if (!Page.IsPostBack)
        {
            DisplayContact(LoadContact(new ContactsEntities(), _contactId));
        }
    }

    public override string TextValue() // Page Name
    {
        newPageName.Text = "Contact Details";
        return newPageName.Text;
    }

    private ContactDetails LoadContact(ContactsEntities entity, Int32 contactId)
    {
        switch (UID)
        {
            case 1: Phone = "10.0.1.47";
                break;
            case 2: Phone = "10.0.1.44";
                break;
            case 3: Phone = "10.0.1.46";
                break;
            case 4: Phone = "10.0.1.35";
                break;
            case 5: Phone = "10.0.1.42";
                break;
            default: Phone = "";
                break;
        }
        
        IQueryable<ContactDetails> contactQuery = from c in entity.ContactDetails.Include("Xlk_Status").Include("Xlk_Type").Include("Xlk_Type.Xlk_Category")
                                                  where c.ContactId == contactId
                                                  select c;

        if (contactQuery.ToList().Count() > 0)
            return contactQuery.ToList().First();

        return null;

        
    }

    private void DisplayContact(ContactDetails contact)
    {
        if (contact != null)
        {
            ContactId.Text = contact.ContactId.ToString();
            Name.Text = String.Format("{0} {1}", contact.FirstName, contact.SurName);
            Status.Text = contact.Xlk_Status.Description;
            Mobile.Text = contact.MobileNumber.ToString();
            Landline.Text = contact.PhoneNumber.ToString();
            email.Text = contact.ContactEmail;
            Address.Text = contact.ContactAddress;
            Company.Text = contact.CompanyName;
            type.Text = contact.Xlk_Type.Description;
            Category.Text = contact.Xlk_Type.Xlk_Category.Description;
            website.Text = contact.ContactWebsite;
        }
    }
}