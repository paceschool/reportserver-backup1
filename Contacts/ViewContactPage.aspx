﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="ViewContactPage.aspx.cs" Inherits="Contacts_ViewContactPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/Js/Silverlight.js") %>"></script>
    <script type="text/javascript">
        $(function () {

            var Id = <%= _contactId %>;

            $("table").tablesorter();

            refreshCurrentPage = function () {
                location.reload();
            }


            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }

            refreshPage = function () {
                location.reload();
            }

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Contact Details Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="imgdiv" style="position: absolute; top: 100px; right: 200px; z-index: 2;
        border: solid 1px black; visibility: hidden;">
    </div>
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col" style="width:7%">
                        <label>
                            Contact ID
                        </label>
                        <br />
                    </th>
                    <td>
                        <asp:Label ID="ContactId" runat="server"></asp:Label>
                    </td>
                    <th scope="col">
                        Name
                    </th>
                    <td>
                        <asp:Label ID="Name" runat="server" style="font-size:x-large; font-weight:bold"></asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Status
                        </label>
                    </th>
                    <td>
                        <asp:Label ID="Status" runat="server" style="font-weight:bold; font-size:x-large;"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Mobile Number
                        </label>
                    </th>
                    <td>
                        <a href="<%= String.Format("http://{1}/adr.htm?adrnumber={0}&outgoing=line-1", Mobile.Text, Phone)%>">
                        <asp:Label ID="Mobile" runat="server" Style="font-size:x-large;"></asp:Label></a>
                    </td>
                    <th scope="col">
                        <label>
                            Land Line Number
                        </label>
                    </th>
                    <td>
                        <a href="<%= String.Format("http://{1}/adr.htm?adrnumber={0}&outgoing=line-1", Landline.Text, Phone)%>">
                        <asp:Label ID="Landline" runat="server" Style="font-size:x-large;"></asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            email
                        </label>
                    </th>
                    <td>
                        <a href="mailto: <%= email.Text %>">
                        <asp:Label ID="email" runat="server" Style="font-size:x-large;"></asp:Label></a>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Address
                        </label>
                    </th>
                    <td>
                        <a href="<%= String.Format("http://maps.google.com/maps?q=" + Address.Text + "&output=embed") %>" target="_blank">
                        <asp:Label ID="Address" runat="server"></asp:Label></a>
                    </td>
                    <th scope="col">
                        <label>
                            Company
                        </label>
                    </th>
                    <td>
                        <asp:Label ID="Company" runat="server"></asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Website
                        </label>
                    </th>
                    <td>
                        <a href="<%= website.Text %>" title="visit website" target="_blank">
                        <asp:Label ID="website" runat="server"></asp:Label></a>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Contact Type
                        </label>
                    </th>
                    <td>
                        <asp:Label ID="type" runat="server"></asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Contact Category
                        </label>
                    </th>
                    <td>
                        <asp:Label ID="Category" runat="server"></asp:Label>
                    </td>
                    <th scope="col">
                        <label>

                        </label>
                    </th>
                    <td>

                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="ViewContactPage.aspx/LoadContactDocs?id=<%= _contactId %>">Documents</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
