﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

using Pace.DataAccess.Documents;


public partial class Documents_AddDocumentControl : BaseDocumentControl
{
    protected Int32 _documentId;
    protected Documents _documentDetails;

    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            if (GetValue<Int32?>("DocId").HasValue)
            {
                _documentId = GetValue<Int32>("DocId");

                _documentDetails = (from n in entity.Documents.Include("Xlk_Type").Include("Xlk_Status")
                                   where n.DocId == _documentId
                         select n).FirstOrDefault();
            }
        }
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_documentDetails != null && _documentDetails.Xlk_Status != null) ? _documentDetails.Xlk_Status.StatusId : (byte?)null;


        foreach (Xlk_Status item in this.LoadStatus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_documentDetails != null && _documentDetails.Xlk_Type != null) ? _documentDetails.Xlk_Type.TypeId : (byte?)null;


        foreach (Xlk_Type item in this.LoadTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TypeId, item.Description, (_current.HasValue && item.TypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetFileTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_documentDetails != null && _documentDetails.Xlk_FileType != null) ? _documentDetails.Xlk_FileType.FileTypeId : (byte?)null;


        foreach (Xlk_FileType item in this.LoadFileTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.FileTypeId, item.Description, (_current.HasValue && item.FileTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetServerList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_documentDetails != null && _documentDetails.Server != null) ? _documentDetails.Server.ServerId : (byte?)null;


        foreach (Server item in this.LoadServers())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ServerId, item.Name, (_current.HasValue && item.ServerId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }


    protected string GetDocId
    {
        get
        {
            if (_documentDetails != null)
                return _documentDetails.DocId.ToString();

            if (GetValue<Int32?>("DocId").HasValue)
                return GetValue<Int32>("DocId").ToString();

            return "0";
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_documentDetails != null)
                return _documentDetails.Description;


            return null;
        }
    }

    protected string GetFileName
    {
        get
        {
            if (_documentDetails != null)
                return _documentDetails.FileName;


            return null;
        }
    }

    protected string GetPath
    {
        get
        {
            if (_documentDetails != null)
                return _documentDetails.Path;


            return null;
        }
    }

    protected string GetTitle
    {
        get
        {
            if (_documentDetails != null)
                return _documentDetails.Title;


            return null;
        }
    }
  
}