﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddDocumentControl.ascx.cs"
    Inherits="Documents_AddDocumentControl" %>
<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#adddocument");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Documents/DocumentsPage.aspx","SaveDocument") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $('.watcher').change(function () {
            process(this);
            var path = $(this).val();
            $('#FileName').val(path);
        });
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="adddocument" name="adddocument" method="post" action="DocumentsPage.aspx/SaveDocument">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="documentId" value="<%= GetDocId %>" />
        <input type="hidden" id="FileName" value="<%= GetFileName %>" />
        <label>
            Title
        </label>
        <input type="text" id="Title" value="<%= GetTitle %>" />
        <label>
            Description
        </label>
        <input type="text" id="Description" value="<%= GetDescription %>" />
        <label>
            Path
        </label>
        <input type="text" id="Path" value="<%= GetPath %>" readonly="readonly" />
        <label>
            FileName
        </label>
        <input type="file" class="watcher" value="<%= GetFileName %>" />
    </fieldset>
    <fieldset>
        <label>
            Status
        </label>
        <select name="Xlk_Status" id="StatusId">
            <%= GetStatusList()%>
        </select>
        <label>
            Type
        </label>
        <select name="Xlk_Type" id="TypeId">
            <%= GetTypeList()%>
        </select>
        <label>
            File Type
        </label>
        <select name="Xlk_FileType" id="FileTypeId">
            <%= GetFileTypeList()%>
        </select>
    </fieldset>
</div>
</form>
