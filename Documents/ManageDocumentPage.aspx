﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManageDocumentPage.aspx.cs" Inherits="AddDocumentPage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
        $(function() {

            $("table").tablesorter()

            // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
            $("#dialog:ui-dialog").dialog("destroy");

            var addtag = $("#addtag"), Id,
                deletetag = $("#deletetag")
            allFields = $([]).add(addtag).add(deletetag);

            function checkLength(o, n, min, max) {
                if (o.val().length > max || o.val().length < min) {
                    o.addClass("ui-state-error");
                    updateTips("Length of " + n + " must be between " +
					min + " and " + max + ".");
                    return false;
                } else {
                    return true;
                }
            }


 


            toggleStatus = function(id) {
            Id = id;
            $.ajax({
                type: "POST",
                url: "ManageDocumentPage.aspx/ToggleStatus",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'id':" + id + "}",
                success: function(msg) {
                    $("#deletetag").html(msg.d);
                    location.reload();
                }
            });
            }


        });

    </script>
	<style type="text/css">
		label, input { display:block; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Add / Manage Documents Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer" style="float: left;">
        <div id="errorcontainer" name="errorcontainer" class="error" visible="false" runat="server">
            <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
        </div>
        <div class="inputArea">
            <fieldset>
                <label>
                    Category
                </label>
                <asp:DropDownList ID="category" runat="server" DataTextField="Description" DataValueField="CategoryId"
                    AutoPostBack="True" OnSelectedIndexChanged="category_SelectedIndexChanged" OnDataBound="category_DataBound">
                </asp:DropDownList>
                <label>
                    Type
                </label>
                    <asp:DropDownList ID="type" runat="server" DataTextField="Description" DataValueField="TypeId"
                        AutoPostBack="True" OnSelectedIndexChanged="type_SelectedIndexChanged" OnDataBound="type_DataBound">
                    </asp:DropDownList>
                <label>
                    Title
                </label>
                <asp:TextBox ID="title" runat="server">
                </asp:TextBox>
           </fieldset>
           <fieldset>
                <label>
                    Description
                </label>
                <asp:TextBox ID="description" runat="server">
                </asp:TextBox>
                <label>
                    File
                </label>
               <asp:FileUpload ID="FileUpload1" runat="server" />
                <input id="filename" name="filename" runat="server" type="file" />
                <asp:Button ID="Save" runat="server" Text="Save" OnClick="Save_Click" />
            </fieldset>
        </div>
    </div>
    <div id="viewconatiner">
        <div class="resultscontainer">
            <table id="box-table-a" class="tablesorter">
                <thead>
                    <tr>
                        <th scope="col">
                            Id
                        </th>
                        <th scope="col">
                            Document
                        </th>
                        <th scope="col">
                            File Name
                        </th>
                        <th scope="col">
                            Location
                        </th>
                       <th scope="col">
                            Status
                        </th>
                        <th scope="col">
                            Tags
                        </th>
                        <th scope="col">
                            Click to delete
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="results" runat="server" EnableViewState="True">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "DocId")%>
                                </td>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "Title")%>
                                </td>
                                <td style="font-weight: bold">
                                    <%#DataBinder.Eval(Container.DataItem, "FileName")%>
                                </td>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "Path")%>
                                </td>
                                <td>
                                     <a href="#" onclick="toggleStatus('<%# DataBinder.Eval(Container.DataItem, "DocId")%>');">
                                         <%#DataBinder.Eval(Container.DataItem, "Xlk_Status.Description")%></a>
                                 </td>
                                <td>
                                        <%# CreateDocumentTagList(DataBinder.Eval(Container.DataItem, "DocumentTags"))%></a>
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageButton1" ToolTip='<%#DataBinder.Eval(Container.DataItem, "DocId")%>'
                                        runat="server" ImageUrl="../Content/img/actions/bin_closed.png" OnClick="ImageButton_Click" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
