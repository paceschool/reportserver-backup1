﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Pace.DataAccess.Documents;

public partial class ViewerPage : BaseDocumentPage
{

    string filePath = String.Empty;
    string contentType = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Request["documentid"]))
            {
                if (GetDocument(Convert.ToInt32(Request["documentid"])))
                {
                    Response.ContentType = contentType;
                    if (File.Exists(@filePath))
                        Response.WriteFile((filePath));
                    else
                        ErrorLoading();
                }
                else
                    ErrorLoading();
            }
            else
                ErrorLoading();
        }
        catch
        {
             ErrorLoading();
        }
    }

    private bool GetDocument(Int32 docId)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Documents> documentSearchQuery = from d in entity.Documents.Include("Xlk_FileType").Include("Xlk_Type").Include("Server")
                                                        where d.DocId == docId
                                                        select d;

            if (documentSearchQuery.Count() == 0)
                return false;

            Documents doc = documentSearchQuery.First();

            if (doc == null)
                return false;

            if (doc.Xlk_FileType == null)
                return false;

            if (doc.Server == null)
                return false;

            filePath = string.Format("{0}\\{1}\\{2}", doc.Server.UncPath, doc.Path, doc.FileName);

            contentType = doc.Xlk_FileType.MimeType;

            return true;
        }
    }

    private void ErrorLoading()
    {
        string html = @"<html xmlns='http://www.w3.org/1999/xhtml'><head><title>Error Loading Document</title><link rel='stylesheet' type='text/css' href='css/style.css' /></head><body><div id='error'><p>There was an error loading your document, please ask the administrators for help with this document.</p></div></body></html>";

        Response.Write(html);
    }


}
