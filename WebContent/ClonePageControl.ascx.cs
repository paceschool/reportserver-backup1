﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_ClonePage : BaseWebContentControl
{
    protected Int32 _PageId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("PageId").HasValue)
        {
            _PageId = GetValue<Int32>("PageId");
        }
    }

    protected string GetPageId
    {
        get
        {
            if (_PageId != null)
                return _PageId.ToString();

            if (Parameters.ContainsKey("PageId"))
                return Parameters["PageId"].ToString();

            return "0";
        }
    }


}
