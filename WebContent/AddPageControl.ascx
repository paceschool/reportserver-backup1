﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddPageControl.ascx.cs"
    Inherits="WebContent_AddPage" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addpage");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","SavePage") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }


    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addpage" name="addenrollment" method="post" action="ContentManagerPage.aspx/SavePage">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="PageId" value="<%= GetPageId %>" />
        <label>
            Page Title</label>
        <input id="PageTitle" value="<%= GetPageTitle %>" />
        <label>
            Description
        </label>
        <input id="Description" value="<%= GetDescription %>" />
        <label>
            Page Url
        </label>
        <input id="PageURL" value="<%= GetPageUrl %>" />
        <label>
            Language</label>
        <select name="Xlk_LanguageCode" id="LanguageId">
            <%= GetLanguageCodeList()%>
        </select>
    </fieldset>
</div>
</form>
