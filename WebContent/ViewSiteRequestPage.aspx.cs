﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;
using Accommodation = Pace.DataAccess.Accommodation;
using Pace.DataAccess.WebSite;

public partial class ViewSiteRequestPage : BaseWebSitePage 
{
    protected Int32 _siteRequestId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["SiteRequestId"]))
            _siteRequestId = Convert.ToInt32(Request["SiteRequestId"]);


        if (!Page.IsPostBack)
        {

            DisplaySiteRequest(LoadSiteRequest(new WebSiteEntities(), _siteRequestId));
        }
    }

    protected SiteRequest LoadSiteRequest(WebSiteEntities entity, Int32 siteRequestId)// Load SiteRequest if passed from GroupsPage
    {
        IQueryable<SiteRequest> groupQuery = from g in entity.SiteRequests.Include("ServiceType").Include("RequestType")
                                             where g.SiteRequestId == siteRequestId
                                       select g;
        if (groupQuery.ToList().Count() > 0)
            return groupQuery.ToList().First();

        return null;
    }

    private void DisplaySiteRequest(SiteRequest siteRequest)// Display loaded SiteRequest details
    {
        if (siteRequest != null)
        {
            SiteRequestId.Text = siteRequest.SiteRequestId.ToString();
            RequestType.Text = siteRequest.RequestType.Description;
            AssignedTo.Text = "";
            ServiceType.Text = siteRequest.ServiceType.Description;
            IpAddress.Text = siteRequest.IpAddress;
            DateCreated.Text = string.Format("{0:D}", siteRequest.DateCreated);
        }
    }

    private SiteRequest GetSiteRequest(WebSiteEntities entity)
    {
        return LoadSiteRequest(entity, _siteRequestId);
    }


    #region Javascript Events

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFormStrings(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Form String</th><th scope=\"col\">Value</th></tr></thead><tbody>");
            using (WebSiteEntities entity = new WebSiteEntities())
            {

                var formStrings = from e in entity.SiteRequestStrings.Include("StringType")
                                  where e.SiteRequestId == id
                                  select e;

                if (formStrings.Count() > 0)
                {
                    foreach (SiteRequestString requeststring in formStrings)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td></tr>", requeststring.StringType.Description,requeststring.Value);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"2\">No form items to Show!</td></tr>");
                }

            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


        [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadActionsTaken(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Date Sent</th><th scope=\"col\">Sent By</th><th scope=\"col\">To</th><th scope=\"col\">Subject</th><th scope=\"col\">Body</th></tr></thead><tbody>");
            using (WebSiteEntities entity = new WebSiteEntities())
            {

                var emails = from e in entity.SiteRequestEmails
                                  where e.SiteRequestId == id
                                  select e;

                if (emails.Count() > 0)
                {
                    foreach (SiteRequestEmail email in emails)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td></tr>", email.DateCreated, email.FromAddress, email.ToAddress, email.Subject, email.Body);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"5\">No email items to Show!</td></tr>");
                }

            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    


    #endregion

}
