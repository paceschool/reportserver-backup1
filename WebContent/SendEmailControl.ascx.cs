﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_SendEmail : BaseWebContentControl
{
    protected Int32 _siteRequestId;
    protected Pace.DataAccess.WebSite.SiteRequest _request;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (GetValue<Int32?>("SiteRequestId").HasValue)
        {
            _siteRequestId = GetValue<Int32>("SiteRequestId");
            using (Pace.DataAccess.WebSite.WebSiteEntities entity = new Pace.DataAccess.WebSite.WebSiteEntities())
            {
                _request = (from enroll in entity.SiteRequests.Include("SiteRequestStrings").Include("RequestType")
                            where enroll.SiteRequestId == _siteRequestId
                               select enroll).FirstOrDefault();

            }

        }
    }


    protected string GetSiteRequestId
    {
        get
        {
            if (_request != null)
                return _request.SiteRequestId.ToString();

            if (Parameters.ContainsKey("SiteRequestId"))
                return Parameters["SiteRequestId"].ToString();

            return "0";
        }
    }

    protected string GetToEmailAddress
    {
        get
        {
            if (_request != null && _request.SiteRequestStrings.Any(s => s.StringTypeId == 1))
                return _request.SiteRequestStrings.Single(s=>s.StringTypeId == 1).Value.ToString();

            return null;
        }
    }


    protected string GetSubject
    {
        get
        {
            if (_request != null && _request.RequestType != null)
                return _request.RequestType.Description;

            return "Web enquiry";
        }
    }

    protected string GetBody
    {
        get
        {

            string firstname = string.Empty, surname = String.Empty;

            if (_request != null && _request.SiteRequestStrings.Any(s => s.StringTypeId == 2))
                firstname = _request.SiteRequestStrings.Single(s=>s.StringTypeId == 2).Value.ToString();

            if (_request != null && _request.SiteRequestStrings.Any(s => s.StringTypeId == 3))
                surname = _request.SiteRequestStrings.Single(s => s.StringTypeId == 3).Value.ToString();

            if (!string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(surname))
                return string.Format("Dear {0} {1}, (Request Number #{2})\nThank you for your web enquiry\n\n", firstname.ToTitleCase(), surname.ToTitleCase(), _request.SiteRequestId.ToString());

            return string.Empty;
        }
    }


    protected string GetFromEmailAddress
    {
        get
        {
            var _user = GetCurrentUser();

            if (_user != null && !string.IsNullOrEmpty(_user.Email))
                return _user.Email.ToString();

            return null;
        }
    }


}
