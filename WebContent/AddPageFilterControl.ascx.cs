﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_AddPageFilter : BaseWebContentControl
{
    protected Int32 _pageId;
    protected Int32 _tagId;
    protected Pace.DataAccess.Content.PageFilter _pageFilter;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("PageId").HasValue)
        {
            _pageId = GetValue<Int32>("PageId");
            if (GetValue<Int32?>("TagId").HasValue)
            {
                _tagId = GetValue<Int32>("TagId");

                using (Pace.DataAccess.Content.ContentEntities entity = new Pace.DataAccess.Content.ContentEntities())
                {
                    _pageFilter = (from enroll in entity.PageFilters
                                   where enroll.PageId == _pageId && enroll.TagId == _tagId
                                  select enroll).FirstOrDefault();

                }
            }
        }
    }

    protected string GetPageId
    {
        get
        {
            if (_pageFilter != null)
                return _pageFilter.PageId.ToString();

            if (Parameters.ContainsKey("PageId"))
                return Parameters["PageId"].ToString();

            return "0";
        }
    }

    protected string GetTagId
    {
        get
        {
            if (_pageFilter != null)
                return _pageFilter.TagId.ToString();

            if (Parameters.ContainsKey("TagId"))
                return Parameters["TagId"].ToString();

            using (Pace.DataAccess.Content.ContentEntities entity = new Pace.DataAccess.Content.ContentEntities())
            {
                var tagids =  (from enroll in entity.PageFilters
                               where enroll.PageId == _pageId
                                 select enroll.TagId);

                return (tagids != null && tagids.Count() > 0) ? (tagids.Max() + 1).ToString() : "1";

            }

        }
    }

    protected string GetTag
    {
        get
        {
            if (_pageFilter != null)
                return _pageFilter.Tag;

            return null;
        }
    }

}
