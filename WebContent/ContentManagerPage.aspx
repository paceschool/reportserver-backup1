﻿<%@  Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContentManagerPage.aspx.cs" Inherits="WebContent_ContentManagerPage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/jqwidgets/css/jqx.base.css")%>"
        type="text/css" />
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/jqwidgets/css/jqx.ui-redmond.css")%>"
        type="text/css" />
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxcore.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxbuttons.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxscrollbar.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxpanel.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxTree.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxsplitter.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxlistbox.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxexpander.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxmenu.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.selection.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.columnsresize.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.pager.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdropdownlist.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdata.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.edit.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdropdownlist.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxcheckbox.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxcalendar.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxnumberinput.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdatetimeinput.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxpanel.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxmenu.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdragdrop.js")%>"></script>

    <script type="text/javascript">
        $(function () {


            $('#<%= lookupString.ClientID %>').bind('keypress', function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) { //Enter keycode
                    stateChanged();
                }
            });

            $('#<%= lookupFilter.ClientID %>').bind('keypress', function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) { //Enter keycode
                    stateChanged();
                }
            });

            refreshCurrentTab = function () {

            }

            function isRightClick(event) {
                var rightclick;
                if (!event) var event = window.event;
                if (event.which) rightclick = (event.which == 3);
                else if (event.button) rightclick = (event.button == 2);
                return rightclick;
            }

            var rss = (function ($) {
                var createWidgets = function () {
                    $('#jqxMenu').jqxMenu({ width: '120px', theme: config.theme, height: '105px', autoOpenPopup: false, mode: 'popup' });
                    $('#mainSplitter').jqxSplitter({ width: '100%', height: 600, height: 600, panels: [{ size: 300, min: 100 }, { min: 200, size: 400}] });
                    $('#contentSplitter').jqxSplitter({ width: '100%', height: '100%', orientation: 'horizontal', panels: [{ size: 200, min: 100, collapsible: false }, { min: 100, collapsible: true}] });
                    $("#feedExpander").jqxExpander({ toggleMode: 'none', showArrow: false, width: "100%", height: "100%",
                        initContent: function () {
                            $('#productTree').jqxTree({
                                allowDrag: true, allowDrop: true, theme: config.theme, height: '100%', width: '100%',
                                dragStart: function (item) {
                                    if (!(item.id.indexOf("ElementId") >= 0))
                                        return false;
                                },
                                dragEnd: function (item, dropItem, args, dropPosition, tree) {

                                    if ((item.id.indexOf("PageId") >= 0))
                                        return false;

                                    if ((dropItem.id.indexOf("PageId") >= 0))
                                        return false;

                                    $.ajax({
                                        'dataType': 'json',
                                        'type': "POST",
                                        'url': config.dataDir + '/DropItems',
                                        'data': JSON.stringify({ draggeditemid: item.id, draggedparentid: item.parentId, droppeditemid: dropItem.id, droppedparentid: dropItem.parentId, dropPosition: dropPosition }),
                                        'contentType': 'application/json; charset=utf-8',
                                        success: function (msg) {
                                            if (msg != null && msg.d != null) {
                                                if (msg.d.Success) {
                                                    stateChanged();
                                                }
                                                else
                                                    alert(msg.d.ErrorMessage);
                                            }
                                            else
                                                alert("No Response, please contact admin to check!");
                                        }
                                      
                                    });
                                    return false;
                                }
                            });
                        }
                    });
                    $("#feedListExpander").jqxExpander({ toggleMode: 'none', showArrow: false, width: "100%", height: "100%", initContent: function () {
                        $("#rateCardContainer").jqxGrid({ theme: config.theme, selectionmode: 'singlerow', editable: false, width: '100%', height: '100%', columns: config.filtercolumns, keyboardnavigation: false, showstatusbar: true, renderstatusbar: function (statusbar) {

                            // appends buttons to the status bar.
                            var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");

                            var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Add</span></div>");
                            var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/delete.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Delete</span></div>");
                            var reloadButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Reload</span></div>");

                            addButton.jqxButton({ theme: config.theme, width: 60, height: 20 });
                            deleteButton.jqxButton({ theme: config.theme, width: 65, height: 20 });
                            reloadButton.jqxButton({ theme: config.theme, width: 65, height: 20 });

                            container.append(addButton);
                            container.append(deleteButton);
                            container.append(reloadButton);

                            statusbar.append(container);

                            // add new row.
                            addButton.click(function (event) {
                                if (config.selectedtype != null)
                                    showEditPopUp(config.selectedtype, config.selectedid, null);
                            });

                            // delete selected row.
                            deleteButton.click(function (event) {
                                var selectedrowindex = $("#rateCardContainer").jqxGrid('getselectedrowindex');
                                var data = $("#rateCardContainer").jqxGrid('getrowdata', selectedrowindex)
                                if (data != null) {
                                    if (data.ElementId != null)
                                        deleteParentChildObjectFromAjaxTabWithCallback('ContentManagerPage.aspx', 'Attribute', data.ElementId, data.AttributeTypeId, gridChanged)
                                    if (data.PageId != null)
                                        deleteParentChildObjectFromAjaxTabWithCallback('ContentManagerPage.aspx', 'PageFilter', data.PageId, data.TagId,gridChanged)
                                }

                            });

                            // reload grid data.
                            reloadButton.click(function (event) {
                                if (config.selectedtype == "ModuleId")
                                    loadFilters(config.selectedid);
                                if (config.selectedtype == "ElementId")
                                    loadAttributes(config.selectedid);
                            });
                        }
                        });
                    }
                    });

                    $("#feedContentExpander").jqxExpander({ toggleMode: 'none', showArrow: false, width: "100%", height: "100%", initContent: function () {
                        $('#previewpanel').jqxPanel({ theme: config.theme, width: '100%', height: '100%' });
                    }
                    });
                };




                var addEventListeners = function () {

                    // disable the default browser's context menu.
                    $('#productTree').on('contextmenu', function (e) {
                        return false;
                    });

                    $("#productTree").on('mousedown', function (event) {
                        var target = $(event.target).parents('li:first')[0];
                        var selectedItem = config.feedTree.jqxTree('selectedItem');
                        var rightClick = isRightClick(event);
                        if (rightClick && selectedItem != null) {
                            if (selectedItem.value.indexOf("PageId") >= 0 || selectedItem.value.indexOf("ModuleId") >= 0 || selectedItem.value.indexOf("ElementId") >= 0) {
                                config.feedTree.jqxTree('selectItem', target);
                                var scrollTop = $(window).scrollTop();
                                var scrollLeft = $(window).scrollLeft();
                                config.contextMenu.jqxMenu('open', parseInt(event.clientX) + 5 + scrollLeft, parseInt(event.clientY) + 5 + scrollTop);
                                return false;
                            }
                        }
                    });


                     $("#productTree").on('remove', function (event) {
                        var item = $.trim($(event.args).text());
                        var selectedItem = config.feedTree.jqxTree('selectedItem');
                    });

                    $('#productTree').on('select', function (event) {
                        var item = config.feedTree.jqxTree('getItem', event.args.element)
                        if (item != null) {
                            if (item.value.indexOf("PageId") >= 0) {
                                loadFilters(item.value.replace("PageId-", ""));
                            }
                            if (item.value.indexOf("ModuleId") >= 0) {
                                ModelModule(item.value.replace("ModuleId-", ""));
                            }
                            if (item.value.indexOf("ElementId") >= 0)
                                loadAttributes(item.value.replace("ElementId-", ""));
                        }
                    });



                    $("#jqxMenu").on('itemclick', function (event) {
                        var item = $.trim($(event.args).text());
                        getTreeViewParents(config.feedTree.jqxTree('selectedItem').parentElement);
                        switch (item) {
                            case "Edit Item":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    if (selectedItem.value.indexOf("PageId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddPageControl.ascx', { PageId: selectedItem.value.replace("PageId-", "") }, stateChanged);
                                    if (selectedItem.value.indexOf("ModuleId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddModuleControl.ascx', { ModuleId: selectedItem.value.replace("ModuleId-", "") }, stateChanged);
                                    if (selectedItem.value.indexOf("ElementId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddElementControl.ascx', { ElementId: selectedItem.value.replace("ElementId-", "") }, stateChanged);
                                }
                                break;
                            case "Remove Item":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    if (selectedItem.value.indexOf("PageId") >= 0)
                                        deleteObjectFromAjaxTabWithCallback('ContentManagerPage.aspx', 'Page', selectedItem.value.replace("PageId-", ""), stateChanged)
                                    if (selectedItem.value.indexOf("ModuleId") >= 0)
                                        deleteObjectFromAjaxTabWithCallback('ContentManagerPage.aspx', 'Module', selectedItem.value.replace("ModuleId-", ""), stateChanged)
                                    if (selectedItem.value.indexOf("ElementId") >= 0)
                                        deleteObjectFromAjaxTabWithCallback('ContentManagerPage.aspx', 'Element', selectedItem.value.replace("ElementId-", ""), stateChanged)
                                }
                                break;
                            case "Add Element":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    if (selectedItem.value.indexOf("PageId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddModuleControl.ascx', { PageId: selectedItem.value.replace("PageId-", "") }, stateChanged);
                                    if (selectedItem.value.indexOf("ModuleId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddElementControl.ascx', { ParentModuleId: selectedItem.value.replace("ModuleId-", "") }, stateChanged);
                                    if (selectedItem.value.indexOf("ElementId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddElementControl.ascx', { ParentElementId: selectedItem.value.replace("ElementId-", "") }, stateChanged);
                                }
                                break;
                            case "Clone":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    if (selectedItem.value.indexOf("PageId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/ClonePageControl.ascx', { PageId: selectedItem.value.replace("PageId-", "") }, stateChanged);
                                    if (selectedItem.value.indexOf("ModuleId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/CloneModuleControl.ascx', { ModuleId: selectedItem.value.replace("ModuleId-", "") }, stateChanged);
                                    if (selectedItem.value.indexOf("ElementId") >= 0)
                                        loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/CloneElementControl.ascx', { ElementId: selectedItem.value.replace("ElementId-", "") }, stateChanged);
                                }
                                break;
                        }
                    });

                    $("#rateCardContainer").on('rowdoubleclick', function (event) {
                        var data = $("#rateCardContainer").jqxGrid('getrowdata', event.args.rowindex)
                        if (data != null) {
                            if (data.ElementId != null)
                                showEditPopUp("ElementId", data.ElementId, data.AttributeTypeId);
                            if (data.PageId != null)
                                showEditPopUp("PageId", data.PageId, data.TagId);
                        }
                    });

                    showEditPopUp = function (type, id, subid) {
                        if (!subid) {
                            if (type == "PageId")
                                loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddPageFilterControl.ascx', { PageId: id }, gridChanged);
                            if (type == "ElementId")
                                loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddAttributesControl.ascx', { ElementId: id }, gridChanged);
                        }
                        else{
                            if (type == "PageId")
                                loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddPageFilterControl.ascx', { PageId: id, TagId: subid }, gridChanged);
                            if (type == "ElementId")
                                loadEditArrayControlWithCallback('#dialog-form', 'ContentManagerPage.aspx', 'WebContent/AddAttributesControl.ascx', { ElementId: id, AttributeTypeId: subid }, gridChanged);
                        }
                    }


                    $('#rateCardContainer').on('rowselect', function (event) {

                    });

                    $(".watcher").change(function (e) {
                        stateChanged()
                    });

                };

                var loadFilters = function (pageid) {
                    $(config.rateCardContainer).jqxGrid({ columns: config.filtercolumns });
                    config.selectedtype = "PageId";
                    config.selectedid = pageid;
                    $("#feedListExpander").jqxExpander('setHeaderContent', 'Filters for Page: ' + pageid);
                    $.ajax({
                        'dataType': 'json',
                        'type': "POST",
                        'url': config.dataDir + '/GetPageFilters',
                        'data': '{pageId:' + pageid + '}',
                        'contentType': 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data.d.length > 0) {
                                config.filtersource.localdata = data.d;
                                var adapter = new $.jqx.dataAdapter(config.filtersource);
                                // update data source.
                                config.rateCardContainer.jqxGrid({ source: adapter });
                                config.rateCardContainer.jqxGrid('selectrow', 0);
                            }
                            else {
                                config.rateCardContainer.jqxGrid('clear');
                            }
                        }
                    });
                };

                var loadAttributes = function (elementid) {
                    $(config.rateCardContainer).jqxGrid({ columns: config.attributecolumns });
                    config.selectedtype = "ElementId";
                    config.selectedid = elementid;
                    $("#feedListExpander").jqxExpander('setHeaderContent', 'Attributes for Element: ' + elementid);
                    $.ajax({
                        'dataType': 'json',
                        'type': "POST",
                        'url': config.dataDir + '/GetAttributes',
                        'data': '{elementId:' + elementid + '}',
                        'contentType': 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data.d.length > 0) {
                                config.attributesource.localdata = data.d;
                                var adapter = new $.jqx.dataAdapter(config.attributesource);
                                // update data source.
                                config.rateCardContainer.jqxGrid({ source: adapter });
                                config.rateCardContainer.jqxGrid('selectrow', 0);
                            }
                            else {
                                config.rateCardContainer.jqxGrid('clear');
                            }
                        }
                    });
                };

                var ModelModule = function (moduleid) {
                    var servicetypeid = $('#<%= listserviceTypes.ClientID %> option:selected').val();
                    $.ajax({
                        'dataType': 'json',
                        'type': "POST",
                        'url': config.dataDir + '/ModelModule',
                        data: JSON.stringify({ servicetypeid: servicetypeid, moduleid: moduleid }),
                        'contentType': 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data.d.length > 0) {
                                $("#previewpanel").jqxPanel("clearcontent");
                                $("#previewpanel").jqxPanel("append", data.d);
                            }
                            else {
                            }
                        }
                    });
                };


                gridChanged = function () {
                    if (config.selectedtype == "PageId")
                        loadFilters(config.selectedid);
                    if (config.selectedtype == "ElementId")
                        loadAttributes(config.selectedid);
                }

                stateChanged = function () {
                    getTreeViewData(
                            $('#<%= listlanguages.ClientID %> option:selected').val(),
                            $('#<%= listserviceTypes.ClientID %> option:selected').val(),
                            $('#<%= lookupString.ClientID %>').val(),
                            $('#<%= lookupFilter.ClientID %>').val()
                            )
                }

                getTreeViewData = function (languagecode, servicetypeid, lookupstring, lookupfilter) {
                    $.ajax({
                        dataType: 'json',
                        type: "POST",
                        url: config.dataDir + '/BuildTreeJson',
                        data: JSON.stringify({ languagecode: languagecode, servicetypeid: servicetypeid, lookupstring: lookupstring,lookupfilter:lookupfilter, opentobranchkey: config.treeselecteditemids }),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            config.feedTree.jqxTree('clear');
                            config.treeviewsource.localdata = data.d;
                            // create data adapter.
                            var dataAdapter = new $.jqx.dataAdapter(config.treeviewsource);
                            // perform Data Binding.
                            dataAdapter.dataBind();
                            var records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }, { name: 'value', map: 'value' }, { name: 'icon', map: 'icon' }]);
                            config.feedTree.jqxTree({ source: records });
                        }
                    });
                }

                getTreeViewParents = function (element) {

                    var parentElement = element;
                    config.treeselecteditemids = new Array();

                    while (parentElement != null) {
                        var parentItem = config.feedTree.jqxTree('getItem', parentElement);
                        var element = parentItem.element;
                        config.treeselecteditemids.push(element.id);
                        var parentElement = parentItem.parentElement;
                    }

                }

                var dataDir = 'ContentManagerPage.aspx';
                var config = {
                    theme: 'ui-redmond',
                    data: '',
                    feed: '',
                    treeselecteditemids: [],
                    dataDir: dataDir,
                    selectedratecardid: 0,
                    selectedid: 0,
                    selectedtype: null,
                    selectedunitid: 0,
                    selectedagencyid: 0,
                    selectedratecardcostingid: 0,
                    contextMenu: $('#jqxMenu'),
                    feedTree: $('#productTree'),
                    rateCardContainer: $('#rateCardContainer'),
                    feedItemContent: $('#feedItemContent'),
                    feedItemHeader: $('#feedItemHeader'),
                    feedUpperPanel: $('#feedUpperPanel'),
                    productHeader: $('#productHeader'),
                    feedContentArea: $('#feedContentArea'),
                    selectedIndex: -1,
                    currentFeed: '',
                    currentRateCardContent: {},
                    attributesource:
                            {
                                datafields:
                                [
                                    { name: 'ElementId' },
                                    { name: 'AttributeTypeId', type: 'number' },
                                    { name: 'Description', type: 'string' },
                                    { name: 'AttributeValue', type: 'string' }
                                ],
                                id: 'ElementId',
                                deleterow: function (rowid, commit) {
                                    $.ajax({
                                        'dataType': 'json',
                                        'type': "POST",
                                        'url': config.dataDir + '/DeleteRateCardCosting',
                                        //'data': '{ratecardcostingid:' + rowid + '}',
                                        'contentType': 'application/json; charset=utf-8',
                                        success: function () {
                                            commit(true);
                                        }
                                    });
                                }
                            },
                    filtersource:
                            {
                                datafields:
                                [
                                    { name: 'PageId', type: 'number' },
                                    { name: 'TagId', type: 'number' },
                                    { name: 'Tag', type: 'string' }
                                ],
                                id: 'ModuleId',
                                deleterow: function (rowid, commit) {
                                    $.ajax({
                                        'dataType': 'json',
                                        'type': "POST",
                                        'url': config.dataDir + '/DeleteRateCard',
                                        //'data': '{ratecardid:' + rowid + '}',
                                        'contentType': 'application/json; charset=utf-8',
                                        success: function () {
                                            commit(true);
                                        }
                                    });

                                }
                            },
                    treeviewsource:
                            {
                                datatype: "json",
                                datafields: [
                                    { name: 'id' },
                                    { name: 'parentid' },
                                    { name: 'text' },
                                    { name: 'value' },
                                    { name: 'expanded', type : 'bool'},
                                    { name: 'icon' }
                                ],
                                id: 'id',
                                localdata: ''
                            },
                    filtercolumns: [
                                            { text: 'Id', dataField: 'PageId', width: 100 },
                                            { text: 'TagId', dataField: 'TagId', width: 100 },
                                            { text: 'Value', dataField: 'Tag', width: 400 },

                                    ],
                    attributecolumns: [
                                            { text: 'Id', datafield: 'ElementId', width: 100 },
                                            { text: 'Type', datafield: 'AttributeTypeId', editable: false, displayfield: 'Description', width: 100 },
                                            { text: 'Html', datafield: 'AttributeValue', width: 400 },
                                    ]
                };
                return {
                    init: function () {
                        createWidgets();
                        addEventListeners();
                        stateChanged();
                    }
                }
            } (jQuery));
            rss.init();



        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <div id="pagename">
        <span style="font-style: italic; font-weight: bolder; font-size: larger">Quotes Page
        </span>
    </div>
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                             <tr>
                    <td>
                        Deployment
                    </td>
                    <td>
                        <asp:DropDownList ID="listserviceTypes" runat="server" DataTextField="Title" DataValueField="ServiceTypeId"
                            AppendDataBoundItems="true">
                        </asp:DropDownList>

                    </td>
                    <td>
                        Language
                    </td>
                    <td>
                        <asp:DropDownList ID="listlanguages" runat="server" DataTextField="Description" DataValueField="LanguageCode"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="All Languages" Value="">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        By String (Url,Text)
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="X-Large"></asp:TextBox>
                    </td>
                    <td>
                        By Filters
                    </td>
                    <td>
                        <asp:TextBox ID="lookupFilter" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="X-Large"></asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id='jqxWidget'>
            <div id="mainSplitter">
                <div>
                    <div style="border: none;" id="feedExpander">
                        <div class="jqx-hideborder">
                            <a href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ContentManagerPage.aspx','WebContent/AddPageControl.ascx',{},stateChanged)"><img src="../Content/img/actions/add.png">Add New Page</a>
                            <a href="#nogo" onclick="$('#productTree').jqxTree('collapseAll')">Close All</a>

                        </div>
                       
                         <div class="jqx-hideborder jqx-hidescrollbars">
                            <div class="jqx-hideborder" id='productTree'>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="contentSplitter">
                        <div class="feed-item-list-container" id="feedUpperPanel">
                            <div class="jqx-hideborder" id="feedListExpander">
                                <div class="jqx-hideborder" id="productHeader">
                                </div>
                                <div class="jqx-hideborder jqx-hidescrollbars">
                                    <div class="jqx-hideborder" id="rateCardContainer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="feedContentArea">
                            <div class="jqx-hideborder" id="feedContentExpander">
                                <div class="jqx-hideborder" id="feedItemHeader">
                                    Content Preview Pane
                                </div>
                               <div class="jqx-hideborder jqx-hidescrollbars">
                                    <div class="jqx-hideborder" id="previewpanel">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div id='jqxMenu'>
            <ul>
                <li>Edit Item</li>
                <li>Remove Item</li>
                <li>Add Element</li>
                <li>Clone</li>
            </ul>
        </div>
    </div>
</asp:Content>
