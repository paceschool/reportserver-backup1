﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;

public partial class WebContent_AddElement : BaseWebContentControl
{
    protected Int32 _elementId;
    protected Int32? _parentelementId;
    protected Int32? _moduleId;
    protected Pace.DataAccess.Content.Element _element;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("ElementId").HasValue)
        {
            Int32 ElementId = GetValue<Int32>("ElementId");
            using (Pace.DataAccess.Content.ContentEntities entity = new Pace.DataAccess.Content.ContentEntities())
            {
                _element = (from enroll in entity.Elements.Include("Xlk_ElementType")
                               where enroll.ElementId == ElementId
                               select enroll).FirstOrDefault();

            }

        }
        if (GetValue<Int32?>("ParentElementId").HasValue)
        {
            _parentelementId = GetValue<Int32>("ParentElementId");
        }
        if (GetValue<Int32?>("ParentModuleId").HasValue)
        {
            _moduleId = GetValue<Int32>("ParentModuleId");
        }
    }

    protected string GetElementId
    {
        get
        {
            if (_element != null)
                return _element.ElementId.ToString();

            return "0";
        }
    }

    protected string GetParentElementId
    {
        get
        {
            if (_parentelementId.HasValue)
                return _parentelementId.Value.ToString();

            return "0";
        }
    }

    protected string GetModuleId
    {
        get
        {
            if (_moduleId.HasValue)
                return _moduleId.Value.ToString();

            return "0";
        }
    }

    protected string GetElementValue
    {
        get
        {
            if (_element != null)
                return _element.ElementValue;

            return null;
        }
    }


    protected string GetElementTypeList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_element != null && _element.Xlk_ElementType != null) ? _element.Xlk_ElementType.ElementTypeId : (short?)null;

        foreach (Pace.DataAccess.Content.Xlk_ElementType item in this.LoadElementTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ElementTypeId, item.Description, (_current.HasValue && item.ElementTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}
