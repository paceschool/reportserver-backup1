﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_AddAttribute : BaseWebContentControl
{
    protected Int32 _elementId;
    protected Int32 _attributeTypeId;
    protected Pace.DataAccess.Content.Attribute _attribute;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("ElementId").HasValue)
        {
            _elementId = GetValue<Int32>("ElementId");
            if (GetValue<Int32?>("AttributeTypeId").HasValue)
            {
                _attributeTypeId = GetValue<Int32>("AttributeTypeId");

                using (Pace.DataAccess.Content.ContentEntities entity = new Pace.DataAccess.Content.ContentEntities())
                {
                    _attribute = (from enroll in entity.Attributes.Include("Xlk_AttributeType")
                                  where enroll.ElementId == _elementId && enroll.AttributeTypeId == _attributeTypeId
                                  select enroll).FirstOrDefault();

                }
            }
        }
    }

    protected string GetElementId
    {
        get
        {
            if (_attribute != null)
                return _attribute.ElementId.ToString();

            if (Parameters.ContainsKey("ElementId"))
                return Parameters["ElementId"].ToString();

            return "0";
        }
    }

    protected string GetAttributeTypeId
    {
        get
        {
            if (_attribute != null)
                return _attribute.AttributeTypeId.ToString();

            if (Parameters.ContainsKey("AttributeTypeId"))
                return Parameters["AttributeTypeId"].ToString();

            return "0";
        }
    }

    protected string GetAttributeValue
    {
        get
        {
            if (_attribute != null)
                return _attribute.AttributeValue;

            return null;
        }
    }

    protected string IsDisabled
    {
        get
        {
            if (_attribute != null && _attribute.AttributeTypeId != null && _attribute.AttributeTypeId > 0)
                return "disabled";

            return string.Empty;
        }
    }

    protected string GetAttributeTypeList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_attribute != null && _attribute.Xlk_AttributeType != null) ? _attribute.Xlk_AttributeType.AttributeTypeId : (int?)null;

        foreach (Xlk_AttributeType item in LoadAttributeTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.AttributeTypeId, item.Description, (_current.HasValue && item.AttributeTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }


}
