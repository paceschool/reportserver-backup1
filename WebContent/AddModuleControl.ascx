﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddModuleControl.ascx.cs"
    Inherits="WebContent_AddModule" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addcontent");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","SaveModule") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }


    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addcontent" name="addenrollment" method="post" action="ContentManagerPage.aspx/SaveModule">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ModuleId" value="<%= GetModuleId %>" />
        <input type="hidden" id="PageId" value="<%= GetPageId %>" />
        <label>
            Title</label>
        <input id="Title" value="<%= GetTitle %>" />
        <label>
            Description
        </label>
        <input id="Description" value="<%= GetDescription %>" />
        <label>
            ElementTarget
        </label>
        <input id="ElementTarget" value="<%= GetElementTarget %>" />
    </fieldset>
</div>
</form>
