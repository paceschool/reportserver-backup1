﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.Services;
using System.Text;
using Pace.DataAccess.WebSite;

public partial class WebContent_SiteRequestPage : BaseWebSitePage
{
    protected string month;
    private int[] statusFilter = new[] {3};

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listservivetypes.DataSource = this.LoadServiceTypes();
            listservivetypes.DataBind();

            listrequesttypes.DataSource = this.LoadRequestTypes();
            listrequesttypes.DataBind();

        }    
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["Action"]))
            ShowSelected(Request["Action"]);
        else 
            Click_LoadCurrentRequests();
    }


    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadRequests();
                break;
            case "current":
                Click_LoadCurrentRequests();
                break;
            default:
                Click_LoadAllRequests();
                break;
        }
    }

    protected void Click_LoadCurrentRequests()
    {
        DateTime today = DateTime.Now.AddDays(-14).Date;
        using (WebSiteEntities entity = new WebSiteEntities())
        {
            var requestSearchQuery = from s in entity.SiteRequests.Include("ServiceType").Include("RequestType")
                                     where s.DateCreated > today
                                     orderby s.DateCreated descending
                                     select s;

            LoadResults(requestSearchQuery.ToList());
        }
    }


    protected void Click_LoadAllRequests()
    {
        using (WebSiteEntities entity = new WebSiteEntities())
        {
            var requestSearchQuery = from s in entity.SiteRequests.Include("ServiceType").Include("RequestType")
                                     orderby s.DateCreated descending
                                     select s;
            LoadResults(requestSearchQuery.ToList());
        }
    }
     
    protected void lookupGroup(object sender, EventArgs e)
    {
        Click_LoadRequests();
    }

    private void LoadResults(List<SiteRequest> requests)
    {
        //Set the datasource of the repeater
        results.DataSource = requests;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", requests.Count().ToString());
    }

    protected void Click_LoadRequests()
    {

        int serviceTypeId = Convert.ToInt32(listservivetypes.SelectedItem.Value);
        int requestTypeId = Convert.ToInt32(listrequesttypes.SelectedItem.Value);


        using (WebSiteEntities entity = new WebSiteEntities())
        {
            string search = lookupString.Text.Trim();
            string search1 = RemoveDiacritics(lookupString.Text.Trim());


            var requestSearchQuery = from s in entity.SiteRequests.Include("ServiceType").Include("RequestType")
                                   select s;

            if (serviceTypeId > 0)
                requestSearchQuery = requestSearchQuery.Where(x => x.ServiceTypeId == serviceTypeId);

            if (requestTypeId > 0)
                requestSearchQuery = requestSearchQuery.Where(x => x.RequestTypeId == requestTypeId);

            if (!string.IsNullOrEmpty(search))
                requestSearchQuery = requestSearchQuery.Where(x => x.SiteRequestStrings.Any(y => search.Contains(y.Value)));

            LoadResults(requestSearchQuery.OrderByDescending(s => s.DateCreated).Take(300).ToList());
        }
    }

}
