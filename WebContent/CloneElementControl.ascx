﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CloneElementControl.ascx.cs"
    Inherits="WebContent_CloneElement" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#cloneelement");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","CloneElement") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="cloneelement" name="cloneelement" method="post" action="ContentManagerPage.aspx/CloneElement">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ElementId" value="<%= GetElementId %>" />
        <label>
            Clone All Children?</label>
        <input type="checkbox" checked id="Recurse" value="true" />
    </fieldset>
</div>
</form>
