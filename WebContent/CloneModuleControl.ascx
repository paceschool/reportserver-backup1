﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CloneModuleControl.ascx.cs"
    Inherits="WebContent_CloneModule" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#clonemodule");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","CloneModule") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="clonemodule" name="clonemodule" method="post" action="ContentManagerPage.aspx/CloneModule">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ModuleId" value="<%= GetModuleId %>" />
        <label>
            CLone All Children?</label>
        <input type="checkbox" checked id="Recurse" value="true" />
    </fieldset>
</div>
</form>
