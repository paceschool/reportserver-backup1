﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_AddModule : BaseWebContentControl
{
    protected Int32 _moduleId;
    protected Int32 _pageId;
    protected Pace.DataAccess.Content.Module _content;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("PageId").HasValue)
        {
            _pageId = GetValue<Int32>("PageId");

        }

        if (GetValue<Int32?>("ModuleId").HasValue)
        {
            _moduleId = GetValue<Int32>("ModuleId");
            using (Pace.DataAccess.Content.ContentEntities entity = new Pace.DataAccess.Content.ContentEntities())
            {
                _content = (from enroll in entity.Modules
                            where enroll.ModuleId == _moduleId
                               select enroll).FirstOrDefault();

            }

        }
    }


    protected string GetModuleId
    {
        get
        {
            if (_content != null)
                return _content.ModuleId.ToString();

            if (Parameters.ContainsKey("ModuleId"))
                return Parameters["ModuleId"].ToString();

            return "0";
        }
    }
    protected string GetPageId
    {
        get
        {
            if (_content != null)
                return _content.PageId.ToString();

            if (Parameters.ContainsKey("PageId"))
                return Parameters["PageId"].ToString();

            return "0";
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_content != null)
                return _content.Description;

            return null;
        }
    }

    protected string GetElementTarget
    {
        get
        {
            if (_content != null)
                return _content.ElementTarget;

            return null;

        }
    }

    protected string GetTitle
    {
        get
        {
            if (_content != null)
                return _content.Title;

            return null;

        }
    }


}
