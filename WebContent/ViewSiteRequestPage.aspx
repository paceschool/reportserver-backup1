﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewSiteRequestPage.aspx.cs" Inherits="ViewSiteRequestPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function() {

            var Id = <%= _siteRequestId %>;

            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function() {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function(xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function(result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="pagename">
        <span style="font-style: italic; font-weight: bolder; font-size: larger">View Group
            Page </span>
    </div>
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        <label>
                            SiteRequest ID
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="SiteRequestId" runat="server">
                        </asp:Label>
                         <a href="<%= ReportPath("SupportReports","WebSiteRequest","WORD", new Dictionary<string, object> { {"SiteRequestId",_siteRequestId}}) %>" title="Print Request Form"><img src="../Content/img/actions/printer.png" title="Print Request Form" /></a>&nbsp;
                        <a title="Create Student" href="#" onclick="loadEditArrayControl('#dialog-form','ViewSiteRequestPage.aspx','WebContent/AddStudentControl.ascx',{'SiteRequestId':<%= _siteRequestId%>})"><img src="../Content/img/actions/user_accept.png"></a>&nbsp;
                        <a title="Send Email" href="#" onclick="loadEditArrayControl('#dialog-form','ViewSiteRequestPage.aspx','WebContent/SendEmailControl.ascx',{'SiteRequestId':<%= _siteRequestId%>})"><img src="../Content/img/actions/email_go.png"></a>
                    </td>
                    <th scope="col">
                        <label>
                            Request Type
                        </label>
                    </th>
                    <td style="text-align: left;">
                        <asp:Label ID="RequestType" runat="server" Font-Bold="true" Font-Size="Larger">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Date Received
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="DateCreated" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Service Type
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="ServiceType" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            IpAddress
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="IpAddress" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Assigned To
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="AssignedTo" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
  
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="ViewSiteRequestPage.aspx/LoadFormStrings?id=<%= _siteRequestId %>">Form Received</a></li>
                <li><a href="ViewSiteRequestPage.aspx/LoadActionsTaken?id=<%= _siteRequestId %>">Actions Taken</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
