﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddPageFilterControl.ascx.cs"
    Inherits="WebContent_AddPageFilter" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addpagefilter");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","SavePageFilter") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addpagefilter" name="addenrollment" method="post" action="ContentManagerPage.aspx/SavePageFilter">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="PageId" value="<%= GetPageId %>" />
         <input type="hidden" id="TagId" value="<%= GetTagId %>" />
        <label>
            Tag</label>
        <input id="Tag" value="<%= GetTag %>" />
    </fieldset>
</div>
</form>
