﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_CloneModule : BaseWebContentControl
{
    protected Int32 _moduleId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("ModuleId").HasValue)
        {
            _moduleId = GetValue<Int32>("ModuleId");
        }
    }

    protected string GetModuleId
    {
        get
        {
            if (_moduleId != null)
                return _moduleId.ToString();

            if (Parameters.ContainsKey("ModuleId"))
                return Parameters["ModuleId"].ToString();

            return "0";
        }
    }


}
