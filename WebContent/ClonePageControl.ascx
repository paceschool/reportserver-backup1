﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClonePageControl.ascx.cs"
    Inherits="WebContent_ClonePage" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#clonePage");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","ClonePage") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="clonePage" name="clonePage" method="post" action="ContentManagerPage.aspx/ClonePage">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="PageId" value="<%= GetPageId %>" />
        <label>
            CLone All Children?</label>
        <input type="checkbox" checked id="Recurse" value="true" />
    </fieldset>
</div>
</form>
