﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_AddPage : BaseWebContentControl
{
    protected Int32 _PageId;
    protected Pace.DataAccess.Content.Page _content;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("PageId").HasValue)
        {
            _PageId = GetValue<Int32>("PageId");
            using (Pace.DataAccess.Content.ContentEntities entity = new Pace.DataAccess.Content.ContentEntities())
            {
                _content = (from enroll in entity.Pages.Include("Xlk_LanguageCode")
                            where enroll.PageId == _PageId
                               select enroll).FirstOrDefault();

            }

        }
    }


    protected string GetPageId
    {
        get
        {
            if (_content != null)
                return _content.PageId.ToString();

            if (Parameters.ContainsKey("PageId"))
                return Parameters["PageId"].ToString();

            return "0";
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_content != null)
                return _content.Description;

            return null;
        }
    }

    protected string GetPageTitle
    {
        get
        {
            if (_content != null)
                return _content.PageTitle;

            return null;

        }
    }

    protected string GetPageUrl
    {
        get
        {
            if (_content != null)
                return _content.PageURL;

            return null;

        }
    }



    protected string GetLanguageCodeList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_content != null && _content.Xlk_LanguageCode != null) ? _content.Xlk_LanguageCode.LanguageId : (int?)null;

        foreach (Xlk_LanguageCode item in LoadLanguageCodes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LanguageId, item.Description, (_current.HasValue && item.LanguageId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

}
