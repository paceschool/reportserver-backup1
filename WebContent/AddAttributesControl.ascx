﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddAttributesControl.ascx.cs"
    Inherits="WebContent_AddAttribute" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addattribute");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","SaveAttribute") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addattribute" name="addenrollment" method="post" action="ContentManagerPage.aspx/SaveAttribute">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ElementId" value="<%= GetElementId %>" />
        <label>
            Value</label>
        <input id="AttributeValue" value="<%= GetAttributeValue %>" />
        <label>
            Type</label>
        <select <%= IsDisabled %> name="Xlk_Attributetype" id="AttributetypeId">
            <%= GetAttributeTypeList()%>
        </select>
    </fieldset>
</div>
</form>
