﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.WebSite;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class WebContent_AddStudentControl : BaseEnrollmentControl
{
    protected SiteRequest _siteRequest;
    protected Int32 _siteRequestId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (WebSiteEntities entity = new WebSiteEntities())
        {
            if (GetValue<Int32?>("SiteRequestId").HasValue)
            {
                Int32 _siteRequestId = GetValue<Int32>("SiteRequestId");
                _siteRequest = (from st in entity.SiteRequests.Include("SiteRequestStrings")
                            where st.SiteRequestId == _siteRequestId
                            select st).FirstOrDefault();
            }
        }
    }

    protected string CheckExists
    {

        get
        {
          using(Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
          {

              var student = (from s in entity.Student
                             where s.FirstName == GetFirstName && s.SurName == GetSurName
                             select s);

              if (student == null || student.Count() == 0)
                  return "visibility:hidden";

              return "visibility:visible";

          }
        }

    }

    protected string GetStudentId
    {
        get
        {
            return "0";
        }
    }

    protected string GetToEmailAddress
    {
        get
        {
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 1))
                return _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 1).Value.ToString();

            return null;
        }
    }

    protected string GetGroupingBlockId
    {
        get
        {

            return "";
        }
    }

    protected string GetFirstName
    {
        get
        {
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 2))
                return _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 2).Value.ToString();

            return null;
        }
    }

    protected string GetSurName
    {
        get
        {
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 3))
                return _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 3).Value.ToString();

            return null;
        }
    }

    protected string GetGender()
    {
        StringBuilder sb = new StringBuilder();

        string _current = String.Empty;

        if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 6))
            _current = _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 6).Value.ToString();

        foreach (Pace.DataAccess.Enrollment.Xlk_Gender item in BaseEnrollmentControl.LoadGender())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.GenderId, item.Description, (!string.IsNullOrEmpty(_current) && item.Description == _current) ? "selected" : string.Empty);   
        }
        return sb.ToString();
    }

    protected string GetDateOfBirth
    {
        get
        {
            DateTime date = DateTime.Now;
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 5))
            {
                if (DateTime.TryParse(_siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 5).Value, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out date))
                    return date.ToString("dd/MM/yy");
            }
            return string.Empty;
        }
    }

    protected string GetNationality()
    {
        StringBuilder sb = new StringBuilder();
        string country = "Ireland";

        if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 11))
            country = _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 11).Value;

        foreach (Pace.DataAccess.Enrollment.Xlk_Nationality item in BaseEnrollmentControl.LoadNationality())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NationalityId, item.Description, (item.Description == country) ? "selected" : String.Empty);
        }
        return sb.ToString();
    }

    protected string GetExam()
    {
        StringBuilder sb = new StringBuilder();
        var exam = String.Empty;

        if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 93))
            exam  = _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 93).Value;

        sb.AppendFormat("<option value={0}>{1}</option>", -1, "");

        foreach (Pace.DataAccess.Enrollment.Exam item in BaseEnrollmentControl.LoadExam())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExamId, item.ExamName, (item.ExamName == exam) ? "selected" : String.Empty);
        }
        return sb.ToString();
    }

    protected string GetArrivalDate
    {
        get
        {
            DateTime date = DateTime.Now;
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 13))
            {
                if (DateTime.TryParse(_siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 13).Value, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out date))
                    return date.ToString("dd/MM/yy");
            }
                return string.Empty;
        }
    }

    protected string GetDepartureDate
    {
        get
        {
            DateTime date = DateTime.Now;
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 15))
            {
                if (DateTime.TryParse(_siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 15).Value, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out date))
                    return date.ToString("dd/MM/yy");
            }
            return string.Empty;
        }
    }

    protected string GetAgent()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Pace.DataAccess.Enrollment.Agency item in BaseEnrollmentControl.LoadAgency())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.AgencyId, item.Name, string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStatus()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current =1;

        foreach (Pace.DataAccess.Enrollment.Xlk_Status item in BaseEnrollmentControl.LoadStatus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current != null && item.StatusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetPlacement
    {
        get
        {

            return null;
        }
    }

    protected string GetStudentType()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (Byte?)1;

        foreach (Pace.DataAccess.Enrollment.Xlk_StudentType item in BaseEnrollmentControl.LoadStudentType())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StudentTypeId, item.Description, (_current != null && item.StudentTypeId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetCampus()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = 1;

        foreach (Pace.DataAccess.Security.Campus item in BaseEnrollmentControl.LoadCampus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.CampusId, item.Name, (_current != null && item.CampusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetPrepaidBook
    {
        get
        {

            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 27))
                return (_siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 27).Value.ToString() != "Rent") ? "checked" : string.Empty;

            return string.Empty;
        }
    }

    protected string GetPrepaidExam
    {
        get
        {

            return string.Empty;
        }
    }


    protected string GetOwnAccommodation
    {
        get
        {
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 18))
                return (_siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 18).Value.ToString() == "None") ? "checked" : string.Empty;

            return string.Empty;
        }
    }

    protected string GetHomeAddress
    {
        get
        {
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 27))
                return (_siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 27).Value.ToString() != "Rent") ? "checked" : string.Empty;


            return string.Empty;
        }
    }

    protected string GetStudentEmail
    {
        get
        {
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 1))
                return _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 1).Value.ToString();

            return string.Empty;
        }
    }

    protected string GetEmergencyContactNumber
    {
        get
        {
            if (_siteRequest != null && _siteRequest.SiteRequestStrings.Any(s => s.StringTypeId == 7))
                return _siteRequest.SiteRequestStrings.Single(s => s.StringTypeId == 7).Value.ToString();

            return string.Empty;
        }
    }

    protected string GetGroupList()
    {
        //StringBuilder sb = new StringBuilder("<option value=0>None</option>");

        //Int32? _current = (_student != null) ? _student.GroupId : (Byte?)null;

        //DateTime arrivaldate = (_student != null) ? _student.ArrivalDate.AddMonths(-1) : DateTime.MinValue;

        //using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
        //{

        //    var groups = from g in gentity.Group
        //                 where g.ArrivalDate >= arrivaldate
        //                 select new { g.GroupId, g.GroupName };

        //    foreach (var item in groups)
        //    {
        //        sb.AppendFormat("<option {2} value={0}>{1}</option>", item.GroupId, item.GroupName, (_current.HasValue && item.GroupId == _current) ? "selected" : string.Empty);
        //    }
        //}
        //return sb.ToString();
        return string.Empty;
    }


}