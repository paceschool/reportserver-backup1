﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddElementControl.ascx.cs"
    Inherits="WebContent_AddElement" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addelement");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/WebContent/ContentManagerPage.aspx","SaveElement") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }


    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addelement" name="addelement" method="post" action="ContentManagerPage.aspx/SaveElement">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ElementId" value="<%= GetElementId %>" />
        <input type="hidden" id="ParentElementId" value="<%= GetParentElementId %>" />
        <input type="hidden" id="ParentModuleId" value="<%= GetModuleId %>" />
        <label>
            Element Type</label>
        <select name="Xlk_ElementType" id="ElementTypeId">
            <%= GetElementTypeList()%>
        </select>
        <label>
            Value
        </label>
        <textarea style="width:500px;" rows="20" id="ElementValue"><%= GetElementValue %></textarea>
    </fieldset>
</div>
</form>
