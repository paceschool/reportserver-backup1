﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Content;

public partial class WebContent_CloneElement : BaseWebContentControl
{
    protected Int32 _elementId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("ElementId").HasValue)
        {
            _elementId = GetValue<Int32>("ElementId");
        }
    }

    protected string GetElementId
    {
        get
        {
            if (_elementId != null)
                return _elementId.ToString();

            if (Parameters.ContainsKey("ElementId"))
                return Parameters["ElementId"].ToString();

            return "0";
        }
    }
    

}
