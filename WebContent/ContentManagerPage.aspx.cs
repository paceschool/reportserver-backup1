﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using Pace.DataAccess.Content;
using System.Text;
using System.Collections.ObjectModel;


public partial class WebContent_ContentManagerPage : BaseWebContentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listlanguages.DataSource = this.LoadLanguageCodes();
            listlanguages.DataBind();

            listserviceTypes.DataSource = this.LoadServiceTypes();
            listserviceTypes.DataBind();

        }    
    }

    private static IEnumerable<object> BuildElements(string parentkey, string[] opentobranchkeys, List<Pace.DataAccess.Content.Element> elements)
    {
        List<object> _objects = new List<object>();
        foreach (Pace.DataAccess.Content.Element element in elements)
        {
            var key = string.Format("ElementId-{0}", element.ElementId);
            _objects.Add(new { id = key, icon = "../Content/img/actions/tv_element.png", parentid = parentkey, expanded = true, text = string.Format("{0} content {1}", element.Xlk_ElementType.Description, (!string.IsNullOrEmpty(element.ElementValue)) ? HttpUtility.HtmlEncode(element.ElementValue) : string.Empty), value = key });

            if (element.ChildElements.Count > 0)
                using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                {
                    _objects.AddRange(BuildElements(string.Format("ElementId-{0}", element.ElementId),opentobranchkeys, logic.GetElements((from i in element.ChildElements orderby i.Sequence select i.ChildElementId).ToList())));
                }
        }
        return _objects;
    }

    private static bool isExpanded(string currentid, string[] requestedids)
    {
        if (requestedids == null || requestedids.Count() <= 0)
            return false;

        return requestedids.Any(s => s.Equals(currentid, StringComparison.InvariantCultureIgnoreCase));

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> BuildTreeJson(string languagecode, byte servicetypeid, string lookupstring,string lookupfilter, string[] opentobranchkey)
    {

        List<Pace.DataAccess.Content.Page> _contents;
        List<string> filters = new List<string>(); ;

        using (ContentEntities entity = new ContentEntities())
        {
            if (!string.IsNullOrEmpty(lookupfilter))
            {
                string search = lookupfilter.Trim();
                filters = search.Split(',').ToList();
            }

            using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
            {
                if (languagecode == "en")
                    _contents = logic.FindContent(lookupstring,filters);
                else
                    _contents = logic.FindContent(lookupstring,filters, languagecode);

            }


            var languages = (from q in _contents select new { q.LanguageId, q.Xlk_LanguageCode }).Distinct();

            if (languages.Count() > 0)
            {

                foreach (var language in languages)
                {
                    var key = string.Format("LanguageId-{0}", language.LanguageId);
                    yield return new { id = string.Format("LanguageId-{0}", language.LanguageId), icon = "../Content/img/actions/tv_language.png", parentid = "-1", expanded = true, text = language.Xlk_LanguageCode.Description, value = key };


                    if (_contents.Count() > 0)
                    {
                        foreach (var page in _contents)
                        {
                            key = string.Format("PageId-{0}", page.PageId);
                            yield return new { id = key, icon = "../Content/img/actions/globe.png", parentid = string.Format("LanguageId-{0}", page.Xlk_LanguageCode.LanguageId), expanded = isExpanded(key, opentobranchkey), text = string.IsNullOrEmpty(page.PageURL) ? "Global" : page.PageURL, value = key };

                            if (page.Modules.Count() > 0)
                            {
                                foreach (var module in page.Modules)
                                {
                                    key = string.Format("ModuleId-{0}", module.ModuleId);
                                    yield return new { id = key, icon = "../Content/img/actions/tv_module.png", parentid = string.Format("PageId-{0}", module.PageId), expanded = isExpanded(key, opentobranchkey), text = module.Title, value = key };

                                    if (module.Lnk_Module_Element.Count > 0)
                                    {
                                        using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                                        {
                                            foreach (var item in BuildElements(key, opentobranchkey, logic.GetElements(module.Lnk_Module_Element.OrderBy(o => o.Sequence).Select(e => e.ElementId).ToList())))
                                            {
                                                yield return item;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }




    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> GetPageFilters(int pageId)
    {

        using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
        {
            return logic.GetPageFilters(pageId).Select(x => new { x.PageId, x.TagId, x.Tag }); 
        }

 
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ModelModule(byte servicetypeid, int moduleid)
    {
        StringBuilder sb = new StringBuilder();

        Pace.Common.Content.ContentHelper helper = new Pace.Common.Content.ContentHelper();


        var serviceType = helper.GetServiceType((byte?)null).Where(x=>x.ServiceTypeId == servicetypeid);

        string uri = (serviceType != null) ? serviceType.FirstOrDefault().BaseURI: "http://pace-web01/PaceWebSite/";

        sb.AppendFormat("<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"{0}content/css/layout.css\" />",uri);

        var module = helper.GetModules(servicetypeid, new[] { moduleid });

        if (module != null && module.Count() > 0)
            sb.Append(Pace.Common.Helper.Content.ModelContent.SimulateWeb(uri,module.FirstOrDefault().Elements));

        return sb.ToString();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)] 
    public static IEnumerable<object> GetAttributes(int elementId)
    {

        using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
        {
            return logic.GetAttributes(elementId).Select(x => new { x.ElementId, x.AttributeTypeId,x.Xlk_AttributeType.Description, x.AttributeValue });
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse CloneModule(Dictionary<string, object> newObject)
    {
        Int32? _moduleId;
        bool? _recurse;
        try
        {
            _moduleId = (newObject.ContainsKey("ModuleId")) ? Convert.ToInt32(newObject["ModuleId"]) : (Int32?)null;

            _recurse = (newObject.ContainsKey("Recurse")) ? Convert.ToBoolean(newObject["Recurse"]) : (bool?)null;

            if (_moduleId.HasValue)
            {
                using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                {
                    if (logic.CloneModule(_moduleId.Value, _recurse,(Int32?)null))
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("There was an issue cloning the module, please try again!"));
                }
            }
            return new AjaxResponse(new Exception("There was an issue pulling out the parameters, please try again!"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse ClonePage(Dictionary<string, object> newObject)
    {
        Int32? _pageId;
        bool? _recurse;
        try
        {
            _pageId = (newObject.ContainsKey("PageId")) ? Convert.ToInt32(newObject["PageId"]) : (Int32?)null;
            
            _recurse = (newObject.ContainsKey("Recurse")) ? Convert.ToBoolean(newObject["Recurse"]) : (bool?)null;

            if (_pageId.HasValue)
            {
                using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                {
                    if (logic.ClonePage(_pageId.Value, _recurse))
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("There was an issue cloning the module, please try again!"));
                }
            }
            return new AjaxResponse(new Exception("There was an issue pulling out the parameters, please try again!"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse CloneElement(Dictionary<string, object> newObject)
    {
        Int32? _elementId;
        bool? _recurse;
        try
        {
            _elementId = (newObject.ContainsKey("ElementId")) ? Convert.ToInt32(newObject["ElementId"]) : (Int32?)null;

            _recurse = (newObject.ContainsKey("Recurse")) ? Convert.ToBoolean(newObject["Recurse"]) : (bool?)null;

            if (_elementId.HasValue)
            {
                using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                {
                    if (logic.CloneElement(_elementId.Value, _recurse))
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("There was an issue cloning the element, please try again!"));
                }
            }
            return new AjaxResponse(new Exception("There was an issue pulling out the parameters, please try again!"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse DropItems(string draggeditemid, string draggedparentid, string droppeditemid, string droppedparentid, string dropPosition)
    {
        TreeMove treemove = new TreeMove(draggeditemid, draggedparentid, droppeditemid, droppedparentid, dropPosition);


        try
        {

            if (treemove.CompleteMove())
                return new AjaxResponse();

            else return new AjaxResponse(new Exception("There was a problem completing the move"));

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }

    }
}







