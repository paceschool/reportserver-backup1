﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SiteRequestPage.aspx.cs" Inherits="WebContent_SiteRequestPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("table").tablesorter()


            $("#<%= repeat.ClientID %>").buttonset();


            $('#<%= lookupString.ClientID %>').bind('keypress', function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) { //Enter keycode
                    eval($("#<%=searchButton.ClientID %>").attr('href'));
                }
            });

            refreshCurrentTab = function () {
                location.reload();
            }


        });
    </script>
    <style type="text/css">
        #toolbar {
            padding: 10px 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Web Enquiries Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>Nationality
                    </td>
                    <td>
                        <asp:DropDownList ID="listservivetypes" runat="server" DataTextField="Description"
                            DataValueField="ServiceTypeId" AppendDataBoundItems="true">
                            <asp:ListItem Text="All Service Types" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>or by Agent
                    </td>
                    <td>
                        <asp:DropDownList ID="listrequesttypes" runat="server" DataTextField="Description" DataValueField="RequestTypeId"
                            AppendDataBoundItems="true" EnableViewState="true">
                            <asp:ListItem Text="All Request Types" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Find By Name/Ref
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="X-Large"></asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="searchButton" PostBackUrl="~/WebContent/SiteRequestPage.aspx" runat="server"
                OnClick="lookupGroup"><span>Search Results</span>
            </asp:LinkButton>
            <a href="?Action=current">Current</a>
            <a href="?Action=all">All</a>
        </span><span style="float: right;">&nbsp;<asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
        </asp:Label>
        </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">Id
                    </th>
                    <th scope="col">Request Type
                    </th>
                    <th scope="col" style="width: 150px">Date Recorded
                    </th>
                    <th scope="col">From Site
                    </th>
                    <th scope="col">Request Type
                    </th>
                    <th scope="col">Actions
                    </th>
                </tr>
            </thead>
            <tbody style="font-size: smaller">
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "SiteRequestId") %>
                            </td>
                            <td>
                                <%# SiteRequestLink(DataBinder.Eval(Container.DataItem, "SiteRequestId"), "~/WebContent/ViewSiteRequestPage.aspx", DataBinder.Eval(Container.DataItem, "RequestType.Description").ToString()) %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "DateCreated", "{0:D}") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "ServiceType.Title") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "RequestType.Description") %>
                            </td>
                            <td><a target="_blank"  href="<%# ReportPath("SupportReports","WebSiteRequest","WORD", new Dictionary<string, object> { {"SiteRequestId",DataBinder.Eval(Container.DataItem, "SiteRequestId") }}) %>"
                                title="Print Request Form">
                                <img src="../Content/img/actions/printer.png" title="Print Request Form" /></a> &nbsp;
                                <a title="Send Email" href="#" onclick="loadEditArrayControl('#dialog-form','ViewSiteRequestPage.aspx','WebContent/SendEmailControl.ascx',{'SiteRequestId':<%#DataBinder.Eval(Container.DataItem, "SiteRequestId") %>})">
                                    <img src="../Content/img/actions/email_go.png"></a>&nbsp;
                                <%# ShowCreateStudentString(DataBinder.Eval(Container.DataItem, "SiteRequestId"),DataBinder.Eval(Container.DataItem, "RequestType.RequestTypeId")) %>
                                <a title="Delete Request" href="#" onclick="deleteObjectFromAjaxTab('ViewSiteRequestPage.aspx','SiteRequest',<%#DataBinder.Eval(Container.DataItem, "SiteRequestId") %>)"><img src="../Content/img/actions/bin_closed.png"></a>
                               
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
