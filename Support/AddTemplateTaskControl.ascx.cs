﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Support;
using System.Data;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;


public partial class Support_AddTemplateTaskControl : BaseSupportControl
{
    protected Int32 _templateTaskId, _parentTaskId;
    protected TemplateTask _templateTask;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int16?>("TemplateTaskId").HasValue)
        {
            Int16 _templateTaskId = GetValue<Int16>("TemplateTaskId");
            using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
            {
                _templateTask = (from task in entity.TemplateTasks.Include("Xlk_Priority").Include("Xlk_Status").Include("AssignedToUser").Include("Template")
                         where task.TemplateTaskId == _templateTaskId
                         select task).FirstOrDefault();
            }
        }
    }


    protected string GetTemplateTaskId
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.TemplateTaskId.ToString();

            if (GetValue<Int32?>("TemplateTaskId").HasValue)
                return GetValue<Int32>("TemplateTaskId").ToString();

            return "0";
        }
    }


    protected string GetTemplateId
    {
        get
        {
            if (_templateTask != null && _templateTask.Template != null)
                return _templateTask.Template.TemplateId.ToString();

            if (GetValue<Int32?>("TemplateId").HasValue)
                return GetValue<Int32>("TemplateId").ToString();

            return "0";
        }
    }
    protected string GetParentTemplateTaskId
    {
        get
        {
            if (_templateTask != null && _templateTask.ParentTask != null)
                return _templateTask.ParentTask.TemplateTaskId.ToString();

            if (GetValue<Int32?>("ParentTemplateTaskId").HasValue)
                return GetValue<Int32>("ParentTemplateTaskId").ToString();

            return "0";
        }
    }
    protected string GetPriorityList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_templateTask != null && _templateTask.Xlk_Priority != null) ? _templateTask.Xlk_Priority.PriorityId : (short?)null;

        foreach (Xlk_Priority item in LoadPriority())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.PriorityId, item.Description, (_current.HasValue && item.PriorityId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_templateTask != null && _templateTask.Xlk_Status != null) ? _templateTask.Xlk_Status.StatusId : (short?)null;


        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetAssignedToUserList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_templateTask != null && _templateTask.AssignedToUser != null) ? _templateTask.AssignedToUser.UserId : (int?)null;

        foreach (Users item in LoadUsers())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetDateUsedList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_templateTask != null && _templateTask.Xlk_UseDate != null) ? _templateTask.Xlk_UseDate.UseDateId : (int?)null;

        foreach (Xlk_UseDate item in LoadDateUsed())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.UseDateId, item.Description, (_current.HasValue && item.UseDateId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetDays
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.Days.ToString();

            return null;
        }
    }


    protected string GetActionNeeded
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.ActionNeeded;

            return null;
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.Description;

            return null;
        }
    }
       
    protected string GetTitle
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.Title;

            return null;
        }
    }
}