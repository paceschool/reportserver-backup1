﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddTemplateTaskControl.ascx.cs"
    Inherits="Support_AddTemplateTaskControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addtask");
        }

        $("#DateDue").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#DateCompleted").datepicker({ dateFormat: 'dd/mm/yy' });

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Support/ViewTaskPage.aspx","SaveTemplateTask") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>
<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addtask" name="addtask" method="post" action="ViewTaskPage.aspx/SaveTemplateTask">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="TemplateTaskId" value="<%= GetTemplateTaskId %>" />
        <input type="hidden" name="ParentTask" id="TemplateTaskId" value="<%= GetParentTemplateTaskId %>" />
        <input type="hidden" name="Template" id="TemplateId" value="<%= GetTemplateId %>" />
        <label>
            Assigned To
        </label>
        <select name="AssignedToUser" id="UserId">
            <%= GetAssignedToUserList()%>
        </select>
        <label>
            Title
        </label>
        <textarea rows="1" id="Title"><%= GetTitle%></textarea>
        <label>
            Task Description
        </label>
        <textarea rows="5" id="Description"><%= GetDescription%></textarea>
        <label>
            Action Needed
        </label>
        <textarea rows="5" id="ActionNeeded"><%= GetActionNeeded%></textarea>
        <label>
            Priority
        </label>
        <select name="Xlk_Priority" id="PriorityId">
            <%= GetPriorityList()%>
        </select>
        <label>
            Status
        </label>
        <select name="Xlk_Status" id="StatusId">
            <%= GetStatusList()%>
        </select>
        <label>
            Days from
        </label>
        <input id="Days" value="<%= GetDays %>" />
        <label>
            Date To Use
        </label>
        <select name="Xlk_UseDate" id="UseDateId">
            <%= GetDateUsedList()%>
        </select>
    </fieldset>
</div>
</form>
