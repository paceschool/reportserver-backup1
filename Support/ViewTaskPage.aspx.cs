﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Support;
using System.Data;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity.Core;

public partial class ViewTaskPage : BaseSupportPage
{
    protected Int32? _taskId;
    protected Task _task;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["TaskId"]))
            _taskId = Convert.ToInt32(Request["TaskId"]);

        if (!Page.IsPostBack)
        {
            newtaskpriority.DataSource = LoadPriority();
            newtaskpriority.DataBind();
            newtaskstatus.DataSource = LoadStatus();
            newtaskstatus.DataBind();
            newassignedto.DataSource = LoadUsers();
            newassignedto.DataBind();

            if (_taskId.HasValue)
            {
                DisplayTask(LoadTask(_taskId.Value));
            }
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    private Task LoadTask(Int32 taskId)
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Task> taskQuery = from t in entity.Task.Include("RaisedByUser").Include("Xlk_Priority").Include("Xlk_Status").Include("TaskAssignments").Include("AssignedToUser")
                                         where t.TaskId == taskId
                                         select t;

            if (taskQuery.ToList().Count() > 0)
            {
                _task = taskQuery.ToList().First();
                return _task;
            }
        }
        return null;
    }

    private void DisplayTask(Task task)
    {
        if (task != null)
        {
            TaskId.Text = task.TaskId.ToString();
            Description.Text = TrimText(task.Description,25);
            DateDue.Text = task.DateDue.ToString("dd/MM/yyyy");
            ActionNeeded.Text = task.ActionNeeded;

            newtaskdescription.Text = task.Description;
            newtaskactionneeded.Text = task.ActionNeeded;
            newtaskdatedue.Text = task.DateDue.ToString("dd/MM/yyyy");
            newtaskactiontaken.Text = task.ActionTaken;
            if (task.DateCompleted.HasValue)
                newtaskdatecompleted.Text = task.DateCompleted.Value.ToString("dd/MM/yyyy");
            else
                newtaskdatecompleted.Text = null;
            newtaskpriority.Items.FindByValue(task.Xlk_Priority.PriorityId.ToString()).Selected = true;
            if (task.Xlk_Status != null)
                newtaskstatus.Items.FindByValue(task.Xlk_Status.StatusId.ToString()).Selected = true;
            newraiseddate.Text = task.RaisedDate.ToString("dd/MM/yyyy");
            newtaskref.Text = task.Ref;
            newassignedto.Items.FindByValue(task.AssignedToUser.UserId.ToString()).Selected = true;
            closeLink.Visible = (task.AssignedToUser.UserId != GetCurrentUser().UserId || task.Xlk_Status.StatusId == 3) ? false :true;
            using (SupportEntities entity = new SupportEntities())
            {
                var getAssign = from u in entity.Users
                                where u.UserId == task.AssignedToUser.UserId
                                select u.UserId;
            }

            //newassignedto.SelectedIndex = getAssign.First()-1;
        }
    }

    protected void Click_Save(object sender, EventArgs e)
    {
        using (SupportEntities entity = new SupportEntities())
        {

            if (!string.IsNullOrEmpty(newtaskdescription.Text) || !string.IsNullOrEmpty(newtaskactionneeded.Text) || !string.IsNullOrEmpty(newtaskdatedue.Text)
                || !string.IsNullOrEmpty(newraiseddate.Text))
            {
                Task _task = GetTask();

                _task.ActionNeeded = newtaskactionneeded.Text;
                _task.ActionTaken = newtaskactiontaken.Text;

                if (!string.IsNullOrEmpty(newtaskdatecompleted.Text))
                    _task.DateCompleted = Convert.ToDateTime(newtaskdatecompleted.Text);

                _task.DateDue = !string.IsNullOrEmpty(newtaskdatedue.Text) ?Convert.ToDateTime(newtaskdatedue.Text) : DateTime.Now;
                _task.Description = newtaskdescription.Text;
                _task.RaisedDate = !string.IsNullOrEmpty(newraiseddate.Text) ? Convert.ToDateTime(newraiseddate.Text) : DateTime.Now;
                _task.Ref = newtaskref.Text;
                _task.RaisedByUserReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", BasePage.GetCurrentUser().UserId);
                _task.Xlk_PriorityReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Priority", "PriorityId", Convert.ToByte(newtaskpriority.SelectedItem.Value));
                _task.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newtaskstatus.SelectedItem.Value));
                _task.AssignedToUserReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", Convert.ToInt32(newassignedto.SelectedItem.Value)); 

                if (newtaskstatus.SelectedItem.Text != "Closed")
                    SaveTask(entity,_task);

                else if (!string.IsNullOrEmpty(newtaskactiontaken.Text) && newtaskstatus.SelectedItem.Text == "Closed")
                {
                    if (string.IsNullOrEmpty(newtaskdatecompleted.Text))
                        _task.DateCompleted = DateTime.Now;
                    SaveTask(entity,_task);
                }
                else
                {
                    string script = "<script type=\"text/javascript\">alert('Cannot Save Task! Task cannot be closed without Completion Date.');</script>";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script);
                    newtaskstatus.SelectedIndex = 0;
                }


            }
        }
    }

    private Task GetTask()
    {
        if(_taskId.HasValue)
        return LoadTask(_taskId.Value);

        return new Task();
    }

    private void SaveTask(SupportEntities entity,Task task)
    {
        if (!_taskId.HasValue)
            entity.AddToTask(task);

        if (entity.SaveChanges() > 0)
            Response.Redirect(string.Format("~/Support/TaskPage.aspx?Action=showall"));
    }

    private List<Task> LoadChildTasks(Int32 taskId)
    {
        using (SupportEntities entity = new SupportEntities())
        {
            IQueryable<Task> lookupQuery = from t in entity.Task.Include("Xlk_Priority").Include("RaisedByUser")
                                           where t.ParentTask.TaskId == taskId
                                           select t;

            return lookupQuery.ToList();
        }
    }

    protected void LoadChildren(Int32 taskId)
    {

    }
    protected string GetTaskId
    {
        get
        {
            if (_taskId != null)
                return _taskId.ToString();

            return "0";
        }
    }


}
