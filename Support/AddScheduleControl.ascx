﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddScheduleControl.ascx.cs"
    Inherits="Support_AddScheduleControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addschedule");
        }
        getTargetUrl = function () {
            return '<%= ToVirtual("~/Support/SchedulePage.aspx","SaveSchedule") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#LastDueDate").datepicker({ dateFormat: 'dd/mm/yy' });
    });
</script>
<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addschedule" name="addschedule" method="post" action="SchedulePage.aspx/SaveSchedule">
<div class="inputArea">
    <% if (_isedit)
       { %>
    <fieldset>
        <input type="hidden" id="ScheduleId" value="<%= GetScheduleId %>" />
        <label>
            Title
        </label>
        <textarea type="text" rows="1" id="ScheduleTitle" <%= ReadOnly %>><%= GetScheduleTitle %></textarea>
        <label>
            Description
        </label>
        <textarea type="text" rows="5" id="ScheduleDescription" <%= ReadOnly %>><%= GetScheduleDescription %></textarea>
        <label>
            Assigned To
        </label>
        <select name="AssignedToUser" id="UserId" <%= ReadOnly %>>
            <%= GetAssignedToUserList()%>
        </select>
        <label>
            Priority
        </label>
        <select name="Xlk_Priority" id="PriorityId">
            <%= GetPriorityList()%>
        </select>
    </fieldset>
    <fieldset>
        <label>
            Task Title
        </label>
        <textarea type="text" rows="1" id="TaskTitle" <%= ReadOnly %>><%= GetTaskTitle%></textarea>
        <label>
            Task Description
        </label>
        <textarea type="text" rows="5" id="TaskDescription" <%= ReadOnly %>><%= GetTaskDescription%></textarea>
        <label>
            Reference
        </label>
        <input type="text" id="Ref" value="<%= GetRef %>" <%= ReadOnly %> />
    </fieldset>
    <fieldset>
        <label>
            Action Needed
        </label>
        <textarea type="text" rows="5" id="ActionNeeded" <%= ReadOnly %>><%= GetActionNeeded%></textarea>
        <label>
            Cycle Days
        </label>
        <input type="text" id="CycleDays" value="<%= GetCycleDays %>" <%= ReadOnly %> />
        <label>
            Start Date
        </label>
        <input type="datetext" id="LastDueDate" value="<%= GetLastDueDate %>" <%= ReadOnly %> />
        <label>
            Status
        </label>
        <select name="Xlk_Status" id="StatusId">
            <%= GetStatusList()%>
        </select>
    </fieldset>
   <% } %>
</div>
</form>
