﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewTaskPage.aspx.cs" Inherits="ViewTaskPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {


            $("#viewTabs").tabs({
                spinner: 'Retrieving data...'
            });


            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                select: function (event, ui) {
                    if (ui.index == 2) {
                        loadgroupstudents(Id);
                    }
                },
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });


            $("#<%= newraiseddate.ClientID %>").datepicker( { dateFormat: 'dd/mm/yy' });
            $("#<%= newtaskdatecompleted.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<%= newtaskdatedue.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="View Task Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <div id="viewTabs">
            <ul>
                <li><a href="#viewTabs-1">View</a></li>
                <% if (_task == null || (_task != null &&  _task.AssignedToUser.UserId == GetCurrentUser().UserId))
                   { %>
                <li><a href="#viewTabs-2">Edit</a></li>
                <% } %>
            </ul>
            <div id="viewTabs-1">
                <table class="box-table-a">
                    <thead>
                        <tr>
                            <th scope="col">
                                <label>
                                    Task Id
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Description
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Due Date
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Action Needed
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Actions
                                </label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="TaskId" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Description" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="DateDue" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="ActionNeeded" runat="server">
                                </asp:Label>
                            </td>
                            <td>
                                <a id="closeLink" runat="server" onclick="loadEditArrayControl('#dialog-form','ViewTaskPage.aspx','Support/AddTaskControl.ascx',{{ TaskId: <%= _taskId %>, Close:<%= _taskId %>}} )"
                                    href="#" title="Close Task">
                                    <img src="../Content/img/actions/user_accept.png"></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="viewTabs-2">
                <div id="errorcontainer" name="errorcontainer" class="error" runat="server" visible="false">
                    <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
                </div>
                <div class="inputArea">
                    <fieldset>
                        <label>
                            Task Description
                        </label>
                        <asp:TextBox ID="newtaskdescription" runat="server">
                        </asp:TextBox>
                        <label>
                            Action Needed
                        </label>
                        <asp:TextBox ID="newtaskactionneeded" runat="server">
                        </asp:TextBox>
                        <label>
                            Date Due
                        </label>
                        <asp:TextBox ID="newtaskdatedue" runat="server">
                        </asp:TextBox>
                        <label>
                            Action Taken
                        </label>
                        <asp:TextBox ID="newtaskactiontaken" runat="server">
                        </asp:TextBox>
                        <label>
                            Date Completed
                        </label>
                        <asp:TextBox ID="newtaskdatecompleted" runat="server">
                        </asp:TextBox>
                    </fieldset>
                    <fieldset>
                        <label>
                            Assigned To
                        </label>
                        <asp:DropDownList ID="newassignedto" runat="server" DataTextField="Name" DataValueField="UserId"
                            EnableViewState="true">
                        </asp:DropDownList>
                        <label>
                            Priority
                        </label>
                        <asp:DropDownList ID="newtaskpriority" runat="server" DataTextField="Description"
                            DataValueField="PriorityId" EnableViewState="true">
                        </asp:DropDownList>
                        <label>
                            Status
                        </label>
                        <asp:DropDownList ID="newtaskstatus" runat="server" DataTextField="Description" DataValueField="StatusId"
                            EnableViewState="true">
                        </asp:DropDownList>
                        <label>
                            Raised Date
                        </label>
                        <asp:TextBox ID="newraiseddate" runat="server">
                        </asp:TextBox>
                                                <label>
                            Reference
                        </label>
                        <asp:TextBox ID="newtaskref" runat="server">
                        </asp:TextBox>
                    </fieldset>
                    <fieldset>
                        <asp:LinkButton ID="newsave" runat="server" class="button" OnClick="Click_Save"><span>Save</span></asp:LinkButton>
                        <asp:LinkButton ID="cancel" runat="server" class="button" PostBackUrl="~/Support/TaskPage.aspx?Action=showall"><span>Cancel</span></asp:LinkButton>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="ViewTaskPage.aspx/LoadChildTasks?id=<%= _taskId %>">Sub Tasks</a></li>
                <li><a href="ViewTaskPage.aspx/LoadTaskAssignments?id=<%= _taskId %>">History</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
