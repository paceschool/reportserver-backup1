﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Support;
using System.Data;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
 
public enum TaskState
{
    Create = 1,
    Edit = 2,
    Close = 3,
}
public partial class Support_AddTaskControl : BaseSupportControl
{
    protected Int32 _taskId, _parentTaskId;
    protected Task _task;
    protected TaskState _state = TaskState.Create;
    protected List<Task> _childBlockingTasks;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("TaskId").HasValue)
        {
            _state = TaskState.Edit;
            _taskId = GetValue<Int32>("TaskId");
            using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
            {
                _task = (from task in entity.Task.Include("TaskAssignments").Include("Ticket").Include("Xlk_Priority").Include("Xlk_Status").Include("RaisedByUser").Include("AssignedToUser")
                         where task.TaskId == _taskId
                         select task).FirstOrDefault();
            }
        }

        if (GetValue<bool?>("Close").HasValue)
        {
            _state = TaskState.Close;
            using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
            {
                List<long> ids = entity.LoadAllParentTasks(_taskId).Select(x => x.TaskId).ToList();
                _childBlockingTasks =
                    (from t in entity.Task.Include("AssignedToUser")
                     where ids.Contains(t.TaskId)
                     && t.Xlk_Status.StatusId != 3
                     select t).ToList();
            }
        }
    }

    protected string ReadOnly
    {
      get
        {
          if ((_task != null && _task.RaisedByUser.UserId != GetCurrentUser().UserId) || _state == TaskState.Close)
                return "readonly=\"readonly\"";

            return string.Empty;
        }
    }

    protected bool Owner
    {
        get
        {
            if ((_task != null && _task.RaisedByUser.UserId == GetCurrentUser().UserId))
                return true;

            return false;
        }
    }

    protected short? GetStatus
    {
        get
        {
            if (_state == TaskState.Close)
                return 3;

            if (_state == TaskState.Edit)
                return (_task != null && _task.Xlk_Status != null) ? _task.Xlk_Status.StatusId : (short?)null;

            return 2;
        }
    }

    protected string GetTaskId
    {
        get
        {
            if (_task != null)
                return _task.TaskId.ToString();

            if (GetValue<Int32?>("TaskId").HasValue)
                return GetValue<Int32>("TaskId").ToString();

            return "0";
        }
    }

    protected string GetParentTaskId
    {
        get
        {
            if (_task != null && _task.ParentTask != null)
                return _task.ParentTask.TaskId.ToString();

            if (GetValue<Int32?>("ParentTaskId").HasValue)
                return GetValue<Int32>("ParentTaskId").ToString();

            return "0";
        }
    }
    protected string GetPriorityList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_task != null && _task.Xlk_Priority != null) ? _task.Xlk_Priority.PriorityId : (short?)null;

        foreach (Xlk_Priority item in LoadPriority())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.PriorityId, item.Description, (_current.HasValue && item.PriorityId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = GetStatus;


        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetAssignedToUserList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_task != null && _task.AssignedToUser != null) ? _task.AssignedToUser.UserId : (int?)null;

        foreach (Users item in LoadUsers())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetRaisedByUserList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_task != null && _task.RaisedByUser != null) ? _task.RaisedByUser.UserId : (int?)null;

        foreach (Users item in LoadUsers())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetRef
    {
        get
        {
            if (_task != null)
                return _task.Ref;

            if (GetValue<string>("Ref")!= null)
                return GetValue<string>("Ref").ToString();

            return null;
        }
    }

    protected string GetDateCompleted
    {
        get
        {
            if (_task != null && _task.DateCompleted.HasValue)
                return _task.DateCompleted.Value.ToString("dd/MM/yyyy"); ;

            return null;
        }
    }

    protected string GetActionTaken
    {
        get
        {
            if (_task != null)
                return _task.ActionTaken;

            return null;
        }
    }

    protected string GetActionNeeded
    {
        get
        {
            if (_task != null)
                return _task.ActionNeeded;

            return null;
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_task != null)
                return _task.Description;

            return null;
        }
    }

    protected string GetRaisedDate
    {
        get
        {
            if (_task != null)
                return _task.RaisedDate.ToString("dd/MM/yyyy");

            return null;
        }
    }

    protected string GetDateDue
    {
        get
        {
            if (_task != null)
                return _task.DateDue.ToString("dd/MM/yyyy");

            return null;
        }
    }

}