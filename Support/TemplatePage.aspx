﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TemplatePage.aspx.cs" Inherits="TemplatePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("#<%= repeat.ClientID %>").buttonset();

            $('ul.tree li:last-child').addClass('last');
            $('ul.tree li b').addClass('draggable');

            refreshCurrentTab = function () {
                location.reload();
            }

            $(".draggable").draggable({
                helper: "clone",
                revert: "invalid",
                containment: ".resultscontainer",
                scroll: false
            });

            $(".droppable").droppable({
                accept: ".draggable",
                activeClass: "ui-state-hover",
                hoverClass: "ui-state-active",
                drop: function (event, ui) {
                    $(this).addClass("ui-state-highlight");
                    assignTaskToParent(this.id, ui.draggable.attr('id'))
                }
            });




            assignTaskToParent = function (parenttasktemplateid, tasktemplateid) {
                $.ajax({
                    type: "POST",
                    url: "TemplatePage.aspx/AssignTaskToParent",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ parenttasktemplateid: parenttasktemplateid, tasktemplateid: tasktemplateid }),
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                location.reload();
                            }

                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            };

        });
    </script>
    <style type="text/css">
        div.example
        {
            background-color: #FFFFFF;
            border: 2px dashed #CCCCCC;
            margin: auto 4em;
            padding: 1em;
        }
        ul.tree, ul.tree ul
        {
            background: url("../Content/img/treeview/vline.png") repeat-y scroll 0 0 transparent;
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        ul.tree ul
        {
            margin-left: 10px;
        }
        ul.tree li
        {
            background: url("../Content/img/treeview/node.png") no-repeat scroll 0 0 transparent;
            color: #336699;
            font-weight: bold;
            line-height: 20px;
            margin: 0;
            padding: 0 12px;
        }
        ul.tree li.last
        {
            background: url("../Content/img/treeview/lastnode.png") no-repeat scroll 0 0 #FFFFFF;
        }
        ul.tree-a, ul.tree-a ul
        {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        ul.tree-a ul
        {
            background: url("../Content/img/treeview/vline.png") repeat-y scroll left bottom transparent;
            margin-left: 10px;
        }
        ul.tree-a li
        {
            color: #336699;
            font-size: 14px;
            font-weight: bold;
            line-height: 20px;
            margin: 0;
            padding: 0 12px;
        }
        ul.tree-a ul li
        {
            background: url("../Content/img/treeview/node.png") no-repeat scroll 0 0 transparent;
        }
        ul.tree-a ul li.last
        {
            background: url("../Content/img/treeview/lastnode.png") no-repeat scroll 0 0 #FFFFFF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Task Template Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Filter Templates by
                    </td>
                    <td>
                        <label>
                            Templates
                        </label>
                        <asp:DropDownList ID="templates" runat="server" DataTextField="Title" DataValueField="TemplateId"
                            EnableViewState="true" AppendDataBoundItems="true" AutoPostBack="True" 
                            onselectedindexchanged="templates_SelectedIndexChanged" >
                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
        </span>
        <div>
            <ul class="tree">
            <%= Click_LoadAllTemplates()%>
            </ul>
        </div>
        <asp:TreeView ID="listTemplateTasks" runat="server" OnSelectedNodeChanged="listTemplateTasks_SelectedNodeChanged">
        </asp:TreeView>
    </div>
</asp:Content>
