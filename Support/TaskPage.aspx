﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TaskPage.aspx.cs" Inherits="TaskPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("#<%= repeat.ClientID %>").buttonset();

            $("table").tablesorter();

            refreshCurrentTab = function () {
                location.reload();
            }

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Tasks Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Filter Tasks by
                    </td>
                    <td>
                        <label>
                            Priority
                        </label>
                        <asp:DropDownList ID="taskpriority" runat="server" DataTextField="Description" DataValueField="PriorityId"
                            EnableViewState="true">
                        </asp:DropDownList>
                        <label>
                            Status
                        </label>
                        <asp:DropDownList ID="taskstatus" runat="server" DataTextField="Description" DataValueField="StatusId"
                            EnableViewState="true">
                        </asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="Click_LookupTasks"><span>Search My Tasks</span>
            </asp:LinkButton>
            <a href="?Action=todays">Todays Tasks</a> <a href="?Action=incomplete">Incomplete Tasks</a>
            <a href="?Action=completed">Completed</a> <a href="?Action=createdbyme">Created By Me</a>
        </span><span style="float: right;">
            <asp:Label ID="resultsreturned" runat="server" Text='Showing Tasks For: 0 ----- Records Found: 0'>
            </asp:Label>
        </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Task Id
                    </th>
                    <th scope="col">
                        Ref
                    </th>
                    <th scope="col">
                        Priority
                    </th>
                    <th scope="col">
                        Status
                    </th>
                    <th scope="col">
                        Description
                    </th>
                    <th scope="col">
                        Action Needed
                    </th>
                    <th scope="col">
                        Date Due
                    </th>
                    <th scope="col">
                        Raised By
                    </th>
                    <th scope="col">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center">
                                <%#DataBinder.Eval(Container.DataItem, "TaskId") %>
                            </td>
                            <td style="text-align: left">
                                <%# BuildPopUp(DataBinder.Eval(Container.DataItem, "Ref"))%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Priority")%>
                            </td>
                            <td style="text-align: left">
                                <%# PopUpLink("TaskId", DataBinder.Eval(Container.DataItem, "TaskId"), "~/Support/ViewTaskPage.aspx", DataBinder.Eval(Container.DataItem, "Xlk_Status.Description").ToString(), "PopUpTask")%>
                            </td>
                            <td style="text-align: left; font-weight: bold; font-size: medium" id="taskdesc"
                                runat="server">
                                <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# TrimText(DataBinder.Eval(Container.DataItem, "Description"),30)%>'
                                    ToolTip='<%#DataBinder.Eval(Container.DataItem, "Description") %>' />
                            </td>
                            <td style="text-align: left">
                                <asp:HyperLink ID="linkaddress" runat="server" Text='<%# TrimText(DataBinder.Eval(Container.DataItem, "ActionNeeded"),30)%>'
                                    ToolTip='<%#DataBinder.Eval(Container.DataItem, "ActionNeeded") %>' />
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "DateDue", "{0:D}") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "RaisedBy.Name")%>
                            </td>
                            <td style="text-align: center">
                                <a <%# IsAssignedToMeAndNotClosed(DataBinder.Eval(Container.DataItem, "AssignedTo.UserId"),DataBinder.Eval(Container.DataItem, "Xlk_Status.StatusId")) %>
                                    onclick="loadEditArrayControl('#dialog-form','ViewTaskPage.aspx','Support/AddTaskControl.ascx',{ TaskId: <%#DataBinder.Eval(Container.DataItem, "TaskId") %>, Close:<%#DataBinder.Eval(Container.DataItem, "TaskId") %>} )"
                                    href="#" title="Close Task">
                                    <img src="../Content/img/actions/user_accept.png"></a> <a href='ViewTaskPage.aspx?TaskId=<%#DataBinder.Eval(Container.DataItem, "TaskId") %>'
                                        title="Manage Task">
                                        <img src="../Content/img/actions/arrow_right.png" /></a><a <%# IsMine(DataBinder.Eval(Container.DataItem, "RaisedBy.UserId")) %>
                                            onclick="loadEditArrayControl('#dialog-form','ViewTaskPage.aspx','Support/AddTaskControl.ascx',{'TaskId':<%#DataBinder.Eval(Container.DataItem, "TaskId") %>})"
                                            href="#"><img src="../Content/img/actions/edit.png"></a><a <%# IsMine(DataBinder.Eval(Container.DataItem, "RaisedBy.UserId")) %>
                                                onclick="deleteObjectFromAjaxTab('TaskPage.aspx','Task',<%#DataBinder.Eval(Container.DataItem, "TaskId") %>)"
                                                href="#" title="Delete Item"><img src="../Content/img/actions/bin_closed.png"></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
