﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SchedulePage.aspx.cs" Inherits="TaskPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("#<%= repeat.ClientID %>").buttonset();

            refreshCurrentTab = function () {
                location.reload();
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Task Schedules Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        To be Created for Users
                    </td>
                    <td>
                        <asp:DropDownList ID="listusers" runat="server" DataTextField="Name" DataValueField="UserId"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="All Users" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        .
                    </td>
                </tr>
                <tr>
                    <td>
                        Find By Title/Description/Ref
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" OnTextChanged="lookuptitle" Font-Size="X-Large"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="LinkButton2" PostBackUrl="~/Support/SchedulePage.aspx" runat="server"
                OnClick="lookupSchedules"><span>Search Results</span>
            </asp:LinkButton>
            <a href="?Action=all">All</a></span> <span style="float: right;"><a onclick="loadEditArrayControl('#dialog-form','SchedulePage.aspx','Support/AddScheduleControl.ascx',{'ScheduleId':0})"
                href="#">
                <img src="../Content/img/actions/add.png">
                Add New Schedule </a>
                <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
                </asp:Label>
            </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Title
                    </th>
                    <th scope="col">
                        Description
                    </th>
                    <th scope="col">
                        Schedule Days
                    </th>
                    <th scope="col">
                        Task Title
                    </th>
                    <th scope="col">
                        Assign to Role
                    </th>
                    <th scope="col">
                        Assign to
                    </th>
                    <th scope="col">
                        Action Needed
                    </th>
                    <th scope="col">
                        Priority
                    </th>
                    <th scope="col">
                        Ref
                    </th>
                    <th scope="col">
                        Status
                    </th>
                    <th scope="col">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody style="font-size: smaller">
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center">
                                <%#DataBinder.Eval(Container.DataItem, "ScheduleId")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "ScheduleTitle")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "ScheduleDescription")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "CycleDays")%>
                            </td>
                            <td style="text-align: left">
                                <%# DataBinder.Eval(Container.DataItem, "TaskTitle") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "AssignedToRoleName")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "AssignedToUser.Name")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "ActionNeeded")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Xlk_Priority.Description")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Ref")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Xlk_Status.Description")%>
                            </td>
                            <td style="text-align: left">
                                <a onclick="loadEditArrayControl('#dialog-form','SchedulePage.aspx','Support/AddScheduleControl.ascx',{'ScheduleId':<%#DataBinder.Eval(Container.DataItem, "ScheduleId")%>})"
                                    href="#"><img src="../Content/img/actions/edit.png"></a>
                                    <a onclick="deleteObjectFromAjaxTab('SchedulePage.aspx','SupportSchedule',<%#DataBinder.Eval(Container.DataItem, "ScheduleId")%>)" href="#" title="Delete Schedule"><img src="../Content/img/actions/bin_closed.png"></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
