﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="ManageBusTicketsPage.aspx.cs" Inherits="Support_ManageBusTicketsPage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(function() {
            
            $("#viewTabs").tabs({ spinner: 'Retrieving data...' });

            $("table").tablesorter()

            refreshCurrentTab = function() {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function(xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function(result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="pagename">
        <span style="font-style:italic; font-weight:bolder; font-size:larger">
            Manage Transport Tickets Page
        </span>
    </div>
    <div class="resultscontainer">
        <div id="viewTabs">
            <ul>
                <li><a href="#viewTabs-1">Ticket Levels Remaining</a></li>
                <li><a href="#viewTabs-2">Add Tickets</a></li>
            </ul>
            <div id="viewTabs-1">
                <table class="box-table-a">
                    <thead>
                        <tr>
                            <th scope="col">
                                <label>
                                    Ticket Type
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Amount Remaining
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Action Needed
                                </label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:HyperLink ID="wizard" runat="server" NavigateUrl="~/Enrollments/AddBusTicketsWizard.aspx">Add</asp:HyperLink>
                        <tr>
                            <td scope="col" style="text-align:center">
                                <label>
                                    4 week Bus & DART
                                </label>
                            </td>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="fourweekamount" runat="server" Font-Bold="true" Font-Size="Larger">
                                </asp:Label>
                            </td>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="reorderfour" runat="server" Font-Bold="true" Font-Size="Large">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td scope="col" style="text-align:center">
                                <label>
                                    3 week Bus & DART
                                </label>
                            </td>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="threeweekamount" runat="server" Font-Bold="true" Font-Size="Larger">
                                </asp:Label>
                            </td>
                            
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="reorderthree" runat="server" Font-Bold="true" Font-Size="Large">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td scope="col" style="text-align:center">
                                <label>
                                    2 week Bus & DART
                                </label>
                            </td>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="twoweekamount" runat="server" Font-Bold="true" Font-Size="Larger">
                                </asp:Label>
                            </td>
                            
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="reordertwo" runat="server" Font-Bold="true" Font-Size="Large">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td scope="col" style="text-align:center">
                                <label>
                                    1 week Bus & DART
                                </label>
                            </td>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="oneweekamount" runat="server" Font-Bold="true" Font-Size="Larger">
                                </asp:Label>
                            </td>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="reorderone" runat="server" Font-Bold="true" Font-Size="Large">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="tenjourney" runat="server" Font-Bold="true" Font-Size="Larger">
                                </asp:Label>
                            </td>
                            <td scope="col" style="text-align:center">
                                <asp:Label ID="reordertenj" runat="server" Font-Bold="true" Font-Size="Large">
                                </asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="viewTabs-2">
                <div id="errorcontainer" name="errorcontainer" class="error" runat="server" visible="false">
                    <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
                </div>
                <div class="inputArea">
                    <fieldset>
                        <label>
                            Bus Ticket Type</label>
                        <asp:DropDownList ID="bustickettype" runat="server" DataTextField="BusTicketType" DataValueField="BusTicketTypeId">
                        </asp:DropDownList>
                        <label>
                            Number of Tickets</label>
                        <asp:TextBox ID="numbertoadd" runat="server">
                        </asp:TextBox>
                        <label>
                            Serial Number Start
                        </label>
                        <asp:TextBox ID="serialstart" runat="server">
                        </asp:TextBox>
                        <label>
                            Serial Number End
                        </label>
                        <asp:TextBox ID="serialend" runat="server">
                        </asp:TextBox>
                        <asp:LinkButton ID="busticketsave" runat="server" class="button" OnClick="Click_Save"><span>Save</span>
                        </asp:LinkButton>
                        <asp:LinkButton ID="busticketcancel" runat="server" class="button"><span>Cancel</span>
                        </asp:LinkButton>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Currently Assigned Tickets</a></li>
                <li><a href="ManageBusTicketsPage.aspx/LoadUnpaidBusTickets">Outstanding Payments Due</a></li>
                <li><a href="ManageBusTicketsPage.aspx/LoadExpiredTickets">Previous/Expired Tickets</a></li>
            </ul>
            <div id="tabs-1">
                <table id="box-table-a" class="tablesorter">
                    <thead>
                        <tr>
                            <th scope="col">
                                Assigned Ticket Id
                            </th>
                            <th scope="col">
                                Student
                            </th>
                            <th scope="col">
                                Ticket Type
                            </th>
                            <th scope="col">
                                Start Date
                            </th>
                            <th scope="col">
                                Expiry Date
                            </th>
                            <th scope="col">
                                Serial No.
                            </th>
                            <th scope="col">
                                Family
                            </th>
                            <th scope="col">
                                Paid Y/N
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="results" runat="server" EnableViewState="True">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "AssignedTicketId") %>
                                    </td>
                                    <td style="text-align: left">
                                     <%# StudentLink(DataBinder.Eval(Container.DataItem, "Student.StudentId"), "~/Enrollments/ViewStudentPage.aspx", CreateName(DataBinder.Eval(Container.DataItem, "Student.FirstName"), DataBinder.Eval(Container.DataItem, "Student.SurName")),"#ui-tabs-3")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "BusTicket.Xlk_BusTicketType.BusTicketType") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "FromDate", "{0:D}") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#CalculateTicketExpiry(Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "FromDate")),Convert.ToInt32(DataBinder.Eval(Container.DataItem, "BusTicket.Xlk_BusTicketType.BusTicketTypeId"))) %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "BusTicket.SerialNUmber") %>
                                    </td>
                                    <td style="text-align:left">
                                        <a title="Go to Family" href="ViewStudentPage.aspx?StudentId=<%#DataBinder.Eval(Container.DataItem, "Student.StudentId") %>#ui-tabs-4">
                                        <img src="../Content/img/actions/house_go.png" /></a>
                                    </td>
                                    <td style="text-align: right; font-weight:bold">
                                        <%#DataBinder.Eval(Container.DataItem, "AmountPaid", "{0:c}") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>