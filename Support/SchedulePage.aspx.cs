﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Support;
using System.Data;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;

public partial class TaskPage :BaseSupportPage
{
    protected Int32 userId, taskId;
    
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!Page.IsPostBack)
        {
            listusers.DataSource = LoadUsers();
            listusers.DataBind();

        if (!string.IsNullOrEmpty(Request["Action"]))
            ShowSelected(Request["Action"]);
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_SearchSchedules();
                break;
            case "all":
                Click_LoadAllSchedules();
                break;
            default:
                Click_LoadAllSchedules();
                break;
        }
    }

    protected void Click_LoadAllSchedules()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            var _searchQuery = from t in entity.Schedulers.Include("AssignedToUser").Include("Xlk_Priority").Include("Xlk_Status")
                               select t;

            LoadResults(_searchQuery.ToList());
        }
        

    }

    protected void Click_SearchSchedules()
    {
        Int32? _userId = Convert.ToInt32(listusers.SelectedItem.Value);
        string _text = lookupString.Text;


        using (SupportEntities entity = new SupportEntities())
        {
            var _searchQuery = from t in entity.Schedulers.Include("AssignedToUser").Include("Xlk_Priority").Include("Xlk_Status")
                               select t;

            if (_userId > 0)
                _searchQuery = _searchQuery.Where(x => x.AssignedToUser.UserId == _userId);

            if (!string.IsNullOrEmpty(_text))
                _searchQuery = _searchQuery.Where(x => x.Ref.Contains(_text) | x.ScheduleTitle.Contains(_text) | x.ScheduleDescription.Contains(_text)  | x.TaskDescription.Contains(_text) | x.TaskTitle.Contains(_text));


                LoadResults(_searchQuery.ToList());
        }
    }

    protected void lookupSchedules(object sender, EventArgs e)
    {
        Click_SearchSchedules();
    }
    protected void lookuptitle(object sender, EventArgs e)
    {
        Click_SearchSchedules();
    }

    private void LoadResults(List<Scheduler> schedulers)
    {
        //Set the datasource of the repeater
        results.DataSource = schedulers;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", schedulers.Count().ToString());
    }

}
