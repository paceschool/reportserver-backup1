﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Data;

public partial class Support_ManageBusTicketsPage : BaseSupportPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bustickettype.DataSource = LoadBusTicketTypes();
            bustickettype.DataBind();
            
        }
    }

    protected void DisplayBusTicketLevels()
    {
        var one = 0; var two = 0; var three = 0;var four = 0;
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            //var lookupQuery = from l in entity.BusTicketBatches.Include("Xlk_BusTicketType").Include("AssignedTickets")
            //                  where l.ReceivedDate == false
            //                  select l;

            //foreach (BusTicket level in lookupQuery)
            //{
            //    if (level.Xlk_BusTicketType.BusTicketTypeId == 1)
            //        one = one + 1;
            //    if (level.Xlk_BusTicketType.BusTicketTypeId == 2)
            //        two = two + 1;
            //    if (level.Xlk_BusTicketType.BusTicketTypeId == 3)
            //        three = three + 1;
            //    if (level.Xlk_BusTicketType.BusTicketTypeId == 4)
            //        four = four + 1;
            //}
            oneweekamount.Text = Convert.ToString(one);
            twoweekamount.Text = Convert.ToString(two);
            threeweekamount.Text = Convert.ToString(three);
            fourweekamount.Text = Convert.ToString(four);

            if (four < 10)
                reorderfour.Text = "Re-order";
            else if (four < 15)
                reorderfour.Text = "Getting low";
            else
                reorderfour.Text = "";
            if (three < 5)
                reorderthree.Text = "Re-order";
            else if (three < 8)
                reorderthree.Text = "Getting low";
            else
                reorderthree.Text = "";
            if (two < 7)
                reordertwo.Text = "Re-order";
            else if (two < 10)
                reordertwo.Text = "Getting low";
            else
                reordertwo.Text = "";
            if (one < 10)
                reorderone.Text = "Re-order";
            else if (one < 15)
                reorderone.Text = "Getting low";
            else
                reorderone.Text = "";
        }
    }

    protected void Click_Save(object sender, EventArgs e)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            if (!string.IsNullOrEmpty(numbertoadd.Text) || !string.IsNullOrEmpty(serialstart.Text) || !string.IsNullOrEmpty(serialend.Text))
            {
                var start = Convert.ToInt32(serialstart.Text); var end = Convert.ToInt32(serialend.Text);

                for (var i = 0; i < Convert.ToInt32(numbertoadd.Text); i++)
                {
                    //BusTicket newtickets = new BusTicket();

                    //newtickets.SerialNumber = Convert.ToString(start);
                    //newtickets.Xlk_BusTicketTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_BusTicketType", "BusTicketTypeId", Convert.ToInt16(bustickettype.SelectedItem.Value));

                    //if (newtickets.BusTicketId == 0)
                    //{
                    //    entity.AddToBusTickets(newtickets);
                    //    entity.SaveChanges();
                    //}
                    //start++;
                }
            }
        }
        DisplayBusTicketLevels();
    }
}