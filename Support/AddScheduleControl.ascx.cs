﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Support;
using System.Data;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;

public partial class Support_AddScheduleControl : BaseSupportControl
{
    protected Int32 _schedulerId, _parentScheduleId;
    protected Scheduler _scheduler;
    protected bool _isedit = false;
    protected bool _doclose = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("ScheduleId").HasValue)
        {
            _isedit = true;
            Int32 _schedulerId = GetValue<Int32>("ScheduleId");

            using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
            {
                _scheduler = (from task in entity.Schedulers.Include("Xlk_Priority").Include("Xlk_Status").Include("AssignedToUser")
                         where task.ScheduleId == _schedulerId
                         select task).FirstOrDefault();
            }

        }
        if (GetValue<bool?>("Close") != null)
            _doclose = true;
    }

    protected string ReadOnly
    {
      get
        {
          if (_doclose)
                return "readonly=\"readonly\"";

            return string.Empty;
        }
        
    }

    protected short? GetStatus
    {
        get
        {
            if (_doclose)
                return 3;

            if (_isedit)
                return (_scheduler != null && _scheduler.Xlk_Status != null) ? _scheduler.Xlk_Status.StatusId : (short?)null;

            return 2;
        }
    }

    protected string GetScheduleId
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.ScheduleId.ToString();

            if (GetValue<Int32?>("ScheduleId") .HasValue)
                return GetValue<Int32>("ScheduleId").ToString();

            return "0";
        }
    }

    protected string GetPriorityList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_scheduler != null && _scheduler.Xlk_Priority != null) ? _scheduler.Xlk_Priority.PriorityId : (short?)null;

        foreach (Xlk_Priority item in LoadPriority())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.PriorityId, item.Description, (_current.HasValue && item.PriorityId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = GetStatus;// (_scheduler != null && _scheduler.Xlk_Status != null) ? _scheduler.Xlk_Status.StatusId : (short?)null;


        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetAssignedToUserList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_scheduler != null && _scheduler.AssignedToUser != null) ? _scheduler.AssignedToUser.UserId : (int?)null;

        foreach (Users item in LoadUsers())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }


    protected string GetRef
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.Ref;

            return null;
        }
    }

    protected string GetCycleDays
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.CycleDays.ToString();

            return null;
        }
    }

    protected string GetLastDueDate
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.LastDueDate.ToString("dd/MM/yyyy"); ;

            return null;
        }
    }

    protected string GetActionNeeded
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.ActionNeeded;

            return null;
        }
    }


    protected string GetScheduleDescription
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.ScheduleDescription;

            return null;
        }
    }

    protected string GetScheduleTitle
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.ScheduleTitle;

            return null;
        }
    }

    protected string GetTaskTitle
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.TaskTitle;

            return null;
        }
    }

    protected string GetTaskDescription
    {
        get
        {
            if (_scheduler != null)
                return _scheduler.TaskDescription;

            return null;
        }
    }

}