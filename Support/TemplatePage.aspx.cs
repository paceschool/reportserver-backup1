﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Support;
using System.Data;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity.Core;

public partial class TemplatePage :BaseSupportPage
{
    protected Int32 userId, taskId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            templates.DataSource = LoadTemplates();
            templates.DataBind();


            Click_LoadAllTemplates();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected string Click_LoadAllTemplates()
    {
        StringBuilder sb = new StringBuilder();


        using (SupportEntities entity = new SupportEntities())
        {
            List<Template> _templatesSearchQuery = (from t in entity.Templates.Include("TemplateTasks").Include("TemplateTasks.Xlk_UseDate")
                                                    orderby t.TemplateId ascending
                                                    select t).ToList();

            if (templates.SelectedItem.Value != "-1")
            {
                short id = Convert.ToInt16(templates.SelectedItem.Value);
                _templatesSearchQuery = _templatesSearchQuery.Where(x => x.TemplateId == id).ToList();
            }

            foreach (Template node in _templatesSearchQuery)
            {

                sb.AppendFormat("<li>{0}&nbsp;<a onclick=\"loadEditArrayControl('#dialog-form','TemplatePage.aspx','Support/AddTemplateTaskControl.ascx', {{'TemplateId':{1}}})\" href=\"#\" title=\"Add Child Template Task\"><img src=\"../Content/img/actions/next.png\"></a>", node.Title, node.TemplateId);

                if (node.TemplateTasks.Count > 0)
                {
                    sb.Append(LoadResults(null, node.TemplateTasks.ToList()));

                }
                else

                    sb.Append("</li>");
            }

        }
        return sb.ToString();
    }


    private string LoadResults(int? parentId, List<TemplateTask> tasks)
    {
        StringBuilder sb = new StringBuilder();

        var nodes = (from t in tasks
                              where (t.ParentTask == null && !parentId.HasValue) || (t.ParentTask != null && t.ParentTask.TemplateTaskId == parentId)
                              select t).ToList();
        if (nodes.Count > 0)
        {
            sb.Append("<ul>");

            foreach (var node in nodes)
            {
                sb.AppendFormat("<li><b id=\"{5}\" class=\"droppable\">{0}</b>;&nbsp;<i>Days from {3}: {4}</i>&nbsp;<a onclick=\"loadEditArrayControl('#dialog-form','TemplatePage.aspx','Support/AddTemplateTaskControl.ascx', {{ TemplateId: {2}, ParentTemplateTaskId: {1}}})\" href=\"#\" title=\"Add Child Template Task\"><img src=\"../Content/img/actions/next.png\"></a>&nbsp;<a onclick=\"loadEditArrayControl('#dialog-form','TemplatePage.aspx','Support/AddTemplateTaskControl.ascx',{{'TemplateTaskId':{1}}})\" href=\"#\" title=\"Edit Template Task\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a onclick=\"deleteObjectFromAjaxTab('TemplatePage.aspx','TemplateTask',{1})\" href=\"#\" title=\"Remove Task from the Template\"><img src=\"../Content/img/actions/bin_closed.png\"></a>", node.Title, node.TemplateTaskId, node.Template.TemplateId, node.Xlk_UseDate.Description, node.Days, node.TemplateTaskId);
                sb.Append(LoadResults(node.TemplateTaskId, tasks));
                sb.Append("</li>");
            }

            sb.Append("</ul>");
        }
        return sb.ToString();
    }

    protected void Click_LookupTemplate(object sender, EventArgs e)
    {

    }
    protected void listTemplateTasks_SelectedNodeChanged(object sender, EventArgs e)
    {

    }

    protected void templates_SelectedIndexChanged(object sender, EventArgs e)
    {
        Click_LoadAllTemplates();
    }

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse AssignTaskToParent(Int16 parenttasktemplateid, Int16 tasktemplateid) 
    {
        try
        {
            using (SupportEntities entity = new SupportEntities())
            {
                TemplateTask task = (from e in entity.TemplateTasks
                                         where e.TemplateTaskId == tasktemplateid
                                         select e).First();

                task.ParentTaskReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".TemplateTasks", "TemplateTaskId", parenttasktemplateid);

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();
                else
                    return new AjaxResponse(new Exception("Could not save this move"));
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }
    
    #endregion

}
