﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PricesPage.aspx.cs" Inherits="Agent_PricePage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/jqwidgets/css/jqx.base.css")%>"
        type="text/css" />
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/jqwidgets/css/jqx.ui-redmond.css")%>"
        type="text/css" />
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxcore.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxbuttons.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxscrollbar.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxpanel.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxTree.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxsplitter.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxlistbox.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxexpander.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxmenu.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.selection.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.columnsresize.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.pager.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdropdownlist.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdata.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxgrid.edit.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdropdownlist.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxcheckbox.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxcalendar.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxnumberinput.js")%>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxdatetimeinput.js")%>"></script>
   <script type="text/javascript" src="<%= Page.ResolveUrl("~/Content/jqwidgets/js/jqxmenu.js")%>"></script>
    <script type="text/javascript">
        $(function () {
        

            function isRightClick(event) {
                var rightclick;
                if (!event) var event = window.event;
                if (event.which) rightclick = (event.which == 3);
                else if (event.button) rightclick = (event.button == 2);
                return rightclick;
            }

            var rss = (function ($) {
                var createWidgets = function () {
                    $('#jqxMenu').jqxMenu({ width: '120px', theme: config.theme, height: '106px', autoOpenPopup: false, mode: 'popup' });
                    $('#mainSplitter').jqxSplitter({  width: '100%', height: 600, height: 600, panels: [{ size: 300, min: 100 }, { min: 200, size: 400}] });
                    $('#contentSplitter').jqxSplitter({ width: '100%', height: '100%', orientation: 'horizontal', panels: [{ size: 200, min: 100, collapsible: false }, { min: 100, collapsible: true}] });
                    $("#feedExpander").jqxExpander({ toggleMode: 'none', showArrow: false, width: "100%", height: "100%",
                        initContent: function () {
                            $('#productTree').jqxTree({ theme: config.theme, height: '100%', width: '100%' });
                        }
                    });                    
                    $("#feedListExpander").jqxExpander({ toggleMode: 'none', showArrow: false, width: "100%", height: "100%", initContent: function () {
                        $("#rateCardContainer").jqxGrid({ theme: config.theme, selectionmode: 'singlerow', editable: false, width: '100%', height: '100%', keyboardnavigation: false, columns: config.ratecardcolumns,showstatusbar: true, renderstatusbar: function (statusbar) {

                                // appends buttons to the status bar.
                                var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");

                                var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Add</span></div>");
                                var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/delete.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Delete</span></div>");
                                var reloadButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Reload</span></div>");
                                
                                addButton.jqxButton({ theme: config.theme, width: 60, height: 20 });
                                deleteButton.jqxButton({ theme: config.theme, width: 65, height: 20 });
                                reloadButton.jqxButton({ theme: config.theme, width: 65, height: 20 });

                                container.append(addButton);
                                container.append(deleteButton);
                                container.append(reloadButton);

                                statusbar.append(container);

                                // add new row.
                                addButton.click(function (event) {
                                  if (config.selecteditemid > 0){
                                        config.selectedratecardid = 0;
                                        showRateCardPopUp();
                                        }
                                });

                                // delete selected row.
                                deleteButton.click(function (event) {
                                    var selectedrowindex = $("#rateCardContainer").jqxGrid('getselectedrowindex');
                                    var rowscount = $("#rateCardContainer").jqxGrid('getdatainformation').rowscount;
                                    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                        var id = $("#rateCardContainer").jqxGrid('getrowid', selectedrowindex);
                                        $("#rateCardContainer").jqxGrid('deleterow', id);
                                    }
                                });

                                // reload grid data.
                                reloadButton.click(function (event) {
                                    loadRateCardFeed();
                                });
                            }});
                        }
                    });


                    $("#feedContentExpander").jqxExpander({ toggleMode: 'none', showArrow: false, width: "100%", height: "100%", initContent: function () {
                        $('#rateCardCostingContainer').jqxGrid({ theme: config.theme, selectionmode: 'singlerow',editable: false, width: '100%', height: '100%', columns: config.ratecardcostingcolumns,showstatusbar: true, renderstatusbar: function (statusbar) {


                                // appends buttons to the status bar.
                                var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");

                                var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Add</span></div>");
                                var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/delete.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Delete</span></div>");
                                var reloadButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='../Content/img/actions/refresh.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Reload</span></div>");
                                
                                addButton.jqxButton({ theme: config.theme, width: 60, height: 20 });
                                deleteButton.jqxButton({ theme: config.theme, width: 65, height: 20 });
                                reloadButton.jqxButton({ theme: config.theme, width: 65, height: 20 });

                                container.append(addButton);
                                container.append(deleteButton);
                                container.append(reloadButton);

                                statusbar.append(container);

                                // add new row.
                                addButton.click(function (event) {
                                    if (config.selectedratecardid > 0){
                                        config.selectedratecardcostingid = 0;
                                        showRateCardCostingPopUp();
                                        }
                                       
                                });

                                // delete selected row.
                                deleteButton.click(function (event) {
                                    var selectedrowindex = $("#rateCardCostingContainer").jqxGrid('getselectedrowindex');
                                    var rowscount = $("#rateCardCostingContainer").jqxGrid('getdatainformation').rowscount;
                                    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                        var id = $("#rateCardCostingContainer").jqxGrid('getrowid', selectedrowindex);
                                        $("#rateCardCostingContainer").jqxGrid('deleterow', id);
                                    }
                                });
                                // reload grid data.
                                reloadButton.click(function (event) {
                                    loadRateCardFeed();
                                });
                            }});
                        }
                    });


                    };
            
   

             var addEventListeners = function () {
                  
                    // disable the default browser's context menu.
                    $('#productTree').on('contextmenu', function (e) {
                            return false;
                    });

                  $("#productTree").on('mousedown', function (event) {
                            var target = $(event.target).parents('li:first')[0];
                            var children = $(target).find('li').length;
                            var rightClick = isRightClick(event);
                            if (rightClick && target != null && children == 0) {
                                config.feedTree.jqxTree('selectItem', target);
                                var scrollTop = $(window).scrollTop();
                                var scrollLeft = $(window).scrollLeft();
                                config.contextMenu.jqxMenu('open', parseInt(event.clientX) + 5 + scrollLeft, parseInt(event.clientY) + 5 + scrollTop);
                                return false;
                            }
                        });

                  $("#productTree").on('remove', function (event) {
                            var item = $.trim($(event.args).text());
                             var selectedItem = config.feedTree.jqxTree('selectedItem');
                             alert(item);
                             alert(selectedItem);
                        });

                    $("#jqxMenu").on('itemclick', function (event) {
                        var item = $.trim($(event.args).text());
                        switch (item) {
                            case "Edit Item":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    loadEditArrayControlWithCallback('#dialog-form','ViewAgentPage.aspx','Agent/AddOrderableItemControl.ascx',{'ItemId':selectedItem.value},stateChanged);
                                }
                                break;
                            case "Remove Item":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    deleteObjectFromAjaxTabWithCallback('ViewAgentPage.aspx','OrderableItem',selectedItem.value,stateChanged)
                                }
                                break;
                            case "Clone Prices":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    loadEditArrayControlWithCallback('#dialog-form','ViewAgentPage.aspx','Agent/CloneRateCardsControl.ascx',{'ItemId':selectedItem.value,'AgencyId':$('#<%= listagents.ClientID %> option:selected').val()},stateChanged);
                                }
                                break;
                            case "Copy Prices":
                                var selectedItem = config.feedTree.jqxTree('selectedItem');
                                if (selectedItem != null) {
                                    loadEditArrayControlWithCallback('#dialog-form','ViewAgentPage.aspx','Agent/CopyRateCardsControl.ascx',{'ItemId':selectedItem.value,'AgencyId':$('#<%= listagents.ClientID %> option:selected').val()},stateChanged);
                                }
                                break;
                        }
                    });

                    $('#productTree').on('select', function (event) {
                        var item = config.feedTree.jqxTree('getItem', event.args.element)
                        if (item.level == 2 && item.hasItems == false){
                            config.selecteditemid  = item.value;
                            config.selectedratecardid = 0;
                            config.selectedunitid = 0;
                            config.selectedagencyid = 0;
                            loadRateCardFeed();
                            }
                    });

                    $("#rateCardContainer").on('rowdoubleclick', function (event) {
                        var data = $("#rateCardContainer").jqxGrid('getrowdata',event.args.rowindex)
                        if (data != null) {
                            config.selectedratecardid = data.RateCardId;
                            showRateCardPopUp();
                        }
                    });
                    
                    $("#rateCardCostingContainer").on('rowdoubleclick', function (event) {
                        var data = $("#rateCardCostingContainer").jqxGrid('getrowdata',event.args.rowindex)
                        if (data != null) {
                            config.selectedratecardcostingid = data.RateCardCostingId,
                            showRateCardCostingPopUp();
                        }
                    });

                    showRateCardPopUp = function()
                    {
                        loadEditArrayControlWithCallback('#dialog-form','ViewAgentPage.aspx','Agent/AddRateCardControl.ascx',{ RateCardId:config.selectedratecardid ,AgencyId: config.selectedagencyid,ItemId: config.selecteditemid},loadRateCardFeed)
                    }
                    
                    showRateCardCostingPopUp = function()
                    {
                        loadEditArrayControlWithCallback('#dialog-form','ViewAgentPage.aspx','Agent/AddRateCardCostingControl.ascx',{ RateCardCostingId:config.selectedratecardcostingid,RateCardId:config.selectedratecardid,UnitId:config.selectedunitid},loadRateCardCostingFeed)
                    }

                    $('#rateCardContainer').on('rowselect', function (event) {
                        config.selectedratecardid = event.args.row.RateCardId;
                        config.selectedagencyid = event.args.row.AgencyId;
                        loadRateCardCostingFeed();
                    });

                    $(".watcher").change(function (e) {
                        stateChanged()
                    });

                    };


                var loadRateCardFeed = function () {
                    var agencyid =  $('#<%= listagents.ClientID %> option:selected').val();
                    $.ajax({
                        'dataType': 'json',
                        'type': "POST",
                        'url':  config.dataDir + '/GetProductRateCards',
                        'data': '{AgencyId:' +  config.selectedagencyid + ',ItemId:' +  config.selecteditemid + ',Year:' + $('#<%= listyears.ClientID %> option:selected').val() + '}',
                        'contentType': 'application/json; charset=utf-8',
                        success: function (data) {
                            config.currentRateCardContent = data.d;
                            if (data.d.RateCards.length > 0)
                            {
                                $("#feedListExpander").jqxExpander('setHeaderContent', 'Rate Cards for: ' +config.currentRateCardContent.Title.Title);
                                displayRateCards(config.currentRateCardContent.RateCards);
                            }
                            else
                            {
                                config.rateCardContainer.jqxGrid('clear');
                                config.rateCardCostingContainer.jqxGrid('clear');
                            }
                        }
                    });
                };
                                
                var loadRateCardCostingFeed = function () {
                    $.ajax({
                        dataType: 'json',
                        type: "POST",
                        url: config.dataDir + '/GetRateCardCosting',
                        data: '{ratecardid:' + config.selectedratecardid + '}',
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            config.ratecardcostingsource.localdata = data.d;
                            $("#feedContentExpander").jqxExpander('setHeaderContent', 'Costings for: RateCardId: ' +config.selectedratecardid);
                            var adapter = new $.jqx.dataAdapter(config.ratecardcostingsource);
                            // update data source.
                            $("#rateCardCostingContainer").jqxGrid({ source: adapter });
                        }
                    });
                };

                stateChanged = function(){
                        getTreeViewData(  
                            $('#<%= listagents.ClientID %> option:selected').val(),
                            $('#<%= listproducttypes.ClientID %> option:selected').val(),
                            $('#<%= listproducts.ClientID %> option:selected').val(),
                            $('#<%= listyears.ClientID %> option:selected').val(),
                            $('#<%= lookupString.ClientID %>').val()
                            )
                }

                getTreeViewData = function(agentid,producttypeid,itemid,year,lookupstring)
                {
                    $.ajax({
                        dataType: 'json',
                        type: "POST",
                        url: config.dataDir + '/BuildTreeJson',
                        data: JSON.stringify({agentid: agentid,producttypeid:producttypeid,itemid:itemid,year:year,lookupstring:lookupstring  }),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) { 
                                config.feedTree.jqxTree('clear');
                                config.treeviewsource.localdata = data.d;
                                // create data adapter.
                                var dataAdapter = new $.jqx.dataAdapter(config.treeviewsource);
                                // perform Data Binding.
                                dataAdapter.dataBind();
                                 var records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label'},{ name: 'value', map: 'value'}]);
                                config.feedTree.jqxTree({ source: records });
                                if (data.d.length < 10)
                                    config.feedTree.jqxTree('selectItem',config.feedTree.find('li:last')[0]);
                        } 
                    });
                }

                
                var displayRateCards = function (items) {                      
                
                    if (items.length == 0)
                         config.rateCardCostingContainer.jqxGrid('clear');

                      config.ratecardsource.localdata = items;
                      var adapter = new $.jqx.dataAdapter( config.ratecardsource);
                      // update data source.
                      config.rateCardContainer.jqxGrid({ source: adapter });
                      config.rateCardContainer.jqxGrid('selectrow', 0);

                };

                var unitsSource= 
                        {
                            datatype: "array",
                            datafields: [
                                 { name: 'UnitId', type: 'number' },
                                 { name: 'Description', type: 'string' }
                             ],
                            localdata: <%= GetUnits() %>
                        };
                var unitsAdapter = new $.jqx.dataAdapter(unitsSource, {
                    autoBind: true
                    });

                var dataDir = 'PricesPage.aspx';
                var config = {
                    theme: 'ui-redmond',
                    data: '',
                    feed: '',
                    dataDir: dataDir,
                    selectedratecardid : 0,
                    selecteditemid : 0,
                    selectedunitid: 0,
                    selectedagencyid : 0,
                    selectedratecardcostingid :0,
                    contextMenu:$('#jqxMenu'),
                    feedTree: $('#productTree'),
                    rateCardCostingContainer: $('#rateCardCostingContainer'),
                    rateCardContainer: $('#rateCardContainer'),
                    feedItemContent: $('#feedItemContent'),
                    feedItemHeader: $('#feedItemHeader'),
                    feedUpperPanel: $('#feedUpperPanel'),
                    productHeader: $('#productHeader'),
                    feedContentArea: $('#feedContentArea'),
                    selectedIndex: -1,
                    currentFeed: '',
                    currentRateCardContent: {},
                    ratecardcostingsource:
                            {
                                datafields:
                                [
                                    { name: 'RateCardCostingId' },
                                    { name: 'RateCardId' },
                                    { name: 'UnitId', type: 'number' },
                                    { name: 'UnitDescription', type: 'string' },
                                    { name: 'QtyFrom', type: 'number' },
                                    { name: 'QtyTo', type: 'number' },
                                    { name: 'CalculationTypeDescription', type: 'string' },
                                    { name: 'Cost', type: 'number' },
                                    { name: 'Discount', type: 'number' },
                                    { name: 'AllowProRate', type: 'bool' },
                                    { name: 'BaseCost', type: 'number' },
                                    { name: 'SageRef', type: 'string' },
                                ],
                                id: 'RateCardCostingId',
                                deleterow: function (rowid, commit) {
                                    $.ajax({
                                        'dataType': 'json',
                                        'type': "POST",
                                        'url': config.dataDir + '/DeleteRateCardCosting',
                                        'data': '{ratecardcostingid:' + rowid  + '}',
                                        'contentType': 'application/json; charset=utf-8',
                                        success: function () {
                                            commit(true);
                                        }
                                    });
                                }
                            },
                    ratecardsource:
                            {
                                datafields:
                                [
                                    { name: 'RateCardId', type: 'number' },
                                    { name: 'AgencyId', type: 'number' },
                                    { name: 'Agency', type: 'string' },
                                    { name: 'ProductSageRef', type: 'string' },
                                    { name: 'DateActiveFrom', type: 'date' },
                                    { name: 'DateActiveTo', type: 'date' },
                                    { name: 'DefaultDiscount', type: 'number' },
                                    { name: 'DefaultUnitId', type: 'number' },
                                    { name: 'DefaultUnit', type: 'string' }
                                ],
                                id: 'RateCardId',
                                deleterow: function (rowid, commit) {
                                    $.ajax({
                                        'dataType': 'json',
                                        'type': "POST",
                                        'url': config.dataDir + '/DeleteRateCard',
                                        'data': '{ratecardid:' + rowid +'}',
                                        'contentType': 'application/json; charset=utf-8',
                                        success: function () {
                                            commit(true);
                                        }
                                    });
 
                                }
                            },
                            treeviewsource:
                            {
                                datatype: "json",
                                datafields: [
                                    { name: 'id' },
                                    { name: 'parentid' },
                                    { name: 'text' },
                                    { name: 'value' },
                                    { name: 'expanded' }
                                ],
                                id: 'id',
                                localdata: ''
                            },
                            ratecardcolumns: [
                                            { text: 'Id', dataField: 'RateCardId', width: 100, hidden: true },
                                            { text: 'AgencyId', dataField: 'AgencyId', width: 100, hidden: true },
                                            { text: 'Agency', dataField: 'Agency', width: 100 },
                                            { text: 'Sage Ref', dataField: 'ProductSageRef', width: 200 },
                                            { text: 'From', dataField: 'DateActiveFrom',width: 150, columntype: 'datetimeinput', align: 'right', cellsalign: 'right', cellsformat: 'dd MMM yyyy' },
                                            { text: 'To', dataField: 'DateActiveTo', width: 150, columntype: 'datetimeinput', align: 'right', cellsalign: 'right', cellsformat: 'dd MMM yyyy' },
                                            { text: 'Default Discount', dataField: 'DefaultDiscount', width: 100, align: 'right', cellsalign: 'right', cellsformat: 'p', columntype: 'numberinput' },
                                            { text: 'Default Unit', datafield: 'DefaultUnitId',displayfield: 'DefaultUnit', columntype: 'dropdownlist',
                                                createeditor: function (row, value, editor) {
                                                    editor.jqxDropDownList({ source: unitsAdapter, displayMember: 'Description', valueMember: 'UnitId' });
                                                }
                                            }
                                    ],
                            ratecardcostingcolumns: [
                                            { text: 'RateCardCostingId', datafield: 'RateCardCostingId', width: 100, hidden: true },
                                            { text: 'RateCardId', datafield: 'RateCardId', width: 100, hidden: true },
                                            { text: 'Unit', datafield: 'UnitId', editable:false, displayfield: 'UnitDescription', width: 100 },
                                            { text: 'CalculationType', datafield: 'CalculationTypeId', editable:false, displayfield: 'CalculationTypeDescription', width: 100 },
                                            { text: 'Cost', datafield: 'Cost', cellsformat: 'd', width: 150, align: 'right', cellsalign: 'right',cellsformat: 'c2', columntype: 'numberinput' },
                                            { text: 'Qty From', datafield: 'QtyFrom', cellsformat: 'd', width: 75, align: 'right', cellsalign: 'right', columntype: 'numberinput' },
                                            { text: 'Qty To', datafield: 'QtyTo', cellsformat: 'd', width: 75, align: 'right', cellsalign: 'right', columntype: 'numberinput' },
                                            { text: 'SageRef', datafield: 'SageRef'},
                                            { text: 'Discount', datafield: 'Discount', align: 'right', cellsalign: 'right', cellsformat: 'p', columntype: 'numberinput' },
                                            { text: 'AllowProRate', datafield: 'AllowProRate', columntype: 'checkbox', width: 100 },
                                            { text: 'BaseCost', datafield: 'BaseCost', align: 'right', cellsalign: 'right', cellsformat: 'c2', columntype: 'numberinput' }
                                    ]
                };
                return {
                    init: function () {
                        createWidgets();
                        addEventListeners();
                        stateChanged();
                    }
                }
            } (jQuery));
            rss.init();



        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Prices Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td style="text-align: right">
                        Search by Agent
                    </td>
                    <td>
                        <asp:DropDownList CssClass="watcher" ID="listagents" DataTextField="Name" DataValueField="AgencyId"
                            runat="server" AppendDataBoundItems="true" AutoPostBack="false">
                            <asp:ListItem Text="All Agents" Value="0">
                            </asp:ListItem>
                            <asp:ListItem Text="Default" Value="-1">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList CssClass="watcher" ID="listproducttypes" DataTextField="Description" DataValueField="ProductTypeId"
                            runat="server" AppendDataBoundItems="true" AutoPostBack="false">
                            <asp:ListItem Text="All Products Types" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList CssClass="watcher" ID="listproducts" DataTextField="Title" DataValueField="ItemId"
                            runat="server" AppendDataBoundItems="true" AutoPostBack="false">
                            <asp:ListItem Text="All Products" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList CssClass="watcher" ID="listyears" DataTextField="Key" DataValueField="Value" runat="server"
                            AppendDataBoundItems="true" AutoPostBack="false">
                            <asp:ListItem Text="Every Year" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="text-align: right">
                        or by text
                    </td>
                    <td>
                        <asp:TextBox CssClass="watcher" ID="lookupString" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="false" Font-Size="X-Large">
                        </asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id='jqxWidget'>
            <div id="mainSplitter">
                <div>
                    <div style="border: none;" id="feedExpander">
                        <div class="jqx-hideborder">
                            <a href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewAgentPage.aspx','Agent/AddOrderableItemControl.ascx',{},stateChanged)"><img src="../Content/img/actions/add.png">Add New Product</a></div>
                        <div class="jqx-hideborder jqx-hidescrollbars">
                            <div class="jqx-hideborder" id='productTree'>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="contentSplitter">
                        <div class="feed-item-list-container" id="feedUpperPanel">
                            <div class="jqx-hideborder" id="feedListExpander">
                                <div class="jqx-hideborder" id="productHeader">
                                </div>
                                <div class="jqx-hideborder jqx-hidescrollbars">
                                    <div class="jqx-hideborder" id="rateCardContainer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="feedContentArea">
                            <div class="jqx-hideborder" id="feedContentExpander">
                                <div class="jqx-hideborder" id="feedItemHeader">
                                </div>
                                <div class="jqx-hideborder jqx-hidescrollbars">
                                    <div class="jqx-hideborder" id="rateCardCostingContainer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div id='jqxMenu'>
            <ul>
                <li>Edit Item</li>
                <li>Remove Item</li>
                <li>Clone Prices</li>
                <li>Copy Prices</li>
            </ul>
        </div>
    </div>
</asp:Content>
