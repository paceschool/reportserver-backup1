﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewQuotePage.aspx.cs" Inherits="Agent_ViewQuotePage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {

            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });


        });
    </script>
    <style>
        tr.rowhighlight
        {
            background-color: #f0f8ff;
            margin: 0;
            border: 0;
            padding: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="View Quote Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        <label>
                            Quote Id
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="QuoteId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Agency
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Agency" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Description
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Description" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Created By
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="CreatedBy" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Date Created
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="DateCreated" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Status
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="quotestatus" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Contact Name
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="ContactName" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Address
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Address" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Comment
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Comment" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        SAGE Reference
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="SAGERef" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Dates
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Dates" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        Actions
                    </th>
                    <td style="text-align: left">
                        <a title="Edit Group" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewQuotePage.aspx','Agent/AddAgencyQuoteControl.ascx',{'QuoteId':<%=_quoteId %>},refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a> <a target="_blank" href="<%= ReportPath("AgentReports","Agent_Quote","PDF", new Dictionary<string, object> { { "QuoteId", _quoteId}}) %>"
                                title="Quote">
                                <img alt="Print Quote" title="Print Quote" src="../Content/img/actions/printer2.png" /></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="ViewQuotePage.aspx/LoadQuoteItems?id=<%= _quoteId %>">Items</a></li>
                <li><a href="ViewQuotePage.aspx/LoadQuoteNotes?id=<%= _quoteId %>">Notes</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
