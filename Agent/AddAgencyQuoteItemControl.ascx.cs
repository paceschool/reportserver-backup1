﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Agent;
using System.Text;
using System.Web.Services;

public partial class Agent_AddAgencyQuoteItem : BaseAgencyControl
{
    protected Int32 _agentId, _quoteId;
    protected QuoteItem _item;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (AgentEntities entity = new AgentEntities())
        {
            if (GetValue<Int32?>("QuoteId").HasValue && GetValue<Int32?>("LineId").HasValue)
            {
                Int32 quoteId = GetValue<Int32>("QuoteId");
                Int32 lineId = GetValue<Int32>("LineId");

                _item = (from qi in entity.QuoteItems.Include("Xlk_Unit").Include("OrderableItem").Include("Quote.Agency").Include("RateCard")
                         where qi.QuoteId == quoteId && qi.LineId == lineId
                         select qi).FirstOrDefault();
            }

            else if (GetValue<Int32?>("AgencyId").HasValue)
            {
                Int32 agentId = GetValue<Int32>("AgencyId");
            }
        }
    }


    protected string GetQuoteId
    {
        get
        {
            if (_item != null)
                return _item.QuoteId.ToString();

            if (Parameters.ContainsKey("QuoteId"))
                return Parameters["QuoteId"].ToString();

            if (_quoteId != null)
                return _quoteId.ToString();

            return "0";
        }
    }


    protected string GetRateCardId
    {
        get
        {
            if (_item != null && _item.RateCard != null)
                return _item.RateCard.RateCardId.ToString();

            if (Parameters.ContainsKey("RateCardId"))
                return Parameters["RateCardId"].ToString();

            return "0";
        }
    }

    protected string GetLineId
    {
        get
        {
            if (_item != null)
                return _item.LineId.ToString();

            if (Parameters.ContainsKey("LineId"))
                return Parameters["LineId"].ToString();

            return "0";
        }
    }

    protected string GetItemList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"-1\">Please Select</option>");

        Int16? _current = (_item != null && _item.OrderableItem != null) ? _item.OrderableItem.ItemId : (Int16?)null;

        using (AgentEntities entity = new AgentEntities())
        {
            var items =
                from o in entity.Xlk_ProductType
                orderby o.Description
                select new { ProductType=o.Description, Items=o.OrderableItems };


            foreach (var type in items)
            {

                sb.AppendFormat("<optgroup label=\"{0}\">", type.ProductType);

                    foreach (var item in type.Items)
                    {
                        sb.AppendFormat("<option {3} value={0}>{2} ({1})</option>", item.ItemId, item.SageRef, item.Title, (_current.HasValue && item.ItemId == _current) ? "selected" : string.Empty);
                    }

                sb.Append("</optgroup>");

            }
        }
        return sb.ToString();
    }

    protected string GetUnitList()
    {
        StringBuilder sb = new StringBuilder();

        if (_item != null && _item.RateCard != null)
        {
            byte? _current = (_item != null && _item.Xlk_Unit != null) ? _item.Xlk_Unit.UnitId : (byte?)null;
            IList<Xlk_Unit> units = LoadUnits();
            var _rateCardId = _item.RateCard.RateCardId;

            using (AgentEntities entity = new AgentEntities())
            {

                units = (from u in entity.Xlk_Unit
                         where u.RateCardCostings.Any(y => y.RateCardId == _rateCardId)
                         select u).ToList();
            }


            foreach (Xlk_Unit unit in units)
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", unit.UnitId, unit.Description, (_current.HasValue && unit.UnitId == _current) ? "selected" : string.Empty);
            }
        }
        return sb.ToString();
    }

    protected string GetDescription
    {
        get
        {
            if (_item != null)
                return _item.Description;

            return String.Empty;
        }
    }

    protected string GetRRPrice
    {
        get
        {
            if (_item != null)
                return _item.RRPrice.ToString();

            return "0.00";
        }
    }

    protected string GetGrossPrice
    {
        get
        {
            if (_item != null && _item.LineGrossPrice.HasValue)
                return _item.LineGrossPrice.Value.ToString();

            return "0.00";
        }
    }


    protected string GetNetPrice
    {
        get
        {
            if (_item != null && _item.LineNetPrice.HasValue)
                return _item.LineNetPrice.Value.ToString();

            return "0.00";
        }
    }

    protected string GetDiscount
    {
        get
        {
            if (_item != null)
                return _item.Discount.ToString();

            return "0.00";
        }
    }

    protected string GetQuantity
    {
        get
        {
            if (_item != null)
                return _item.Qty.ToString();

            return "1";
        }
    }
}