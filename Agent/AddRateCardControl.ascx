﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddRateCardControl.ascx.cs" 
    Inherits="Agent_AddRateCardControl" %>

<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addagencyratecard");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","SaveAgencyRateCard") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $(".date").datepicker({ dateFormat: 'dd/mm/yy' });

    });
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addagencyratecard" name="addagencyratecard" method="post" action="ViewAgencyPage.aspx/SaveAgencyRateCard">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="RateCardId" value="<%= GetRateCardId %>" />
            <label>
                Agency
            </label>
            <select name="Agency" id="AgencyId" >
                <%= GetAgencyItems() %>
            </select>
            <label>
                Item
            </label>
            <select type="text" <%= (_itemId.HasValue) ? "disabled" : String.Empty %> name="OrderableItem" id="ItemId" >
                <%= GetOrderableItems() %>
            </select>
            <label>
                Discount
            </label>
            <input type="text" id="DefaultDiscount" value="<%= GetDiscount %>" />
            <label>
                Date Active From
            </label>
            <input type="datetext" id="DateActiveFrom" class="date" value="<%= GetDateFrom %>" />
            <label>
                Date Active To
            </label>
            <input type="datetext" id="DateActiveTo" class="date" value="<%= GetDateTo %>" />
            <label>
                Default Unit
            </label>
            <select type="text" name="Xlk_Unit" id="UnitId">
                <%= GetUnits() %>
            </select>
            <label>
                <i><b>
                    This will create a new Rate Card for this Agent
                </b></i>
            </label>
        </fieldset>
    </div>
</form>