﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Agent;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;
using System.Data.Entity.Core;


public partial class Agent_PricePage : BaseAgentPage
{
    protected Int32? _agentId;
    protected Int32? _itemId;
    protected static List<FilterRateCard_Result> cards;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["AgencyId"]))
            _agentId = Convert.ToInt32(Request["AgencyId"]);

        if (!string.IsNullOrEmpty(Request["ItemId"]))
            _itemId = Convert.ToInt32(Request["ItemId"]);

        if (!Page.IsPostBack)
        {
            listagents.DataSource = this.LoadAgents();
            listagents.DataBind();

            if (_agentId.HasValue)
                listagents.Items.FindByValue(_agentId.Value.ToString()).Selected = true;

            listproducttypes.DataSource = this.LoadProductType().OrderBy(x=>x.Description);
            listproducttypes.DataBind();


            listproducts.DataSource = this.LoadOrderableItems();
            listproducts.DataBind();

            if (_itemId.HasValue)
                listproducts.Items.FindByValue(_itemId.Value.ToString()).Selected = true;


            listyears.DataSource = GetYears();
            listyears.DataBind();
        }
        LoadRateCards();
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected Dictionary<string, string> GetYears()
    {
        Dictionary<string, string> years = new Dictionary<string, string>();

        DateTime _date = DateTime.Now.AddYears(-1);

        while (_date < DateTime.Now.AddYears(2))
        {
            years.Add(_date.Year.ToString(), _date.Year.ToString());
            _date = _date.AddYears(1);
        }

        return years;
    }

    protected void LoadRateCards()
    {
        int? agencyId = Convert.ToInt32(listagents.SelectedItem.Value);
        short? producttypeId = Convert.ToInt16(listproducttypes.SelectedItem.Value);
        short? itemId = Convert.ToInt16(listproducts.SelectedItem.Value);
        int? year = Convert.ToInt32(listyears.SelectedItem.Value);

        using (AgentEntities entity = new AgentEntities())
        {
            cards = entity.FilterRateCard((agencyId > 0) ? agencyId : null, null, (producttypeId > 0) ? producttypeId : null, (itemId > 0) ? itemId : null, (year > 0) ? year : null).ToList();
        }
    }

    protected string GetUnits()
    {
        var records = this.LoadUnits().Select(q=>new {q.UnitId,q.Description}).ToList();
        return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(records);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> BuildTreeJson(int? agentid, byte? producttypeid, short? itemid, int? year, string lookupstring)
    {
        List<FilterOrderableItems_Result> cards;
        bool branchexpand = false;

        using (AgentEntities entity = new AgentEntities())
        {
            cards = entity.FilterOrderableItems((agentid > 0) ? agentid : null, null, (producttypeid > 0) ? producttypeid : null, (itemid > 0) ? itemid : null, (year > 0) ? year : null).ToList();
        }

        var categories = (from q in cards select new { q.CategoryId, q.Category }).Distinct();

        if (categories.Count() > 0)
        {
            branchexpand = true;

            foreach (var cats in categories)
            {
                yield return new { id = string.Format("CategoryId:{0}", cats.CategoryId), parentid = "-1", expanded = branchexpand.ToString(), text = cats.Category, value = string.Format("CategoryId:{0}", cats.CategoryId) };
                var types = (from q in cards where q.CategoryId == cats.CategoryId select new { q.ProductTypeId, q.ProductType }).Distinct();

                if (types.Count() > 0)
                {
                    branchexpand = true;

                    foreach (var type in types)
                    {
                        yield return new { id = string.Format("CategoryId:{0},ProductTypeId:{1}", cats.CategoryId, type.ProductTypeId), parentid = string.Format("CategoryId:{0}", cats.CategoryId), expanded = Boolean.FalseString, text = type.ProductType, value = string.Format("ProductTypeId:{0}", type.ProductTypeId) };

                        var products = (from q in cards where q.CategoryId == cats.CategoryId && q.ProductTypeId == type.ProductTypeId select new { q.ItemId, q.Title, q.Description, q.ProductSageRef }).Distinct();

                        if (products.Count() > 0)
                        {
                        foreach (var product in products.OrderBy(x=>x.Title))
                            {
                                yield return new { id = string.Format("CategoryId:{0},ProductTypeId:{1}, Itemid: {2}", cats.CategoryId, type.ProductTypeId, product.ItemId), parentid = string.Format("CategoryId:{0},ProductTypeId:{1}", cats.CategoryId, type.ProductTypeId), text = string.Format("{0} - Ref:{1}", product.Title, product.ProductSageRef), value = product.ItemId };

                            }
                        }


                    }

                }
            }
        }
    }




    protected IEnumerable<KeyValuePair<string, string>> GetAgents()
    {
        return (from q in cards
               select new {q.AgencyId,q.Name, q.AgencySageRef }).Distinct().Select( x=> new KeyValuePair<string, string>(x.AgencyId.ToString(), string.Format("{0}-{1}", x.Name, x.AgencySageRef)));
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static Dictionary<string, object> GetProductRateCards(short ItemId, int AgencyId, int? Year)
    {
        Dictionary<string, object> prices = new Dictionary<string, object>();
        try
        {

            using (AgentEntities entity = new AgentEntities())
            {
                var cards = entity.FilterRateCard((AgencyId > 0) ? AgencyId : default(Int32?), null, null, ItemId, (Year > 0) ? Year : default(Int32?)).ToList();
                prices.Add("Title", (from q in cards select new { q.Title,q.AgencyId,q.ItemId }).Distinct().FirstOrDefault());
                prices.Add("RateCards", (from q in cards where q.RateCardId.HasValue select new { q.ProductSageRef,Agency = q.Name, DateActiveFrom = q.DateActiveFrom,q.AgencyId, q.RateCardId, DateActiveTo = q.DateActiveTo.Value.ToString("dd MMM yyyy"), q.DefaultDiscount, q.DefaultUnitId, q.DefaultUnit }).Distinct().ToList());
            }

            return prices;
        }
        catch (Exception ex)
        {
            throw;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GetRateCardCosting(int ratecardid)
    {
        try
        {
            using (AgentEntities entity = new AgentEntities())
            {
                return (from q in entity.RateCardCostings where q.RateCardId == ratecardid select new {q.RateCardCostingId, q.RateCardId,q.Xlk_CalculationType.CalculationTypeId, q.UnitId,q.QtyFrom,q.QtyTo,q.SageRef, UnitDescription=q.Xlk_Unit.Description,CalculationTypeDescription=q.Xlk_CalculationType.Description, q.Cost, q.Discount, q.AllowProRate,q.BaseCost }).ToList();
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool SaveRateCard(Dictionary<string, object> newObject)
    {

        Int32? key = newObject.GetSafeDictionaryValue<Int32?>("uid");
        try
        {
            RateCard newrate = new RateCard();
            using (AgentEntities entity = new AgentEntities())
            {
                if (key.HasValue)
                    newrate = (from r in entity.RateCards where r.RateCardId == key.Value select r).FirstOrDefault();

                newrate.AgencyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Agencies","AgentId", newObject.GetSafeDictionaryValue<Int32>("AgentId"));
                //newrate.DateActiveFrom = Convert.ToDateTime(newObject.DateActiveFrom);
                //newrate.DateActiveTo = Convert.ToDateTime(newObject.DateActiveTo);
                //newrate.DefaultDiscount = Convert.ToDecimal(newObject.DefaultDiscount);
                //newrate.Xlk_UnitReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Unit", newObject.Xlk_Unit);
                //newrate.OrderableItemReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".OrderableItems", newObject.OrderableItem);

                if (!key.HasValue)
                    entity.AddToRateCards(newrate);
            }
            return false;
        }
        catch (Exception ex)
        {
            throw;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool SaveRateCardCosting(RateCardCosting newObject)
    {

        try
        {
            RateCardCosting newratecard = new RateCardCosting();
            using (AgentEntities entity = new AgentEntities())
            {
                if (newObject.RateCardId > 0)
                    newratecard = (from c in entity.RateCardCostings where c.RateCardId == newObject.RateCardId && c.UnitId == newObject.UnitId select c).FirstOrDefault();

                newratecard.AllowProRate = newObject.AllowProRate;
                newratecard.BaseCost = newObject.BaseCost;
                newratecard.Cost = newObject.Cost;
                newratecard.Discount = newObject.Discount;


                if (newratecard.RateCardId == 0)
                    entity.AddToRateCardCostings(newratecard);

                if (entity.SaveChanges() > 0)
                    return true;
            }
            return false;



        }
        catch (Exception ex)
        {
            throw;
        }
    }

    [WebMethod]
    public static int DeleteRateCard(int ratecardid)
    {
        RateCard newratecard = new RateCard();
        try
        {
            using (AgentEntities entity = new AgentEntities())
            {
                return entity.DeleteRateCard(ratecardid);
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    [WebMethod]
    public static int DeleteRateCardCosting(int ratecardcostingid)
    {
        RateCardCosting _object = new RateCardCosting();
        try
        {
            using (AgentEntities entity = new AgentEntities())
            {
                if (ratecardcostingid > 0)
                    _object = (from c in entity.RateCardCostings where c.RateCardCostingId == ratecardcostingid select c).FirstOrDefault();

                if (_object != null)
                    entity.DeleteObject(_object);

                return entity.SaveChanges();

            }

        }
        catch (Exception ex)
        {
            throw;
        }
    }

}