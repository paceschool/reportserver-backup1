﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Agent;

public partial class Agency_AddNote : BaseAgencyControl
{
    protected Int32 _familyId;
    protected AgencyNote _note;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("AgencyNoteId").HasValue)
        {
            using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
            {
                Int32 _agencyNoteId = GetValue<Int32>("AgencyNoteId");

                _note = (from x in entity.AgencyNotes.Include("Agency").Include("User").Include("Xlk_NoteType") where x.AgencyNoteId == _agencyNoteId select x).FirstOrDefault();
            }
        }
    }


    protected string GetAgencyId
    {
        get
        {
            if (_note != null)
                return _note.Agency.AgencyId.ToString();

            if (Parameters.ContainsKey("AgencyId"))
                return Parameters["AgencyId"].ToString();

            return "0";
        }
    }

    protected string GetAgencyNoteId
    {
        get
        {
            if (_note != null)
                return _note.AgencyNoteId.ToString();

            if (Parameters.ContainsKey("AgencyNoteId"))
                return Parameters["AgencyNoteId"].ToString();

            return "0";
        }
    }

    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_note != null && _note.Xlk_NoteType != null) ? _note.Xlk_NoteType.NoteTypeId : (byte?)null;

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NoteTypeId, item.Description, (_current.HasValue && item.NoteTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNoteUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_note != null && _note.User != null) ? _note.User.UserId : GetCurrentUser().UserId;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }


    protected string GetNote
    {
        get
        {
            if (_note != null)
                return _note.Note.ToString();

            return null;
        }
    }
}
