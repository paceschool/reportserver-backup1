﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddOrderableItemControl.ascx.cs"
    Inherits="Agent_AddOrderableItemControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addorderableitem");
        }

        getTargetUrl = function () {
            return  '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","SaveOrderableItem") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $(".date").datepicker({ dateFormat: 'dd/mm/yy' });

    });
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addorderableitem" name="addorderableitem" method="post" action="ViewAgencyPage.aspx/SaveOrderableItem">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ItemId" value="<%= GetItemId %>" />
        <label>
            Title
        </label>
        <input type="text" id="Title" value="<%= GetTitle %>" />
        <label>
            Description
        </label>
        <textarea id="Description"></textarea>
        <label>
            Product Type
        </label>
        <select type="text" name="Xlk_ProductType" id="ProductTypeId">
            <%= GetProductTypes()%>
        </select>
        <label>
            Sage Ref
        </label>
        <input type="text" id="SageRef" value="<%= GetSageRef %>" />
        <label>
            <i><b>This will create a new Rate Card for this Agent </b></i>
        </label>
    </fieldset>
</div>
</form>
