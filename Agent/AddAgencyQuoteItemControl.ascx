﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddAgencyQuoteItemControl.ascx.cs" 
    Inherits="Agent_AddAgencyQuoteItem" %>

<script type="text/javascript">
    $(function () {

        var items = null;

        getForm = function () {
            return $("#addagencyquoteitem");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Agent/ViewQuotePage.aspx","SaveQuoteItem") %>';
        }
        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        getRateCards = function (quoteid, itemid, qty) {
            var params = '{quoteid:' + quoteid + ', itemid:' + itemid + ', qty:' + qty + '}';
            $.ajax({
                type: "POST",
                url: "ViewQuotePage.aspx/GetQuoteRateCardCostings",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    items = null;

                    if (msg.d != null && !msg.d.Success) {
                        alert(msg.d.ErrorMessage);
                        return;
                    }

                    if (msg.d != null && msg.d.Success && msg.d.Payload.length > 0) {
                        items = msg.d.Payload;
                        $('#UnitId').find('option').remove();
                        if (items.length == 0)
                            $('#UnitId').find('option').end().append('<option value="">Not available</option>');
                        if (items.length >= 1) {
                            $('#UnitId').find('option').end().append('<option value="">Please Select</option>');

                            $(items).each(function () {
                                $('#UnitId').append($("<option></option>")
                                .attr("value", this.UnitId)
                                .text(this.Description)
                                .attr("selected", (this.DefaultUnitId == this.UnitId)));
                            });

                            if (items.length >= 1)
                                setItemPriceData(0);
                        }

                    }
                }
            });
        }

        $('.watcher').change(function () {
            getRateCards($('#QuoteId').val(), $('#ItemId option:selected').val(), $('#Qty').val());
        });

        $('#Qty').keyup(function () {
            getRateCards($('#QuoteId').val(), $('#ItemId option:selected').val(), $('#Qty').val());
        });

        $('.lwatcher').change(function () {
            var index = $('#UnitId')[0].selectedIndex;
            setItemPriceData(index);
        });

        $('.qwatcher').keyup(function () {
            setLinePrice();
        });

        $('.nwatcher').keyup(function () {
            setDiscount();
        });

        setItemPriceData = function (index) {
            if (items.length > 0) {
                $('#RRPrice').val(items[index].Cost);
                $('#CalculationTypeId').val(items[index].CalculationTypeId);
                $('#Discount').val(items[index].Discount);
                $('#RateCardId').val(items[index].RateCardId);
                setLinePrice();
            }
        }
        setDiscount = function () {
            var discount = 0;
            discount = 100 - (($('#LineNetPrice').val() / $('#LineGrossPrice').val()) * 100);
            $('#Discount').val(discount.toFixed(2));
        }
        setLinePrice = function () {
            var nett = 0;
            var gross = 0;
            if ($('#CalculationTypeId').val() == 1) {
                nett = (($('#RRPrice').val() * $('#Qty').val())) * (($('#Discount').val() > 0) ? (1 - $('#Discount').val() / 100) : 1);
                gross = $('#RRPrice').val() * $('#Qty').val();
            }
            else {
                gross = $('#RRPrice').val();
                nett = (($('#RRPrice').val())) * (($('#Discount').val() > 0) ? (1 - $('#Discount').val() / 100) : 1);
            }
            $('#LineGrossPrice').val(gross);
            $('#LineNetPrice').val(nett);

        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addagencyquoteitem" name="addagencyquoteitem" method="post" action="ViewQuotePage.aspx/GetQuoteRateCardCostings">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="QuoteId" value="<%= GetQuoteId %>" />
        <input type="hidden" id="LineId" value="<%= GetLineId %>" />
        <input type="hidden" name="RateCard" id="RateCardId" value="<%= GetRateCardId %>" />
        <input type="hidden" id="CalculationTypeId" />
        <label>
            Item
        </label>
        <select class="watcher" type="text" name="OrderableItem" id="ItemId">
            <%= GetItemList() %>
        </select>
        <label>
            Description
        </label>
        <textarea rows="2" id="description"><%=GetDescription %></textarea>
        <label>
            Quantity
        </label>
        <input type="text" id="Qty" value="<%= GetQuantity %>" />
        <label>
            Unit
        </label>
        <select class="lwatcher" type="text" name="Xlk_Unit" id="UnitId">
            <%= GetUnitList()%>
        </select>
        <label>
            Recommended Retail Price
        </label>
        <input  class="qwatcher" type="text" id="RRPrice" value="<%= GetRRPrice %>" />
        <label>
            Total Gross Price 
        </label>
        <input type="text" id="LineGrossPrice" value="<%= GetGrossPrice %>"/>
        <label>
            Discount (default: 0%)
        </label>
        <input  class="qwatcher" type="text" id="Discount" value="<%= GetDiscount %>"/>
        <label>
            Total Net Price 
        </label>
        <input class="nwatcher" type="text" id="LineNetPrice" value="<%= GetNetPrice %>"/>
    </fieldset>
</div>
</form>