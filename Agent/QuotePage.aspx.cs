﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Agent;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;

public partial class Agent_QuotePage : BaseAgentPage
{
    protected Int32 _agentId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["AgencyId"]))
            _agentId = Convert.ToInt32(Request["AgencyId"]);

        if (!Page.IsPostBack)
        {
            listagents.DataSource = this.LoadAgents();
            listagents.DataBind();

            listusers.DataSource = this.LoadUsers();
            listusers.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadQuotes();
                break;
            case "pending":
                Click_LoadPending();
                break;
            case "current":
                Click_LoadCurrentQuotes();
                break;
            case "myquotes":
                Click_LoadMyQuotes();
                break;
            case "all":
                Click_LoadAllQuotes();
                break;
            default:
                Click_LoadQuotes();
                break;
        }
    }

    protected void lookupQuote(object sender, EventArgs e)
    {
        Click_LoadQuotes();
    }

    private void LoadResults(List<Quote> quotes)
    {
        //Set the datasource of the repeater
        results.DataSource = quotes;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", quotes.Count().ToString());
    }

    protected void Click_LoadPending()
    {
        int agencyId = Convert.ToInt32(listagents.SelectedItem.Value);
        using (AgentEntities entity = new AgentEntities())
        {
            var quoteSearchQuery = from p in entity.Quotes
                                   where p.Xlk_Status.StatusId == 1
                                   select p;
            LoadResults(quoteSearchQuery.ToList());
        }
    }

    protected void Click_LoadQuotes()
    {
        int agencyId = Convert.ToInt32(listagents.SelectedItem.Value);
        int userId = Convert.ToInt32(listusers.SelectedItem.Value);
        using (AgentEntities entity = new AgentEntities())
        {
            var quoteSearchQuery = from q in entity.Quotes
                                   select q;
            if (!string.IsNullOrEmpty(lookupString.Text) || agencyId > -1 || userId > -1)
            {
                if (!string.IsNullOrEmpty(lookupString.Text))
                    quoteSearchQuery = quoteSearchQuery.Where(s => s.Agency.Name.StartsWith(lookupString.Text));

                if (agencyId > -1)
                    quoteSearchQuery = quoteSearchQuery.Where(s => s.Agency.AgencyId == agencyId);

                if (userId > -1)
                    quoteSearchQuery = quoteSearchQuery.Where(s => s.CreatedByUser.UserId == userId);

                LoadResults(quoteSearchQuery.ToList());
            }
            else
                LoadResults(quoteSearchQuery.OrderBy(g => g.DateCreated).Take(50).ToList());
        }
        results.DataBind();
        listagents.SelectedIndex = -1; listusers.SelectedIndex = -1; 

    }

    protected void Click_LoadCurrentQuotes()
    {
        DateTime today = DateTime.Now;

        using (AgentEntities entity = new AgentEntities())
        {
            var quoteSearchQuery = from q in entity.Quotes.Include("Xlk_Status").Include("Agency")
                                   where q.DateCreated <= DateTime.Now && q.Xlk_Status.StatusId == 2
                                   orderby q.DateCreated ascending
                                   select q;
            LoadResults(quoteSearchQuery.ToList());
        }
    }

    protected void Click_LoadMyQuotes()
    {
        int _userId = GetCurrentUser().UserId;

        using (AgentEntities entity = new AgentEntities())
        {
            var quoteSearchQuery = from q in entity.Quotes.Include("Xlk_Status").Include("Agency")
                                   where q.CreatedByUser.UserId == _userId
                                   orderby q.DateCreated ascending
                                   select q;
            LoadResults(quoteSearchQuery.ToList());
        }
    }

    protected void Click_LoadAllQuotes()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            var quoteSearchQuery = from q in entity.Quotes.Include("Xlk_Status").Include("Agency")
                                   orderby q.DateCreated ascending
                                   select q;
            LoadResults(quoteSearchQuery.ToList());
        }
    }

    protected void Click_AddNewQuote(object sender, EventArgs e)
    {
        LoadEditControl("#addagencyquote, Agent/AddAgencyQuoteControl.ascx",new Dictionary<string,object>() {{"AgentId", _agentId}});
    }

    
}