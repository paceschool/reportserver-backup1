﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CloneRateCardsControl.ascx.cs"
    Inherits="Agent_AddOrderableItemControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#cloneratecards");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","CloneRateCards") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $(".date").datepicker({ dateFormat: 'dd/mm/yy' });

    });
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="cloneratecards" name="cloneratecards" method="post" action="ViewAgencyPage.aspx/SaveOrderableItem">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ItemId" value="<%= GetItemId %>" />
        <label>
           <%= GetTitle %>
        </label>
        <label>
            Agency
        </label>
        <select id="AgencyId">
            <%= GetAgencyItems() %>
        </select>
        <label>
            Clone from year
        </label>
        <select type="text" id="FromYear">
            <%= GetPreviousYears(3)%>
        </select>
        <label>
            Clone to year
        </label>
        <select type="text" id="ToYear">
            <%= GetFutureYears(1)%>
        </select>
    </fieldset>
</div>
</form>
