﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddAgencyControl.ascx.cs"
    Inherits="Agency_AddAgency" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addagency");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","SaveAgency") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addagency" name="addagency" method="post" action="ViewAgencyPage.aspx/SaveAgency">
<div class="inputArea">
    <input type="hidden" id="AgencyId" value="<%= GetAgencyId %>" />
    <fieldset>
        <label>
            Agent Name
        </label>
        <input type="text" id="Name" value="<%= GetName %>" />
        <label>
            Contact Name
        </label>
        <input type="text" id="ContactName" value="<%= GetContactName %>" />
        <label>
            Contact Email
        </label>
        <input type="text" id="Email" value="<%= GetEmail %>" />
        <label>
            Nationality
        </label>
        <select name="Xlk_Nationality" id="NationalityId">
            <%= GetNationalityList()%>
        </select>
    </fieldset>
    <fieldset>
        <label>
            Address
        </label>
        <input type="text" id="Address" value="<%= GetAddress %>" />
        <label>
            Edit SAGE Reference?
        </label>
        <input type="text" id="SageRef" value="<%= GetSageRef %>" />
                <label>
           Commissioanble?
        </label>
        <input type="checkbox" id="IsCommissionable" value="true" <%= GetIsCommissionable %> />
    </fieldset>
    <fieldset>
</div>
</form>
