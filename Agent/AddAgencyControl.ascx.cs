﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Agent;

public partial class Agency_AddAgency : BaseAgencyControl
{
    protected Int32 _agencyId;
    protected Agency _agency;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("AgencyId").HasValue)
        {
            using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
            {
                Int32 _agencyId = GetValue<Int32>("AgencyId");

                _agency = (from x in entity.Agencies.Include("Xlk_Nationality")
                           where x.AgencyId == _agencyId
                           select x).FirstOrDefault();
            }
        }
    }


    protected string GetAgencyId
    {
        get
        {
            if (_agency != null)
                return _agency.AgencyId.ToString();

            if (Parameters.ContainsKey("AgencyId"))
                return Parameters["AgencyId"].ToString();

            return "0";
        }
    }
    protected string GetName
    {
        get
        {
            if (_agency != null)
                return _agency.Name;

            return string.Empty;
        }
    }
    protected string GetEmail
    {
        get
        {
            if (_agency != null)
                return _agency.Email;

            return string.Empty;
        }
    }
    protected string GetSageRef
    {
        get
        {
            if (_agency != null)
                return _agency.SageRef;

            return string.Empty;
        }
    }
    protected string GetAddress
    {
        get
        {
            if (_agency != null)
                return _agency.Address;

            return string.Empty;
        }
    }
    protected string GetContactName
    {
        get
        {
            if (_agency != null)
                return _agency.ContactName;

            return string.Empty;
        }
    }
    protected string GetIsCommissionable
    {
        get
        {
            if (_agency != null && _agency.IsCommissionable.HasValue && _agency.IsCommissionable.Value)
                return "checked";

            return string.Empty;
        }
    }

    protected string GetNationalityList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_agency != null && _agency.Xlk_Nationality != null) ? _agency.Xlk_Nationality.NationalityId : (short?)null;

        foreach (Xlk_Nationality item in LoadNationalities())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NationalityId, item.Description, (_current.HasValue && item.NationalityId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

}
