﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Agency_AddComplaint : BaseEnrollmentControl
{
    protected Int32 _familyId;
    protected Int32 _groupId;
    protected Pace.DataAccess.Agent.AgencyComplaint _complaint;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
        {
            if (GetValue<Int32?>("AgencyComplaintId") != null)
            {
                Int32 agencycomplaintId = GetValue<Int32>("AgencyComplaintId");

                _complaint = (from x in entity.AgencyComplaints where x.AgencyComplaintId == agencycomplaintId select x).FirstOrDefault();
            }
        }
    }


    protected string GetComplaintTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_ComplaintType item in LoadComplaintTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>",item.ComplaintTypeId,item.Description );
        }

        return sb.ToString();

    }
    protected string GetSeverityList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_Severity item in LoadSeverity())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.SeverityId, item.Description);
        }

        return sb.ToString();

    }

    protected string GetActionNeeded
    {
        get
        {
            if (_complaint != null && _complaint.ActionNeeded == true)
                return "checked=\"true\"";

            return "checked=\"false\"";
        }
    }
}
