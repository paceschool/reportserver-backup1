﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Agent;
using System.Data.Entity.Core;

public partial class AgentPage : BaseAgentPage
{
    Int32 _agencyId;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["AgencyId"]))
            _agencyId = Convert.ToInt32(Request["AgencyId"]);
            
        if (!Page.IsPostBack)
        {
            agentnationality.DataSource = LoadNationalities();
            agentnationality.DataBind();
        }


    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["Action"]))
            ShowSelected(Request["Action"]);
    }


    #region Private Methods

    private void LoadAll() //Load All Records
    {
        using (AgentEntities entity = new AgentEntities())
        {
            var AgentSearchQuery = from a in entity.Agencies
                                   select a;

            LoadResults(AgentSearchQuery.ToList());
            agentnationality.SelectedIndex = 0;
        }
    }

    protected void LoadSearch() //Load from Selected Nationality & Search Field
    {
        int nationalityId = Convert.ToInt32(agentnationality.SelectedItem.Value);
        using (AgentEntities entity = new AgentEntities())
        {
            var AgentSearchQuery = from d in entity.Agencies
                                       select d;

            if (!string.IsNullOrEmpty(lookupString.Text) || nationalityId > 0)
            {

                if (!string.IsNullOrEmpty(lookupString.Text))
                    AgentSearchQuery = AgentSearchQuery.Where(d => d.ContactName.Contains(lookupString.Text) || d.Name.Contains(lookupString.Text));

                if (nationalityId > 0)
                    AgentSearchQuery = AgentSearchQuery.Where(d => d.Xlk_Nationality.NationalityId == nationalityId);


                LoadResults(AgentSearchQuery.ToList());
            }
            else
                LoadResults(AgentSearchQuery.Take(50).ToList());

       }
    }

    protected void LoadCurrent() //Load from Selected Nationality & Search Field
    {
        int nationalityId = Convert.ToInt32(agentnationality.SelectedItem.Value);
        List<int> ids;
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            ids = (from s in entity.Student
                   where (s.Enrollment.Any(e => e.StartDate.CompareTo(DateTime.Now) * DateTime.Now.CompareTo(e.EndDate.Value) > 0))
                   select s.Agency.AgencyId).Distinct().ToList();
        }

        using (AgentEntities entity = new AgentEntities())
        {
            var agentSearchQuery = (from d in entity.Agencies
                                    where (ids.Contains(d.AgencyId))
                                    select d).Distinct();
            LoadResults(agentSearchQuery.ToList());

        }
    }

    private void LoadResults(List<Agency> agents) //Bind data & populate Records Found
    {
        results.DataSource = agents.OrderBy(a=>a.Name);
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", agents.Count().ToString());
    }

    protected void ShowSelected(string action) //Actions for LinkButtons
    {
        switch (action.ToLower())
        {
            case "search":
                LoadSearch();
                break;
            case "current":
                LoadCurrent();
                break;
            case "all":
                LoadAll();
                lookupString.Text = "";
                break;
            default:
                LoadCurrent();
                break;
        }
    }

    protected void DeleteAgent_Click(object sender, ImageClickEventArgs e)
    {
        object toDelete = null;

        using (AgentEntities entity = BaseAgentPage.CreateEntity)
        {
            Int32 _agentId = Convert.ToInt32(((ImageButton)sender).ToolTip);
            if (entity.TryGetObjectByKey(new EntityKey(entity.DefaultContainerName + ".Agencies", "AgencyId", _agentId), out toDelete))
            {
                entity.DeleteObject(toDelete);
                if (entity.SaveChanges() > 0)
                    LoadAll();
            }
        }
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
       LoadSearch();
    }
    #endregion

}
