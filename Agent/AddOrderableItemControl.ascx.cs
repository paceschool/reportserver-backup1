﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Agent;

public partial class Agent_AddOrderableItemControl : BaseAgencyControl
{

    protected OrderableItem _item;
    protected Int16? _itemId;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AgentEntities entity = new AgentEntities())
        {

            if (GetValue<Int16?>("ItemId").HasValue)
            {
                _itemId = GetValue<Int16>("ItemId");
                _item = (from item in entity.OrderableItems.Include("Xlk_ProductType")
                         where item.ItemId == _itemId
                         select item).FirstOrDefault();
            }
        }
    }


    protected string IsEditMode
    {
        get
        {
            if (_itemId.HasValue || _item != null)
                return "disabled";

            return String.Empty;

        }
    }


    protected string GetItemId
    {
        get
        {
            if (_item != null)
                return _item.ItemId.ToString();
            if (_itemId.HasValue)
                return _itemId.Value.ToString();

            return "0";
        }
    }

    protected string GetTitle
    {
        get
        {
            if (_item != null)
                return _item.Title;

            return null;
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_item != null)
                return _item.Description;

            return null;
        }
    }

    protected string GetSageRef
    {
        get
        {
            if (_item != null)
                return _item.SageRef;

            return null;
        }
    }

    protected string GetProductTypes()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_item != null && _item.Xlk_ProductType.ProductTypeId != null) ? _item.Xlk_ProductType.ProductTypeId : (short?)null;

        foreach (Xlk_ProductType item in LoadProductType())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ProductTypeId, item.Description, (_current.HasValue && item.ProductTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}