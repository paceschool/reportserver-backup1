﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AgentPage.aspx.cs" Inherits="AgentPage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            $("#<%= repeat.ClientID %>").buttonset();

            $('#<%= lookupString.ClientID %>').bind('keypress', function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) { //Enter keycode
                    eval($("#<%=searchButton.ClientID %>").attr('href'));
                }
            });

        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Agent Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->    
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Nationality
                    </td>
                    <td>
                        <asp:DropDownList ID="agentnationality" runat="server" DataTextField="Description" DataValueField="NationalityId"  AppendDataBoundItems="true" EnableViewState="true">
                          <asp:ListItem Text="All Nationalities" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Search By Agency/Contact Name
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" runat="server"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="searchButton" PostBackUrl="~/Agent/AgentPage.aspx" 
            runat="server" OnClick="LinkButton2_Click"><span>Search Results</span>
            </asp:LinkButton>
            <a href="?Action=current">Current</a> 
            <a href="?Action=all">All</a> 
        </span>
        <span style="float: right;">
            <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
            </asp:Label>
        </span>

        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Agent Name
                    </th>
                    <th scope="col">
                        Contact Name
                    </th>
                    <th scope="col">
                        email
                    </th>
                    <th scope="col">
                        SAGE Ref
                    </th>
                    <th scope="col">
                        Commissionable?
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align:left">
                                <%#DataBinder.Eval(Container.DataItem, "AgencyId") %>
                            </td>
                            <td style="text-align:left">
                                <%# AgencyLink(DataBinder.Eval(Container.DataItem, "AgencyId"), "~/Agent/ViewAgentPage.aspx", CreateName(DataBinder.Eval(Container.DataItem, "Name"), ""))%>
                            </td>
                            <td style="text-align:left">
                                <%#DataBinder.Eval(Container.DataItem, "ContactName") %>
                            </td>
                            <td style="text-align:center">
                                <asp:HyperLink ID="lnkEmail" runat="server" NavigateUrl='<%#DataBinder.Eval(Container.DataItem, "Email", "MailTo:{0}") %>'
                                    Target="_blank" ToolTip="Click to open email message window" ImageUrl="~/Content/img/actions/email_go.png">
                                    <%#DataBinder.Eval(Container.DataItem, "Email") %>
                                </asp:HyperLink>
                            </td>
                            <td style="text-align:left">
                                <%#DataBinder.Eval(Container.DataItem, "SageRef") %>
                            </td>
                            <td style="text-align:left">
                                <%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCommissionable")) ? "Yes" : "No"%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
               
            </tbody>
        </table>
    </div>
</asp:Content>