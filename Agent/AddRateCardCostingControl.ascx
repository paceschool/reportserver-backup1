﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddRateCardCostingControl.ascx.cs"
    Inherits="Agent_AddRateCardCostingControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addratecardcosting");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","SaveRateCardCosting") %>';
        }

        getLocalRules = function () {
            var localrules = {};
            localrules = {
                QtyFrom: {
                    number: true,
                    minlength: 1,
                    minStrict: 0,
                    required: true
                },
                QtyTo: {
                    number: true,
                    required: function(element) {
                            return  $("#QtyFrom").val() == '' || $("#QtyTo").val() >= $("#QtyFrom").val();
                          },
                }

            }
            return localrules;
        }

        getLocalMessages = function () {
            var localmessages = {};
            localmessages = {
                QtyFrom: {
                    required: "Please provide a quantity from",
                    minlength: "The quantity from is not valid"
                },
                QtyTo: {
                    required: "Please provide a quantity to and it must be greater than the quantity from",
                    minlength: "The quantity to is not valid"
                }
            }
            return localmessages;
        }

        $(".date").datepicker({ dateFormat: 'dd/mm/yy' });

    });
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addratecardcosting" name="addratecardcosting" method="post" action="ViewAgencyPage.aspx/SaveRateCardCosting">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="RateCardCostingId" value="<%= GetRateCardCostingId %>" />
        <input type="hidden" id="RateCardId" value="<%= GetRateCardId %>" />
        <label>
            Unit
        </label>
        <select <%= (_unitId.HasValue && _unitId > 0) ? "disabled" : String.Empty %> name="Xlk_Unit"
            id="UnitId">
            <%= GetUnits() %>
        </select>
        <label>
            Quantity From
        </label>
        <input type="text" name="QtyFrom" id="QtyFrom" value="<%= GetQtyFrom %>" />
        <label>
            Quantity To
        </label>
        <input type="text" name="QtyTo" id="QtyTo" value="<%= GetQtyTo %>" />
        <select name="Xlk_CalculationType" id="CalculationTypeId">
            <%= GetCalculationType() %>
        </select>
        <label>
            Cost
        </label>
        <input type="text" name="Cost" id="Cost" value="<%= GetCost %>" />
        <label>
            Discount
        </label>
        <input type="text" name="Discount" id="Discount" value="<%= GetDiscount %>" />
        <label>
            Allow ProRate
        </label>
        <input type="checkbox" id="AllowProRate" <%= GetAllowProRate %> value="true" />
        <label>
            SageRef
        </label>
        <input type="text" id="SageRef" value="<%= GetSageRef %>" />
        <label>
            BaseCost
        </label>
        <input type="text" id="BaseCost" value="<%= GetBaseCost %>" />
    </fieldset>
</div>
</form>
