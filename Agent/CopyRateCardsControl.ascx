﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CopyRateCardsControl.ascx.cs"
    Inherits="Agent_AddOrderableItemControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#copyratecards");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","CopyRateCards") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $(".date").datepicker({ dateFormat: 'dd/mm/yy' });

    });
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="copyratecards" name="copyratecards" method="post" action="ViewAgencyPage.aspx/CopyRateCards">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ItemId" value="<%= GetItemId %>" />
        <label>
           <%= GetTitle %>
        </label>
        <label>
            From Agency
        </label>
        <select id="FromAgencyId">
            <%= GetAgencyItems() %>
        </select>
        <label>
            To Agency
        </label>
        <select id="ToAgencyId">
            <%= GetAgencyItems() %>
        </select>
        <label>
            Copy from year
        </label>
        <select type="text" id="FromYear">
            <%= GetPreviousYears(3)%>
        </select>
    </fieldset>
</div>
</form>
