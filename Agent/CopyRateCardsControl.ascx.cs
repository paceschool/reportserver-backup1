﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Agent;

public partial class Agent_AddOrderableItemControl : BaseAgencyControl
{

    protected RateCard _card;
    protected Int16? _itemId;
    protected Int32? _agencyId;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AgentEntities entity = new AgentEntities())
        {
            if (GetValue<Int16?>("ItemId").HasValue)
            {
                _itemId = GetValue<Int16>("ItemId");
                _card = (from item in entity.RateCards.Include("OrderableItem").Include("Agency")
                         where item.OrderableItem.ItemId == _itemId
                         select item).FirstOrDefault();
            }

            if (GetValue<Int32?>("AgencyId").HasValue)
            {
                _agencyId = GetValue<Int32?>("AgencyId");
            }
        }
    }

    protected string IsEditMode
    {
        get
        {
            if (_card != null)
                return "disabled";

            return String.Empty;

        }
    }

    protected string GetItemId
    {
        get
        {
            if (_card != null)
                return _card.OrderableItem.ItemId.ToString();
            if (_itemId.HasValue)
                return _itemId.Value.ToString();

            return "0";
        }
    }

    protected string GetTitle
    {
        get
        {
            if (_card != null)
                return _card.OrderableItem.Title.ToString();

            return "";
        }
    }

    protected string GetPreviousYears(int number)
    {
        StringBuilder sb = new StringBuilder();

        using (AgentEntities entity = new AgentEntities())
        {
            var years = (from i in entity.RateCards
                         orderby i.DateActiveFrom
                         select i.DateActiveFrom.Year).Distinct().Take(number);

            foreach (var year in years)
            {
                sb.AppendFormat("<option {1} value={0}>{0}</option>", year, (DateTime.Now.Year == year) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();
    }

    protected string GetAgencyItems()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_agencyId.HasValue) ? _agencyId.Value : default(Int32);

        foreach (Agency item in LoadAgents().OrderBy(y => y.Name))
        {
            sb.AppendFormat("<option {2} value={0}>{1} ({3})</option>", item.AgencyId, item.Name, (_current.HasValue && item.AgencyId == _current) ? "selected" : string.Empty, item.SageRef);
        }

        return sb.ToString();
    }

    protected string GetFutureYears(int number)
    {
        StringBuilder sb = new StringBuilder();

        using (AgentEntities entity = new AgentEntities())
        {
            var yearquery = (from i in entity.RateCards
                             where i.OrderableItem.ItemId == _itemId
                             select i);

            if (_agencyId.HasValue && _agencyId.Value > 0)
                yearquery = yearquery.Where(a => a.Agency.AgencyId == _agencyId.Value);

            var maxyear = DateTime.Now.Year;

            if (yearquery.Count() > 0)
                maxyear = yearquery.Select(x => x.DateActiveTo.Year).Max();


            sb.AppendFormat("<option {1} value={0}>{0}</option>", maxyear + 1, (DateTime.Now.Year == maxyear + 1) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}