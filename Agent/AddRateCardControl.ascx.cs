﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Agent;

public partial class Agent_AddRateCardControl : BaseAgencyControl
{
    protected Int32? _agencyId;
    protected Int16? _itemId;
    protected Int32? _rateCardId;
    protected RateCard _card;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (AgentEntities entity = new AgentEntities())
        {
            if (GetValue<Int32?>("ItemId").HasValue)
            {
                _itemId = GetValue<Int16>("ItemId");
            }
            if (GetValue<Int32?>("RateCardId").HasValue)
            {
                _rateCardId = GetValue<Int32>("RateCardId");
                _card = (from cards in entity.RateCards.Include("Agency").Include("OrderableItem").Include("RateCardCostings").Include("Xlk_Unit")
                         where cards.RateCardId == _rateCardId
                         select cards).FirstOrDefault();
            }
        }
    }

    protected string IsEditMode
    {
        get
        {
            if ((_agencyId.HasValue && _itemId.HasValue ) || _card != null)
                return "disabled";

            return String.Empty;

        }
    }

    protected string GetRateCardId
    {
        get
        {
            if (_card != null)
                return _card.RateCardId.ToString();
            if (_rateCardId.HasValue)
                return _rateCardId.Value.ToString();
            return "0";
        }
    }


    protected string GetAgencyId
    {
        get
        {
            if (_card != null && _card.Agency != null)
                return _card.Agency.AgencyId.ToString();

            if (_agencyId.HasValue)
                return _agencyId.Value.ToString();

            return "0";
        }
    }

    protected string GetItemId
    {
        get
        {
            if (_card != null && _card.OrderableItem != null)
                return _card.OrderableItem.ItemId.ToString();
            if (_itemId.HasValue)
                return _itemId.Value.ToString();
            return "0";
        }
    }

    protected string GetAgencyItems()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_card != null && _card.Agency != null) ? _card.Agency.AgencyId : (_agencyId.HasValue) ? _agencyId.Value : default(Int32);

        foreach (Agency item in LoadAgents().OrderBy(y=>y.Name))
        {
            sb.AppendFormat("<option {2} value={0}>{1} ({3})</option>", item.AgencyId, item.Name, (_current.HasValue && item.AgencyId == _current) ? "selected" : string.Empty,item.SageRef);
        }

        return sb.ToString();
    }

    protected string GetOrderableItems()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_card != null && _card.OrderableItem != null) ? _card.OrderableItem.ItemId : (_itemId.HasValue) ? _itemId.Value : default(Int16);

        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Pace.DataAccess.Agent.OrderableItem> lookupQuery =
                from o in entity.OrderableItems
                select o;

            foreach (OrderableItem item in lookupQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ItemId, item.Title, (_current.HasValue && item.ItemId == _current) ? "selected" : string.Empty);
            }
        }
        return sb.ToString();
    }

    protected string GetRateCardCostings()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_card != null && _card.RateCardCostings != null) ? _card.RateCardId : (Int32?)null;

        foreach (RateCardCosting item in LoadRateCardCostings())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.RateCardId, item.Cost, (_current.HasValue && item.RateCardId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetDateFrom
    {
        get
        {
            if (_card != null)
                return _card.DateActiveFrom.ToString("dd/MM/yy");

            return new DateTime(DateTime.Now.Year, 1, 1).ToString();
        }
    }

    protected string GetDateTo
    {
        get
        {
            if (_card != null)
                return _card.DateActiveTo.ToString("dd/MM/yy");

            return  new DateTime( DateTime.Now.Year, 12, 31).ToString();
        }
    }

    protected string GetDiscount
    {
        get
        {
            if (_card != null)
                return _card.DefaultDiscount.Value.ToString();

            return "0";
        }
    }

    protected string GetUnits()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_card != null && _card.Xlk_Unit != null) ? _card.Xlk_Unit.UnitId : (byte?)null;

        foreach (Xlk_Unit item in LoadUnits())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UnitId, item.Description, (_current.HasValue && item.UnitId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}