﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewAgentPage.aspx.cs" Inherits="Agent_ViewAgentPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {

            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }

            showPrintOptions = function () {
                $("#dialog-print").dialog("open");
            };

            $("#dialog-print").css({ 'background-color': '#E0FFFF' }).dialog({
                autoOpen: false,
                modal: true
            });

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="View Agent Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <td>
                        <asp:Label ID="curAgencyId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Agent Name
                    </th>
                    <td>
                        <asp:Label ID="curName" runat="server">
                        </asp:Label><%= AgencyLink(_agentId, "~/Agent/ViewAgentPage.aspx", "") %>
                    </td>
                    <th scope="col">
                        Contact Name
                    </th>
                    <td>
                        <asp:Label ID="curContactName" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Address
                    </th>
                    <td>
                        <asp:Label ID="curAddress" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Email
                    </th>
                    <td>
                        <asp:Label ID="curEmail" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Natioanlity
                    </th>
                    <td>
                        <asp:Label ID="curNationality" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Month To Date
                    </th>
                    <td>
                        <asp:Label ID="curMonth" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Year To Date
                    </th>
                    <td>
                        <asp:Label ID="curYear" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Previous Year Turnover
                    </th>
                    <td>
                        <asp:Label ID="curPreviousYear" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        SAGE Ref
                    </th>
                    <td>
                        <asp:Label ID="curSageRef" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Commissionable?
                    </th>
                    <td>
                        <asp:Label ID="curIsCommissionable" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Actions
                    </th>
                    <td>
                        <a title="Edit Agency" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyControl.ascx',{'AgencyId':<%=_agentId %>}, refreshPage)">
                            <img src="../Content/img/actions/edit.png" /></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Current Students</a></li>
                <li><a href="#tabs-2">Past Students</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyGroups?id=<%= _agentId %>">Groups</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyComplaints?id=<%= _agentId %>">Complaints</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyNotes?id=<%= _agentId %>">Notes</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyInvoices?id=<%= _agentId %>">Invoices</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyQuotations?id=<%= _agentId %>">Quotes</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyCredits?id=<%= _agentId %>">Credit Notes</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyReceipts?id=<%= _agentId %>">Receipts</a></li>
                <li><a href="ViewAgentPage.aspx/LoadAgencyRates?id=<%= _agentId %>">Rate Cards</a></li>
            </ul>
            <div id="tabs-1">
                <table class="box-table-a">
                    <thead>
                        <tr>
                            <th scope="col">
                                Id
                            </th>
                            <th scope="col">
                                Name
                            </th>
                            <th scope="col">
                                Arrival
                            </th>
                            <th scope="col">
                                Departure
                            </th>
                            <th scope="col">
                                #Weeks
                            </th>
                            <th scope="col">
                                Nationality
                            </th>
                            <th scope="col">
                                Agent
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="currentStudents" runat="server" EnableViewState="True">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "StudentId") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%# StudentLink(DataBinder.Eval(Container.DataItem, "StudentId"), "~/Enrollments/ViewStudentPage.aspx", CreateName(DataBinder.Eval(Container.DataItem, "FirstName"), DataBinder.Eval(Container.DataItem, "SurName")))%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "ArrivalDate", "{0:dd MMM yyyy}") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "DepartureDate", "{0:dd MMM yyyy}")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%# CalculateTimePeriod(DataBinder.Eval(Container.DataItem, "ArrivalDate"), DataBinder.Eval(Container.DataItem, "DepartureDate"),'W')%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Xlk_Nationality.Description") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Agency.Name") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div id="tabs-2">
                <table id="Table2" class="box-table-a">
                    <thead>
                        <tr>
                            <th scope="col">
                                Id
                            </th>
                            <th scope="col">
                                Name
                            </th>
                            <th scope="col">
                                Arrival
                            </th>
                            <th scope="col">
                                Departure
                            </th>
                            <th scope="col">
                                #Weeks
                            </th>
                            <th scope="col">
                                Nationality
                            </th>
                            <th scope="col">
                                Agent
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="pastStudents" runat="server" EnableViewState="True">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "StudentId") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%# StudentLink(DataBinder.Eval(Container.DataItem, "StudentId"), "~/Enrollments/ViewStudentPage.aspx", CreateName(DataBinder.Eval(Container.DataItem, "FirstName"), DataBinder.Eval(Container.DataItem, "SurName")))%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "ArrivalDate", "{0:dd MMM yyyy}") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "DepartureDate", "{0:dd MMM yyyy}")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%# CalculateTimePeriod(DataBinder.Eval(Container.DataItem, "ArrivalDate"), DataBinder.Eval(Container.DataItem, "DepartureDate"),'W')%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Xlk_Nationality.Description") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Agency.Name") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="dialog-print" title="Print Options" style="display: none;">
        <a href="<%= ReportPath("AgentReports","Admin_StudentsByAgent","PDF", new Dictionary<string, object> { {"AgencyId",_agentId}})%>"
            title="Print Student List">
            <img src="../Content/img/actions/report-paper.png" title="Print Student List" />
            Print Student List </a>
        <br />
        <br />
    </div>
</asp:Content>
