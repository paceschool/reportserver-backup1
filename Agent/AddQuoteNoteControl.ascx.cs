﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Agent;

public partial class Quote_AddNote : BaseAgencyControl
{
    protected QuoteNote _note;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (GetValue<Int32?>("QuoteNoteId").HasValue)
        {
            using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
            {
                Int32 _quoteNoteId = GetValue<Int32>("QuoteNoteId");

                _note = (from x in entity.QuoteNotes.Include("Quote").Include("User").Include("Xlk_NoteType") where x.QuoteNoteId == _quoteNoteId select x).FirstOrDefault();
            }
        }
    }


    protected string GetQuoteId
    {
        get
        {
            if (_note != null)
                return _note.Quote.QuoteId.ToString();

            if (Parameters.ContainsKey("QuoteId"))
                return Parameters["QuoteId"].ToString();

            return "0";
        }
    }

    protected string GetQuoteNoteId
    {
        get
        {
            if (_note != null)
                return _note.QuoteNoteId.ToString();

            if (Parameters.ContainsKey("QuoteNoteId"))
                return Parameters["QuoteNoteId"].ToString();

            return "0";
        }
    }

    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();


        byte? _current = (_note != null && _note.Xlk_NoteType != null) ? _note.Xlk_NoteType.NoteTypeId : (byte?)null;

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NoteTypeId, item.Description, (_current.HasValue && item.NoteTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNoteUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_note != null && _note.User != null) ? _note.User.UserId : GetCurrentUser().UserId;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }


    protected string GetNote
    {
        get
        {
            if (_note != null)
                return _note.Note.ToString();

            return null;
        }
    }
}
