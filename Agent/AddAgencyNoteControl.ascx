﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddAgencyNoteControl.ascx.cs"
    Inherits="Agency_AddNote" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
        return $("#addagencynote");
        }

        getTargetUrl = function() {
            return  '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","SaveAgencyNote") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addagencynote" name="addagencynote" method="post" action="ViewAgencyPage.aspx/SaveAgencyNote">
<div class="inputArea">
    <fieldset>

       <input type="hidden" name="Agency" id="AgencyId" value="<%= GetAgencyId %>" />
        <input type="hidden" id="AgencyNoteId" value="<%= GetAgencyNoteId %>" />

        <label>
            Type
        </label>
        <select name="Xlk_NoteType" id="NoteTypeId">
            <%= GetNoteTypeList()%>
        </select>
        <label>
            Created By
        </label>
        <select name="User" id="UserId">
             <%= GetNoteUserList()%>
        </select>
        <label>
            Note
        </label>
        <textarea id="Note"><%= GetNote %></textarea>
    </fieldset>
</div>
</form>
