﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewGroupPage.aspx.cs" Inherits="ViewGroupPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function() {

            var Id = <%= _groupId %>;

            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function() {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function(xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function(result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>
    <style type="text/css">
        .singleRow
{
  float: left;
  width: 322px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="View Group Details Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        <label>
                            Group ID
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="GroupId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Name
                        </label>
                    </th>
                    <td style="text-align: left;">
                        <asp:Label ID="GroupName" runat="server" Font-Bold="true" Font-Size="Larger">
                        </asp:Label>
                        <%= GroupLink(_groupId, "~/Groups/ViewGroupPage.aspx", "")%>
                        &nbsp;
                        <% if (groupStat != 2)
                           { %>
                        <asp:Label ID="GroupStatusText" runat="server" Font-Italic="true" ForeColor="Red" Font-Bold="true" Font-Size="Larger">
                        </asp:Label>
                        <% } %>
                    </td>
                    <th scope="col">
                        <label>
                            #Weeks
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Weeks" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Arrival Date
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="GroupArrival" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Departure Date
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="GroupDeparture" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Closed
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="closed" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Nationality
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="nationality" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Agency
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="agency" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Leader's Mobile
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="mobile" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            No. Students
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="nosts" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            No. Leaders
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="nolds" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            No. Classes
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="noclass" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Bus Tickets
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="bustickets" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Curfew
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curfew" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Actions
                        </label>
                    </th>
                    <td style="text-align: center">
                        <a title="Edit Group" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','GroupPage.aspx','Groups/AddGroupControl.ascx',{'GroupId':<%=_groupId %>},refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a> <a href="#" title="Create Tasks"
                                onclick="ajaxMessageBox('ViewGroupPage.aspx','ProcessTasks','All tasks complete or incomplete for this gourp will be deleted, are you sure you wish to continue?', <%=_groupId %>)">
                                <img src="../Content/img/actions/tools.png" title="Create Tasks" /></a>&nbsp;
                        <a href="#" title="Process Family Payments" onclick="ajaxMessageBox('ViewGroupPage.aspx','ProcessFamilyPayments','All unprocessed payments for this gourp will be deleted, are you sure you wish to continue?',<%=_groupId %>)">
                            <img src="../Content/img/actions/euro_sign.png" title="Process Family Payments" /></a>&nbsp;
                        <a href="#" title="Email Families" onclick="loadEditArrayControl('#dialog-form','ViewGroupPage.aspx','Groups/EmailGroupFamilyControl.ascx',{'GroupId':<%=_groupId %>})">
                            <img src="../Content/img/actions/email_go.png" title="Email Families" /></a>&nbsp;
                        <a href="#" title="Print Options" onclick="showPrintOptions('#dialog-print');">
                            <img src="../Content/img/actions/printer.png" title="Print Options" /></a>
                        &nbsp
                        &nbsp
                        &nbsp
                        &nbsp
                        <a href="<%= ReportPath("GroupReports","FamilyDetails","HTML4.0", new Dictionary<string, object> { {"GroupId",_groupId}, {"AgencyId",_agencyId}}) %>"
                title="Print Agent Families Report">
                <img src="../Content/img/actions/hand.png" title="Print Agent Families Report" /></a>
                        <a href="<%= ReportPath("GroupReports","GroupFamilyListing","HTML4.0", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Family Listing">
                <img src="../Content/img/actions/report-paper.png" title="Print Family Listing" /></a>
                        <a href="<%= ReportPath("GroupReports", "Group_AustrianTimetable", "PDF", new Dictionary<string, object> { {"GroupId", _groupId}}) %>"
                title="Print Group Timetable">
                <img src="../Content/img/actions/table.png" title="Print Group Timetable" /></a>
                        <a href="<%= ReportPath("GroupReports","FamilyGroupCheatSheet","WORD", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Family Cheat Sheets">
                <img src="../Content/img/actions/F.png" title="Print Family Cheat Sheets" /></a>
                        <a href="<%= ReportPath("GroupReports","OfficeGroupDetails","PDF", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Email Detail Lists for Office">
                <img src="../Content/img/actions/email_go.png" title="Email Detail Lists for Office" /></a>
                    </td>
                </tr>
            </tbody>
        </table>
  
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="ViewGroupPage.aspx/LoadGroupEnrollments?id=<%= _groupId %>">Academic</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupStudents?id=<%= _groupId %>">Students</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupHostings?id=<%= _groupId %>">View Families</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupReservedFamilies?id=<%= _groupId %>">Reserved
                    Families</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupTransfers?id=<%= _groupId %>">Transfers</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupTransport?id=<%= _groupId %>">Buses</a></li>
                <li><a href="ViewGroupPage.aspx/LoadOrderedItems?id=<%= _groupId %>">Ordered Items</a></li>
                <%--<li><a href="ViewGroupPage.aspx/LoadInvoiceReceipts?id=<%= _groupId %>">Receipts</a></li>--%>
                <% if (LoadGroup(new Pace.DataAccess.Group.GroupEntities(), _groupId).IsClosed)
                   { %>
                <li><a href="ViewGroupPage.aspx/LoadGroupClasses?id=<%= _groupId %>">Classes</a></li>
                <% } %>
                <li><a href="ViewGroupPage.aspx/LoadGroupExcursions?id=<%= _groupId %>">Excursions</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupComplaints?id=<%= _groupId %>">Complaints</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupNotes?id=<%= _groupId %>">Notes</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupInvoice?id=<%= _groupId %>">Finance</a></li>
                <li><a href="ViewGroupPage.aspx/LoadGroupTasks?id=<%= _groupId %>">Tasks</a></li>
                <%--<li><a href="#">Docs</a></li>--%>
            </ul>
        </div>
    </div>
    <!-- Areas for showing in the dialog -->
    <div id="dialog-print" title="Print Option" style="display: none;">
        <fieldset>
            <a href="<%= ReportPath("GroupReports", "Group_NewChecklist", "PDF", new Dictionary<string, object> { { "GroupId", _groupId } } ) %>"
                title="Print Check List">
                <img src="../Content/img/actions/printer.png" title="Print Check List" />Print Check
                List</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports","GroupFolder","PDF", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Cover Sheet">
                <img src="../Content/img/actions/printer.png" title="Print Cover Sheet" />Print
                Cover Sheet for File</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports","CompleteGroupDetails","PDF", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Save Detail Lists for SFA">
                <img src="../Content/img/actions/save.png" title="Save Detail Lists for SFA" />Save
                Group Details for SFA</a><br />
            <br />
            <a href="<%= ReportPath("AcademicReports","Academic_GroupTeacherLabels","HTML4.0", new Dictionary<string, object> { {"GroupId",_groupId}, {"ModellingDate", _StartDate}, {"CourseTypeId", 1}, {"BuildingId", 1}, {"IsClosed", true}, {"ClassId", 0}}) %>"
                title="Print Room Door Lists">
                <img src="../Content/img/door.png" title="Print Room Door Lists" />Print Room Door
                Lists</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports","GroupExcursionList","PDF", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Group Excursion List">
                <img src="../Content/img/actions/report-paper.png" title="Print Group Excursion List" />Print
                Group Excursion List</a><br />
            <br />
            <a href="<%= ReportPath("ExcursionReports","Excursion_GroupVouchers","PDF", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Group Excursion Vouchers">
                <img src="../Content/img/actions/printer.png" title="Print Group Excursion Vouchers" />Print
                Group Excursion Vouchers</a><br />
            <br />
        </fieldset>
        <fieldset>
            <a href="<%= ReportPath("GroupReports","GroupStudentCert","HTML4.0", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Group Certificates">
                <img src="../Content/img/actions/printer.png" title="Print Group Certs" />Print
                Group Certs</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports","StudentReport","HTML4.0", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Teacher Appraisal for Students">
                <img src="../Content/img/actions/printer.png" title="Print Teacher Appraisal for Students" />Print
                Teacher Appraisal for Students</a><br />
            <br />
            <% if (GroupStatus == true)
               { %>
            <a href="<%= ReportPath ("GroupReports", "Group_NewEOCAppraisal", "HTML4.0", new Dictionary<string,object> { {"Group", _groupId} } ) %>"
                <% }
                   else 
                   { %>
            <a href="<%= ReportPath("GroupReports","GroupEOCAppraisal","HTML4.0", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                <% } %>
                title="Print Group Appraisals">
                <img src="../Content/img/actions/printer.png" title="Print Group Appraisals" />Print
                Group Appraisals</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports","GroupLeaderAppraisal","HTML4.0", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Group Leader Appraisals">
                <img src="../Content/img/actions/printer.png" title="Print Group Leader Appraisals" />Print
                Group Leader Appraisals</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports","FamilyGroupLeavingSlips","PDF", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Print Family Leaving Slips">
                <img src="../Content/img/actions/hand.png" title="Print Family Leaving Slips" />Print
                Family Leaving Slips</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports","GroupPlacementTest","PDF", new Dictionary<string, object> { {"GroupId",_groupId}}) %>"
                title="Outstanding Placement Tests">
                <img src="../Content/img/actions/hand.png" title="Outstanding Placement Tests" />Outstanding
                Placement Tests</a><br />
            <br />
            <a href="<%= ReportPath("TransportReports", "Transport_GroupTransport", "PDF", new Dictionary<string, object> { { "GroupId", _groupId } } ) %>"
                title="Group Transport Report">
                <img src="../COntent/img/actions/car-taxi.png" title="Group Transport Report" />Group
                Transport Report</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports", "BusStopDetails", "PDF", new Dictionary<string, object> { { "GroupId", _groupId } } ) %>"
                title="Group Transport Report">
                <img src="../Content/img/actions/car-taxi.png" title="Private Bus Routes" />Group
                Private Bus Routes</a><br />
            <br />
            <a href="<%= ReportPath("GroupReports", "Group_QuickDetails", "PDF", new Dictionary<string, object> { { "GroupId", _groupId } } ) %>"
                title="Group Quick Details">
                <img src="../Content/img/actions/printer.png" title="Group Quick Details" />Group
                Quick Details</a><br />
            <br />
            <a target ="_blank" href="<%= ReportPath("AcademicReports", "Academic_Documents", "HTML4.0", new Dictionary<string,object> { { "GroupId", _groupId } } ) %>"
                title="Group Appraisals">
                <img src="../Content/img/tick.jpg" title="Group Appraisals" />Group Appraisals
            </a><br />
            <br />
            <a target="_blank" href="<%= ReportPath("TuitionReports", "Test_Calendar", "HTML4.0", new Dictionary<string, object> { { "Start", _Arrival }, { "End", _Departure }, { "GroupId", _groupId } } ) %>"
                title="Group Calendar">
                <img src="../Content/img/actions/table.png" title="Group Calendar" /> Group Calendar
            </a><br />
                <br />
        </fieldset>
    </div>
</asp:Content>
