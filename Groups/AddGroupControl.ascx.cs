﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;

public partial class Group_AddGroup : BaseGroupControl
{
    protected Int32 _groupId;
    protected Pace.DataAccess.Group.Group _group;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("GroupId").HasValue)
        {
            Int32 GroupId = GetValue<Int32>("GroupId");
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                _group = (from enroll in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status")
                               where enroll.GroupId == GroupId
                               select enroll).FirstOrDefault();

            }

        }
    }

    protected string GetCampusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_group != null) ? _group.CampusId : GetCampusId;

        foreach (Pace.DataAccess.Security.Campus item in BasePage.LoadCampus)
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.CampusId, item.Name, (_current.HasValue && item.CampusId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetNationalityList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_group != null && _group.Xlk_Nationality != null) ? _group.Xlk_Nationality.NationalityId : (short?)null;

        foreach (Xlk_Nationality item in LoadNationalities())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NationalityId, item.Description, (_current.HasValue && item.NationalityId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_group != null && _group.Xlk_Status != null) ? _group.Xlk_Status.StatusId : (short?)null;

        foreach (Xlk_Status item in LoadStatuses())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);

        }
        
        return sb.ToString();
    }

    protected string GetAgencyList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_group != null && _group.Agency != null) ? _group.Agency.AgencyId : (int?)null;

        foreach (Agency item in LoadAgencies())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.AgencyId, item.Name, (_current.HasValue && item.AgencyId == _current) ? "selected" : string.Empty);
        }
        
        return sb.ToString();
    }

    protected string GetGroupId
    {
        get
        {
            if (_group != null)
                return _group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

    protected string GetArrivalDate
    {
        get
        {
            if (_group != null)
                return _group.ArrivalDate.ToString("dd/MM/yyyy");

            return null;
        }
    }

    protected string GetDepartureDate
    {
        get
        {
            if (_group != null)
                return _group.DepartureDate.ToString("dd/MM/yyyy");

            return null;

        }
    }


    protected string GetBusTickets
    {
        get
        {
            if (_group != null && _group.BusTickets.HasValue && _group.BusTickets.Value == true)
                return "checked";

            return String.Empty;

        }
    }

    protected string GetDepartureInfo
    {
        get
        {
            if (_group != null)
                return _group.DepartureInfo.ToString();

            return null;

        }
    }

    protected string GetArrivalInfo
    {
        get
        {
            if (_group != null)
                return _group.ArrivalInfo.ToString();

            return null;

        }
    }

    protected string GetGroupCurfew
    {
        get
        {
            if (_group != null)
                return _group.GroupCurfew.ToString(@"hh\:mm");

            return "22:00";

        }
    }

    protected string GetGroupName
    {
        get
        {
            if (_group != null)
                return _group.GroupName;

            return null;

        }
    }

    protected string GetIsClosed
    {
        get
        {
            if (_group != null && _group.IsClosed)
                return "checked";

            return null;

        }
    }

    protected string GetLeaderMobile
    {
        get
        {
            if (_group != null)
                return _group.LeaderMobile;

            return null;

        }
    }

    protected string GetNoOfClasses
    {
        get
        {
            if (_group != null)
                return _group.NoOfClasses.ToString();

            return null;

        }
    }

    protected string GetNoOfLeaders
    {
        get
        {
            if (_group != null)
                return _group.NoOfLeaders.ToString();

            return null;

        }
    }
    protected string GetNoOfStudents
    {
        get
        {
            if (_group != null)
                return _group.NoOfStudents.ToString();

            return null;

        }
    }

}
