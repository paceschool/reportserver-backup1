﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupFamilyReservationControl.ascx.cs"
    Inherits="Group_AddGroupFamilyReservationControl" %>

<script type="text/javascript">
    $(function() {

        $("#ArrivalDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#DepartureDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function() {
            return $("#addreservedfamily");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveHosting") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addreservedfamily" name="addreservedfamily" method="post">
<div class="inputArea">
    <fieldset style="float:left">
        <input type="hidden" name="Group" id="GroupId" value="<%= GetGroupId %>" />
        <label for="familyName">
            Family</label>
        <input class="watcher" type="text" name="familyName" id="familyName" value="" />
        <label for="family">
            Family</label>
        <ul type="text" name="Family" id="FamilyId" >
            <%= BuildFamilies(false,null)%>
        </ul>
    </fieldset>
    <fieldset style="float:right">
    <ul>
         <%= BuildFamilies(true,null)%>
    </ul>
    </fieldset>
</div>
</form>
