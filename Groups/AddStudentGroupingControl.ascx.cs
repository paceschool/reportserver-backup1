﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;


public partial class Groups_StudentGroupingControl : BaseGroupControl
{
    protected Int32 _groupId;
    protected Group _group;
    protected Int32 _maxgrouping;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            if (GetValue<Int32?>("GroupId").HasValue)
            {
                _groupId = GetValue<Int32>("GroupId");
                _group = (from g in entity.Group.Include("Student")
                          where g.GroupId == _groupId
                          select g).FirstOrDefault();
            }
        }

        BuildGroupBoxes();
    }

    private void BuildGroupBoxes()
    {
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var studentQuery = (from s in entity.Student
                                where s.GroupId == _groupId && (s.GroupingBlockId.HasValue)
                                select new
                                {
                                    GroupId = s.GroupId,
                                    GroupingBlockId = s.GroupingBlockId,
                                }
                                ).Distinct();

            _maxgrouping = studentQuery.Count()+1;
            results.DataSource = studentQuery.ToList();
            results.DataBind();
        }
    }

    protected string BuildNames(object groupingId)
    {
        int _groupingId = Convert.ToInt32(groupingId);
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            List<Pace.DataAccess.Enrollment.Student> studentswithgrouping = (
                from s in entity.Student
                where s.GroupId == _groupId &&  s.GroupingBlockId == _groupingId
                select s)
                .Union(
                from s in entity.Student
                where s.GroupId == _groupId && s.GroupingBlockId == _groupingId
                select s).OrderBy(s => s.FirstName).ToList();

            StringBuilder sb = new StringBuilder();

            if (studentswithgrouping != null && studentswithgrouping.Count > 0)
            {
                foreach (Pace.DataAccess.Enrollment.Student student in studentswithgrouping)
                {
                    sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}</div>", CreateName(student.FirstName, student.SurName), student.GroupingBlockId);
                }
            }

            return sb.ToString();

        }
    }


    protected string GetGroupId
    {
        get
        {
            if (_group != null)
                return _group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

}