﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using Pace.DataAccess.Group;

public partial class Groups_AddGroupExcursionControl : BaseGroupControl
{
    protected Int32 _groupId;
    protected GroupExcursion _excursion;
    protected Dictionary<string, int> qtys;

    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            if (GetValue<Int32?>("GroupExcursionId") .HasValue)
            {
                Int32 groupExcursionId = GetValue<Int32>("GroupExcursionId");

                _excursion = (from x in entity.GroupExcursion.Include("Group").Include("Excursion") where x.GroupExcursionId == groupExcursionId select x).FirstOrDefault();
            }
            if (GetValue<Int32?>("GroupId").HasValue)
            {
                int _groupId = GetValue<Int32>("GroupId");

                qtys = (from s in entity.Student
                        where s.Group.GroupId == _groupId
                        group s by s.Xlk_StudentType.Description into student_Group
                        select new { Key = student_Group.Key, Value = student_Group.Count() }).AsEnumerable().ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            }
        }

    }

    protected string GetExcursions()
    {

        StringBuilder sb = new StringBuilder();

        int? _current = (_excursion != null && _excursion.Excursion != null) ? _excursion.Excursion.ExcursionId : (int?)null;


        foreach (Pace.DataAccess.Excursion.Excursion item in BaseExcursionPage.LoadExcursions())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExcursionId, item.Title, (_current.HasValue && item.ExcursionId == _current) ? "selected" : string.Empty);

        }

        return sb.ToString();
    }

    protected string GetGroupExcursionId
    {
        get
        {
            if (_excursion != null)
                return _excursion.GroupExcursionId.ToString();

            if (Parameters.ContainsKey("GroupExcursionId"))
                return Parameters["GroupExcursionId"].ToString();

            return "0";
        }
    }

    protected string GetGroupId
    {
        get
        {
            if (_excursion != null)
                return _excursion.Group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

    protected string GetIsDeleted
    {
        get
        {
            if (_excursion != null && _excursion.IsDeleted.HasValue && _excursion.IsDeleted.Value)
                return  "checked";

            return "0";
        }
    }

    protected string GetQtyGuides
    {
        get
        {
            if (_excursion != null)
                return _excursion.QtyGuides.ToString();

            return "0";
        }
    }

    protected string GetQtyLeaders
    {
        get
        {
            if (_excursion != null)
                return _excursion.QtyLeaders.ToString();

            if (qtys != null && qtys.ContainsKey("Group Leader"))
                return qtys["Group Leader"].ToString();

            return "";
        }
    }

    protected string GetQtyStudents
    {
        get
        {
            if (_excursion != null)
                return _excursion.QtyStudents.ToString();

            if (qtys != null && qtys.ContainsKey("Student"))
                return qtys["Student"].ToString();

            return "";
        }
    }


    protected string GetTransportRequired
    {
        get
        {
            if (_excursion != null && _excursion.TransportRequired.HasValue && _excursion.TransportRequired.Value)
            {
                return "checked=\"true\"";
            }

            return string.Empty;
        }
    }

    protected string GetTitle
    {
        get
        {
            if (_excursion != null && _excursion.TransportRequired.HasValue)
            {
                return "true";
            }

            return "false";
        }
    }

    protected string GetComment
    {
        get
        {
            if (_excursion != null)
                return _excursion.Comment;

            return "";
        }
    }

    protected string GetPreferredDate
    {
        get
        {
            if (_excursion != null)
                return _excursion.PreferredDate.ToString("dd/MM/yy");

            return "";
        }
    }

    protected string GetPreferredTime
    {
        get
        {
            if (_excursion != null && _excursion.PreferredTime.HasValue)
                return _excursion.PreferredTime.Value.ToString();

            return "";
        }
    }
}
