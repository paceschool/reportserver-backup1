﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupStudentControl.ascx.cs"
    Inherits="Group_AddNote" %>

<script type="text/javascript">
    $(function() {

        $("#DateOfBirth").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function() {
            return $("#addgroupnote");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveGroupStudent") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addgroupnote" name="addgroupnote" method="post" action="ViewGroupPage.aspx/SaveGroupStudent">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="GroupId" value="<%= GetValue<Int32>("GroupId") %>" />
        <label>
            Student First Name</label>
        <input type="text" id="FirstName" />
        <label>
            Student Surname</label>
        <input type="text" id="SurName" />
        <label>
            Gender
        </label>
        <select name="Xlk_Gender" id="GenderId">
            <%= GetGenderList()%>
        </select>
        <label>
            Date of Birth
        </label>
        <input type="datetext" id="DateOfBirth" />
        <label>
            Student Nationality</label>
        <select name="Xlk_Nationality" id="NationalityId">
            <%= GetNationalityList()%>
        </select>
        <label>
            Member Type</label>
        <select name="Xlk_StudentType" id="StudentTypeId">
            <%= GetStudentTypeList()%>
        </select>
    </fieldset>
</div>
</form>
