﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AssignAccommodationPage.aspx.cs" Inherits="Accommodation_AssignAccommodationPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function() {

            var Id;
   

            loadFamilies = function(id) {
                var d = $("#frmfilter").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "GroupHostingPage.aspx/LoadFamilies",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ id: id, details: d }),
                    success: function(msg) {
                        if (msg.d.length > 0) {
                            $('#familieslist').html(msg.d);
                            $(".droppable").droppable({
                                accept: ".draggable",
                                activeClass: "ui-state-hover",
                                hoverClass: "ui-state-active",
                                drop: function(event, ui) {
                                    $(this).addClass("ui-state-highlight");
                                    assignStudentToFamily(id, this.id, ui.draggable.attr('id'))
                                }
                            });
                        }
                        else
                            alert('There was an issue!');
                    }
                });
            };

            assignStudentToFamily = function(groupId, familyId, studentId) {
                $.ajax({
                    type: "POST",
                    url: "GroupHostingPage.aspx/AssignStudentToFamily",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ id: groupId, familyid: familyId, studentid: studentId }),
                    success: function(msg) {
                        if (msg.d) {
                            loadgroupstudents(Id);
                            loadFamilies(id);
                        }
                        else
                            alert('There was an issue!');
                    }
                });
            };

            loadgroupstudents = function(id) {
                $.ajax({
                    type: "POST",
                    url: "GroupHostingPage.aspx/LoadGroupStudents",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{id:" + id + "}",
                    success: function(msg) {
                        if (msg.d.length > 1) {
                            $('#students').html(msg.d.Payload[1]);
                            $('#group').html(msg.d.Payload[0]);
                            $(".draggable").draggable({
                                helper: "clone",
                                revert: "invalid",
                                containment: "#dragdroppanel",
                                scroll: false
                            });
                            loadFamilies(id);
                        }
                        else
                            alert(msg.d[0]);
                    }
                });
            };



            $("ul, li").disableSelection();

            $('#showFilter').click(function() {
                $('#familiesfilter').toggle("slow");
            });


            $('#btnfilter').click(function() {
                loadFamilies(Id);
            });
            
            $("#groups").change(function() {
                Id = $("option:selected", this).attr("value");
                loadgroupstudents(Id);
            });

            if (<%= GetGroupId() %> > 0)
                $("#groups option[value='<%= GetGroupId() %>']").attr('selected', 'selected').trigger('change');
            else
                $("#groups option:first").attr('selected', 'selected').trigger('change');

        });
    </script>

    <style type="text/css">
        #students ul
        {
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 10px;
        }
        #students li
        {
            margin: 5px;
            padding: 5px;
            min-width: 150px;
        }
        #students
        {
            background-color: #fff;
            width: 20%;
            float: left;
            border: 1px solid #CCCCCC;
            height: 100%;
            overflow: visible;
            margin-right: 10px;
        }
        #families
        {
            background-color: #fff;
            overflow: scroll;
            width: 75%;
            float: left;
            border: 1px solid #CCCCCC;
            max-height: 500px;
            min-height: 400px;
        }

       #families .daysavailable
        {
            margin: 0 0 5px 0;
            padding: 0 0 2px 0;
            list-style-type: none;
        }
        #families .daysavailable li
        {
            width: 30px;
            height: 20px;
            float: left;
            position: relative;
        }
        #families .daysavailable a
        {
            display: block;
            position: relative;
            border: 1px solid #CCCCCC;
        }
        #families .students a
        {
            background-color: #FFFF99;
        }
        #families .nostudents a
        {
            background-color: #E0F9D9;
        }
        #familieslist
        {
            height: 100%;
        }
        .assigned
        {
            background: repeat-x scroll 50% 50% #FFFF99;
            border: 1px solid #AED0EA;
            color: #2779AA;
            font-weight: bold;
        }
        #dragdroppanel
        {
            background-color: #D6E5F4;
        }
        dl
        {
            margin-bottom: 50px;
        }
        dl dt
        {
            background: #5f9be3;
            color: #fff;
            float: left;
            font-weight: bold;
            margin-right: 10px;
            padding: 5px;
            width: 100px;
        }
        dl dd
        {
            margin: 2px 0;
            padding: 5px 0;
        }
        .chartlist
        {
            border-top: 1px solid #EEEEEE;
            float: left;
            width: 100%;
        }
        .chartlist li
        {
            border-bottom: 1px solid #EEEEEE;
            display: block;
            position: relative;
        }
        .chartlist li a
        {
            display: block;
            padding: 0.4em 4.5em 0.4em 0.5em;
            position: relative;
            z-index: 2;
        }
        .chartlist .count
        {
            color: #999999;
            display: block;
            font-size: 0.875em;
            font-weight: bold;
            line-height: 2em;
            margin: 0 0.3em;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
        }
        .chartlist .index
        {
            background: none repeat scroll 0 0 #B8E4F5;
            display: block;
            height: 100%;
            left: 0;
            line-height: 2em;
            overflow: hidden;
            position: absolute;
            text-indent: -9999px;
            top: 0;
        }
        .chartlist .index2
        {
            background: none repeat scroll 0 0 #ccc;
            display: block;
            height: 100%;
            left: 0;
            line-height: 2em;
            overflow: hidden;
            position: absolute;
            text-indent: -9999px;
            top: 0;
        }
        .chartlist li:hover
        {
            background: none repeat scroll 0 0 #EFEFEF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Assign Accommodation Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
         <div id="Div1">
            <table id="Table1">
                <tbody>
                    <tr>
                        <td>
                            Browse By
                        </td>
                        <td>
                            <select id="Select1" name="groups">
                                <%= LoadGroupList() %>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="resultscontainer">
            <div id="Div2">
                <table class="box-table-a">
                    <thead>
                        <tr>
                            <th scope="col">
                                <label>
                                    Group ID
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Name
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Arrival Date
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    Departure Date
                                </label>
                            </th>
                            <th scope="col">
                                <label>
                                    #Weeks
                                </label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label1" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Label2" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Label3" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Label4" runat="server">
                                </asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="Label5" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="Div3" class="resultscontainer">
            <span>
                <h1>
                    Students</h1>
                <div id="Div4">
                
                </div>
            </span>
            <div id="Div5">
                <label id="Label6">
                    Filter Applied:</label><span style="float: right;"><a id="A1">
                        <img src="../Content/img/actions/add.png" /></a></span>
                <div id="Div6" style="display: none;">
                    <div class="inputArea">
                        <fieldset>
                            <label for="excludenationality">
                                Exclude Group Nationality</label>
                            <input type="checkbox" id="excludenationality" value="true" />
                            <label for="familyname">
                                Family Name</label>
                            <input type="text" name="familyname" id="Text1" value="" />
                            <label for="statusid">
                                Status
                            </label>
                            <select name="statusid" id="Select2">
                                <%= LoadStatusList()%>
                            </select>
                        </fieldset>
                        <fieldset>
                            <label for="zoneid">
                                Zone
                            </label>
                            <select name="zoneid" id="Select3">
                                <%--<%= LoadZoneList()%>--%>
                            </select>
                            <label for="roomtypeid">
                                Accommodation Type
                            </label>
                            <select name="roomtypeid" id="Select4">
                                <%= GetRoomTypeList()%>
                            </select>
                            <label>
                                Ensuite
                            </label>
                            <input type="checkbox" id="ensuite" value="true" />
                            <input type="button" id="Button1" value="Filter" />
                        </fieldset>
                    </div>
                </div>
                <div id="Div7">
                </div>
            </div>
        </div>
</asp:Content>

