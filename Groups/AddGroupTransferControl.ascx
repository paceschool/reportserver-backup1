﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupTransferControl.ascx.cs"
    Inherits="Group_AddTransfer" %>
<script type="text/javascript">
    $(function () {

        $("#Date").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function () {
            return $("#addgrouptransfer");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveGroupTransfer") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addgrouptransfer" name="addgrouptransfer" method="post" action="ViewGroupPage.aspx/SaveGroupTransfer">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Group" id="GroupId" value="<%= GetGroupId %>" />
        <input type="hidden" id="GroupTransferId" value="<%= GetGroupTransferId %>" />
        <input type="hidden" id="TransportBookingId" value="<%= GetTransportBookingId %>" />
        <label>
            Location
        </label>
        <select type="text" name="Xlk_Location" id="LocationId">
            <%= GetLocationList() %>
        </select>
        <label>
            Type
        </label>
        <select type="text" name="Xlk_TransferType" id="TransferTypeId">
            <%= GetTransferTypeList() %>
        </select>
        <label>
            Completed By
        </label>
        <select type="text" name="Xlk_BusinessEntity" id="EntityId">
            <%= GetBusinessEntityList()%>
        </select>
        <label for="Time">
            Qty of Passengers</label>
        <input type="text" name="NumberOfPassengers" id="NumberOfPassengers" value="<%= GetNoOfPassengers %>" />
        <label for="Date">
            Date</label>
        <input type="datetext" name="Date" id="Date" value="<%= GetDate %>" />
        <label for="FlightNumber">
            FlightNumber</label>
        <input type="text" name="FlightNumber" id="FlightNumber" value="<%= GetFlight %>" />
        <label for="Time">
            Time</label>
        <input type="text" name="Time" id="Time" value="<%= GetTime %>" />
        <label>
            Comment
        </label>
        <input type="text" id="Comment" value="<%= GetComment %>" />
    </fieldset>
    <fieldset>
        <label>
            Family Collection Time
        </label>
        <input type="text" id="FamilyCollectionTime" value="<%= GetFamilyCollectionTime %>" />
        <label>
            Family Collection Location
        </label>
        <br />
        <label>
            <i>Choose from the drop down list</i>
        </label>
        <select type="text" name="CollectionDropDown" id="CollectionDropDown" onchange="$('#FamilyCollectionLocation').val($('#CollectionDropDown option:selected').text());">
            <%= GetDropDown1() %>
        </select>
        <label>
            <i>Or enter collection location here</i>
        </label>
        <input type="text" name="FamilyCollectionLocation" id="FamilyCollectionLocation"
            value="<%= GetCollectionLocation %>" />
        <br />
        <label>
            Family Drop Off Time
        </label>
        <input type="text" name="FamilyDropOffTime" id="FamilyDropOffTime" value="<%= GetFamilyDropOffTime %>" />
        <label>
            Family Drop Off Location
        </label>
        <br />
        <label>
            <i>Choose from the drop down list</i>
        </label>
        <select type="text" name="DropOffDropDown" id="DropOffDropDown" onchange="$('#FamilyDropOffLocation').val($('#DropOffDropDown option:selected').text());">
            <%= GetDropDown2() %>
        </select>
        <label>
            <i>Or enter collection location here</i>
        </label>
        <input type="text" name="FamilyDropOffTime" id="FamilyDropOffLocation" value="<%= GetDropOffLocation %>" />
    </fieldset>
</div>
</form>
