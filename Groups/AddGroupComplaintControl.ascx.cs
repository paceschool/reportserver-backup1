﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Group_AddComplaint : BaseGroupControl
{
    protected Int32 _groupId;
    protected GroupComplaint _complaint;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            if (GetValue<Int32?>("GroupComplaintId").HasValue)
            {
                Int32 groupcomplaintId = GetValue<Int32>("GroupComplaintId");
                _complaint = (from comp in entity.GroupComplaint.Include("Group").Include("Users").Include("Xlk_Severity")
                              where comp.GroupComplaintId == groupcomplaintId
                              select comp).FirstOrDefault();
            }
        }
    }

    protected string GetGroupId
    {
        get
        {
            if (_complaint != null)
                return _complaint.Group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

    protected string GetGroupComplaintId
    {
        get
        {
            if (_complaint != null)
                return _complaint.GroupComplaintId.ToString();

            if (Parameters.ContainsKey("GroupComplaintId"))
                return Parameters["GroupComplaintId"].ToString();

            return "0";
        }
    }

    protected string GetComplaintTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_complaint != null && _complaint.Xlk_ComplaintType != null) ? _complaint.Xlk_ComplaintType.ComplaintTypeId : (byte?)null;

        foreach (Xlk_ComplaintType item in LoadComplaintTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ComplaintTypeId, item.Description, (_current.HasValue && item.ComplaintTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetSeverityList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_complaint != null && _complaint.Xlk_Severity != null) ? _complaint.Xlk_Severity.SeverityId : (byte?)null;

        foreach (Xlk_Severity item in LoadSeverity())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.SeverityId, item.Description, (_current.HasValue && item.SeverityId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetComplaintUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_complaint != null && _complaint.Users != null) ? _complaint.Users.UserId : (Int32?)null;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetComplaint
    {
        get
        {
            if (_complaint != null)
                return _complaint.Complaint.ToString();

            return null;
        }
    }

    protected string GetAction
    {
        get
        {
            if (_complaint != null)
                if (_complaint.ActionNeeded == true)
                    return "checked";

            return string.Empty;
        }
    }
}
