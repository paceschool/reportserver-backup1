﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupInvoiceControl.ascx.cs" 
    Inherits="Groups_AddGroupInvoiceControl" %>

<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addgroupsageref");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveGroupInvoice") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addgroupsageref" name="addgroupsageref" method="post" action="ViewGroupPage.aspx/SaveGroupInvoice">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Group" id="GroupId" value="<%=GetGroupId %>" />
        <input type="hidden" id="GroupInvoiceId" value="<%= GetGroupInvoiceId %>" />
                <label>
            Finance Reference
        </label>
        <input type="text" id="SageInvoiceRef"  value="<%= GetSageRef %>" />
        <label>
            Type
        </label>
        <select type="text" name="Xlk_SageDataType" id="SageDataTypeId">
            <%= GetDataTypeList() %>
        </select>
    </fieldset>
</div>
</form>