﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using Pace.DataAccess.Group;



public partial class Finance_ReceiptInvoiceControl : BaseEnrollmentControl
{
    protected Int32? _groupId;
    protected Int32? _invoiceReceiptId;
    protected InvoiceReceipt _receipt = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("InvoiceReceiptId").HasValue)
        {
            _invoiceReceiptId = GetValue<Int32>("InvoiceReceiptId");

            using (GroupEntities entity = new GroupEntities())
            {
                _receipt = (from i in entity.InvoiceReceipts.Include("Group").Include("Xlk_PaymentMethod")
                                     where i.InvoiceReceiptId == _invoiceReceiptId
                                     select i).FirstOrDefault();
            }
        }

        if (GetValue<Int32?>("GroupId").HasValue)
            _groupId = GetValue<Int32>("GroupId");
    }

    protected string IsEditMode
    {
        get
        {
            if (_receipt == null)
                return String.Empty;

            return "disabled";
        }
    }

    protected string GetInvoiceReceiptId
    {
        get
        {
            if (_receipt != null)
                return _receipt.InvoiceReceiptId.ToString();

            if (_invoiceReceiptId.HasValue)
                return _invoiceReceiptId.ToString();

            if (Parameters.ContainsKey("InvoiceReceiptId"))
                return Parameters["InvoiceReceiptId"].ToString();

            return "0";
        }
    }

    protected string GetGroupId
    {
        get
        {
            if (_receipt != null)
                return   _receipt.Group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

    protected string GetInvoices
    {
        get
        {
            using (Pace.DataAccess.Sage.SageEntities entity = new Pace.DataAccess.Sage.SageEntities())
            {
                var Groupinvoices = ((from  s in entity.GroupInvoices
                                     where s.GroupId == _groupId.Value
                                       join sd in entity.Invoices on s.SageInvoiceRef equals sd.InvoiceNumber into lg
                                       from l in lg.DefaultIfEmpty()
                                      select new { s.SageInvoiceRef, l.NetAmount }).ToList().Select(x => new { value = x.SageInvoiceRef, label = (x.NetAmount.HasValue) ? string.Format("{0} for {1}", x.SageInvoiceRef, x.NetAmount.Value.ToString("C")) : x.SageInvoiceRef.ToString(), category = "Group Invoice" })).ToList();

                return new JavaScriptSerializer().Serialize(Groupinvoices);
            }
        }
    }

    protected string GetInvoiceNumber
    {
        get
        {
            if (_receipt != null)
                return _receipt.SageInvoiceRef.ToString();

            return "";
        }
    }

    protected string GetAmountPaid
    {
        get
        {
            if (_receipt != null)
                return _receipt.AmountPaid.ToString();

            return "0.0";
        }
    }

    protected string GetComment
    {
        get
        {
            if (_receipt != null)
                return _receipt.Comment;

            return "";
        }
    }

    protected string GetPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_receipt != null && _receipt.Xlk_PaymentMethod != null) ? _receipt.Xlk_PaymentMethod.PaymentMethodId : (byte?)null;


        foreach (Pace.DataAccess.Enrollment.Xlk_PaymentMethod item in LoadPaymentMethods())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.PaymentMethodId, item.Description, (_current.HasValue && item.PaymentMethodId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}