﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Enrollment =Pace.DataAccess.Enrollment;
using System.Globalization;
using System.Threading;
using System.Data;
using Group = Pace.DataAccess.Group;
using System.Reflection;
using System.Data.Entity.Core;


public partial class Groups_ParseStudentListPage : BaseGroupPage
{
    protected string _action = "raw";
    protected string _rawtext;
    protected List<string> errorList;
    protected Int32 CampusId = 0;

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        _rawtext = rawtext.Value;

        if (!Page.IsPostBack)
        {
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                CampusId = GetCampusId;
                var GroupSearchQuery = from g in entity.Group
                                       where g.ArrivalDate >= DateTime.Now && g.Enrollment.Count > 0 && g.CampusId == CampusId
                                       orderby g.ArrivalDate
                                       select g;

                listgroups.DataSource = GroupSearchQuery.ToList();
                listgroups.DataBind();
            }
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void submit_Click(object sender, EventArgs e)
    {
        using (Enrollment.EnrollmentsEntities entity = new Enrollment.EnrollmentsEntities())
        {
            SaveGroupStudent(entity,GetStudentsList(entity));
        }
    }

    protected void parsed_Click(object sender, EventArgs e)
    {
        _action = "";
        LoadStudentsList();
       
    }
    protected void raw_Click(object sender, EventArgs e)
    {
        _action = "raw";
    }
    #endregion

    #region Private Methods

    protected void LoadStudentsList()
    {
        try
        {
            results.DataSource = GetStudentsList(BaseEnrollmentPage.CreateEntity);
            results.DataBind();
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected List<Enrollment::Student> GetStudentsList(Enrollment.EnrollmentsEntities entity)
    {
        List<Enrollment::Student> students = new List<Enrollment::Student>();

        int groupId = Convert.ToInt32(listgroups.SelectedItem.Value);
        Group::Group group = BaseGroupPage.GetGroup(groupId);
        errorList = new List<string>();
        int counter = 0;

        try
        {
            if (group == null)
                errorList.Add(string.Format("The group could not be found, have you selected a group from the dropdown!"));
                
            if (!string.IsNullOrEmpty(rawtext.Value))
            {
                string[] lines = rawtext.Value.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                Enrollment::Student student = null;
                try
                {
                    foreach (string line in lines)
                    {
                        counter++;
                        student = new Enrollment::Student();

                        string[] details = line.Split(new string[] { "\t" }, StringSplitOptions.None);
                        student.Xlk_StudentType = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_StudentType>(entity).FirstOrDefault(x => x.StudentTypeId == ((details[0].ToUpper() == "TEACHER" || details[0].ToUpper() == "LEADER") ? (byte)2 : (byte)1));
                        
                        if (!string.IsNullOrEmpty(details[1]))
                            student.FirstName = details[1].ToTitleCase();
                        else
                            new Exception("The first name is blank");

                        if (string.IsNullOrEmpty(details[2]))
                            new Exception("The second name is blank");

                        student.SurName = string.Format("{0} {1}", details[2].Trim(), details[3].Trim()).ToTitleCase();

                        if (!string.IsNullOrEmpty(details[4]))
                            student.DateOfBirth = DateTime.Parse(details[4]);

                        if (details.Count() > 6 && !string.IsNullOrEmpty(details[6]))
                            student.ArrivalDate = DateTime.Parse(details[6]);
                        else
                            student.ArrivalDate = group.ArrivalDate;

                        if (details.Count() > 7 && !string.IsNullOrEmpty(details[7]))
                            student.DepartureDate = DateTime.Parse(details[7]);
                        else
                            student.DepartureDate = group.DepartureDate;

                        if (details.Count() > 8 && !string.IsNullOrEmpty(details[8]))
                        {
                            Pace.DataAccess.Enrollment.StudentNote note = new Enrollment.StudentNote();
                            note.Xlk_NoteTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_NoteType", "NoteTypeId", (byte)1);
                            note.Note = details[8].Trim();
                            note.DateCreated = DateTime.Now;
                            note.UsersReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", 0);
                            student.StudentNotes.Add(note);
                        }

                        if (details.Count() > 9 && !string.IsNullOrEmpty(details[9]))
                        {
                            Pace.DataAccess.Enrollment.StudentNote note = new Enrollment.StudentNote();
                            note.Xlk_NoteTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_NoteType", "NoteTypeId", (byte)4);
                            note.Note = details[9].Trim();
                            note.DateCreated = DateTime.Now;
                            note.UsersReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", 0);
                            student.StudentNotes.Add(note);
                        }

                        student.CampusId = GetCampusId;
                        student.GroupId = Convert.ToInt32(listgroups.SelectedItem.Value);
                        student.Xlk_Gender = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_Gender>(entity).FirstOrDefault(x => x.GenderId == ((details[5].ToUpper() == "F" || details[5].ToUpper() == "FEMALE") ? 2 : 1));
                        student.Xlk_Status = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_Status>(entity).FirstOrDefault(x => x.StatusId == ((group.Xlk_Status.StatusId == 1) ? (byte)0 : (byte)1));
                        student.Xlk_Nationality = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_Nationality>(entity).FirstOrDefault(x => x.NationalityId == group.Xlk_Nationality.NationalityId);
                        student.Agency = BaseEnrollmentPage.GetFromStore<Enrollment::Agency>(entity).FirstOrDefault(x => x.AgencyId == group.Agency.AgencyId);


                        students.Add(student);
                    }
                }
                catch (Exception ex)
                {
                    errorList.Add(string.Format("{4} {0} {1} at position {2} has error {3}", student.FirstName, student.SurName, counter, ex.Message, student.Xlk_StudentType.Description));
                }

            }
            return students;
        }
        catch (Exception ex)
        {
            throw;
        }

    }

    private static string FilterWords(string str)
    {
        var upper = str.Split(' ')
                    .Where(s => String.Equals(s, s.ToUpper(),
                                StringComparison.Ordinal));

        return string.Join(" ", upper.ToArray());
    }

    protected string LoadRawText()
    {
        return _rawtext.ToString();
    }

    public int SaveGroupStudent(Pace.DataAccess.Enrollment.EnrollmentsEntities eentity,List<Pace.DataAccess.Enrollment.Student> students)
    {
        int i = 0;
        int groupId = Convert.ToInt32(listgroups.SelectedItem.Value);
        int qty;

        try
        {
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                var studentsQuery = (from s in entity.DeleteStudents(groupId) select s).ToList();

                if (studentsQuery.Count == 0)
                {
                    foreach (Enrollment::Student student in students)
                    {
                        eentity.AddToStudent(student);
                    }
                }

               if (eentity.SaveChanges()>0)
                   qty = entity.CreateGroupEnrollments(groupId, null);

            }
            return i;
        }
        catch (Exception)
        {
            return 0;
        }
    }

    #endregion
}
