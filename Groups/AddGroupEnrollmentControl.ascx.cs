﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;

public partial class Group_AddEnrollment : BaseEnrollmentControl
{
    protected Int32 _familyId;
    protected Pace.DataAccess.Group.Enrollment _enrollment;
    protected Pace.DataAccess.Group.Group _group;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("EnrollmentId").HasValue)
        {
            Int32 enrollmentId = GetValue<Int32>("EnrollmentId");
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                _enrollment = (from enroll in entity.Enrollment.Include("Xlk_ProgrammeType").Include("Xlk_CourseType").Include("Xlk_LessonBlock").Include("Group")
                               where enroll.EnrollmentId == enrollmentId
                               select enroll).FirstOrDefault();

            }

        }
        if (GetValue<Int32?>("GroupId").HasValue)
        {
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                Int32 groupId = GetValue<Int32>("GroupId");
                _group = (from host in entity.Group
                            where host.GroupId == groupId
                            select host).FirstOrDefault();
            }

        }
    }

    protected string GetProgrammeTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_enrollment != null && _enrollment.Xlk_ProgrammeType != null) ? _enrollment.Xlk_ProgrammeType.ProgrammeTypeId : (byte?)null;

        foreach (Xlk_ProgrammeType item in LoadProgrammeTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ProgrammeTypeId, item.Description, (_current.HasValue && item.ProgrammeTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetCourseTypeList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_enrollment != null && _enrollment.Xlk_CourseType != null) ? _enrollment.Xlk_CourseType.CourseTypeId : (short?)null;

        if (_enrollment != null && _enrollment.Xlk_ProgrammeType != null)
        {
            foreach (Xlk_CourseType item in LoadCourseTypes().Where(x => x.Xlk_ProgrammeType.Any(p => p.ProgrammeTypeId == _enrollment.Xlk_ProgrammeType.ProgrammeTypeId)))
            {
                sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.CourseTypeId, item.Description, (_current.HasValue && item.CourseTypeId == _current) ? "selected" : string.Empty);

            }
        }
        else
        {
            foreach (Xlk_CourseType item in LoadCourseTypes())
            {
                sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.CourseTypeId, item.Description, (_current.HasValue && item.CourseTypeId == _current) ? "selected" : string.Empty);
            }
        }
        return sb.ToString();
    }

    protected string GetLessonBlockTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_enrollment != null && _enrollment.Xlk_LessonBlock != null) ? _enrollment.Xlk_LessonBlock.LessonBlockId : (byte?)null;

        if (_enrollment != null && _enrollment.Xlk_CourseType != null)
        {
            foreach (Xlk_LessonBlock item in LoadLessonBlocks().Where(x => x.Xlk_CourseType.Any(p => p.CourseTypeId == _enrollment.Xlk_CourseType.CourseTypeId)))
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LessonBlockId, item.Description, (_current.HasValue && item.LessonBlockId == _current) ? "selected" : string.Empty);

            }
        }
        else
        {
            foreach (Xlk_LessonBlock item in LoadLessonBlocks())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LessonBlockId, item.Description, (_current.HasValue && item.LessonBlockId == _current) ? "selected" : string.Empty);
            }
        }
        return sb.ToString();

    }

    protected string GetEnrollmentId
    {
        get
        {
            if (_enrollment != null)
                return _enrollment.EnrollmentId.ToString();

            if (Parameters.ContainsKey("GroupExcursionId"))
                return Parameters["GroupExcursionId"].ToString();

            return "0";
        }
    }

    protected string GetStartDate
    {
        get
        {
            if (_enrollment != null)
                return _enrollment.StartDate.ToString("dd/MM/yyyy");

            if (_group != null)
                return _group.ArrivalDate.ToString("dd/MM/yy");


            return null;
        }
    }

    protected string GetEndDate
    {
        get
        {
            if (_enrollment != null && _enrollment.EndDate.HasValue)
                return _enrollment.EndDate.Value.ToString("dd/MM/yyyy");

            if (_group != null)
                return _group.DepartureDate.ToString("dd/MM/yy");


            return null;

        }
    }
    protected string GetGroupId
    {

        get
        {
            if (_enrollment != null && _enrollment.Group != null)
                return _enrollment.Group.GroupId.ToString();
            if (GetValue<Int32?>("GroupId").HasValue)
                return GetValue<Int32>("GroupId").ToString();
            return null;
        }
    }
    protected int? GetMorningLessonDays
    {
        get
        {
            if (_enrollment != null && _enrollment.MorningLessonDays.HasValue)
                return _enrollment.MorningLessonDays.Value;

            return null;

        }
    }
    protected int? GetAfternoonLessonDays
    {
        get
        {
            if (_enrollment != null && _enrollment.AfternoonLessonDays.HasValue)
                return _enrollment.AfternoonLessonDays.Value;

            return null;

        }
    }
    protected int? GetEveningLessonDays
    {
        get
        {
            if (_enrollment != null && _enrollment.EveningLessonDays.HasValue)
                return _enrollment.EveningLessonDays.Value;

            return null;

        }
    }
}
