﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using Pace.DataAccess.Group;
using System.Web.UI;
using System.Data.Entity.Core;

public partial class ManageGroupPage : BaseGroupPage
{
    protected Int32? _groupId;
    protected string _errorMessage = "All form fields are required";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["GroupId"]))
            _groupId = Convert.ToInt32(Request["GroupId"]);

        if (!Page.IsPostBack)
        {
            newnationality.DataSource = this.LoadNationalities();
            newnationality.DataBind();

            newagent.DataSource = this.LoadAgents();
            newagent.DataBind();

            newstatus.DataSource = this.LoadStatus();
            newstatus.DataBind();

            listcampus.DataSource = BasePage.LoadCampus;
            listcampus.DataBind();
            listcampus.Items.FindByValue(GetCampusId.ToString()).Selected = true;

            if (_groupId.HasValue)
            {
                DisplayGroup(LoadGroup(new GroupEntities(),_groupId.Value));
            }
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    #region Methods

    protected void Click_ClearFields(object sender, EventArgs e)// Clear all fields
    {
        GroupName.Text = ""; newnationality.SelectedIndex = 0; newarrdate.Text = "";
        newdepdate.Text = ""; newagent.SelectedIndex = 0; newstatus.SelectedIndex = 0;
        chkiscolsed.Checked = false; errorcontainer.Visible = false;
        newgroupleaders.Text = ""; newgroupstudents.Text = ""; arrivalinfo.InnerText = ""; departureinfo.InnerText = "";
    }

    protected void Click_Save(object sender, EventArgs e)// Save the record
    {
        string _message = ""; errorcontainer.Visible = false;

        if (!string.IsNullOrEmpty(GroupName.Text) && !string.IsNullOrEmpty(newarrdate.Text) && !string.IsNullOrEmpty(newdepdate.Text)
            && newnationality.SelectedIndex != 0 && newagent.SelectedIndex != 0)
        {
            using (GroupEntities entity = new GroupEntities())
            {
                Group _group = GetGroup(entity);

                _group.GroupName = GroupName.Text;
                _group.Xlk_NationalityReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
                _group.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
                _group.DepartureDate = Convert.ToDateTime(newdepdate.Text);
                _group.AgencyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
                _group.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));
                _group.IsClosed = chkiscolsed.Checked;
                _group.LeaderMobile = leadersmobile.Text;
                _group.ArrivalInfo = arrivalinfo.InnerText;
                _group.DepartureInfo = departureinfo.InnerText;
                _group.CampusId = Convert.ToInt16(listcampus.SelectedItem.Value);

                if (!string.IsNullOrEmpty(groupcurfew.Text))
                    _group.GroupCurfew = TimeSpan.Parse(groupcurfew.Text);
                else
                    _group.GroupCurfew = new TimeSpan(10,0,0);

                if (chkiscolsed.Checked == true)
                {
                    if (newgroupclasses != null && newgroupclasses.Text != "")
                        _group.NoOfClasses = Convert.ToInt16(newgroupclasses.Text);
                    else
                    {
                        Int16 grcl = Convert.ToInt16(Convert.ToInt16(newgroupstudents.Text) / 15);
                        _group.NoOfClasses = grcl;
                    }
                }

                _group.NoOfStudents = Convert.ToInt16(newgroupstudents.Text);
                _group.NoOfLeaders = Convert.ToInt16(newgroupleaders.Text);
                _group.BusTickets = ticketsassigned.Checked;

                SaveGroup(entity,_group);
            }
        }
        else
        {
            _message = "Not all fields are complete. Please check.";
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }
    }

    private Group LoadGroup(GroupEntities entity, Int32 GroupId)// Load Group if passed from GroupPage
    {
        IQueryable<Group> groupQuery = from h in entity.Group.Include("Xlk_Nationality")
                                       where h.GroupId == GroupId
                                       select h;
        if (groupQuery.ToList().Count() > 0)
            return groupQuery.ToList().First();

        return null;
    }

    private void DisplayGroup(Group group)// Display loaded Group details
    {
        if (group != null)
        {
            GroupName.Text = group.GroupName;
            newnationality.Items.FindByValue(group.Xlk_Nationality.NationalityId.ToString()).Selected = true;
            newarrdate.Text = group.ArrivalDate.ToString("dd/MM/yyyy");
            newdepdate.Text = group.DepartureDate.ToString("dd/MM/yyyy");
            newagent.Items.FindByValue(group.Agency.AgencyId.ToString()).Selected = true;
            newstatus.Items.FindByValue(group.Xlk_Status.StatusId.ToString()).Selected = true;
            chkiscolsed.Checked = group.IsClosed;
            leadersmobile.Text = group.LeaderMobile;
            arrivalinfo.InnerText = group.ArrivalInfo;
            departureinfo.InnerText = group.DepartureInfo;
            listcampus.Items.FindByValue(group.CampusId.ToString()).Selected = true;
        }
    }

    private Group GetGroup(GroupEntities entity)// Load Group or ceate new Group
    {
        if (_groupId.HasValue)
            return LoadGroup(entity,_groupId.Value);

        return new Group();
    }

    private void SaveGroup(GroupEntities entity,Group group)// Final Save stage & Redirect
    {
        if (!_groupId.HasValue)
            entity.AddToGroup(group);

        if (entity.SaveChanges() > 0)
            Response.Redirect(string.Format("ViewGroupPage.aspx?GroupId=" + group.GroupId));
    }

    protected void Text_CheckGroupName(object sender, EventArgs e)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            var lookupQuery = from t in entity.Group
                              select t;

            foreach (var g in lookupQuery)
            {
                if (g.GroupName == GroupName.Text)
                {
                    Alert.Show("A group with this name already exists. Are you sure you want to create another group with this name?");

                }
            }
        }
    }

    public static class Alert
    {
        public static void Show(string message)
        {
            string cleanMessage = message.Replace("'", "\\'");
            string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";

            Page page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", script);
            }
        }
    }
    
    #endregion

}
