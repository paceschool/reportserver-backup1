﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupNoteControl.ascx.cs"
    Inherits="Group_AddNote" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addgroupnote");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveGroupNote") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });

</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addgroupnote" name="addgroupnote" method="post" action="ViewGroupPage.aspx/SaveGroupNote">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Group" id="GroupId" value="<%= GetGroupId %>" />
        <input type="hidden" id="GroupNoteId" value="<%= GetGroupNoteId %>" />
        <label>
            Type
        </label>
        <select type="text" name="Xlk_NoteType" id="NoteTypeId">
            <%= GetNoteTypeList()%>
        </select>
        <label>
            Created By
        </label>
        <select disabled name="Users" id="UserId">
            <%= GetNoteUserList() %>
        </select>
        <label>
            Note
        </label>
        <textarea rows="5" id="Note"><%= GetNote %></textarea>
          </fieldset>
</div>
</form>
