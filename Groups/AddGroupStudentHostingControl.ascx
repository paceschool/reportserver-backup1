﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupStudentHostingControl.ascx.cs"
    Inherits="Student_AddHosting" %>

<script type="text/javascript">
    $(function() {

        $("#ArrivalDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#DepartureDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function() {
            return $("#addhosting");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveHosting") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addhosting" name="addhosting" method="post">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
        <input type="hidden" id="HostingId" value="<%= GetHostingId %>" />
        <label for="family">
            Family</label>
        <select type="text" name="Family" id="FamilyId" >
            <%= GetFamilyList() %>
        </select>
        <label for="ArrivalDate">
            Start Date</label>
        <input type="datetext" name="ArrivalDate" id="ArrivalDate" value="<%= GetArrivalDate %>"/>
        <label for="DepartureDate">
            End Date</label>
        <input type="datetext" name="DepartureDate" id="DepartureDate" value="<%= GetDepartureDate %>"/>
        <label for="WeeklyRate">
            Weekly Rate</label>
        <input type="text" id="WeeklyRate" value="<%= GetWeeklyRate %>" />
    </fieldset>
</div>
</form>
