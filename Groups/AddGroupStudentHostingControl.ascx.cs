﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Student_AddHosting : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected  Hosting _hosting;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            if (GetValue<Int32?>("HostingId").HasValue)
            {
                Int32 hostingId = GetValue<Int32>("HostingId");
                _hosting = (from host in entity.Hostings.Include("Student").Include("Family")
                            where host.HostingId == hostingId
                            select host).FirstOrDefault();
            }
        }
    }


    private List<Pace.DataAccess.Accommodation.Family> GetFamilies(string filter)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Pace.DataAccess.Accommodation.Family> familyQuery = from f in entity.Families
                                                                       select f;
            if (!string.IsNullOrEmpty(filter))
                familyQuery = familyQuery.Where(f => f.FirstName.StartsWith(filter) || f.SurName.StartsWith(filter));

            return familyQuery.ToList();
        }
    }

    protected string GetFamilyList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_hosting != null && _hosting.Family != null) ? _hosting.Family.FamilyId : (int?)null;

        var familyQuery = GetFamilies(string.Empty).Select(f => new { FamilyId = f.FamilyId, Name = f.SurName.ToString() + " " + f.FirstName.ToString() }).OrderBy(f => f.Name);
        
        foreach (var item in familyQuery.ToList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.FamilyId, item.Name, (_current.HasValue && item.FamilyId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetHostingId
    {
        get
        {
            if (_hosting != null)
                return _hosting.HostingId.ToString();
            return "0";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_hosting != null)
                return _hosting.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetArrivalDate
    {
        get
        {
            if (_hosting != null)
                return _hosting.ArrivalDate.ToString("dd/MM/yy");

            return null;
        }
    }
    protected string GetDepartureDate
    {
        get
        {
            if (_hosting != null && _hosting.DepartureDate != null)
                return _hosting.DepartureDate.ToString("dd/MM/yy");

            return null;

        }
    }

    protected string GetWeeklyRate
    {
        get
        {
            if (_hosting != null)
                return _hosting.WeeklyRate.ToString();

            return null;
        }
    }
}
