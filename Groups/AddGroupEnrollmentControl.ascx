﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupEnrollmentControl.ascx.cs"
    Inherits="Group_AddEnrollment" %>
<script type="text/javascript">
    $(function () {

        $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#EndDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function () {
            return $("#addenrollment");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveEnrollment") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $('.horizontalcheckboxes').change(function () {

            var classname = $(this).find('input:checkbox').attr('class');
            var total = 0;

            $("." + classname + ":checked").each(function () {
                total += parseInt($(this).val(), 10);
            });

            $('#' + classname + '').val(total);

        });
        loadcourseTypes = function (programmetypeid) {
            $("#CourseTypeId").html('');
            $.ajax({
                type: "POST",
                url: "ViewGroupPage.aspx/GetCourseTypes",
                data: "{programmetypeid:" + programmetypeid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $("#CourseTypeId").html(msg.d.Payload).change();
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        }

        loadLessonBlocks = function (courestypeid) {
            $("#LessonBlockId").html('');
            $.ajax({
                type: "POST",
                url: "ViewGroupPage.aspx/GetLessonBlocks",
                data: "{courestypeid:" + courestypeid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $("#LessonBlockId").html(msg.d.Payload);
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        }

        $("#ProgrammeTypeId").change(function (e) {
            loadcourseTypes($(this).val());

        });

        $("#CourseTypeId").change(function (e) {
            loadLessonBlocks($(this).val());

        });

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addenrollment" name="addenrollment" method="post" action="ViewGroupPage.aspx/SaveEnrollment">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="MorningLessonDays" value="<%=  GetMorningLessonDays %>" />
        <input type="hidden" id="AfternoonLessonDays" value="<%=  GetAfternoonLessonDays %>" />
        <input type="hidden" id="EveningLessonDays" value="<%=  GetEveningLessonDays %>" />
        <input type="hidden" name="Group" id="GroupId" value="<%= GetGroupId %>" />
        <input type="hidden" id="EnrollmentId" value="<%= GetEnrollmentId %>" />
        <label>
            Programme Type
        </label>
        <select name="Xlk_ProgrammeType" id="ProgrammeTypeId">
            <%= GetProgrammeTypeList() %>
        </select>
        <label>
            Course Type
        </label>
        <select name="Xlk_CourseType" id="CourseTypeId">
            <%= GetCourseTypeList() %>
        </select>
        <label>
            Hours
        </label>
        <select name="Xlk_LessonBlock" id="LessonBlockId">
            <%= GetLessonBlockTypeList() %>
        </select>
        <label for="StartDate">
            Start Date</label>
        <input type="datetext" name="StartDate" id="StartDate" value="<%= GetStartDate %>" />
        <label for="EndDate">
            End Date</label>
        <input type="datetext" name="EndDate" id="EndDate" value="<%= GetEndDate %>" />
        <label>
           <b>Morning Tuition Days</b>
        </label>
        <div class="horizontalcheckboxes">
            <%= TuitionDays("m", GetMorningLessonDays, "MorningLessonDays")%></div>
        <br /><br />
        <label>
            <b>Afternoon Tuition Days</b>
        </label>
        <div class="horizontalcheckboxes">
            <%= TuitionDays("a", GetAfternoonLessonDays, "AfternoonLessonDays")%></div>
        <br /><br />
        <label>
            <b>Evening Tuition Days</b>
        </label>
        <div class="horizontalcheckboxes">
            <%= TuitionDays("e", GetEveningLessonDays, "EveningLessonDays")%></div>
    </fieldset>
</div>
</form>
