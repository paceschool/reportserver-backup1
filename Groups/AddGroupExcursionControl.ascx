﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupExcursionControl.ascx.cs"
    Inherits="Groups_AddGroupExcursionControl" %>

<script type="text/javascript">
    $(function () {

        $("#PreferredDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function () {
            return $("#addgroupexcursion");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveGroupExcursion") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $('.watcher').live("change", function () {
            if ($(this).attr("value") == "on") {
                $('#TransportRequired').val("true");
            }
            else {
                $('#TransportRequired').val("false");
            }
        });

    });
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addgroupexcursion" name="addgroupexcursion" method="post" action="ViewGroupPage.aspx/SaveGroupExcursion">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="GroupExcursionId" value="<%= GetGroupExcursionId %>" />
        <input type="hidden" name="Group" id="GroupId" value="<%= GetGroupId %>" />
        <label>
            Select Excursion
        </label>
        <select name="Excursion" id="ExcursionId">
            <%= GetExcursions() %>
        </select>
        <label for="StartDate">
            Preferred Date</label>
        <input type="datetext" name="PreferredDate" id="PreferredDate" value="<%= GetPreferredDate %>" />
        <label for="PreferredTime">
            Preferred Time</label>
        <input type="text" name="PreferredTime" id="PreferredTime" value="<%= GetPreferredTime %>" />
        <label for="IsDeleted">
            IsDeleted</label>
        <input type="checkbox" id="IsDeleted" <%= GetIsDeleted %> value="true"  />
        <label for="QtyStudents">
            Qty Students</label>
        <input type="text" name="QtyStudents" id="QtyStudents" value="<%= GetQtyStudents %>" />
        <label for="QtyLeaders">
            Qty Leaders</label>
        <input type="text" name="QtyLeaders" id="QtyLeaders" value="<%= GetQtyLeaders %>" />
        <label for="QtyGuides">
            Qty Guides</label>
        <input type="text" name="QtyGuides" id="QtyGuides" value="<%= GetQtyGuides %>" />
        <label for="TransportRequired">
            Private Transport Required</label>
        <input type="checkbox" id="TransportReq" <%= GetTransportRequired %> class="watcher" value="true"/>
        <input type="hidden" id="TransportRequired" />
       <label for="Comment">
            Comment</label>
        <textarea id="Comment"><%= GetComment%></textarea>
    </fieldset>
</div>
</form>
