﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ParseStudentListPage.aspx.cs" Inherits="Groups_ParseStudentListPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {

            $("#<%= repeat.ClientID %>").buttonset();

            $('#loadGroup').bind('click', function () {
                if ($('#loadGroup').attr("href") == "#")
                    $('#loadGroup').attr("href", "ViewGroupPage.aspx?GroupId=" + $('#<%= listgroups.ClientID  %> option:selected').val())
            });

            $('#<%= listgroups.ClientID  %>').change(function () {

                if ($(this).val() > 0) {
                   $('input[type="submit"]').removeAttr('disabled');
                    $("#<%= parsed.ClientID %>").removeAttr('disabled').button("refresh");
                }
                else {
                    $('input[type="submit"]').attr('disabled', 'disabled');
                    $("#<%= parsed.ClientID %>").attr('disabled', 'disabled').button("refresh");
                }

                $('#loadGroup').attr("href", "ViewGroupPage.aspx?GroupId=" + $('#<%= listgroups.ClientID  %> option:selected').val())
            });

            saveStudents = function () {
                $("#dialog-message").html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><big><b>' + $('#<%= listgroups.ClientID  %> option:selected').text() + ':- </b></big>All student currently tied to this group will be deleted and cannot be recovered. Are you sure?</p>')
                     .dialog({
                         autoOpen: true,
                         resizable: false,
                         width: 'auto',
                         height: 'auto',
                         modal: true,
                         buttons: {
                             "Save Students": function () {
                                 __doPostBack('<%= submit.UniqueID %>', '');


                             },
                             Cancel: function () {
                                 $(this).dialog("close");
                             }
                         }
                     });
                return false; ;
            }

            if ($('#<%= listgroups.ClientID  %>').val() > 0) {
                $('input[type="submit"]').removeAttr('disabled');
                $("#<%= parsed.ClientID %>").removeAttr('disabled').button("refresh");
            }
            else
            {
                $('input[type="submit"]').attr('disabled', 'disabled');
                $("#<%= parsed.ClientID %>").attr('disabled', 'disabled').button("refresh");
            }

            $('#<%= repeat.ClientID  %>').find("input:radio").attr('checked', false);

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Pace Importer Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Group
                    </td>
                    <td>
                        <asp:DropDownList ID="listgroups" runat="server" DataTextField="GroupName" DataValueField="GroupId"
                            AppendDataBoundItems="true">
                            <asp:ListItem Value="-1" Text="Select a Group"></asp:ListItem>
                        </asp:DropDownList>
                        <a id="loadGroup" href="#" title="View Selected Group">
                            <img alt="View Selected Group" src="../Content/img/actions/next.png" /></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <p>
            Format is:
            <table id="box-table-a">
                <tr>
                    <th>
                        Type (Teacher/Student)
                    </th>
                    <th>
                        First Name
                    </th>
                    <th>
                        Last Name
                    </th>
                    <th>
                        Second Last Name
                    </th>
                    <th>
                        DOB
                    </th>
                    <th>
                        Gender (M/F)
                    </th>
                    <th>
                        Arrival Date
                    </th>
                    <th>
                        Departure Date
                    </th>
                    <th>
                        Comment
                    </th>
                    <th>
                        Allergy
                    </th>
                </tr>
            </table>
        </p>
        <br />
        <p>
            <span id="repeat" runat="server" repeatdirection="Horizontal">
                <asp:RadioButton ID="raw" name="radio" runat="server" AutoPostBack="true" OnCheckedChanged="raw_Click" Text="Raw"></asp:RadioButton>
                <asp:RadioButton ID="parsed" name="radio" runat="server" AutoPostBack="true" OnCheckedChanged="parsed_Click" Text="Parsed Students"></asp:RadioButton>
            </span>
            <% if (errorList != null && errorList.Count > 0)
               { %>
            <div class="error">
                <% foreach (string item in errorList)
                   {%>
                <p>
                    <b>
                        <%= item.ToString()%></b></p>
                <% } %>
            </div>
            <%  } %>
            <div>
                <% if (_action == "raw")
                   { %>
                <textarea rows="20" cols="10" id="rawtext" runat="server" style="width: 100%"><%# LoadRawText() %></textarea>
                <% }
                   else
                   { %>
                <table id="box-table-a" class="tablesorter">
                    <thead>
                        <tr>
                            <th scope="col">
                                Type
                            </th>
                            <th scope="col">
                                Name
                            </th>
                            <th scope="col">
                                Gender
                            </th>
                            <th scope="col">
                                Age
                            </th>
                            <th scope="col">
                                DOB
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="results" runat="server" EnableViewState="true">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Xlk_StudentType.Description") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#CreateName(DataBinder.Eval(Container.DataItem, "FirstName"), DataBinder.Eval(Container.DataItem, "SurName")) %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Xlk_Gender.Description")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%# CalculateAge((DateTime?)DataBinder.Eval(Container.DataItem, "DateOfBirth")) %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "DateOfBirth", "{0:dd MMM yyyy}")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <span style="float: right">
                    <asp:Button ID="submit" class="button" runat="server" Text="Save Student" OnClick="submit_Click"
                        OnClientClick="javascript:return saveStudents()" /></span>
                <% } %>
            </div>
        </p>
    </div>
</asp:Content>
