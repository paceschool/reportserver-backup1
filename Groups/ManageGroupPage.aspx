﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManageGroupPage.aspx.cs" Inherits="ManageGroupPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()
            $("#<%= newarrdate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<%= newdepdate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Add / Manage Group Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <div id="errorcontainer" name="errorcontainer" class="error" runat="server" visible="false">
            <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
        </div>
        <div class="inputArea">
            <fieldset>
                <label>
                    Group Name</label>
                <asp:TextBox ID="GroupName" runat="server" AutoPostBack="true" OnTextChanged="Text_CheckGroupName">
                </asp:TextBox>
                <label>
                    Arrival Date
                </label>
                <asp:TextBox ID="newarrdate" runat="server">
                </asp:TextBox>
                <label>
                    Departure Date
                </label>
                <asp:TextBox ID="newdepdate" runat="server"></asp:TextBox>
                <label>
                    Number of Students
                </label>
                <asp:TextBox ID="newgroupstudents" runat="server">
                </asp:TextBox>
                <asp:RangeValidator ID="RangeValidator1" ControlToValidate="newgroupstudents" MinimumValue="0"
                    MaximumValue="120" Type="Integer" EnableClientScript="false" runat="server" Text="Please enter a number!">
                </asp:RangeValidator>
                <label>
                    Number of Leaders
                </label>
                <asp:TextBox ID="newgroupleaders" runat="server">
                </asp:TextBox>
                <asp:RangeValidator ID="RangeValidator2" ControlToValidate="newgroupleaders" MinimumValue="0"
                    MaximumValue="10" Type="Integer" EnableClientScript="false" runat="server" Text="Please enter a number of leaders between 0 & 10">
                </asp:RangeValidator>
                <label>
                    Leader's Mobile
                </label>
                <asp:TextBox ID="leadersmobile" runat="server">
                </asp:TextBox>
            </fieldset>
            <fieldset>
                <label>
                    Group Nationality</label>
                <asp:DropDownList ID="newnationality" runat="server" DataTextField="Description"
                    DataValueField="NationalityId">
                </asp:DropDownList>
                <label>
                    Agent</label>
                <asp:DropDownList ID="newagent" runat="server" DataTextField="Name" DataValueField="AgencyId">
                </asp:DropDownList>
                <label>
                    Status
                </label>
                <asp:DropDownList ID="newstatus" runat="server" DataTextField="Description" DataValueField="StatusId">
                </asp:DropDownList>
                <label>
                    Is Closed?
                </label>
                <asp:CheckBox ID="chkiscolsed" runat="server" Checked="false" />
                <label>
                    Number of Classes
                </label>
                <asp:TextBox ID="newgroupclasses" runat="server">
                </asp:TextBox>
                <asp:RangeValidator ID="RangeValidator3" ControlToValidate="newgroupclasses" MinimumValue="0"
                    MaximumValue="12" Type="Integer" EnableClientScript="false" runat="server" Text="Please enter a number!">
                </asp:RangeValidator>
                <label>
                    Curfew
                </label>
                <asp:TextBox ID="groupcurfew" runat="server">
                </asp:TextBox>
            </fieldset>
            <fieldset>
                <label>
                    Arrival Information
                </label>
                <textarea id="arrivalinfo" runat="server" cols="20" rows="5">
                </textarea>
                <label>
                    Departure Information
                </label>
                <textarea id="departureinfo" runat="server" cols="20" rows="5">
                </textarea>
                <label>
                    Bus Tickets?
                </label>
                <asp:CheckBox ID="ticketsassigned" runat="server" Checked="false" />
            </fieldset>
            <fieldset>
                                    <label>
                            Campus
                        </label>
                        <asp:DropDownList ID="listcampus" runat="server" DataValueField="CampusId" DataTextField="Name">
                        </asp:DropDownList>
                <asp:LinkButton ID="newsave" runat="server" class="button" OnClick="Click_Save"><span>Save</span></asp:LinkButton>
                <%--                 <asp:LinkButton ID="cancel" runat="server" class="button" OnClientClick=""><span>Cancel</span></asp:LinkButton>
                --%>
            </fieldset>
        </div>
    </div>
</asp:Content>
