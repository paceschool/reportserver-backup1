﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupControl.ascx.cs"
    Inherits="Group_AddGroup" %>
<script type="text/javascript">
    $(function () {

        $("#ArrivalDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#DepartureDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getForm = function () {
            return $("#addenrollment");
        }

        getTargetUrl = function () {
            return  '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveGroup") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $('.horizontalcheckboxes').change(function () {

            var classname = $(this).find('input:checkbox').attr('class');
            var total = 0;

            $("." + classname + ":checked").each(function () {
                total += parseInt($(this).val(), 10);
            });

            $('#' + classname + '').val(total);

        });

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addenrollment" name="addenrollment" method="post" action="ViewGroupPage.aspx/SaveGroup">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="GroupId" value="<%= GetGroupId %>" />
        <label>
            Group Name</label>
        <input id="GroupName" value="<%= GetGroupName %>" />
        <label>
            Arrival Date
        </label>
        <input type="datetext" id="ArrivalDate" value="<%= GetArrivalDate %>" />
        <label>
            Departure Date
        </label>
        <input type="datetext" id="DepartureDate" value="<%= GetDepartureDate %>" />
        <label>
            No of Students
        </label>
        <input id="NoOfStudents" value="<%= GetNoOfStudents %>" />
        <label>
            No of Leaders
        </label>
        <input id="NoOfLeaders" value="<%= GetNoOfLeaders %>" />
        <label>
            Leader's Mobile
        </label>
        <input id="LeaderMobile" value="<%= GetLeaderMobile %>" />
    </fieldset>
    <fieldset>
        <label>
            Group Nationality</label>
        <select name="Xlk_Nationality" id="NationalityId">
            <%= GetNationalityList()%>
        </select>
        <label>
            Agent</label>
        <select name="Agency" id="AgencyId">
            <%= GetAgencyList()%>
        </select>
        <label>
            Status
        </label>
        <select name="Xlk_Status" id="StatusId">
            <%= GetStatusList()%>
        </select>
        <label>
            Closed Group?
        </label>
        <input type="checkbox" id="IsClosed" <%= GetIsClosed %> value="true" />
        <label>
            No of Classes
        </label>
        <input id="NoOfClasses" value="<%= GetNoOfClasses %>" />
        <label>
            Curfew
        </label>
        <input id="GroupCurfew" value="<%= GetGroupCurfew %>" />
    </fieldset>
    <fieldset>
        <label>
            Arrival Information
        </label>
        <input id="ArrivalInfo" value="<%= GetArrivalInfo %>" />
        <label>
            Departure Information
        </label>
        <input id="DepartureInfo" value="<%= GetDepartureInfo %>" />
        <label>
            Bus Tickets?
        </label>
        <input type="checkbox" id="BusTickets" <%= GetBusTickets %> value="true" />
        <label>
            Campus
        </label>
        <select id="CampusId">
            <%= GetCampusList()%>
        </select>
    </fieldset>
</div>
</form>
