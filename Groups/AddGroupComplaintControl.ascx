﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupComplaintControl.ascx.cs"
    Inherits="Group_AddComplaint" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addgroupcomplaint");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Groups/ViewGroupPage.aspx","SaveGroupComplaint") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addgroupcomplaint" name="addgroupcomplaint" method="post" action="ViewGroupPage.aspx/SaveGroupComplaint">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Group" id="GroupId" value="<%= GetGroupId %>" />
        <input type="hidden" id="GroupComplaintId" value="<%= GetGroupComplaintId %>" />
                <label>
            Severity
        </label>
        <select type="text" name="Xlk_Severity" id="SeverityId">
            <%= GetSeverityList() %>
        </select>
        <label>
            Type
        </label>
        <select type="text" name="Xlk_ComplaintType" id="ComplaintTypeId">
            <%= GetComplaintTypeList() %>
        </select>
        <label>
            Complaint
        </label>
        <textarea rows="5" id="Complaint"><%= GetComplaint %></textarea>
        <label>
            Action Needed
        </label>
        <input type="checkbox" id="ActionNeeded" <%= GetAction %> value="true" />
                <label>
            Raised By
        </label>
        <select type="text" name="Users" id="UserId">
            <%= GetComplaintUserList() %>
        </select>
    </fieldset>
</div>
</form>
