﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentGroupingControl.ascx.cs" 
    Inherits="Groups_StudentGroupingControl" %>

<script type="text/javascript">
    $(function () {

        setDraggable = function () {
            $(".draggable").draggable({
                helper: "clone",
                revert: "invalid",
                containment: "#dialog-form",
                scroll: false
            });
        }

        setDroppable = function () {
            $(".droppable").droppable({
                accept: ".draggable",
                activeClass: "ui-state-hover",
                hoverClass: "ui-state-active",
                drop: function (event, ui) {
                    $(this).addClass("ui-state-highlight");
                    AssignStudentToGroupingBlock(this.id, ui.draggable.attr('id'))
                }
            });
        }

        AssignStudentToGroupingBlock = function (groupingblockid, studentid) {
            $.ajax({
                type: "POST",
                url: "ViewGroupPage.aspx/AssignStudentToGroupingBlock",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ groupingblockid: groupingblockid, studentid: studentid }),
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            GetGroups($('#GroupId').val());
                            GetStudents($('#GroupId').val());
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        };

        GetStudents = function (groupid) {
            $.ajax({
                type: "POST",
                url: "ViewGroupPage.aspx/GetGroupStudents",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ id: groupid }),
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $('#filteredstudents').html(msg.d.Payload);
                            setDraggable();
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        };

        $(function () {
            $("#notassigned").jScroll();
        });

        GetGroups = function (groupid) {
            $.ajax({
                type: "POST",
                url: "ViewGroupPage.aspx/GetGroupedStudents",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ id: groupid }),
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $('#assigned').html(msg.d.Payload);
                            setDroppable();
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        };

        deleteStudentGrouping = function (groupingblockid, groupid) {
            $.ajax({
                type: "POST",
                url: "ViewGroupPage.aspx/DeleteGrouping",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ groupingblockid: groupingblockid, groupid: groupid }),
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            GetGroups($('#GroupId').val());
                            GetStudents($('#GroupId').val());
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        };

        setDraggable();
        setDroppable();
    });
</script>

    <style type="text/css">
        #disconnect
        {
            float: left;
        }
        #notassigned .classtd div
        {
            background-color: Red;
        }
        .classtd
        {
            border: 1px solid #AED0EA;
            vertical-align: top;
        }
        .classlist
        {
            float: left;
        }
        .classlist div
        {
            background-color: Fuchsia;
            font-weight: bold;
        }
        #notassigned
        {
            float: left;
        }
        #assigned
        {
            float: left;
        }
    </style>

<p id="message" style="display: none;">
    All fields must be completed</p>

<form id="addstudentgrouping" name="addstudentgrouping" method="post">
    <div class="inputArea">
        <div id="notassigned">
            <input type="hidden" name="Group" id="GroupId" value="<%= GetGroupId %>" />
            <table id="ctl00_maincontent_results" cellspacing="0" border="0" style="border-collapse: collapse;">
                <tbody>
                    <tr>
                        <td class="classtd">
                            <ul class="classlist" style="float: left; list-style-type: none; font-style: italic;">
                                <div id="-1" class="droppable">
                                </div>
                                <li id="filteredstudents">
                                   <%= BaseGroupPage.GetGroupStudents(_groupId).Payload %>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <fieldset>
            <ul class="classlist" style="float: left; list-style-type: none;">
                <div id="-1" class="droppable">
                    <li style="text-align:center">
                        New <br />Student Grouping <br />
                    </li>
                </div>
            </ul>
        </fieldset>
        <fieldset style="width:650px">
            <div id="assigned" style="width:90%">
                <asp:DataList ID="results" runat="server" RepeatDirection="Horizontal" RepeatColumns="3">
                    <HeaderStyle Font-Names="Verdana" Font-Size="12pt" HorizontalAlign="center" Font-Bold="True" />
                    <ItemStyle CssClass="classtd" />
                    <ItemTemplate>
                        <ul class="classlist" style="float: left; list-style-type: none;">
                            <div id="<%# DataBinder.Eval(Container.DataItem, "GroupingBlockId") %>" class="droppable" style="width: 200px;">
                                <li style="text-align:center; width:200px;">
                                    Student Grouping <a href="#" onclick="deleteStudentGrouping(<%#DataBinder.Eval(Container.DataItem, "GroupingBlockId") %>, <%#DataBinder.Eval(Container.DataItem, "GroupId") %>); DeleteGrouping(<%#DataBinder.Eval(Container.DataItem, "GroupingBlockId") %>, <%#DataBinder.Eval(Container.DataItem, "GroupId") %>)"><img src="../Content/img/actions/delete.png" alt="Delete Grouping" style="float:right" id="DeleteGrouping"/></a>No.<%# DataBinder.Eval(Container.DataItem, "GroupingBlockId") %></li>
                                <li>
                                    <%# BuildNames(DataBinder.Eval(Container.DataItem, "GroupingBlockId")) %>
                                </li>
                            </div>
                        </ul>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </fieldset>
    </div>
</form>