﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Groups_AddGroupInvoiceControl : BaseGroupControl
{
    protected Int32 _groupInvoiceId;
    protected GroupInvoice _invoice;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("GroupInvoiceId").HasValue)
        {
            _groupInvoiceId = GetValue<Int32>("GroupInvoiceId");
            using (GroupEntities entity = new GroupEntities())
            {
                _invoice = (from invoices in entity.GroupInvoice.Include("Xlk_SageDataType").Include("Group")
                            where invoices.GroupInvoiceId == _groupInvoiceId
                            select invoices).FirstOrDefault();
            }
        }
    }

    protected string GetSageRef
    {
        get
        {
            if (_invoice != null)
                return _invoice.SageInvoiceRef.ToString();

            return "";
        }
    }

    protected string GetGroupId
    {
        get
        {
            if (_invoice != null)
                return _invoice.Group.GroupId.ToString();

            if (GetValue<Int32?>("GroupId").HasValue)
                return GetValue<Int32>("GroupId").ToString();
            return "0";
        }
    }

    protected string GetGroupInvoiceId
    {
        get
        {
            if (_invoice != null)
                return _invoice.GroupInvoiceId.ToString();


            if (GetValue<Int32?>("GroupInvoiceId").HasValue)
                return GetValue<Int32>("GroupInvoiceId").ToString();

            return "0";
        }
    }

    protected string GetDataTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_invoice != null && _invoice.Xlk_SageDataType != null) ? _invoice.Xlk_SageDataType.SageDataTypeId : (byte?)null;

        foreach (Xlk_SageDataType item in LoadDataTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.SageDataTypeId, item.Description, (_current.HasValue && item.SageDataTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}