﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Group_AddGroupOrderedItem : BaseGroupControl
{
    protected Int32 _studentId;
    protected OrderedItem _orderedItem;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            if (GetValue<Int32?>("OrderedItemId").HasValue)
            {
                Int32 orderedItemId = GetValue<Int32>("OrderedItemId");
                _orderedItem = (from comp in entity.OrderedItems.Include("Xlk_Unit").Include("OrderableItem").Include("Group")
                                where comp.OrderedItemId == orderedItemId
                              select comp).FirstOrDefault();
            }
        }

    }

    protected string GetOrderedItemId
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.OrderedItemId.ToString();

            if (Parameters.ContainsKey("OrderedItemId"))
                return Parameters["OrderedItemId"].ToString();

            return "0";
        }
    }
    protected string GetGroupId
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.Group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

    protected string IsEditMode
    {
        get
        {
            if (_orderedItem == null)
                return String.Empty;

            return "disabled";
        }
    }


    protected string GetProductTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_ProductType item in LoadProductTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.ProductTypeId, item.Description);
        }

        return sb.ToString();
    }

    protected string GetPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_PaymentMethod item in LoadPaymentMethods())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.PaymentMethodId, item.Description);
        }

        return sb.ToString();
    }


    protected string GetItemsList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_orderedItem != null && _orderedItem.OrderableItem != null) ? _orderedItem.OrderableItem.ItemId : (short?)null;

        List<OrderableItem> items;

        using (GroupEntities entity = new GroupEntities())
        {
            items = (from i in entity.OrderableItems select i).ToList();
        }

        foreach (OrderableItem item in items)
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ItemId, item.Title, (_current.HasValue && item.ItemId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetQtyUnitList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_orderedItem != null && _orderedItem.Xlk_Unit != null) ? _orderedItem.Xlk_Unit.UnitId : (byte?)null;

        foreach (Xlk_Unit item in LoadUnits())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UnitId, item.Description, (_current.HasValue && item.UnitId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetDescription
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.Description;

            return null;
        }
    }

    protected string GetQty
    {
        get
        {
            if (_orderedItem != null && _orderedItem.Qty.HasValue)
                return _orderedItem.Qty.Value.ToString();

            return string.Empty;
        }
    }

    protected string GetSpecialInstructions
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.SpecialInstructions;

            return string.Empty;
        }
    }

    protected string GetComment
    {
        get
        {
            if (_orderedItem != null)
                return _orderedItem.Comment;

            return string.Empty;
        }
    }


}
