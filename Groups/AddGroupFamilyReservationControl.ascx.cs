﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Group;

public partial class Group_AddGroupFamilyReservationControl : BaseGroupControl
{
    protected Int32 _familyId;
    protected Int32 _groupId;
    protected ReservedFamily _reservedFamily;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            if (GetValue<Int32?>("GroupId") .HasValue && GetValue<Int32?>("FamilyId").HasValue)
            {
                _groupId = GetValue<Int32>("GroupId");
                _familyId = GetValue<Int32>("FamilyId");
                _reservedFamily = (from host in entity.ReservedFamilies.Include("Family")
                            where host.GroupId == _groupId && host.FamilyId == _familyId
                            select host).FirstOrDefault();
            }
            if (GetValue<Int32?>("GroupId").HasValue)
            {
                Int32 _groupId = GetValue<Int32>("GroupId");
            }
        }
    }

 

    private List<Pace.DataAccess.Accommodation.Family> GetFamilies(string filter)
    {
        using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Pace.DataAccess.Accommodation.Family> familyQuery = from f in entity.Families
                                                                       select f;
            if (!string.IsNullOrEmpty(filter))
                familyQuery = familyQuery.Where(f => f.FirstName.StartsWith(filter) || f.SurName.StartsWith(filter));

            return familyQuery.ToList();
        }
    }

    protected string BuildFamilies(bool assigned, string filter)
    {
        StringBuilder sb = new StringBuilder();

        IEnumerable<Pace.DataAccess.Group.Family> familyQuery;

        using (GroupEntities entity = new GroupEntities())
        {

            if (assigned)
                familyQuery = from f in entity.Families
                              join a in entity.ReservedFamilies on f.FamilyId equals a.FamilyId
                              select f;
            else
                familyQuery = from f in entity.Families
                              join a in entity.ReservedFamilies on f.FamilyId equals a.FamilyId into groupfamilies
                              from gf in groupfamilies.DefaultIfEmpty()
                              select f;


            if (!string.IsNullOrEmpty(filter))
                familyQuery = familyQuery.Where(f => f.FirstName.StartsWith(filter) || f.SurName.StartsWith(filter));

            foreach (var item in familyQuery.ToList())
            {
                sb.AppendFormat("<option id={0}>{1} {2}</option>", item.FamilyId, item.FirstName, item.SurName);
            }
        }
        return sb.ToString();
    }

    protected string GetGroupId
    {
        get
        {
            if (_groupId != null)
                return _groupId.ToString();
            return "0";
        }
    }

 
}
