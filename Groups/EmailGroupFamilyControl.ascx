﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailGroupFamilyControl.ascx.cs"
    Inherits="Group_AddNote" %>

<script type="text/javascript">

    $(function() {

        var data = <%= ItemsToJsonArray() %>;

        getForm = function() {
            return $("#addgroupnote");
        }

        getTargetUrl = function() {
            return "ViewGroupPage.aspx/SendStudentInfoEmailToFamilies";
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("select#MessageId").change(function(){ 
        // Post string 
            var val =  $(this).val();
            $.each(data, function(i, j){ 
                if (j.Id == val)
                    loadItem(i);
            }); 
        });
       
        loadItem = function(index)
        {
            var item =   data[index];
            $('#Subject').val(item.Subject);
            $('#Body').html(item.Body); 
        }
    });

</script>

<p id="message" style="display: none;">
    All fields must be compltetd</p>
<form id="addgroupnote" name="addgroupnote" method="post" action="ViewGroupPage.aspx/SendStudentInfoEmailToFamilies">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="GroupId" value="<%= GetValue<Int32>("GroupId") %>" />
         <label>
            From
        </label>
        <select type="text" id="UserId">
            <%=GetUserList()%>
        </select>
        <label>
            Template
        </label>
        <select type="text" id="MessageId">
            <%=BuildTitlesList()%>
        </select>
       <label>
            Subject
        </label>
        <input type="text" id="Subject" value="" validation="required"/>
        <label>
            Body
        </label>
        <textarea style="width:400px;height:300px;" id="Body" validation="required"></textarea>
    </fieldset>
</div>
</form>
