﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;
using Pace.Common;

public partial class Group_AddNote : BaseGroupControl
{
    protected Int32 _grouipId;
    protected static CommunicationItemRecord[] items;

    protected void Page_Load(object sender, EventArgs e)
    {
        items = (from f in CommunicationItems.Instance.GetCommunicationItemsDetails("message") where f.Formats.Contains("email") select f).ToArray();
    }

    protected static string ItemsToJsonArray()
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();

        return serializer.Serialize(items);
    }

    protected string GetUserList()
    {
        StringBuilder sb = new StringBuilder();
        int _current = GetCurrentUser().UserId;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Email, (item.UserId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string BuildTitlesList()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("<option>Select Template</option>");
        foreach (CommunicationItemRecord item in items)
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.Id, item.Title);
        }

        return sb.ToString();
    }


}
