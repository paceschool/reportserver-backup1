﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Enrollment =Pace.DataAccess.Enrollment;
using System.Globalization;
using System.Threading;
using System.Data;
using Group = Pace.DataAccess.Group;
using System.Reflection;
using System.Data.Entity.Core;


public partial class Groups_ParseSFAStudentListPage : BaseGroupPage
{
    protected string _action = "raw";
    protected string _rawtext;

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        _rawtext = rawtext.Value;

        if (!Page.IsPostBack)
        {
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            var GroupSearchQuery = from g in entity.Group
                                   where g.ArrivalDate >= DateTime.Now && g.CampusId == GetCampusId && g.Enrollment.Count > 0 && g.Agency.AgencyId == 86 // agencyid of SFA
                                   orderby g.GroupName ascending
                                   select g;


            listgroups.DataSource = GroupSearchQuery.ToList();
            listgroups.DataBind();
        }
    }

    protected void submit_Click(object sender, EventArgs e)
    {
        using (Enrollment.EnrollmentsEntities entity = new Enrollment.EnrollmentsEntities())
        {
            SaveGroupStudent(entity, GetStudentsList(entity));
        }
    }

    protected void parsed_Click(object sender, EventArgs e)
    {
        _action = "";
        LoadStudentsList();
       
    }
    protected void raw_Click(object sender, EventArgs e)
    {
        _action = "raw";
    }
    #endregion

    #region Private Methods

    protected void LoadStudentsList()
    {
        try
        {
            results.DataSource = GetStudentsList(BaseEnrollmentPage.CreateEntity);
            results.DataBind(); 
            qty.InnerHtml = string.Format("Students {0}", results.Items.Count);
        }
        catch (Exception ex)
        {
            throw;
        }

    }

    protected List<Enrollment::Student> GetStudentsList(Enrollment.EnrollmentsEntities entity)
    {
        List<Enrollment::Student> students = new List<Enrollment::Student>();
        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        TextInfo textInfo = cultureInfo.TextInfo;
        int groupId = Convert.ToInt32(listgroups.SelectedItem.Value);
        Group::Group group = BaseGroupPage.GetGroup(groupId);

        try
        {
            if (!string.IsNullOrEmpty(rawtext.Value))
            {
                string[] lines = rawtext.Value.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string line in lines)
                {
                    Enrollment::Student student = new Enrollment::Student();

                    string[] details = line.Split(new string[] { "\t" }, StringSplitOptions.None);

                    student.SurName = textInfo.ToTitleCase(FilterWords(details[2]).ToLower());
                    student.FirstName = details[2].Substring(details[2].IndexOf(FilterWords(details[2])) + student.SurName.Length).Trim();
                    student.DateOfBirth = DateTime.Parse(details[4]);
                    student.GroupId = Convert.ToInt32(listgroups.SelectedItem.Value);
                    student.ArrivalDate = group.ArrivalDate;
                    student.DepartureDate = group.DepartureDate;

                    if (details.Count() > 5 && !string.IsNullOrEmpty(details[5]))
                    {
                        for (int i = 5; i < details.Count(); i++)
                        {
                            Pace.DataAccess.Enrollment.StudentNote note = new Enrollment.StudentNote();
                            note.Xlk_NoteTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_NoteType", "NoteTypeId", (byte)4);
                            note.Note = details[i].Trim();
                            note.DateCreated = DateTime.Now;
                            note.UsersReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", 0);
                            student.StudentNotes.Add(note);
                        }
                    }

                    student.CampusId = GetCampusId;
                    student.Xlk_Gender = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_Gender>(entity).FirstOrDefault(x => x.GenderId == ((details[3].ToUpper() == "F") ? 2 : 1));
                    student.Xlk_StudentType = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_StudentType>(entity).FirstOrDefault(x => x.StudentTypeId == ((details[1].ToUpper() == "STUDENT") ? (byte)1 : (byte)2));
                    student.Xlk_Status = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_Status>(entity).FirstOrDefault(x => x.StatusId == ((group.Xlk_Status.StatusId == 1) ? (byte)0 : (byte)1));
                    student.Xlk_Nationality = BaseEnrollmentPage.GetFromStore<Enrollment::Xlk_Nationality>(entity).FirstOrDefault(x => x.NationalityId == group.Xlk_Nationality.NationalityId);
                    student.Agency = BaseEnrollmentPage.GetFromStore<Enrollment::Agency>(entity).FirstOrDefault(x => x.AgencyId == group.Agency.AgencyId);

                    students.Add(student);
                }

            }

            return students;

        }
        catch (Exception ex)
        {
            throw;
        }
    }

    private Enrollment::Xlk_Gender GetGender(int genderId)
    {
        return (from g in BaseEnrollmentPage.LoadGender()
                where g.GenderId == genderId
                select g).FirstOrDefault();
       }

    private Enrollment::Xlk_StudentType GetStudentType(int studentTypeId)
    {
        return (from g in BaseEnrollmentPage.LoadStudentTypes()
                where g.StudentTypeId == studentTypeId
                select g).FirstOrDefault();
    }

    private Enrollment::Xlk_Status GetStatus(int statusId)
    {
        return (from g in BaseEnrollmentPage.LoadStatus()
                where g.StatusId == statusId
                select g).FirstOrDefault();
    }

    private Enrollment::Xlk_Nationality GetNationality(int nationalityId)
    {
        return (from x in BaseEnrollmentPage.LoadNationalities()
                where x.NationalityId == nationalityId
                select x).FirstOrDefault();
    }
    private Enrollment::Agency GetAgent(int agencyId)
    {
        return (from x in BaseEnrollmentPage.LoadAgents()
                where x.AgencyId == agencyId
                select x).FirstOrDefault();
    }

    private static string FilterWords(string str)
    {
        var upper = str.Split(' ')
                    .Where(s => String.Equals(s, s.ToUpper(),
                                StringComparison.Ordinal));

        return string.Join(" ", upper.ToArray());
    }

    protected string LoadRawText()
    {
        return _rawtext.ToString();
    }

    public int SaveGroupStudent(Pace.DataAccess.Enrollment.EnrollmentsEntities eentity, List<Pace.DataAccess.Enrollment.Student> students)
    {
        int i = 0;
        int groupId = Convert.ToInt32(listgroups.SelectedItem.Value);
        int qty;

        try
        {
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                var studentsQuery = (from s in entity.DeleteStudents(groupId) select s).ToList();

                if (studentsQuery.Count == 0)
                {
                    foreach (Enrollment::Student student in students)
                    {
                        eentity.AddToStudent(student);
                    }
                }

               if (eentity.SaveChanges()>0)
                   qty = entity.CreateGroupEnrollments(groupId, null);

            }
            return i;
        }
        catch (Exception)
        {
            return 0;
        }
    }

    #endregion
}
