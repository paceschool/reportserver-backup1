﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Text;
using System.Web.Services;

public partial class Group_AddNote : BaseGroupControl
{
    protected Int32 _groupId;
    protected GroupNote _note;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            if (GetValue<Int32?>("GroupNoteId").HasValue)
            {
                Int32 groupnoteId = GetValue<Int32>("GroupNoteId");
                _note = (from n in entity.GroupNote.Include("Users").Include("Group").Include("Xlk_NoteType")
                         where n.GroupNoteId == groupnoteId
                         select n).FirstOrDefault();
            }
        }
    }

    protected string GetGroupId
    {
        get
        {
            if (_note != null)
                return _note.Group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

    protected string GetGroupNoteId
    {
        get
        {
            if (_note != null)
                return _note.GroupNoteId.ToString();

            if (Parameters.ContainsKey("GroupNoteId"))
                return Parameters["GroupNoteId"].ToString();

            return "0";
        }
    }

    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_note != null && _note.Xlk_NoteType != null) ? _note.Xlk_NoteType.NoteTypeId : (byte?)null;

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.NoteTypeId, item.Description, (_current.HasValue && item.NoteTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNoteUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_note != null && _note.Users != null) ? _note.Users.UserId : GetCurrentUser().UserId;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetNote
    {
        get
        {
            if (_note != null)
                return _note.Note.ToString();

            return null;
        }
    }
}
