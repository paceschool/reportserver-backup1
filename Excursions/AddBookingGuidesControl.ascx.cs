﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using Pace.DataAccess.Group;
using Pace.DataAccess.Excursion;

public partial class Excursions_AddBookingGuidesControl : BaseControl
{
    private Int32? _contactId;
    private Int32? _bookingId;
    private Booking _booking;
    private BookingStaff _staff;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue<Int32?>("BookingId").HasValue)
            {
                _bookingId = GetValue<Int32>("BookingId");
                _booking = entity.Bookings.Include("BookingStaffs").Single(b => b.BookingId == _bookingId);
            }
        }
    }

    protected string GetContactList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">Select</option>");
        List<int> _current = _booking.BookingStaffs.Select(s => s.ContactId).ToList();

        using (Pace.DataAccess.Contacts.ContactsEntities entity = new Pace.DataAccess.Contacts.ContactsEntities())
        {
            IQueryable<Pace.DataAccess.Contacts.ContactDetails> contactQuery = (from s in entity.ContactDetails.Include("Xlk_Type") where !_current.Contains(s.ContactId) && s.Xlk_Type.TypeId < 5 && s.Xlk_Status.StatusId == 1 orderby s.Xlk_Type.TypeId, s.FirstName ascending, s.SurName ascending select s);

            if (_contactId.HasValue)
            {
                contactQuery = contactQuery.Where(c => c.ContactId == _contactId.Value);
            }

            int TempId = 1;
            foreach (Pace.DataAccess.Contacts.ContactDetails item in contactQuery.ToList())
            {
                if (item.Xlk_Type.TypeId == TempId)
                {
                    sb.AppendFormat("<option value={0}>{1}</option>", item.ContactId, BaseControl.CreateName(item.FirstName, item.SurName));
                    TempId = item.Xlk_Type.TypeId;
                }
                else
                {
                    sb.AppendFormat("<option value={0}>{1}</option>", 0, "-------");
                    TempId = item.Xlk_Type.TypeId;
                }
            }
        }

        return sb.ToString();
    }

    protected string GetBookingId
    {
        get
        {
            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }

}