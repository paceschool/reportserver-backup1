﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddBookingControl.ascx.cs"
    Inherits="Excursions_AddBookingControl" %>
<script type="text/javascript">
    $(function () {


        getMeetTime = function (excursionid, excursiontime, transporttypeid) {
            var params = '{excursionid:' + excursionid + ', excursiontime:"' + excursiontime + '", transporttypeid:' + transporttypeid + '}';
            $.ajax({
                type: "POST",
                url: '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","GetMeetTime") %>',
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $('#MeetTime').val(msg.d.Payload);
                    }
                    else
                        alert(msg.d.ErrorMessage);
                }
                else
                    alert("No Response, please contact admin to check!");
                }
            });
        }

        $('.watcher').change(function () {
            getMeetTime($('#ExcursionId').val(), $('#ExcursionTime option:selected').val(), $('#TransportTypeId option:selected').val());
        });



        getForm = function () {
            return $("#addbooking");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","SaveBooking") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#ExcursionDate").datepicker({ dateFormat: 'dd/mm/yy' });

    });
    
</script>
<form id="addbooking" name="addbooking" method="post" action="ViewBookingPage.aspx/SaveBooking">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="BookingId" value="<%= GetBookingId %>" />
        <input type="hidden" id="GroupExcursionId" value="<%= GetGroupExcursionId %>" />
        <label>
            Title
        </label>
        <input type="text" name="Title" id="Title" value="<%= GetTitle %>" />
        <label>
            Excursion
        </label>
        <select class="watcher" name="Excursion" id="ExcursionId">
            <%= GetExcursionList()%>
        </select>
        <label>
            Excursion Date
        </label>
        <input type="datetext" name="ExcursionDate" id="ExcursionDate" value="<%= GetExcursionDate %>" />
        <label>
            Excursion Time
        </label>
        <select class="watcher" id="ExcursionTime">
            <%= GetExcursionTimeList()%>
        </select>
        <label>
            Transport Type
        </label>
        <select class="watcher" name="Xlk_TransportType" id="TransportTypeId">
            <%= GetTransportTypeList()%>
        </select>
    </fieldset>
    <fieldset>        <label>
            Campus
        </label>
        <select id="Select1">
            <%= GetCampusList()%>
        </select>
            <label>
            Booking Ref
        </label>
        <input type="text" name="BookingRef" id="BookingRef" value="<%= GetBookingRef %>" />
        <label>
            Meet Time
        </label>
        <select id="MeetTime">
            <%= GetMeetTimeList()%>
        </select>
        <label>
            Meet Location
        </label>
        <input type="text" name="MeetLocation" id="MeetLocation" value="<%= GetMeetLocation %>" />
        <label>
            Status
        </label>
        <select name="Xlk_Status" id="StatusId">
            <%= GetStatusList()%>
        </select>
    </fieldset>
    <fieldset>
        <label>
            Qty Students
        </label>
        <input type="text" name="QtyStudents" id="QtyStudents" value="<%= GetQtyStudents %>" />
        <label>
            Qty Leaders
        </label>
        <input type="text" name="QtyLeaders" id="QtyLeaders" value="<%= GetQtyLeaders %>" />
        <label>
            Pace Guides
        </label>
        <input type="text" name="QtyGuides" id="QtyGuides" value="<%= GetQtyGuides %>" />
        <label>
            Closed
        </label>
        <input type="checkbox" id="IsClosed" <%= GetIsClosed %> value="true" />
    </fieldset>
</div>
</form>
