﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddProgrammeTypeGroupsToBookingControl.ascx.cs"
    Inherits="Excursions_AddGroupToBookingControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addlnkstudentexcursion");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","AssignGroupsByProgrammeType") %>';
        }

        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addlnkstudentexcursion" name="addlnkstudentexcursion" method="get" action="ViewBookingPage.aspx/AssignGroupsByProgrammeType">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="BookingId" value="<%= GetBookingId %>" />
        <label>
            Programme Type
        </label>
        <select class="target" id="ProgrammeTypeId">
            <%= GetProgrammeTypeList()%>
        </select>
    </fieldset>
</div>
</form>
