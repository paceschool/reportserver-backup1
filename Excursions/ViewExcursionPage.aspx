﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewExcursionPage.aspx.cs" Inherits="Excursions_ViewExcursionsPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="View Excursion Details Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Excursion Name
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curTitle" runat="server">
                        </asp:Label><%= ExcursionLink(_excursionId, "~/Excursions/ViewExcursionPage.aspx","")%>
                    </td>
                    <th scope="col">
                        Description
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curDescription" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Duration
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curDurationHours" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Venue Guide?
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curVenueGuidesSupplied" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Transport Req?
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curTransportRequired" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Website Promo
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curPromoText" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Type
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curExcursionType" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Capacity (Min/Max)
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curMinCapacity" runat="server">
                        </asp:Label>/<asp:Label ID="curMaxCapacity" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        Location
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curLocation" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        Comment
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curComment" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Actions
                        </label>
                    </th>
                    <td>
                        <a title="Edit Excursion" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewExcursionPage.aspx','Excursions/AddExcursionControl.ascx',{'ExcursionId':<%=_excursionId %>},refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a>&nbsp;
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    <div class="resultscontainer">
        <div id="tabs" style="font-size: smaller">
            <ul>
                <li><a href="ViewExcursionPage.aspx/LoadBookings?id=<%= _excursionId %>">Bookings</a></li>
                <li><a href="ViewExcursionPage.aspx/LoadPastBookings?id=<%= _excursionId %>">Past Bookings</a></li>
                <li><a href="ViewExcursionPage.aspx/LoadTransportMethods?id=<%= _excursionId %>">Transport
                    Methods</a></li>
                <li><a href="ViewExcursionPage.aspx/LoadCosts?id=<%= _excursionId %>">Costs</a></li>
                <li><a href="ViewExcursionPage.aspx/LoadDocuments?id=<%= _excursionId %>">Documents</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
