﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddExcursionCostControl.ascx.cs"
    Inherits="Excursion_AddExcursionCostControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addattendeecostcontrol");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","SaveExcursionCost") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addattendeecostcontrol" name="addattendeecostcontrol" method="post" action="ViewBookingPage.aspx/SaveExcursionCost">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Excursion" id="ExcursionId" value="<%= GetExcursionId %>" />
        <label>
            Attendee Type
        </label>
        <select name="Xlk_AttendeeType" id="AttendeeTypeId">
            <%= GetAttendeeTypeList()%>
        </select>
        <label>
            Cost Per Student
        </label>
        <input type="text" id="CostPerStudent" value="<%= GetCostPerStudent %>" />
        <label>
            Free Per
        </label>
        <input type="text" id="FreePer" value="<%= GetFreePer %>" />
        <label>
            Markup Multiplier
        </label>
        <input type="text" id="MarkupMultiplier" value="<%= GetMarkupMultiplier %>" />
        <label>
            Default Advertised Cost
        </label>
        <input type="text"  id="DefaultAdvertisedCost" value="<%= GetDefaultAdvertisedCost %>" />
    </fieldset>
</div>
</form>
