﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using Pace.DataAccess.Excursion;

public partial class Excursion_AddExcursionCostControl : BaseExcursionControl
{
    protected Int32 _studentId;
    protected ExcursionCost _excursionCost;
    protected Int32 _ticketType;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue<Int32?>("ExcursionId").HasValue && GetValue<byte?>("AttendeeTypeId").HasValue)
            {
                Int32 excursionId = GetValue<Int32>("ExcursionId");
                byte attendeeTypeId = GetValue<byte>("AttendeeTypeId");

                _excursionCost = (from n in entity.ExcursionCosts.Include("Xlk_AttendeeType")
                                  where n.ExcursionId == excursionId && n.Xlk_AttendeeType.AttendeeTypeId == attendeeTypeId
                         select n).FirstOrDefault();
            }
        }
    }

    protected string GetAttendeeTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_excursionCost != null && _excursionCost.Xlk_AttendeeType != null) ? _excursionCost.Xlk_AttendeeType.AttendeeTypeId : (byte?)null;


        foreach (Xlk_AttendeeType item in this.LoadAttendeeTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.AttendeeTypeId, item.Description, (_current.HasValue && item.AttendeeTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }


    protected string GetExcursionId
    {
        get
        {
            if (_excursionCost != null)
                return _excursionCost.ExcursionId.ToString();

            if (GetValue<Int32?>("ExcursionId").HasValue)
                return GetValue<Int32>("ExcursionId").ToString();

            return "0";
        }
    }
    protected string GetCostPerStudent
    {
        get
        {
            if (_excursionCost != null)
                return _excursionCost.CostPerStudent.ToString();


            return "0";
        }
    }
    protected string GetFreePer
    {
        get
        {
            if (_excursionCost != null)
                return _excursionCost.FreePer.ToString();

            return "0";
        }
    }

    protected string GetMarkupMultiplier
    {
        get
        {
            if (_excursionCost != null)
                return _excursionCost.MarkupMultiplier.ToString();

            return "0";
        }
    }
    protected string GetDefaultAdvertisedCost
    {
        get
        {
            if (_excursionCost != null)
                return _excursionCost.DefaultAdvertisedCost.ToString();

            return "0";
        }
    }
}