﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewBookingPage.aspx.cs" Inherits="Excursions_SearchExcursionsPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            refreshPage = function () {
                location.reload();
            }

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="View Excursion Booking Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        <label>
                            Id
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Title
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curTitle" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Booking Ref
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curBookingRef" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Date @Time
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curDate" runat="server">
                        </asp:Label>
                        @
                        <asp:Label ID="curTime" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Transport Type
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curTransportType" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Meet Time/Location
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curMeetTime" runat="server">
                        </asp:Label>
                        <asp:Label ID="curMeetLocation" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Qty Students
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curQtyStudents" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Qty Leaders
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curQtyLeaders" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Qty Guides
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curQtyPaceGuides" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Is Closed
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curIsClosed" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Excursion
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="curExcursion" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Actions
                        </label>
                    </th>
                    <td>
                        <a title="Edit Group" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewExcursionPage.aspx','Excursions/AddBookingControl.ascx',{'BookingId':<%=_bookingId %>},refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a>&nbsp; <a href="<%= ReportPath("ExcursionReports","Excursion_Voucher","PDF", new Dictionary<string, object> { {"BookingId",_bookingId }}) %>"
                                title="Print Voucher" style="font-size: smaller">
                                <img src="../Content/img/actions/printer.png" title="Print Voucher" /></a>&nbsp;
                        <a href="#" title="Create Tasks" onclick="ajaxMessageBox('ViewBookingPage.aspx','ProcessTasks','All tasks complete or incomplete for this Excursion Booking will be deleted, are you sure you wish to continue?', <%=_bookingId %>)">
                            <img src="../Content/img/actions/tools.png" title="Create Tasks" /></a>&nbsp;
                        <a title="Jump to Excursion Family Payments" href="ViewExcursionPage.aspx?ExcursionId=<%= _excursionId %>">
                            <img src="../Content/img/actions/next.png" title="Show Excursion" /></a>
                        &nbsp
                        &nbsp
                        &nbsp
                        &nbsp
                        <a title="Back to Planner" href="PlanBookingsPage.aspx">
                            <img src="../Content/img/actions/back.png" title="Back to Planner" />
                        <a title="Print Attendees" href="<%= ReportPath("ExcursionReports", "Excursion_AttendeeNames", "PDF", new Dictionary<string, object> { { "Date", _excursionDate } } ) %>">
                            <img src="../Content/img/actions/printer.png" title="Print Attendee Names" /></a>&nbsp;
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id="tabs" style="font-size: smaller">
            <ul>
                <% if (_isClosed)
                   { %>
                <li><a href="ViewBookingPage.aspx/LoadBookingClosedGroups?id=<%= _bookingId %>">Closed
                    Excursions</a></li>
                <% }
                   else
                   { %>
                <li><a href="ViewBookingPage.aspx/LoadBookingStudents?id=<%= _bookingId %>">Students</a></li>
                <li><a href="ViewBookingPage.aspx/LoadBookingOpenGroup?id=<%= _bookingId %>">Open Groups</a></li>
                <%  } %>
                <li><a href="ViewBookingPage.aspx/LoadBookingGuides?id=<%=_bookingId %>">Guides</a></li>
                <li><a href="ViewBookingPage.aspx/LoadBookingTasks?id=<%= _bookingId %>">Tasks</a></li>
                <li><a href="ViewBookingPage.aspx/LoadBookingDocs?id=<%= _bookingId %>">Documents</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
