﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PlanBookingsPage.aspx.cs" Inherits="Excursions_PlanBookingsPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            $("#<%= startDate.ClientID %>").datepicker({ minDate: "-1D", maxDate: "+3M +60D", dateFormat: 'dd/mm/yy' });
            $("#<%= endDate.ClientID %>").datepicker({ minDate: "-1D", maxDate: "+3M +60D", dateFormat: 'dd/mm/yy' });

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); }, 
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });

            deleteBooking = function (bookingid) {
                $.ajax({
                    type: "POST",
                    url: "PlanBookingsPage.aspx/DeleteBooking",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{bookingid:" + bookingid + "}",
                    success: function (msg) {
                   if (msg.d != null) {
                        if (msg.d.Success) {
                            refreshCurrentTab();

                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                    }
                });
            }

            confirmBooking = function (bookingid,action) {
                $.ajax({
                    type: "POST",
                    url: "PlanBookingsPage.aspx/ConfirmBooking",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{bookingid:" + bookingid + ", action:'" + action + "'}",
                    success: function (msg) {
                   if (msg.d != null) {
                        if (msg.d.Success) {
                            refreshCurrentTab();
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                    }
                });

            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" Runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Excursion Bookings Planner Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
    Filter excursions by
        <span>start date:
            <asp:TextBox ID="startDate" runat="server"
                AutoPostBack="true">
            </asp:TextBox>
        </span>
        <span>by enddate:
            <asp:TextBox ID="endDate" runat="server"
                AutoPostBack="true">
            </asp:TextBox>
        </span>
        <span>by enddate:
            <asp:CheckBox ID="isOpen" runat="server"
                AutoPostBack="true">
            </asp:CheckBox>
        </span>
    </div>
    <div class="resultscontainer">
       <div id="tabs" style="font-size: smaller">
            <ul>
                <li><a href="PlanBookingsPage.aspx/LoadRequested">To Be Booked</a></li>
                <li><a href="PlanBookingsPage.aspx/LoadToBeConfirmed">To Be Confirmed</a></li>
                <li><a href="PlanBookingsPage.aspx/LoadDeleted">To Be Cancelled</a></li>
                <li><a href="PlanBookingsPage.aspx/LoadConfirmed">Confirmed</a></li>
                <li><a href="PlanBookingsPage.aspx/LoadAll">All</a></li>
            </ul>
        </div>
    </div>
</asp:Content>

