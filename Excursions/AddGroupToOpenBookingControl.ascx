﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupToOpenBookingControl.ascx.cs"
    Inherits="Enrollments_AddGroupToBookingControl" %>
<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addlnkgroupexcursion");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","SaveLnkGroup") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $('.target').change(function () {
            var groupid = $(this).val();
            if (groupid > 0)
                findQuantities(groupid);
        });

        findQuantities = function (groupid) {
            var params = '{groupid:' + groupid + '}';
            $.ajax({
                type: "POST",
                url: "ViewBookingPage.aspx/GetGroup",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            var obj = jQuery.parseJSON(msg.d.Payload);
                            $('#QtyStudents').val(obj.NoOfStudents);
                            $('#QtyLeaders').val(obj.NoOfLeaders);
                    }
                    else
                        alert(msg.d.ErrorMessage);
                }
                else
                    alert("No Response, please contact admin to check!");
                }
            });
        }

        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addlnkgroupexcursion" name="addlnkgroupexcursion" method="post" action="ViewBookingPage.aspx/SaveLnkGroup">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="BookingId" value="<%= GetBookingId %>" />
        <label>
            Group
        </label>
        <select class="target" id="GroupId">
            <%= GetGroupList()%>
        </select>
        <label>
            Qty Leaders
        </label>
        <input type="text" name="QtyLeaders" id="QtyLeaders" value="<%= GetQtyLeaders %>" />
        <label>
            Qty Students
        </label>
        <input type="text" name="QtyStudents" id="QtyStudents" value="<%= GetQtyStudents %>" />
        <label>
            Qty Guides
        </label>
        <input type="text" name="QtyGuides" id="QtyGuides" value="<%= GetQtyGuides %>" />
    </fieldset>
</div>
</form>
