﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddGroupToBookingControl.ascx.cs" 
    Inherits="Enrollments_AddStudentBusTicketControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addlnkgroupexcursion");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","SaveLnkGroupExcursion") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
        
        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addlnkgroupexcursion" name="addlnkgroupexcursion" method="post" action="ViewBookingPage.aspx/SaveLnkGroupExcursion">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="BookingId" value="<%= GetBookingId %>" />
            <label>
                Group
            </label>
            <select id="GroupExcursionId">
                <%= GetGroupList()%>
            </select>
    </fieldset>
</div>
</form>