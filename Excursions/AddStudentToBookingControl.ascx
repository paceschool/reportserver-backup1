﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentToBookingControl.ascx.cs"
    Inherits="Excursions_AddStudentToBookingControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addlnkstudentexcursion");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","SaveLnkStudentExcursion") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });

        $('.target').change(function () {
            var programmetypeid = $(this).val();
            findStudents(programmetypeid);
        });

        findStudents = function (programmetypeid) {
            var params = '{programmetypeid:' + programmetypeid + ', bookingid:' + <%= GetBookingId %> + '}';
            $.ajax({
                type: "POST",
                url: "ViewBookingPage.aspx/GetStudentList",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d.Success) {
                            $('#StudentId').html(msg.d.Payload);
                        }
                        else
                            alert(msg.d.ErrorMessage);
                    }
                    else
                        alert("No Response, please contact admin to check!");
                }
            });
        }


    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addlnkstudentexcursion" name="addlnkstudentexcursion" method="post" action="ViewBookingPage.aspx/SaveLnkStudentExcursion">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="StudentBookingId" value="<%= GetStudentBookingId %>" />
        <input type="hidden" id="BookingId" value="<%= GetBookingId %>" />
        <label>
            Programme Type
        </label>
        <select class="target"  type="ignore">
            <%= GetProgrammeTypeList()%>
        </select>
        <label>
            Student
        </label>
        <select id="StudentId">
            <%= GetStudentList()%>
        </select>
        <label>
            Amount Paid
        </label>
        <input type="text" name="AmountPaid" id="AmountPaid" value="<%= GetAmountPaid %>" />
    </fieldset>
</div>
</form>
