﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

using Pace.DataAccess.Excursion;
using System.Web.Script.Services;

public partial class Excursions_AddStudentToBookingControl : BaseExcursionControl
{
    protected Lnk_Student_Booking _lnk_student_excursion;
    private Int32? _bookingId;
    private Booking _booking;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue<Int32?>("BookingId") .HasValue)
            {
                _bookingId = GetValue<Int32>("BookingId");
                _booking = entity.Bookings.Include("Lnk_Student_Booking").Single(b => b.BookingId == _bookingId);
            }
            else
                if (GetValue<Int32?>("StudentBookingId").HasValue)
                {
                    Int32 studentBookingId = GetValue<Int32>("StudentBookingId");

                    _lnk_student_excursion = (from excursion in entity.Lnk_Student_Booking
                                              where excursion.StudentBookingId == studentBookingId
                                              select excursion).FirstOrDefault();

                    _booking = entity.Bookings.Include("Lnk_Student_Booking").Single(b => b.BookingId == _lnk_student_excursion.BookingId);
                }
        }
    }

    protected string GetProgrammeTypeList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">All</option>");

        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_ProgrammeType> lookupQuery = (from s in entity.Xlk_ProgrammeType select s);
            foreach (Xlk_ProgrammeType item in lookupQuery.ToList())
            {
                sb.AppendFormat("<option value={0}>{1}</option>", item.ProgrammeTypeId, item.Description);
            }

        }

        return sb.ToString();
    }

    protected string GetStudentList()
    {
        StringBuilder sb = new StringBuilder();
        DateTime _bookingDate = _booking.ExcursionDate;
        List<int> _currentIds = _booking.Lnk_Student_Booking.Select(e => e.StudentId).ToList();

        Int32? _current = (_lnk_student_excursion != null) ? _lnk_student_excursion.StudentId : (Int32?)null;


        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Student> studentsQuery = (from s in entity.Student where !s.GroupId.HasValue && !_currentIds.Contains(s.StudentId) && s.ArrivalDate < _bookingDate && s.DepartureDate > _bookingDate && s.CampusId == GetCampusId select s);

            foreach (Student item in studentsQuery.ToList())
            {
                sb.AppendFormat("<option {3} value={0}>{1} {2}</option>", item.StudentId, item.FirstName, item.SurName, (_current.HasValue && item.StudentId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetAmountPaid
    {
        get
        {
            if (_lnk_student_excursion != null)
                return _lnk_student_excursion.AmountPaid.ToString();

            return "0";
        }
    }

    protected string GetStudentBookingId
    {
        get
        {
            if (_lnk_student_excursion != null)
                return _lnk_student_excursion.StudentBookingId.ToString();

            if (Parameters.ContainsKey("StudentBookingId"))
                return Parameters["StudentBookingId"].ToString();

            return "0";
        }
    }

    protected string GetBookingId
    {
        get
        {
            if (_lnk_student_excursion != null)
                return _lnk_student_excursion.BookingId.ToString();

            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }



}