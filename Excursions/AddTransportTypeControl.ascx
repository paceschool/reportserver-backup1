﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddTransportTypeControl.ascx.cs"
    Inherits="Enrollments_AddTransportTypeControl" %>
<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addtransporttypecontrol");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","AddTransportType") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#FromDate").datepicker({ dateFormat: 'dd/mm/yy' });
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addtransporttypecontrol" name="addtransporttypecontrol" method="post" action="ViewBookingPage.aspx/AddTransportType">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ExcursionId" value="<%= GetExcursionId %>" />
        <label>
            Transport Cost Per Seat
        </label>
        <input type="text" name="TransportCostPerSeat" id="TransportCostPerSeat" value="<%= GetTransportCostPerSeat %>" />
        <label>
            Transport Duration Hours
        </label>
        <input type="text" name="TransportDurationHours" id="TransportDurationHours" value="<%= GetTransportDurationHours %>" />
        <label>
            Transport Type
        </label>
        <select name="Xlk_TransportType" id="TransportTypeId">
            <%= GetTransportTypeList()%>
        </select>
        <label>
            supplier
        </label>
        <select id="SupplierId">
            <%= GetSupplierList()%>
        </select>
    </fieldset>
</div>
</form>
