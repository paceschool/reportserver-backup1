﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

using Pace.DataAccess.Group;
using Pace.DataAccess.Excursion;

public partial class Enrollments_AddGroupToBookingControl : BaseExcursionControl
{
    private Int32? _bookingId;
    private Int32? _groupId;
    private Booking _booking;
    private Lnk_Group_Booking _groupBooking;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue<Int32?>("BookingId").HasValue)
            {
                _bookingId = GetValue<Int32>("BookingId");
                _booking = entity.Bookings.Include("Lnk_Group_Booking").Single(b => b.BookingId == _bookingId);
            }
            else
                if (GetValue<Int32?>("BookingId").HasValue && GetValue<Int32?>("GroupId").HasValue)
                {
                    _bookingId = GetValue<Int32>("BookingId");
                    _booking = entity.Bookings.Include("Lnk_Group_Booking").Single(b => b.BookingId == _bookingId);
                    _groupId = GetValue<Int32>("GroupId");

                    _groupBooking = (from excursion in entity.Lnk_Group_Booking
                                     where excursion.BookingId == _bookingId && excursion.GroupId == _groupId
                                     select excursion).FirstOrDefault();
                }
        }

    }

    protected string GetGroupList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">Select</option>");
        DateTime _bookingDate = _booking.ExcursionDate;
        List<int> _current = _booking.Lnk_Group_Booking.Select(e => e.GroupId).ToList();

        using (GroupEntities entity = new GroupEntities())
        {

            IQueryable<Group> groupsQuery = (from s in entity.Group where !_current.Contains(s.GroupId) && s.ArrivalDate < _bookingDate && s.DepartureDate > _bookingDate && s.CampusId == GetCampusId select s);

            if (_groupId.HasValue)
               groupsQuery = groupsQuery.Where(g=>g.GroupId ==_groupId.Value);

            foreach (Group item in groupsQuery.ToList())
            {
                sb.AppendFormat("<option value={0}>{1}</option>", item.GroupId, item.GroupName);
            }
        }

        return sb.ToString();
    }

    protected string GetBookingId
    {
        get
        {
            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }

    protected string GetQtyLeaders
    {
        get
        {

            if (_groupBooking != null && _groupBooking.QtyLeaders.HasValue)
                return _groupBooking.QtyLeaders.Value.ToString();

            return "0";
        }
    }

    protected string GetQtyStudents
    {
        get
        {
            if (_groupBooking != null && _groupBooking.QtyStudents.HasValue)
                return _groupBooking.QtyStudents.Value.ToString();


            return "0";
        }
    }

    protected string GetQtyGuides
    {
        get
        {
            if (_groupBooking != null && _groupBooking.QtyGuides.HasValue)
                return _groupBooking.QtyGuides.Value.ToString();

            return "0";
        }
    }
}