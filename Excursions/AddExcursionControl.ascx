﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddExcursionControl.ascx.cs"
    Inherits="Excursions_AddExcursionControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addexcursion");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewExcursionPage.aspx","SaveExcursion") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#ExcursionDate").datepicker({ dateFormat: 'dd/mm/yy' });

    });
    
</script>
<form id="addexcursion" name="addexcursion" method="post" action="ViewExcursionPage.aspx/SaveExcursion">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ExcursionId" value="<%= GetExcursionId %>" />
        <label>
            Title
        </label>
        <input type="text" id="Title" value="<%= GetTitle %>" />
        <label>
            Contact Method
        </label>
        <select class="watcher" name="Xlk_ContactMethod" id="ContactMethodId">
            <%= GetContactMethodList()%>
        </select>
        <label>
            Excursion Type
        </label>
        <select class="watcher" name="Xlk_ExcursionType" id="ExcursionTypeId">
            <%= GetExcursionTypeList()%>
        </select>
        <label>
            Excursion Duration (hours)
        </label>
        <input type="text" id="DurationHours" value="<%= GetDurationHours %>" />
        <label>
            Supplier
        </label>
        <select class="watcher" name="Supplier" id="SupplierId">
            <%= GetSupplierList()%>
        </select>
        <label>
            Location
        </label>
        <select class="watcher" name="Xlk_Location" id="LocationId">
            <%= GetLocationList()%>
        </select>
    </fieldset>
    <fieldset>
        <label>
            Description
        </label>
        <textarea type="text" id="Description" rows="4" ><%= GetDescription %></textarea>
        <label>
            Promotional Text
        </label>
        <textarea type="text" id="PromoText" rows="6" ><%= GetPromoText %></textarea>
        <label>
            Venue Guide Supplied</label>
        <input type="checkbox" id="VenueGuideSupplied" value="true" <%= GetVenueGuideSupplied %> />
        <label>
            Transport Required</label>
        <input type="checkbox"  id="TransportRequired" value="true" <%= GetTransportRequired %> />
    </fieldset>
    <fieldset>
        <label>
            Promo URI
        </label>
        <input type="text" id="PromoURI" value="<%= GetPromoURI %>" />
        <label>
            Min Capacity
        </label>
        <input type="text" id="MinCapacity" value="<%= GetMinCapacity %>" />
        <label>
            Max Capacity
        </label>
        <input type="text" id="MaxCapacity" value="<%= GetMaxCapacity %>" />
        <label>
            Comment
        </label>
        <input type="text" id="Comment" value="<%= GetComment %>" />
        <label>
            ImageUrl
        </label>
        <input type="text" id="ImageUrl" value="<%= GetImageUrl %>" />
    </fieldset>
</div>
</form>
