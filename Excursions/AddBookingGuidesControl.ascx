﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddBookingGuidesControl.ascx.cs" 
    Inherits="Excursions_AddBookingGuidesControl" %>

<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addbookingguide");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","SaveBookingGuide") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addbookingguide" name="addbookingguide" method="post" action="ViewBookingPage.aspx/SaveBookingGuide">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="BookingId" value="<%= GetBookingId %>" />
                <label>
                    Guide
                </label>
                <select id="ContactId">
                    <%= GetContactList() %>
                </select>
        </fieldset>
    </div>
</form>