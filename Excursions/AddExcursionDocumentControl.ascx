﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddExcursionDocumentControl.ascx.cs"
    Inherits="Excursion_AddExcursionCostControl" %>
<script type="text/javascript">

    $(function () {

        getForm = function () {
            return $("#addattendeecostcontrol");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Excursions/ViewBookingPage.aspx","SaveExcursionDocument") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
    
</script>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addattendeecostcontrol" name="addattendeecostcontrol" method="post" action="ViewBookingPage.aspx/SaveExcursionDocument">
<div class="inputArea">
    <fieldset>
        <input type="hidden" name="Excursion" id="ExcursionId" value="<%= GetExcursionId %>" />
        <label>
            Attendee Type
        </label>
        <select id="DocId">
            <%= GetDocumentsList()%>
        </select>
    </fieldset>
</div>
</form>
