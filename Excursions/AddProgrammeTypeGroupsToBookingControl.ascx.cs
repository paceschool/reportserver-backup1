﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

using Pace.DataAccess.Excursion;
using System.Web.Script.Services;

public partial class Excursions_AddGroupToBookingControl : BaseExcursionControl
{
    protected Lnk_Student_Booking _lnk_student_excursion;
    private Int32? _bookingId;
    private Booking _booking;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue<Int32?>("BookingId") != null)
            {
                _bookingId = GetValue<Int32>("BookingId");
                _booking = entity.Bookings.Include("Lnk_Group_Booking").Single(b => b.BookingId == _bookingId);
            }
        }
    }


    protected string GetProgrammeTypeList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">All</option>");

        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<Xlk_ProgrammeType> lookupQuery = (from s in entity.Xlk_ProgrammeType select s);
            foreach (Xlk_ProgrammeType item in lookupQuery.ToList())
            {
                sb.AppendFormat("<option value={0}>{1}</option>", item.ProgrammeTypeId, item.Description);
            }

        }

        return sb.ToString();
    }


    protected string GetBookingId
    {
        get
        {
            if (_lnk_student_excursion != null)
                return _lnk_student_excursion.BookingId.ToString();

            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }



}