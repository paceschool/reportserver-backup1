﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using Pace.DataAccess.Excursion;

public partial class Excursions_AddExcursionControl : BaseExcursionControl
{
    private Int32? _excursionId;
    private Excursion _excursion;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue<Int32?>("ExcursionId") != null)
            {
                _excursionId = GetValue<Int32>("ExcursionId");
                _excursion = (from excur in entity.Excursions.Include("Xlk_ContactMethod").Include("Xlk_ExcursionType").Include("Supplier").Include("Xlk_Location")
                              where excur.ExcursionId == _excursionId
                              select excur).FirstOrDefault();
            }
        }
    }


            protected string GetExcursionId
    {
        get
        {
            if (_excursion != null)
                return _excursion.ExcursionId.ToString();

            if (_excursionId.HasValue)
                return _excursionId.Value.ToString();

            return "0";
        }
    }

    protected string GetTitle
    {
        get
        {
            if (_excursion != null)
                return _excursion.Title;

            return "";
        }
    }

    protected string GetDurationHours
    {
        get
        {
            if (_excursion != null && _excursion.DurationHours.HasValue)
                return _excursion.DurationHours.Value.ToString();

            return "";
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_excursion != null)
                return _excursion.Description;

            return "";
        }
    }

    protected string GetPromoText
    {
        get
        {
            if (_excursion != null)
                return _excursion.PromoText;

            return "";
        }
    }

    protected string GetPromoURI
    {
        get
        {
            if (_excursion != null)
                return _excursion.PromoURI;

            return "";
        }
    }

    protected string GetComment
    {
        get
        {
            if (_excursion != null)
                return _excursion.Comment;

            return "";
        }
    }

    protected string GetExcursionImage
    {
        get
        {
            if (_excursion != null)
                return _excursion.ExcursionImage;

            return "";
        }
    }

    protected string GetImageUrl
    {
        get
        {
            if (_excursion != null)
                return _excursion.ImageUrl;

            return "";
        }
    }
    protected string GetMaxCapacity
    {
        get
        {
            if (_excursion != null && _excursion.MaxCapacity.HasValue)
                return _excursion.MaxCapacity.Value.ToString();

            return "";
        }
    }
    protected string GetMinCapacity
    {
        get
        {
            if (_excursion != null && _excursion.MinCapacity.HasValue)
                return _excursion.MinCapacity.Value.ToString();

            return "";
        }
    }

    protected string GetVenueGuideSupplied
    {
        get
        {
            if (_excursion != null && _excursion.VenueGuideSupplied)
                return "checked";

            return "";
        }
    }

    protected string GetTransportRequired
    {
        get
        {
            if (_excursion != null && _excursion.TransportRequired)
                return "checked";

            return "";
        }
    }
    protected string GetContactMethodList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_excursion != null && _excursion.Xlk_ContactMethod != null) ? _excursion.Xlk_ContactMethod.ContactMethodId : (Int32?)null;

        foreach (Xlk_ContactMethod item in this.LoadContactMethods())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ContactMethodId, item.Description, (_current.HasValue && item.ContactMethodId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetExcursionTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_excursion != null && _excursion.Xlk_ExcursionType != null) ? _excursion.Xlk_ExcursionType.ExcursionTypeId : (Int32?)null;

        foreach (Xlk_ExcursionType item in this.LoadExcursionTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExcursionTypeId, item.Description, (_current.HasValue && item.ExcursionTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetSupplierList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_excursion != null && _excursion.Supplier != null) ? _excursion.Supplier.SupplierId : (Int32?)null;

        foreach (Supplier item in this.LoadSuppliers())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.SupplierId, item.Description, (_current.HasValue && item.SupplierId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_excursion != null && _excursion.Xlk_Location != null) ? _excursion.Xlk_Location.LocationId : (Int32?)null;

        foreach (Xlk_Location item in this.LoadLocations())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current.HasValue && item.LocationId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

}