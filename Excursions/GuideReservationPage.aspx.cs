﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using Accommodation = Pace.DataAccess.Accommodation;
using Pace.DataAccess.Excursion;

public partial class Excursions_GuidesReservationPage : BaseTuitionPage
{

    private DateTime _modellingDate;
    private DateTime _maxDate = DateTime.Now.AddMonths(6);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            contactsType.DataSource = GetTypes();
            contactsType.DataBind();
            contactsType.SelectedIndex = 2;

            _modellingDate = DateTime.Now;
            BuildBookings();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    #region Private Methods

    private object GetTypes()
    {
        using (Pace.DataAccess.Contacts.ContactsEntities entity = new Pace.DataAccess.Contacts.ContactsEntities())
        {
           return entity.Xlk_Type.Select(p=> new { p.TypeId, Description =p.Xlk_Category.Description + ":- " + p.Description }).ToList();
        }
    }



    protected void Click_ReBuild(object sender, EventArgs e)
    {
        BuildBookings();
    }

    protected void BuildBookings()
    {
        _modellingDate = DateTime.Now;

        using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
        {
            var bookingQuery = from g in entity.Bookings
                               where g.ExcursionDate > _modellingDate && g.QtyGuides > 0 && g.IsClosed
                               orderby g.ExcursionDate
                               select new
                             {
                                 BookingId = g.BookingId,
                                 Title = g.Title,
                                 ExcursionDate = g.ExcursionDate,
                                 ExcursionTime = g.ExcursionTime,
                                 QtyGuides = g.QtyGuides
                             };

            results.DataSource = bookingQuery;
            results.DataBind();
        }
    }

    #endregion

    #region ScriptMethods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> FindTeachers(string contactsearch, byte typeid)
    {
        StringBuilder sb = new StringBuilder();
        Dictionary<int, int> familyIds = new Dictionary<int, int>();

        try
        {
            using (Pace.DataAccess.Contacts.ContactsEntities entity = new Pace.DataAccess.Contacts.ContactsEntities())
            {

                var contactsQuery = (from f in entity.ContactDetails
                                     where f.Xlk_Status.StatusId == 1
                                     select f);

                contactsQuery = contactsQuery.OrderBy(e => e.FirstName).ThenBy(e => e.SurName);


                if (typeid > 0)
                    contactsQuery = contactsQuery.Where(e => e.Xlk_Type.TypeId == typeid);


                if (!string.IsNullOrEmpty(contactsearch))
                    contactsQuery = contactsQuery.Where(e => e.FirstName.Contains(contactsearch) || e.SurName.Contains(contactsearch));


                if (contactsQuery != null && contactsQuery.ToList().Count() > 0)
                {

                    foreach (var item in contactsQuery)
                    {
                        sb.AppendFormat("<div id=\"{1}\" name=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}<a href=\"#\" class=\"highlighter\"  onclick=\"ShowTeacher({1})\"><img src=\"../Content/img/actions/orange_button.png\" alt=\"Highlight Teacher\" style=\"float:right\"/></a></div>", CreateName(item.FirstName, item.SurName), item.ContactId);
                    }
                }
                return new AjaxResponse<string>(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> BuildBookingStaff(object bookingid)
    {
        List<int> bookingStaffList;
        int _bookingId = Convert.ToInt32(bookingid);
        StringBuilder sb = new StringBuilder();
        try
        {

            using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
            {
                bookingStaffList = (
                    from f in entity.BookingStaffs
                    where f.BookingId == _bookingId
                    select f.ContactId).ToList();
            }

            using (Pace.DataAccess.Contacts.ContactsEntities centity = new Pace.DataAccess.Contacts.ContactsEntities())
            {
                var staffList = (from f in centity.ContactDetails
                                 where bookingStaffList.Contains(f.ContactId)
                                 select f).ToList();
                int i = 1;

                foreach (Pace.DataAccess.Contacts.ContactDetails staff in staffList)
                {
                    sb.AppendFormat("<div id=\"f_{1}-g_{2}\" name=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{3}. {0}<a href=\"#\" onclick=\"DeleteBookingStaff({1},{2})\"><img src=\"../Content/img/actions/delete.png\" alt=\"Remove from Group\" style=\"float:right\" id=\"DeleteGrouping\"/></a><a href=\"#\" class=\"highlighter\" onclick=\"ShowTeacher({1})\"><img src=\"../Content/img/actions/orange_button.png\" alt=\"Highlight Family\" style=\"float:right\" /></a></div>", CreateName(staff.FirstName, staff.SurName), staff.ContactId, bookingid, i++);
                }
            }
            return new AjaxResponse<string>(sb.ToString());
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse AssignBookingStaff(int contactid, int bookingid)
    {
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                BookingStaff newbookingstaff = BookingStaff.CreateBookingStaff(bookingid, contactid);
                entity.AddToBookingStaffs(newbookingstaff);

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();
                else
                    return new AjaxResponse(new Exception("Could not save this change"));
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse DeleteBookingStaff(int contactid, int bookingid)
    {
        try
        {
            using (ExcursionEntities entity = new ExcursionEntities())
            {
                var bookingstaff = (from g in entity.BookingStaffs
                                where g.ContactId == contactid && g.BookingId == bookingid
                                select g).FirstOrDefault();

                if (bookingstaff != null)
                    entity.DeleteObject(bookingstaff);


                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();
                else
                    return new AjaxResponse(new Exception("Could not remove assignment"));
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    #endregion
}