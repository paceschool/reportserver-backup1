﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using Pace.DataAccess.Excursion;

public partial class Excursion_AddExcursionCostControl : BaseExcursionControl
{
    protected Int32 _studentId;
    protected ExcursionDocument _excursionDocument;
    protected Int32 _ticketType;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue<Int32?>("ExcursionId").HasValue && GetValue<Int32?>("AttendeeTypeId").HasValue)
            {
                Int32 excursionId = GetValue<Int32>("ExcursionId");
                byte docId = GetValue<byte>("DocId");

                _excursionDocument = (from n in entity.ExcursionDocuments
                                      where n.ExcursionId == excursionId && n.DocId == docId
                         select n).FirstOrDefault();
            }
        }
    }

    protected string GetDocumentsList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_excursionDocument != null) ? _excursionDocument.DocId : (int?)null;

        using (Pace.DataAccess.Documents.DocumentsEntities entity = new Pace.DataAccess.Documents.DocumentsEntities())
        {
            var documentSearchQuery = (from d in entity.Documents
                                       where d.Xlk_Type.Xlk_Category.CategoryId == 6 //category type of excursion
                                       select d).ToList();


            foreach (Pace.DataAccess.Documents.Documents item in documentSearchQuery)
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.DocId, item.Title, (_current.HasValue && item.DocId == _current) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();
    }


    protected string GetExcursionId
    {
        get
        {
            if (_excursionDocument != null)
                return _excursionDocument.ExcursionId.ToString();

            if (GetValue<Int32?>("ExcursionId").HasValue)
                return GetValue<Int32>("ExcursionId").ToString();

            return "0";
        }
    }
 }