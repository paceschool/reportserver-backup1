﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Excursion;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;
using Pace.DataAccess.Documents;

public partial class Excursions_ViewExcursionsPage : BaseExcursionPage
{
    #region Events
    protected Int32 _excursionId;
    protected string excursionTitle;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["ExcursionId"]))
            _excursionId = Convert.ToInt32(Request["ExcursionId"]);

        if (!Page.IsPostBack)
        {

            DisplayExcursion(LoadExcursion(CreateEntity, _excursionId));
        }
    }

    public override string TextValue() // Page Name
    {
        newPageName.Text = "View Excursion Details - " + excursionTitle;
        return newPageName.Text;
    }

    private Excursion GetExcursion(ExcursionEntities entity)
    {
        return LoadExcursion(entity, _excursionId);
    }

    private void SaveVenue(ExcursionEntities entity, Excursion venue)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayExcursion(LoadExcursion(entity, _excursionId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }

  
    #endregion

    #region Private Methods   

    private Dictionary<string, string> LoadTimes() //Duration DropDownList
    {
        Dictionary<string, string> times = new Dictionary<string, string>();

        int i = 0;

        while (i < 24)
        {
            i++;
            times.Add(i.ToString(), string.Format("{0} hour(s)", i));
        }
        return times;
    }

    private Excursion LoadExcursion(ExcursionEntities entity, Int32 excursionId)
    {
        IQueryable<Excursion> venueQuery = from v in entity.Excursions.Include("Supplier").Include("Xlk_ContactMethod").Include("Xlk_ExcursionType").Include("Xlk_Location")
                                           where v.ExcursionId == excursionId
                                           select v;
        if (venueQuery.ToList().Count() > 0)
            return venueQuery.ToList().First();

        return null;
    }

    private void DisplayExcursion(Excursion excursion)
    {
        if (excursion != null)
        {
            curId.Text = excursion.ExcursionId.ToString();
            curTitle.Text = TrimText(excursion.Title, 40); excursionTitle = excursion.Title;
            curDescription.Text = TrimText(excursion.Description, 40);
            curDurationHours.Text = (excursion.DurationHours.HasValue)?excursion.DurationHours.Value.ToString() : "Non Specified";
            if (excursion.Xlk_ExcursionType != null)
                curExcursionType.Text = excursion.Xlk_ExcursionType.Description;
            curMaxCapacity.Text =  (excursion.MaxCapacity.HasValue)? excursion.MaxCapacity.Value.ToString(): "No Max";
            curPromoText.Text = TrimText(excursion.PromoText,40);
            curTransportRequired.Text = (excursion.TransportRequired) ? "Yes":"No";
            curVenueGuidesSupplied.Text = (excursion.VenueGuideSupplied) ? "Yes" : "No";
            curMinCapacity.Text = (excursion.MinCapacity.HasValue) ? excursion.MinCapacity.Value.ToString() : "0";
            curLocation.Text = excursion.Xlk_Location.Description;
            curComment.Text = excursion.Comment;

        }
    }

  [WebMethod]
  [ScriptMethod(UseHttpGet=true,ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookings(int id)
  {
      DateTime _now = DateTime.Now.Date;

      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}\"><img src=\"../Content/img/actions/printer.png\" />Print Listing</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewExcursionPage.aspx','Excursions/AddBookingControl.ascx',{{'ExcursionId':{0}}})\"><img src=\"../Content/img/actions/add.png\" />Add Booking</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\"> Id </th><th scope=\"col\"> Title </th><th scope=\"col\">Booking Ref</th><th scope=\"col\"> Date - Time </th><th scope=\"col\"> Transport Type </th><th scope=\"col\"> Meet Time </th><th scope=\"col\"> Meet Location </th><th scope=\"col\"> Confirmed Date <th scope=\"col\"> Confirmed By </th><th scope=\"col\"> Voucher Id </th><th scope=\"col\"> Qty Pace Guides </th><th scope=\"col\"> Actions </th></tr></thead><tbody >", id, ReportPath("ExcursionReports", "ExcursionBookingDetails", "HTML4.0", new Dictionary<string, object> { { "ExcursionId", id } })));

          using (ExcursionEntities entity = new ExcursionEntities())
          {

              List<Booking> query = (from n in entity.Bookings.Include("Xlk_TransportType")
                                     where n.Excursion.ExcursionId == id && n.ExcursionDate >= _now
                                     orderby n.ExcursionDate
                                     select n).ToList();

              if (query.Count() > 0)
              {
                  foreach (var booking in query.ToList())
                  {
                      sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td>{10}</td><td>{2} - {3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td></td><td>{9}</td><td></td></tr>", booking.BookingId, TrimText(booking.Title, 30), booking.ExcursionDate.ToString("ddd dd/MM/yyyy"), booking.ExcursionTime, booking.Xlk_TransportType.Description, booking.MeetTime, booking.MeetLocation, (booking.ConfirmedDate.HasValue) ? booking.ConfirmedDate.Value.ToString("dd/MM/yyyy") : "Not Confirmed", booking.ConfirmedBy, booking.QtyGuides,booking.BookingRef);
                  }
              }
              else
              {
                  sb.Append("<tr><td colspan=\"12\">No Bookings to Show!</td></tr>");
              }
          }
          
          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }

  
  [WebMethod]
  [ScriptMethod(UseHttpGet=true,ResponseFormat = ResponseFormat.Json)]
  public static string LoadTransportMethods(int id)
  {
      DateTime _now = DateTime.Now.Date;

      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddTransportTypeControl.ascx',{{'ExcursionId':{0}}})\"><img src=\"../Content/img/actions/add.png\" />Add Transport</a></span> <table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Transport Type</th><th scope=\"col\">Cost PerSeat</th><th scope=\"col\">Travel Time (Hours)</th><th scope=\"col\">Supplier</th><th scope=\"col\">Actions</th></tr></thead><tbody >", id));

          using (ExcursionEntities entity = new ExcursionEntities())
          {

              List<ExcursionTransportType> query = (from n in entity.ExcursionTransportTypes.Include("Xlk_TransportType").Include("Supplier")
                                     where n.ExcursionId == id
                                     orderby n.TransportTypeId
                                     select n).ToList();
              if (query.Count() > 0)
              {
                  foreach (var transporttype in query.ToList())
                  {
                      sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td></td></tr>", transporttype.ExcursionId, transporttype.Xlk_TransportType.Description, transporttype.TransportCostPerSeat, transporttype.TransportDurationHours, (transporttype.Supplier != null) ? transporttype.Supplier.Title : "No Preferred");
                  }
              }
              else
              {
                  sb.Append("<tr><td colspan=\"11\">No Transport Options to Show!</td></tr>");
              }
          }
          
          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }

  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadCosts(int id)
  {
      DateTime _now = DateTime.Now.Date;

      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddExcursionCostControl.ascx',{{'ExcursionId':{0}}})\"><img src=\"../Content/img/actions/add.png\" />Add Cost</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Attendee Type</th><th scope=\"col\">Cost Per Student</th><th scope=\"col\">Free Per</th><th scope=\"col\">Markup Multiplier</th><th>Default Advertised Cost</th><th scope=\"col\">Actions</th></tr></thead><tbody >", id));

          using (ExcursionEntities entity = new ExcursionEntities())
          {

              List<ExcursionCost> query = (from n in entity.ExcursionCosts.Include("Xlk_AttendeeType")
                                                     where n.ExcursionId == id
                                                     orderby n.Xlk_AttendeeType.Description
                                                     select n).ToList();
              if (query.Count() > 0)
              {
                  foreach (var cost in query.ToList())
                  {
                      sb.AppendFormat("<tr><td>{0}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddExcursionCostControl.ascx',{{ ExcursionId: {0}, AttendeeTypeId: {1}}})\"><img src=\"../Content/img/actions/edit.png\" /></a>&nbsp;<a title=\"Remove Cost from Excursion\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewExcursionPage.aspx','ExcursionCost',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", cost.ExcursionId, cost.Xlk_AttendeeType.AttendeeTypeId, cost.Xlk_AttendeeType.Description, cost.CostPerStudent, cost.FreePer, cost.MarkupMultiplier, cost.DefaultAdvertisedCost);
                  }
              }
              else
              {
                  sb.Append("<tr><td colspan=\"11\">No Transport Options to Show!</td></tr>");
              }
          }

          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }


  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadDocuments(int id)
  {
      DateTime _now = DateTime.Now.Date;
      List<ExcursionDocument> query;
      List<Documents> documentQuery;

      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddExcursionDocumentControl.ascx',{{'ExcursionId':{0}}})\"><img src=\"../Content/img/actions/add.png\" />Add Document</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Doc Id</th><th scope=\"col\">Title</th><th scope=\"col\">Description</th><th scope=\"col\">File Type</th><th>Doc Type</th><th scope=\"col\">Actions</th></tr></thead><tbody >", id));

          using (ExcursionEntities entity = new ExcursionEntities())
          {
              query = (from n in entity.ExcursionDocuments
                       where n.ExcursionId == id
                       select n).ToList();
          }

          using (Pace.DataAccess.Documents.DocumentsEntities dentity = new Pace.DataAccess.Documents.DocumentsEntities())
          {
              List<int> docids = query.Select(s => s.DocId).ToList();

              documentQuery = (from d in dentity.Documents.Include("Xlk_Type").Include("Xlk_FileType")
                               where docids.Contains(d.DocId)
                               select d).ToList();
          }

          if (documentQuery.Count() > 0)
          {
              foreach (var cost in documentQuery.ToList())
              {
                  sb.AppendFormat("<tr><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td><a title=\"Click to open Instruction sheet\" target=\"_blank\" href=\"{8}\"><img src=\"../Content/img/actions/map_magnify.png\"></a>&nbsp;<a title=\"Remove Document from Excursion\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewExcursionPage.aspx','ExcursionDocument',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", id, cost.DocId, cost.Title, cost.Description, cost.Xlk_FileType.Description, cost.Xlk_Type.Description, cost.FileName, cost.Xlk_FileType.Extension, cost.Path);
              }
          }
          else
            {
                  sb.Append("<tr><td colspan=\"7\">No Documents to Show!</td></tr>");
            }


          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }

  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadPastBookings(int id)
  {
      DateTime _now = DateTime.Now.Date;

      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\"> Id </th><th scope=\"col\"> Title </th><th scope=\"col\">Booking Ref</th><th scope=\"col\"> Date - Time </th><th scope=\"col\"> Transport Type </th><th scope=\"col\"> Meet Time </th><th scope=\"col\"> Meet Location </th><th scope=\"col\"> Confirmed Date <th scope=\"col\"> Confirmed By </th><th scope=\"col\"> Voucher Id </th><th scope=\"col\"> Qty Pace Guides </th><th scope=\"col\"> Actions </th></tr></thead><tbody >", id));

          using (ExcursionEntities entity = new ExcursionEntities())
          {

              List<Booking> query = (from n in entity.Bookings.Include("Xlk_TransportType")
                                     where n.Excursion.ExcursionId == id && n.ExcursionDate < _now
                                     orderby n.ExcursionDate
                                     select n).ToList();
              if (query.Count() > 0)
              {
                  foreach (var booking in query.ToList())
                  {
                      sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td>{10}</td><td>{2} - {3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td></td><td>{9}</td><td></td></tr>", booking.BookingId, TrimText(booking.Title, 30), booking.ExcursionDate.ToString("dd/MM/yyyy"), booking.ExcursionTime, booking.Xlk_TransportType.Description, booking.MeetTime, booking.MeetLocation, (booking.ConfirmedDate.HasValue) ? booking.ConfirmedDate.Value.ToString("dd/MM/yyyy") : "Not Confirmed", booking.ConfirmedBy, booking.QtyGuides, booking.BookingRef);
                  }
              }
              else
              {
                  sb.Append("<tr><td colspan=\"12\">No Bookings to Show!</td></tr>");
              }
          }

          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }
    #endregion
}
