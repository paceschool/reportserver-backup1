﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="ExcursionsPage.aspx.cs" Inherits="ViewVenuePage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Excursions Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Browse By Venue
                    </td>
                    <td>
                        <asp:DropDownList ID="excursionlist" runat="server" DataTextField="Title" DataValueField="ExcursionId"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="All Excursions" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Or Search
                    </td>
                    <td>
                        <asp:TextBox ID="searchvenue" runat="server" />
                    </td>
                    <td>
                        <asp:LinkButton ID="venueclick" runat="server" class="button" OnClick="Search_Click"><span>Search</span>
                        </asp:LinkButton>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span style="float: right;">
            <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
            </asp:Label>
        </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Excursion Name
                    </th>
                    <th scope="col">
                        Description
                    </th>
                    <th scope="col">
                        Duration (Hours)
                    </th>
                    <th scope="col">
                        Venue Guide?
                    </th>
                    <th scope="col">
                        Transport Req?
                    </th>
                    <th scope="col">
                        Website Promo
                    </th>
                    <th scope="col">
                        Cost Per Student
                    </th>
                    <th scope="col">
                        Max Capacity
                    </th>
                    <th scope="col">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "ExcursionId") %>
                            </td>
                            <td style="text-align: left">
                                <%# ExcursionLink(DataBinder.Eval(Container.DataItem, "ExcursionId"), "~/Excursions/ViewExcursionPage.aspx", DataBinder.Eval(Container.DataItem, "Title").ToString())%>
                            </td>
                            <td style="text-align: left">
                                <a href="#" class="information">
                                    <%#TrimText(DataBinder.Eval(Container.DataItem, "PromoText"),20) %><span><%#DataBinder.Eval(Container.DataItem, "PromoText")%></span></a>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "DurationHours") %>
                            </td>
                            <td style="text-align: center">
                                <asp:Image runat="server" ID="guideyesno" Visible='<%#Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "VenueGuideSupplied")) %>'
                                    ImageUrl="~/Content/img/actions/thumb-up.png" />
                            </td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:HyperLink ID="weblink" Text='<%#TrimText(DataBinder.Eval(Container.DataItem, "PromoURI"), 20) %>'
                                    runat="server" NavigateUrl='<%#DataBinder.Eval(Container.DataItem, "PromoURI") %>'
                                    Target="_blank">
                                </asp:HyperLink>
                            </td>
                            <td style="text-align: left">
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "MaxCapacity") %>
                            </td>
                            <td style="text-align: center">
                                <asp:ImageButton ID="delvenue" runat="server" ImageUrl="~/Content/img/actions/bin_closed.png"
                                    ToolTip="Delete" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
