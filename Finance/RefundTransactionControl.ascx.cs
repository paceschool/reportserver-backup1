﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Finance;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;

public partial class Student_AddStudentOrderedItem : BaseEnrollmentControl
{
    protected Int64 _transactionId;
    protected Transaction _transaction;
    protected Int32 _studentOrderedItemId;
    protected Int32 _groupOrderedItemId;
    protected string _description;


    protected void Page_Load(object sender, EventArgs e)
    {
        using (FinanceEntities entity = new FinanceEntities())
        {
            if (GetValue<Int64?>("TransactionId").HasValue)
            {
                _transactionId = GetValue<Int64>("TransactionId");
                _transaction = (from t in entity.Transactions.Include("Postings").Include("Postings.Account").Include("Postings.Account.AccountType")
                               where t.TransactionId == _transactionId
                               select t).FirstOrDefault();
            }

            if (GetValue<Int32?>("StudentOrderedItemId").HasValue)
            {
                _studentOrderedItemId = GetValue<Int32>("StudentOrderedItemId");
            }
            if (GetValue<Int32?>("GroupOrderedItemId").HasValue)
            {
                _groupOrderedItemId = GetValue<Int32>("GroupOrderedItemId");
            }
            if (GetValue<string>("Description") != null)
            {
                _description = GetValue<string>("Description").ToString();
            }
        }

    }

    protected string GetComputer
    {
        get
        {
            return GetComputerName(Request.ServerVariables["remote_addr"]);
        }
    }


    protected string GetTransactionId
    {
        get
        {


            if (_transaction != null)
                return _transaction.TransactionId.ToString();

            return "0";
        }
    }

    protected string GetStudentOrderedItemId
    {
        get
        {
            if (_studentOrderedItemId != null)
                return _studentOrderedItemId.ToString();

            return "0";
        }
    }

    protected string GetGroupOrderedItemId
    {
        get
        {
            if (_groupOrderedItemId != null)
                return _groupOrderedItemId.ToString();

            return "0";
        }
    }

    protected string GetDescription
    {
        get
        {
            if (!string.IsNullOrEmpty(_description))
                return _description;

            return "";
        }
    }

    protected string GetAmount
    {
        get
        {
            if (_transaction != null)
                return _transaction.Postings.Where(p=>p.Account.AccountTypeId ==1).Select(t=>t.PostingAmount).FirstOrDefault().ToString();

            return "0.00";
        }
    }

}
