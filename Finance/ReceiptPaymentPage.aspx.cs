﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Finance;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Finance_ReceiptPayment : BaseFinancePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadTransctions();
            LoadAccounts();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected string BuildPopUp(object reference)
    {

        if (reference != null)
        {
            var strings = reference.ToString().Split(':');

            if (strings.Count() > 0)
            {
                object id = strings[1];

                if (strings[0].Contains("StudentOrderedItemId"))
                    return PopUpLink("OrderedItemId", id, "~/Enrollments/ViewStudentPage.aspx", "", "PopupOrderedItem");

                if (strings[0].Contains("GroupOrderedItemId"))
                    return PopUpLink("OrderedItemId", id, "~/Groups/ViewGroupPage.aspx", "", "PopupOrderedItem");

            }
        }

        return string.Empty;
    }

    private void LoadTransctions()
    {
        using (FinanceEntities entity = new FinanceEntities())
        {
            IQueryable<Transaction> transctionsQuery = from t in entity.Transactions.Include("Postings").Include("PostingType").Include("Xlk_PaymentMethod")
                                                       orderby t.TransactionDatetime descending
                                                       select t;

            resultstransactions.DataSource = transctionsQuery.ToList();
            resultstransactions.DataBind();
        }
    }

    private void LoadAccounts()
    {
        using (FinanceEntities entity = new FinanceEntities())
        {
            IQueryable<Account> accountsQuery = from a in entity.Accounts.Include("AccountType")
                                                   orderby a.AccountId descending
                                                   select a;

            resultsaccounts.DataSource = accountsQuery.ToList();
            resultsaccounts.DataBind();
        }
    }



    #region Ajax Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetTransctions()
    {
        using (FinanceEntities entity = new FinanceEntities())
        {
            IQueryable<Transaction> transctionsQuery = from t in entity.Transactions.Include("Postings")
                                                       orderby t.TransactionDatetime descending
                                                       select t;

            
        }
        return string.Empty;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveBusTicketReceipt(Dictionary<string, object> newObject)
    {
        byte postingTypeId = 1; //1	Bus Ticket

        try
        {
            if (newObject["AmountReceived"] != null)
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    if (newObject["StudentId"] != null)
                    {
                        try
                        {
                            int batchid = 0;
                            using (Pace.DataAccess.Support.SupportEntities sentity = new Pace.DataAccess.Support.SupportEntities())
                            {
                                   Pace.DataAccess.Support.CheckBusTicket_Result result = sentity.CheckBusTicket( newObject["TicketNumberFrom"].ToString(), Convert.ToInt16(newObject["BusTicketTypeId"])).FirstOrDefault();
                                   if (result.AlreadySold.HasValue && result.AlreadySold.Value)
                                       return new AjaxResponse(new Exception("This ticket was previously sold"));

                                if (!result.BusTicketBatchId.HasValue)
                                    return new AjaxResponse(new Exception("Could not find the batch for this ticket"));

                                   batchid = result.BusTicketBatchId.Value;

                            }
                            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
                            {

                            }
                        }

                        catch (Exception ex)
                        {
                            scope.Dispose();
                            return new AjaxResponse(ex);
                        }

                    }



                    if (newObject["GroupId"] != null)
                    {
                        try
                        {
                            scope.Complete();
                            return new AjaxResponse();
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            return new AjaxResponse(ex);
                        }
                    }

                    scope.Dispose();
                }
                 return new AjaxResponse(new Exception("Validation Errors"));
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveBookReceipt(Dictionary<string, object> newObject)
    {
        byte postingTypeId = 1; //1	Bus Ticket

        try
        {
            if (newObject["AmountReceived"] != null)
            {
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    if (newObject["StudentId"] != null)
                    {
                        try
                        {
                            int batchid = 0;
                            using (Pace.DataAccess.Support.SupportEntities sentity = new Pace.DataAccess.Support.SupportEntities())
                            {
                                   Pace.DataAccess.Support.CheckBusTicket_Result result = sentity.CheckBusTicket( newObject["TicketNumberFrom"].ToString(), Convert.ToInt16(newObject["BusTicketTypeId"])).FirstOrDefault();
                                   if (result.AlreadySold.HasValue && result.AlreadySold.Value)
                                        return new AjaxResponse(new Exception("This ticket was previously sold"));

                                if (!result.BusTicketBatchId.HasValue)
                                     return new AjaxResponse(new Exception("Could not find the batch for this ticket"));

                                   batchid = result.BusTicketBatchId.Value;

                            }
                            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
                            {
                                //Pace.DataAccess.Enrollment.AssignedTicket ticket = new Pace.DataAccess.Enrollment.AssignedTicket();
                            
                                //ticket.FromDate = Convert.ToDateTime(newObject["StartDate"]);
                                //ticket.BusTicketBatchReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".BusTicketBatch", "BusTicketBatchId", batchid);
                                //ticket.SerialNumber = newObject["TicketNumberFrom"].ToString();
                                //ticket.StudentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Student", "StudentId", Convert.ToInt32(newObject["StudentId"]));
                                //ticket.Xlk_BusTicketTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_BusTicketType", "BusTicketTypeId", Convert.ToInt16(newObject["BusTicketTypeId"]));

                                //entity.AddToAssignedTickets(ticket);

                                //if (entity.SaveChanges() > 0)
                                //{
                                //    ticket.PaidByTransactionId = BaseFinancePage.PostTransaction(string.Format("AssignedTicketId:{0}", ticket.AssignedTicketId), false, postingTypeId, 0, Convert.ToByte(newObject["PaymentTypeId"]), HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], Convert.ToDecimal(newObject["AmountReceived"]));

                                //    if (entity.SaveChanges() > 0)
                                //        scope.Complete();

                                //    return string.Empty;
                                //}
                            }
                        }

                        catch (Exception ex)
                        {
                            scope.Dispose();
                            return new AjaxResponse(ex);
                        }

                    }



                    if (newObject["GroupId"] != null)
                    {
                        try
                        {
                            scope.Complete();
                            return new AjaxResponse();
                        }
                        catch (Exception ex)
                        {
                            scope.Dispose();
                            return new AjaxResponse(ex);
                        }
                    }

                    scope.Dispose();
                }
                return new AjaxResponse(new Exception("Validation Errors"));
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }
    
    #endregion


}