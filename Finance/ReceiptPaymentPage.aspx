﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReceiptPaymentPage.aspx.cs" Inherits="Finance_ReceiptPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            font-style: italic;
            font-weight: bold;
            font-size: larger;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("#tabs").tabs();

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Accounts & Receipts Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <object id="XEncrypt" classid="CLSID:D24C5D1A-8205-4A19-9ED4-698A8BAFD314" codebase="../Bin/PaceReceiptPrinter.dll">
    </object>
    <div id="searchcontainer">
    </div>
     <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="#transactions">Transactions</a></li>
                <li><a href="#accounts">Accounts</a></li>
            </ul>
            <div id="transactions">
                <table id="tbltransaction" class="box-table-a tablesorter">
                    <thead>
                        <tr>
                            <th scope="col">
                                Id
                            </th>
                            <th scope="col">
                                Ref
                            </th>
                            <th scope="col">
                                Date
                            </th>
                            <th scope="col">
                                Posting Type
                            </th>
                            <th scope="col">
                                Payment Method
                            </th>
                            <th scope="col">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultstransactions" runat="server" EnableViewState="True">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "TransactionId")  %>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Reference")%>&nbsp;<%#  BuildPopUp(DataBinder.Eval(Container.DataItem, "Reference"))%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "TransactionDatetime")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "PostingType.Description")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "Xlk_PaymentMethod.Description")%>
                                    </td>
                                   <td style="text-align: left">
                                        <a href="#" onclick="printReceipt(<%# DataBinder.Eval(Container.DataItem, "TransactionId")  %>,'***Reprint***')"><img src="../Content/img/actions/printer.png" /></a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div id="accounts">
                <table id="tblaccounts" class="box-table-a" class="tablesorter">
                    <thead>
                        <tr>
                            <th scope="col">
                                Id
                            </th>
                            <th scope="col">
                                Type
                            </th>
                            <th scope="col">
                                Name
                            </th>
                            <th scope="col">
                                Balance
                            </th>
                            <th scope="col">
                               Description
                            </th>
                            <th scope="col">
                                Credit Only
                            </th>
                            <th scope="col">
                                Status
                            </th>
                            <th scope="col">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="resultsaccounts" runat="server" EnableViewState="True">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "AccountId")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "AccountType.Description")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "AccountName")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "LastCalculatedBalance")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "AccountDescription")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "CanOnlyCredit")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%#DataBinder.Eval(Container.DataItem, "AccountStatusId")%>
                                    </td>
                                    <td style="text-align: left">
                                        <a href="#" onclick="printReceipt(<%# DataBinder.Eval(Container.DataItem, "AccountId")  %>,'***Reprint***')">
                                            <img src="../Content/img/actions/printer.png" /></a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
