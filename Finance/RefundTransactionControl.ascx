﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RefundTransactionControl.ascx.cs"
    Inherits="Student_AddStudentOrderedItem" %>
<script type="text/javascript">
    $(function () {

        var unitCosts = {};

        getForm = function () {
            return $("#addstudentordereditem");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Finance/ReceiptPaymentPage.aspx","RefundTransaction") %>';
        }

        getLocalRules = function () {
            var localrules = {};
            localrules = {
                Qty: {
                    required: true,
                    minlength: 1,
                    number: true,
                    minStrict: 0
                },
                StartDate: {
                    required: true,
                    minlength: 6
                },
                AmountPaid: {
                    number: true,
                    minlength: 1,
                    required: true
                },
                TicketNumberFrom: {
                    required: true,
                    minlength: 4
                },
                OrderableItem: {
                    selectcheck: true
                },
                Xlk_PaymentMethod: {
                    selectcheck: true
                }
            }
            return localrules;
        }

        getLocalMessages = function () {
            var localmessages = {};
            localmessages = {
                Qty: {
                    required: "Please provide a quantity",
                    minlength: "The quantity must be greater than 0"
                },
                StartDate: {
                    required: "Please select start date for ticket",
                    minlength: "The date must be valid"
                },
                TicketNumberFrom: {
                    required: "Please enter the ticket number",
                    minlength: "The ticket number must be part of a batch"
                },
                TicketNumberTo: {
                    required: "Please enter the ticket number",
                    minlength: "The ticket number must be part of a batch"
                },
                AmountPaid: {
                    required: "Please enter the amount paid",
                    minlength: "This value must be at greater than 0"
                },
                ItemId: {
                    required: "Please select a payment method",
                    minlength: "Please select a payment method"
                },
                Xlk_PaymentMethod: {
                    required: "Please select a payment method",
                    minlength: "Please select a payment method"
                }
            }
            return localmessages;
        }

    });
    
</script>
<style>
    label.error
    {
        float: right;
        color: red;
        padding-left: .5em;
        vertical-align: top;
    }
    p
    {
        clear: both;
    }
    .submit
    {
        margin-left: 12em;
    }
    em
    {
        font-weight: bold;
        padding-right: 1em;
        vertical-align: top;
    }
</style>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addstudentordereditem" name="addstudentordereditem" method="post" action="ViewAccommodationPage.aspx/SaveStudentOrderedItem">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="TransactionId" value="<%= GetTransactionId %>" />
        <input type="hidden" id="StudentOrderedItemId" value="<%= GetStudentOrderedItemId %>" />
        <input type="hidden" id="GroupOrderedItemId" value="<%= GetGroupOrderedItemId %>" />
        <label>
            Description
        </label>
        <input disabled type="text" id="Description" value="<%= GetDescription %>" />
        <label>
            Amount
        </label>
        <input disabled type="text" id="Amount" value="<%= GetAmount %>" />
    </fieldset>
</div>
</form>
