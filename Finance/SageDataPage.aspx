﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SageDataPage.aspx.cs" Inherits="SageDataPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            $("#dialog:ui-dialog").dialog("destroy");

            $("#<%= repeat.ClientID %>").buttonset();

            showOverview = function (id) {
                $(id).css({ 'background-color': '#E0FFFF' }).dialog("open");
            };
            $("#<%= FromDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<%= ToDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

        });
    </script>
    <style type="text/css">
        #toolbar
        {
            padding: 10px 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="SAGE Invoices Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        <i>Search by:</i><br /><b>Month, Agency, Period, Type, Text Search</b><br /><i>Or a combination of each:</i><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        .
                    </td>
                </tr>
                <tr>
                    <td style="font-size:large">
                        From:
                        <asp:TextBox ID="FromDate" runat="server" ToolTip="From" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="X-Large">
                        </asp:TextBox>
                    </td>
                    <td style="font-size:large">
                        To:
                        <asp:TextBox ID="ToDate" runat="server" ToolTip="To" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="X-Large">
                        </asp:TextBox>
                    </td>
                    <td>
                        Search (Agency, SageRef)
                        <asp:TextBox ID="searchstring" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" Font-Size="Large" OnTextChanged="lookupRecords">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        .
                    </td>
                </tr>
                <tr>
                    <td style="font-size:large">
                        Month (<i style="font-size:smaller"><%= DateTime.Now.Year %></i>):
                        <asp:DropDownList ID="MonthSelect" runat="server" AppendDataBoundItems="true" Font-Size="Large">
                            <asp:ListItem Text="Any Month" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="January" Value="1"></asp:ListItem>
                            <asp:ListItem Text="February" Value="2"></asp:ListItem>
                            <asp:ListItem Text="March" Value="3"></asp:ListItem>
                            <asp:ListItem Text="April" Value="4"></asp:ListItem>
                            <asp:ListItem Text="May" Value="5"></asp:ListItem>
                            <asp:ListItem Text="June" Value="6"></asp:ListItem>
                            <asp:ListItem Text="July" Value="7"></asp:ListItem>
                            <asp:ListItem Text="August" Value="8"></asp:ListItem>
                            <asp:ListItem Text="September" Value="9"></asp:ListItem>
                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="font-size:large">
                        Agency:
                        <asp:DropDownList ID="AgentSelect" runat="server" AppendDataBoundItems="true" DataTextField="Name" DataValueField="AgencyId" Font-Size="Large">
                            <asp:ListItem Text="Any Agency" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="font-size:large">
                        Type:
                        <asp:DropDownList ID="TypeSelect" runat="server" AppendDataBoundItems="true" DataTextField="Description" DataValueField="InvoiceTypeId" Font-Size="Large">   
                            <asp:ListItem Text="Any Type" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="LinkButton2" PostBackUrl="~/Finance/SageDataPage.aspx" runat="server"
                OnClick="lookupRecords"><span>Search Results</span>
            </asp:LinkButton>
            <a href="?Action=thismonth">This Month</a>
            <a href="?Action=lastmonth">Last Month</a>
            <a href="?Action=thisyear">This Year</a>
            <a href="?Action=all">All</a>
            </span><span style="float: right;">
                <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
                </asp:Label>
            </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Id
                    </th>
                    <th scope="col">
                        Agency
                    </th>
                    <th scope="col">
                        Type
                    </th>
                    <th scope="col">
                        Date
                    </th>
                    <th scope="col">
                        Net Amount
                    </th>
                    <th scope="col">
                        Gross Amount
                    </th>
                    <th scope="col">
                        Notes
                    </th>
                    <th scope="col">
                        Status
                    </th>
                    <th scope="col">
                        Type
                    </th>
                </tr>
            </thead>
            <tbody style="font-size: smaller">
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center">
                            <a href="ViewSageInvoicePage.aspx?InvoiceNumber=<%#DataBinder.Eval(Container.DataItem, "InvoiceNumber")%>"><%#DataBinder.Eval(Container.DataItem, "InvoiceNumber")%></a>
                            </td>
                            <td style="text-align: left">
                                <%# AgencyLink(DataBinder.Eval(Container.DataItem, "Agency.AgencyId"), "~/Agent/AgentPage.aspx", CreateName(DataBinder.Eval(Container.DataItem, "Agency.Name"), "")) %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Xlk_InvoiceType.Description") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "InvoiceDate", "{0:D}")%>
                            </td>
                            <td style="text-align: left">
                               <%#DataBinder.Eval(Container.DataItem, "NetAmount", "{0:c}")%>
                            </td>
                            <td style="text-align: left">
                               <%#DataBinder.Eval(Container.DataItem, "GrossAmount", "{0:c}")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Notes1")%>
                                /
                                <%#DataBinder.Eval(Container.DataItem, "Notes2")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Status") %>
                            </td>
                           <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "Type") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
