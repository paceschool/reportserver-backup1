﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Data;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
using Pace.DataAccess.Sage;
using Pace.DataAccess.Finance;


public partial class SageDataPage :BaseFinancePage 
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MonthSelect.SelectedIndex = Convert.ToInt32(DateTime.Now.Month);

            AgentSelect.DataSource = LoadAgents();
            AgentSelect.DataBind();

            TypeSelect.DataSource = LoadTypes();
            TypeSelect.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

        }

      
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadRecords();
                break;
            case "thismonth":
                Click_LoadThisMonth();
                break;
            case "lastmonth":
                Click_LoadLastMonth();
                break;
            case "thisyear":
                Click_LoadThisYear();
                break;
            case "all":
                Click_LoadAllRecords();
                break;
            default:
                Click_LoadAllRecords();
                break;
        }
    }

    protected void lookupRecords(object sender, EventArgs e)
    {
        Click_LoadRecords();
    }

    protected void Click_LoadRecords()
    {
        int agencyid = Convert.ToInt32(AgentSelect.SelectedItem.Value);
        int typeid = Convert.ToInt32(TypeSelect.SelectedItem.Value);
        int monthid = Convert.ToInt32(MonthSelect.SelectedItem.Value);
        string _record = searchstring.Text;
        DateTime start; DateTime end;
        if (FromDate.Text != "" && ToDate.Text != "")
        {
            start = Convert.ToDateTime(FromDate.Text);
            end = Convert.ToDateTime(ToDate.Text);
        }
        else
        {
            start = DateTime.Now; end = DateTime.Now;
        }

        using (SageEntities entity = new SageEntities())
        {
            var recordQuery = from r in entity.Invoices.Include("Xlk_InvoiceType").Include("Agency")
                              select r;

            if (agencyid > 0)
                recordQuery = recordQuery.Where(x => x.Agency.AgencyId == agencyid);

            if (typeid > 0)
                recordQuery = recordQuery.Where(x => x.Xlk_InvoiceType.InvoiceTypeId == typeid);

            if (monthid > 0 && FromDate.Text == "" && ToDate.Text == "")
                recordQuery = recordQuery.Where(x => x.InvoiceDate.Value.Month == monthid && x.InvoiceDate.Value.Year == DateTime.Now.Year);
            else if ((monthid > 0 && FromDate.Text != "") || (monthid > 0 && ToDate.Text != ""))
                recordQuery = recordQuery.Where(x => x.Agency.AgencyId == 0);

            if (FromDate.Text != "" && ToDate.Text != "")
                recordQuery = recordQuery.Where(x => x.InvoiceDate > start && x.InvoiceDate < end);

            if (!string.IsNullOrEmpty(_record))
            {
                
                Int32 invoicenum;
                if (Int32.TryParse(_record, out invoicenum))
                {
                    List<Int32> invoicenums = new [] { invoicenum  }.ToList();
                    recordQuery = recordQuery.Where(x => (invoicenums.Contains(x.InvoiceNumber)));
                }
                else
                    recordQuery = recordQuery.Where(x => (x.Agency.SageRef.Contains(_record) || (x.Agency.Name.Contains(_record)) || (x.Notes1.Contains(_record)) || (x.Notes2.Contains(_record))));
            }


            recordQuery = recordQuery.OrderBy(x => x.InvoiceDate).OrderByDescending(x => x.InvoiceDate);
            LoadResults(recordQuery.ToList());
        }
    }

    protected void Click_LoadThisMonth()
    {
        using (SageEntities entity = new SageEntities())
        {
            var recordQuery = from r in entity.Invoices.Include("Xlk_InvoiceType").Include("Agency")
                              where r.InvoiceDate.Value.Month == DateTime.Now.Month && r.InvoiceDate.Value.Year == DateTime.Now.Year
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    protected void Click_LoadLastMonth()
    {
        DateTime last = DateTime.Now.AddMonths(-1);
        using (SageEntities entity = new SageEntities())
        {
            var recordQuery = from r in entity.Invoices.Include("Xlk_InvoiceType").Include("Agency")
                              where r.InvoiceDate.Value.Month == last.Month && r.InvoiceDate.Value.Year == last.Year
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    protected void Click_LoadThisYear()
    {
        DateTime year = DateTime.Now;
        using (SageEntities entity = new SageEntities())
        {
            var recordQuery = from r in entity.Invoices.Include("Xlk_InvoiceType").Include("Agency")
                              where r.InvoiceDate.Value.Year == year.Year
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    protected void Click_LoadAllRecords()
    {
        using (SageEntities entity = new SageEntities())
        {
            var recordQuery = from r in entity.Invoices.Include("Xlk_InvoiceType").Include("Agency")
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    private void LoadResults(List<Invoice> data)
    {
        results.DataSource = data;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", (data != null) ? data.Count().ToString() : "0");
    }

    #region Lists

    protected IList<Pace.DataAccess.Agent.Agency> LoadAgents()
    {
        using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
        {
            var query = from a in entity.Agencies
                        orderby a.Name
                        select a;

            return query.ToList();
        }
    }

    protected IList<Xlk_InvoiceType> LoadTypes()
    {
        using (SageEntities entity = new SageEntities())
        {
            var query = from t in entity.Xlk_InvoiceType
                        orderby t.Description
                        select t;

            return query.ToList();
        }
    }

    #endregion

}
