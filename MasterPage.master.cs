﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    BasePage CurrentPage = null;
    protected void Page_Load(object sender, EventArgs e)
    {

        CurrentPage = this.Page as BasePage;
        if (CurrentPage != null)
        {
            txtMaster.Text = CurrentPage.TextValue();
        }
    }
}
