﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Enrollment;
using System.Data.Objects;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for BaseEnrollmentPage
/// </summary>
public partial class BaseEnrollmentPage : BasePage
{
    public EnrollmentsEntities _entities { get; set; }

    public BaseEnrollmentPage()
    {
    }

    #region Events

    public static EnrollmentsEntities Entities
    {
        get
        {
            EnrollmentsEntities value;

            if (CacheManager.Instance.GetFromCache<EnrollmentsEntities>("EnrollmentsEntitiesObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<EnrollmentsEntities>("EnrollmentsEntitiesObject", new EnrollmentsEntities());

        }
    }


    public IList<Xlk_Nationality> LoadNationalities()
    {
        List<Xlk_Nationality> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Nationality>>("AllEnrollmentNationalities", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Nationality>>("AllEnrollmentNationalities", GetNationalities().ToList());
    }

    public IList<Xlk_LessonBlock> LoadLessonBlocks()
    {
        List<Xlk_LessonBlock> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_LessonBlock>>("AllEnrollmentLessonBlocks", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_LessonBlock>>("AllEnrollmentLessonBlocks", GetLessonBlocks().ToList());
    }

    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllEnrollmentProgrammeTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllEnrollmentProgrammeTypes", GetProgrammeTypes().ToList());
    }

    public IList<Xlk_Status> LoadStatus()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllEnrollmentStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllEnrollmentStatus", GetStatus().ToList());
    }

    public IList<Agency> LoadAgents()
    {
        List<Agency> values = null;

        if (CacheManager.Instance.GetFromCache<List<Agency>>("AllEnrollmentAgents", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Agency>>("AllEnrollmentAgents", GetAgents().ToList());
    }

    public IList<Student> LoadStudents()
    {
        List<PaceManager.Enrollment.Student> values = null;

        if (CacheManager.Instance.GetFromCache<List<Student>>("AllEnrollmentStudents", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Student>>("AllEnrollmentStudents", GetStudents().ToList());
    }

    public IList<Xlk_Gender> LoadGender()
    {
        List<Xlk_Gender> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Gender>>("AllEnrollmentGender", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Gender>>("AllEnrollmentGender", GetGender().ToList());
    }

    public IList<Exam> LoadExams()
    {
        List<Exam> values = null;

        if (CacheManager.Instance.GetFromCache<List<Exam>>("AllEnrollmentExmas", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Exam>>("AllEnrollmentExams", GetExams().ToList());
    }

    #endregion


    #region Private Methods

    private IQueryable<Xlk_Nationality> GetNationalities()
    {
        IQueryable<Xlk_Nationality> lookupQuery =
            from a in Entities.Xlk_Nationality
            select a;
        return lookupQuery;
    }

    private IQueryable<Xlk_LessonBlock> GetLessonBlocks()
    {
        IQueryable<Xlk_LessonBlock> lookupQuery =
            from a in Entities.Xlk_LessonBlock
            select a;
        return lookupQuery;
    }

    private IQueryable<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        IQueryable<Xlk_ProgrammeType> lookupQuery =
            from b in Entities.Xlk_ProgrammeType
            select b;
        return lookupQuery;
    }

    private IQueryable<Xlk_Status> GetStatus()
    {
        IQueryable<Xlk_Status> lookupQuery =
            from c in Entities.Xlk_Status
            select c;
        return lookupQuery;
    }

    private IQueryable<Agency> GetAgents()
    {
        IQueryable<Agency> lookupQuery =
            from d in Entities.Agency
            select d;
        return lookupQuery;
    }

    private IQueryable<PaceManager.Enrollment.Student> GetStudents()
    {
        IQueryable<PaceManager.Enrollment.Student> lookupQuery =
            from e in Entities.Student
            select e;
        return lookupQuery;
    }

    private IQueryable<Xlk_Gender> GetGender()
    {
        IQueryable<Xlk_Gender> lookupQuery =
            from e in Entities.Xlk_Gender
            select e;
        return lookupQuery;
    }

    private IQueryable<Exam> GetExams()
    {
        IQueryable<Exam> lookupQuery =
            from ex in Entities.Exam
            select ex;
        return lookupQuery;
    }

    protected string DaysLeft(DateTime startingDate, DateTime EndingDate, string DaysLeft)
    {
        if (startingDate != null && EndingDate != null)
        {
            TimeSpan daysleft = EndingDate.Subtract(startingDate);
            return DaysLeft = Convert.ToString((daysleft.Days)/7);
        }
        else
        {
            return (startingDate != null) ? startingDate.ToString() : EndingDate.ToString();
        }
    }

    private static bool CheckNoConflict(Int32 studentId,DateTime startDate, DateTime endDate)
    {
        if (startDate > endDate)
        {
            return false;
        }
        var chechQuery = from h in BaseAccommodationPage.Entities.Hosting
                         where h.Student.StudentId == studentId && ((h.ArrivalDate <= startDate && h.DepartureDate >= startDate) || ((h.ArrivalDate <= endDate && h.DepartureDate >= endDate)))
                         select h;

        return chechQuery.Count() == 0;
    }

    #endregion


    #region Report Methods

    public IEnumerable<KeyValuePair<string, IEnumerable>> LoadStudents(Int32 studentid)
    {
        IList<KeyValuePair<string, IEnumerable>> list = new List<KeyValuePair<string, IEnumerable>>();

        IQueryable<Student> query = from s in Entities.Student.Include("Xlk_Status").Include("Enrollment")
                                    where s.Enrollment.Count > 0
                                    select s;

        list.Add(new KeyValuePair<string, IEnumerable>("Student", query.ToList()));

        IQueryable<Enrollment> query1 = from s in Entities.Enrollment
                                    select s;
        list.Add(new KeyValuePair<string, IEnumerable>("Enrollment", query1.ToList()));
        return list;

    }

    #endregion


    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadEnrollments(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}?/PaceManagerReports/StudentWrittenTest&StudentId={0}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\"><img src=\"../Content/img/actions/printer.png\" />Print Written Test</a></span><span style=\"float:left\"><a href=\"{1}?/PaceManagerReports/IndividualStudentCert&StudentId={0}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\"><img src=\"../Content/img/actions/printer.png\" />Print Certificate</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentEnrollmentControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Enrollment</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Enrollment Id</th><th scope=\"col\">Course Start Date</th><th scope=\"col\">Course Finish Date</th><th scope=\"col\">#Weeks</th><th scope=\"col\">Programme Type</th><th scope=\"col\">Lesson Block</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from n in Entities.Enrollment.Include("Xlk_ProgrammeType").Include("Xlk_LessonBlock")
                        where n.Student.StudentId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (Enrollment enrollment in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: left\">{5}</td><td style=\"text-align: center\"><a title=\"Delete Enrollment\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewStudentPage.aspx','StudentEnrollment',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", enrollment.EnrollmentId, enrollment.StartDate.ToString("D"), (enrollment.EndDate.HasValue) ? enrollment.EndDate.Value.ToString("D") : String.Empty, CalculateTimePeriod(enrollment.StartDate, enrollment.EndDate, 'W'), enrollment.Xlk_ProgrammeType.Description, enrollment.Xlk_LessonBlock.Description);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"7\">No Enrollments to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadHostings(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentHostingControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Hosting</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">HostingId</th><th scope=\"col\">Family</th><th scope=\"col\">Arrival Date</th><th scope=\"col\">Departure Date</th><th scope=\"col\">#Weeks</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from h in BaseAccommodationPage.Entities.Hosting
                        join f in BaseAccommodationPage.Entities.Family on h.Family.FamilyId equals f.FamilyId
                        where h.Student.StudentId == id
                        select new { h.Student.StudentId, h.HostingId, h.ArrivalDate, h.DepartureDate, Name = f.FirstName + " " + f.SurName };


            if (query.Count() > 0)
            {
                foreach (var hosting in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\"><a href=\"../Acommodation/ViewAccommodationPage.aspx?FamilyId={0}\">{1}</a></td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Delete Hosting\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewStudentPage.aspx','StudentHosting',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", hosting.HostingId, hosting.Name, hosting.ArrivalDate.ToString("D"), hosting.DepartureDate.ToString("D"), CalculateTimePeriod(hosting.ArrivalDate, hosting.DepartureDate, 'W'));
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Hostings to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadStudentTransfers(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentTransferControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Transfer</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Student TransferId</th><th scope=\"col\">Type</th><th scope=\"col\">Transfer Date</th><th scope=\"col\">Location </th><th scope=\"col\">Flight Number</th><th scope=\"col\">Completed By</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from h in Entities.StudentTransfer.Include("Xlk_Location").Include("Xlk_TransferType").Include("Xlk_BusinessEntity")
                        where h.Student.StudentId == id
                        select h;


            if (query.Count() > 0)
            {
                foreach (StudentTransfer transfer in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2} {7}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\"><a href=\"#\" class=\"information\" style=\"font-size:x-small\">{6}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Delete Hosting\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewStudentPage.aspx','StudentTransfer',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", transfer.StudentTransferId, transfer.Xlk_TransferType.Description, transfer.Date.ToString("D"), transfer.Xlk_Location.Description, transfer.FlightNumber, transfer.Xlk_BusinessEntity.Description, BaseEnrollmentPage.TrimText(transfer.Comment, 20), (transfer.Time.HasValue) ? string.Format("{0:HH\\:mm}", transfer.Time) : string.Empty, transfer.Comment);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadStudentComplaints(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentComplaintControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Complaint</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">StudentComplaintId</th><th scope=\"col\">Severity</th><th scope=\"col\">Type</th><th scope=\"col\">Complaint</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.StudentComplaint.Include("Xlk_Severity").Include("Xlk_ComplaintType")
                        where n.Student.StudentId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (StudentComplaint complaint in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Delete Complaint\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewStudentPage.aspx','StudentComplaint',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", complaint.StudentComplaintId, complaint.Xlk_Severity.Description, complaint.Xlk_ComplaintType.Description, complaint.Complaint, complaint.DateCreated.ToString("D"));
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Complaints to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadAgencyComplaints(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyComplaintControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Complaint</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">StudentComplaintId</th><th scope=\"col\">Severity</th><th scope=\"col\">Type</th><th scope=\"col\">Complaint</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.AgencyComplaint.Include("Xlk_Severity").Include("Xlk_ComplaintType")
                        where n.Agency.AgencyId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (AgencyComplaint complaint in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: center\"><a title=\"Delete Complaint\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAgentPage.aspx','AgencyComplaint',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", complaint.AgencyComplaintId, complaint.Xlk_Severity.Description, complaint.Xlk_ComplaintType.Description, complaint.Complaint, complaint.DateCreated.ToString("D"));
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Complaints to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadAgencyNotes(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyNoteControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Note</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">NoteId</th><th scope=\"col\">Note Type</th><th scope=\"col\">Note</th><th scope=\"col\">Date Created</th><th scope=\"col\">Action</th></tr></thead><tbody>");

            var query = from n in Entities.AgencyNote.Include("Xlk_NoteType")
                        where n.Agency.AgencyId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (AgencyNote note in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\"> <a title=\"Delete Note\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAgentPage.aspx','AgencyNote',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", note.AgencyNoteId, note.Xlk_NoteType.Description, note.Note, note.DateCreated.Value.ToString("D"));
                }

            }
            else
            {
                sb.Append("<tr><td colspan=\"5\">No Notes to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadNotes(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentNoteControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Note</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">NoteId</th><th scope=\"col\">Note Type</th><th scope=\"col\">Note</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.StudentNote.Include("Xlk_NoteType")
                        where n.Student.StudentId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (StudentNote note in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: center\"><a title=\"Delete Note\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewStudentPage.aspx','StudentNote',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td> </tr>", note.StudentNoteId, note.Xlk_NoteType.Description, note.Note, note.DateCreated.Value.ToString("D"), note.StudentNoteId);
                }

            }
            else
            {
                sb.Append("<tr><td colspan=\"5\">No Notes to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseEnrollmentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("StudentId", id);
            userControl.Parameters.Add("AgencyId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    public static string SaveStudentNote(StudentNote newObject)
    {
        try
        {
            StudentNote newnote = new StudentNote();

            newnote.DateCreated = DateTime.Now;
            newnote.StudentNoteId = newObject.StudentNoteId;
            newnote.Note = newObject.Note;
            newnote.StudentReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Student", newObject.Student);
            newnote.Xlk_NoteTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_NoteType", newObject.Xlk_NoteType);
            newnote.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newnote.StudentNoteId == 0)
                Entities.AddToStudentNote(newnote);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveAgencyNote(AgencyNote newObject)
    {
        try
        {
            AgencyNote newnote = new AgencyNote();

            newnote.DateCreated = DateTime.Now;
            newnote.AgencyNoteId = newObject.AgencyNoteId;
            newnote.Note = newObject.Note;
            newnote.AgencyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Agency", newObject.Agency);
            newnote.Xlk_NoteTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_NoteType", newObject.Xlk_NoteType);
            newnote.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newnote.AgencyNoteId == 0)
                Entities.AddToAgencyNote(newnote);

            Entities.SaveChanges();

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveStudentComplaint(StudentComplaint newObject)
    {
        try
        {
            StudentComplaint newcomplaint = new StudentComplaint();

            newcomplaint.ActionNeeded = newObject.ActionNeeded;
            newcomplaint.Complaint = newObject.Complaint;
            newcomplaint.DateCreated = DateTime.Now;
            newcomplaint.StudentReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Student", newObject.Student);
            newcomplaint.Xlk_ComplaintTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_ComplaintType", newObject.Xlk_ComplaintType);
            newcomplaint.Xlk_SeverityReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_Severity", newObject.Xlk_Severity);
            newcomplaint.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newcomplaint.StudentComplaintId == 0)
                Entities.AddToStudentComplaint(newcomplaint);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveStudentTransfer(StudentTransfer newObject)
    {
        try
        {
            StudentTransfer newtransfer = new StudentTransfer();

            newtransfer.Comment = newObject.Comment;
            newtransfer.Date = newObject.Date;
            newtransfer.FlightNumber = newObject.FlightNumber;
            newtransfer.Time = newObject.Time;
            newtransfer.TransportBookingId = newObject.TransportBookingId;
            newtransfer.StudentReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Student", newObject.Student);
            newtransfer.Xlk_LocationReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_Location", newObject.Xlk_Location);
            newtransfer.Xlk_TransferTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_TransferType", newObject.Xlk_TransferType);
            newtransfer.Xlk_BusinessEntityReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_BusinessEntity", newObject.Xlk_BusinessEntity);

            if (newtransfer.StudentTransferId == 0)
                Entities.AddToStudentTransfer(newtransfer);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveAgencyComplaint(AgencyComplaint newObject)
    {
        try
        {
            AgencyComplaint newcomplaint = new AgencyComplaint();

            newcomplaint.ActionNeeded = newObject.ActionNeeded;
            newcomplaint.Complaint = newObject.Complaint;
            newcomplaint.DateCreated = DateTime.Now;
            newcomplaint.AgencyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Agency", newObject.Agency);
            newcomplaint.Xlk_ComplaintTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_ComplaintType", newObject.Xlk_ComplaintType);
            newcomplaint.Xlk_SeverityReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_Severity", newObject.Xlk_Severity);
            newcomplaint.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newcomplaint.AgencyComplaintId == 0)
                Entities.AddToAgencyComplaint(newcomplaint);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveHosting(PaceManager.Accommodation.Hosting newObject)
    {
        try
        {
            if (CheckNoConflict(newObject.Student.StudentId, newObject.ArrivalDate, newObject.DepartureDate))
            {
                PaceManager.Accommodation.Hosting hosting = new PaceManager.Accommodation.Hosting();
                hosting.ArrivalDate = newObject.ArrivalDate;
                hosting.DepartureDate = newObject.DepartureDate;

                hosting.HostingId = newObject.HostingId;
                hosting.StudentReference.EntityKey = Entities.CreateEntityKey(BaseAccommodationPage.Entities.DefaultContainerName + ".Student", newObject.Student);
                hosting.FamilyReference.EntityKey = BaseAccommodationPage.Entities.CreateEntityKey(BaseAccommodationPage.Entities.DefaultContainerName + ".Family", newObject.Family);

                if (hosting.HostingId == 0)
                    BaseAccommodationPage.Entities.AddToHosting(hosting);

                BaseAccommodationPage.Entities.SaveChanges();

                return string.Empty;
            }
            else
                return "There was a problem with the dates, please try again!";

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveEnrollment(Enrollment newObject)
    {
        try
        {
            Enrollment newenrollment = new Enrollment();

            newenrollment.EndDate = newObject.EndDate;
            newenrollment.EnrollmentId = newObject.EnrollmentId;
            newenrollment.StartDate = newObject.StartDate;
            newenrollment.StudentReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Student", newObject.Student);
            newenrollment.Xlk_LessonBlockReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_LessonBlock", newObject.Xlk_LessonBlock);
            newenrollment.Xlk_ProgrammeTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_ProgrammeType", newObject.Xlk_ProgrammeType);

            if (newenrollment.EnrollmentId == 0)
                Entities.AddToEnrollment(newenrollment);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    #endregion

}
