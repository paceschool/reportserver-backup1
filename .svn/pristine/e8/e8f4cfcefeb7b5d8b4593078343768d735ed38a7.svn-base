﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using System.Text;
using System.Web.Services;

public partial class Transport_AddVehicleBookingControl : BaseTransportControl
{
    protected Int32 _bookingId;
    protected Int32 _vehicleId;
    protected Int32 _number;
    protected Int32 _plocation;
    protected Int32 _dlocation;
    protected VehicleBooking _vehiclebooking;
    protected Booking _booking;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (TransportEntities entity = new TransportEntities())
        {
            if (GetValue("VehicleId") != null && GetValue("BookingId") != null)
            {
                Int32 VehicleId = (Int32)GetValue("VehicleId");
                Int32 BookingId = (Int32)GetValue("BookingId");

                _vehiclebooking = (from v in entity.VehicleBookings
                            where v.VehicleId == VehicleId && v.BookingId == BookingId
                            select v).FirstOrDefault();
                _booking = (from b in entity.Bookings
                            where b.BookingId == BookingId
                            select b).FirstOrDefault();
                _number = _booking.NumberOfPassengers.Value;
                _plocation = _booking.PickupLocation.LocationId;
                _dlocation = _booking.DropoffLocation.LocationId;
            }

            if (GetValue("VehicleId") == null && GetValue("BookingId") != null)
            {
                Int32 VehicleId = 0;
                Int32 BookingId = (Int32)GetValue("BookingId");

                _booking = (from v in entity.Bookings.Include("VehicleBookings")
                            where v.BookingId == BookingId
                            select v).FirstOrDefault();
                _number = _booking.NumberOfPassengers.Value;
                _plocation = _booking.PickupLocation.LocationId;
                _dlocation = _booking.DropoffLocation.LocationId;
            }
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetBookingId
    {
        get
        {
            if (_booking != null)
                return _booking.BookingId.ToString();

            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }

    protected string GetVehicleId
    {
        get
        {
            if (_vehiclebooking != null)
                return _vehiclebooking.VehicleId.ToString();

            if (Parameters.ContainsKey("VehicleId"))
                return Parameters["VehicleId"].ToString();

            return "0";
        }
    }

    #region Get strings

    protected string GetCost
    {
        get
        {
            if (_vehiclebooking != null)
                return _vehiclebooking.Cost.Value.ToString();

            return "0";
        }
    }

    protected string GetDriver
    {
        get
        {
            if (_vehiclebooking != null && _vehiclebooking.DriverName != null)
                return _vehiclebooking.DriverName.ToString();

            return string.Empty;
        }
    }

    protected string GetPhone
    {
        get
        {
            if (_vehiclebooking != null && _vehiclebooking.DriverContactNumber != null)
                return _vehiclebooking.DriverContactNumber.ToString();

            return string.Empty;
        }
    }

    protected string GetVehicleDate
    {
        get
        {
            if (_vehiclebooking != null && _vehiclebooking.ConfirmedDate.HasValue)
                return _vehiclebooking.ConfirmedDate.Value.ToString("dd/MMM/yyyy");

            return null;
        }
    }

    #endregion

    #region Get Lists

    protected string GetUserList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclebooking != null && _vehiclebooking.User != null) ? _vehiclebooking.User.UserId : (Int32?)null;

        foreach (Pace.DataAccess.Security.Users item in LoadUserList())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "Selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (_vehiclebooking != null && _vehiclebooking.StatusId != null) ? _vehiclebooking.StatusId : (Byte?)null;

        foreach (Xlk_Status stat in LoadStatusTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", stat.StatusId, stat.Description, (_current.HasValue && stat.StatusId == _current) ? "Selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetVehiclesList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclebooking != null && _vehiclebooking.Vehicle != null) ? _vehiclebooking.Vehicle.VehicleId : (Int32?)null;

        foreach (Vehicle item in LoadVehicles())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.VehicleId, item.Xlk_VehicleType.Description, (_current.HasValue && item.VehicleId == _current) ? "Selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetPricePlanList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_vehiclebooking != null && _vehiclebooking.VehiclePricePlan != null) ? _vehiclebooking.VehiclePricePlan.VehiclePricePlanId : (Int32?)null;
        
        foreach (VehiclePricePlan item in LoadPricePlans(_number, _plocation, _dlocation))
        {
            sb.AppendFormat("<option {2} value={0}>{4}, {3} - {1}</option>", item.VehiclePricePlanId, item.Cost, (_current.HasValue && item.VehiclePricePlanId == _current) ? "Selected" : string.Empty, item.Vehicle.Xlk_VehicleType.Description, item.Vehicle.TransportSupplier.Supplier.Title);
        }

        return sb.ToString();
    }

    #endregion

    private IList<VehiclePricePlan> LoadPricePlans(int num, int ploc, int dloc)
    {
        TransportEntities entity = new TransportEntities();

        var vehicleLookup = from v in entity.VehiclePricePlans
                            where v.PickupLocation.LocationId == ploc && v.DropoffLocation.LocationId == dloc
                            select v;
        return vehicleLookup.ToList();
    }
}