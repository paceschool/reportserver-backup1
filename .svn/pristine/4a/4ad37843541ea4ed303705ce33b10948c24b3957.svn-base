﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Enrollment;
using System.Text;
using System.Web.Services;
using PaceManager.Security;

public partial class Enrollments_AddStudentBusTicketControl : BaseEnrollmentControl
{
    protected Int32 _studentId;
    protected BusTicket _busTicket;
    protected Int32 busTicketId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("BusTicketId") != null)
        {
            Int32 busTicketId = (Int32)GetValue("BusTicketId");
            _busTicket = (from tickets in Entities.BusTickets
                          where tickets.BusTicketId == busTicketId
                          select tickets).FirstOrDefault();
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetBusTicketTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_busTicket != null && _busTicket.Xlk_BusTicketType != null) ? _busTicket.Xlk_BusTicketType.BusTicketTypeId : (Int16?)null;

        foreach (Xlk_BusTicketType item in LoadBusTicketType())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.BusTicketTypeId, item.BusTicketType, (_current.HasValue && item.BusTicketTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetBusTicketPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_busTicket != null && _busTicket.Xlk_BusTicketPaymentType != null) ? _busTicket.Xlk_BusTicketPaymentType.BusTicketPaymentTypeId : (byte?)null;

        foreach (Xlk_BusTicketPaymentType item in LoadBusTicketPaymentType())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.BusTicketPaymentTypeId, item.PaymentType, (_current.HasValue && item.BusTicketPaymentTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetBusTicketId
    {
        get
        {
            if (_busTicket != null)
                return _busTicket.BusTicketId.ToString();

            if (Parameters.ContainsKey("BusTicketId"))
                return Parameters["BusTicketId"].ToString();

            return "0";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_busTicket != null)
                return _busTicket.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetFromDate
    {
        get
        {
            if (_busTicket != null)
                return _busTicket.FromDate.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetAmountPaid
    {
        get
        {
            if (_busTicket != null)
                return _busTicket.AmountPaid.ToString();

            return null;
        }
    }

    protected string GetComment
    {
        get
        {
            if (_busTicket != null)
                return _busTicket.Comment.ToString();

            return null;
        }
    }
}