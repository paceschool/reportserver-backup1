﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using Pace.DataAccess.Agent;
using Pace.DataAccess.Excursion;
using Pace.DataAccess.Security;
using Pace.Common;

/// <summary>
/// Summary description for BaseEnrollmentPage
/// </summary>
public partial class BaseAgentPage : BasePage
{
    public AgentEntities _entities { get; set; }

    public BaseAgentPage()
    {
    }

    #region Events

    public static AgentEntities CreateEntity
    {
        get
        {
            return new AgentEntities();
        }
    }


    public IList<Pace.DataAccess.Agent.Xlk_Status> LoadStatus()
    {
        IList<Pace.DataAccess.Agent.Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.Xlk_Status>>("AllAgentStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.Xlk_Status>>("AllAgentStatus", GetStatus());
    }

    public IList<Xlk_Unit> LoadUnits()
    {
        IList<Xlk_Unit> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Unit>>("AllUnit", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Unit>>("AllUnit", GetUnit());
    }

    public IList<Pace.DataAccess.Agent.User> LoadUsers()
    {
        IList<Pace.DataAccess.Agent.User> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.User>>("AllUser", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.User>>("AllUser", GetUsers());
    }

    public IList<Pace.DataAccess.Agent.OrderableItem> LoadOrderableItems()
    {
        IList<Pace.DataAccess.Agent.OrderableItem> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.OrderableItem>>("AllOrderableItem", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.OrderableItem>>("AllOrderableItem", GetOrderableItems());
    }

    public static IList<Pace.DataAccess.Enrollment.Xlk_Nationality> LoadNationalities()
    {
        IList<Pace.DataAccess.Enrollment.Xlk_Nationality> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Enrollment.Xlk_Nationality>>("AllEnrollmentNationalities", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Enrollment.Xlk_Nationality>>("AllEnrollmentNationalities", GetNationalities());
    }

    #endregion


    #region Private Methods


    private IList<Pace.DataAccess.Agent.Xlk_Status> GetStatus()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Pace.DataAccess.Agent.Xlk_Status> lookupQuery =
                from c in entity.Xlk_Status
                select c;
            return lookupQuery.ToList();
        }
    }

    private IList<Pace.DataAccess.Agent.Xlk_Unit> GetUnit()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Xlk_Unit> lookupQuery =
                from u in entity.Xlk_Unit
                select u;

            return lookupQuery.ToList();
        }
    }

    private IList<Pace.DataAccess.Agent.User> GetUsers()
    {
        using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
        {
            IQueryable<Pace.DataAccess.Agent.User> lookupQuery =
                from u in entity.Users
                select u;
            return lookupQuery.ToList();
        }
    }

    private IList<Pace.DataAccess.Agent.OrderableItem> GetOrderableItems()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Pace.DataAccess.Agent.OrderableItem> lookupQuery =
                from o in entity.OrderableItems
                select o;
            return lookupQuery.ToList();
        }
    }

    public IList<Agency> LoadAgents()
    {
        return GetAgents();
    }

    private IList<Agency> GetAgents()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Agency> lookupQuery =
                from d in entity.Agencies
                orderby d.Name
                select d;
            return lookupQuery.ToList();
        }
    }


    protected static Agency GetAgent(int agentId)
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Agency> agentQuery = from g in entity.Agencies.Include("Xlk_Status").Include("Xlk_Nationality")
                                           where g.AgencyId == agentId
                                           select g;

            if (agentQuery.Count() > 0)
            {
                return agentQuery.First();

            }

        }
        return null;
    }

    private static IList<Pace.DataAccess.Enrollment.Xlk_Nationality> GetNationalities()
    {
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            IQueryable<Pace.DataAccess.Enrollment.Xlk_Nationality> lookupQuery =
                from a in entity.Xlk_Nationality
                select a;
            return lookupQuery.ToList();
        }
    }

    #endregion



    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupAgent(int id)
    {
        try
        {
            using (AgentEntities entity = new AgentEntities())
            {
                var agent = (from s in entity.Agencies
                             where s.AgencyId == id
                             select new { s.Name, s.SageRef, s.Address, s.ContactName, s.Email, Nationality = s.Xlk_Nationality.Description }).SingleOrDefault();

                if (agent != null)
                {
                    return string.Format("<table class=\"popup-contents\"><tbody><tr><th>Name</th><td>{0}</td></tr><tr><th>Address:</th><td>{1}</td></tr><tr><th>SageRef:</th><td>{2}</td></tr><tr><th>Email:</th><td>{3}</td></tr><tr><th>Nationality:</th><td>{4}</td></tr><tr id=\"release-notes\">	<th>Read the release notes:</th>	<td><a href=\"./releasenotes.html\" title=\"Read the release notes\">release notes</a></td></tr></tbody></table>", agent.Name, agent.Address, agent.SageRef, agent.Email, agent.Nationality);
                }

                return string.Empty;

            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadAgencyComplaints(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyComplaintControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Complaint</a></span><table id=\"box-table-a\"><thead><tr><th scope=\"col\">Complaint Id</th><th scope=\"col\">Type</th><th scope=\"col\">Severity</th><th scope=\"col\">Details</th><th scope=\"col\">Raised By</th><th scope=\"col\">Action Needed</th><th scope=\"col\">Action Taken</th><th scope=\"col\">Date Created</th><th scope=\"col\">Resolved</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (AgentEntities entity = new AgentEntities())
            {
                var complaintQuery = from c in entity.AgencyComplaints
                                     where c.Agency.AgencyId == id
                                     select c;

                if (complaintQuery.Count() > 0)
                {
                    foreach (AgencyComplaint complaint in complaintQuery.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: left\">{8}</td><td style=\"text-align: center\"><a title=\"Edit Complaint\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyComplaintControl.ascx','AgencyComplaintId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Complaint\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAgentPage.aspx','AgencyComplaint',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", complaint.AgencyComplaintId, complaint.ComplaintTypeId, complaint.SeverityId, complaint.Complaint, complaint.RaisedBy, complaint.ActionNeeded, complaint.ActionTaken, complaint.DateCreated.ToString("dd/MM/yyyy"), complaint.Resolved);
                    }
                }
                else
                {
                    sb.AppendFormat("<tr><td colspan=\"10\">No Complaints to Show!</td></tr>");
                }
            }
            sb.AppendFormat("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadAgencyRates(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewAgentPage.aspx','Agent/AddRateCardControl.ascx','AgencyId'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Rate Card</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Rate Card Id</th><th scope=\"col\">Orderable Item</th><th scope=\"col\">Active From</th><th scope=\"col\">Active To</th><th scope=\"col\">Discount</th><th scope=\"col\">Unit</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (AgentEntities entity = new AgentEntities())
            {
                var rateQuery = (from r in entity.RateCards.Include("OrderableItem").Include("Agency").Include("RateCardCostings").Include("RateCardCostings.Xlk_Unit")
                                 where r.RateCardId > 2
                                 orderby r.DateActiveFrom ascending
                                 select r);

                var costQuery = (from c in entity.RateCardCostings.Include("Xlk_Unit")
                                 select c);

                var defaultAgent = (from a in entity.Agencies where a.AgencyId == id select a).FirstOrDefault();

                if (rateQuery.Count() > 0)
                {
                    foreach (RateCard item in rateQuery)
                    {
                        foreach (RateCardCosting ritem in costQuery)
                        {
                            //if (item.Agency == null)
                            //{
                            //    item.AgencyReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Agencies", defaultAgent);
                            //}
                            if (ritem.RateCardId == item.RateCardId)
                                sb.AppendFormat("<tr><td style=\"text-align:left\">{0}</td><td style=\"text-align:left\">{1}</td><td style=\"text-align:left\">{2}</td><td style=\"text-align:left\">{3}</td><td style=\"text-align:left\">{4}</td><td style=\"text-align:left\">{5}</td><td style=\"text-align:left\">None</td></tr>", item.RateCardId, item.OrderableItem.Description, item.DateActiveFrom.ToString("dd/MM/yyyy"), item.DateActiveTo.ToString("dd/MM/yyyy"), item.DefaultDiscount.Value.ToString("00.00"), ritem.Xlk_Unit.Description);
                        }
                    }
                }
                else
                {
                    sb.AppendFormat("<tr><td colspan=\"7\">No Records to show!</td></tr>");
                }
            }
            sb.AppendFormat("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadAgencyInvoices(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Sage Ref</th><th scope=\"col\">Type</th><th scope=\"col\">Date Raised</th><th scope=\"col\">Amount</th><th scope=\"col\">Printed</th><th scope=\"col\">Posted</th></tr></thead><tbody>", id));
            using (Pace.DataAccess.Finance.FinanceEntities entity = new Pace.DataAccess.Finance.FinanceEntities())
            {
                var financeQuery = (from f in entity.SageDatas.Include("Xlk_SageDataType")
                                    where f.AgencyId == id
                                    orderby f.DateRaised descending
                                    select f);

                if (financeQuery.Count() > 0)
                {
                    foreach (Pace.DataAccess.Finance.SageData item in financeQuery)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">€{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td></tr>", item.SageRef, item.Xlk_SageDataType.Description, item.DateRaised.HasValue ? item.DateRaised.Value.ToString("dd/MM/yyyy") : string.Empty, item.Amount.HasValue ? item.Amount.Value.ToString("00.00") : string.Empty, item.Printed.HasValue ? "Yes" : "No", item.Posted.HasValue ? "Yes" : "No");
                    }
                }
                else
                {
                    sb.AppendFormat("<tr><td colspan=\"6\">No Records to show!</td></tr>");
                }
            }
            sb.AppendFormat("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadAgencyNotes(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyNoteControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Note</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Note Id</th><th scope\"col\">Note Type</th><th scope\"col\">Note</th><th scope=\"col\">Created By</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (AgentEntities entity = new AgentEntities())
            {
                var query = (from n in entity.AgencyNotes
                             where n.Agency.AgencyId == id
                             orderby n.AgencyNoteId
                             select n);

                if (query.Count() > 0)
                {
                    foreach (AgencyNote note in query)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Edit Note\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyNoteControl.ascx','AgencyNoteId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Note\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAgentPage.aspx','AgencyNote',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", note.AgencyNoteId, note.Xlk_NoteType.NoteTypeId, note.Note, note.User.Name, note.DateCreated.HasValue ? note.DateCreated.Value.ToLongDateString() : string.Empty);
                    }
                }
                else
                {
                    sb.AppendFormat("<tr><td colspan=\"6\">No Notes to Show!</td></tr>");
                }
            }
            sb.AppendFormat("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
   
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadQuoteItems(int id)
    {

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewQuotePage.aspx','Agent/AddAgencyQuoteItemControl.ascx','QuoteId'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Item</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Quote Id</th><th scope=\"col\">Line Id</th><th scope=\"col\">Item</th><th scope=\"col\">Description</th><th scope=\"col\">RRP</th><th scope=\"col\">Discount</th><th scope=\"col\">Qty</th><th scope=\"col\">Unit</th><th scope=\"col\">Total Line Price</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (AgentEntities entity = new AgentEntities())
            {
                var query = (from c in entity.QuoteItems
                              where c.Quote.QuoteId == id
                              orderby c.LineId
                              select c);

                if (query.Count() > 0)
                {
                    foreach (QuoteItem _item in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td><td style=\"text-align:left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">€{4}</td><td style=\"text-align: left\">{5}%</td><td style=\"text-align:left\">{6}</td> <td style=\"text-align: left\">{7}</td><td style=\text-align:left\">€{8}</td><td style=\"text-align: center\"><a title=\"Edit Quote Item\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewQuotePage.aspx','Agent/AddAgencyQuoteItemControl.ascx',{{ QuoteId: {0}, LineId: {1}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Enrollment\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewQuotePage.aspx','QuoteItem',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", _item.QuoteId, _item.LineId, _item.OrderableItem.Title, _item.Description, _item.RRPrice, _item.Discount, _item.Qty, _item.Xlk_Unit.Description, _item.TotalLinePrice);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"10\">No Items to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadQuotesByAgent(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','QuotePage.aspx','Agent/AddAgencyQuoteControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Quote</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Type</th><th scope=\"col\">QuoteId</th><th scope=\"col\">Created By</th><th scope=\"col\">Date Created</th><th scope=\"col\">Status</th><th scope=\"col\">SAGE Invoice Ref</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (AgentEntities entity = new AgentEntities())
            {
                var query = (from c in entity.Quotes
                             where c.Agency.AgencyId == id
                             orderby c.QuoteId
                             select c);

                if (query.Count() > 0)
                {
                    foreach (Quote quote in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">Quote</td> <td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align:left\">test</td><td style=\"text-align: center\"><a title=\"Edit Quote\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewAgentPage.aspx','Agent/AddAgencyQuoteControl.ascx','AgencyId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Quote\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('QuotePage.aspx','AgencyQuote',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", quote.QuoteId, quote.DateCreated.ToLongDateString(), quote.CreatedByUser.Name, quote.Xlk_Status.Description);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"7\">No Quotes to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, int> entitykeys)
    {
        try
        {
            var page = new BaseAgentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseAgentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("AgencyId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveAgencyComplaint(AgencyComplaint newObject)
    {
        try
        {
            AgencyComplaint newcomplaint = new AgencyComplaint();
            using (AgentEntities entity = new AgentEntities())
            {
                if (newObject.AgencyComplaintId > 0)
                    newcomplaint = (from c in entity.AgencyComplaints where c.AgencyComplaintId == newObject.AgencyComplaintId select c).FirstOrDefault();
                else
                    newcomplaint.DateCreated = DateTime.Now;

                newcomplaint.AgencyReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Agencies", newObject.Agency);
                newcomplaint.UserReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", GetCurrentUser());
                if (newObject.ActionNeeded == true)
                    newcomplaint.ActionNeeded = true;
                else
                    newcomplaint.ActionNeeded = false;
                newcomplaint.ActionTaken = newObject.ActionTaken;
                newcomplaint.Complaint = newObject.Complaint;
                newcomplaint.ComplaintTypeId = newObject.ComplaintTypeId;
                newcomplaint.RaisedBy = newObject.RaisedBy;
                newcomplaint.Resolved = newObject.Resolved;
                newcomplaint.SeverityId = newObject.SeverityId;

                if (newcomplaint.AgencyComplaintId == 0)
                    entity.AddToAgencyComplaints(newcomplaint);

                entity.SaveChanges();
            }

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveAgencyRateCard(RateCard newObject)
    {
        try
        {
            RateCard newrate = new RateCard();
            using (AgentEntities entity = new AgentEntities())
            {
                if (newObject.RateCardId > 0)
                    newrate = (from r in entity.RateCards where r.RateCardId == newObject.RateCardId select r).FirstOrDefault();

                newrate.AgencyReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Agencies", newObject.Agency);
                newrate.DateActiveFrom = Convert.ToDateTime(newObject.DateActiveFrom);
                newrate.DateActiveTo = Convert.ToDateTime(newObject.DateActiveTo);
                newrate.DefaultDiscount = Convert.ToDecimal(newObject.DefaultDiscount);
                newrate.DefaultUnitId = newObject.DefaultUnitId;
                newrate.OrderableItemReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".OrderableItems", newObject.OrderableItem);

                if (newrate.RateCardId == 0)
                    entity.AddToRateCards(newrate);

                entity.SaveChanges();
            }

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveAgencyNote(AgencyNote newObject)
    {
        try
        {
            AgencyNote newnote = new AgencyNote();
            using (AgentEntities entity = new AgentEntities())
            {
                if (newObject.AgencyNoteId > 0)
                    newnote = (from n in entity.AgencyNotes where n.AgencyNoteId == newObject.AgencyNoteId select n).FirstOrDefault();
                else
                    newnote.DateCreated = DateTime.Now;

                newnote.AgencyReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Agencies", newObject.Agency);
                newnote.Note = newObject.Note;
                newnote.Xlk_NoteTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_NoteType", newObject.Xlk_NoteType);
                newnote.UserReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", GetCurrentUser());

                if (newnote.AgencyNoteId == 0)
                    entity.AddToAgencyNotes(newnote);

                entity.SaveChanges();
            }

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveQuote(Quote newObject)
    {
        try
        {
            Quote newquote = new Quote();
            using (AgentEntities entity = new AgentEntities())
            {
                if (newObject.QuoteId > 0)
                    newquote = (from c in entity.Quotes where c.QuoteId == newObject.QuoteId select c).FirstOrDefault();
                else
                    newquote.DateCreated = DateTime.Now;

                if (!string.IsNullOrEmpty(newObject.SageInvoiceRef))
                {
                    newquote.SageInvoiceRef = newObject.SageInvoiceRef;
                    newquote.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status","StatusId" ,Convert.ToByte(3));
                }
                else
                    newquote.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);

                newquote.Description = newObject.Description;
                newquote.Comment = newObject.Comment;
                newquote.AgencyReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Agencies", newObject.Agency);
                newquote.CreatedByUserReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Users", GetCurrentUser());

                newquote.ArrivalDate = newObject.ArrivalDate;
                newquote.DepartureDate = newObject.DepartureDate;


                if (newquote.QuoteId == 0)
                    entity.AddToQuotes(newquote);

                entity.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveQuoteItem(QuoteItem newObject)
    {
        try
        {
            QuoteItem newquoteitem = new QuoteItem();
            using (AgentEntities entity = new AgentEntities())
            {
                if (newObject.QuoteId > 0 && newObject.LineId > 0)
                {
                    newquoteitem = (from q in entity.QuoteItems where q.QuoteId == newObject.QuoteId && q.LineId == newObject.LineId select q).FirstOrDefault();
                }

                newquoteitem.QuoteId = newObject.QuoteId;
                newquoteitem.Description = newObject.Description;
                newquoteitem.Discount = Convert.ToDecimal(newObject.Discount);
                newquoteitem.OrderableItemReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".OrderableItems", newObject.OrderableItem);
                newquoteitem.Qty = Convert.ToInt32(newObject.Qty);
                newquoteitem.RRPrice = Convert.ToDecimal(newObject.RRPrice);

                if (newObject.RateCard != null)
                    newquoteitem.RateCardReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".RateCards", newObject.RateCard);
                newquoteitem.Xlk_UnitReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Unit", newObject.Xlk_Unit);

                newquoteitem.TotalLinePrice = (newquoteitem.RRPrice-(newquoteitem.RRPrice * (newquoteitem.Discount/100))) * Convert.ToDecimal(newquoteitem.Qty);

                if (newObject.QuoteId > 0 && newObject.LineId == 0)
                    entity.AddToQuoteItems(newquoteitem);

                
                entity.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<AgentRateCardCostings> GetRateCardCostings(int quoteid, short itemid)
    {
        List<AgentRateCardCostings> _card = null;
        try
        {
            using (AgentEntities entity = new AgentEntities())
            {

               _card = entity.GetRateCardCostings(quoteid, itemid).ToList();

            }
            return _card;
        }
        catch (Exception)
        {
            return null;
        }
    }

    #endregion

}
