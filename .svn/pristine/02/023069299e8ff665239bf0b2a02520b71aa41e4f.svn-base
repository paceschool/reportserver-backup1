﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using PaceManager.Accommodation;
using System.Text;
using System.Data;

public partial class Accommodation_ViewAccomodationPage : BaseAccommodationPage
{
    protected Int32 _familyId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["FamilyId"]))
            _familyId = Convert.ToInt32(Request["FamilyId"]);

        if (!Page.IsPostBack)
        {
            listtype.DataSource = this.LoadFamilyTypes();
            listtype.DataBind();

            newzone.DataSource = this.LoadZones();
            newzone.DataBind();

            famstatus.DataSource = this.LoadStatus();
            famstatus.DataBind();

            DisplayFamily(LoadFamily(_familyId));
            DisplayCurrentStudents(LoadCurrentStudents(_familyId));

            if (Request.UrlReferrer != null)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
        }
    }
    protected void Save_Click(object sender, EventArgs e)
    {
        Family _family = LoadFamily(_familyId);

        _family.FirstName = famfirstname.Text;
        _family.SurName = famsurname.Text;
        _family.Address = famaddress.Text;
        _family.LandLine = famtel.Text;
        _family.Mobile = fammobile.Text;
        _family.EmailAddress = famemail.Text;
        _family.SageRef = sageref.Text;
        _family.Description = famdescription.Text;
        _family.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(famstatus.SelectedItem.Value));
        _family.Xlk_ZoneReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Zone", "ZoneId", Convert.ToInt32(newzone.SelectedItem.Value));
        _family.Xlk_FamilyTypeReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_FamilyType", "FamilyTypeId", Convert.ToByte(listtype.SelectedItem.Value));

        SaveFamily(_family);
    }

    private void SaveFamily(Family family)
    {
        if (Entities.SaveChanges() > 0)
        {
            DisplayFamily(LoadFamily(_familyId));
            DisplayCurrentStudents(LoadCurrentStudents(_familyId));
        }
    }

    private void DisplayFamily(Family family)
    {
        if (family != null)
        {
            //results.DataSource = new List<Family> { family };
            //results.DataBind();

            famfirstname.Text = family.FirstName;
            famsurname.Text = family.SurName;
            famaddress.Text = family.Address;
            famtel.Text = family.LandLine;
            fammobile.Text = family.Mobile;
            famemail.Text = family.EmailAddress;
            sageref.Text = family.SageRef;
            famdescription.Text = family.Description;
            famstatus.Items.FindByValue(family.Xlk_Status.StatusId.ToString()).Selected = true;
            newzone.SelectedIndex = 0;
            listtype.Items.FindByValue(family.Xlk_FamilyType.FamilyTypeId.ToString()).Selected = true;

            curFamId.Text = family.FamilyId.ToString();
            curFamName.Text = family.FirstName + " " + family.SurName;
            curFamAdd.Text = TrimText(family.Address, 20);
            curFamAdd.ToolTip = family.Address;
            curFamAdd.NavigateUrl = String.Format("http://maps.google.com/maps?q=" + family.Address + "&output=embed");
            curZone.Text = family.Xlk_Zone.Description;
            curPhone.Text = family.LandLine;
            curMob.Text = family.Mobile;
            curEmail.Text = family.EmailAddress;
            curSage.Text = family.SageRef;
            curStatus.Text = family.Xlk_Status.Description;

            string TextToTrim = "<br/>Family Type: " + family.Xlk_FamilyType.Description + "<br/><br/>Description: " + family.Description + "<br/><br/>";
            trimmedtext.Text = TrimText(TextToTrim, 1350);
        }
    }

    private Family LoadFamily(Int32 familyId)
    {
        IQueryable<Family> familyQuery = from f in Entities.Family.Include("FamilyMember").Include("FamilyMember.Xlk_FamilyMemberType").Include("Xlk_FamilyType").Include("Xlk_Zone").Include("Xlk_Status")
                                         where f.FamilyId == familyId
                                         select f;

        if (familyQuery.ToList().Count() > 0)
            return familyQuery.ToList().First();

        return null;


    }

    private List<Student> LoadCurrentStudents(Int32 familyId)
    {
        IQueryable<Student> query = from n in Entities.Student
                                    from h in n.Hostings
                                           where h.Family.FamilyId == familyId && (n.ArrivalDate >= DateTime.Now || (n.ArrivalDate <= DateTime.Now && n.DepartureDate >= DateTime.Now))
                                           select n;

        return query.ToList();


    }

    private void DisplayCurrentStudents(List<Student> students)
    {
        currentStudents.DataSource = students;
        currentStudents.DataBind();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> ListCalendarEvents(DateTime start, DateTime end, int id)
    {
        var studentQuery = from h in Entities.Hostings
                           where h.Family.FamilyId == id && ((h.ArrivalDate <= start && h.DepartureDate >= end) || (h.ArrivalDate >= start && h.DepartureDate <= end) || (h.ArrivalDate >= start && h.ArrivalDate <= end) || (h.DepartureDate >= start && h.DepartureDate <= end))
                           select new { Title = h.Student.FirstName + " " + h.Student.SurName, ArrivalDate =h.ArrivalDate, DepartureDate = h.DepartureDate };

        foreach (var item in studentQuery.ToList())
        {
            yield return new { Title = item.Title, ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString() };
        }
    }

  

}
