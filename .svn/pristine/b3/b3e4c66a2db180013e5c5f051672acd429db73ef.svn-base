﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using Pace.DataAccess.Content;
using System.Text;

public partial class WebContent_ContentManagerPage : BaseWebContentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listlanguages.DataSource = this.LoadLanguageCodes();
            listlanguages.DataBind();
        }    
    }



    private static IEnumerable<object> BuildElements(string parentkey, List<Pace.DataAccess.Content.Element> elements)
    {
        List<object> _objects = new List<object>();
        foreach (Pace.DataAccess.Content.Element element in elements)
        {
            _objects.Add(new { id = string.Format("ElementId:{0}", element.ElementId), icon = "../Content/img/actions/tv_element.png", parentid = parentkey, expanded = Boolean.FalseString, text = string.Format("{0} content {1}", element.Xlk_ElementType.Description, element.ElementValue), value = string.Format("ElementId:{0}", element.ElementId) });

            if (element.ChildElements.Count > 0)
                using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                {
                    _objects.AddRange(BuildElements(string.Format("ElementId:{0}", element.ElementId), logic.GetElements((from i in element.ChildElements select i.ChildElementId).ToList())));
                }
        }
        return _objects;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> BuildTreeJson(string languagecode, string curdeployment, string lookupstring)
    {
        bool branchexpand = false;

        List<Pace.DataAccess.Content.Module> _contents;
        List<string> filters = new List<string>(); ;

        using (ContentEntities entity = new ContentEntities())
        {
            if (!string.IsNullOrEmpty(lookupstring))
            {
                string search = lookupstring.Trim();
                filters = search.Split(',').ToList();
            }

            using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
            {
                if (languagecode == "en")
                    _contents = logic.FindContent(filters);
                else
                    _contents = logic.FindContent(filters, languagecode);

            }

            var languages = (from q in _contents select new { q.LanguageId, q.Xlk_LanguageCode }).Distinct();

            if (languages.Count() > 0)
            {
                branchexpand = true;


                foreach (var language in languages)
                {
                    yield return new { id = string.Format("LanguageId:{0}", language.LanguageId), icon = "../Content/img/actions/tv_language.png", parentid = "-1", expanded = branchexpand.ToString(), text = language.Xlk_LanguageCode.Description, value = string.Format("LanguageId:{0}", language.LanguageId) };

                    var pages = (from q in _contents where q.LanguageId == language.LanguageId select new { q.ModuleId, q.PageURL }).Distinct();

                    if (pages.Count() > 0)
                    {
                        branchexpand = true;

                        foreach (var page in pages)
                        {
                            yield return new { id = string.Format("PageURL:{1}", language.LanguageId, page.PageURL), icon = "../Content/img/actions/globe.png", parentid = string.Format("LanguageId:{0}", language.LanguageId), expanded = Boolean.FalseString, text = string.IsNullOrEmpty(page.PageURL) ? "Global" : page.PageURL, value = string.Format("PageURL:{0}", page.PageURL) };

                            var modules = (from q in _contents where q.LanguageId == language.LanguageId && q.PageURL == page.PageURL select new { q.ModuleId, q.Title, q.Description, q.Lnk_Module_Element }).Distinct();

                            if (modules.Count() > 0)
                            {

                                foreach (var module in modules)
                                {
                                    yield return new { id = string.Format("ModuleId:{0}", page.ModuleId), icon = "../Content/img/actions/tv_module.png", parentid = string.Format("PageURL:{0}", page.PageURL), expanded = Boolean.FalseString, text = module.Title, value = string.Format("ModuleId:{0}", page.ModuleId) };

                                    if (module.Lnk_Module_Element.Count > 0)
                                    {
                                        using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                                        {
                                            foreach (var item in BuildElements(string.Format("ModuleId:{0}", page.ModuleId), logic.GetElements(module.Lnk_Module_Element.Select(e => e.ElementId).ToList())))
                                            {
                                                yield return item;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }




    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> GetModuleFilters(int moduleId)
    {

        using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
        {
            return logic.GetModuleFilters(moduleId).Select(x => new {x.ModuleId,x.TagId,x.Tag }); 
        }

 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> GetAttributes(int elementId)
    {

        using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
        {
            return logic.GetAttributes(elementId).Select(x => new { x.ElementId, x.AttributeTypeId,x.Xlk_AttributeType.Description, x.AttributeValue });
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse CloneModule(Dictionary<string, object> newObject)
    {
        Int32? _moduleId;
        bool? _recurse;
        try
        {
            _moduleId = (newObject.ContainsKey("ModuleId")) ? Convert.ToInt32(newObject["ModuleId"]) : (Int32?)null;

            _recurse = (newObject.ContainsKey("Recurse")) ? Convert.ToBoolean(newObject["Recurse"]) : (bool?)null;

            if (_moduleId.HasValue)
            {
                using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                {
                    if (logic.CloneModule(_moduleId.Value, _recurse))
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("There was an issue cloning the module, please try again!"));
                }
            }
            return new AjaxResponse(new Exception("There was an issue pulling out the parameters, please try again!"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse CloneElement(Dictionary<string, object> newObject)
    {
        Int32? _elementId;
        bool? _recurse;
        try
        {
            _elementId = (newObject.ContainsKey("ElementId")) ? Convert.ToInt32(newObject["ElementId"]) : (Int32?)null;

            _recurse = (newObject.ContainsKey("Recurse")) ? Convert.ToBoolean(newObject["Recurse"]) : (bool?)null;

            if (_elementId.HasValue)
            {
                using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
                {
                    if (logic.CloneElement(_elementId.Value, _recurse))
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("There was an issue cloning the element, please try again!"));
                }
            }
            return new AjaxResponse(new Exception("There was an issue pulling out the parameters, please try again!"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }

    }
}



