﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using PaceManager.Group;
using PaceManager.Excursion;


/// <summary>
/// Summary description for BaseEnrollmentPage
/// </summary>
public partial class BaseGroupPage : BasePage
{
    public GroupEntities _entities { get; set; }

    public BaseGroupPage()
    {
    }

    #region Events

    public static GroupEntities Entities
    {
        get
        {
            GroupEntities value;

            if (CacheManager.Instance.GetFromCache<GroupEntities>("GroupEntitiesObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<GroupEntities>("GroupEntitiesObject", new GroupEntities());

        }
    }

    public IList<Xlk_Nationality> LoadNationalities()
    {
        List<Xlk_Nationality> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Nationality>>("AllGroupNationalities", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Nationality>>("AllGroupNationalities", GetNationalities().ToList());
    }

    public IList<Xlk_LessonBlock> LoadLessonBlocks()
    {
        List<Xlk_LessonBlock> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_LessonBlock>>("AllGroupLessonBlocks", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_LessonBlock>>("AllGroupLessonBlocks", GetLessonBlocks().ToList());
    }

    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllGroupProgrammeTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllGroupProgrammeTypes", GetProgrammeTypes().ToList());
    }

    //public IList<Xlk_CourseType> LoadCourseTypes()
    //{
    //    List<Xlk_CourseType> values = null;

    //    if (CacheManager.Instance.GetFromCache<List<Xlk_CourseType>>("AllGroupCourseTypes", out values))
    //        return values;

    //    return CacheManager.Instance.AddToCache<List<Xlk_CourseType>>("AllGroupCourseTypes", GetCourseTypes().ToList());
    //}

    public IList<Agency> LoadAgents()
    {
        List<Agency> values = null;

        if (CacheManager.Instance.GetFromCache<List<Agency>>("AllGroupAgents", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Agency>>("AllGroupAgents", GetAgents().ToList());
    }

    public IList<PaceManager.Group.Xlk_Status> LoadStatus()
    {
        List<PaceManager.Group.Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<PaceManager.Group.Xlk_Status>>("AllGroupStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<PaceManager.Group.Xlk_Status>>("AllGroupStatus", GetStatus().ToList());
    }
    #endregion


    #region Private Methods

    private IQueryable<Xlk_Nationality> GetNationalities()
    {
        IQueryable<Xlk_Nationality> lookupQuery =
            from a in Entities.Xlk_Nationality
            select a;
        return lookupQuery;
    }

    private IQueryable<Xlk_LessonBlock> GetLessonBlocks()
    {
        IQueryable<Xlk_LessonBlock> lookupQuery =
            from a in Entities.Xlk_LessonBlock
            select a;
        return lookupQuery;
    }

    private IQueryable<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        IQueryable<Xlk_ProgrammeType> lookupQuery =
            from b in Entities.Xlk_ProgrammeType
            select b;
        return lookupQuery;
    }

    //private IQueryable<Xlk_CourseType> GetCourseTypes()
    //{

    //    IQueryable<Xlk_CourseType> lookupQuery =
    //        from c in Entities.Xlk_CourseType
    //        select c;
    //    return lookupQuery;
    //}

    private IQueryable<PaceManager.Group.Xlk_Status> GetStatus()
    {
        IQueryable<PaceManager.Group.Xlk_Status> lookupQuery =
            from c in Entities.Xlk_Status
            select c;
        return lookupQuery;
    }

    private IQueryable<Agency> GetAgents()
    {
        IQueryable<Agency> lookupQuery =
            from d in Entities.Agency
            select d;
        return lookupQuery;
    }



    protected string CreateName(object firstName, object secondName)
    {
        if (firstName != null && secondName != null)
        {
            return string.Format("{0} {1}", firstName, secondName);
        }
        else
        {
            return (firstName != null) ? firstName.ToString() : secondName.ToString();
        }
    }
    protected static Group GetGroup(int groupId)
    {

        IQueryable<Group> groupQuery = from g in Entities.Group.Include("Agency").Include("Xlk_Status").Include("Xlk_Nationality")
                           where g.GroupId == groupId
                           select g;

        if (groupQuery.Count() > 0)
        {
            return groupQuery.First();

        }
            

        return null;
    }

    #endregion



    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupEnrollments(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}?/PaceManagerReports/TestGroupCert&GroupId={0}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\"><img src=\"../Content/img/actions/printer.png\" />Print Certificates</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupEnrollmentControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Enrollment</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Enrollment Id</th><th scope=\"col\">Course Start Date</th><th scope=\"col\">Course Finish Date</th><th scope=\"col\">#Weeks</th><th scope=\"col\">Programme Type</th><th scope=\"col\">Lesson Block</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from n in Entities.Enrollment.Include("Xlk_ProgrammeType").Include("Xlk_LessonBlock")
                        where n.Group.GroupId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (Enrollment enrollment in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: left\">{5}</td><td style=\"text-align: center\"><a title=\"Edit Enrollment\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupEnrollmentControl.ascx','EnrollmentId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Enrollment\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupEnrollment',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", enrollment.EnrollmentId, enrollment.StartDate.ToString("D"), (enrollment.EndDate.HasValue) ? enrollment.EndDate.Value.ToString("D") : String.Empty, CalculateTimePeriod(enrollment.StartDate, enrollment.EndDate, 'W'), enrollment.Xlk_ProgrammeType.Description, enrollment.Xlk_LessonBlock.Description);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"7\">No Enrollments to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupExcursions(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}?/PaceManagerReports/GroupExcursion&GroupId={0}&rs:Command=Render&rs:Format=PDF&rc:Parameters=False\"><img src=\"../Content/img/actions/printer.png\" />Print Excursions List</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupExcursionControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add Excursion</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Group Excursion Id</th><th scope=\"col\">Excursion</th><th scope=\"col\">Preferred Date</th><th scope=\"col\">Preferred Time</th><th scope=\"col\">Qty Leaders/Students/Guides</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from e in Entities.GroupExcursion.Include("Excursion")
                        where e.Group.GroupId == id
                        orderby e.PreferredDate, e.PreferredTime
                        select e;

            if (query.Count() > 0)
            {
                foreach (GroupExcursion group in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4} / {5} / {6}</td><td style=\"text-align: left\"><a href=\"#\" class=\"information\">{7}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Edit Excursion\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupExcursionControl.ascx','GroupExcursionId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Excursion\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupExcursion', {0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", group.GroupExcursionId, group.Excursion.Title, group.PreferredDate.ToString("D"), group.PreferredTime, group.QtyStudents, group.QtyLeaders, group.QtyGuides, TrimText(group.Comment,20), group.Comment);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"9\">No Excursions to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupComplaints(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupComplaintControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Complaint</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">StudentComplaintId</th><th scope=\"col\">Severity</th><th scope=\"col\">Type</th><th scope=\"col\">Complaint</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.GroupComplaint.Include("Xlk_Severity").Include("Xlk_ComplaintType")
                        where n.Group.GroupId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (GroupComplaint complaint in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Delete Complaint\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupComplaint',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", complaint.GroupComplaintId, complaint.Xlk_Severity.Description, complaint.Xlk_ComplaintType.Description, complaint.Complaint, complaint.DateCreated.ToString("D"));
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Complaints to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupNotes(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupNoteControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Note</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">NoteId</th><th scope=\"col\">Note Type</th><th scope=\"col\">Note</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.GroupNote.Include("Xlk_NoteType")
                        where n.Group.GroupId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (GroupNote note in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: center\"><a title=\"Delete Note\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupNote',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td> </tr>", note.GroupNoteId, note.Xlk_NoteType.Description, note.Note, note.DateCreated.Value.ToString("D"), note.GroupNoteId);
                }
           }
            else
            {
                sb.Append("<tr><td colspan=\"5\">No Notes to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupHostings(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}?/PaceManagerReports/GroupFamilyListing&GroupId={0}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\"><img src=\"../Content/img/actions/printer.png\" />Print Listing</a></span><span style=\"float:right\"><a href=\"../Accommodation/GroupHostingPage.aspx?GroupId={0}\"><img src=\"../Content/img/actions/house_go.png\" />Assign Family</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Type</th><th scope=\"col\">Student</th><th scope=\"col\">Family</th><th scope=\"col\">Arrival</th><th scope=\"col\">Departure</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from n in BaseAccommodationPage.Entities.Hostings.Include("Family").Include("Student")
                        where n.Student.GroupId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (PaceManager.Accommodation.Hosting hosting in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{6}</td><td style=\"text-align: left\"><a href=\"../Enrollments/ViewStudentPage.aspx?StudentId={1}\">{0}</a></td> <td style=\"text-align: left\"><a href=\"../Accommodation/ViewAccommodationPage.aspx?FamilyId={3}\">{2}</a></td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: left\">{5}</td><td style=\"text-align: center\"><a title=\"Delete Hosting\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','StudentHosting',{6})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", BasePage.CreateName(hosting.Student.FirstName, hosting.Student.SurName), hosting.Student.StudentId, BasePage.CreateName(hosting.Family.FirstName, hosting.Family.SurName), hosting.Family.FamilyId, hosting.ArrivalDate.ToString("D"), hosting.DepartureDate.ToString("D"), hosting.HostingId);
                }

            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Hosting Records to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupStudents(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}?/PaceManagerReports/GroupFolder&GroupId={0}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\"><img src=\"../Content/img/actions/printer.png\" />Print Cover Sheet</a></span><span style=\"float:right\"><a href=\"../Accommodation/GroupHostingPage.aspx?GroupId={0}\"><img src=\"../Content/img/actions/house_go.png\" />Assign Family</a>&nbsp;<a href=\"#\" onclick=\"loadControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupStudentControl.ascx',{0})\"><img src=\"../Content/img/actions/add.png\" />Add New Member</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Type</th><th scope=\"col\">Name</th><th scope=\"col\">Gender</th><th scope=\"col\">Age</th><th scope=\"col\">DOB</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from n in BaseEnrollmentPage.Entities.Student.Include("Xlk_Gender").Include("Xlk_StudentType")
                        where n.GroupId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (PaceManager.Enrollment.Student student in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{6}</td><td style=\"text-align: left\"><a href=\"../Enrollments/ViewStudentPage.aspx?StudentId={5}\">{0} {1}</a></td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Delete Complaint\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupStudent',{5})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", student.FirstName, student.SurName, student.Xlk_Gender.Description, BasePage.CalculateAge(student.DateOfBirth), (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("D") : string.Empty, student.StudentId, student.Xlk_StudentType.Description);
                }

            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Previous Students to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupTransfers(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:left\"><a href=\"{1}?/PaceManagerReports/GroupDetailsReport&GroupId={0}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False\"><img src=\"../Content/img/actions/printer.png\" />Print Group Details</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewGroupPage.aspx','Groups/AddGroupTransferControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Transfer</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Group TransferId</th><th scope=\"col\">Type</th><th scope=\"col\">Transfer Date</th><th scope=\"col\">Location </th><th scope=\"col\">Flight Number</th><th scope=\"col\">Completed By</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from h in Entities.GroupTransfer.Include("Xlk_Location").Include("Xlk_TransferType").Include("Xlk_BusinessEntity")
                        where h.Group.GroupId == id
                        select h;


            if (query.Count() > 0)
            {
                foreach (GroupTransfer transfer in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2} @ {7}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\"><a href=\"#\" class=\"information\" style=\"font-size: x-small\">{6}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Delete Hosting\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewStudentPage.aspx','StudentTransfer',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", transfer.GroupTransferId, transfer.Xlk_TransferType.Description, transfer.Date.ToString("D"), transfer.Xlk_Location.Description, transfer.FlightNumber, transfer.Xlk_BusinessEntity.Description, TrimText(transfer.Comment,20), (transfer.Time.HasValue) ? transfer.Time.Value.ToString() : string.Empty, transfer.Comment);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, int> entitykeys)
    {
        try
        {
            var page = new BaseEnrollmentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseEnrollmentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("GroupId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    public static string SaveGroupNote(GroupNote newObject)
    {
        try
        {
            GroupNote newnote = new GroupNote();

            newnote.DateCreated = DateTime.Now;
            //newnote.GroupNoteId = newObject.GroupNoteId;
            newnote.Note = newObject.Note;
            newnote.GroupReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Group", newObject.Group);
            newnote.Xlk_NoteTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_NoteType", newObject.Xlk_NoteType);
            newnote.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newnote.GroupNoteId == 0)
                Entities.AddToGroupNote(newnote);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    public static string SaveGroupComplaint(GroupComplaint newObject)
    {
        try
        {
            GroupComplaint newcomplaint = new GroupComplaint();

            newcomplaint.ActionNeeded = newObject.ActionNeeded;
            newcomplaint.Complaint = newObject.Complaint;
            newcomplaint.DateCreated = DateTime.Now;
            newcomplaint.GroupReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Group", newObject.Group);
            newcomplaint.Xlk_ComplaintTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_ComplaintType", newObject.Xlk_ComplaintType);
            newcomplaint.Xlk_SeverityReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_Severity", newObject.Xlk_Severity);
            newcomplaint.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newcomplaint.GroupComplaintId == 0)
                Entities.AddToGroupComplaint(newcomplaint);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveGroupExcursion(GroupExcursion newObject)
    {
        GroupExcursion newexcursion = new GroupExcursion();

        try
        {

            if (newObject.GroupExcursionId > 0)
                newexcursion = (from e in Entities.GroupExcursion where e.GroupExcursionId == newObject.GroupExcursionId select e).FirstOrDefault();


            newexcursion.Comment = newObject.Comment;
            newexcursion.PreferredDate = newObject.PreferredDate;
            newexcursion.PreferredTime = newObject.PreferredTime;
            newexcursion.QtyGuides = newObject.QtyGuides;
            newexcursion.QtyLeaders = newObject.QtyLeaders;
            newexcursion.QtyStudents = newObject.QtyStudents;
            newexcursion.GroupReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Group", newObject.Group);
            newexcursion.ExcursionReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Excursion", newObject.Excursion);

            if (newexcursion.GroupExcursionId == 0)
                Entities.AddToGroupExcursion(newexcursion);

            Entities.SaveChanges();

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveEnrollment(Enrollment newObject)
    {
        Enrollment newenrollment = new Enrollment();
        
        try
        {
            if (newObject.EnrollmentId > 0)
                newenrollment = (from e in Entities.Enrollment where e.EnrollmentId == newObject.EnrollmentId select e).FirstOrDefault();

            newenrollment.EndDate = newObject.EndDate;
            newenrollment.StartDate = newObject.StartDate;
            newenrollment.GroupReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Group", newObject.Group);
            newenrollment.Xlk_LessonBlockReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_LessonBlock", newObject.Xlk_LessonBlock);
            newenrollment.Xlk_ProgrammeTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_ProgrammeType", newObject.Xlk_ProgrammeType);

            if (newenrollment.EnrollmentId == 0)
                Entities.AddToEnrollment(newenrollment);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveGroupStudent(PaceManager.Enrollment.Student newObject)
    {
        try
        {

            Group _group = GetGroup(newObject.GroupId.Value);
            PaceManager.Enrollment.Xlk_Status status = (from s in BaseEnrollmentPage.LoadStatus()
                                                        where s.StatusId == Convert.ToByte((_group.Xlk_Status.StatusId == 1) ? 0 : 1)
                                                        select s).FirstOrDefault();


            if (_group != null)
            {
                PaceManager.Enrollment.Student newstudent = new PaceManager.Enrollment.Student();

                newstudent.DateOfBirth = newObject.DateOfBirth;
                newstudent.ArrivalDate = _group.ArrivalDate;
                newstudent.DepartureDate = _group.DepartureDate;
                newstudent.FirstName = newObject.FirstName;
                newstudent.SurName = newObject.SurName;
                newstudent.GroupId = newObject.GroupId;

                newstudent.Xlk_GenderReference.EntityKey = BaseEnrollmentPage.Entities.CreateEntityKey(BaseEnrollmentPage.Entities.DefaultContainerName + ".Xlk_Gender", newObject.Xlk_Gender);
                newstudent.Xlk_StudentTypeReference.EntityKey = BaseEnrollmentPage.Entities.CreateEntityKey(BaseEnrollmentPage.Entities.DefaultContainerName + ".Xlk_StudentType", newObject.Xlk_StudentType);

                newstudent.Xlk_NationalityReference.EntityKey = new EntityKey(BaseEnrollmentPage.Entities.DefaultContainerName + ".Xlk_Nationality", "NationalityId", (newObject.Xlk_Nationality == null) ? _group.Xlk_Nationality.NationalityId : newObject.Xlk_Nationality.NationalityId);// BaseEnrollmentPage.Entities.CreateEntityKey(BaseEnrollmentPage.Entities.DefaultContainerName + ".Xlk_Nationality", (newObject.Xlk_Nationality == null) ? group.Xlk_Nationality : newObject.Xlk_Nationality);
                newstudent.Xlk_StatusReference.EntityKey = new EntityKey(BaseEnrollmentPage.Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte((_group.Xlk_Status.StatusId == 1) ? 0 : 1)); 
                newstudent.AgencyReference.EntityKey = new EntityKey(BaseEnrollmentPage.Entities.DefaultContainerName + ".Agency", "AgencyId", _group.Agency.AgencyId);


                if (newstudent.StudentId == 0)
                    BaseEnrollmentPage.Entities.AddToStudent(newstudent);

                BaseEnrollmentPage.Entities.SaveChanges();

                return string.Empty;
            }
            else
                return "The Group could not be found";

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveGroupTransfer(GroupTransfer newObject)
    {
        try
        {
            GroupTransfer newtransfer = new GroupTransfer();

            newtransfer.Comment = newObject.Comment;
            newtransfer.Date = newObject.Date;
            newtransfer.FlightNumber = newObject.FlightNumber;
            newtransfer.Time = newObject.Time;
            newtransfer.NumberOfPassengers = newObject.NumberOfPassengers;

            newtransfer.TransportBookingId = newObject.TransportBookingId;
            newtransfer.GroupReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Group", newObject.Group);
            newtransfer.Xlk_LocationReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_Location", newObject.Xlk_Location);
            newtransfer.Xlk_TransferTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_TransferType", newObject.Xlk_TransferType);
            newtransfer.Xlk_BusinessEntityReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_BusinessEntity", newObject.Xlk_BusinessEntity);

            if (newtransfer.GroupTransferId == 0)
                Entities.AddToGroupTransfer(newtransfer);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

}
