﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Support;
using System.Data;
using System.Text;
using System.Data.Objects.DataClasses;
using System.Web.Services;
using System.Web.Script.Services;


public partial class Support_AddTemplateTaskControl : BaseSupportControl
{
    protected Int32 _templateTaskId, _parentTaskId;
    protected TemplateTask _templateTask;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("TemplateTaskId") != null)
        {
            Int32 _templateTaskId = (Int32)GetValue("TemplateTaskId");
            using (PaceManager.Support.SupportEntities entity = new PaceManager.Support.SupportEntities())
            {
                _templateTask = (from task in entity.TemplateTasks.Include("Xlk_Priority").Include("Xlk_Status").Include("AssignedToUser").Include("Template")
                         where task.TemplateTaskId == _templateTaskId
                         select task).FirstOrDefault();
            }

        }
    }


    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetTemplateTaskId
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.TemplateTaskId.ToString();

            if (GetValue("TemplateTaskId") != null)
                return GetValue("TemplateTaskId").ToString();

            return "0";
        }
    }


    protected string GetTemplateId
    {
        get
        {
            if (_templateTask != null && _templateTask.Template != null)
                return _templateTask.Template.TemplateId.ToString();

            if (GetValue("TemplateId") != null)
                return GetValue("TemplateId").ToString();

            return "0";
        }
    }
    protected string GetParentTemplateTaskId
    {
        get
        {
            if (_templateTask != null && _templateTask.ParentTask != null)
                return _templateTask.ParentTask.TemplateTaskId.ToString();

            if (GetValue("ParentTemplateTaskId") != null)
                return GetValue("ParentTemplateTaskId").ToString();

            return "0";
        }
    }
    protected string GetPriorityList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_templateTask != null && _templateTask.Xlk_Priority != null) ? _templateTask.Xlk_Priority.PriorityId : (short?)null;

        foreach (Xlk_Priority item in LoadPriority())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.PriorityId, item.Description, (_current.HasValue && item.PriorityId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_templateTask != null && _templateTask.Xlk_Status != null) ? _templateTask.Xlk_Status.StatusId : (short?)null;


        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetAssignedToUserList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_templateTask != null && _templateTask.AssignedToUser != null) ? _templateTask.AssignedToUser.UserId : (int?)null;

        foreach (Users item in LoadUsers())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetDateUsedList()
    {
        StringBuilder sb = new StringBuilder();

        int? _current = (_templateTask != null && _templateTask.Xlk_DateUsed != null) ? _templateTask.Xlk_DateUsed.DateUsedId : (int?)null;

        foreach (Xlk_DateUsed item in LoadDateUsed())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.DateUsedId, item.Description, (_current.HasValue && item.DateUsedId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetDays
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.Days.ToString();

            return null;
        }
    }


    protected string GetActionNeeded
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.ActionNeeded;

            return null;
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.Description;

            return null;
        }
    }
       
    protected string GetTitle
    {
        get
        {
            if (_templateTask != null)
                return _templateTask.Title;

            return null;
        }
    }
}