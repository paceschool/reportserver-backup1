﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Support;
using System.Data;
using System.Text;
using System.Data.Objects.DataClasses;
using System.Web.Services;
using System.Web.Script.Services;

public partial class ViewTaskPage : BaseSupportPage
{
    protected Int32? _taskId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["TaskId"]))
            _taskId = Convert.ToInt32(Request["TaskId"]);

        if (!Page.IsPostBack)
        {
            newtaskpriority.DataSource = LoadPriority();
            newtaskpriority.DataBind();
            newtaskstatus.DataSource = LoadStatus();
            newtaskstatus.DataBind();
            newtaskusers.DataSource = LoadUsers();
            newtaskusers.DataBind();
            newassignedto.DataSource = LoadUsers();
            newassignedto.DataBind();

            if (_taskId.HasValue)
            {
                DisplayTask(LoadTask(_taskId.Value));
                DisplayChildTasks(LoadChildTasks(_taskId.Value));
            }
        }
    }

    private Task LoadTask(Int32 taskId)
    {
        IQueryable<Task> taskQuery = from t in Entities.Task.Include("Users").Include("Xlk_Priority").Include("Xlk_Status").Include("TaskAssignments")
                                     where t.TaskId == taskId
                                     select t;

        if (taskQuery.ToList().Count() > 0)
            return taskQuery.ToList().First();
            

        return null;
    }

    private void DisplayTask(Task task)
    {
        if (task != null)
        {
            TaskId.Text = task.TaskId.ToString();
            Description.Text = task.Description;
            DateDue.Text = task.DateDue.ToString("dd/MM/yyyy");
            ActionNeeded.Text = task.ActionNeeded;
            
            newtaskdescription.Text = task.Description;
            newtaskactionneeded.Text = task.ActionNeeded;
            newtaskdatedue.Text = task.DateDue.ToString("dd/MM/yyyy");
            newtaskactiontaken.Text = task.ActionTaken;
            if (task.DateCompleted.HasValue)
                newtaskdatecompleted.Text = task.DateCompleted.Value.ToString("dd/MM/yyyy");
            else
                newtaskdatecompleted.Text = null;
            newtaskpriority.Items.FindByValue(task.Xlk_Priority.PriorityId.ToString()).Selected = true;
            newtaskusers.Items.FindByValue(task.Users.UserId.ToString()).Selected = true;
            if (task.Xlk_Status != null)
                newtaskstatus.Items.FindByValue(task.Xlk_Status.StatusId.ToString()).Selected = true;
            newraiseddate.Text = task.RaisedDate.ToString("dd/MM/yyyy");
            newtaskref.Text = task.Ref;

            var getAssign = from u in Entities.Users
                            where u.UserId == task.AssignedToUserId
                            select u.UserId;
            newassignedto.SelectedIndex = getAssign.First()-1;
        }
    }

    protected void Click_Save(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(newtaskdescription.Text) || !string.IsNullOrEmpty(newtaskactionneeded.Text) || !string.IsNullOrEmpty(newtaskdatedue.Text)
            || !string.IsNullOrEmpty(newraiseddate.Text))
        {
            Task _task = GetTask();

            _task.ActionNeeded = newtaskactionneeded.Text;
            _task.ActionTaken = newtaskactiontaken.Text;
            if (newtaskdatecompleted.Text != "")
                _task.DateCompleted = Convert.ToDateTime(newtaskdatecompleted.Text);
            _task.DateDue = Convert.ToDateTime(newtaskdatedue.Text);
            _task.Description = newtaskdescription.Text;
            _task.RaisedDate = Convert.ToDateTime(newraiseddate.Text);
            _task.Ref = newtaskref.Text;
            _task.UsersReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Users", "UserId", Convert.ToInt32(newtaskusers.SelectedItem.Value));
            _task.Xlk_PriorityReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Priority", "PriorityId", Convert.ToByte(newtaskpriority.SelectedItem.Value));
            _task.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newtaskstatus.SelectedItem.Value));
            _task.AssignedToUserId = Convert.ToInt32(newassignedto.SelectedItem.Value);

            if (!string.IsNullOrEmpty(newtaskactiontaken.Text) && newtaskstatus.SelectedItem.Text == "Closed")
            {
                if (string.IsNullOrEmpty(newtaskdatecompleted.Text))
                    _task.DateCompleted = DateTime.Now;
                SaveTask(_task);
            }
            else
            {
                string script = "<script type=\"text/javascript\">alert('Cannot Save Task!');</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script);

            }
        }
    }

    private Task GetTask()
    {
        if(_taskId.HasValue)
        return LoadTask(_taskId.Value);

        return new Task();
    }

    private void SaveTask(Task task)
    {
        if (!_taskId.HasValue)
            Entities.AddToTask(task);

        if (Entities.SaveChanges() > 0)
            Response.Redirect(string.Format("~/Support/TaskPage.aspx?Action=showall"));
    }

    private List<Task> LoadChildTasks(Int32 taskId)
    {
        IQueryable<Task> lookupQuery = from t in Entities.Task.Include("Xlk_Priority").Include("Users")
                                       where t.Task2.TaskId == taskId
                                       select t;

        return lookupQuery.ToList();
    }

    private void DisplayChildTasks(List<Task> tasks)
    {
        childtasks.DataSource = tasks;
        childtasks.DataBind();
    }

    protected void LoadChildren(Int32 taskId)
    {

    }

}
