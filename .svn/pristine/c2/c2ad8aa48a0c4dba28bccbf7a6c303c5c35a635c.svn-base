﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Text;

/// <summary>
/// Summary description for ExtensionMethods
/// </summary>
public static class ExtensionMethods
{
    public static DateTime NextWeekDay(this DateTime date, DayOfWeek weekday)
    {
        return (from i in Enumerable.Range(0, 7)
                where date.AddDays(i).DayOfWeek == weekday
                select date.AddDays(i)).First();

    }

    public static string ToQueryString(this Dictionary<string, object> _params)
    {
        StringBuilder sb = new StringBuilder();

        if (_params != null && _params.Count > 0)
        {
            foreach (KeyValuePair<string,object> item in _params)
            {
                sb.AppendFormat("&{0}={1}", item.Key, item.Value.ToString());
            }
        }
        return sb.ToString();

    }

    public static Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(

        Expression<Func<TElement, TValue>> valueSelector, IEnumerable<TValue> values)
    {

        if (null == valueSelector) { throw new ArgumentNullException("valueSelector"); }

        if (null == values) { throw new ArgumentNullException("values"); }

        ParameterExpression p = valueSelector.Parameters.Single();

        // p => valueSelector(p) == values[0] || valueSelector(p) == ...

        if (!values.Any())
        {
            return e => false;

        }

        var equals = values.Select(value => (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));

        var body = equals.Aggregate<Expression>((accumulate, equal) => Expression.Or(accumulate, equal));

        return Expression.Lambda<Func<TElement, bool>>(body, p);

    }
}
