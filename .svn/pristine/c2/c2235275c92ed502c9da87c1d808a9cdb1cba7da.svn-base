﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Enrollment;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ManageEnrollmentPage : BaseEnrollmentPage
{
    protected Int32 _studentId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!Page.IsPostBack)
        {
            if (_studentId == 0)
            {
                
            }
            else
            {
                IQueryable<Student> tuQuery = from f in Entities.Student
                                              where f.StudentId == _studentId
                                              select f;

                LoadStudentDetails(tuQuery.ToList().First());

                var enQuery = from g in Entities.Enrollment.Include("Student")
                              where g.Student.StudentId == _studentId
                              select g;

                LoadEnrollmentDetails(enQuery.ToList());

                listprogramme.DataSource = this.LoadProgrammeTypes();
                listprogramme.DataBind();

                listlesson.DataSource = this.LoadLessonBlocks();
                listlesson.DataBind();
            }
        }
    }

    #region Methods

    private void LoadStudentDetails(Student student)
    {
        StudentId.Text = student.StudentId.ToString();
        Name.Text = String.Format("{0} {1}", student.FirstName, student.SurName);
        CourseStart.Text = student.ArrivalDate.Value.ToString("d");
        CourseFinish.Text = student.DepartureDate.Value.ToString("d");
    }

    private void LoadEnrollmentDetails(List<Enrollment> enrollment)
    {
        results.DataSource = enrollment;
        results.DataBind();
    }

    protected void Click_DeleteEnrollment(object sender, ImageClickEventArgs e)
    {
        object todelete = null;
        _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!string.IsNullOrEmpty(((ImageButton)sender).ToolTip))
        {
            Int64 _enrollmentId = Convert.ToInt64(((ImageButton)sender).ToolTip);
            if (Entities.TryGetObjectByKey(new EntityKey(Entities.DefaultContainerName + ".Enrollment", "EnrollmentId", _enrollmentId), out todelete))
            {
                Entities.DeleteObject(todelete);
                if (Entities.SaveChanges() > 0)
                {
                    Response.Redirect(string.Format("~/Enrollments/ManageEnrollmentPage.aspx?StudentId={0}", _studentId = Convert.ToInt32(Request["StudentId"])));
                }
            }
        }
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    public static string AddEnrollment(int studentid, string startdate, string enddate, byte lessontype, byte programmetype)
    {

        string _message = string.Empty;

        Enrollment addEnrollment = new Enrollment();

        if (string.IsNullOrEmpty(startdate))
            return "You must supply a start date";

        if (string.IsNullOrEmpty(enddate))
            return "You must supply a end date";

        if (lessontype == 0)
            return "You must supply a lesson type";

        if (programmetype == 0)
            return "You must supply a programme type";

        addEnrollment.StudentReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Student", "StudentId", studentid);
        addEnrollment.StartDate = Convert.ToDateTime(startdate);
        addEnrollment.EndDate = Convert.ToDateTime(enddate);
        addEnrollment.Xlk_LessonBlockReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_LessonBlock", "LessonBlockId", lessontype);
        addEnrollment.Xlk_ProgrammeTypeReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_ProgrammeType", "ProgrammeTypeId", programmetype);

        Entities.AddToEnrollment(addEnrollment);

        if (!(Entities.SaveChanges() > 0))
            return "There was a problem saving the data please try again";

        return string.Empty;
    }

    #endregion
}
