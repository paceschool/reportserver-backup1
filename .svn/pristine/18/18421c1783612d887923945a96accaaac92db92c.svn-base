﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Excursion;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Script.Services;

public partial class Excursions_SearchExcursionsPage : BaseExcursionPage
{
    #region Events
    protected Int32 _bookingId;
    protected Int32 _excursionId;
    protected DateTime _excursionDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["BookingId"]))
            _bookingId = Convert.ToInt32(Request["BookingId"]);

        if (!Page.IsPostBack)
        {
            Excursion.DataSource = LoadExcursions();
            Excursion.DataBind();

            TransportType.DataSource = this.LoadTransportTypes();
            TransportType.DataBind();

            Status.DataSource = this.LoadExcursionStatus();
            Status.DataBind();

            ExcursionTime.DataSource = GetExcursionTimeList();
            ExcursionTime.DataBind();

            MeetTime.DataSource = GetExcursionTimeList();
            MeetTime.DataBind();

            DisplayExcursion(LoadExcursion(CreateEntity,_bookingId));
        }
    }


    protected Dictionary<TimeSpan, string> GetExcursionTimeList()
    {
        Dictionary<TimeSpan, string> returnlist = new Dictionary<TimeSpan, string>();

        TimeSpan _span = new TimeSpan(0, 0, 0);
        TimeSpan _endtimespan = new TimeSpan(23, 30, 0);

        while (_span < _endtimespan)
        {
            _span = _span.Add(new TimeSpan(0, 15, 0));
            returnlist.Add(_span, _span.ToString("hh\\:mm"));
        }

        return returnlist;

    }

    protected void Click_SaveExcursion(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {

            Booking newbooking = LoadExcursion(entity, _bookingId);

            newbooking.Title = Title.Text;
            newbooking.ExcursionDate = DateTime.Parse(ExcursionDate.Text);
            newbooking.ExcursionTime = TimeSpan.Parse(ExcursionTime.SelectedItem.Value);
            newbooking.IsClosed = IsClosed.Checked;
            newbooking.MeetLocation = MeetLocation.Text;
            newbooking.MeetTime = TimeSpan.Parse(MeetTime.SelectedItem.Value);
            newbooking.QtyGuides = (!string.IsNullOrEmpty(QtyGuides.Text)) ? Convert.ToByte(QtyGuides.Text) : (Byte)0;
            newbooking.QtyLeaders = (!string.IsNullOrEmpty(QtyLeaders.Text)) ? Convert.ToByte(QtyLeaders.Text) : (Byte?)null;
            newbooking.QtyStudents = (!string.IsNullOrEmpty(QtyStudents.Text)) ? Convert.ToInt16(QtyStudents.Text) : (Int16?)null;
            newbooking.BookingRef = BookingRef.Text.Trim();

            newbooking.ExcursionReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Excursions", "ExcursionId", Convert.ToInt32(Excursion.SelectedItem.Value));
            newbooking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(Status.SelectedItem.Value));
            newbooking.Xlk_TransportTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_TransportType", "TransportTypeId", Convert.ToByte(TransportType.SelectedItem.Value));

            SaveExcursion(entity, newbooking);

        }
    }


  
    #endregion

    #region Private Methods   


    private Booking LoadExcursion(ExcursionEntities entity ,Int32 excursionId)
    {
        IQueryable<Booking> excursionQuery = from e in entity.Bookings.Include("Xlk_TransportType").Include("Xlk_Status").Include("Excursion")
                                             where e.BookingId == excursionId
                                             select e;
        if (excursionQuery.ToList().Count() > 0)
            return excursionQuery.ToList().First();

        return null;
    }

    private void DisplayExcursion(Booking excursion)
    {
        if (excursion != null)
        {
            _excursionId = excursion.Excursion.ExcursionId;

            curId.Text = excursion.BookingId.ToString();
            curTitle.Text = TrimText(excursion.Title.ToString(), 40);
            curDate.Text = excursion.ExcursionDate.ToString("dd MMM yyyy");
            curTime.Text = excursion.ExcursionTime.ToString();
            curTransportType.Text = excursion.Xlk_TransportType.Description;
            curMeetTime.Text = excursion.MeetTime.ToString();
            curMeetLocation.Text = excursion.MeetLocation.ToString();
            curConfirmedDate.Text = (excursion.ConfirmedDate.HasValue) ? excursion.ConfirmedDate.Value.ToString("dd MMM yyyy") : "Not Confirmed";
            curConfirmedBy.Text = excursion.ConfirmedBy;
            curVoucherId.Text = excursion.VoucherId.ToString();
            curQtyPaceGuides.Text = excursion.QtyGuides.ToString();
            curQtyLeaders.Text = (excursion.QtyLeaders.HasValue) ? excursion.QtyLeaders.Value.ToString():"0";
            curQtyStudents.Text = (excursion.QtyStudents.HasValue) ? excursion.QtyStudents.Value.ToString() : "0";
            curBookingRef.Text = excursion.BookingRef;

            _excursionDate = Convert.ToDateTime(excursion.ExcursionDate);

            Title.Text = excursion.Title;
            ExcursionDate.Text = excursion.ExcursionDate.ToString("dd/MM/yyyy");
            ExcursionTime.Items.FindByValue(excursion.ExcursionTime.ToString()).Selected = true;
            IsClosed.Checked = excursion.IsClosed;
            MeetLocation.Text = excursion.MeetLocation;
            MeetTime.Items.FindByValue(excursion.MeetTime.ToString()).Selected = true;
            QtyGuides.Text = excursion.QtyGuides.ToString();
            QtyLeaders.Text = excursion.QtyLeaders.ToString();
            QtyStudents.Text = excursion.QtyStudents.ToString();
            Excursion.Items.FindByValue(excursion.Excursion.ExcursionId.ToString()).Selected = true;
            Status.Items.FindByValue(excursion.Xlk_Status.StatusId.ToString()).Selected = true;
            TransportType.Items.FindByValue(excursion.Xlk_TransportType.TransportTypeId.ToString()).Selected = true;
            BookingRef.Text = excursion.BookingRef;
        }
    }

  private void SaveExcursion(ExcursionEntities entity ,Booking excursion)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayExcursion(LoadExcursion(entity, _bookingId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }

[WebMethod]
  public static string AssignStudentsByProgrammeType(System.Collections.Generic.Dictionary<string,object> newObject)
  {
      Lnk_Student_Booking newmember = null;
      List<int> _currentIds = null;
      Booking _booking = null;
      try
      {  
          int _bookingId;
        int _programmeTypeId;
        decimal _amountPaid;


        if (newObject != null && newObject.Count > 0)
        {
            _bookingId = Convert.ToInt32(newObject["BookingId"]);
            _programmeTypeId =  Convert.ToInt32(newObject["ProgrammeTypeId"]);
            _amountPaid = Convert.ToDecimal (newObject["ProgrammeTypeId"]);

            using (ExcursionEntities entity = new ExcursionEntities())
            {
                _booking = entity.Bookings.Include("Lnk_Student_Booking").Single(b => b.BookingId == _bookingId);
                _currentIds = _booking.Lnk_Student_Booking.Select(e => e.StudentId).ToList();
                using (Pace.DataAccess.Enrollment.EnrollmentsEntities eentity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
                {
                    IQueryable<Pace.DataAccess.Enrollment.Student> studentsQuery = (from s in eentity.Student where !s.GroupId.HasValue && !_currentIds.Contains(s.StudentId) && s.Enrollment.Any(e => e.Xlk_ProgrammeType.ProgrammeTypeId == _programmeTypeId) && s.ArrivalDate < _booking.ExcursionDate && s.DepartureDate > _booking.ExcursionDate select s);

                    foreach (Pace.DataAccess.Enrollment.Student item in studentsQuery.ToList())
                    {
                        newmember = new Lnk_Student_Booking();
                        newmember.BookingId = _booking.BookingId;
                        newmember.StudentId = item.StudentId;
                        newmember.AmountPaid = _amountPaid;

                        if (_amountPaid > 0 && !newmember.DatePaid.HasValue)
                            newmember.DatePaid = DateTime.Now;
                        entity.AddToLnk_Student_Booking(newmember);
                        
                    }
                    entity.SaveChanges();
                }

            }

            return string.Empty;
        }
        else

            return "We could not process this request";
      }
      catch (Exception ex)
      {
          return ex.Message;
      }
}

[WebMethod]
[ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
public static string LoadBookingDocs(int id)
{
    try
    {
        ExcursionEntities preEx = new ExcursionEntities();
        var preQuery = (from p in preEx.Bookings.Include("Excursion") where p.BookingId == id select p).FirstOrDefault();
        Int32 exId = preQuery.Excursion.ExcursionId;
        StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\"onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddExcursionDocumentControl.ascx','ExcursionId',{1})\"><img src=\"../Content/img/actions/add.png\" />Add a Document</a></span><table id=\"box-table-a\" class\"tablesorter\"><thead><tr><th scope=\"col\">Doc Id</th><th scope=\"col\">Title</th><th scope=\"col\">Description</th><th scope=\"col\">File Type</th><th scope=\"col\">Doc Type</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, exId));

        using (ExcursionEntities entity = new ExcursionEntities())
        {
            var bookQuery = (from b in entity.Bookings.Include("Excursion") where b.BookingId == id select b).FirstOrDefault();
            List<ExcursionDocument> docQuery = (from d in entity.ExcursionDocuments where d.ExcursionId == bookQuery.Excursion.ExcursionId select d).ToList();
            List<Int32> docids = docQuery.Select(s => s.DocId).ToList();

            using (Pace.DataAccess.Documents.DocumentsEntities dentity = new Pace.DataAccess.Documents.DocumentsEntities())
            {
                List<Pace.DataAccess.Documents.Documents> dQuery = (from d in dentity.Documents.Include("Xlk_FileType").Include("Xlk_Type")
                                                                    where docids.Contains(d.DocId)
                                                                    orderby d.FileName ascending
                                                                    select d).ToList();
                if (dQuery.Count() > 0)
                {
                    var dcounter = 1;
                    foreach (var document in dQuery)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align:left\">{7}</td><td style=\"text-align:left\">{1}</td><td style=\"text-align:left\">{2}</td><td style=\"text-align:left\">{3}</td><td style=\"text-align:left\">{4}</td><td style=\"text-align:left\"><a title=\"Click to open Instruction sheet\" href=\"file://pace-server01/Academic/Excursions and Activities/Excursion Instructions/{5}.{6}\"><img src=\"../Content/img/actions/map_magnify.png\"></a></td>", id, document.Title, document.Description, document.Xlk_FileType.Description, document.Xlk_Type.Description, document.FileName, document.Xlk_FileType.Extension, document.DocId);
                        dcounter++;
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"6\">No Documents to Show!</td></tr>");
                }
            }
        }

        sb.Append("</tbody></table>");

        return sb.ToString();
    }
    catch (Exception ex)
    {
        return ex.Message;
    }
}

[WebMethod]
[ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
public static string LoadBookingGuides(int id)
{
    try
    {
        StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddBookingGuidesControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Add a Guide</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Guide No.</th><th scope=\"col\">Guide Name</th><th scope=\"col\">Confirmed Y/N</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));

        using (ExcursionEntities entity = new ExcursionEntities())
        {
            List<BookingStaff> staffQuery = (from s in entity.BookingStaffs where s.BookingId == id select s).ToList();
            List<Int32> staffids = staffQuery.Select(s => s.ContactId).ToList();
            
            using (Pace.DataAccess.Contacts.ContactsEntities centity = new Pace.DataAccess.Contacts.ContactsEntities())
            {
                List<Pace.DataAccess.Contacts.ContactDetails> query = (from c in centity.ContactDetails
                                                                       where staffids.Contains(c.ContactId)
                                                                       orderby c.FirstName ascending
                                                                       select c).ToList();

                if (query.Count() > 0)
                {
                    var counter = 1;
                    foreach (var contact in query)
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: center\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddBookingGuidesControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/edit.png\" /></a>&nbsp;<a title=\"Confirm Guide\" href=\"#\" onclick=\"confirmBookingStaff({0},'conmfirmbooked')\"><img src=\"../Content/img/actions/accept.png\"></a>&nbsp;<a title=\"Remove Guide from Excursion\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewBookingPage.aspx','BookingStaff',{0},{4})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", id, BasePage.CreateName(contact.FirstName, contact.SurName), true, counter, contact.ContactId);
                        counter = counter + 1;
                    }
                }

                else
                {
                    sb.Append("<tr><td colspan=\"4\">No Guides to Show!</td></tr>");
                }
            }
        }

        sb.Append("</tbody></table>");

        return sb.ToString();
    }

    catch (Exception ex)
    {
        return ex.Message;
    }
}

  [WebMethod]
  [ScriptMethod(UseHttpGet=true,ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookingStudents(int id)
  {
      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddProgrammeTypeStudentsToBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Signup from Programme</a></span><span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddStudentToBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Sign up a Student</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">No.</th><th scope=\"col\">Type</th><th scope=\"col\">Name</th><th scope=\"col\">Gender</th><th scope=\"col\">Age</th><th scope=\"col\">DOB</th><th scope=\"col\">Amount Paid</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
          
          using (ExcursionEntities entity = new ExcursionEntities())
          {

              List<Lnk_Student_Booking> studentsQuery = (from s in entity.Lnk_Student_Booking where s.BookingId == id select s).ToList();
              List<Int32> studentids = studentsQuery.Select(s => s.StudentId).ToList();

              using (Pace.DataAccess.Enrollment.EnrollmentsEntities eentity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
              {
                  List<Pace.DataAccess.Enrollment.Student> query = (from n in eentity.Student.Include("Xlk_Gender").Include("Xlk_StudentType")
                                                                where studentids.Contains(n.StudentId)
                                                               orderby n.Xlk_StudentType.Description, n.FirstName, n.SurName
                                                               select n).ToList();

                  if (query.Count() > 0)
                  {
                      var counter = 1;
                      foreach (var student in (from s in studentsQuery join st in query on s.StudentId equals st.StudentId select new { Student=st,Booking=s }).ToList())
                      {
                          sb.AppendFormat("<tr><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{9}</td><td style=\"text-align: center\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddStudentToBookingControl.ascx','StudentBookingId',{8})\"><img src=\"../Content/img/actions/edit.png\" /></a>&nbsp;<a title=\"Remove Student from Excursion\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewBookingPage.aspx','ExcursionStudentLink',{8})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", BasePage.StudentLink(student.Student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(student.Student.FirstName, student.Student.SurName)), student.Student.Xlk_Gender.Description, BasePage.CalculateAge(student.Student.DateOfBirth), (student.Student.DateOfBirth.HasValue) ? student.Student.DateOfBirth.Value.ToString("D") : string.Empty, student.Student.StudentId, student.Student.Xlk_StudentType.Description, student.Student.StudentId, id, student.Booking.StudentBookingId, student.Booking.AmountPaid);
                          counter = counter + 1;
                      }
                  }
                  else
                  {
                      sb.Append("<tr><td colspan=\"8\">No Students to Show!</td></tr>");
                  }
              }
          }
          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }

  

  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookingClosedGroups(int id)
  {
      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddGroupToBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Sign up a Group</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">No.</th><th scope=\"col\">Venue</th><th scope=\"col\">Date</th><th scope=\"col\">Time</th><th scope=\"col\">Students/Leaders/Guides</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));

          using (ExcursionEntities entity = new ExcursionEntities())
          {

              List<Lnk_GroupExcursion_Booking> groupsQuery = (from s in entity.Lnk_GroupExcursion_Booking where s.BookingId == id select s).ToList();
              List<Int32> ids = groupsQuery.Select(s => s.GroupExcursionId).ToList();

              using (Pace.DataAccess.Group.GroupEntities eentity = new Pace.DataAccess.Group.GroupEntities())
              {
                  var query = from n in eentity.GroupExcursion.Include("Excursion")
                              where ids.Contains(n.GroupExcursionId)
                              orderby n.Group.GroupName
                              select n;

                  if (query.Count() > 0)
                  {
                      foreach (Pace.DataAccess.Group.GroupExcursion group in query.ToList())
                      {
                          sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4} / {5} / {6}</td><td style=\"text-align: left; font-size:large\"><a href=\"#\" class=\"information\">{7}<span>{8}</span></a></td><td style=\"text-align: center\"><a title=\"Delete Excursion For Group\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewBookingPage.aspx','ClosedGroupExcursionBooking', {0},{9})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", group.GroupExcursionId, group.Excursion.Title, group.PreferredDate.ToString("D"), group.PreferredTime, group.QtyStudents, group.QtyLeaders, group.QtyGuides, TrimText(group.Comment, 20), group.Comment, id);
                      }
                  }
                  else
                  {
                      sb.Append("<tr><td colspan=\"9\">No Groups to Show!</td></tr>");
                  }
              }
          }
          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }

  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookingOpenGroup(int id)
  {
      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddGroupToOpenBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Sign up a Group</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Group Name</th><th scope=\"col\">Qty Leaders/Students/Guides</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));

          using (ExcursionEntities entity = new ExcursionEntities())
          {

              List<Lnk_Group_Booking> groupsQuery = (from s in entity.Lnk_Group_Booking where s.BookingId == id select s).ToList();
              List<Int32> ids = groupsQuery.Select(s => s.GroupId).ToList();

              using (Pace.DataAccess.Group.GroupEntities eentity = new Pace.DataAccess.Group.GroupEntities())
              {
                  var query =(from n in eentity.Group
                              where ids.Contains(n.GroupId)
                              orderby n.GroupName
                              select n).ToList();

                  var result = from g in query
                               join l in groupsQuery on g.GroupId equals l.GroupId
                               select new { Group = g, Link = l };

                  if (query.Count() > 0)
                  {
                      foreach (var group in result.ToList())
                      {
                          sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2} / {3} / {4}</td><td style=\"text-align: center\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewBookingPage.aspx','Excursions/AddGroupToOpenBookingControl.ascx',{{ BookingId: {5}, GroupId: {0}}} )\"><img src=\"../Content/img/actions/edit.png\" /></a>&nbsp;<a title=\"Delete Excursion For Group\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewBookingPage.aspx','OpenGroupExcursionBooking', {0},{5})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", group.Group.GroupId, group.Group.GroupName, group.Link.QtyStudents, group.Link.QtyLeaders, group.Link.QtyGuides, id);
                      }
                  }
                  else
                  {
                      sb.Append("<tr><td colspan=\"7\">No Groups to Show!</td></tr>");
                  }
              }
          }
          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }


  [WebMethod]
  public static string GetGroup(int groupid)
  {
      try
      {
          using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
          {

              var data = (from g in entity.Group
                          where g.GroupId == groupid
                          select new { g.NoOfLeaders, g.NoOfStudents }).FirstOrDefault();

              return new JavaScriptSerializer().Serialize(data);
          }
      }
      catch (Exception)
      {
          return null;
      }
  }

  

    #endregion
}
