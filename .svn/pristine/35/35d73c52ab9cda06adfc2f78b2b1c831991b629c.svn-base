﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using PaceManager.Group;
using System.Web.UI;

public partial class ManageGroupPage : BaseGroupPage
{
    protected Int32? _groupId;
    protected string _errorMessage = "All form fields are required";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["GroupId"]))
            _groupId = Convert.ToInt32(Request["GroupId"]);

        if (!Page.IsPostBack)
        {
            newnationality.DataSource = this.LoadNationalities();
            newnationality.DataBind();
            newagent.DataSource = this.LoadAgents();
            newagent.DataBind();
            newstatus.DataSource = this.LoadStatus();
            newstatus.DataBind();

            if (_groupId.HasValue)
            {
                DisplayGroup(LoadGroup(_groupId.Value));
            }
        }
    }

    #region Methods

    protected void Click_ClearFields(object sender, EventArgs e)// Clear all fields
    {
        GroupName.Text = ""; newnationality.SelectedIndex = 0; newarrdate.Text = "";
        newdepdate.Text = ""; newagent.SelectedIndex = 0; newstatus.SelectedIndex = 0;
        chkiscolsed.Checked = false; errorcontainer.Visible = false;
        newgroupleaders.Text = ""; newgroupstudents.Text = ""; arrivalinfo.InnerText = ""; departureinfo.InnerText = "";
    }

    protected void Click_Save(object sender, EventArgs e)// Save the record
    {
        string _message = ""; errorcontainer.Visible = false;

        if (!string.IsNullOrEmpty(GroupName.Text) && !string.IsNullOrEmpty(newarrdate.Text) && !string.IsNullOrEmpty(newdepdate.Text)
            && newnationality.SelectedIndex != 0 && newagent.SelectedIndex != 0 && newstatus.SelectedIndex != 0)
        {
            Group _group = GetGroup();

            _group.GroupName = GroupName.Text;
            _group.Xlk_NationalityReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
            _group.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
            _group.DepartureDate = Convert.ToDateTime(newdepdate.Text);
            _group.AgencyReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
            _group.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));
            _group.IsClosed = chkiscolsed.Checked;
            _group.LeaderMobile = leadersmobile.Text;
            _group.ArrivalInfo = arrivalinfo.InnerText;
            _group.DepartureInfo = departureinfo.InnerText;
            if (chkiscolsed.Checked == true)
                _group.NoOfClasses = Convert.ToInt16(newgroupclasses.Text);
            _group.NoOfStudents = Convert.ToInt16(newgroupstudents.Text);
            _group.NoOfLeaders = Convert.ToInt16(newgroupleaders.Text);

            SaveGroup(_group);
        }
        else
        {
            _message = "Not all fields are complete. Please check.";
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }
    }

    private Group LoadGroup(Int32 GroupId)// Load Group if passed from GroupPage
    {
        IQueryable<Group> groupQuery = from h in Entities.Group.Include("Xlk_Nationality")
                                       where h.GroupId == GroupId
                                       select h;
        if (groupQuery.ToList().Count() > 0)
            return groupQuery.ToList().First();

        return null;
    }

    private void DisplayGroup(Group group)// Display loaded Group details
    {
        if (group != null)
        {
            GroupName.Text = group.GroupName;
            newnationality.Items.FindByValue(group.Xlk_Nationality.NationalityId.ToString()).Selected = true;
            newarrdate.Text = group.ArrivalDate.ToString("dd/MM/yyyy");
            newdepdate.Text = group.DepartureDate.ToString("dd/MM/yyyy");
            newagent.Items.FindByValue(group.Agency.AgencyId.ToString()).Selected = true;
            newstatus.Items.FindByValue(group.Xlk_Status.StatusId.ToString()).Selected = true;
            chkiscolsed.Checked = group.IsClosed;
            leadersmobile.Text = group.LeaderMobile;
            arrivalinfo.InnerText = group.ArrivalInfo;
            departureinfo.InnerText = group.DepartureInfo;
        }
    }

    private Group GetGroup()// Load Group or ceate new Group
    {
        if (_groupId.HasValue)
            return LoadGroup(_groupId.Value);

        return new Group();
    }

    private void SaveGroup(Group group)// Final Save stage & Redirect
    {
        if (!_groupId.HasValue)
            Entities.AddToGroup(group);

        if (Entities.SaveChanges() > 0)
            Response.Redirect(string.Format("ViewGroupPage.aspx?GroupId=" + group.GroupId));
    }

    protected void Text_CheckGroupName(object sender, EventArgs e)
    {
        var lookupQuery = from t in Entities.Group
                          select t;

        foreach (var g in lookupQuery)
        {
            if (g.GroupName == GroupName.Text)
            {
                Alert.Show("A group with this name already exists. Are you sure you want to create another group with this name?");

            }
        }
    }

    public static class Alert
    {
        public static void Show(string message)
        {
            string cleanMessage = message.Replace("'", "\\'");
            string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";

            Page page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", script);
            }
        }
    }
    
    #endregion

}
