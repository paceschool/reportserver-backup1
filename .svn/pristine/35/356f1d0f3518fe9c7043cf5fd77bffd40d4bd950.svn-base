﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Tuition;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for BaseTuitionPage
/// </summary>
public partial class BaseTuitionPage : BasePage
{
    public TuitionEntities _entities { get; set; }
    
    public BaseTuitionPage()
	{
    }

    public static IList<Xlk_Building> LoadBuildings()
    {
        List<Xlk_Building> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Building>>("AllBuilding", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Building>>("AllBuilding", GetBuildings().ToList());
    }

    private static IQueryable<Xlk_Building> GetBuildings()
    {
        IQueryable<Xlk_Building> lookupQuery =
            from c in Entities.Xlk_Building
            select c;
        return lookupQuery;
    }

    public IList<ExamDates> LoadExamDates()
    {
        List<ExamDates> values = null;

        if (CacheManager.Instance.GetFromCache<List<ExamDates>>("AllExamDates", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<ExamDates>>("AllExamDates", GetExamDates().ToList());
    }

    public IList<Exam> LoadExams()
    {
        List<Exam> values = null;

        if (CacheManager.Instance.GetFromCache<List<Exam>>("AllEnrollmentExmas", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Exam>>("AllEnrollmentExams", GetExams().ToList());
    }

    private IQueryable<Exam> GetExams()
    {
        IQueryable<Exam> lookupQuery =
            from ex in Entities.Exam
            select ex;
        return lookupQuery;
    }
    private IQueryable<ExamDates> GetExamDates()
    {
        IQueryable<ExamDates> lookupQuery =
            from ed in Entities.ExamDates
            orderby ed.ExamDate ascending
            select ed;
        return lookupQuery;
    }

    #region Events

    public static TuitionEntities Entities
    {
        get
        {
            TuitionEntities value;

            if (CacheManager.Instance.GetFromCache<TuitionEntities>("ClassObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<TuitionEntities>("ClassObject", new TuitionEntities());
        }
    }

    public IList<TuitionLevel> LoadTuitionLevels()
    {
        List<TuitionLevel> values = null;

        if (CacheManager.Instance.GetFromCache<List<TuitionLevel>>("AllTuitionLevel", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<TuitionLevel>>("AllTuitionLevel", GetTuitionLevels().ToList());
    }

    private IQueryable<TuitionLevel> GetTuitionLevels()
    {
        IQueryable<TuitionLevel> lookupQuery =
            from l in Entities.TuitionLevel
            select l;
        return lookupQuery;
    }

    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllTuitionProgrammeType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllTuitionProgrammeType", GetProgrammeTypes().ToList());
    }
    private IQueryable<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        IQueryable<Xlk_ProgrammeType> lookupQuery =
            from p in Entities.Xlk_ProgrammeType
            select p;
        return lookupQuery;
    }

    public IList<Teacher> LoadTeachers()
    {
        List<Teacher> values = null;

        if (CacheManager.Instance.GetFromCache<List<Teacher>>("AllTeacher", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Teacher>>("AllTeacher", GetTeachers().ToList());
    }

    private IQueryable<Teacher> GetTeachers()
    {
        IQueryable<Teacher> lookupQuery =
            from t in Entities.Teacher
            orderby t.Name
            select t;
        return lookupQuery;
    }

    public IList<ClassRoom> LoadClassRooms()
    {
        List<ClassRoom> values = null;

        if (CacheManager.Instance.GetFromCache<List<ClassRoom>>("AllClassRoom", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<ClassRoom>>("AllClassRoom", GetClassRooms().ToList());
    }

    private IQueryable<ClassRoom> GetClassRooms()
    {
        IQueryable<ClassRoom> lookupQuery =
            from c in Entities.ClassRoom
            select c;
        return lookupQuery;
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, int> entitykeys)
    {
        try
        {
            var page = new BaseTuitionPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseEnrollmentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("ClassId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    #endregion

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadStudents(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewExamsPage.aspx','Tuition/AddExamEnrollmentControl.ascx','ExamDateId', {0})\"><img src=\"../Content/img/actions/add.png\" />Add New Student</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Student Exam Id</th><th scope=\"col\">Student Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">Date Registered</th><th scope=\"col\">Paid</th><th scope=\"col\">Amount</th><th scope=\"col\">Date Paid</th><th scope=\"col\">As Individual</th><th scope=\"col\">Result Score</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from n in Entities.StudentExam.Include("ExamDates").Include("Student")
                        where n.ExamDates.ExamDateId == id && n.Student.ArrivalDate < DateTime.Now && n.Student.DepartureDate > DateTime.Now
                        select n;

            if (query.Count() > 0)
            {
                foreach (StudentExam exam in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\"><a href=\"../Enrollments/ViewStudentPage.aspx?StudentId={1}\">{2}</a></td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: left\">{8}</td><td style=\"text-align: left\"><a title=\"Edit Exam Enrollment\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewExamsPage.aspx','Tuition/AddExamEnrollmentControl.ascx','StudentExamId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Exam Enrollment\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewExamsPage.aspx','StudentExam',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", exam.StudentExamId, exam.Student.StudentId, CreateName(exam.Student.FirstName, exam.Student.SurName), (exam.DateRegistered.HasValue) ? exam.DateRegistered.Value.ToString("D") : String.Empty, (exam.Paid.Value == true) ? "<b>Yes</b>" : String.Empty, exam.Amount.HasValue ? exam.Amount.Value.ToString("€0.00") : "", (exam.DatePaid.HasValue) ? exam.DatePaid.Value.ToString("D") : String.Empty, (exam.AsIndividual.Value == true) ? "<b>Yes</b>" : String.Empty, exam.ResultScore);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"9\">No Students Found for this Exam!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadPreviousStudents(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Student Exam Id</th><th scope=\"col\">Student Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">As Individual</th><th scope=\"col\">Result</th></tr></thead><tbody>", id, BasePage.ReportServerURL));

            var query = from n in Entities.StudentExam.Include("ExamDates").Include("Student")
                        where n.ExamDates.ExamDateId == id && n.Student.DepartureDate < DateTime.Now
                        select n;

            if (query.Count() > 0)
            {
                foreach (StudentExam exam in query.ToList())
                {
                    sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td></tr>", exam.StudentExamId, exam.Student.StudentId, CreateName(exam.Student.FirstName, exam.Student.SurName), (exam.AsIndividual.Value == true) ? "<b>Yes</b>" : String.Empty, exam.ResultScore);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"5\">No Students Found for this Exam!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod]
    public static string SaveExamEnrollment(StudentExam newObject)
    {
        StudentExam newexam = new StudentExam();
        try
        {

            if (newObject.StudentExamId > 0)
                newexam = (from e in Entities.StudentExam where e.StudentExamId == newObject.StudentExamId select e).FirstOrDefault();


            newexam.StudentReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Student", newObject.Student);
            newexam.ExamDatesReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".ExamDates", newObject.ExamDates);
            if (newObject.DateRegistered.HasValue)
                newexam.DateRegistered = Convert.ToDateTime(newObject.DateRegistered.Value);
            newexam.Paid = newObject.Paid.Value;
            if (newObject.Amount.HasValue)
                newexam.Amount = Convert.ToDecimal(newObject.Amount.Value);
            newexam.AsIndividual = newObject.AsIndividual.Value;
            newexam.ResultScore = newObject.ResultScore;

            if (newexam.StudentExamId == 0)
                Entities.AddToStudentExam(newexam);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
