﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Enrollment;
using PaceManager.Accommodation;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;

public partial class Enrollments_AssignHostingPage : BaseEnrollmentPage
{
    protected Int32? _studentId;
    protected string _errorMessage = "All form fields are required.";


    protected void Page_Load(object sender, EventArgs e)
    {

       _studentId = Convert.ToInt32(Request["StudentId"]);

       if (!Page.IsPostBack)
       {
           if (_studentId != 0)
           {
               IQueryable<Student> tuQuery = from f in Entities.Student
                                             where f.StudentId == _studentId
                                             select f;

               LoadStudentDetails(tuQuery.ToList().First());

               LoadHostings();
               LoadFamilies(GetFamilies(string.Empty));
           }
       }
    }

    protected void DeleteHosting_Click(object sender, EventArgs e)
    {
        object todelete = null;

        if (!string.IsNullOrEmpty(((ImageButton)sender).ToolTip))
        {
            Int32 _hostingId = Convert.ToInt32(((ImageButton)sender).ToolTip);
            if (Entities.TryGetObjectByKey(new EntityKey(Entities.DefaultContainerName + ".Hosting", "HostingId", _hostingId), out todelete))
            {
                Entities.DeleteObject(todelete);
                if (Entities.SaveChanges() > 0)
                {
                    LoadHostings();
                }
            }
        }
    }

    #region Private Methods

    private void LoadFamilies(List<PaceManager.Accommodation.Family> families)
    {
        var familyQuery = families.Select(f => new { FamilyId = f.FamilyId, Name = f.FirstName.ToString() + " " + f.SurName.ToString() });
        family.DataSource = familyQuery;
        family.DataBind();
    }

    private List<PaceManager.Accommodation.Family> GetFamilies(string filter)
    {

        IQueryable<PaceManager.Accommodation.Family> familyQuery = from f in BaseAccommodationPage.Entities.Family
                                                                   select f;
        if (!string.IsNullOrEmpty(filter))
            familyQuery = familyQuery.Where(f => f.FirstName.StartsWith(filter) || f.SurName.StartsWith(filter));

        return familyQuery.ToList();
    }

    private void LoadHostings()
    {
        if (_studentId.HasValue)
        {
            var hostingQuery = from h in BaseAccommodationPage.Entities.Hosting
                               join f in BaseAccommodationPage.Entities.Family on h.Family.FamilyId equals f.FamilyId
                               where h.StudentId == _studentId.Value
                               select new { h.StudentId, h.ArrivalDate, h.DepartureDate, Name = f.FirstName + " " + f.SurName };

            results.DataSource = hostingQuery.ToList();
            results.DataBind();
        }
    }

    private void LoadStudentDetails(Student student)
    {
        StudentId.Text = student.StudentId.ToString();
        Name.Text = String.Format("{0} {1}", student.FirstName, student.SurName);
        CourseStart.Text = student.ArrivalDate.Value.ToString("d");
        CourseFinish.Text = student.DepartureDate.Value.ToString("d");
    }

    protected void addhosting_Click(object sender, EventArgs e)
    {

        if (_studentId.HasValue && !string.IsNullOrEmpty(txtcstart.Value) && !string.IsNullOrEmpty(txtcfinish.Value))
        {
            if (CheckNoConflict(DateTime.Parse(txtcstart.Value), DateTime.Parse(txtcfinish.Value)))
            {
                Hosting hosting = new Hosting();
                hosting.ArrivalDate = DateTime.Parse(txtcstart.Value);
                hosting.DepartureDate = DateTime.Parse(txtcfinish.Value);
                hosting.StudentId = _studentId.Value;
                hosting.FamilyReference.EntityKey = new System.Data.EntityKey(BaseAccommodationPage.Entities.DefaultContainerName + ".Family", "FamilyId", Convert.ToInt32(family.SelectedItem.Value));
                BaseAccommodationPage.Entities.AddToHosting(hosting);

                if (BaseAccommodationPage.Entities.SaveChanges() > 0)
                {
                    LoadHostings();
                }
            }
            else
                _errorMessage = "A Date conflict has occured please chech dates.";
        }
        else
            _errorMessage = "All fields must be completed.";
    }
    private bool CheckNoConflict(DateTime startDate, DateTime endDate)
    {
        if (startDate > endDate)
        {
            _errorMessage = "Start Date after End Date";
            return false;
        }
        var chechQuery = from h in BaseAccommodationPage.Entities.Hosting
                         where h.StudentId == _studentId.Value && ((h.ArrivalDate <= startDate && h.DepartureDate >= startDate) || ((h.ArrivalDate <= endDate && h.DepartureDate >= endDate)))
                         select h;

        return chechQuery.Count() == 0;
    }

    #endregion
}
