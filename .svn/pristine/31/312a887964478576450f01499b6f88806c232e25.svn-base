﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Tuition;
using System.Data;

public partial class Tuition_ManageClassPage : BaseTuitionPage
{
    private Int32? _classId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            newclasslevel.DataSource = this.LoadTuitionLevels();
            newclasslevel.DataBind();
            newclassteacher.DataSource = this.LoadTeachers();
            newclassteacher.DataBind();
            newclassroom.DataSource = this.LoadClassRooms();
            newclassroom.DataBind();
            newclassprogramme.DataSource = this.LoadProgrammeTypes();
            newclassprogramme.DataBind();
            newclasscoursetype.DataSource = BaseEnrollmentControl.LoadCourseTypes();
            newclasscoursetype.DataBind();
        }
    }
    protected void Click_SaveClass(object sender, EventArgs e)
    {
        string _message = ""; errorcontainer.Visible = false;
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (newclasslevel.SelectedIndex > 0 && newclassteacher.SelectedIndex > 0 && newclassroom.SelectedIndex > 0
                && newclassprogramme.SelectedIndex > 0 && !string.IsNullOrEmpty(newstartdate.Text) && !string.IsNullOrEmpty(newenddate.Text)
               )
            {
                Class _newclass = GetClass(entity);

                _newclass.Label = newclasslevel.SelectedValue.ToString();
                _newclass.TeacherReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Teacher", "TeacherId", Convert.ToInt16(newclassteacher.SelectedItem.Value));
                _newclass.ClassRoomReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ClassRoom", "ClassroomId", Convert.ToInt16(newclassroom.SelectedItem.Value));
                _newclass.TuitionLevelReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".TuitionLevel", "TuitionLevelId", Convert.ToByte(newclasslevel.SelectedItem.Value));
                _newclass.Xlk_ProgrammeTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_ProgrammeType", "ProgrammeTypeId", Convert.ToByte(newclassprogramme.SelectedItem.Value));
                _newclass.Xlk_CourseTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_CourseType", "CourseTypeId", Convert.ToInt16(newclasscoursetype.SelectedItem.Value));
                _newclass.StartDate = Convert.ToDateTime(newstartdate.Text);
                _newclass.EndDate = Convert.ToDateTime(newenddate.Text);
                _newclass.DailyStartTime = TimeSpan.Parse(newstarttime.Text);
                _newclass.DailyEndTime = TimeSpan.Parse(newendtime.Text);
                _newclass.IsClosed = newclosedclass.Checked;
                SaveClass(entity,_newclass);
            }
            else
            {
                _message = "Not all fields are complete. Please check.";
                errorcontainer.Visible = true;
                errormessage.Text = _message;
            }
        }
    }

    private Class GetClass(TuitionEntities entity)
    {
        if (_classId.HasValue)
            return LoadClass(entity,_classId.Value);

        return new Class();
    }

    private Class LoadClass(TuitionEntities entity,Int32 classId)
    {
        IQueryable<Class> classQuery = from c in entity.Class
                                       where c.ClassId == classId
                                       select c;

        if (classQuery.ToList().Count() > 0)
            return classQuery.ToList().First();

        return null;
    }

    private void DisplayClass(Class classinstance)
    {
        if (classinstance != null)
        {
            newclasslevel.Items.FindByValue(classinstance.TuitionLevel.TuitionLevelId.ToString()).Selected = true;
            newclassteacher.Items.FindByValue(classinstance.Teacher.TeacherId.ToString()).Selected = true;
            newclassroom.Items.FindByValue(classinstance.ClassRoom.ClassroomId.ToString()).Selected = true;
            newclassprogramme.Items.FindByValue(classinstance.Xlk_ProgrammeType.ProgrammeTypeId.ToString()).Selected = true;
            newclasscoursetype.Items.FindByValue(classinstance.Xlk_CourseType.CourseTypeId.ToString()).Selected = true;
            newstartdate.Text = classinstance.StartDate.ToString();
            newenddate.Text = classinstance.EndDate.ToString();
            newstarttime.Text = classinstance.DailyStartTime.ToString();
            newendtime.Text = classinstance.DailyEndTime.ToString();
        }
    }

    private void SaveClass(TuitionEntities entity,Class classinstance)
    {
        if (!_classId.HasValue)
            entity.AddToClass(classinstance);


        if (entity.SaveChanges() > 0)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }

}
