﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using PaceManager.Security;
using System.Data.Objects;
using PaceManager.Excursion;

public partial class Excursions_AddBookingControl : BaseExcursionControl
{
    private Int32? _bookingId;
    private Int32? _excursionId;
    private Excursion _excursion;
    private Booking _booking;
    private PaceManager.Group.GroupExcursion _groupExcursion;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue("ExcursionId") != null)
            {
                _excursionId = Convert.ToInt32(GetValue("ExcursionId"));
                _excursion = (from excur in entity.Excursion
                              where excur.ExcursionId == _excursionId
                              select excur).FirstOrDefault();
            }
        }

        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (GetValue("BookingId") != null)
            {
                _bookingId = Convert.ToInt32(GetValue("BookingId"));
                _booking = (from excursion in entity.Booking.Include("Xlk_Status")
                            where excursion.BookingId == _bookingId
                            select excursion).FirstOrDefault();
            }
        }

        using (PaceManager.Group.GroupEntities gentity = new PaceManager.Group.GroupEntities())
        {
            if (GetValue("GroupExcursionId") != null)
            {
                int _groupExcursionId = Convert.ToInt32(GetValue("GroupExcursionId"));
                _groupExcursion = (from excursion in gentity.GroupExcursion.Include("Excursion").Include("Group")
                                   where excursion.GroupExcursionId == _groupExcursionId
                                   select excursion).FirstOrDefault();
            }
        }
    }

    protected string GetTitle
    {
        get
        {
            if (_booking != null)
                return _booking.Title;

            if (_groupExcursion != null)
                return string.Format("{0} :- {1}",_groupExcursion.Group.GroupName, _groupExcursion.Excursion.Title);

            if (_excursion != null)
                return string.Format("{0}", _excursion.Title);

            return "";
        }
    }

    protected string GetExcursionDate
    {
        get
        {
            if (_booking != null)
                return _booking.ExcursionDate.ToString("dd/MM/yy");

            if (_groupExcursion != null)
                return _groupExcursion.PreferredDate.ToString("dd/MM/yy");

            return "";
        }
    }

    protected string GetQtyLeaders
    {
        get
        {
            if (_booking != null)
                return _booking.QtyLeaders.ToString();

            if (_groupExcursion != null)
                return _groupExcursion.QtyLeaders.ToString();

            return "0";
        }
    }

    protected string GetQtyGuides
    {
        get
        {
            if (_booking != null)
                return _booking.QtyGuides.ToString();

            if (_groupExcursion != null)
                return _groupExcursion.QtyGuides.ToString();

            return "0";
        }
    }

    protected string GetQtyStudents
    {
        get
        {
            if (_booking != null)
                return _booking.QtyStudents.ToString();

            if (_groupExcursion != null)
                return _groupExcursion.QtyStudents.ToString();

            return "0";
        }
    }
    protected string GetMeetLocation
    {
        get
        {
            if (_booking != null)
                return _booking.MeetLocation;

            return "";
        }
    }

    protected string GetIsClosed
    {
        get
        {
            if (_booking != null)
                return (_booking.IsClosed) ? "checked" : "";

            if (_groupExcursion != null)
                return "checked";

            return "";
        }
    }

    protected string GetBookingId
    {
        get
        {
            if (_booking != null)
                return _booking.BookingId.ToString();

            if (GetValue("BookingId") != null)
                return GetValue("BookingId").ToString();

            return "0";
        }
    }

    protected string GetBookingRef
    {
        get
        {
            if (_booking != null)
                return _booking.BookingRef;

            return "";
        }
    }
    protected string GetGroupExcursionId
    {
        get
        {
            if (GetValue("GroupExcursionId") != null)
                return GetValue("GroupExcursionId").ToString();

            return "0";
        }
    }
    protected string GetExcursionList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_booking != null) ? (_booking != null && _booking.Excursion != null) ? _booking.Excursion.ExcursionId : (Int32?)null : (_groupExcursion != null && _groupExcursion.Excursion != null) ? _groupExcursion.Excursion.ExcursionId : (Int32?)null;

        if ((_booking != null && _booking.Excursion != null) || (_groupExcursion != null && _groupExcursion.Excursion != null) || _excursion != null)
        {
            if (_excursion != null)
                sb.AppendFormat("<option value={0}>{1}</option>", _excursion.ExcursionId, _excursion.Title);

            if (_groupExcursion != null && _groupExcursion.Excursion != null)
                sb.AppendFormat("<option value={0}>{1}</option>", _groupExcursion.Excursion.ExcursionId, _groupExcursion.Excursion.Title);

            if (_booking != null && _booking.Excursion != null) 
                sb.AppendFormat("<option value={0}>{1}</option>", _booking.Excursion.ExcursionId, _booking.Excursion.Title);

        }
        else
        {
            foreach (Excursion item in this.LoadExcursions().OrderBy(o=>o.Title))
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExcursionId, item.Title, (_current.HasValue && item.ExcursionId == _current) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetTransportTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_booking != null) ? (_booking != null && _booking.Xlk_TransportType != null) ? _booking.Xlk_TransportType.TransportTypeId : (Int32?)null : (_groupExcursion != null && _groupExcursion.TransportRequired.HasValue) ? 4 : 1;

        foreach (Xlk_TransportType item in this.GetTransportTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TransportTypeId, item.Description, (_current.HasValue && item.TransportTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    private IList<Xlk_TransportType> GetTransportTypes()
    {

        Int32? _current = (_groupExcursion != null) ? _groupExcursion.Excursion.ExcursionId : (_excursion != null) ? _excursion.ExcursionId : (_booking != null && _booking.Excursion != null) ? _booking.Excursion.ExcursionId : (Int32?)null;


        using (ExcursionEntities entity = new ExcursionEntities())
        {
            IQueryable<Xlk_TransportType> lookupQuery = (from p in entity.Xlk_TransportType
                                                    select p);

            if (_current.HasValue && entity.ExcursionTransportTypes.Any(x=>x.ExcursionId == _current.Value && x.Excursion.TransportRequired))
                lookupQuery = (from e in entity.ExcursionTransportTypes
                               where e.ExcursionId == _current.Value
                               select e.Xlk_TransportType);


            return lookupQuery.ToList();
        }
    }


    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_booking != null && _booking.Xlk_Status != null) ? _booking.Xlk_Status.StatusId : (Int32?)null;

        foreach (Xlk_Status item in this.LoadStatus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetExcursionTimeList()
    {
        StringBuilder sb = new StringBuilder();

        TimeSpan? _current = (_booking != null) ? (_booking != null && _booking.ExcursionTime != null) ? _booking.ExcursionTime : (TimeSpan?)null : (_groupExcursion != null && _groupExcursion.PreferredTime.HasValue) ? _groupExcursion.PreferredTime.Value : (TimeSpan?)null;

        TimeSpan _span = new TimeSpan(0, 0, 0);
        TimeSpan _endtimespan = new TimeSpan(23, 30, 0);

        while (_span < _endtimespan)
        {
            _span = _span.Add(new TimeSpan(0, 15, 0));

            sb.AppendFormat("<option {1} value={0}>{0}</option>", _span.ToString("hh\\:mm"), (_current.HasValue && _span.Equals(_current)) ? "selected" : string.Empty);
        }

        return sb.ToString();

   }

    protected string GetMeetTimeList()
    {
        StringBuilder sb = new StringBuilder();

        TimeSpan? _current = (_booking != null) ? (_booking != null && _booking.ExcursionTime != null) ? _booking.ExcursionTime : (TimeSpan?)null : (_groupExcursion != null && _groupExcursion.PreferredTime.HasValue) ? _groupExcursion.PreferredTime.Value.Add(new TimeSpan(0,-90,0)) : (TimeSpan?)null;


        TimeSpan _span = new TimeSpan(0, 0, 0);
        TimeSpan _endtimespan = new TimeSpan(23, 30, 0);

        while (_span < _endtimespan)
        {
            _span = _span.Add(new TimeSpan(0, 30, 0));

            sb.AppendFormat("<option {1} value={0}>{0}</option>", _span.ToString("hh\\:mm"), (_current.HasValue && _span.Equals(_current)) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }



}