﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Documents;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;

public partial class AddDocumentPage : BaseDocumentPage
{
    private Int32? _documentId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["DocId"]))
        {
            _documentId = Convert.ToInt32(Request["DocId"]);
        }

        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            category.DataSource = this.LoadCategories();
            category.DataBind();

            if (_documentId.HasValue)
            {
                DisplayDocument(LoadDocument(_documentId.Value));
            }
        }
    }

    protected void Save_Click(object sender, EventArgs e)
    {
        string _message = String.Empty;
        string _path, _filename;
        Pace.DataAccess.Documents.Server _server;
        errorcontainer.Visible = false;

        if (!string.IsNullOrEmpty(filename.Value))
        {
            using (DocumentsEntities entity = new DocumentsEntities())
        {
            Documents _document = GetDocument();

            var InitialStatus = 1;
            _document.Description = description.Text;
            _document.Title = title.Text;
            _document.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(InitialStatus));

            _document.Xlk_FileType = GetFileType(filename.Value);

            if (_document.Xlk_FileTypeReference.EntityKey == null)
                _message = "Cannot determine file type please ask admin to add extension";

            _document.Xlk_TypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Type", "TypeId", Convert.ToByte(type.SelectedItem.Value));

            if (GetFilePathAndName(filename.Value, out _server, out _path, out _filename, out _message))
            {
                _document.Path = _path;
                _document.FileName = _filename;
                _document.Server = _server;

                SaveDocument(_document);
            }
            }
        }
        else
            _message = "You have not selected a file; please do so and try again";


        if (!string.IsNullOrEmpty(_message))
        {
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }

    }

    protected void category_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTypesToDropDown();

    }

    protected void category_DataBound(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }

    protected void type_DataBound(object sender, EventArgs e)
    {
        TypeChanged();
    }

    protected void type_SelectedIndexChanged(object sender, EventArgs e)
    {
        TypeChanged();
    }

    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            object todelete = null;

            if (!string.IsNullOrEmpty(((ImageButton)sender).ToolTip))
            {
                Int32 _docId = Convert.ToInt32(((ImageButton)sender).ToolTip);
                if (entity.TryGetObjectByKey(new EntityKey(entity.DefaultContainerName + ".Documents", "DocId", _docId), out todelete))
                {
                    entity.DeleteObject(todelete);
                    if (entity.SaveChanges() > 0)
                        TypeChanged();
                }

            }
        }
    }

    #region Private Methods

    public static Pace.DataAccess.Documents.Server GetServer(string serverName)
    {
        IQueryable<Pace.DataAccess.Documents.Server> x = (from f in LoadServers()
                                                      where f.UncPath.Contains(serverName)
                                                      select f).AsQueryable();
        if (x.Count() > 0)
            return x.First();

        return null;
    }

    private bool GetFilePathAndName(string fileFullfilePath, out Pace.DataAccess.Documents.Server server, out string path, out string fileName, out string message)
    {
        path = string.Empty;
        fileName = string.Empty;
        message = string.Empty;
        server = null;

        if (string.IsNullOrEmpty(fileFullfilePath))
            message = "No File path specified";


        string workPath = fileFullfilePath;
        string[] pathParts = workPath.Split('\\');

        if (pathParts.Count() <= 1)
            message = "Unable to interpret path";

        server = GetServer(pathParts[2]);

        if (server == null)
            message = "Unable to determine server";
        else
            workPath = fileFullfilePath.Replace(server.UncPath.ToUpper(), string.Empty);

        fileName = pathParts[pathParts.Length - 1];

        if (string.IsNullOrEmpty(fileName))
            message = "Unable to determine file name";
        else
            path = workPath.Replace(fileName, string.Empty);

        if (string.IsNullOrEmpty(path))
            message = "Unable to determine file path";

        return string.IsNullOrEmpty(message);

    }

    private Xlk_FileType GetFileType(string filename)
    {

        if (!string.IsNullOrEmpty(filename))
        {
            string extension = filename.Split('.')[1];

            var t = this.LoadFileType(extension);

            if (t != null && t.Count > 0)
                return t.First();
        }
        return null;
    }

    private EntityKey GetFileTypeEntityKey(string filename)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            if (!string.IsNullOrEmpty(filename))
            {
                string extension = filename.Split('.')[1];

                var t = this.LoadFileType(extension);

                if (t != null && t.Count > 0)
                    return new EntityKey(entity.DefaultContainerName + ".Xlk_FileType", "FileTypeId", Convert.ToByte(t.First().FileTypeId));
            }
            return null;
        }
    }

    private void LoadTypesToDropDown()
    {
        //Set datasource of the Type DropDown
        ListItem _category = category.SelectedItem;
        type.DataSource = this.LoadTypes(Convert.ToInt32(_category.Value));
        type.DataBind();
    }

    private void TypeChanged()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            if (type.SelectedItem != null)
            {
                Int32 _typeId = Convert.ToInt32(type.SelectedItem.Value);

                var documentSearchQuery = from d in entity.Documents.Include("Xlk_FileType").Include("Xlk_Type").Include("Server").Include("DocumentTag").Include("Xlk_Status")
                                          where d.Xlk_Type.TypeId == _typeId
                                          select d;

                LoadResults(documentSearchQuery.ToList());
            }
        }
    }

    private void LoadResults(List<Documents> documents)
    {
        //Set datasource of the repeater
        results.DataSource = documents;
        results.DataBind();
    }

    protected string CreateDocumentTagList(object DocumentTags)
    {
        System.Text.StringBuilder DocumentTagsString = new System.Text.StringBuilder();

        var s = from st in (System.Data.Objects.DataClasses.EntityCollection<DocumentTag>)DocumentTags
                select st.Tag;

        return String.Join("/", s.ToArray());
    }

    protected string CreateId(object id)
    {
        return String.Format("DocumentTagId{0}", id);
    }

    private Documents LoadDocument(Int32 documentId)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Documents> documentQuery = from d in entity.Documents.Include("Xlk_Type").Include("Server")
                                                  where d.DocId == documentId
                                                  select d;
            if (documentQuery.ToList().Count() > 0)
                return documentQuery.ToList().First();
        }

        return null;
    }

    private void DisplayDocument(Documents document)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            if (document != null)
            {
                //type.Items.FindByValue(document.Xlk_Type.TypeId.ToString()).Selected = true;
                title.Text = document.Title;
                description.Text = document.Description;
            }
        }
    }

    private Documents GetDocument()
    {
        if (_documentId.HasValue)
            return LoadDocument(_documentId.Value);

        return new Documents();
    }

    private void SaveDocument(Documents document)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            if (!_documentId.HasValue)
                entity.AddToDocuments(document);

            if (entity.SaveChanges() > 0)
                Response.Redirect(string.Format("DocumentsPage.aspx"));
        }
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    public static string AddDocumentDocumentTag(int id, string DocumentTag)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            if (!DocumentTagExists(id, DocumentTag))
            {
                DocumentTag newDocumentTag = new DocumentTag();
                newDocumentTag.DocumentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Documents", "DocId", id);
                newDocumentTag.DocId = id;
                newDocumentTag.TagId = NextDocumentTagId(id, 1);
                newDocumentTag.Tag = DocumentTag;

                entity.AddToDocumentTags(newDocumentTag);

                if (entity.SaveChanges() > 0)
                    return string.Empty;

                return "Could not add the new DocumentTag; there was a problem";

            }
            else
                return "Could not add the new DocumentTag; DocumentTag already exists";
        }
    }

    [WebMethod]
    public static string LoadDocumentTags(int id)
    {
        StringBuilder sb = new StringBuilder();

        using (DocumentsEntities entity = new DocumentsEntities())
        {

            IQueryable<DocumentTag> DocumentTagQuery = from d in entity.DocumentTags
                                       where d.DocId == id
                                       select d;

            foreach (DocumentTag DocumentTag in DocumentTagQuery.ToList())
            {
                sb.AppendFormat("<option>{0}</option>", DocumentTag.Tag);
            }
            return sb.ToString();
        }
    }

    [WebMethod]
    public static string DeleteDocumentTag(int id, string DocumentTag)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<DocumentTag> DocumentTagObject = from d in entity.DocumentTags
                                        where d.Tag == DocumentTag && d.DocId == id
                                        select d;

            if (DocumentTagObject.Count() > 0)
                entity.DeleteObject(DocumentTagObject.First());
            else
                return "Could not delete DocumentTag; DocumentTag not found";

            if (entity.SaveChanges() > 0)
                return string.Empty;

            return "Could not add the new DocumentTag; there was a problem";
        }
    }

    [WebMethod]
    public static string ToggleStatus(int id)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Documents> documentQuery = from d in entity.Documents.Include("Xlk_Status")
                                                  where d.DocId == id
                                                  select d;

            if (documentQuery.Count() > 0)
            {
                Documents doc = documentQuery.First();
                doc.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte((doc.Xlk_Status.StatusId == 1) ? 2 : 1));
            }
            else
                return "Could not toggle the status, document not found";

            if (entity.SaveChanges() > 0)
                return string.Empty;
        }

        return "Could not add the new DocumentTag; there was a problem";

    }

    private static bool DocumentTagExists(int id, string DocumentTag)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            var i = from d in entity.DocumentTags
                    where d.Tag == DocumentTag && d.DocId == id
                    select d.DocId;

            return (i.Count() > 0);
        }
    }

    private static byte NextDocumentTagId(int id, byte step)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<DocumentTag> DocumentTagQuery = from t in entity.DocumentTags
                                       where t.DocId == id
                                       select t;
            if (DocumentTagQuery.Count() > 0)
                return (byte)(DocumentTagQuery.Max(t => t.TagId) + step);

            return step;
        }
    }

    #endregion
}
