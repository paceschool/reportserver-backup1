﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Accommodation;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Text;

public partial class Accommodation_PaymentPage : BaseAccommodationPage
{
    protected string _action;

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

            filterstring.Focus();
        }
    }

    protected void lookupPayment(object sender, EventArgs e)
    {
        BuildSearchList();
        filterstring.Attributes.Add("onfocus", "this.select();"); filterstring.Focus();
    }
    #endregion

    #region Private Methods


    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "unconfirmed":
                BuildUnconfirmedList();
                break;
            case "confirmed":
                BuildConfirmedList();
                break;
            case "processed":
                BuildProcessedList();
                break;
            case "search":
                BuildSearchList();
                break;
            default:
                BuildUnconfirmedList();
                break;
        }

    }

    protected void BuildProcessedList()
    {
        try
        {
            using (AccomodationEntities entity = new AccomodationEntities())
            {
                var familyQuery = from f in entity.FamilyPayments.Include("Family")
                                  where f.Confirmed == true && f.ExportedRef.Length > 0 && f.ExportedDate.HasValue
                                  orderby f.ProcessedDate descending
                                  select f;

                LoadResults(familyQuery.ToList());
            }

        }
        catch (Exception)
        {
        }
    }

    protected void BuildUnconfirmedList()
    {
        try
        {
            using (AccomodationEntities entity = new AccomodationEntities())
            {
                var familyQuery = from f in entity.FamilyPayments.Include("Family")
                                  where f.Confirmed == false
                                  orderby f.ProcessedDate descending
                                  select f;

                LoadResults(familyQuery.ToList());
            }

        }
        catch (Exception)
        {
        }
    }

    protected void BuildConfirmedList()
    {
        try
        {
            using (AccomodationEntities entity = new AccomodationEntities())
            {
                var familyQuery = from f in entity.FamilyPayments.Include("Family")
                                  where f.Confirmed == true && (f.ExportedRef == null && !f.ExportedDate.HasValue)
                                  orderby f.ProcessedDate descending
                                  select f;

                LoadResults(familyQuery.ToList());
            }
        }
        catch (Exception)
        {
        }
    }

    protected void BuildSearchList()
    {
        DateTime fromDate = DateTime.Now, toDate = DateTime.Now;
        string filterString = string.Empty;

        try
        {
            fromDate= (!string.IsNullOrEmpty(startperiod.Text)) ? Convert.ToDateTime(startperiod.Text) :DateTime.Now.AddDays(-7) ;
            toDate = (!string.IsNullOrEmpty(endperiod.Text)) ? Convert.ToDateTime(endperiod.Text): DateTime.Now.AddDays(7) ;
            filterString = filterstring.Text;

            using (AccomodationEntities entity = new AccomodationEntities())
            {
                var familyQuery = from f in entity.FamilyPayments.Include("Family")
                                  where (f.ProcessedDate >= fromDate && f.ProcessedDate <= toDate) && ((f.Comment.Contains(filterString) || f.PaymentRef.Contains(filterString) || f.Family.SurName.Contains(filterString) || f.Family.FirstName.Contains(filterString)))
                                  orderby f.ProcessedDate descending
                                  select f;

                LoadResults(familyQuery.ToList());
            }

        }
        catch (Exception ex)
        {
          
        }
    }

    private void LoadResults(List<FamilyPayment> family)
    {
        results.DataSource = family;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}    TotalValue: <b>{1}</b>", family.Count().ToString(), family.Sum(x => x.Amount).ToString("C"));
    }

    private static string BuildActions(long paymentId, bool confirmed)
    {
        StringBuilder sb = new StringBuilder().AppendFormat("<span id=\"{0}actions\">", paymentId);

        if (confirmed)
            return string.Format("<a class=\"{2}\" title=\"{3}\" onClick=\"paymentAction({0},{1})\"  href=\"#\"></a>", paymentId, 1, "ConfirmPayment", "Confirm Payment");
        else
            return string.Format("<a class=\"{2}\" title=\"{3}\" onClick=\"paymentAction({0},{1})\"  href=\"#\"></a>", paymentId, 2, "UnConfirmPayment", "Unconfirm Payment");
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string PaymentAction(long paymentid,int actionid)
    {
        try
        {
            using (AccomodationEntities entity = new AccomodationEntities())
            {
                FamilyPayment familyPayment = (from c in entity.FamilyPayments where c.PaymentId == paymentid select c).FirstOrDefault();


                if (familyPayment != null)
                {
                    switch (actionid)
                    {
                        case 1:
                            familyPayment.Confirmed = true;
                            familyPayment.ConfirmedBy = 1;
                            break;
                        case 2:
                            familyPayment.Confirmed = false;
                            familyPayment.ConfirmedBy = null;
                            break;
                        default:
                            break;
                    }


                }

                if (entity.SaveChanges() > 0)
                {
                    return "0";
                }
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    


    #endregion
}