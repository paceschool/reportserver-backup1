﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using PaceManager.Security;
using System.Globalization;

/// <summary>
/// Summary description for BasePage
/// </summary>
public partial class BasePage : System.Web.UI.Page
{
	public BasePage()
	{
	}

    protected DateTime FirstDayOfWeek(DateTime date, CalendarWeekRule rule)
    {

        int weeknumber = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, rule, DayOfWeek.Monday);

        DateTime jan1 = new DateTime(date.Year, 1, 1);

        int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        DateTime firstMonday = jan1.AddDays(daysOffset);

        var cal = CultureInfo.CurrentCulture.Calendar;
        int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

        if (firstWeek <= 1)
        {
            weeknumber -= 1;
        }

        DateTime result = firstMonday.AddDays(weeknumber * 7);

        return result;

    }

    public static SecurityEntities SecurityEntities
    {
        get
        {
            SecurityEntities value;

            if (CacheManager.Instance.GetFromCache<SecurityEntities>("SecurityObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<SecurityEntities>("SecurityObject", new SecurityEntities());
        }
    }

    protected static string TrimText(object text, int amount)
    {
        if (text != null && !string.IsNullOrEmpty(text.ToString()))
            return string.Format("{0}...", text.ToString().Substring(0, (text.ToString().Length < amount) ? text.ToString().Length : amount));


        return string.Empty;
    }

    protected string CalculateTimePeriod(object startdate, object enddate, char timeperiod)
    {
        int weeks;
        switch (timeperiod.ToString().ToUpper())
        {
            case "D":
                weeks = (int)(Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays;
                break;
            case "W":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 7);
                break;
            case "M":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 31);
                break;
            default:
                return "0";
        }

        return weeks.ToString();
    }

    protected static string CalculateAge(DateTime? dob)
    {
        if (dob.HasValue)
        return Convert.ToInt32(((DateTime.Now - dob.Value).Days)/365).ToString();

        return string.Empty;
    }

    protected static string CalculateTimePeriod(DateTime startdate, DateTime? enddate, char timeperiod)
    {
        int weeks;
        DateTime _enddate = (!enddate.HasValue) ? DateTime.Now :enddate.Value;
        switch (timeperiod.ToString().ToUpper())
        {
            case "D":
                weeks = (int)(Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays;
                break;
            case "W":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 7);
                break;
            case "M":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 31);
                break;
            default:
                return "0";
        }

        return weeks.ToString();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteTabItem(DeleteTabAction actionName, object id)
    {
        try
        {
            object x = null;
            System.Data.Objects.ObjectContext context = null ;

            switch (actionName)
            {
                case DeleteTabAction.FamilyMember:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyMember", "FamilyMemberId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyVisit:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyVisit", "FamilyVisitId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyAvailability:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Availability", "AvailabilityId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyComplaint:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyComplaint", "FamilyComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.Room:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Room", "AccomodationId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyNote:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyNote", "FamilyNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyBank:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyBank", "FamilyBankBankId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.StudentEnrollment:
                    context = BaseEnrollmentPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Enrollment", "EnrollmentId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentHosting:
                    context = BaseAccommodationPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Hosting", "HostingId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentComplaint:
                    context = BaseEnrollmentPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentComplaint", "StudentComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.AgencyComplaint:
                    context = BaseEnrollmentPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AgencyComplaint", "AgencyComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.AgencyNote:
                    context = BaseEnrollmentPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AgencyNote", "AgencyNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentNote:
                    context = BaseEnrollmentPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentNote", "StudentNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentTransfer:
                    context = BaseEnrollmentPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentTransfer", "StudentTransferId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupNote:
                    context = BaseGroupPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupNote", "GroupNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupTransfer:
                    context = BaseGroupPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupTransfer", "GroupTransferId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupComplaint:
                    context = BaseGroupPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupComplaint", "GroupComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupStudent:
                    context = BaseEnrollmentPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Student", "StudentId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupEnrollment:
                    context = BaseGroupPage.Entities;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Enrollment", "EnrollmentId", Convert.ToInt64(id)));
                    break;
                default:
                    break;
            }
            if (x != null && context != null)
            {
                context.DeleteObject(x);
                context.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

}

public enum DeleteTabAction
{
    FamilyMember,
    FamilyVisit,
    FamilyComplaint,
    FamilyAvailability,
    FamilyBank,
    Room,
    FamilyNote,
    StudentEnrollment,
    StudentHosting,
    StudentComplaint,
    AgencyComplaint,
    AgencyNote,
    StudentNote,
    StudentTransfer,
    GroupNote,
    GroupTransfer,
    GroupComplaint,
    GroupStudent,
    GroupEnrollment,
}