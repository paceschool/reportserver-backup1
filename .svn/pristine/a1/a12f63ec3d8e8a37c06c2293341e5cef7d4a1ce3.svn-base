﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Accommodation;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for BaseAccommodationPage
/// </summary>
public partial class BaseAccommodationPage : BasePage
{
    public AccomodationEntities _entities { get; set; }

    public BaseAccommodationPage()
	{
    }


    #region Events

    public static AccomodationEntities Entities
    {
        get
        {
            AccomodationEntities value;

            if (CacheManager.Instance.GetFromCache<AccomodationEntities>("FamilyObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<AccomodationEntities>("FamilyObject", new AccomodationEntities());
        }
    }

    public IList<Xlk_RoomType> LoadRoomTypes()
    {
        List<Xlk_RoomType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_RoomType>>("AllAccommodationRoomTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_RoomType>>("AllAccommodationRoomTypes", GetRoomTypes().ToList());
    }

    private IQueryable<Xlk_RoomType> GetRoomTypes()
    {

        IQueryable<Xlk_RoomType> lookupQuery =
            from p in Entities.Xlk_RoomType
            select p;
        return lookupQuery;

    }

    public IList<Xlk_Status> LoadStatus()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllAccommodationStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllAccommodationStatus", GetStatus().ToList());
    }
    private IQueryable<Xlk_Status> GetStatus()
    {
        IQueryable<Xlk_Status> lookupQuery =
            from c in Entities.Xlk_Status
            select c;
        return lookupQuery;
    }

    public IList<Xlk_Zone> LoadZones()
    {
        List<Xlk_Zone> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Zone>>("AllAccommodationZone", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Zone>>("AllAccommodationZone", GetZones().ToList());
    }
    private IQueryable<Xlk_Zone> GetZones()
    {
        IQueryable<Xlk_Zone> lookupQuery =
            from z in Entities.Xlk_Zone
            select z;
        return lookupQuery;
    }

    public IList<Xlk_FamilyType> LoadFamilyTypes()
    {
        List<Xlk_FamilyType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_FamilyType>>("AllAccommodationFamilyType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_FamilyType>>("AllAccommodationFamilyType", GetFamilyType().ToList());
    }

    private IQueryable<Xlk_FamilyType> GetFamilyType()
    {

        IQueryable<Xlk_FamilyType> lookupQuery =
            from p in Entities.Xlk_FamilyType
            select p;
        return lookupQuery;

    }

    protected static string CreateName(object firstName, object secondName)
    {
        if (firstName != null && secondName != null)
        {
            return string.Format("{0} {1}", secondName, firstName);
        }

        else
        {
            return (firstName != null) ? firstName.ToString() : secondName.ToString();
        }
    }
    protected static string CreateRooms(List<Room> rooms)
    {
        int doublebeds = 0;
        int singlebeds = 0;

        foreach (Room room in rooms)
        {
            if (room.NumberofDoubleBeds.HasValue)
                doublebeds += room.NumberofDoubleBeds.Value;

            if (room.NumberofSingleBeds.HasValue)
                singlebeds += room.NumberofSingleBeds.Value;
        }

        return string.Format("S:{0} / D:{1}", singlebeds, doublebeds);
    }
    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilyMembers(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAccommodationPage.aspx','Accommodation/AddFamilyMemberControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Member</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Family Member Id</th><th scope=\"col\">Name</th><th scope=\"col\">DateofBirth</th><th scope=\"col\">Type</th><th scope=\"col\">Profession</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.FamilyMember.Include("Xlk_FamilyMemberType")
                        where n.Family.FamilyId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (FamilyMember member in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Delete Family Member\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAccommodationPage.aspx','FamilyMember',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", member.FamilyMemberId, member.Name, member.DOB.ToString("D"), member.Xlk_FamilyMemberType.Description, member.Profession );
                }

            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Family Members to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilyVisits(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAccommodationPage.aspx','Accommodation/AddFamilyVisitControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Visit</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">FamilyVisitId</th><th scope=\"col\">Type</th><th scope=\"col\">Date Visited</th><th scope=\"col\">Comment</th><th scope=\"col\">Visited By</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.FamilyVisit.Include("Xlk_FamilyVisitType").Include("Users")
                        where n.Family.FamilyId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (FamilyVisit visit in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Delete Visit Record\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAccommodationPage.aspx','FamilyVisit',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", visit.FamilyVisitId, visit.Xlk_FamilyVisitType.Description, visit.DateVisited.ToString("D"), visit.Comment, visit.Users.Name);
                }

            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Visits to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilyAvailability(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form', 'ViewAccommodationPage.aspx', 'Accommodation/AddFamilyAvailabilityControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Holiday</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Family Availability Id</th><th scope=\"col\">Availability Type</th><th scope=\"col\">Date From</th><th scope=\"col\">Date To</th><th scope=\"col\">Comment</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from h in Entities.Availability.Include("Xlk_AvailabilityType")
                        where h.Family.FamilyId == id
                        select h;

            if (query.Count() > 0)
            {
                foreach (Availability availability in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align:left\">{0}</td><td style=\"text-align:left\">{1}</td><td style=\"text-align:left\">{2}</td><td style=\"text-align:left\">{3}</td><td style=\"text-align:left\">{4}</td><td style=\"text-align:center\"><a title=\"Delete Availability Record\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAccommodationPage.aspx','FamilyAvailability',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", availability.AvailabilityId, availability.Xlk_AvailabilityType.Description, availability.FromDate.Value.ToString("D"), availability.ToDate.Value.ToString("D"), availability.Comment);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Records to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilyBank(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAccommodationPage.aspx','Accommodation/AddFamilyBankControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add Bank Details</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Family Bank Id</th><th scope=\"col\">Account Name</th><th scope=\"col\">Bank Name</th><th scope=\"col\">Branch</th><th scope=\"col\">Account Number</th><th scope=\"col\">Sort Code</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from b in Entities.FamilyBank
                        where b.Family.FamilyId == id
                        select b;

            if (query.Count() > 0)
            {
                foreach (FamilyBank bank in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align:left\">{0}</td><td style=\"text-align:left\">{1}</td><td style=\"text-align:left\">{2}</td><td style=\"text-align:left\">{3}</td><td style=\"text-align:left\">{4}</td><td style=\"text-align:left\">{5}</td><td style=\"text-align:left\">{6}</td><td style=\"text-align:center\"><a title=\"Delete Bank Details\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAccommodationPage.aspx','FamilyBank',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", bank.FamilyBankBankId, bank.AccountName, bank.BankName, bank.Branch, bank.AccountNumber, bank.SortCode, bank.DateCreated.ToString("D"));
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"8\">No Bank Details to Show!</td></tr>");
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilyComplaints(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAccommodationPage.aspx','Accommodation/AddFamilyComplaintControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Complaint</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">FamilyComplaintId</th><th scope=\"col\">Severity</th><th scope=\"col\">Type</th><th scope=\"col\">Complaint</th><th scope=\"col\">Date Created</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.FamilyComplaint.Include("Xlk_Severity").Include("Xlk_ComplaintType")
                        where n.Family.FamilyId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (FamilyComplaint complaint in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td><td style=\"text-align: center\"><a title=\"Delete Complaint Record\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAccommodationPage.aspx','FamilyComplaint',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", complaint.FamilyComplaintId, complaint.Xlk_Severity.Description, complaint.Xlk_ComplaintType.Description, complaint.Complaint, complaint.DateCreated.ToString("D"));
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Complaints to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilyRooms(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAccommodationPage.aspx','Accommodation/AddFamilyRoomControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Room</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Room Id</th><th scope=\"col\">Type</th><th scope=\"col\">Description</th><th scope=\"col\">Is Ensuite</th><th scope=\"col\">Beds Single/Double</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from n in Entities.Room.Include("Xlk_RoomType")
                        where n.Family.FamilyId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (Room room in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: center\"><img src=\"../Content/img/actions/thumb-up.png\" visible={3}/></td> <td style=\"text-align: left\">{5} Single, {4} Double</td><td style=\"text-align: center\"><a title=\"Delete Room Record\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAccommodationPage.aspx','Room',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", room.AccomodationId, room.Xlk_RoomType.Description, room.Description, room.Ensuite, room.NumberofDoubleBeds, room.NumberofSingleBeds);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Rooms to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilyTags(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAccommodationPage.aspx','Accommodation/AddFamilyTagControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add new Tag</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">Tags</th><th scope=\"col\">Actions</th></tr></thead><tbody>");

            var query = from t in Entities.Tag
                        from f in t.Family
                        where f.FamilyId == id
                        select t;

            if (query.Count() > 0)
            {
                foreach (Tag tag in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{1}</td><td style=\"text-align: center\"><a title=\"Disassociate Tag from Family\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewAccommodationPage.aspx','FamilyTag',{2}, {0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", tag.TagId, tag.Text, id);
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"2\">No Tags to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadNotes(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadControl('#dialog-form','ViewAccommodationPage.aspx','Accommodation/AddFamilyNoteControl.ascx'," + id + ")\"><img src=\"../Content/img/actions/add.png\" />Add New Note</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead>  <tr>  <th scope=\"col\">  NoteId  </th>  <th scope=\"col\">  Note Type  </th>  <th scope=\"col\">  Note  </th>  <th scope=\"col\">  Date Created  </th>  <th scope=\"col\">  Actions  </th>  </tr>  </thead>  <tbody>");

            var query = from n in Entities.FamilyNote.Include("Xlk_NoteType")
                        where n.Family.FamilyId == id
                        select n;

            if (query.Count() > 0)
            {
                foreach (FamilyNote note in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: center\"><a title=\"Delete Note\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewAccommodationPage.aspx','FamilyNote',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", note.FamilyNoteId, note.Xlk_NoteType.Description, note.Note, note.DateCreated.Value.ToString("D"));
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"6\">No Notes to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadPreviousStudents(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"> <thead>  <tr>  <th scope=\"col\">  Name  </th>  <th scope=\"col\">Arrival</th>  <th scope=\"col\">Departure</th>  <th scope=\"col\">Agent</th><th scope=\"col\">DOB</th>  </tr>  </thead>  <tbody>");

            var query = from n in Entities.Hosting.Include("Student").Include("Student.Agency")
                        where n.Family.FamilyId == id && n.DepartureDate < DateTime.Now
                        select n;

            if (query.Count() > 0)
            {
                foreach (Hosting host in query.ToList())
                {
                    sb.AppendFormat("<td style=\"text-align: left\"><a href=\"../Enrollments/ViewStudentPage.aspx?StudentId={6}\">{0} {1}</a></td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: left\">{5}</td> </tr>", host.Student.FirstName, host.Student.SurName, host.ArrivalDate.ToString("D"), host.DepartureDate.ToString("D"), host.Student.Agency.Name, (host.Student.DateOfBirth.HasValue) ? host.Student.DateOfBirth.Value.ToString("D") : string.Empty, host.Student.StudentId);
                }

            }
            else
            {
                sb.Append("<tr><td colspan=\"5\">No Previous Students to Show!</td></tr>");
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseAccommodationPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("FamilyId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveFamilyMember(FamilyMember newObject)
    {
        try
        {
            FamilyMember newmember = new FamilyMember();

            newmember.DOB = newObject.DOB;
            newmember.FamilyMemberId = newObject.FamilyMemberId;
            newmember.FamilyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Family", newObject.Family);
            newmember.Name = newObject.Name;
            newmember.Profession = newObject.Profession;
            newmember.Xlk_FamilyMemberTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_FamilyMemberType", newObject.Xlk_FamilyMemberType);

            if (newmember.FamilyMemberId == 0)
                Entities.AddToFamilyMember(newmember);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveFamilyAvailability(Availability newObject)
    {
        try
        {
            Availability newAvailability = new Availability();

            newAvailability.Xlk_AvailabilityTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_AvailabilityType", newObject.Xlk_AvailabilityType);
            newAvailability.FromDate = newObject.FromDate;
            newAvailability.ToDate = newObject.ToDate;
            newAvailability.Comment = newObject.Comment;
            newAvailability.FamilyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Family", newObject.Family);

            if (newAvailability.AvailabilityId == 0)
                Entities.AddToAvailability(newAvailability);

            Entities.SaveChanges();

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveFamilyTag(ParentChildObject<int,Tag> newObject)
    {
        try
        {
            Family family = (from f in Entities.Family
                              where f.FamilyId == newObject.ParentId
                              select f).FirstOrDefault();

            Tag tag = (from f in Entities.Tag
                       where f.TagId == newObject.Child.TagId
                       select f).FirstOrDefault();

            if (!family.Tag.Contains(tag))
            {
                family.Tag.Add(tag);
                Entities.SaveChanges();
            }

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveFamilyNote(FamilyNote newObject)
    {
        try
        {
            FamilyNote newnote = new FamilyNote();

            newnote.DateCreated = DateTime.Now;
            newnote.FamilyNoteId = newObject.FamilyNoteId;
            newnote.Note = newObject.Note;
            newnote.FamilyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Family", newObject.Family);
            newnote.Xlk_NoteTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_NoteType", newObject.Xlk_NoteType);
            newnote.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newnote.FamilyNoteId == 0)
                Entities.AddToFamilyNote(newnote);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveRoom(Room newObject)
    {
        try
        {
            Room newroom = new Room();

            newroom.AccomodationId = newObject.AccomodationId;
            newroom.Description = newObject.Description;
            newroom.Ensuite = newObject.Ensuite;
            newroom.NumberofDoubleBeds = newObject.NumberofDoubleBeds;
            newroom.NumberofSingleBeds = newObject.NumberofSingleBeds;

            newroom.FamilyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Family", newObject.Family);
            newroom.Xlk_RoomTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_RoomType", newObject.Xlk_RoomType);

            if (newroom.AccomodationId == 0)
                Entities.AddToRoom(newroom);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveFamilyVisit(FamilyVisit newObject)
    {
        try
        {
            FamilyVisit newvisit = new FamilyVisit();

            newvisit.Comment = newObject.Comment;
            newvisit.DateVisited = newObject.DateVisited;
            newvisit.FamilyVisitId = newObject.FamilyVisitId;
            newvisit.FamilyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Family", newObject.Family);
            newvisit.Xlk_FamilyVisitTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_FamilyVisitType", newObject.Xlk_FamilyVisitType);
            newvisit.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newvisit.FamilyVisitId == 0)
                Entities.AddToFamilyVisit(newvisit);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveFamilyBank(FamilyBank newObject)
    {
        try
        {
            FamilyBank newbank = new FamilyBank();

            newbank.AccountName = newObject.AccountName;
            newbank.BankName = newObject.BankName;
            newbank.Branch = newObject.Branch;
            newbank.AccountNumber = newObject.AccountNumber;
            newbank.SortCode = newObject.SortCode;
            newbank.DateCreated = newObject.DateCreated;
            newbank.FamilyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Family", newObject.Family);

            if (newbank.FamilyBankBankId == 0)
                Entities.AddToFamilyBank(newbank);

            Entities.SaveChanges();

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveFamilyComplaint(FamilyComplaint newObject)
    {
        try
        {
            FamilyComplaint newcomplaint = new FamilyComplaint();

            newcomplaint.ActionNeeded = newObject.ActionNeeded;
            newcomplaint.Complaint = newObject.Complaint;
            newcomplaint.DateCreated = DateTime.Now;
            newcomplaint.FamilyReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Family", newObject.Family);
            newcomplaint.Xlk_ComplaintTypeReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_ComplaintType", newObject.Xlk_ComplaintType);
            newcomplaint.Xlk_SeverityReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Xlk_Severity", newObject.Xlk_Severity);
            newcomplaint.UsersReference.EntityKey = Entities.CreateEntityKey(Entities.DefaultContainerName + ".Users", newObject.Users);

            if (newcomplaint.FamilyComplaintId == 0)
                Entities.AddToFamilyComplaint(newcomplaint);

            Entities.SaveChanges();

            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
       
    #endregion
}
