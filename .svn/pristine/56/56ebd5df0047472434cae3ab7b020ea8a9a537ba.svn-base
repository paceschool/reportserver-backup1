﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using Pace.DataAccess.Agent;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;

public partial class Agent_ViewQuotePage : BaseAgentPage
{
    protected Int32 _agencyId;
    protected Int32? _quoteId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["QuoteId"]))
            _quoteId = Convert.ToInt32(Request["QuoteId"]);

        if (!Page.IsPostBack)
        {
            newnationality.DataSource = BaseAgentPage.LoadNationalities();
            newnationality.DataBind();

            editAgent.DataSource = this.LoadAgents();
            editAgent.DataBind();

            editStatus.DataSource = this.LoadStatus();
            editStatus.DataBind();

            DisplayCurrentQuote(LoadCurrentQuote(new AgentEntities(), _quoteId.Value));
        }
    }

    protected Quote LoadCurrentQuote(AgentEntities entity, Int32 quoteId)
    {
        IQueryable<Quote> quoteQuery = from q in entity.Quotes.Include("Xlk_Status").Include("Xlk_Nationality")
                                       where q.QuoteId == quoteId
                                       select q;
        

        if (quoteQuery.ToList().Count() > 0)
            return quoteQuery.ToList().First();

        return null;
    }

    private void DisplayCurrentQuote(Quote quote)
    {
        if (quote != null)
        {
            QuoteId.Text = quote.QuoteId.ToString();
            CreatedBy.Text = quote.CreatedByUser.Name;
            DateCreated.Text = quote.DateCreated.ToString("dd/MM/yyyy");
            Agency.Text = AgencyLink(quote.Agency.AgencyId, "~/Agent/AgentPage.aspx", quote.Agency.Name);
            quotestatus.Text = quote.Xlk_Status.Description;
            Description.Text = quote.Description;
            SAGERef.Text = quote.SageInvoiceRef;
            Dates.Text = string.Format("{0} to {1}", quote.ArrivalDate.ToString("dd/MM/yyyy"), quote.DepartureDate.HasValue ? quote.DepartureDate.Value.ToString("dd/MM/yyyy") : "Not Specified");
            editAgent.Items.FindByValue(quote.Agency.AgencyId.ToString()).Selected = true;
            editStatus.Items.FindByValue(quote.Xlk_Status.StatusId.ToString()).Selected = true;
            editSage.Text = quote.SageInvoiceRef;
            editDescription.Text = quote.Description;
            editComment.Text = quote.Comment;
            newarrdate.Text = quote.ArrivalDate.ToString("dd/MM/yyyy");
            newdepdate.Text = quote.DepartureDate.HasValue ? quote.DepartureDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            if (quote.Xlk_Nationality != null)
                newnationality.Items.FindByValue(quote.Xlk_Nationality.NationalityId.ToString()).Selected = true;
            newstudentno.Text = quote.NoOfStudents.ToString();
            newleaderno.Text = quote.NoOfLeaders.ToString();
            newname.Text = quote.ContactName;
            newaddress.Text = quote.Address;

            var list = GetAgentStudentsAndGroups(quote.QuoteId, quote.Agency.AgencyId);
            if (list.Success && list.Payload != null)
            {
                groups.DataSource = list.Payload["Groups"];
                groups.DataBind();

                students.DataSource = list.Payload["Students"];
                students.DataBind();
            }

        }
    }

    protected void Click_Save(object sender, EventArgs e)
    {
        using (AgentEntities entity = new AgentEntities())
        {
            if (Convert.ToInt32(editAgent.SelectedItem.Value) != 0 && Convert.ToInt32(editStatus.SelectedItem.Value) != 0)
            {
                Quote _quote = GetQuote(entity);

                _quote.AgencyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Agencies", "AgencyId", Convert.ToInt32(editAgent.SelectedValue));
                _quote.CreatedByUserReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", GetCurrentUser().UserId);
                _quote.QuoteId = Convert.ToInt32(QuoteId.Text);
                _quote.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(editStatus.SelectedValue));
                _quote.SageInvoiceRef = SAGERef.Text;
                _quote.Description = editDescription.Text;
                _quote.Comment = editComment.Text;
                _quote.ContactName = newname.Text;
                _quote.Address = newaddress.Text;

                _quote.NoOfLeaders = (Int16)(string.IsNullOrEmpty(newleaderno.Text) ? 0 : Convert.ToInt16(newleaderno.Text));
                _quote.NoOfStudents = (Int16)(string.IsNullOrEmpty(newstudentno.Text) ? 0 : Convert.ToInt16(newstudentno.Text));
                _quote.Xlk_NationalityReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedValue));

                if (groups.SelectedValue != null && Convert.ToInt32(groups.SelectedValue) > 0)
                {
                    entity.AssignQuote(_quote.QuoteId, Convert.ToInt32(groups.SelectedValue), null);
                }

                if (students.SelectedValue != null && Convert.ToInt32(students.SelectedValue) > 0)
                {
                    entity.AssignQuote(_quote.QuoteId, Convert.ToInt32(students.SelectedValue), null);
                }

                SaveQuote(entity, _quote);
            }
            else
            {

            }
        }
    }

    private Quote GetQuote(AgentEntities entity)
    {
        if (_quoteId.HasValue)
        {
            return LoadCurrentQuote(entity, _quoteId.Value);
        }

        return new Quote();
    }

    private void SaveQuote(AgentEntities entity, Quote quote)
    {
        if (!_quoteId.HasValue)
        {
            entity.AddToQuotes(quote);
        }

        if (entity.SaveChanges() > 0)
            Response.Redirect(string.Format("ViewQuotePage.aspx?QuoteId=" + quote.QuoteId));
    }
}