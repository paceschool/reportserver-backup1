﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Enrollment;
using System.Text;
using System.Web.Services;

public partial class Group_AddEnrollment : BaseEnrollmentControl
{
    protected Int32 _familyId;
    protected PaceManager.Group.Enrollment _enrollment;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("EnrollmentId") != null)
        {
            Int32 enrollmentId = (Int32)GetValue("EnrollmentId");
            _enrollment = (from enroll in BaseGroupPage.Entities.Enrollment.Include("Xlk_ProgrammeType").Include("Xlk_CourseType").Include("Xlk_LessonBlock").Include("Group")
                           where enroll.EnrollmentId == enrollmentId
                           select enroll).FirstOrDefault();

        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetProgrammeTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_enrollment != null && _enrollment.Xlk_ProgrammeType != null) ? _enrollment.Xlk_ProgrammeType.ProgrammeTypeId : (byte?)null;

        foreach (Xlk_ProgrammeType item in LoadProgrammeTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ProgrammeTypeId, item.Description, (_current.HasValue && item.ProgrammeTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetCourseTypeList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_enrollment != null && _enrollment.Xlk_CourseType != null) ? _enrollment.Xlk_CourseType.CourseTypeId : (short?)null;

        foreach (Xlk_CourseType item in LoadCourseTypes())
        {
            sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.CourseTypeId, item.Description, (_current.HasValue && item.CourseTypeId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetLessonBlockTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_enrollment != null && _enrollment.Xlk_LessonBlock != null) ? _enrollment.Xlk_LessonBlock.LessonBlockId : (byte?)null;

        foreach (Xlk_LessonBlock item in LoadLessonBlocks())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LessonBlockId, item.Description, (_current.HasValue && item.LessonBlockId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();

    }

    protected string GetEnrollmentId
    {
        get
        {
            if (_enrollment != null)
                return _enrollment.EnrollmentId.ToString();
            return "0";
        }
    }

    protected string GetStartDate
    {
        get
        {
            if (_enrollment != null)
                return _enrollment.StartDate.ToString("dd/MM/yyyy");

            return null;
        }
    }
    protected string GetEndDate
    {
        get
        {
            if (_enrollment != null && _enrollment.EndDate.HasValue)
                return _enrollment.EndDate.Value.ToString("dd/MM/yyyy");

            return null;

        }
    }
    protected string GetGroupId
    {

        get
        {
            if (_enrollment != null && _enrollment.Group != null)
                return _enrollment.Group.GroupId.ToString();

            return null;
        }
    }
}
