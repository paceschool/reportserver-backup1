﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Agent;
using System.Text;
using Pace.Common;

/// <summary>
/// Summary description for BaseAgencyControl
/// </summary>
public class BaseAgencyControl : BaseControl
{
    public AgentEntities _entities { get; set; }
    
    public BaseAgencyControl()
	{
		
	}

    public static AgentEntities CreateEntity
    {
        get
        {
            return new AgentEntities();
        }
    }

    public IList<Xlk_Status> LoadStatus()
    {
        List<Xlk_Status> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Status>>("AllStatus", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Status>>("AllStatus", GetStatus().ToList());
    }

    public IList<Agency> LoadAgents()
    {
        List<Agency> values = null;

        if (CacheManager.Instance.GetFromCache<List<Agency>>("AllAgent", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Agency>>("AllAgent", GetAgents().ToList());
    }

    public IList<Pace.DataAccess.Agent.Xlk_ProductType> LoadProductType()
    {
        IList<Pace.DataAccess.Agent.Xlk_ProductType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.Xlk_ProductType>>("AllAgentProductType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.Xlk_ProductType>>("AllAgentProductType", GetProductType());
    }

    public static IList<Xlk_Nationality> LoadNationalities()
    {
        IList<Xlk_Nationality> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Nationality>>("AllAgenttNationalities", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Nationality>>("AllAgenttNationalities", GetNationalities());
    }

    private static IList<Xlk_Nationality> GetNationalities()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Xlk_Nationality> lookupQuery =
                from a in entity.Xlk_Nationality
                select a;
            return lookupQuery.ToList();
        }
    }

    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.NoteTypeId, item.Description);
        }

        return sb.ToString();

    }
    public IList<Xlk_NoteType> LoadNoteTypes()
    {
        List<Xlk_NoteType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_NoteType>>("AllAgencyNoteType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_NoteType>>("AllAgencyNoteType", GetNoteType().ToList());
    }

    private IList<Xlk_NoteType> GetNoteType()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Xlk_NoteType> lookupQuery =
                from p in entity.Xlk_NoteType
                select p;
            return lookupQuery.ToList();
        }

    }


    private IList<Xlk_ProductType> GetProductType()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Xlk_ProductType> lookupQuery =
                from q in entity.Xlk_ProductType
                select q;
            return lookupQuery.ToList();
        }
    }
    private IList<Xlk_Status> GetStatus()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Xlk_Status> lookupQuery =
                from q in entity.Xlk_Status
                select q;
            return lookupQuery.ToList();
        }
    }

    private IList<Agency> GetAgents()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Agency> lookupQuery =
                from a in entity.Agencies
                orderby a.Name ascending
                select a;
            return lookupQuery.ToList();
        }
    }

    public IList<Pace.DataAccess.Agent.OrderableItem> LoadOrderableItems()
    {
        IList<Pace.DataAccess.Agent.OrderableItem> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.OrderableItem>>("AllOrderableItem", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.OrderableItem>>("AllOrderableItem", GetOrderableItems());
    }

    public IList<Pace.DataAccess.Agent.RateCardCosting> LoadRateCardCostings()
    {
        IList<Pace.DataAccess.Agent.RateCardCosting> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.RateCardCosting>>("AllRateCardCosting", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.RateCardCosting>>("AllRateCardCosting", GetRateCardCostings());
    }

    public IList<Pace.DataAccess.Agent.Xlk_Unit> LoadUnits()
    {
        IList<Pace.DataAccess.Agent.Xlk_Unit> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.Xlk_Unit>>("AllUnit", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.Xlk_Unit>>("AllUnit", GetUnits());
    }
    public IList<Pace.DataAccess.Agent.Xlk_CalculationType> LoadCalculationType()
    {
        IList<Pace.DataAccess.Agent.Xlk_CalculationType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Agent.Xlk_CalculationType>>("AllCalculationType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Agent.Xlk_CalculationType>>("AllCalculationType", GetCalculationType());
    }


    public IList<User> LoadUsers()
    {
        List<User> values = null;

        if (CacheManager.Instance.GetFromCache<List<User>>("AllUser", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<User>>("AllUser", GetUsers().ToList());
    }

    private IList<User> GetUsers()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<User> lookupQuery =
                from u in entity.Users
                select u;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_Unit> GetUnits()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Xlk_Unit> lookupQuery =
                from un in entity.Xlk_Unit
                select un;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_CalculationType> GetCalculationType()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Xlk_CalculationType> lookupQuery =
                from un in entity.Xlk_CalculationType
                select un;
            return lookupQuery.ToList();
        }
    }
    private IList<Pace.DataAccess.Agent.OrderableItem> GetOrderableItems()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Pace.DataAccess.Agent.OrderableItem> lookupQuery =
                from o in entity.OrderableItems
                select o;
            return lookupQuery.ToList();
        }
    }

    private IList<Pace.DataAccess.Agent.RateCardCosting> GetRateCardCostings()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            IQueryable<Pace.DataAccess.Agent.RateCardCosting> lookupQuery =
                from r in entity.RateCardCostings
                select r;
            return lookupQuery.ToList();
        }
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.StatusId, item.Description);
        }

        return sb.ToString();
    }
}