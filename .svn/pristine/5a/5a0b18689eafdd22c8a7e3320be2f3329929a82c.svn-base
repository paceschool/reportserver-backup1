﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class NameValue
{
    public string name { get; set; }
    public object value { get; set; }
}
/// <summary>
/// Simple NameValue class that maps name and value
/// properties that can be used with jQuery's 
/// $.serializeArray() function and JSON requests
/// </summary>

public static class NameValueExtensionMethods
{
    /// <summary>
    /// Retrieves a single form variable from the list of
    /// form variables stored
    /// </summary>
    /// <param name="formVars"></param>
    /// <param name="name">formvar to retrieve</param>
    /// <returns>value or string.Empty if not found</returns>
    public static T Form<T>(this  NameValue[] formVars, string name)
    {
        var matches = formVars.Where(nv => nv.name.ToLower() == name.ToLower()).FirstOrDefault();
        if (matches != null && !string.IsNullOrEmpty(matches.value.ToString()))
            return (T)Convert.ChangeType(matches.value, typeof(T));
        return default(T);
    }

    /// <summary>
    /// Retrieves a single form variable from the list of
    /// form variables stored
    /// </summary>
    /// <param name="formVars"></param>
    /// <param name="name">formvar to retrieve</param>
    /// <returns>value or string.Empty if not found</returns>
    public static bool AnyValues(this  NameValue[] formVars)
    {
        return formVars.Where(nv => nv.value != null).Count() > 0;
    }
}