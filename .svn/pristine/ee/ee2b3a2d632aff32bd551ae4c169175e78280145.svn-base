﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Accommodation;
using System.Data.Objects.DataClasses;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Collections.Specialized;

public partial class Accommodation_AssignAccommodationPage : BaseAccommodationPage 
{
    protected Int32? _groupId;
    protected DateTime _startDate;
    protected DateTime _endDate;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["GroupId"]))
            _groupId = Convert.ToInt32(Request["GroupId"]);

    }

     protected string LoadStatusList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">Any</option>");


        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.StatusId, item.Description);
        }

        return sb.ToString();

    }
    protected string GetRoomTypeList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">Any</option>");

        foreach (Xlk_RoomType item in LoadRoomTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.RoomTypeId, item.Description);
        }

        return sb.ToString();

    }
    protected string LoadGroupList()
    {

        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            var groupQuery = from g in entity.Group
                             where g.ArrivalDate > DateTime.Now
                             orderby g.ArrivalDate, g.GroupName
                             select new { g.GroupId, g.GroupName };

            foreach (var item in groupQuery.ToList())
            {
                sb.AppendFormat("<option value=\"{0}\">{1}</option>", item.GroupId, item.GroupName);
            }
        }
        return sb.ToString();

    }

    protected static Dictionary<int, DateTime> LoadGroupDetails(int id, out Pace.DataAccess.Group.Group group)
    {
        group = null;

        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            IQueryable<Pace.DataAccess.Group.Group> groupQuery = from s in entity.Group.Include("Xlk_Nationality")
                                                             where s.GroupId == id
                                                             select s;
            Dictionary<int, DateTime> timeperiods = new Dictionary<int, DateTime>();

            if (groupQuery.Count() > 0)
            {
                group = groupQuery.First();

                int noofdays = (group.DepartureDate - group.ArrivalDate).Days;

                for (int i = 0; i < noofdays; i++)
                {
                    timeperiods.Add(i, group.ArrivalDate.AddDays(i));
                }
            }

            return timeperiods;
        }

    }

    protected int GetGroupId()
    {

        if (_groupId.HasValue)
            return _groupId.Value;

        return 0;

    }

    protected static string CreateFamilyHostingList(Dictionary<int, DateTime> timeperiods, object hosting)
    {

        decimal dimension = (85 / timeperiods.Count);
        StringBuilder sb = new StringBuilder();

        foreach (var item in timeperiods)
        {
            int i = 0;
            if (hosting != null && ((EntityCollection<Hosting>)hosting).Count > 0)
                i = ((EntityCollection<Hosting>)hosting).Where(h => (h.ArrivalDate <= item.Value && item.Value <= h.DepartureDate)).Count();
            if (i>0)
                sb.AppendFormat("<span class=\"index\" style=\"margin-left:{0}%;width:{1}%;{3}\">{2}</span>", item.Key * dimension, dimension, i,(i>1) ? (i >= 3) ? "background-color:#3C849F" : "background-color:#95AEB8":string.Empty);

           
        } 
        sb.Append("<span class=\"count\">2 students</span>");
        return sb.ToString();
    }


    protected static string CreateClass(object hosting)
    {
        if (hosting != null && ((EntityCollection<Hosting>)hosting).Count > 0 )
            return "assigned";

        return "draggable ui-state-default";
    }

    private static IQueryable<Family> FilterFamilies(Pace.DataAccess.Group.Group group, NameValue[] details)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            IQueryable<Family> familyQuery = from f in entity.Families.Include("Hosting")
                                             from h in f.Hostings
                                             where f.Xlk_FamilyType.FamilyTypeId == 2 && h.DepartureDate >= DateTime.Now
                                             select f;

            if (!details.AnyValues())
                return familyQuery;


            if (!string.IsNullOrEmpty(details.Form<string>("familyname")))
            {
                string filterstring = details.Form<string>("familyname");
                familyQuery = familyQuery.Where(f => f.FirstName.StartsWith(filterstring) || f.SurName.StartsWith(filterstring));
            }

            if (details.Form<int>("excludenationality") == 1)
                familyQuery = familyQuery.Where(f => f.Hostings.Where(h => h.Student.Xlk_Nationality.NationalityId != group.Xlk_Nationality.NationalityId).Count() > 0);

            if (details.Form<int>("ensuite") == 1)
                familyQuery = familyQuery.Where(f => f.Rooms.Where(r => r.Ensuite == true).Count() > 0);

            if (details.Form<int>("statusid") > 0)
            {
                int statusid = details.Form<int>("statusid");

                familyQuery = familyQuery.Where(f => f.Xlk_Status.StatusId == statusid);
            }

            if (details.Form<int>("roomtypeid") > 0)
            {
                int roomtypeid = details.Form<int>("roomtypeid");
                familyQuery = familyQuery.Where(f => f.Rooms.Where(r => r.Xlk_RoomType.RoomTypeId == roomtypeid).Count() > 0);
            }

            if (details.Form<int>("zoneid") > 0)
            {
                int zoneid = details.Form<int>("zoneid");
                familyQuery = familyQuery.Where(f => f.Xlk_Zone.ZoneId == zoneid);
            }

            return familyQuery;
        }


    }


    #region Javascript Events

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] LoadGroupStudents(int id)
    {
        try
        {
            StringBuilder groupString = new StringBuilder("<table class=\"box-table-a\"><thead ><tr ><th scope=\"col\" ><label>  Group ID  </label ></th ><th scope=\"col\" ><label>  Name  </label ></th ><th scope=\"col\" ><label>  Arrival Date  </label ></th ><th scope=\"col\" ><label>  Departure Date  </label ></th ><th scope=\"col\" ><label>  #Weeks  </label ></th ></tr></thead><tbody>");
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                Pace.DataAccess.Group.Group group = (from g in entity.Group
                                                 where g.GroupId == id
                                                 select g).FirstOrDefault();
                if (group != null)
                {
                    groupString.AppendFormat("<tr ><td style=\"text-align: left\">{0}</td ><td style=\"text-align: left\" >{1}</td ><td style=\"text-align: left\" >{2}</td ><td style=\"text-align: left\" >{3}</td ><td style=\"text-align: left\" >{4}</td>  </tr>", group.GroupId, group.GroupName, group.ArrivalDate.ToString("D"), group.DepartureDate.ToString("D"), CalculateTimePeriod(group.ArrivalDate, group.DepartureDate, 'W'));
                }
                else
                {
                    groupString.Append("<li>There are no students in this group</li>");
                }
            }

           groupString.Append("</tbody></table>");

            StringBuilder studentsString = new StringBuilder("<ul>");
            using (AccomodationEntities entity = new AccomodationEntities())
            {
                IQueryable<Student> hostingQuery = from s in entity.Student.Include("Hosting")
                                                   where s.GroupId.Value == id
                                                   select s;

                if (hostingQuery.Count() > 0)
                {
                    foreach (Student student in hostingQuery.ToList())
                    {
                        studentsString.AppendFormat("<li id=\"{2}\" class=\"{0}\">{1}</li>", CreateClass(student.Hostings), CreateName(student.FirstName, student.SurName), student.StudentId);
                    }
                }
                else
                {
                    studentsString.Append("<li>There are no students in this group</li>");
                }

                studentsString.Append("</ul>");

                return new string[] { groupString.ToString(), studentsString.ToString() };
            }

        }
        catch (Exception ex)
        {
            return new string[] {};;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilies(int id, NameValue[] details) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        Pace.DataAccess.Group.Group group;
        try
        {

            StringBuilder sb = new StringBuilder("<ul class=\"chartlist\">");
            Dictionary<int, DateTime> timeperiods = LoadGroupDetails(id, out group);


            #region FilterFamilies
            using (AccomodationEntities entity = new AccomodationEntities())
            {
                var familyQuery = from f in entity.Families
                                  from h in f.Hostings
                                  where f.Xlk_FamilyType.FamilyTypeId == 2 && h.DepartureDate >= DateTime.Now
                                  select new { Family = f, f.Hostings, f.Rooms };


                if (!string.IsNullOrEmpty(details.Form<string>("familyname")))
                {
                    string filterstring = details.Form<string>("familyname");
                    familyQuery = familyQuery.Where(f => f.Family.FirstName.StartsWith(filterstring) || f.Family.SurName.StartsWith(filterstring));
                }

                if (details.Form<int>("excludenationality") == 1)
                    familyQuery = familyQuery.Where(f => f.Hostings.Where(h => h.Student.Xlk_Nationality.NationalityId != group.Xlk_Nationality.NationalityId).Count() > 0);

                if (details.Form<int>("ensuite") == 1)
                    familyQuery = familyQuery.Where(f => f.Family.Rooms.Where(r => r.Ensuite == true).Count() > 0);

                if (details.Form<int>("statusid") > 0)
                {
                    int statusid = details.Form<int>("statusid");

                    familyQuery = familyQuery.Where(f => f.Family.Xlk_Status.StatusId == statusid);
                }

                if (details.Form<int>("roomtypeid") > 0)
                {
                    int roomtypeid = details.Form<int>("roomtypeid");
                    familyQuery = familyQuery.Where(f => f.Family.Rooms.Where(r => r.Xlk_RoomType.RoomTypeId == roomtypeid).Count() > 0);
                }

                //int zoneid = details.Form<int>("zoneid");
                //{
                //    int zoneid = details.Form<int>("zoneid");
                //familyQuery = familyQuery.Where(f => f.Xlk_Zone.ZoneId == zoneid);
                //}

            #endregion

                if (familyQuery.Count() > 0)
                {
                    foreach (var family in familyQuery.Take(50).ToList())
                    {
                        sb.AppendFormat("<li id=\"{0}\" class=\"droppable\">", family.Family.FamilyId);
                        sb.AppendFormat("<a href=\"ViewAccommodationPage.aspx?FamilyId={1}\">{0}</a> ", CreateName(family.Family.FirstName, family.Family.SurName), family.Family.FamilyId);

                        sb.Append(CreateFamilyHostingList(timeperiods, family.Hostings));
                        sb.Append("</li>");
                    }
                }
                else
                {
                    sb.Append("<li></li>");
                }
            }
            sb.Append("</ul>");

            return sb.ToString();

            //StringBuilder sb = new StringBuilder("<dl>");
            //Dictionary<int, DateTime> timeperiods = LoadGroupDetails(id, out group);


            //#region FilterFamilies

            //var familyQuery = from f in Entities.Family
            //                  from h in f.Hosting
            //                  where f.Xlk_FamilyType.FamilyTypeId == 2 && h.DepartureDate >= DateTime.Now
            //                  select new {Family=f, f.Hosting,f.Room };


            //if (!string.IsNullOrEmpty(details.Form<string>("familyname")))
            //{
            //    string filterstring = details.Form<string>("familyname");
            //    familyQuery = familyQuery.Where(f => f.Family.FirstName.StartsWith(filterstring) || f.Family.SurName.StartsWith(filterstring));
            //}

            //if (details.Form<int>("excludenationality") == 1)
            //    familyQuery = familyQuery.Where(f => f.Hosting.Where(h => h.Student.NationalityId != group.Xlk_Nationality.NationalityId).Count() > 0);

            //if (details.Form<int>("ensuite") == 1)
            //    familyQuery = familyQuery.Where(f => f.Family.Room.Where(r => r.Ensuite == true).Count() > 0);

            //if (details.Form<int>("statusid") > 0)
            //{
            //    int statusid = details.Form<int>("statusid");

            //    familyQuery = familyQuery.Where(f => f.Family.Xlk_Status.StatusId == statusid);
            //}

            //if (details.Form<int>("roomtypeid") > 0)
            //{
            //    int roomtypeid = details.Form<int>("roomtypeid");
            //    familyQuery = familyQuery.Where(f => f.Family.Room.Where(r => r.Xlk_RoomType.RoomTypeId == roomtypeid).Count() > 0);
            //}

            ////int zoneid = details.Form<int>("zoneid");
            ////{
            ////    int zoneid = details.Form<int>("zoneid");
            ////familyQuery = familyQuery.Where(f => f.Xlk_Zone.ZoneId == zoneid);
            ////}

            //#endregion


            //if (familyQuery.Count() > 0)
            //{
            //    foreach (var family in familyQuery.Take(50).ToList())
            //    {
            //        sb.AppendFormat("<dt id=\"{0}\" class=\"droppable\">{1} {2}</dt>", family.Family.FamilyId, CreateName(family.Family.FirstName, family.Family.SurName),CreateRooms(family.Room.ToList()));
            //        sb.AppendFormat("<dd><ul class=\"daysavailable\">{0}</ul></dd>", CreateFamilyHostingList(timeperiods, family.Hosting));
            //    }
            //}
            //else
            //{
            //    sb.Append("<li></li>");
            //}

            //sb.Append("</dl>");

            //return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool AssignStudentToFamily(int id, int familyid, int studentid) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        Pace.DataAccess.Group.Group group;
        try
        {

            Dictionary<int, DateTime> timeperiods = LoadGroupDetails(id, out group);

            if (group != null)
            {

                using (AccomodationEntities entity = new AccomodationEntities())
                {
                    Hosting hosting = new Hosting();
                    hosting.ArrivalDate = group.ArrivalDate;
                    hosting.DepartureDate = group.DepartureDate;
                    hosting.FamilyReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Family", "FamilyId", familyid);
                    hosting.StudentReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Student", "StudentId", studentid);

                    entity.AddToHostings(hosting);

                    return (entity.SaveChanges() > 0);
                }


            }

            return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

}
