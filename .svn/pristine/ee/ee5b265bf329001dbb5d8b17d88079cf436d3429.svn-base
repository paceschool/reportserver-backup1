﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Excursion;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;

public partial class Excursions_ManageExcursionsPage : BaseExcursionPage
{
    #region Events

    private Int32? _schoolExcursionId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["SchoolExcursionId"]))
            _schoolExcursionId = Convert.ToInt32(Request["SchoolExcursionId"]);

        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            excursiontype.DataSource = this.LoadExcursionTypes();
            excursiontype.DataBind();
            monthselector.SelectedIndex = Convert.ToInt32(DateTime.Now.Month);
            yearselector.SelectedIndex = Convert.ToInt32(DateTime.Now.Year) - 2011;
            ExcursionsChanged();
            excursionvenue.DataSource = this.LoadExcursionVenues();
            excursionvenue.DataBind();
            LoadHours();
            meettime.SelectedIndex = (Convert.ToInt32(DateTime.Now.Hour))*4;
            extime.SelectedIndex = (Convert.ToInt32(DateTime.Now.Hour))*4;
            transporttype.DataSource = this.LoadTransportTypes();
            transporttype.DataBind();
            rlstatus.DataSource = this.LoadExcursionStatus();
            rlstatus.DataBind();

            if (_schoolExcursionId.HasValue)
            {
                DisplayExcursion(LoadExcursion(_schoolExcursionId.Value));
            }
        }
    }

    protected void excursiontype_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExcursionsChanged();
    }

    protected void Click_FilterByMonth(object sender, EventArgs e)
    {
        ExcursionsChanged();
    }

    #endregion

    #region Private Methods

    private void LoadResults(List<SchoolExcursion> excursions)
    {
        //Set datasource of the repeater
        //results.DataSource = excursions;
        //results.DataBind();
        //resultsreturned.Text = string.Format("Records Found: {0}", excursions.Count().ToString());
        
    }

    private void ExcursionsChanged()
    {
        if (excursiontype.SelectedItem != null)
        {
            Int32 _typeId = Convert.ToInt32(excursiontype.SelectedItem.Value);
            var _month = Convert.ToInt32(monthselector.SelectedValue);
            var _year = Convert.ToInt32(yearselector.SelectedValue);

            var ContactSearchQuery = from d in Entities.SchoolExcursion.Include("ExcursionVenue")
                                     where d.Xlk_ExcursionType.ExcursionTypeId == _typeId && d.ExcursionDate.Month == _month && d.ExcursionDate.Year == _year
                                     orderby d.ExcursionDate ascending, d.Title ascending
                                     select d;

            LoadResults(ContactSearchQuery.ToList());
        }
    }

    private void LoadHours()
    {
        var _hours = 0; var _mins = 0; string _time = ""; string _lead = ""; string _end = "";
        while (_hours != 24)
        {
            while (_mins != 60)
            {
                if (_hours < 10)
                    _lead = "0";
                else _lead = "";
                if (_mins == 0)
                    _end = "0";
                else _end = "";
                _time = _lead + Convert.ToString(_hours) + ":" + Convert.ToString(_mins) + _end;
                meettime.Items.Add(new ListItem(_time));
                extime.Items.Add(new ListItem(_time));
                _mins = _mins + 15;
            }
            _hours = _hours + 1;
            _mins = 0;
        }
        meettime.DataBind();
        extime.DataBind();
    }

    protected void Click_SaveExcursion(object sender, EventArgs e)
    {
        bool isAcceptable = false;
        string _message = String.Empty;

        SchoolExcursion newExcursion = GetExcursion();

        if (!string.IsNullOrEmpty(exdate.Text))
        {
            newExcursion.Xlk_ExcursionTypeReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_ExcursionType", "ExcursionTypeId", Convert.ToByte(excursiontype.SelectedItem.Value));
            if (chkcomp.Checked)
                newExcursion.Complimentary = true;
            else newExcursion.Complimentary = false;
            if (chkguide.Checked)
                newExcursion.PACEGuide = true;
            else newExcursion.PACEGuide = false;
            if (chkclosed.Checked)
                newExcursion.IsClosed = true;
            else newExcursion.IsClosed = false;
            newExcursion.Title = Convert.ToString(excursionvenue.SelectedItem);
            newExcursion.ExcursionVenueReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".ExcursionVenue", "ExcursionVenueId", Convert.ToInt32(excursionvenue.SelectedItem.Value));
            newExcursion.Xlk_TransportTypeReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_TransportType", "TransportTypeId", Convert.ToByte(transporttype.SelectedItem.Value));
            newExcursion.ExcursionDate = Convert.ToDateTime(exdate.Text);
            newExcursion.Description = description.Text;
            newExcursion.ExcursionTime = TimeSpan.Parse(extime.Text);
            newExcursion.MeetTime = TimeSpan.Parse(meettime.Text);
            newExcursion.MeetLocation = melocation.Text;
            if (!string.IsNullOrEmpty(condate.Text))
                newExcursion.ConfirmedDate = Convert.ToDateTime(condate.Text);
            if (ddconfirm.SelectedIndex != 0)
                newExcursion.ConfirmedBy = Convert.ToString(ddconfirm.SelectedItem);
            newExcursion.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(rlstatus.SelectedValue));

            //var imageLookup = from i in Entities.ExcursionVenue
            //                  where i.ExcursionVenueId == newExcursion.ExcursionVenue.ExcursionVenueId
            //                  select i.ImageUrl;
            //newExcursion.ExcursionImage = imageLookup.First().ToString();
            isAcceptable = true;
        }
        else
        {
            isAcceptable = false;
        }

        if (isAcceptable == true)
            SaveExcursion(newExcursion);

        else _message = "Record was not saved.";

        if (string.IsNullOrEmpty(_message))
        {
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }
    }

    private SchoolExcursion LoadExcursion(Int32 excursionId)
    {
        IQueryable<SchoolExcursion> excursionQuery = from e in Entities.SchoolExcursion.Include("Xlk_TransportType").Include("Xlk_Status")
                                                     where e.SchoolExcursionId == excursionId
                                                     select e;
        if (excursionQuery.ToList().Count() > 0)
            return excursionQuery.ToList().First();

        return null;
    }

    private void DisplayExcursion(SchoolExcursion excursion)
    {
        if (excursion != null)
        {
            exdate.Text = excursion.ExcursionDate.ToString("dd/MM/yyyy");
            excursionvenue.Items.FindByValue(excursion.ExcursionVenue.ExcursionVenueId.ToString()).Selected = true;
            extime.Text = excursion.ExcursionTime.ToString();
            description.Text = excursion.Description;
            meettime.Text = excursion.MeetTime.ToString();
            melocation.Text = excursion.MeetLocation;
            transporttype.Items.FindByValue(excursion.Xlk_TransportType.TransportTypeId.ToString()).Selected = true;
            rlstatus.Items.FindByValue(excursion.Xlk_Status.StatusId.ToString()).Selected = true;
            
            chkcomp.Checked = excursion.Complimentary;
            chkguide.Checked = excursion.PACEGuide;
            chkclosed.Checked = excursion.IsClosed;
            if (excursion.ConfirmedBy == null)
                ddconfirm.Text = "";
            else ddconfirm.Items.FindByValue(excursion.ConfirmedBy.ToString()).Selected = true;
            condate.Text = excursion.ConfirmedDate.ToString();
        }
    }

    private SchoolExcursion GetExcursion()
    {
        if (_schoolExcursionId.HasValue)
            return LoadExcursion(_schoolExcursionId.Value);

        return new SchoolExcursion();
    }

    private void SaveExcursion(SchoolExcursion excursion)
    {
        if (!_schoolExcursionId.HasValue)
            Entities.AddToSchoolExcursion(excursion);

        if (Entities.SaveChanges() > 0)
            Response.Redirect(string.Format("~/Excursions/SearchExcursionsPage.aspx"));
    }

    #endregion
}
