﻿using Pace.DataAccess.Content;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using System.Web;

/// <summary>
/// Summary description for TreeMove
/// </summary>
public class TreeMove
{

    public Branch DraggedItem{ get; set; }
    public Branch DraggedItemParent { get; set; }
    public Branch DroppedItem { get; set; }
    public Branch DroppedToParent { get; set; }
    public DropAction Action { get; set; }

    public TreeMove(string draggeditemid, string draggedparentid, string droppeditemid, string droppedparentid, string action)
    {
        Action = (DropAction)Enum.Parse(typeof(DropAction), action,true);

        DraggedItem = new Branch(draggeditemid);
        DraggedItemParent = new Branch(draggedparentid);
        DroppedItem = new Branch(droppeditemid);
        DroppedToParent = new Branch(droppedparentid);
        if (Action == DropAction.Inside)
        {
            DroppedItem = null;
            DroppedToParent = new Branch(droppeditemid);

        }

    }

    public bool IsMovePermitted
    {
        get
        {
            if (DraggedItem.BranchType == BranchType.ElementId && DroppedToParent.BranchType == BranchType.ModuleId)// only an element  can be dragged under an module
                return true;

            if (DraggedItem.BranchType == BranchType.ModuleId && DroppedToParent.BranchType == BranchType.PageId)// only a module  can be dragged under an page
                return true;

            if (DraggedItem.BranchType == BranchType.ElementId && DroppedToParent.BranchType == BranchType.ElementId) // an element can be xdragged under an element
                return true;

            return false;


        }
    }


    public bool IsSameBranch
    {
        get
        {
            return ((DraggedItemParent.BranchType == DroppedToParent.BranchType) && (DraggedItemParent.Id == DroppedToParent.Id));
        }
    }


    public bool CompleteMove()
    {

        using (Pace.BusinessLogic.Content.ContentLogic logic = new Pace.BusinessLogic.Content.ContentLogic())
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (ContentEntities entity = new ContentEntities())
                {
                    if (IsMovePermitted)
                    {

                        if (IsSameBranch) // reordering the sequence in a branch so preform a move on the item list
                        {
                            switch (DroppedToParent.BranchType)
                            {
                                case BranchType.PageId:
                                    //nothing to do here
                                    break;
                                case BranchType.ModuleId:
                                    var linkelements = new ObservableCollection<Lnk_Module_Element>(logic.GetModule(DraggedItemParent.Id).Lnk_Module_Element.OrderBy(Y => Y.Sequence));
                                    linkelements.Move(linkelements.IndexOf(linkelements.Single(s => s.ElementId == DraggedItem.Id)), linkelements.IndexOf(linkelements.Single(s => s.ElementId == DroppedItem.Id)));
                                    foreach (var element in linkelements)
                                    {
                                        entity.Attach(element);
                                        element.Sequence = linkelements.IndexOf(element) + 1;
                                    }
                                    break;
                                case BranchType.ElementId:
                                    var elementtrees = new ObservableCollection<ElementTree>(logic.GetElement(DraggedItemParent.Id).ChildElements.OrderBy(Y => Y.Sequence));
                                    elementtrees.Move(elementtrees.IndexOf(elementtrees.Single(s => s.ChildElementId == DraggedItem.Id)), elementtrees.IndexOf(elementtrees.Single(s => s.ChildElementId == DroppedItem.Id)));
                                    foreach (var element in elementtrees)
                                    {
                                        entity.Attach(element);
                                        element.Sequence = elementtrees.IndexOf(element) + 1;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            entity.SaveChanges();
                        }
                        else
                        {
                            switch (DroppedToParent.BranchType)
                            {
                                case BranchType.PageId:
                                    var module = logic.GetModule(DraggedItem.Id);
                                    module.PageId = DraggedItemParent.Id;
                                    entity.Attach(module);
                                    return entity.SaveChanges() > 0;

                                default:
                                    if (DraggedItemParent.BranchType == BranchType.ElementId)
                                    {
                                        var elements = new ObservableCollection<ElementTree>(logic.GetElement(DraggedItemParent.Id).ChildElements.OrderBy(Y => Y.Sequence));
                                        logic.DeleteElementTree(elements.Single(s => s.ChildElementId == DraggedItem.Id));
                                        elements.Remove(elements.Single(s => s.ChildElementId == DraggedItem.Id));
                                        foreach (var element in elements)
                                        {
                                            entity.Attach(element);
                                            element.Sequence = elements.IndexOf(element) + 1;
                                        }
                                        entity.SaveChanges();

                                    }
                                    if (DraggedItemParent.BranchType == BranchType.ModuleId)
                                    {

                                        var elements = new ObservableCollection<Lnk_Module_Element>(logic.GetModule(DraggedItemParent.Id).Lnk_Module_Element.OrderBy(Y => Y.Sequence));

                                        logic.DeleteLnk_Module_Element(elements.Single(s => s.ElementId == DraggedItem.Id));
                                        elements.Remove(elements.Single(s => s.ElementId == DraggedItem.Id));
                                        foreach (var element in elements)
                                        {
                                            entity.Attach(element);
                                            element.Sequence = elements.IndexOf(element) + 1;
                                        }
                                        entity.SaveChanges();



                                    }

                                    if (DroppedToParent.BranchType == BranchType.ElementId)
                                    {

                                        var elementTree = new ElementTree() { ParentElementId = DroppedToParent.Id, LinkTypeId = 1, ChildElementId = DraggedItem.Id };
                                        var elements = new ObservableCollection<ElementTree>(logic.GetElement(DroppedToParent.Id).ChildElements.OrderBy(Y => Y.Sequence));
                                        if (DroppedItem != null)
                                            elements.Insert(elements.IndexOf(elements.Single(s => s.ChildElementId == DroppedItem.Id)), elementTree);
                                        else elements.Add(elementTree);

                                        entity.AddToElementTrees(elementTree);
                                        entity.SaveChanges();

                                        foreach (var element in elements)
                                        {
                                            entity.Attach(element);
                                            element.Sequence = elements.IndexOf(element) + 1;
                                        }
                                        entity.SaveChanges();


                                    }

                                    if (DroppedToParent.BranchType == BranchType.ModuleId)
                                    {
                                        var elementTree = new Lnk_Module_Element() { ModuleId = DroppedItem.Id, ElementId = DraggedItem.Id };

                                        var elements = new ObservableCollection<Lnk_Module_Element>(logic.GetModule(DroppedToParent.Id).Lnk_Module_Element.OrderBy(Y => Y.Sequence));
                                        elements.Insert(elements.IndexOf(elements.Single(s => s.ElementId == DroppedItem.Id)), elementTree);

                                        foreach (var element in elements)
                                        {
                                            entity.Attach(element);
                                            element.Sequence = elements.IndexOf(element) + 1;
                                        }
                                        entity.SaveChanges();




                                    }


                                    break;
                            }


                        }


                    }
                }
                scope.Complete();
                return true;
            }
            
        }
        return false;

    }

}

public class Branch
{

    public int Id = 0;
    public BranchType BranchType = BranchType.Unsupported;

    public Branch(string branchid)
    {
        var branchsplit = branchid.Split('-');
        if (branchsplit.Length > 0)
        {
            Id = Convert.ToInt32(branchsplit[1]);
            this.BranchType = (BranchType)Enum.Parse(typeof(BranchType), branchsplit[0]);
        }

    }

}

public enum DropAction
{
    Inside,
    After,
    Before,
}
public enum BranchType
{
    PageId,
    ModuleId,
    ElementId,
    Unsupported,
}