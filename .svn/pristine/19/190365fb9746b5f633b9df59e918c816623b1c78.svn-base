﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Documents;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
using Pace.Common;


/// <summary>
/// Summary description for BaseDocumentPage
/// </summary>

public partial class BaseDocumentPage : BasePage
{
    public DocumentsEntities _entities { get; set; }

    public BaseDocumentPage()
    {
    }

    #region Events

    public static DocumentsEntities CreateEntity
    {
        get
        {  
            return new DocumentsEntities();
        }
    }

    public static IList<Pace.DataAccess.Documents.Server> LoadServers()
    {
        IList<Pace.DataAccess.Documents.Server> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Documents.Server>>("All_DocumentsServers", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Documents.Server>>("All_DocumentsServers", GetServers());
    }

    public IList<Pace.DataAccess.Documents.Server> LoadServer(Int32 serverId)
    {
            IQueryable<Pace.DataAccess.Documents.Server> values = (from s in LoadServers()
                                                              where s.ServerId == serverId
                                                              select s).AsQueryable();
            return values.ToList();
    }

    public IList<Xlk_Category> LoadCategories()
    {
        IList<Xlk_Category> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Category>>("All_DocumentsCategory", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Category>>("All_DocumentsCategory", GetCategories());
    }

    public IList<Xlk_Type> LoadTypes()
    {
        IList<Xlk_Type> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Type>>("All_DocumentsType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Type>>("All_DocumentsType", GetTypes());
    }

    public IList<Xlk_Type> LoadTypes(int categoryId)
    {
        IQueryable<Xlk_Type> values = (from t in LoadTypes()
                where t.Xlk_Category.CategoryId == categoryId
                select t).AsQueryable();

        return values.ToList();
    }

    public IList<Pace.DataAccess.Documents.Xlk_FileType> LoadFileTypes()
    {
        IList<Pace.DataAccess.Documents.Xlk_FileType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Documents.Xlk_FileType>>("All_DocumentsFileType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Documents.Xlk_FileType>>("All_DocumentsFileType", GetFileTypes());
    }

    public IList<Xlk_FileType> LoadFileType(string extension)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_FileType> x = (from f in LoadFileTypes()
                                         where f.Extension == extension
                                         select f).AsQueryable();

            return x.ToList();
        }
    }

    #endregion

    #region Public Methods

    public EntityKey GetServerEntityKey(string serverName)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Server> x = (from f in LoadServers()
                                   where f.UncPath.Contains(serverName)
                                   select f).AsQueryable();
            if (x.Count() > 0)
                return new EntityKey(entity.DefaultContainerName + ".Server", "ServerId", Convert.ToByte(x.First().ServerId));
        }
        return null;
    }

    public static Pace.DataAccess.Documents.Server GetServer(string serverName)
    {
        IQueryable<Pace.DataAccess.Documents.Server> x = (from f in LoadServers()
                               where f.UncPath.Contains(serverName)
                               select f).AsQueryable();
        if (x.Count() > 0)
            return x.First();

        return null;
    }

    #endregion

    #region Private Methods

    private static bool GetFilePathAndName(string fileFullfilePath, out Pace.DataAccess.Documents.Server server, out string path, out string fileName, out string message)
    {
        path = string.Empty;
        fileName = string.Empty;
        message = string.Empty;
        server = null;

        if (string.IsNullOrEmpty(fileFullfilePath))
            message = "No File path specified";


        string workPath = fileFullfilePath;
        string[] pathParts = workPath.Split('\\');

        if (pathParts.Count() <= 1)
            message = "Unable to interpret path";

        server = GetServer(pathParts[2]);

        if (server == null)
            message = "Unable to determine server";
        else
            workPath = fileFullfilePath.Replace(server.UncPath.ToUpper(), string.Empty);

        fileName = pathParts[pathParts.Length - 1];

        if (string.IsNullOrEmpty(fileName))
            message = "Unable to determine file name";
        else
            path = workPath.Replace(fileName, string.Empty);

        if (string.IsNullOrEmpty(path))
            message = "Unable to determine file path";

        return string.IsNullOrEmpty(message);

    }
    
    private IList<Xlk_FileType> GetFileTypes()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_FileType> lookupQuery =
                 from p in entity.Xlk_FileType
                 select p;

            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_Type> GetTypes()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_Type> lookupQuery =
                 from p in entity.Xlk_Type.Include("Xlk_Category")
                 select p;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_Category> GetCategories()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_Category> lookupQuery =
                 from p in entity.Xlk_Category
                 select p;
            return lookupQuery.ToList();
        }
    }

    private static IList<Server> GetServers()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Server> lookupQuery =
                 from p in entity.Server
                 select p;
            return lookupQuery.ToList();
        }
    }

    #endregion


    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseDocumentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("DocId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, int> entitykeys)
    {
        try
        {
            var page = new BaseDocumentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveDocument(Documents newObject)
    {
        Documents newDocuments = new Documents();
        string _path, _filename;
        Pace.DataAccess.Documents.Server _server;
        string _message = String.Empty;

        try
        {
            using (DocumentsEntities entity = new DocumentsEntities())
            {
                if (newObject.DocId > 0)
                    newDocuments = (from e in entity.Documents where e.DocId == newObject.DocId select e).FirstOrDefault();

                newDocuments.Description = newObject.Description;
   
                newDocuments.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);
                newDocuments.Xlk_TypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Type", newObject.Xlk_Type);

                if (GetFilePathAndName(newObject.FileName, out _server, out _path, out _filename, out _message))
                {
                    newDocuments.Path = _path;
                    newDocuments.FileName = _filename;
                    newDocuments.Server = _server;
               }

                if (newDocuments.DocId == 0)
                    entity.AddToDocuments(newDocuments);

                entity.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    #endregion
}

