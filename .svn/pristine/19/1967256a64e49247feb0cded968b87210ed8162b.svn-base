﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Excursion;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;

public partial class Excursions_SearchExcursionsPage : BaseExcursionPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            excursiontype.DataSource = this.LoadExcursionTypes();
            excursiontype.DataBind();
            monthselector.SelectedIndex = Convert.ToInt32(DateTime.Now.Month);
            yearselector.SelectedIndex = Convert.ToInt32(DateTime.Now.Year) - 2011;

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
        }
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_SearchExcursionBookings();
                break;
            case "current":
                Click_LoadCurrentExcursionBookings();
                break;
            case "thismonth":
                Click_LoadThisMonthExcursionBookings();
                break;
            case "nextmonth":
                Click_LoadNextMonthExcursionBookings();
                break;
            case "future":
                Click_LoadFutureExcursionBookings();
                break;
            default:
                Click_LoadAllExcursionBookings();
                break;
        }
    }

    protected void lookupExcursion(object sender, EventArgs e)
    {
        Click_SearchExcursionBookings();
    }

    #endregion

    #region Private Methods

    private void Click_SearchExcursionBookings()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (excursiontype.SelectedItem != null)
            {
                Int32 _typeId = Convert.ToInt32(excursiontype.SelectedItem.Value);
                var _month = Convert.ToInt32(monthselector.SelectedValue);
                var _year = Convert.ToInt32(yearselector.SelectedValue);

                var ContactSearchQuery = from d in entity.Booking
                                         where d.Excursion.Xlk_ExcursionType.ExcursionTypeId == _typeId && d.ExcursionDate.Month == _month && d.ExcursionDate.Year == _year
                                         orderby d.ExcursionDate ascending, d.Title ascending
                                         select d;

                LoadResults(ContactSearchQuery.ToList());
            }
        }
    }

    private void Click_LoadCurrentExcursionBookings()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            DateTime _start = DateTime.Now.AddDays(-1);
            DateTime _end = DateTime.Now.AddDays(7);
            var contactSearchQuery = from d in entity.Booking
                                     where d.ExcursionDate >= _start && d.ExcursionDate <= _end
                                     orderby d.ExcursionDate ascending, d.Title ascending
                                     select d;

            LoadResults(contactSearchQuery.ToList());
        }
    }

    private void Click_LoadThisMonthExcursionBookings()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (excursiontype.SelectedItem != null)
            {

                int _end = DateTime.Now.Month;

                var ContactSearchQuery = from d in entity.Booking
                                         where d.ExcursionDate.Month == _end
                                         orderby d.ExcursionDate ascending, d.Title ascending
                                         select d;

                LoadResults(ContactSearchQuery.ToList());
            }
        }
    }

    private void Click_LoadNextMonthExcursionBookings()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (excursiontype.SelectedItem != null)
            {

                int _end = DateTime.Now.AddMonths(1).Month;

                var ContactSearchQuery = from d in entity.Booking
                                         where d.ExcursionDate.Month == _end
                                         orderby d.ExcursionDate ascending, d.Title ascending
                                         select d;

                LoadResults(ContactSearchQuery.ToList());
            }
        }
    }

    private void Click_LoadFutureExcursionBookings()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (excursiontype.SelectedItem != null)
            {

                DateTime _end = DateTime.Now.AddDays(7);

                var ContactSearchQuery = from d in entity.Booking
                                         where d.ExcursionDate > _end
                                         orderby d.ExcursionDate ascending, d.Title ascending
                                         select d;

                LoadResults(ContactSearchQuery.ToList());
            }
        }
    }

    private void Click_LoadAllExcursionBookings()
    {
        using (ExcursionEntities entity = new ExcursionEntities())
        {
            if (excursiontype.SelectedItem != null)
            {

                var ContactSearchQuery = from d in entity.Booking
                                         orderby d.ExcursionDate ascending, d.Title ascending
                                         select d;

                LoadResults(ContactSearchQuery.ToList());
            }
        }
    }

    private void LoadResults(List<Booking> excursions)
    {
        //Set datasource of the repeater
        results.DataSource = excursions;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", excursions.Count().ToString());
    }

    #endregion
}
