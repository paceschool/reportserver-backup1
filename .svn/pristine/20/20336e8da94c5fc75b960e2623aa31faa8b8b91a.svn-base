﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using Pace.DataAccess.Content;
using System.ServiceModel;
using Pace.Common;
using System.Transactions;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for BaseEnrollmentPage
/// </summary>
public partial class BaseWebContentPage : BasePage
{
    public ContentEntities _entities { get; set; }

    public BaseWebContentPage()
    {
    }

    #region Events

    public static ContentEntities CreateEntity
    {
        get
        {
            return new ContentEntities();
        }
    }


    public IList<Pace.DataAccess.Content.Xlk_AttributeType> LoadAttributeTypes()
    {
        IList<Pace.DataAccess.Content.Xlk_AttributeType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Content.Xlk_AttributeType>>("AllWebContentAttributeType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Content.Xlk_AttributeType>>("AllWebContentAttributeType", GetAttributeTypes());
    }


    public IList<Pace.DataAccess.Content.Xlk_ElementType> LoadElementTypes()
    {
        IList<Pace.DataAccess.Content.Xlk_ElementType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Content.Xlk_ElementType>>("AllWebContentElementType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Content.Xlk_ElementType>>("AllWebContentElementType", GetElementTypes());
    }


    public IList<Pace.DataAccess.Content.Xlk_LinkType> LoadLinkTypes()
    {
        IList<Pace.DataAccess.Content.Xlk_LinkType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Pace.DataAccess.Content.Xlk_LinkType>>("AllWebContentLinkType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Pace.DataAccess.Content.Xlk_LinkType>>("AllWebContentLinkType", GetLinkTypes());
    }

    public IList<Xlk_LanguageCode> LoadLanguageCodes()
    {
        List<Xlk_LanguageCode> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_LanguageCode>>("AllWebContentLanguageCode", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_LanguageCode>>("AllWebContentLanguageCode", GetLanguageCodes().ToList());
    }



    #endregion


    #region Private Methods

    private IList<Xlk_LanguageCode> GetLanguageCodes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_LanguageCode> lookupQuery =
                from c in entity.Xlk_LanguageCode
                select c;
            return lookupQuery.ToList();
        }
    }
    private IList<Xlk_AttributeType> GetAttributeTypes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_AttributeType> lookupQuery =
                from a in entity.Xlk_AttributeType
                select a;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_ElementType> GetElementTypes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_ElementType> lookupQuery =
                from a in entity.Xlk_ElementType
                select a;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_LinkType> GetLinkTypes()
    {
        using (ContentEntities entity = new ContentEntities())
        {
            IQueryable<Xlk_LinkType> lookupQuery =
                from b in entity.Xlk_LinkType
                select b;
            return lookupQuery.ToList();
        }
    }


    #endregion



    #region Javascript Enabled Methods

    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopUpFilters(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder().AppendFormat("<span><a title=\"Add Filter\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../WebContent/WebContentPage.aspx','WebContent/AddModuleFilterControl.ascx',{{'ModuleId':{0}}})\"><img src=\"../Content/img/actions/add.png\">Add New Filter</a></span><table  id=\"box-table-a\"><thead><tr><th>Tag</th><th>Actions</th></tr></thead><tbody>",id);

            using (ContentEntities entity = new ContentEntities())
            {
                var _filters = (from s in entity.ModuleFilters
                                   where s.ModuleId == id
                                   select new { s.TagId, s.Tag}).ToList();

                if (_filters != null && _filters.Count() > 0)
                {

                    foreach (var filter in _filters)
                    {
                        sb.AppendFormat("<tr><td>{2}</td><td><a title=\"Edit Filter\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../WebContent/WebContentPage.aspx','WebContent/AddModuleFilterControl.ascx',{{'ModuleId':{0}, 'TagId' :{1}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Item\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('WebContentPage.aspx','ModuleFilter',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", id, filter.TagId, filter.Tag);
                    }
                }
                else
                    sb.Append("<tr><td colspan=\"4\"></td></tr>");

                sb.Append("</tbody></table>");
                return sb.ToString();
            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopUpAttributes(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder().AppendFormat("<span><a title=\"Add Attribute\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../WebContent/WebContentPage.aspx','WebContent/AddAttributesControl.ascx',{{'ElementId':{0}}})\"><img src=\"../Content/img/actions/add.png\">Add New Attribute</a></span><table  id=\"box-table-a\"><thead><tr><th>Type</th><th>Value</th><th>Actions</th></tr></thead><tbody>",id);

            using (ContentEntities entity = new ContentEntities())
            {
                var _attributes = (from s in entity.Attributes
                                   where s.ElementId == id
                                   select new { s.AttributeTypeId, s.Xlk_AttributeType.Description, s.AttributeValue }).ToList();

                if (_attributes != null && _attributes.Count() > 0)
                {

                    foreach (var attribute in _attributes)
                    {
                        sb.AppendFormat("<tr><td>{2}</td><td>{3}</td><td><a title=\"Edit Note\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','../WebContent/WebContentPage.aspx','WebContent/AddAttributesControl.ascx',{{'ElementId':{0}, 'AttributeTypeId' :{1}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Item\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('WebContentPage.aspx','Attribute',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", id, attribute.AttributeTypeId, attribute.Description, attribute.AttributeValue);
                    }
                }
                else
                    sb.Append("<tr><td colspan=\"4\"></td></tr>");

                sb.Append("</tbody></table>");
                return sb.ToString();
            }

        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveContent(Module newObject)
    {
        try
        {
            Module newWebContent = new Module();
            using (ContentEntities entity = new ContentEntities())
            {
                if (newObject.ModuleId > 0)
                    newWebContent = (from c in entity.Modules where c.ModuleId == newObject.ModuleId select c).FirstOrDefault();
                else
                    entity.AddToModules(newWebContent);


                newWebContent.Description = newObject.Description;
                newWebContent.PageURL = newObject.PageURL;
                newWebContent.Title = newObject.Title;
                newWebContent.Xlk_LanguageCodeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_LanguageCode", newObject.Xlk_LanguageCode);


                entity.SaveChanges();

                if (newObject.ModuleId == 0)
                {
                    if (newWebContent.ModuleId > 0)
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("Could not find the new WebContent Identifier, please search for this WebContent before trying to add again!"));
                }
            }

            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveElement(Element newObject)
    {
        try
        {
            Element newWebContent = new Element();
            using (ContentEntities entity = new ContentEntities())
            {
                if (newObject.ElementId > 0)
                    newWebContent = (from c in entity.Elements where c.ElementId == newObject.ElementId select c).FirstOrDefault();
                else
                    entity.AddToElements(newWebContent);

                newWebContent.ElementValue = newObject.ElementValue;
                newWebContent.Xlk_ElementTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_ElementType", newObject.Xlk_ElementType);


                entity.SaveChanges();


                if (newObject.ElementId == 0)
                {
                    if (newWebContent.ElementId > 0)
                    {
                        try
                        {
                            if (newObject.ParentModuleId > 0)
                            {
                                if (newWebContent.Lnk_Module_Element == null)
                                    newWebContent.Lnk_Module_Element = new System.Data.Objects.DataClasses.EntityCollection<Lnk_Module_Element>();

                                newWebContent.Lnk_Module_Element.Add(new Lnk_Module_Element() { ModuleId = newObject.ParentModuleId, ElementId = newWebContent.ElementId });


                                entity.SaveChanges();
                            }

                            if (newObject.ParentElementId > 0)
                            {
                                if (newWebContent.ParentElements == null)
                                    newWebContent.ParentElements = new System.Data.Objects.DataClasses.EntityCollection<ElementTree>();


                                newWebContent.ParentElements.Add(new ElementTree {  ParentElementId = newObject.ParentElementId, ChildElementId = newWebContent.ElementId,LinkTypeId =1 });

                                entity.SaveChanges();
                            }

                            return new AjaxResponse();
                        }

                        catch (Exception ex)
                        {
                            return new AjaxResponse(ex);
                        }

                    }
                    else
                        return new AjaxResponse(new Exception("Could not find the new WebContent Identifier, please search for this WebContent before trying to add again!"));
                }
            }

           return new AjaxResponse();

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveAttribute(Pace.DataAccess.Content.Attribute newObject)
    {
        try
        {
            try
            {
                Pace.DataAccess.Content.Attribute newWebContent = new Pace.DataAccess.Content.Attribute();

                using (ContentEntities entity = new ContentEntities())
                {
                    if (newObject.ElementId > 0 && newObject.Xlk_AttributeType.AttributeTypeId > 0)
                        newWebContent = (from c in entity.Attributes where c.ElementId == newObject.ElementId && c.AttributeTypeId == newObject.Xlk_AttributeType.AttributeTypeId select c).FirstOrDefault();
                    if (newWebContent == null)
                    {
                        newWebContent = new Pace.DataAccess.Content.Attribute();
                        entity.AddToAttributes(newWebContent);
                    }

                    newWebContent.ElementId = newObject.ElementId;
                    newWebContent.AttributeValue = newObject.AttributeValue;
                    newWebContent.Xlk_AttributeTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_AttributeType", newObject.Xlk_AttributeType);


                    int i = entity.SaveChanges();

                    if (i > 0)
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("Could not find the new attribute Identifier, please search for this attribute before trying to add again!"));

                }

            }
            catch (Exception ex)
            {
                return new AjaxResponse(ex);
            }

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveModuleFilter(Pace.DataAccess.Content.ModuleFilter newObject)
    {
        try
        {
            try
            {
                Pace.DataAccess.Content.ModuleFilter newWebContent = new Pace.DataAccess.Content.ModuleFilter();

                using (ContentEntities entity = new ContentEntities())
                {
                    if (newObject.ModuleId > 0 && newObject.TagId > 0)
                        newWebContent = (from c in entity.ModuleFilters where c.ModuleId == newObject.ModuleId && c.TagId == newObject.TagId select c).FirstOrDefault();
                    else
                        entity.AddToModuleFilters(newWebContent);

                    newWebContent.ModuleId = newObject.ModuleId;
                    newWebContent.TagId = newObject.TagId;
                    newWebContent.Tag = newObject.Tag;



                    int i = entity.SaveChanges();

                    if (i > 0)
                        return new AjaxResponse();
                    else
                        return new AjaxResponse(new Exception("Could not find the new module filter Identifier, please search for this module filter before trying to add again!"));

                }

            }
            catch (Exception ex)
            {
                return new AjaxResponse(ex);
            }

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }
    

    #endregion

}
