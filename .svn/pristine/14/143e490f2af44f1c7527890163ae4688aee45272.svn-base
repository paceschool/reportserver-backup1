﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Enrollment;
using System.Data.Objects;
using System.Data;

public partial class AddStudentPage : BaseEnrollmentPage
{
    private Int32? _studentId;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["StudentId"]))
            _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!Page.IsPostBack)
        {
            ViewState["RefUrl"] = Request.UrlReferrer.ToString();

            newnationality.DataSource = this.LoadNationalities();
            newnationality.DataBind();
            newagent.DataSource = this.LoadAgents();
            newagent.DataBind();
            newstatus.DataSource = this.LoadStatus();
            newstatus.DataBind();
            errorcontainer.Visible = false;
            gender.DataSource = this.LoadGender();
            gender.DataBind();
            managetype.DataSource = this.LoadProgrammeTypes();
            managetype.DataBind();
            managelesson.DataSource = this.LoadLessonBlocks();
            managelesson.DataBind();

            if (_studentId.HasValue)
            {
               DisplayStudent(LoadStudent(_studentId.Value));
            }
        }
    }

    #region private methods

    protected void Click_ClearFields(object sender, EventArgs e)
    {
        newfname.Text = ""; newsname.Text = "";
        newnationality.SelectedIndex = 0; newagent.SelectedIndex = 0; newstatus.SelectedIndex = 0;
        newarrdate.Text = ""; newdepdate.Text = ""; newplacement.Text = "";
        errorcontainer.Visible = false;
    }

    protected void Click_Save(object sender, EventArgs e)
    {
        string _message = ""; errorcontainer.Visible = false;

        if (!string.IsNullOrEmpty(newfname.Text) || !string.IsNullOrEmpty(newsname.Text) || !string.IsNullOrEmpty(newarrdate.Text) ||
            !string.IsNullOrEmpty(newdepdate.Text))
        {
            Student _student = GetStudent();

            _student.FirstName = newfname.Text;
            _student.SurName = newsname.Text;
            _student.Xlk_GenderReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Gender", "GenderId", Convert.ToByte(gender.SelectedItem.Value));
            _student.DateOfBirth = Convert.ToDateTime(dob.Text);
            _student.Xlk_NationalityReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
            _student.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
            _student.DepartureDate = Convert.ToDateTime(newdepdate.Text);
            if (!string.IsNullOrEmpty(newplacement.Text))
                _student.PlacementTestResult = Convert.ToDecimal(newplacement.Text);
            _student.AgencyReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
            _student.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));

            SaveStudent(_student);

        }
        else
        {
            _message = "Not all fields are complete. Please check.";
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }


    }

    private Student LoadStudent(Int32 studentId)
    {

        IQueryable<Student> studentQuery = from s in Entities.Student.Include("StudentInvoice").Include("Xlk_ProgrammeType").Include("Enrollment")
                                           join t in Entities.Xlk_ProgrammeType on s.StudentId equals t.ProgrammeTypeId
                                           where s.StudentId == studentId
                                           select s;

        if (studentQuery.ToList().Count() > 0)
            return studentQuery.ToList().First();

        return null;
    }

    private void DisplayStudent(Student student)
    {
        if (student != null)
        {
            newfname.Text = student.FirstName;
            newsname.Text = student.SurName;
            dob.Text = (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("dd/MM/yyyy") : string.Empty;
            newarrdate.Text = (student.ArrivalDate.HasValue) ? student.ArrivalDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            newdepdate.Text = (student.DepartureDate.HasValue) ? student.DepartureDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            newplacement.Text = student.PlacementTestResult.ToString();
            gender.Items.FindByValue(student.Xlk_Gender.GenderId.ToString()).Selected = true;
            newagent.Items.FindByValue(student.Agency.AgencyId.ToString()).Selected = true;
            newstatus.Items.FindByValue(student.Xlk_Status.StatusId.ToString()).Selected = true;
            newnationality.Items.FindByValue(student.Xlk_Nationality.NationalityId.ToString()).Selected = true;
        }
    }

    private Student GetStudent()
    {
        if (_studentId.HasValue)
            return LoadStudent(_studentId.Value);

        return new Student();
    }

    private void SaveStudent(Student student)
    {
        if (!_studentId.HasValue)
            Entities.AddToStudent(student);

        if (Entities.SaveChanges() > 0)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }

    }

  #endregion  
}
