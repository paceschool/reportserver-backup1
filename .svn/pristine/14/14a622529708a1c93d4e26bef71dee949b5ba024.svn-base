﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Payments;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;

public partial class VouchersPage : BasePaymentPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            //Set datasource of the Category DropDown
            suppliertype.DataSource = this.LoadSupplierTypes();
            suppliertype.DataBind();
            //Set datasource of PaymentMethod DropDown
            paymenttype.DataSource = this.LoadPaymentMethods();
            paymenttype.DataBind();
            //Populate Suppliers DropDown on load based on initial SupplierType value
        }
    }

    //Update Suppliers DropDown
    protected void suppliertype_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSuppliersToDropDown();
    }
    protected void suppliertype_DataBound(object sender, EventArgs e)
    {
        LoadSuppliersToDropDown();
    }
    //Clear Fields
    protected void clear_Click(object sender, EventArgs e)
    {
        paceref.Text = ""; supplierref.Text = ""; TextBox1.Text = "";
        TextBox2.Text = ""; TextBox3.Text = ""; TextBox4.Text = "";
        TextBox7.Text = ""; expectedcost.Text = ""; suppliertype.SelectedIndex = 0;
        LoadSuppliersToDropDown();
        
    }

    //Save record and Print if required
    protected void Save_Click(object sender, EventArgs e)
    {
        Voucher newVoucher;
        string _message = String.Empty;
        bool IsAcceptable = true;

        if (String.IsNullOrEmpty(paceref.Text))
            { _message = "No Title entered."; IsAcceptable = false; }
        else if (String.IsNullOrEmpty(TextBox3.Text))
        { _message = "A number of Students must be entered"; IsAcceptable = false; }
        else
        {
            IsAcceptable = true;
        }
        if (IsAcceptable == true)
        {
            newVoucher = new Voucher();
            newVoucher.OurRef = paceref.Text;
            newVoucher.SupplierRef = supplierref.Text;
            newVoucher.Title = TextBox1.Text;
            newVoucher.Description = TextBox2.Text;
            newVoucher.DateCreated = DateTime.Now.Date;
            newVoucher.VoucherDate = Convert.ToDateTime(voucherDate.Text);

            int vhour = Convert.ToInt32((voucherTime.Text).Substring(0, 2));
            int vmin = Convert.ToInt32((voucherTime.Text).Substring(3, 2));
            newVoucher.VoucherTime = new TimeSpan(vhour, vmin, 0);

            newVoucher.NumberOfStudents = Convert.ToInt32(TextBox3.Text);
            newVoucher.NumberOfLeaders = Convert.ToInt32(TextBox4.Text);
            newVoucher.ExpectedCost = Convert.ToDecimal(expectedcost.Text);
            newVoucher.PrepaidReference = TextBox7.Text;
            newVoucher.SupplierReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Supplier", "SupplierId", Convert.ToInt16(supplier.SelectedItem.Value));
            
            Entities.AddToVoucher(newVoucher);
        

            if(Entities.SaveChanges()>0)
            {
                _message = "Saved Successfully!";
                Response.Redirect(string.Format("PrintPage.aspx?VoucherId={0}", newVoucher.VoucherId));
                //PostBackOptions
            }
        }
        else _message = "Record not saved.";

        MessageBoxShow(this, _message);
        
    }

    protected void supplier_DataBound(object sender, EventArgs e)
    {
        SupplierChanged();

    }
    protected void supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        SupplierChanged();
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        CreateRef();
    }
    protected void voucherDate_TextChanged(object sender, EventArgs e)
    {
        CreateRef();
    }
    #endregion

    #region Private Methods

    private void CreateRef()
    {
        DateTime _useDate = (!string.IsNullOrEmpty(voucherDate.Text)) ? Convert.ToDateTime(voucherDate.Text) :DateTime.Now;
        Supplier _supplier = LoadSupplier(Convert.ToInt32(supplier.SelectedItem.Value));
        if (_supplier != null)
        {
            if (TextBox1.Text == "" || TextBox1.Text.Length < 3)
                paceref.Text = string.Format("PACE-{0}-{1}-{2}", _supplier.VoucherRef, _useDate.Year
                    + _useDate.DayOfYear + "0", supplier.SelectedValue);
            else
                paceref.Text = string.Format("PACE-{0}-{1}-{2}-{3}", _supplier.VoucherRef, TextBox1.Text.Substring(0, 3).ToUpper(),_useDate.Year
                    + _useDate.DayOfYear , supplier.SelectedValue);
        }
    }
    //Populate Suppliers DropDown based on value of SupplierTypes DropDown
    private void LoadSuppliersToDropDown()
    {
        ListItem _supplier = suppliertype.SelectedItem;
        supplier.DataSource = this.LoadSuppliers(Convert.ToInt32(_supplier.Value));
        supplier.DataBind();
    }

    //Error MessageBox
    private void MessageBoxShow(Page page, string message)
    {
        Literal ltr = new Literal();
        ltr.Text = @"<script type='text/javascript'> alert('" + message + "') </script>";
        page.Controls.Add(ltr);
    }

    private void SupplierChanged()
    {
        if (supplier.SelectedItem != null)
        {
            Int32 _typeId = Convert.ToInt32(supplier.SelectedItem.Value);
            Int32 _suppTypeId = Convert.ToInt32(suppliertype.SelectedItem.Value);

            var contactSearchQuery = from d in Entities.Voucher.Include("Supplier.Xlk_SupplierType")
                                     where d.Supplier.SupplierId == _typeId
                                     orderby d.Title ascending
                                     select d;

            LoadResults(contactSearchQuery.ToList());
            CreateRef();
        }

    }

    private void LoadResults(List<Voucher> details)
    {
        //Set datasource of the repeater
        results.DataSource = details;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", details.Count().ToString());
    }

    

    #endregion
}
