﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using Pace.DataAccess.Group;
using Accommodation = Pace.DataAccess.Accommodation;

public partial class Accommodation_PlanAccommodationPage : BaseGroupPage
{
    protected string _action = "assign";

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["Action"]))
            _action = Request["Action"];

        if (!Page.IsPostBack)
        {
            startDate.Text = DateTime.Now.ToString("dd/MM/yy");
            endDate.Text = DateTime.Now.AddDays(14).ToString("dd/MM/yy");
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                familyStatusList.DataSource = (from s in entity.Xlk_Status
                                               select s).ToList();
            }

            familyStatusList.DataBind();
            familyStatusList.Items[0].Selected = true;
        }


        foreach (ListItem item in familyStatusList.Items)
        {
            item.Attributes.Add("statusid", item.Value);// = usersInRole.Find(u => u.UserName == item.Text).InRole;
        }


    }


    protected void filterDate_TextChanged(object sender, EventArgs e)
    {

    }

    #endregion
    
    #region Private Methods

    protected string BuildGroupList()
    {
        string familiesOptionList = string.Empty;
        List<Pace.DataAccess.Accommodation.Family> families = null;
        List<Xlk_ConfirmationAction> actions = null;
        DateTime _startDate = DateTime.Parse(startDate.Text);
        DateTime _endDate = DateTime.Parse(endDate.Text);

        try
        {
            StringBuilder sb = new StringBuilder();

            int[] statusList =  (from item in familyStatusList.Items.Cast<ListItem>() 
                               where item.Selected 
                               select int.Parse(item.Value)).ToArray();

            families = LoadFamilies(new int[] { 1, 5 },statusList);


            using (GroupEntities entity = new GroupEntities())
            {
                actions = (from a in entity.Xlk_ConfirmationAction select a).ToList();

                var studentQuery = (from g in entity.Group
                                    where g.ArrivalDate >= _startDate && g.ArrivalDate <= _endDate && g.Xlk_Status.StatusId != 3 && g.CampusId == GetCampusId
                                    orderby g.ArrivalDate
                                    select new { g.GroupId, g.GroupName, g.ArrivalDate, g.DepartureDate, g.NoOfLeaders, g.NoOfStudents, AssignedStudents = (from s in g.Student where s.Hostings.Any(h=>(!h.Cancelled)) && s.Hostings.Count > 0  select s).Count(), Student = (from s in g.Student orderby s.Xlk_StudentType.StudentTypeId descending, s.FirstName, s.SurName select new { s.StudentId, s.GroupingBlockId, s.FirstName, s.SurName, NoteCount = s.StudentNotes.Count, StudentType = s.Xlk_StudentType.Description, HostingDetails = (from h in s.Hostings where (!h.Cancelled) select new { Hosting = h, h.HostingConfirmations, h.Family }).FirstOrDefault() }) });

                foreach (var group in studentQuery.ToList())
                {
                    List<Pace.DataAccess.Accommodation.Family> groupfamilies = LoadReservedFamilies(group.GroupId, new int[] { 1, 5 }, statusList);
                    familiesOptionList = (groupfamilies != null && groupfamilies.Count > 0) ? BuildFamilyDropDownOptions(groupfamilies) : BuildFamilyDropDownOptions(families);

                    sb.AppendFormat("<div><h2 id=\"{0}\">{1}</h2><div><table id=\"box-table-a\"><thead><tr><th>Type</th><th>Name</th><th>Family</th><th>Action</th></tr></thead><tbody>", group.GroupId, string.Format("<table class=\"navtdst\"><tr><td>{0}</td><td>Dates: {1} - {2}</td><td>.     Leaders: {3}</td><td>Students: {4}</td><td>Unassigned {5}</td></tr></table>", group.GroupName, group.ArrivalDate.ToString("dd/MMM/yyyy"), group.DepartureDate.ToString("dd/MMM/yyyy"), (group.NoOfLeaders.HasValue) ? group.NoOfLeaders.Value.ToString() : "0", (group.NoOfStudents.HasValue) ? group.NoOfStudents.Value.ToString() : "0", (group.NoOfLeaders + group.NoOfStudents) - group.AssignedStudents));

                    foreach (var studentGroup in group.Student.Where(y=>y.GroupingBlockId.HasValue).GroupBy(x=>x.GroupingBlockId))
                    {
                        sb.AppendFormat("<tr id=\"{0}-{1}\">", group.GroupId, studentGroup.Key);

                        string studentnames = string.Empty, studenttypes = string.Empty;
                        Family family = (from s in studentGroup where s.HostingDetails != null select s.HostingDetails.Family).Distinct().FirstOrDefault();
                        List<HostingConfirmation> confirmations = new List<HostingConfirmation>();

                        foreach (var student in studentGroup)
                        {
                            if (student.HostingDetails != null && student.HostingDetails.HostingConfirmations != null && student.HostingDetails.HostingConfirmations.Count > 0)
                                confirmations.AddRange(student.HostingDetails.HostingConfirmations.ToList());
                            studenttypes = string.Format("{0} <br /> {1}",studenttypes, student.StudentType);
                            studentnames = string.Format("{0} <br /> {1}&nbsp;{2}",studentnames, BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", CreateName(student.FirstName, student.SurName)), (student.NoteCount > 0) ? PopUpLink("id", student.StudentId, "~/Enrollments/ViewStudentPage.aspx", "<img src=\"../Content/img/actions/exclamation-button.png\">", "PopupStudentNotes") : string.Empty);
                        }

                        sb.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td>", studenttypes, studentnames, (studentGroup.Any(y => y.HostingDetails == null)) ? BuildFamilyDropDown(studentGroup.Key.Value, group.GroupId, familiesOptionList) : string.Format("<a class=\"DeleteHosting\" onClick=\"deleteHostingBlockAction({0},{2})\"></a>{1}", group.GroupId, BaseEnrollmentPage.CreateName(family.FirstName, family.SurName), studentGroup.Key), (family == null) ? string.Empty : BuildActions(group.GroupId, studentGroup.Key, actions, confirmations));
                        sb.Append("</tr>");
                    }

                    foreach (var student in group.Student.Where(y => !y.GroupingBlockId.HasValue))
                    {
                        sb.AppendFormat("<tr id=\"{0}\"><td>{1}</td><td>{2}&nbsp;{5}</td><td>{3}</td><td>{4}</td></tr>", student.StudentId, student.StudentType, BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", CreateName(student.FirstName, student.SurName)), (student.HostingDetails == null) ? BuildFamilyIndividualDropDown(student.StudentId, group.GroupId, familiesOptionList) : string.Format("<a class=\"DeleteHosting\" onClick=\"deleteHostingAction({0},{2})\"></a>{1}", student.StudentId, BaseEnrollmentPage.CreateName(student.HostingDetails.Family.FirstName, student.HostingDetails.Family.SurName), student.HostingDetails.Hosting.HostingId), (student.HostingDetails == null) ? string.Empty : BuildActions(student.StudentId, student.HostingDetails.Hosting.HostingId, actions, student.HostingDetails.HostingConfirmations.ToList()), (student.NoteCount > 0) ? PopUpLink("id", student.StudentId, "~/Enrollments/ViewStudentPage.aspx", "<img src=\"../Content/img/actions/exclamation-button.png\">", "PopupStudentNotes") : string.Empty);
                    }

                    sb.Append("</table></div></div>");
                }
            }
            return sb.ToString();
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }


    private static List<Pace.DataAccess.Accommodation.Family> LoadReservedFamilies(int groupid, int[] familyTypeId, int[] statusId)
    {

        using (GroupEntities gentity = new GroupEntities())
        {
            var familyIdList = (from g in gentity.ReservedFamilies
                                where !g.Family.Hostings.Any(h => h.Student.Group.GroupId == groupid) && g.GroupId == groupid
                                select g.FamilyId).ToList();

            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                var familyQuery = from f in entity.Families
                                  where familyIdList.Contains(f.FamilyId) && f.CampusId == GetCampusId
                                  orderby f.SurName, f.FirstName
                                  select f;
                if (familyQuery != null && familyQuery.Count() > 0)
                return familyQuery.ToList();
            }
        }
        return null;

    }

    private static List<Pace.DataAccess.Accommodation.Family> LoadFamilies(int[] familyTypeId, int[] statusId)
    {

        using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
        {
            var familyQuery = from f in entity.Families
                              where familyTypeId.Contains(f.Xlk_FamilyType.FamilyTypeId) && statusId.Contains(f.Xlk_Status.StatusId) && f.CampusId == GetCampusId
                              orderby f.SurName, f.FirstName
                              select f;

            return familyQuery.ToList();
        }

    }

    private static string BuildFamilyDropDown(int groupingBlockId,int groupId, string families)
    {
        StringBuilder sb = new StringBuilder(string.Format("<select id=\"{0}\" onClick=\"loadFamilies(this,{0},{1})\" onChange=\"assignFamilyBlock(this,{0},{1})\">", groupId, groupingBlockId));
        sb.Append("<option \"selected\">Please Select</option>");
        sb.Append("</select>");
        return sb.ToString();
    }

    private static string BuildFamilyIndividualDropDown(int studentId, int groupId, string families)
    {
        StringBuilder sb = new StringBuilder(string.Format("<select id=\"{0}\" onClick=\"loadFamilies(this,{1},{0})\"  onChange=\"assignFamily(this,{0})\">", studentId, groupId));
        sb.Append(families);
        sb.Append("</select>");
        return sb.ToString();
    }

    private static string BuildFamilyDropDownOptions(List<Pace.DataAccess.Accommodation.Family> families)
    {

        StringBuilder sb = new StringBuilder();
        sb.Append("<option \"selected\">Please Select</option>");

        foreach (Pace.DataAccess.Accommodation.Family family in families)
        {
            sb.AppendFormat("<option value={0}>{1} {2}</option>", family.FamilyId, family.SurName, family.FirstName);
        }
        return sb.ToString();

    }

    private static string BuildActions(int studentid,long hostingId, List<Xlk_ConfirmationAction> actions, List<HostingConfirmation> confirmations)
    {
        StringBuilder sb = new StringBuilder().AppendFormat("<span id=\"{0}actions\">", hostingId);

        foreach (var action in actions)
        {
            if ((from c in confirmations where c.ConfirmationActionId == action.ConfirmationActionId && c.ConfirmedDate.HasValue && c.ConfirmedBy.HasValue select c).Count() == 0)
                sb.AppendFormat("<a class=\"{3}\" title=\"{4}\" onClick=\"confirmHostingAction({0},{1},{2})\"></a>",studentid, hostingId, action.ConfirmationActionId,action.Description.Replace(" ", string.Empty),action.Description);
        }

        return sb.ToString();
    }

    private static string BuildActions(int groupid, short? groupingBlockId, List<Xlk_ConfirmationAction> actions, List<HostingConfirmation> confirmations)
    {
        StringBuilder sb = new StringBuilder().AppendFormat("<span id=\"{0}actions\">", groupingBlockId);

        foreach (var action in actions)
        {
            if ((from c in confirmations where c.ConfirmationActionId == action.ConfirmationActionId && c.ConfirmedDate.HasValue && c.ConfirmedBy.HasValue select c).Count() == 0)
                sb.AppendFormat("<a class=\"{3}\" title=\"{4}\" onClick=\"confirmHostingBlockAction({0},{1},{2})\"></a>", groupid, groupingBlockId, action.ConfirmationActionId, action.Description.Replace(" ", string.Empty), action.Description);
        }

        return sb.ToString();
    }

    protected static string BuildStudentRow(int studentId, int[] statuslist)
    {
        string familiesOptionList = string.Empty;
        List<Pace.DataAccess.Accommodation.Family> families = null;
        List<Xlk_ConfirmationAction> actions = null;

        families = LoadFamilies(new int[] { 1, 5 }, statuslist);

        familiesOptionList = BuildFamilyDropDownOptions(families);

        using (GroupEntities entity = new GroupEntities())
        {
            actions = (from a in entity.Xlk_ConfirmationAction select a).ToList();

            var student = (from s in entity.Student.Include("Hosting")
                           where s.StudentId == studentId && s.CampusId == GetCampusId
                           select new { s.StudentId, s.FirstName, s.SurName,s.Group.GroupId, StudentType = s.Xlk_StudentType.Description, NoteCount = s.StudentNotes.Count, HostingDetails = (from h in s.Hostings select new { Hosting = h, h.HostingConfirmations, h.Family }).FirstOrDefault() }).FirstOrDefault();


            return string.Format("<td>{0}</td><td>{4}&nbsp;{1}</td><td>{2}</td><td>{3}</td>", student.StudentType, BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", CreateName(student.FirstName, student.SurName)), (student.HostingDetails == null) ? BuildFamilyIndividualDropDown(student.StudentId, student.GroupId,familiesOptionList) : string.Format("<a class=\"DeleteHosting\" onClick=\"deleteHostingAction({0},{2})\"></a>{1}", student.StudentId, BaseEnrollmentPage.CreateName(student.HostingDetails.Family.FirstName, student.HostingDetails.Family.SurName), student.HostingDetails.Hosting.HostingId), (student.HostingDetails == null) ? string.Empty : BuildActions(studentId, student.HostingDetails.Hosting.HostingId, actions, student.HostingDetails.HostingConfirmations.ToList()), (student.NoteCount > 0) ? PopUpLink("id", student.StudentId, "~/Enrollments/ViewStudentPage.aspx", "<img src=\"../Content/img/actions/exclamation-button.png\">", "PopupStudentNotes") : string.Empty);
        }

    }

    protected static string BuildStudentRow(int groupid, short groupingblockid, int[] statuslist)
    {
        string familiesOptionList = string.Empty;
        List<Pace.DataAccess.Accommodation.Family> families = null;
        List<Xlk_ConfirmationAction> actions = null;

        families = LoadFamilies(new int[] { 1, 5 }, statuslist);


        using (GroupEntities entity = new GroupEntities())
        {

            List<Pace.DataAccess.Accommodation.Family> groupfamilies = LoadReservedFamilies(groupid, new int[] { 1, 5 }, statuslist);
            familiesOptionList = (groupfamilies != null && groupfamilies.Count > 0) ? BuildFamilyDropDownOptions(groupfamilies) : BuildFamilyDropDownOptions(families);

            actions = (from a in entity.Xlk_ConfirmationAction select a).ToList();

            var students = (from s in entity.Student
                            where s.GroupingBlockId == groupingblockid && s.Group.GroupId == groupid && s.CampusId == GetCampusId
                            select new { s.StudentId, s.GroupingBlockId, s.FirstName, s.SurName, StudentType = s.Xlk_StudentType.Description, NoteCount = s.StudentNotes.Count, HostingDetails = (from h in s.Hostings select new { Hosting = h, h.HostingConfirmations, h.Family }).FirstOrDefault() }).ToList();



            foreach (var studentGroup in students.Where(y => y.GroupingBlockId.HasValue).GroupBy(x => x.GroupingBlockId))
            {

                string studentnames = string.Empty, studenttypes = string.Empty;
                Family family = (from s in studentGroup where s.HostingDetails != null select s.HostingDetails.Family).Distinct().FirstOrDefault();
                List<HostingConfirmation> confirmations = new List<HostingConfirmation>();

                foreach (var student in studentGroup)
                {
                    if (student.HostingDetails != null && student.HostingDetails.HostingConfirmations != null)
                        confirmations.AddRange(student.HostingDetails.HostingConfirmations.ToList());
                    studenttypes = string.Format("{0} <br /> {1}", studenttypes, student.StudentType);
                    studentnames = string.Format("{0} <br /> {1}&nbsp;{2}", studentnames, BasePage.StudentLink(student.StudentId, "~/Enrollments/ViewStudentPage.aspx", CreateName(student.FirstName, student.SurName)), (student.NoteCount > 0) ? PopUpLink("id", student.StudentId, "~/Enrollments/ViewStudentPage.aspx", "<img src=\"../Content/img/actions/exclamation-button.png\">", "PopupStudentNotes") : string.Empty);
                }

                string trs =  string.Format("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td>", studenttypes, studentnames, (studentGroup.Any(y => y.HostingDetails == null)) ? BuildFamilyDropDown(studentGroup.Key.Value, groupid, familiesOptionList) : string.Format("<a class=\"DeleteHosting\" onClick=\"deleteHostingBlockAction({0},{2})\"></a>{1}", groupid, BaseEnrollmentPage.CreateName(family.FirstName, family.SurName), studentGroup.Key), (family == null) ? string.Empty : BuildActions(groupid, studentGroup.Key, actions, confirmations));
                return trs;
            }


            return string.Empty;
        }

    }

    #endregion

    #region Javascript Enabled Methods


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> LoadFamiliesDynamically(int groupid, short groupingblockid, int[] statuslist)
    {
        try
        {
            List<Pace.DataAccess.Accommodation.Family> groupfamilies = LoadReservedFamilies(groupid, new int[] { 1, 5 }, statuslist);
            return new AjaxResponse<string>((groupfamilies != null && groupfamilies.Count > 0) ? BuildFamilyDropDownOptions(groupfamilies) : BuildFamilyDropDownOptions(LoadFamilies(new int[] { 1, 5 }, statuslist)));

        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> AssignStudentToFamily(int familyid, int studentid, int[] statuslist)
    {
       
        try
        {
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                Pace.DataAccess.Accommodation.Hosting currenthosting = (from h in entity.Hostings
                                                                        where h.Student.StudentId == studentid && h.ArrivalDate > DateTime.Now && !(h.Cancelled)
                                                                    select h).FirstOrDefault();

                if (currenthosting != null)
                {
                    currenthosting.FamilyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Families", "FamilyId", familyid);
                    //set the confirmed date and details to null

                    if (entity.SaveChanges() > 0)
                        return new AjaxResponse<string>(BuildStudentRow(studentid, statuslist));
                }
                else
                { 
                    Pace.DataAccess.Accommodation.Hosting hosting = new Pace.DataAccess.Accommodation.Hosting();
                    using (GroupEntities gentity = new GroupEntities())
                    {
                        Group group = (from s in gentity.Student
                                       where s.StudentId == studentid
                                       select s.Group).FirstOrDefault();



                        hosting.FamilyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Families", "FamilyId", familyid);
                        hosting.StudentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Student", "StudentId", studentid);
                        hosting.ArrivalDate = group.ArrivalDate;
                        hosting.DepartureDate = group.DepartureDate;
                    }
                    entity.AddToHostings(hosting);

                    if (entity.SaveChanges() > 0)
                        return new AjaxResponse<string>(BuildStudentRow(studentid, statuslist));
                }
            }
           return new AjaxResponse<string>(string.Empty);
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> AssignBlockToFamily(int familyid, int groupid, short groupingblockid, int[] statuslist)
    {

        try
        {
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                Pace.DataAccess.Accommodation.Hosting currenthosting = (from h in entity.Hostings
                                                                        where h.Student.GroupId == groupid && h.Student.GroupingBlockId == groupingblockid && h.ArrivalDate > DateTime.Now && !(h.Cancelled)
                                                                        select h).FirstOrDefault();

                if (currenthosting != null)
                {

                    using (GroupEntities gentity = new GroupEntities())
                    {
                        if (gentity.DeleteGroupBlockHosting(groupid, groupingblockid).Count() > 0)
                            return new AjaxResponse<string>(new Exception("Cannot unallocate a student in this group; please contact system administrator"));
                    }
                }

                using (GroupEntities gentity = new GroupEntities())
                {
                    var group = (from g in gentity.Group
                                 from s in g.Student
                                 where g.GroupId == groupid && s.GroupingBlockId == groupingblockid
                                 select new { g.ArrivalDate, g.DepartureDate, s.StudentId }).ToList();

                    foreach (var item in group)
                    {
                        Pace.DataAccess.Accommodation.Hosting hosting = new Pace.DataAccess.Accommodation.Hosting();
                        hosting.FamilyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Families", "FamilyId", familyid);
                        hosting.StudentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Student", "StudentId", item.StudentId);
                        hosting.ArrivalDate = item.ArrivalDate;
                        hosting.DepartureDate = item.DepartureDate;
                        entity.AddToHostings(hosting);
                    }
                }

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse<string>(BuildStudentRow(groupid, groupingblockid, statuslist));
            }

            return new AjaxResponse<string>(new Exception("No updates made please contact administrator!"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> ConfirmHostingAction(int studentid,int[] statuslist,long hostingid, byte actionid)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                List<HostingConfirmation> confirmations = (from c in entity.HostingConfirmation where c.HostingId == hostingid select c).ToList();

                HostingConfirmation current = (from h in confirmations where h.Xlk_ConfirmationAction.ConfirmationActionId == actionid select h).FirstOrDefault();

                if (current == null)
                {
                    current = HostingConfirmation.CreateHostingConfirmation(hostingid, actionid);
                    current.HostingReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Hostings", "HostingId", hostingid);
                    current.Xlk_ConfirmationActionReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_ConfirmationAction", "ConfirmationActionId", actionid);
                    entity.AddToHostingConfirmation(current);
                    if (entity.SaveChanges() > 0)
                        confirmations.Add(current);
                }

                current.ConfirmedDate = DateTime.Now;
                current.ConfirmedBy = 1; // we dont have security yet so defalut to 1

                if (entity.SaveChanges() > 0)
                {
                    //return BuildActions(hostingid, (from a in Entities.Xlk_ConfirmationAction select a).ToList(), confirmations);
                    return new AjaxResponse<string>(BuildStudentRow(studentid, statuslist));
                }
            }
            return new AjaxResponse<string>(new Exception("There was a problem saving, please contact admin"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> ConfirmHostingBlockAction( int groupid, short groupingblockid, int[] statuslist, byte actionid)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                List<HostingConfirmation> confirmations = (from c in entity.HostingConfirmation
                                                           where c.Hosting.Student.Group.GroupId == groupid && c.Hosting.Student.GroupingBlockId == groupingblockid
                                                           && c.Xlk_ConfirmationAction.ConfirmationActionId == actionid
                                                           select c).ToList();

                if (confirmations != null && confirmations.Count > 0)
                {
                    foreach (var confirmation in confirmations)
                    {
                        entity.DeleteObject(confirmation);
                    }
                    entity.SaveChanges();
                }

                var hostings = (from h in entity.Hostings
                                where h.Student.Group.GroupId == groupid && h.Student.GroupingBlockId == groupingblockid
                                select h).ToList();

                foreach (Hosting hosting in hostings)
                {
                    HostingConfirmation current = HostingConfirmation.CreateHostingConfirmation(hosting.HostingId, actionid);
                    current.HostingReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Hostings", "HostingId", current.HostingId);
                    current.Xlk_ConfirmationActionReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_ConfirmationAction", "ConfirmationActionId", actionid);
                    current.ConfirmedDate = DateTime.Now;
                    current.ConfirmedBy = GetCurrentUser().UserId;
                    entity.AddToHostingConfirmation(current);
                }

                if (entity.SaveChanges() > 0)
                {
                    return new AjaxResponse<string>(BuildStudentRow(groupid, groupingblockid, statuslist));
                }

            }
            return new AjaxResponse<string>(new Exception("Could not complete the save, plerase contact admin"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> DeleteHostingBlock(int groupid, short groupingblockid, int[] statuslist)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                if (entity.DeleteGroupBlockHosting(groupid, groupingblockid).Count() == 0)
                    return new AjaxResponse<string>(BuildStudentRow(groupid, groupingblockid, statuslist));
            }
            return new AjaxResponse<string>(new Exception("Nothing deleted, please confirm the action"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> DeleteHosting(int studentid, int hostingid,int[] statuslist)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                if (entity.DeleteHosting(hostingid).Count() == 0)
                    return new AjaxResponse<string>(BuildStudentRow(studentid, statuslist));
            }
            return new AjaxResponse<string>(new Exception("Nothing deleted, please confirm the action"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }


    #endregion
        
}
