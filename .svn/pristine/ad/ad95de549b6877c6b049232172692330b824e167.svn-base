﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Finance;
using System.Data.Objects;
using System.Data;
using System.Text;
using System.Globalization;
using System.Data.Objects.SqlClient;

public partial class Finance_FinancePage : BaseFinancePage
{
    protected Int32 SageRef = 0;
    protected Byte DataType = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MonthSelect.SelectedIndex = Convert.ToInt32(DateTime.Now.Month);

            AgentSelect.DataSource = LoadAgents();
            AgentSelect.DataBind();

            TypeSelect.DataSource = LoadTypes();
            TypeSelect.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
        }
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadRecords();
                break;
            case "thismonth":
                Click_LoadThisMonth();
                break;
            case "lastmonth":
                Click_LoadLastMonth();
                break;
            case "thisyear":
                Click_LoadThisYear();
                break;
            default:
                Click_LoadAllRecords();
                break;
        }
    }

    protected void lookupRecords(object sender, EventArgs e)
    {
        Click_LoadRecords();
    }

    protected void Click_LoadRecords()
    {
        int agencyid = Convert.ToInt32(AgentSelect.SelectedItem.Value);
        int typeid = Convert.ToInt32(TypeSelect.SelectedItem.Value);
        int monthid = Convert.ToInt32(MonthSelect.SelectedItem.Value);
        string _record = searchstring.Text;
        DateTime start; DateTime end;
        if (FromDate.Text != "" && ToDate.Text != "")
        {
            start = Convert.ToDateTime(FromDate.Text);
            end = Convert.ToDateTime(ToDate.Text);
        }
        else
        {
            start = DateTime.Now; end = DateTime.Now;
        }

        using (FinanceEntities entity = new FinanceEntities())
        {
            var recordQuery = from r in entity.SageDatas.Include("Xlk_SageDataType").Include("Agency")
                              select r;

            if (agencyid > 0)
                recordQuery = recordQuery.Where(x => x.AgencyId == agencyid);

            if (typeid > 0)
                recordQuery = recordQuery.Where(x => x.Xlk_SageDataType.SageDataTypeId == typeid);

            if (monthid > 0 && FromDate.Text == "" && ToDate.Text == "")
                recordQuery = recordQuery.Where(x => x.DateRaised.Value.Month == monthid && x.DateRaised.Value.Year == DateTime.Now.Year);
            else if ((monthid > 0 && FromDate.Text != "") || (monthid > 0 && ToDate.Text != ""))
                recordQuery = recordQuery.Where(x => x.AgencyId == 0);

            if (FromDate.Text != "" && ToDate.Text != "")
                recordQuery = recordQuery.Where(x => x.DateRaised > start && x.DateRaised < end);

            if (!string.IsNullOrEmpty(_record))
                recordQuery = recordQuery.Where(x => (x.Agency.SageRef.Contains(_record) || (x.Agency.Name.Contains(_record))));

            recordQuery = recordQuery.OrderBy(x => x.DateRaised).OrderByDescending(x => x.DateRaised);
            LoadResults(recordQuery.ToList());

            AgentSelect.SelectedIndex = 0; MonthSelect.SelectedIndex = 0; TypeSelect.SelectedIndex = 0;
            FromDate.Text = ""; ToDate.Text = ""; searchstring.Focus();
        }
    }

    protected void Click_LoadThisMonth()
    {
        using (FinanceEntities entity = new FinanceEntities())
        {
            var recordQuery = from r in entity.SageDatas.Include("Xlk_SageDataType").Include("Agency")
                              where r.DateRaised.Value.Month == DateTime.Now.Month
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    protected void Click_LoadLastMonth()
    {
        DateTime last = DateTime.Now.AddMonths(-1);
        using (FinanceEntities entity = new FinanceEntities())
        {
            var recordQuery = from r in entity.SageDatas.Include("Xlk_SageDataType").Include("Agency")
                              where r.DateRaised.Value.Month == last.Month
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    protected void Click_LoadThisYear()
    {
        DateTime year = DateTime.Now;
        using (FinanceEntities entity = new FinanceEntities())
        {
            var recordQuery = from r in entity.SageDatas.Include("Xlk_SageDataType").Include("Agency")
                              where r.DateRaised.Value.Year == year.Year
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    protected void Click_LoadAllRecords()
    {
        using (FinanceEntities entity = new FinanceEntities())
        {
            var recordQuery = from r in entity.SageDatas.Include("Xlk_SageDataType").Include("Agency")
                              select r;

            LoadResults(recordQuery.ToList());
        }
    }

    private void LoadResults(List<SageData> data)
    {
            results.DataSource = data;
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", (data != null) ? data.Count().ToString() : "0");
    }

    #region Lists

    protected IList<Pace.DataAccess.Agent.Agency> LoadAgents()
    {
        using (Pace.DataAccess.Agent.AgentEntities entity = new Pace.DataAccess.Agent.AgentEntities())
        {
            var query = from a in entity.Agencies
                        orderby a.Name
                        select a;

            return query.ToList();
        }
    }

    protected IList<Xlk_SageDataType> LoadTypes()
    {
        using (FinanceEntities entity = new FinanceEntities())
        {
            var query = from t in entity.Xlk_SageDataType
                        orderby t.Description
                        select t;

            return query.ToList();
        }
    }

    #endregion
}