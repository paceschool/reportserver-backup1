﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Accommodation;
using System.Data;
using System.Data.Objects;
using PaceManager.Enrollment;

public partial class ManageAccommodationPage : BaseAccommodationPage
{
    private Int32? _familyId;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["FamilyId"]))
            _familyId = Convert.ToInt32(Request["FamilyId"]);

        if (!Page.IsPostBack)
        {
            listtype.DataSource = this.LoadFamilyTypes();
            listtype.DataBind();

            liststatus.DataSource = this.LoadStatus();
            liststatus.DataBind();

            listzone.DataSource = this.LoadZones();
            listzone.DataBind();

            listpaymenttype.DataSource = this.LoadPaymentMethods();
            listpaymenttype.DataBind();

            if (_familyId.HasValue)
                DisplayFamily(LoadFamily(new AccomodationEntities(),_familyId.Value));

            if (Request.UrlReferrer != null)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();


        }
    }

    protected void Save_Click(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            Family _family = GetFamily(entity);

            _family.FirstName = famfirstname.Text;
            _family.SurName = famsurname.Text;
            _family.Address = famaddress.Text;
            _family.LandLine = famtel.Text;
            _family.Mobile = fammobile.Text;
            _family.EmailAddress = famemail.Text;
            _family.SageRef = sageref.Text;
            _family.Comments = famcomment.Text;
            _family.Description = famdescription.Text;
            _family.GeoCode = famgeocode.Text;
            _family.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(liststatus.SelectedItem.Value));
            _family.Xlk_FamilyTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_FamilyType", "FamilyTypeId", Convert.ToByte(listtype.SelectedItem.Value));
            _family.Xlk_PaymentMethodReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_PaymentMethod", "PaymentMethodId", Convert.ToByte(listtype.SelectedItem.Value));
           
            if (listzone.SelectedIndex > 0)
                _family.Xlk_ZoneReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Zone", "ZoneId", Convert.ToInt32(listzone.SelectedItem.Value));

            SaveFamily(entity, _family);
        }
    }

    private Family LoadFamily(AccomodationEntities entity,Int32 familyId)
    {
        IQueryable<Family> familyQuery = from f in entity.Family.Include("Xlk_FamilyType").Include("Xlk_Zone")
                                             where f.FamilyId == familyId
                                             select f;

            if (familyQuery.ToList().Count() > 0)
                return familyQuery.ToList().First();

            return null;
        
    }

    private void DisplayFamily(Family family)
    {
        if (family != null)
        {
            famfirstname.Text = family.FirstName;
            famsurname.Text = family.SurName;
            famaddress.Text = family.Address;
            famtel.Text = family.LandLine;
            fammobile.Text = family.Mobile;
            famemail.Text = family.EmailAddress;
            sageref.Text = family.SageRef;

            liststatus.Items.FindByValue(family.Xlk_Status.StatusId.ToString()).Selected = true;
            listtype.Items.FindByValue(family.Xlk_FamilyType.FamilyTypeId.ToString()).Selected = true;
            listzone.Items.FindByValue(family.Xlk_Zone.ZoneId.ToString()).Selected = true;
        }
    }

    private Family GetFamily(AccomodationEntities entity)
    {
        if (_familyId.HasValue)
            return LoadFamily(entity,_familyId.Value);

        return new Family();
    }

    private void SaveFamily(AccomodationEntities entity, Family family)
    {

        if (!_familyId.HasValue)
            entity.AddToFamily(family);

        if (entity.SaveChanges() > 0)
                Response.Redirect(string.Format("ViewAccommodationPage.aspx?FamilyId=" + family.FamilyId));

    }

}
