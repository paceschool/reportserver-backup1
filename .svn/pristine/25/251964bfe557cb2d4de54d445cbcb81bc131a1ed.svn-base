﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Data;
using System.Text;
using System.Data.Objects.DataClasses;
using System.Web.Services;
using System.Web.Script.Services;
using Accommodation = Pace.DataAccess.Accommodation;

public partial class ViewGroupPage :BaseGroupPage 
{
    protected Int32 _groupId, _agencyId;
    protected DateTime[] period = new DateTime[40];
    protected string[] dayboxes = new string[500];
    protected Int32 grDays = 0;
    protected string groupCost;
    protected bool GroupStatus;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["GroupId"]))
            _groupId = Convert.ToInt32(Request["GroupId"]);
        if (!string.IsNullOrEmpty(Request["AgencyId"]))
            _agencyId = Convert.ToInt32(Request["AgencyId"]);

        if (!Page.IsPostBack)
        {
            newnationality.DataSource = this.LoadNationalities();
            newnationality.DataBind();
            newagent.DataSource = this.LoadAgents();
            newagent.DataBind();
            newstatus.DataSource = this.LoadStatus();
            newstatus.DataBind();
            errorcontainer.Visible = false;

            listcampus.DataSource = BasePage.LoadCampus;
            listcampus.DataBind();

            DisplayGroup(LoadGroup(new GroupEntities(), _groupId));
        }
    }

    protected Group LoadGroup(GroupEntities entity,Int32 groupId)// Load Group if passed from GroupsPage
    {
        IQueryable<Group> groupQuery = from g in entity.Group.Include("Enrollment").Include("Xlk_Status").Include("Agency").Include("Xlk_Nationality")
                                       where g.GroupId == groupId
                                       select g;
        if (groupQuery.ToList().Count() > 0)
            return groupQuery.ToList().First();

        return null;
    }

    private void DisplayGroup(Group group)// Display loaded Group details
    {
        if (group != null)
        {
            GroupId.Text = group.GroupId.ToString();
            GroupName.Text = group.GroupName;
            GroupArrival.Text = string.Format("{0:D}", group.ArrivalDate);
            GroupDeparture.Text = string.Format("{0:D}", group.DepartureDate);
            Weeks.Text = CalculateTimePeriod(group.ArrivalDate,group.DepartureDate,'W');
            nosts.Text = group.NoOfStudents.ToString();
            newstudentno.Text = group.NoOfStudents.ToString();
            nolds.Text = group.NoOfLeaders.ToString();
            newleaderno.Text = group.NoOfLeaders.ToString();
            noclass.Text = group.NoOfClasses.ToString();

            if (group.IsClosed)
            {
                closed.Text = "YES";
                GroupStatus = true;
            }
            else
            {
                closed.Text = "NO";
                GroupStatus = false;
            }

            if (group.CampusId.HasValue)
                listcampus.Items.FindByValue(group.CampusId.ToString()).Selected = true;

            newname.Text = group.GroupName;
            newarrdate.Text = group.ArrivalDate.ToString();
            newdepdate.Text = group.DepartureDate.ToString();
            newnationality.Items.FindByValue(group.Xlk_Nationality.NationalityId.ToString()).Selected = true;
            newagent.Items.FindByValue(group.Agency.AgencyId.ToString()).Selected = true;
            newstatus.Items.FindByValue(group.Xlk_Status.StatusId.ToString()).Selected = true;
            chkIsClosed.Checked = Convert.ToBoolean(group.IsClosed);
            newleaderno.Text = Convert.ToString(group.NoOfLeaders);
            newstudentno.Text = Convert.ToString(group.NoOfStudents);
            newleadermobile.Text = group.LeaderMobile;
            newarrinfo.InnerText = group.ArrivalInfo;
            newdepinfo.InnerText = group.DepartureInfo;
            newclassno.Text = group.NoOfClasses.ToString();
            groupcurfew.Text = Convert.ToString(group.GroupCurfew);
            if (group.BusTickets.HasValue)
                ticketsassigned.Checked = group.BusTickets.Value;
            else
                ticketsassigned.Checked = false;

            if (group.Xlk_Status.StatusId == 3)
                canc.Visible = true;
            else
                canc.Visible = false;
        }
    }

    protected void Click_Save(object sender, EventArgs e)
    {
        string _message = ""; errorcontainer.Visible = false;

        if (!string.IsNullOrEmpty(newname.Text) && !string.IsNullOrEmpty(newarrdate.Text) &&
            !string.IsNullOrEmpty(newdepdate.Text) && newnationality.SelectedIndex != 0)
        {
            using (GroupEntities entity = new GroupEntities())
            {
                Group _group = GetGroup(entity);

                _group.GroupName = newname.Text;
                _group.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
                _group.DepartureDate = Convert.ToDateTime(newdepdate.Text);
                _group.Xlk_NationalityReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
                _group.AgencyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
                _group.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));
                _group.IsClosed = Convert.ToBoolean(chkIsClosed.Checked);
                _group.CampusId = Convert.ToInt16(listcampus.SelectedItem.Value);

                if (string.IsNullOrEmpty(newleaderno.Text))
                    _group.NoOfLeaders = 0;
                else
                    _group.NoOfLeaders = Convert.ToInt16(newleaderno.Text);
                if (string.IsNullOrEmpty(newstudentno.Text))
                    _group.NoOfStudents = 0;
                else
                    _group.NoOfStudents = Convert.ToInt16(newstudentno.Text);

                if (string.IsNullOrEmpty(newclassno.Text))
                    _group.NoOfClasses = 0;
                else
                    _group.NoOfClasses = Convert.ToInt16(newclassno.Text);

                _group.LeaderMobile = newleadermobile.Text;
                _group.ArrivalInfo = newarrinfo.InnerText;
                _group.DepartureInfo = newdepinfo.InnerText;


                if (!string.IsNullOrEmpty(groupcurfew.Text))
                    _group.GroupCurfew = TimeSpan.Parse(groupcurfew.Text);
                else
                    _group.GroupCurfew = new TimeSpan(10, 0, 0); ;

                _group.BusTickets = ticketsassigned.Checked;

                SaveGroup(entity,_group);
            }
        }
        else
        {
            _message = "Not all fields are complete.  Please check.";
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }
    }

    private Group GetGroup(GroupEntities entity)
    {
        return LoadGroup(entity,_groupId);
    }


    private void SaveGroup(GroupEntities entity,Group group)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayGroup(LoadGroup(entity,_groupId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }

    protected string LoadStatusList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">Any</option>");


        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.StatusId, item.Description);
        }

        return sb.ToString();

    }

    protected string GetRoomTypeList()
    {
        StringBuilder sb = new StringBuilder("<option value=\"0\">Any</option>");

        foreach (Pace.DataAccess.Accommodation.Xlk_RoomType item in BaseAccommodationPage.LoadRoomTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.RoomTypeId, item.Description);
        }

        return sb.ToString();

    }

    protected static string CreateClass(object hosting)
    {
        if (hosting != null && ((EntityCollection<Pace.DataAccess.Accommodation.Hosting>)hosting).Count > 0)
            return "assigned";

        return "draggable ui-state-default";
    }

    protected static string CreateFamilyHostingList(Dictionary<int, DateTime> timeperiods, object hosting)
    {

        decimal dimension = (85 / timeperiods.Count);
        StringBuilder sb = new StringBuilder();

        foreach (var item in timeperiods)
        {
            int i = 0;
            if (hosting != null && ((EntityCollection<Pace.DataAccess.Accommodation.Hosting>)hosting).Count > 0)
                i = ((EntityCollection<Pace.DataAccess.Accommodation.Hosting>)hosting).Where(h => (h.ArrivalDate <= item.Value && item.Value <= h.DepartureDate)).Count();
            if (i > 0)
                sb.AppendFormat("<span class=\"index\" style=\"margin-left:{0}%;width:{1}%;{3}\">{2}</span>", item.Key * dimension, dimension, i, (i > 1) ? (i >= 3) ? "background-color:#3C849F" : "background-color:#95AEB8" : string.Empty);


        }
        sb.Append("<span class=\"count\">2 students</span>");
        return sb.ToString();
    }

    private static IQueryable<Pace.DataAccess.Accommodation.Family> FilterFamilies(Pace.DataAccess.Group.Group group, NameValue[] details)
    {
        using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
        {
            IQueryable<Pace.DataAccess.Accommodation.Family> familyQuery = from f in entity.Families.Include("Hosting")
                                                                       from h in f.Hostings
                                                                       where f.Xlk_FamilyType.FamilyTypeId == 2 && h.DepartureDate >= DateTime.Now
                                                                       select f;

            if (!details.AnyValues())
                return familyQuery;


            if (!string.IsNullOrEmpty(details.Form<string>("familyname")))
            {
                string filterstring = details.Form<string>("familyname");
                familyQuery = familyQuery.Where(f => f.FirstName.StartsWith(filterstring) || f.SurName.StartsWith(filterstring));
            }

            if (details.Form<int>("excludenationality") == 1)
                familyQuery = familyQuery.Where(f => f.Hostings.Where(h => h.Student.Xlk_Nationality.NationalityId != group.Xlk_Nationality.NationalityId).Count() > 0);

            if (details.Form<int>("ensuite") == 1)
                familyQuery = familyQuery.Where(f => f.Rooms.Where(r => r.Ensuite == true).Count() > 0);

            if (details.Form<int>("statusid") > 0)
            {
                int statusid = details.Form<int>("statusid");

                familyQuery = familyQuery.Where(f => f.Xlk_Status.StatusId == statusid);
            }

            if (details.Form<int>("roomtypeid") > 0)
            {
                int roomtypeid = details.Form<int>("roomtypeid");
                familyQuery = familyQuery.Where(f => f.Rooms.Where(r => r.Xlk_RoomType.RoomTypeId == roomtypeid).Count() > 0);
            }

            if (details.Form<int>("zoneid") > 0)
            {
                int zoneid = details.Form<int>("zoneid");
                familyQuery = familyQuery.Where(f => f.Xlk_Zone.ZoneId == zoneid);
            }

            return familyQuery;
        }


    }

    protected static Dictionary<int, DateTime> LoadGroupDetails(int id, out Pace.DataAccess.Group.Group group)
    {
        group = null;

        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            IQueryable<Pace.DataAccess.Group.Group> groupQuery = from s in entity.Group.Include("Xlk_Nationality")
                                                             where s.GroupId == id
                                                             select s;
            Dictionary<int, DateTime> timeperiods = new Dictionary<int, DateTime>();

            if (groupQuery.Count() > 0)
            {
                group = groupQuery.First();

                int noofdays = (group.DepartureDate - group.ArrivalDate).Days;

                for (int i = 0; i < noofdays; i++)
                {
                    timeperiods.Add(i, group.ArrivalDate.AddDays(i));
                }
            }
            return timeperiods;
        }

    }

    #region Javascript Events

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadGroupDraggableStudents(int id)
    {
        try
        {
            StringBuilder groupString = new StringBuilder("<table class=\"box-table-a\"><thead ><tr ><th scope=\"col\" ><label>  Group ID  </label ></th ><th scope=\"col\" ><label>  Name  </label ></th ><th scope=\"col\" ><label>  Arrival Date  </label ></th ><th scope=\"col\" ><label>  Departure Date  </label ></th ><th scope=\"col\" ><label>  #Weeks  </label ></th ></tr></thead><tbody>");
            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                Pace.DataAccess.Group.Group group = (from g in entity.Group
                                                 where g.GroupId == id
                                                 select g).FirstOrDefault();
                if (group != null)
                {
                    groupString.AppendFormat("<tr ><td style=\"text-align: left\">{0}</td ><td style=\"text-align: left\" >{1}</td ><td style=\"text-align: left\" >{2}</td ><td style=\"text-align: left\" >{3}</td ><td style=\"text-align: left\" >{4}</td>  </tr>", group.GroupId, group.GroupName, group.ArrivalDate.ToString("D"), group.DepartureDate.ToString("D"), CalculateTimePeriod(group.ArrivalDate, group.DepartureDate, 'W'));
                }
                else
                {
                    groupString.Append("<li>There are no students in this group</li>");
                }
            }

            groupString.Append("</tbody></table>");

            StringBuilder studentsString = new StringBuilder("<ul>");
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                IQueryable<Pace.DataAccess.Accommodation.Student> hostingQuery = from s in entity.Student.Include("Hosting")
                                                                             where s.GroupId.Value == id
                                                                             select s;

                if (hostingQuery.Count() > 0)
                {
                    foreach (Pace.DataAccess.Accommodation.Student student in hostingQuery.ToList())
                    {
                        studentsString.AppendFormat("<li id=\"{2}\" class=\"{0}\">{1}</li>", CreateClass(student.Hostings), BasePage.CreateName(student.FirstName, student.SurName), student.StudentId);
                    }
                }
                else
                {
                    studentsString.Append("<li>There are no students in this group</li>");
                }
            }

            studentsString.Append("</ul>");

            return studentsString.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message ;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadFamilies(int id, NameValue[] details) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        Pace.DataAccess.Group.Group group;
        try
        {

            StringBuilder sb = new StringBuilder("<ul class=\"chartlist\">");
            Dictionary<int, DateTime> timeperiods = LoadGroupDetails(id, out group);

            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
            #region FilterFamilies

                var familyQuery = from f in entity.Families
                                  from h in f.Hostings
                                  where f.Xlk_FamilyType.FamilyTypeId == 2 && h.DepartureDate >= DateTime.Now
                                  select new { Family = f, f.Hostings, f.Rooms };


                if (!string.IsNullOrEmpty(details.Form<string>("familyname")))
                {
                    string filterstring = details.Form<string>("familyname");
                    familyQuery = familyQuery.Where(f => f.Family.FirstName.StartsWith(filterstring) || f.Family.SurName.StartsWith(filterstring));
                }

                if (details.Form<int>("excludenationality") == 1)
                    familyQuery = familyQuery.Where(f => f.Hostings.Where(h => h.Student.Xlk_Nationality.NationalityId != group.Xlk_Nationality.NationalityId).Count() > 0);

                if (details.Form<int>("ensuite") == 1)
                    familyQuery = familyQuery.Where(f => f.Family.Rooms.Where(r => r.Ensuite == true).Count() > 0);

                if (details.Form<int>("statusid") > 0)
                {
                    int statusid = details.Form<int>("statusid");

                    familyQuery = familyQuery.Where(f => f.Family.Xlk_Status.StatusId == statusid);
                }

                if (details.Form<int>("roomtypeid") > 0)
                {
                    int roomtypeid = details.Form<int>("roomtypeid");
                    familyQuery = familyQuery.Where(f => f.Family.Rooms.Where(r => r.Xlk_RoomType.RoomTypeId == roomtypeid).Count() > 0);
                }

                //int zoneid = details.Form<int>("zoneid");
                //{
                //    int zoneid = details.Form<int>("zoneid");
                //familyQuery = familyQuery.Where(f => f.Xlk_Zone.ZoneId == zoneid);
                //}

            #endregion

                if (familyQuery.Count() > 0)
                {
                    foreach (var family in familyQuery.Take(50).ToList())
                    {
                        sb.AppendFormat("<li id=\"{0}\" class=\"droppable\">", family.Family.FamilyId);
                        sb.AppendFormat("<a href=\"ViewAccommodationPage.aspx?FamilyId={1}\">{0}</a> ", BasePage.CreateName(family.Family.FirstName, family.Family.SurName), family.Family.FamilyId);

                        sb.Append(CreateFamilyHostingList(timeperiods, family.Hostings));
                        sb.Append("</li>");
                    }
                }
                else
                {
                    sb.Append("<li></li>");
                }

                sb.Append("</ul>");
            }
            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool AssignStudentToFamily(int id, int familyid, int studentid) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        Pace.DataAccess.Group.Group group;
        try
        {
            Dictionary<int, DateTime> timeperiods = LoadGroupDetails(id, out group);

            if (group != null)
            {
                using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
                {
                    Pace.DataAccess.Accommodation.Hosting hosting = new Pace.DataAccess.Accommodation.Hosting();
                    hosting.ArrivalDate = group.ArrivalDate;
                    hosting.DepartureDate = group.DepartureDate;
                    hosting.FamilyReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Family", "FamilyId", familyid);
                    hosting.StudentReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Student", "StudentId", studentid);

                    entity.AddToHostings(hosting);

                    return (entity.SaveChanges() > 0);
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
        
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool ProcessFamilyPayments(int id) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                int? result = entity.DeleteFamilyPayments(id).FirstOrDefault();

                if (result.HasValue && result.Value == 0)
                {
                    List<Int64?> ids = entity.CreateFamilyPayments(id).ToList();
                    return (ids.Count > 0);
                }
                return false;
            }
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    
    #endregion

}
