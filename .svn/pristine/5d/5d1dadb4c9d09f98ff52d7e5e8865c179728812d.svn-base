﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Documents;
using System.Web.SessionState;
using System.Web.Services;
using System.Data;
using System.Text;
using System.Web.Script.Services;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core;

public partial class DocumentsPage : BaseDocumentPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            category.DataSource = this.LoadCategories();
            category.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void ShowSelected(string action)
    {
        Click_LoadByCategory(action.ToLower());

    }

    protected void Click_LoadByCategory(string category)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            var StudentSearchQuery = from f in entity.Documents.Include("Xlk_FileType").Include("Xlk_Type").Include("Server").Include("DocumentTags")
                                     where f.Xlk_Type.Xlk_Category.Description == category
                                     orderby f.Title ascending, f.Server ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }



    protected void category_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }


    protected void type_SelectedIndexChanged(object sender, EventArgs e)
    {
        TypeChanged();
    }


    protected void search_Click(object sender, EventArgs e)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            string searchString = searchstring.Text;

            var documentSearchQuery = ((from d in entity.Documents
                                        join t in entity.DocumentTags on d.DocId equals t.DocId
                                        where d.Xlk_Status.StatusId == 1 && (d.Title.Contains(searchString) | d.Description.Contains(searchString)) | d.DocumentTags.Any(f => f.Tag.Contains(searchString))
                                        select d).Distinct() as ObjectQuery<Documents>).Include("Xlk_FileType").Include("Xlk_Type").Include("Server").Include("DocumentTags");

            LoadResults(documentSearchQuery.ToList());
        }
    }

    protected void type_DataBound(object sender, EventArgs e)
    {
        TypeChanged();
    }

    protected void category_DataBound(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }

    #endregion

    #region Private Methods

    private void LoadTypesToDropDown()
    {
        //Set datasource of the Type DropDown
        ListItem _category = category.SelectedItem;
        type.DataSource = this.LoadTypes(Convert.ToInt32(_category.Value));
        type.DataBind();
    }

    private void TypeChanged()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            if (type.SelectedItem != null)
            {
                Int32 _typeId = Convert.ToInt32(type.SelectedItem.Value);

                var documentSearchQuery = from d in entity.Documents.Include("Xlk_FileType").Include("Xlk_Type").Include("Server").Include("DocumentTags")
                                          where d.Xlk_Type.TypeId == _typeId && d.Xlk_Status.StatusId == 1
                                          select d;

                LoadResults(documentSearchQuery.ToList());
            }
        }
    }

    private void LoadResults(List<Documents> documents)
    {
        //Set datasource of the repeater
        results.DataSource = documents;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", documents.Count().ToString());
    }

    private void LineClicked(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "error", "alert('oops');", true);
    }


    protected string CreateUri(object docId)
    {
        string URI = string.Format("~/ViewerPage.aspx?documentid={0}",docId.ToString());
        return URI;
    }

    protected string CreateDirectUri(object server,object path , object filename)
    {
        return string.Format("file:///{0}{1}{2}", server.ToString().Replace("\\", "/"), path.ToString().Replace("\\", "/"), filename.ToString());
    }

    protected string CreateTagList(object tags)
    {
        System.Text.StringBuilder tagsString = new System.Text.StringBuilder();

        var s = from st in (EntityCollection<DocumentTag>)tags
                select st.Tag;

        return String.Join("/", s.ToArray());
    }

    private bool GetDocument(Int32 docId)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Documents> documentSearchQuery = from d in entity.Documents.Include("Xlk_FileType").Include("Xlk_Type").Include("Server")
                                                        where d.DocId == docId
                                                        select d;

            if (documentSearchQuery.Count() == 0)
                return false;

            Documents doc = documentSearchQuery.First();

            if (doc == null)
                return false;

            if (doc.Xlk_FileType == null)
                return false;

            if (doc.Server == null)
                return false;

            return true;
        }
    }


    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse AddDocumentTag(int id, string tag)
    {
        try
        {
            using (DocumentsEntities entity = new DocumentsEntities())
            {
                if (!TagExists(id, tag))
                {
                    DocumentTag newtag = new DocumentTag();
                    newtag.DocumentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Documents", "DocId", id);
                    newtag.DocId = id;
                    newtag.TagId = NextTagId(id, 1);
                    newtag.Tag = tag;

                    entity.AddToDocumentTags(newtag);

                    if (entity.SaveChanges() > 0)
                        return new AjaxResponse();

                    return new AjaxResponse(new Exception("Could not add the new tag; there was a problem"));

                }
                else
                     return new AjaxResponse(new Exception( "Could not add the new tag; tag already exists"));
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> LoadTags(int id)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            using (DocumentsEntities entity = new DocumentsEntities())
            {

                IQueryable<DocumentTag> tagQuery = from d in entity.DocumentTags
                                                   where d.DocId == id
                                                   select d;

                foreach (DocumentTag tag in tagQuery.ToList())
                {
                    sb.AppendFormat("<option>{0}</option>", tag.Tag);
                }
            }
            return new AjaxResponse<string>(sb.ToString());
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse DeleteTag(int id, string tag)
    {
        try
        {
            using (DocumentsEntities entity = new DocumentsEntities())
            {
                IQueryable<DocumentTag> tagObject = from d in entity.DocumentTags
                                                    where d.Tag == tag && d.DocId == id
                                                    select d;

                if (tagObject.Count() > 0)
                    entity.DeleteObject(tagObject.First());
                else
                     return new AjaxResponse(new Exception("Could not delete tag; tag not found"));

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();
            }

            return new AjaxResponse(new Exception( "Could not add the new tag; there was a problem"));
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }

    }

    [WebMethod]
    public static string ToggleStatus(int id)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Documents> documentQuery = from d in entity.Documents.Include("Xlk_Status")
                                                  where d.DocId == id
                                                  select d;

            if (documentQuery.Count() > 0)
            {
                Documents doc = documentQuery.First();
                doc.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte((doc.Xlk_Status.StatusId == 1) ? 2 : 1));
            }
            else
                return "Could not toggle the status, document not found";

            if (entity.SaveChanges() > 0)
                return string.Empty;
        }

        return "Could not add the new tag; there was a problem";

    }

    private static bool TagExists(int id, string tag)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            var i = from d in entity.DocumentTags
                    where d.Tag == tag && d.DocId == id
                    select d.DocId;

            return (i.Count() > 0);
        }
    }

    private static byte NextTagId(int id, byte step)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<DocumentTag> tagQuery = from t in entity.DocumentTags
                                       where t.DocId == id
                                       select t;
            if (tagQuery.Count() > 0)
                return (byte)(tagQuery.Max(t => t.TagId) + step);

            return step;
        }
    }

    #endregion
}
