﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using PaceManager.Accommodation;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using PaceManager.Group;

public partial class Accommodation_PlanAccommodationPage : BaseGroupPage
{
    protected string _action = "assign";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["Action"]))
            _action = Request["Action"];

        if (!Page.IsPostBack)
        {
        }
    }

    protected string BuildStudentsList()
    {
        List<Family> families = null;
        try
        {
            StringBuilder collection = new StringBuilder("<table id=\"box-table-a\"><thead><tr><th colspan=\"5\"></th></tr></thead>");


            if (families == null)
                families = LoadFamilies();


            var studentQuery = from g in BaseGroupPage.Entities.Group
                               where g.ArrivalDate >= DateTime.Now
                               select new { g.GroupId, g.GroupName, g.ArrivalDate, g.DepartureDate, Student = (from s in g.Student select new { s.StudentId, s.FirstName, s.SurName, StudentType = s.Xlk_StudentType.Description, StudentHosting = s.StudentHosting.FirstOrDefault() }) };

            foreach (var group in studentQuery.ToList())
            {
                StringBuilder sb = new StringBuilder("<tbody>");
                sb.AppendFormat("<tr class=\"groupheader\"><th colspan=\"5\">{0} <span style=\"font-size:0.7em;\">from: {1} to: {2}</span></th></tr>", group.GroupName, group.ArrivalDate.ToString("dd/MMM/yy"), group.DepartureDate.ToString("dd/MMM/yy"));
                sb.Append("<tr class=\"groupheader\"><th>StudentId</th><th>Type</th><th>First Name</th><th>Sur Name</th><th>Family</th></tr>");
                foreach (var student in group.Student)
                {

                    sb.AppendFormat("<tr id=\"{0}\" class=\"{5}\"><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>", student.StudentId, student.StudentType, student.FirstName, student.SurName, BuildFamilyDropDown(student.StudentId,families, student.StudentHosting), (student.StudentHosting == null) ? string.Empty : "assigned");
                }
                sb.Append("</tbody>");
                collection.Append(sb);
            }

            collection.Append("</table>");
            return collection.ToString();
        }
        catch (Exception)
        {
            return string.Empty;
        }

    }

    protected string BuildConfirmList()
    {
        List<Family> families = null;
        try
        {
            StringBuilder collection = new StringBuilder("<table id=\"box-table-a\"><thead><tr><th colspan=\"5\"></th></tr></thead>");


            if (families == null)
                families = LoadFamilies();


            var studentQuery = from g in BaseGroupPage.Entities.Group
                               where g.ArrivalDate >= DateTime.Now
                               select new { g.GroupId, g.GroupName, g.ArrivalDate, g.DepartureDate, Student = (from s in g.Student select new { s.StudentId, s.FirstName, s.SurName, StudentType = s.Xlk_StudentType.Description, StudentHosting = s.StudentHosting.FirstOrDefault() }) };

            foreach (var group in studentQuery.ToList())
            {
                StringBuilder sb = new StringBuilder("<tbody>");
                sb.AppendFormat("<tr class=\"groupheader\"><th colspan=\"5\">{0} <span style=\"font-size:0.7em;\">from: {1} to: {2}</span></th></tr>", group.GroupName, group.ArrivalDate.ToString("dd/MMM/yy"), group.DepartureDate.ToString("dd/MMM/yy"));
                sb.Append("<tr class=\"groupheader\"><th>StudentId</th><th>Type</th><th>First Name</th><th>Sur Name</th><th>Family</th></tr>");
                foreach (var student in group.Student)
                {

                    sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>", student.StudentId, student.StudentType, student.FirstName, student.SurName, BuildFamilyDropDown(student.StudentId,families, student.StudentHosting), (student.StudentHosting == null) ? string.Empty : "assigned");
                       
                }
                sb.Append("</tbody>");
                collection.Append(sb);
            }
    
            collection.Append("</table>");
            return collection.ToString();
        }
        catch (Exception)
        {
            return string.Empty;
        }

    }

    protected void filterDate_TextChanged(object sender, EventArgs e)
    {

    }

    private List<Family> LoadFamilies()
    {
        var familyQuery = from f in BaseAccommodationPage.Entities.Family
                          select f;

        return familyQuery.ToList();

    }

    private string BuildFamilyDropDown(int studentId,List<Family> families, PaceManager.Group.StudentHosting studentHosting)
    {

        if (studentHosting.ConfirmedDate.HasValue)
        {
            Family _family = (from f in families where f.FamilyId == studentHosting.FamilyId select f).FirstOrDefault();
            return string.Format("{0} {1}", _family.FirstName, _family.SurName);
        }

        StringBuilder sb = new StringBuilder(string.Format("<select id=\"{0}\" onChange=\"assignFamily(this,{0})\">", studentId));
        if (studentHosting == null)
            sb.Append("<option \"selected\">Please Select</option>");

        foreach (Family family in families)
        {
            sb.AppendFormat("<option {3} value={0}>{1} {2}</option>", family.FamilyId, family.FirstName, family.SurName, (studentHosting != null && family.FamilyId == studentHosting.FamilyId) ? "selected" : string.Empty);

        }
        
        sb.Append("</select>");
        return sb.ToString();

    }

    private string BuildFamilyConfirm(int studentId, List<Family> families, PaceManager.Group.StudentHosting studentHosting)
    {

        StringBuilder sb = new StringBuilder(string.Format("<select id=\"{0}\" onChange=\"assignFamily(this,{0})\">", studentId));
        if (studentHosting == null)
            sb.Append("<option \"selected\">Please Select</option>");

        foreach (Family family in families)
        {
            sb.AppendFormat("{0} {1}",BaseEnrollmentPage.CreateName( family.FirstName, family.SurName), (studentHosting != null && family.FamilyId == studentHosting.FamilyId) ? "selected" : string.Empty);
        }

        sb.Append("</select>");
        return sb.ToString();

    }  
    
    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool AssignStudentToFamily(int familyId,int studentId)
    {
        try
        {

            Hosting currenthosting = (from h in BaseAccommodationPage.Entities.Hosting
                               where h.Student.StudentId == studentId && h.ArrivalDate > DateTime.Now
                               select h).FirstOrDefault();

            if (currenthosting != null)
            {
                currenthosting.FamilyReference.EntityKey = new EntityKey(BaseAccommodationPage.Entities.DefaultContainerName + ".Family", "FamilyId", familyId);
                //set the confirmed date and details to null
                //currenthosting

                return BaseAccommodationPage.Entities.SaveChanges() > 0;
            }
            else
            {

                Group group = (from s in Entities.Student
                               where s.StudentId == studentId
                               select s.Group).FirstOrDefault();


                Hosting hosting = new Hosting();
                hosting.FamilyReference.EntityKey = new EntityKey(BaseAccommodationPage.Entities.DefaultContainerName + ".Family", "FamilyId", familyId);
                hosting.StudentReference.EntityKey = new EntityKey(BaseAccommodationPage.Entities.DefaultContainerName + ".Student", "StudentId", studentId);
                hosting.ArrivalDate = group.ArrivalDate;
                hosting.DepartureDate = group.DepartureDate;

                BaseAccommodationPage.Entities.AddToHosting(hosting);

                return BaseAccommodationPage.Entities.SaveChanges() > 0;
            }
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    
}
