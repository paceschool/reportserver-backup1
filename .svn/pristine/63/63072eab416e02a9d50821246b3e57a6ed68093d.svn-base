﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Documents;
using System.Data.Objects;
using System.Data;


/// <summary>
/// Summary description for BaseDocumentPage
/// </summary>

public partial class BaseDocumentPage : BasePage
{
    public DocumentsEntities _entities { get; set; }

    public BaseDocumentPage()
    {
    }

    #region Events

    public static DocumentsEntities CreateEntity
    {
        get
        {  
            //DocumentsEntities value;

            //if (CacheManager.Instance.GetFromCache<DocumentsEntities>("DocumentsEntitiesObject", out value))
            //    return value;

            //return CacheManager.Instance.AddToCache<DocumentsEntities>("DocumentsEntitiesObject", new DocumentsEntities());
            return new DocumentsEntities();
        }
    }

    public IList<PaceManager.Documents.Server> LoadServers()
    {
        IList<PaceManager.Documents.Server> values = null;

        if (CacheManager.Instance.GetFromCache<IList<PaceManager.Documents.Server>>("All_DocumentsServers", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<PaceManager.Documents.Server>>("All_DocumentsServers", GetServers());
    }

    public IList<PaceManager.Documents.Server> LoadServer(Int32 serverId)
    {
            IQueryable<PaceManager.Documents.Server> values = (from s in LoadServers()
                                                              where s.ServerId == serverId
                                                              select s).AsQueryable();
            return values.ToList();
    }

    public IList<Xlk_Category> LoadCategories()
    {
        IList<Xlk_Category> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Category>>("All_DocumentsCategory", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Category>>("All_DocumentsCategory", GetCategories());
    }

    public IList<Xlk_Type> LoadTypes()
    {
        IList<Xlk_Type> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Type>>("All_DocumentsType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Type>>("All_DocumentsType", GetTypes());
    }

    public IList<Xlk_Type> LoadTypes(int categoryId)
    {
        IQueryable<Xlk_Type> values = (from t in LoadTypes()
                where t.Xlk_Category.CategoryId == categoryId
                select t).AsQueryable();

        return values.ToList();
    }

    public IList<PaceManager.Documents.Xlk_FileType> LoadFileTypes()
    {
        IList<PaceManager.Documents.Xlk_FileType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<PaceManager.Documents.Xlk_FileType>>("All_DocumentsFileType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<PaceManager.Documents.Xlk_FileType>>("All_DocumentsFileType", GetFileTypes());
    }

    public IList<Xlk_FileType> LoadFileType(string extension)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_FileType> x = (from f in LoadFileTypes()
                                         where f.Extension == extension
                                         select f).AsQueryable();

            return x.ToList();
        }
    }

    #endregion

    #region Public Methods

    public EntityKey GetServerEntityKey(string serverName)
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Server> x = (from f in LoadServers()
                                   where f.UncPath.Contains(serverName)
                                   select f).AsQueryable();
            if (x.Count() > 0)
                return new EntityKey(entity.DefaultContainerName + ".Server", "ServerId", Convert.ToByte(x.First().ServerId));
        }
        return null;
    }

    public PaceManager.Documents.Server GetServer(string serverName)
    {
        IQueryable<PaceManager.Documents.Server> x = (from f in LoadServers()
                               where f.UncPath.Contains(serverName)
                               select f).AsQueryable();
        if (x.Count() > 0)
            return x.First();

        return null;
    }

    #endregion

    #region Private Methods

    private IList<Xlk_FileType> GetFileTypes()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_FileType> lookupQuery =
                 from p in entity.Xlk_FileType
                 select p;

            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_Type> GetTypes()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_Type> lookupQuery =
                 from p in entity.Xlk_Type.Include("Xlk_Category")
                 select p;
            return lookupQuery.ToList();
        }
    }

    private IList<Xlk_Category> GetCategories()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Xlk_Category> lookupQuery =
                 from p in entity.Xlk_Category
                 select p;
            return lookupQuery.ToList();
        }
    }

    private IList<Server> GetServers()
    {
        using (DocumentsEntities entity = new DocumentsEntities())
        {
            IQueryable<Server> lookupQuery =
                 from p in entity.Server
                 select p;
            return lookupQuery.ToList();
        }
    }

    #endregion
}

