﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Group;
using System.Globalization;
using System.Web.Services;
using System.Text;

public partial class Group_GroupsPage : BaseGroupPage
{
    protected string month;
    private int[] statusFilter = new[] {3};

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listnationality.DataSource = this.LoadNationalities();
            listnationality.DataBind();

            listagent.DataSource = this.LoadAgents();
            listagent.DataBind();

            listprogrammes.DataSource = LoadProgrammeTypes();
            listprogrammes.DataBind();

            Click_LoadCurrentGroups();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

            lookupString.Focus();
        }    
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadGroups();
                break;
            case "current":
                Click_LoadCurrentGroups();
                break;
            case "next":
                Click_NextArrivingGroups();
                break;
            case "arriving":
                Click_Arriving();
                break;
            case "departing":
                Click_Departing();
                break;
            case "junior":
                Click_Junior();
                break;
            case "tocome":
                Click_LoadAllToCome();
                break;
            case "cancelled":
                Click_Cancelled();
                break;
            default:
                Click_LoadAllGroups();
                break;
        }
    }

    protected void Click_LoadCurrentGroups()
    {
        DateTime today = DateTime.Now.Date;
        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = from f in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                   where f.ArrivalDate <= DateTime.Now && f.DepartureDate >= today && !statusFilter.Contains(f.Xlk_Status.StatusId)
                                   orderby f.GroupName ascending
                                   select f;
            LoadResults(GroupSearchQuery.ToList());
        }

    }

    protected void Click_LoadAllToCome()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = from t in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                   where t.ArrivalDate >= DateTime.Now && t.Xlk_Status.StatusId != 3
                                   orderby t.ArrivalDate ascending
                                   select t;
            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_Cancelled()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = from c in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                   where c.Xlk_Status.StatusId == 3
                                   orderby c.ArrivalDate ascending
                                   select c;
            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected string GetMonth()
    {
        StringBuilder sb = new StringBuilder();

        for (var MonthId = 1; MonthId < 13; MonthId++)
        {
            switch (MonthId)
            {
                case 1: month = "January";
                    break;
                case 2: month = "February";
                    break;
                case 3: month = "March";
                    break;
                case 4: month = "April";
                    break;
                case 5: month = "May";
                    break;
                case 6: month = "June";
                    break;
                case 7: month = "July";
                    break;
                case 8: month = "August";
                    break;
                case 9: month = "September";
                    break;
                case 10: month = "October";
                    break;
                case 11: month = "November";
                    break;
                case 12: month = "December";
                    break;
                default: month = "January";
                    break;
            }
            sb.AppendFormat("<option value={0}>{1}</option>", MonthId, month);
        }
        return sb.ToString();
    }

    protected void Click_LoadAllGroups()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = from f in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                   where !statusFilter.Contains(f.Xlk_Status.StatusId)
                                   orderby f.ArrivalDate ascending
                                   select f;
            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_Arriving()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now.AddDays(7), CalendarWeekRule.FirstFullWeek);
        DateTime endarrivingDate = startarrivingDate.AddDays(7);
        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = (from f in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                    where f.ArrivalDate > DateTime.Now && !statusFilter.Contains(f.Xlk_Status.StatusId)
                                    orderby f.ArrivalDate ascending
                                    select f).ToList();
            LoadResults(GroupSearchQuery.ToList());
        }
        listnationality.SelectedIndex = 0;
    }

    protected void Click_Departing()
    {
        DateTime startleavingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime endleavingDate = startleavingDate.AddDays(7);
        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = from g in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                   where (g.DepartureDate >= startleavingDate && g.DepartureDate <= endleavingDate && g.DepartureDate > DateTime.Now) && !statusFilter.Contains(g.Xlk_Status.StatusId)
                                   orderby g.GroupName ascending
                                   select g;
            LoadResults(GroupSearchQuery.ToList());
        }
        listnationality.SelectedIndex = 0;
    }

    protected void Click_NextArrivingGroups()
    {
        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = (from g in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                    where g.ArrivalDate > DateTime.Now && !statusFilter.Contains(g.Xlk_Status.StatusId)
                                    orderby g.ArrivalDate ascending
                                    select g).ToList().Take(4);
            LoadResults(GroupSearchQuery.ToList());
        }
    }

    protected void Click_Junior()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now.AddDays(7), CalendarWeekRule.FirstFullWeek);
        DateTime endarrivingDate = startarrivingDate.AddDays(7);


        using (GroupEntities entity = new GroupEntities())
        {
            var GroupSearchQuery = (from f in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                    where f.ArrivalDate.Year == DateTime.Now.Year && f.Enrollment.Where(x => x.Xlk_ProgrammeType.ProgrammeTypeId == 7).Count() > 0 && !statusFilter.Contains(f.Xlk_Status.StatusId)
                                    orderby f.ArrivalDate ascending
                                    select f).ToList();

            LoadResults(GroupSearchQuery.ToList());
        }
        listnationality.SelectedIndex = 0;
    }

    protected void lookupGroup(object sender, EventArgs e)
    {
        Click_LoadGroups();
    }

    private void LoadResults(List<Group> groups)
    {
        //Set the datasource of the repeater
        results.DataSource = groups;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", groups.Count().ToString());
    }

    protected void Click_LoadGroups()
    {

        int agencyId = Convert.ToInt32(listagent.SelectedItem.Value);
        int nationalityId = Convert.ToInt32(listnationality.SelectedItem.Value);
        int programmeTypeId = Convert.ToInt32(listprogrammes.SelectedItem.Value);

        using (GroupEntities entity = new GroupEntities())
        {
            string search = lookupString.Text.Trim();
            string search1 = RemoveDiacritics(lookupString.Text.Trim());

            List<Int32?> ids = entity.SearchGroups((agencyId > 0) ? agencyId : (Int32?)null, (nationalityId > 0) ? nationalityId : (Int32?)null, (programmeTypeId > 0) ? programmeTypeId : (Int32?)null, search1).ToList();

            var groupSearchQuery = from s in entity.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                     where ids.Contains(s.GroupId)
                                     select s;

            LoadResults(groupSearchQuery.OrderByDescending(s => s.ArrivalDate).Take(300).ToList());
        }
    }

    protected string CreateGroupInvoiceList(object groupinvoices)
    {
        System.Text.StringBuilder invoiceString = new System.Text.StringBuilder();

        foreach (GroupInvoice gi in (System.Data.Objects.DataClasses.EntityCollection<GroupInvoice>)groupinvoices)
        {
            invoiceString.AppendFormat("<a href=\"{0}?InvoiceNumber={1}\">{1}</a>", VirtualPathUtility.ToAbsolute("~/Finance/ViewSageInvoicePage.aspx"), gi.SageInvoiceRef);
        }

        return invoiceString.ToString();
    }

    [WebMethod]
    public static string LoadSageInvoiceRefs(int groupId)
    {
        StringBuilder sb = new StringBuilder();
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<GroupInvoice> sageRefQuery = from d in entity.GroupInvoice
                                                    where d.Group.GroupId == groupId
                                                    select d;
            foreach (GroupInvoice sageref in sageRefQuery.ToList())
            {
                sb.AppendFormat("<option>{0}</option>", sageref.SageInvoiceRef);
            }
        }
        return sb.ToString();
    }

    [WebMethod]
    public static string DeleteSageInvoiceRef(int groupId, int sageinvoiceref)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            IQueryable<GroupInvoice> sageRefObject = from d in entity.GroupInvoice
                                                     where d.SageInvoiceRef == sageinvoiceref && d.Group.GroupId == groupId
                                                     select d;
            if (sageRefObject.Count() > 0)
                entity.DeleteObject(sageRefObject.First());
            else
                return "Could not delete reference: reference not found.";
            if (entity  .SaveChanges() > 0)
                return string.Empty;

            return "Could not add the new reference. There was a problem.";
        }
    }

    [WebMethod]
    public static string AddSageInvoiceRef(int groupId, int sageinvoiceref)
    {
        try
        {
            if (!SageInvoiceRefExists(groupId, sageinvoiceref))
            {
                using (GroupEntities entity = new GroupEntities())
                {
                    GroupInvoice groupInvoice = new GroupInvoice();
                    groupInvoice.GroupReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Group", "GroupId", groupId);
                    groupInvoice.SageInvoiceRef = sageinvoiceref;
                    groupInvoice.DateRecorded = DateTime.Now;

                    entity.AddToGroupInvoice(groupInvoice);

                    if (entity.SaveChanges() > 0)
                        return string.Empty;
                }
                return string.Empty;
            }
            else
                return "Could not save the reference, this reference already exists.";
        }

        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveSageReference(GroupInvoice groupInvoice)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            entity.AddToGroupInvoice(groupInvoice);

            if (entity.SaveChanges() > 0)
                return string.Empty;
            else
                return "Could not save reference, there was a problem.";
        }
    }

    [WebMethod]
    public static string ShowGroupsArrivals(int month, bool showDetails)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            return ReportPath("AdministrationReports", "Admin_GroupsArrivals", "PDF", new Dictionary<string, object> { { "Month", month }, { "ShowDetails", showDetails } });
        }
    }

    [WebMethod]
    private static bool SageInvoiceRefExists(int groupid, int sageinvoiceref)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            var i = from d in entity.GroupInvoice
                    where d.SageInvoiceRef == sageinvoiceref && d.Group.GroupId == groupid
                    select d.GroupInvoiceId;
            return (i.Count() > 0);
        }
    }
}
