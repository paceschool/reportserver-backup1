﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Tuition;
using System.Text;
using System.Data.Objects.DataClasses;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Objects;

public partial class Tuition_ClassPlanPage : BaseTuitionPage 
{

    private DateTime _modellingDate;

    #region Events    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {           
            _modellingDate = DateTime.Now.NextWeekDay(DayOfWeek.Monday); 
            
            listcoursetype.DataSource = BaseEnrollmentControl.LoadCourseTypes();
            listcoursetype.DataBind();

            listbuilding.DataSource = LoadBuildings().ToList();
            listbuilding.DataBind();

            datepicker.Text = _modellingDate.ToString("dd/MM/yy");
            BuildClasses();
        }
    }

    protected void datepicker_TextChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    protected void listbuilding_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    protected void listcoursetype_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    protected void isClosed_CheckedChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    #endregion

    #region Private Methods

    private void BuildClasses()
    {

        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classQuery = from c in entity.Class
                             where (c.IsClosed == _isClosed) && (c.StartDate <= _modellingDate.Date && c.EndDate >= _modellingDate.Date) && (c.ClassRoom.Xlk_Building.BuildingId == buildingId) && (c.Xlk_CourseType.CourseTypeId == courseTypeId)
                             select new
                             {
                                 ClassId = c.ClassId,
                                 ClassRoom = c.ClassRoom.Label,
                                 Teacher = c.Teacher.Name,
                                 TuitionLevel = c.TuitionLevel.Description,
                                 CEFR = c.TuitionLevel.CEFR
                             };

            results.DataSource = classQuery;
            results.DataBind();
        }
    }

    protected string GetReportURL()
    {
        if (string.IsNullOrEmpty(datepicker.Text))
            return "#";
        
        _modellingDate = Convert.ToDateTime(datepicker.Text);
        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);

        return string.Format("{0}?/PaceManagerReports/ClassList&Date={1}&CourseTypeId={2}&BuildingId={3}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False", BasePage.ReportServerURL, _modellingDate.Date.ToString("dd/MM/yyyy"), courseTypeId,buildingId);
    }

    protected List<Enrollment> BuildLooseGroupStudents(DateTime modellingDate)
    {
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);
        using (TuitionEntities entity = new TuitionEntities())
        {
            return (from n in entity.Enrollment.Include("Student").Include("Student.Group")
                    where (n.Student.Group.IsClosed == _isClosed) && (n.StartDate <= modellingDate && n.EndDate >= modellingDate) && n.Class.ClassId == null && n.ClassSchedule.Count == 0 && n.Xlk_CourseType.CourseTypeId == courseTypeId && n.Student.StudentTypeId == 1
                    select n).ToList();
        }

    }

    protected List<Enrollment> BuildLooseStudents(DateTime modellingDate)
    {
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);
        using (TuitionEntities entity = new TuitionEntities())
        {
            return (
                from n in entity.Enrollment.Include("Student")
                where (!n.Student.GroupId.HasValue) && (n.StartDate <= modellingDate && n.EndDate >= modellingDate) && n.Class.ClassId == null && n.ClassSchedule.Count == 0 && n.Xlk_CourseType.CourseTypeId == courseTypeId && n.Student.StudentTypeId == 1
                select n).ToList();
        }

    }

    protected string BuildStudents(List<Enrollment> enrollments)
    {
        StringBuilder sb = new StringBuilder();
        if (enrollments != null && enrollments.Count > 0)
        {
            foreach (Enrollment enrollment in enrollments)
            {
                sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0} {2}% <i>{3}</i></div>", CreateName(enrollment.Student.FirstName, enrollment.Student.SurName), enrollment.EnrollmentId, enrollment.Student.PlacementTestResult, enrollment.Student.GroupId != null ? enrollment.Student.Group.GroupName : string.Empty);
            }
        }
        return sb.ToString();
    }

    protected string BuildStudents(object classId)
    {
        int _classId = Convert.ToInt32(classId);
        using (TuitionEntities entity = new TuitionEntities())
        {
            List<Enrollment> enrollmentswithschedulelist = (
                from e in entity.Enrollment.Include("Student")
                where (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date) && e.ClassSchedule.Any(x => x.Class.ClassId == _classId && x.StartDate <= _modellingDate.Date && !x.DateProcessed.HasValue)
                select e)
                .Union(
                 from e in entity.Enrollment.Include("Student")
                 where (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date) && e.Class.ClassId == _classId && (e.ClassSchedule.Count == 0 || !e.ClassSchedule.Any(x => x.StartDate <= _modellingDate.Date && !x.DateProcessed.HasValue))
                 select e).OrderBy(s => s.Student.FirstName).ToList();




            StringBuilder sb = new StringBuilder();
            if (enrollmentswithschedulelist != null && enrollmentswithschedulelist.Count > 0)
            {
                foreach (Enrollment enrollment in enrollmentswithschedulelist)
                {
                    sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}</div>", CreateName(enrollment.Student.FirstName, enrollment.Student.SurName), enrollment.EnrollmentId);
                }
            }
            return sb.ToString();
        }
        
    }

    private void RebuildPage()
    {
        if (!string.IsNullOrEmpty(datepicker.Text))
        {
            _modellingDate = Convert.ToDateTime(datepicker.Text);

            BuildClasses();
        }
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static bool AssignStudentToClassSchedule(int classid, long enrollmentid,DateTime assignmentdate) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
         try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                Enrollment enrollment = (from e in entity.Enrollment.Include("Class").Include("Student").Include("ClassSchedule")
                                         where e.EnrollmentId == enrollmentid
                                         select e).First();

                if (assignmentdate.Date < DateTime.Now.Date)
                    return false;

                if (enrollment.Class != null && enrollment.Class.ClassId == classid)
                    return false;


                if (enrollment.ClassSchedule.Any(x => !x.DateProcessed.HasValue))
                {
                    List<ClassSchedule> schedules = enrollment.ClassSchedule.Where(x => !x.DateProcessed.HasValue).ToList();
                    foreach (ClassSchedule schedule in schedules)
                    {
                        entity.DeleteObject(schedule);
                    }
                    entity.SaveChanges();
                }

                ClassSchedule newSchedule = ClassSchedule.CreateClassSchedule(enrollmentid, assignmentdate.Date);
                newSchedule.ClassReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Class", "ClassId", classid);
                newSchedule.EnrollmentReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Enrollment", "EnrollmentId", enrollmentid);
                entity.AddToClassSchedule(newSchedule);

                return entity.SaveChanges() > 0;
            }
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    
    #endregion

}
