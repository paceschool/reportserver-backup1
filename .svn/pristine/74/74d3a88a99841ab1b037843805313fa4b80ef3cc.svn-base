﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddStudentOrderedItemControl.ascx.cs"
    Inherits="Student_AddStudentOrderedItem" %>
<script type="text/javascript">
    $(function () {

        var unitCosts = {};

        getForm = function () {
            return $("#addstudentordereditem");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","SaveStudentOrderedItem") %>';
        }

        getLocalRules = function () {
            var localrules = {};
            localrules = {
                Qty: {
                    required: true,
                    minlength: 1,
                    number: true,
                    minStrict: 0
                },
                StartDate: {
                    required: function(element) {
                            return $("#ProductTypeId").val() == 2 || $("#ProductTypeId").val() == 4 || $("#ProductTypeId").val() == 8;
                          },
                    minlength: 6
                },
                AmountPaid: {
                    number: true,
                    minlength: 1,
                    required: function (element) {
                    if ($("#PaymentMethodId").val() == 6)
                        return true;
                    else
                        return ($(element).val() > 0);
                    }
                },
                TicketNumberFrom: {
                    required: function(element) {
                            return $("#ProductTypeId").val() == 4 || $("#ProductTypeId").val() == 8;
                          },
                    minlength: 4
                },
                TicketNumberTo: {
                    minlength: 6,
                    required: function(element) {
                            return $("#ProductTypeId").val() == 4 || $("#ProductTypeId").val() == 8;
                          }
                },
                Xlk_PaymentMethod: {
                    minStrict:1,
                    required: function (element) {
                        return ($(element).val() > 0);
                    }
                }
            }
            return localrules;
        }

        getLocalMessages = function () {
            var localmessages = {};
            localmessages = {
                Qty: {
                    required: "Please provide a quantity",
                    minlength: "The quantity must be greater than 0"
                },
                StartDate: {
                    required: "Please select start date for ticket",
                    minlength: "The date must be valid"
                },
                TicketNumberFrom: {
                    required: "Please enter the ticket number",
                    minlength: "The ticket number must be part of a batch"
                },
                TicketNumberTo: {
                    required: "Please enter the ticket number",
                    minlength: "The ticket number must be part of a batch"
                },
                AmountPaid: {
                    required: "Please enter the amount paid",
                    minlength: "This value must be at greater than 0"
                },
                Xlk_PaymentMethod: {
                    required: "Please select a payment method",
                    minlength: "Please select a payment method"
                },
            }
            return localmessages;
        }


        loadProducts = function (producttypeid) {
            $("#ItemId").html('');
            $.ajax({
                type: "POST",
                url: '<%= ToVirtual("~/Agent/ViewAgentPage.aspx","GetOrderableItems") %>',
                data: "{producttypeid:" + producttypeid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#ItemId").html(msg.d);
                    getProduct($("#StudentId").val(), $("#ItemId").val());
                }
            });
        }

        getProduct = function (studentid, itemid) {
            $.ajax({
                type: "POST",
                url: '<%= ToVirtual("~/Enrollments/ViewStudentPage.aspx","GetOrderableItem") %>',
                data: "{studentid:" + studentid + ",itemid:" + itemid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d.length > 0) 
                        assignValues(jQuery.parseJSON(msg.d)[0]);
                    else
                        $("#UnitId").html('<option>No Units</option>')
                }
            });
        }

        assignValues = function (object) {
            unitCosts = object.Costs;
            $("#UnitId").html('').addItems(object.Costs).change();

        }
        $.fn.addItems = function (data) {
            return this.each(function () {
                var list = this;
                $.each(data, function (index, itemData) {
                    var option = new Option(itemData.Description, itemData.UnitId);
                    list.add(option);
                });
            });
        };

        calculate = function (price, qty) {
            $("#ExpectedAmount").val(price * qty);
        }

        $("#ProductTypeId").change(function (e) {
            loadProducts($(this).val());
        });

        $("#ItemId").change(function (e) {
            getProduct($("#StudentId").val(), $(this).val());
        });

        calculatePrice = function (unitid, qty) {
            for (i = 0, l = unitCosts.length; i < l; i++) {
                if (unitCosts[i].UnitId == unitid)
                    calculate(unitCosts[i].CostPer, qty);
            }
        }

        $("#UnitId").change(function (e) {
            $("#ExpectedAmount").val(0);
            calculatePrice($(this).val(), $("#Qty").val());
        });
        $("#Qty").keyup(function () {
            $("#ExpectedAmount").val(0);
            calculatePrice($("#UnitId").val(), $(this).val());
        });

        $("#ProductTypeId").change();

        $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy' });
    });
    
</script>
<style>
    label.error
    {
        float: right;
        color: red;
        padding-left: .5em;
        vertical-align: top;
    }
    p
    {
        clear: both;
    }
    .submit
    {
        margin-left: 12em;
    }
    em
    {
        font-weight: bold;
        padding-right: 1em;
        vertical-align: top;
    }
</style>
<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addstudentordereditem" name="addstudentordereditem" method="post" action="ViewAccommodationPage.aspx/SaveStudentOrderedItem">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="OrderedItemId" value="<%= GetOrderedItemId %>" />
        <input type="hidden" name="Student" id="StudentId" value="<%= GetStudentId %>" />
        <label>
            Item Type
        </label>
        <select name="Xlk_ProductType" id="ProductTypeId">
            <%= GetProductTypeList()%>
        </select>
        <label>
            Ordered Item
        </label>
        <select type="text" name="OrderableItem" id="ItemId">
            <option>Loading....</option>
        </select>
        <label for="Qty">
            Qty
        </label>
        <input <%= IsEditMode %> type="text" name="Qty" id="Qty" value="1" />
        <label>
            By Unit
        </label>
        <select <%= IsEditMode %> type="text" name="Xlk_Unit" id="UnitId">
            <option>Loading....</option>
        </select>
        <label>
            Starting Date
        </label>
        <input type="text" name="StartDate" id="StartDate" value="<%= DateTime.Now.ToString("dd/MM/yyyy") %>" />
        <label>
            Reference From
        </label>
        <input type="text" name="TicketNumberFrom" id="TicketNumberFrom" />
        <div class="togglevis" style="display: none">
            <label>
                Reference To
            </label>
            <input type="text" name="TicketNumberTo" id="TicketNumberTo" /></div>
    </fieldset>
    <fieldset>
        <label>
            Price
        </label>
        <input type="text" disabled name="ExpectedAmount" id="ExpectedAmount" value="0" />
        <label>
            Amount Received
        </label>
        <input type="text" <%= IsEditMode %> name="AmountPaid" id="AmountPaid" value="0" />
        <label>
            Type
        </label>
        <select <%= IsEditMode %> name="Xlk_PaymentMethod" id="PaymentMethodId">
            <%= GetPaymentTypeList()%>
        </select>
        <label>
            Description
        </label>
        <textarea name="Description" id="Description"><%= GetDescription %></textarea>
        <label>
            Comment
        </label>
        <textarea name="Comment" id="Comment"><%= GetComment %></textarea>
        <label>
            Special Instructions
        </label>
        <textarea name="SpecialInstructions" id="SpecialInstructions"><%= GetSpecialInstructions%></textarea>
    </fieldset>
</div>
</form>
