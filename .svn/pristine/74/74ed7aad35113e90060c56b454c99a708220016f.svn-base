﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using Pace.DataAccess.Contacts;

public partial class Contacts_AddContactControl : BaseContactControl
{
    protected Int32 _contactId;
    protected ContactDetails _contactDetails;

    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            if (GetValue<Int32?>("ContactId").HasValue)
            {
                _contactId = GetValue<Int32>("ContactId");

                _contactDetails = (from n in entity.ContactDetails.Include("Xlk_Type").Include("Xlk_Status")
                                   where n.ContactId == _contactId
                         select n).FirstOrDefault();
            }
        }
    }

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_contactDetails != null && _contactDetails.Xlk_Status != null) ? _contactDetails.Xlk_Status.StatusId : (byte?)null;


        foreach (Xlk_Status item in this.LoadStatus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_contactDetails != null && _contactDetails.Xlk_Type != null) ? _contactDetails.Xlk_Type.TypeId : (byte?)null;

        using (ContactsEntities entity = new ContactsEntities())
        {
            var items =
                from o in entity.Xlk_Category
                orderby o.Description
                select new { CategoryType = o.Description, Types = o.Xlk_Type };

            foreach (var type in items)
            {

                sb.AppendFormat("<optgroup label=\"{0}\">", type.CategoryType);

                foreach (var item in type.Types)
                {
                    sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TypeId, item.Description, (_current.HasValue && item.TypeId == _current) ? "selected" : string.Empty);
                }

                sb.Append("</optgroup>");

            }
        }
        return sb.ToString();
    }

    protected string GetContactId
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.ContactId.ToString();

            if (GetValue<Int32?>("ContactId").HasValue)
                return GetValue<Int32>("ContactId").ToString();

            return "0";
        }
    }

    protected string GetContactAddress
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.ContactAddress;


            return null;
        }
    }

    protected string GetContactEmail
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.ContactEmail;


            return null;
        }
    }

    protected string GetContactWebsite
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.ContactWebsite;


            return null;
        }
    }

    protected string GetFaxNumber
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.FaxNumber;


            return null;
        }
    }

    protected string GetFirstName
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.FirstName;


            return null;
        }
    }

    protected string GetMobileNumber
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.MobileNumber;


            return null;
        }
    }

    protected string GetPhoneNumber
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.PhoneNumber;


            return null;
        }
    }

    protected string GetSurName
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.SurName;


            return null;
        }
    }

    protected string GetCompanyName
    {
        get
        {
            if (_contactDetails != null)
                return _contactDetails.CompanyName;

            return null;
        }
    }

}