﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Support;
using System.Data;
using System.Text;
using System.Data.Objects.DataClasses;
using System.Web.Services;
using System.Web.Script.Services;

public partial class TaskPage :BaseSupportPage
{
    protected Int32 userId, taskId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
        
        if (!Page.IsPostBack)
        {
            taskpriority.DataSource = LoadPriority();
            taskpriority.DataBind();
            taskstatus.DataSource = LoadStatus();
            taskstatus.DataBind();
        }
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                //Click_LoadTasks();
                break;
            case "incomplete":
                Click_LoadIncompleteTasks();
                break;
            case "completed":
                Click_LoadCompletedTasks();
                break;
            case "createdbyme":
                Click_LoadMyTasks();
                break;
            case "todays":
                Click_LoadTodaysTasks();
                break;
            default:
                Click_LoadAllTasks();
                break;
        }
    }


    protected void Click_LoadMyTasks()
    {
        int _currentUserId = GetCurrentUser().UserId;
        DateTime _today = DateTime.Now.Date;

        using (SupportEntities entity = new SupportEntities())
        {
            var TaskSearchQuery = from t in entity.Task.Include("Ticket").Include("Xlk_Status").Include("Xlk_Priority").Include("AssignedToUser").Include("TaskAssignments")
                                  where t.RaisedByUser.UserId == _currentUserId
                                  orderby t.DateDue ascending
                                  select new { t.TaskId, t.Description, t.ActionNeeded, t.DateDue, t.ActionTaken, t.DateCompleted, Priority = t.Xlk_Priority.Description, RaisedBy = t.RaisedByUser, t.RaisedDate, AssignedTo = t.AssignedToUser, Xlk_Status = t.Xlk_Status };

            results.DataSource = TaskSearchQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Showing Tasks For: {1} ----- Records Found: {0}", TaskSearchQuery.Count().ToString(), "All");

        }
    }

    protected string IsAssignedToMeAndNotClosed(object Id,object statusId)
    {
        return ((Convert.ToInt32(Id) != GetCurrentUser().UserId || Convert.ToInt32(statusId) == 3) ? "style=\"display:none;\"" : string.Empty);
    }

    protected string IsAssignedToMe(object Id)
    {
        return ((Convert.ToInt32(Id) != GetCurrentUser().UserId) ? "style=\"display:none;\"":string.Empty);
    }

    protected string IsMine(object Id)
    {
        return ((Convert.ToInt32(Id) != GetCurrentUser().UserId) ? "style=\"display:none;\"":string.Empty);
    }
    protected void Click_LoadTodaysTasks()
    {
        int _currentUserId = GetCurrentUser().UserId;
        DateTime _today = DateTime.Now.Date;

        using (SupportEntities entity = new SupportEntities())
        {
            var TaskSearchQuery = from t in entity.Task.Include("Ticket").Include("Xlk_Status").Include("Xlk_Priority").Include("AssignedToUser").Include("TaskAssignments")
                                  where t.AssignedToUser.UserId == _currentUserId && t.DateDue == _today
                                  orderby t.DateDue ascending
                                  select new { t.TaskId, t.Description, t.ActionNeeded, t.DateDue, t.ActionTaken, t.DateCompleted, Priority = t.Xlk_Priority.Description, RaisedBy = t.RaisedByUser, t.RaisedDate, AssignedTo = t.AssignedToUser, Xlk_Status = t.Xlk_Status };

            results.DataSource = TaskSearchQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Showing Tasks For: {1} ----- Records Found: {0}", TaskSearchQuery.Count().ToString(), "All");

        }
    }

    protected void Click_LoadAllTasks()
    {
        using (SupportEntities entity = new SupportEntities())
        {
            var TaskSearchQuery = from t in entity.Task.Include("Ticket").Include("Xlk_Status").Include("Xlk_Priority").Include("AssignedToUser").Include("TaskAssignments")
                                  orderby t.DateDue ascending
                                  select new { t.TaskId, t.Description, t.ActionNeeded, t.DateDue, t.ActionTaken, t.DateCompleted, Priority = t.Xlk_Priority.Description, RaisedBy = t.RaisedByUser, t.RaisedDate, AssignedTo = t.AssignedToUser, Xlk_Status = t.Xlk_Status };

            results.DataSource = TaskSearchQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Showing Tasks For: {1} ----- Records Found: {0}", TaskSearchQuery.Count().ToString(), "All");

        }
    }

    protected void Click_LoadIncompleteTasks()
    {
        int _currentUserId = GetCurrentUser().UserId;
        using (SupportEntities entity = new SupportEntities())
        {
            var TaskSearchQuery = from t in entity.Task.Include("Ticket").Include("Xlk_Status").Include("Xlk_Priority").Include("AssignedToUser").Include("TaskAssignments")
                                  where t.AssignedToUser.UserId == _currentUserId && t.DateCompleted == null
                                  select new { t.TaskId, t.Description, t.ActionNeeded, t.DateDue, t.ActionTaken, t.DateCompleted, Priority = t.Xlk_Priority.Description, RaisedBy = t.RaisedByUser, t.RaisedDate, AssignedTo = t.AssignedToUser, Xlk_Status = t.Xlk_Status };

            results.DataSource = TaskSearchQuery.ToList();
            resultsreturned.Text = string.Format("Records Found: {0}", TaskSearchQuery.Count().ToString());
            results.DataBind();
        }
        
    }

    protected void Click_LoadCompletedTasks()
    {
        int _currentUserId = GetCurrentUser().UserId;
        using (SupportEntities entity = new SupportEntities())
        {
            var TaskSearchQuery = from t in entity.Task.Include("Ticket").Include("Xlk_Status").Include("Xlk_Priority").Include("AssignedToUser").Include("TaskAssignments")
                                  where t.AssignedToUser.UserId == _currentUserId && t.DateCompleted <= DateTime.Now
                                  select new { t.TaskId, t.Description, t.ActionNeeded, t.DateDue, t.ActionTaken, t.DateCompleted, Priority = t.Xlk_Priority.Description, RaisedBy = t.RaisedByUser, t.RaisedDate, AssignedTo = t.AssignedToUser, Xlk_Status = t.Xlk_Status };

            results.DataSource = TaskSearchQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", TaskSearchQuery.ToList().Count().ToString());
        }
    }

    protected void Click_LoadTasks()
    {
        int _currentUserId = GetCurrentUser().UserId;

        using (SupportEntities entity = new SupportEntities())
        {
            var TaskSearchQuery = from t in entity.Task.Include("Ticket").Include("Xlk_Status").Include("Xlk_Priority").Include("AssignedToUser").Include("TaskAssignments")
                                  where t.AssignedToUser.UserId == _currentUserId
                                  select new { t.TaskId, t.Description, t.ActionNeeded, t.DateDue, t.ActionTaken, t.DateCompleted, Priority = t.Xlk_Priority.Description, RaisedBy = t.RaisedByUser, t.RaisedDate, AssignedTo = t.AssignedToUser, Xlk_Status = t.Xlk_Status };

            results.DataSource = TaskSearchQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Showing Tasks For: {1} ----- Records Found: {0}", TaskSearchQuery.ToList().Count().ToString(), "Stephen");
        }
    }

    private void LoadResults(List<Task> tasks)
    {
        //Set the datasource of the repeater
        results.DataSource = tasks;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", tasks.Count().ToString());
    }

    protected void Click_LookupTasks(object sender, EventArgs e)
    {
        using (SupportEntities entity = new SupportEntities())
        {
            var TaskSearchQuery = from t in entity.Task.Include("Ticket").Include("Xlk_Status").Include("Xlk_Priority").Include("Users").Include("TaskAssignments")
                                  orderby t.DateDue ascending
                                  select new { t.TaskId, t.Description, t.ActionNeeded, t.DateDue, t.ActionTaken, t.DateCompleted, Priority = t.Xlk_Priority.Description, RaisedBy = t.RaisedByUser, t.RaisedDate, AssignedTo = t.AssignedToUser, Xlk_Status = t.Xlk_Status };

            results.DataSource = TaskSearchQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Showing Tasks For: {1} ----- Records Found: {0}", TaskSearchQuery.ToList().Count().ToString(), "All");
        }
    }
}
