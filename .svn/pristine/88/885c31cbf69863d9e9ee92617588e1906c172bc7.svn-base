﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Accommodation;
using System.Text;
using System.Web.Services;

public partial class Accommodation_AddFamilyBank : BaseAccommodationControl
{
    protected Int32 _familyId;
    protected FamilyBank _bank;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("FamilyBankBankId") != null)
        {
            Int32 bankId = (Int32)GetValue("FamilyBankBankId");
            _bank = (from banks in Entities.FamilyBank.Include("Family")
                     where banks.FamilyBankBankId == bankId
                     select banks).FirstOrDefault();
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetFamilyId
    {
        get
        {
            if (_bank != null && _bank.Family != null)
                return _bank.Family.FamilyId.ToString();

            if (Parameters.ContainsKey("FamilyId"))
                return Parameters["FamilyId"].ToString();

            return "0";
        }
    }

    protected string GetFamilyBankBankId
    {
        get
        {
            if (_bank != null)
                return _bank.FamilyBankBankId.ToString();
            return "0";
        }
    }

    protected string GetBankName
    {
        get
        {
            if (_bank != null)
                return _bank.BankName.ToString();
            return null;
        }
    }

    protected string GetBankBranch
    {
        get
        {
            if (_bank != null)
                return _bank.Branch.ToString();
            return null;
        }
    }

    protected string GetBankAccNo
    {
        get
        {
            if (_bank != null)
                return _bank.AccountNumber.ToString();
            return null;
        }
    }

    protected string GetBankSortCode
    {
        get
        {
            if (_bank != null)
                return _bank.SortCode.ToString();
            return null;
        }
    }

    protected string GetBankAccName
    {
        get
        {
            if (_bank != null)
                return _bank.AccountName.ToString();
            return null;
        }
    }

    protected string GetBankDateCreated
    {
        get
        {
            if (_bank != null)
                return _bank.DateCreated.ToString("dd/MM/yy");
            return DateTime.Now.ToString("dd/MM/yy");
        }
    }
}
