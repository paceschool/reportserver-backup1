﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Tuition;
using System.Text;
using System.Web.Services;

public partial class Student_AddStudentResultControl : BaseEnrollmentControl
{
    protected Int32 StudentId;
    protected Int32 PreTestId;
    protected PreTest _test;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("PreTestId") != null)
        {
            Int32 PreTestId = (Int32)GetValue("PreTestId");
            _test = (from p in BaseTuitionPage.Entities.PreTests.Include("Student").Include("ExamDate").Include("ExamDate.Exam")
                     where p.PreTestId == PreTestId
                     select p).FirstOrDefault();
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetPreTestId
    {
        get
        {
            if (_test != null)
                return _test.PreTestId.ToString();

            if (Parameters.ContainsKey("PreTestId"))
                return Parameters["PreTestId"].ToString();

            return "0";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_test != null)
                return _test.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetScore
    {
        get
        {
            if (_test != null && _test.Score.HasValue)
                return _test.Score.Value.ToString();

            return null;
        }
    }

    protected string GetExamDates()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_test != null && _test.ExamDate != null) ? _test.ExamDateId : (Int32?)null;

        foreach (ExamDates item in BaseTuitionControl.LoadExamDates().Where(w=>w.ExamDate > DateTime.Now).Take(10))
        {
            sb.AppendFormat("<option {2} value={0}>{3}: {1}</option>", item.ExamDateId, item.ExamDate.Value.ToString("dd MMM yyyy"), (_current.HasValue && item.ExamDateId == _current) ? "selected" : string.Empty,item.Exam.ExamName);
        }

        return sb.ToString();
    }
}