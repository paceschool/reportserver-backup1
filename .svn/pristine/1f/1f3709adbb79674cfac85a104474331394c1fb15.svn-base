﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using PaceManager.Enrollment;
using System.Data.Objects;
using System.Data;
using System.Text;using System.Globalization;
using System.Data.Objects.SqlClient;

public partial class EnrollmentPage : BaseEnrollmentPage
{
    protected string lookupText = string.Empty;
    protected Int32 agencyid = 0;
    protected Int32 nationalityid = 0;

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listnationality.DataSource = LoadNationalities();
            listnationality.DataBind();

            listagent.DataSource = LoadAgents();
            listagent.DataBind();

            listprogrammes.DataSource = LoadProgrammeTypes();
            listprogrammes.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
        }
        lookupString.Focus();
    }

    #endregion

    #region private Methods

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadStudents();
                break;
            case "current":
                Click_LoadCurrentStudents();
                break;
            case "arriving":
                Click_Arriving();
                break;
            case "departing":
                Click_Departing();
                break;
            case "cancelled":
                Click_Cancelled();
                break;
            case "noenrollments":
                Click_LoadNoEnrollmentsStudents();
                break;
            case "nohostings":
                Click_LoadNoHostingStudents();
                break;
            case "allnotgroups":
                Click_LoadAllNotGroups();
                break;
            case "birthdays":
                Click_Birthdays();
                break;
            case "lastentered":
                Click_Last(20);
                break;
            default:
                Click_LoadAllStudents();
                break;
        }
    }

    protected void Click_LoadCurrentStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.GroupId == null && f.ArrivalDate <= DateTime.Now && f.DepartureDate >= DateTime.Now
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }

    }

    protected void Click_LoadAllStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected void Click_Last(int qty)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.GroupId == null
                                     orderby f.StudentId descending, f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.Take(qty).ToList());
        }
    }

    protected void Click_Birthdays()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from b in entity.Student.Include("Xlk_Status")
                                     where b.DateOfBirth.Value.Month == DateTime.Now.Month && (b.ArrivalDate < DateTime.Now && b.DepartureDate >= DateTime.Now)
                                     select b;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected void Click_Arriving()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime endarrivingDate = startarrivingDate.AddDays(12);
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.GroupId == null && (f.Enrollment.Any(g=>g.StartDate >= startarrivingDate && g.StartDate <= endarrivingDate))
                                     orderby f.ArrivalDate, f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
            listnationality.SelectedIndex = 0;
        }
    }

    protected void Click_Departing()
    {
        DateTime startleavingDate = (FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek));
        DateTime endleavingDate = startleavingDate.AddDays(7);
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from g in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where g.GroupId == null && (g.Enrollment.Any(h=>h.EndDate >= startleavingDate && h.EndDate <= endleavingDate))
                                     orderby g.FirstName ascending, g.SurName ascending
                                     select g;
            LoadResults(StudentSearchQuery.ToList());
            listnationality.SelectedIndex = 0;
        }
    }

    protected void Click_Cancelled()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.Xlk_Status.StatusId == 5
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
            listnationality.SelectedIndex = 0;
        }
    }

    protected List<PaceManager.Enrollment.Student> Get_Cancelled()
    {
        using (PaceManager.Enrollment.EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Status")
                                     where f.Xlk_Status.StatusId == 5
                                     select f;
            return StudentSearchQuery.ToList();
        }
    }

    protected void Click_LoadAllNotGroups()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.GroupId == null
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected void Click_LoadNoHostingStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.GroupId == null && f.Hostings.Count == 0
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }
    protected void Click_LoadNoEnrollmentsStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.GroupId == null && f.Enrollment.Count == 0
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }


    protected void lookupStudent(object sender, EventArgs e)
    {
        Click_LoadStudents();
        listagent.SelectedIndex = 0; listnationality.SelectedIndex = 0; lookupString.Attributes.Add("onfocus", "this.select();");
        ; lookupString.Focus();
    }

    protected void Click_LoadStudents()
    {
        int agencyId = Convert.ToInt32(listagent.SelectedItem.Value);
        int nationalityId = Convert.ToInt32(listnationality.SelectedItem.Value);
        int programmeTypeId = Convert.ToInt32(listprogrammes.SelectedItem.Value);
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {                
            var studentSearchQuery = from s in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     select s;

            if (!string.IsNullOrEmpty(lookupString.Text) || agencyId > 0 || nationalityId > 0 || programmeTypeId > 0)
            {

                if (!string.IsNullOrEmpty(lookupString.Text))
                {
                    var search = lookupString.Text.Trim();
                    var search1 = RemoveDiacritics(lookupString.Text.Trim());
                    studentSearchQuery = studentSearchQuery.Where(s => (s.FirstName.Contains(search) || s.FirstName.Contains(search1)) || (s.SurName.Contains(search) || s.SurName.Contains(search1)) || SqlFunctions.StringConvert((double)s.StudentId).Contains(search) || s.StudentInvoice.Count(x => x.SageInvoiceRef.StartsWith(lookupString.Text)) > 0);
                }

                if (agencyId > 0)
                    studentSearchQuery = studentSearchQuery.Where(s => s.Agency.AgencyId == agencyId);

                if (nationalityId > 0)
                    studentSearchQuery = studentSearchQuery.Where(s => s.Xlk_Nationality.NationalityId == nationalityId);

                if (programmeTypeId > 0)
                    studentSearchQuery = studentSearchQuery.Where(s => s.Enrollment.Where(p => p.Xlk_ProgrammeType.ProgrammeTypeId == programmeTypeId).Count() > 0);

                LoadResults(studentSearchQuery.ToList());
            }


            else
                LoadResults(studentSearchQuery.OrderByDescending(s=>s.ArrivalDate).Take(300).ToList());
        }


    }

    private bool SearchUnicodeString(string first, string second)
    {
        return true;
    }

    private void LoadResults(List<Student> students)
    {
        //Set the datasource of the repeater
        results.DataSource = students;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", (students != null) ? students.Count().ToString() : "0");
    }

    //Create list of Sage references
    protected string CreateSageRefsList(object sagerefs)
    {
        System.Text.StringBuilder sagerefstring = new System.Text.StringBuilder();
        var s = from st in (System.Data.Objects.DataClasses.EntityCollection<Agency>)sagerefs
                select st.SageRef;
        return String.Join("/", s.ToArray());
    }

    protected string CreateStudentInvoiceList(object studentinvoices)
    {
        System.Text.StringBuilder invoiceString = new System.Text.StringBuilder();

        var s = from st in (System.Data.Objects.DataClasses.EntityCollection<StudentInvoice>)studentinvoices
                select st.SageInvoiceRef;

        return String.Join("/", s.ToArray());
    }
    protected bool IsCancelled(object statusid,string value)
    {
        if (statusid.ToString() != value)
            return false;
        else
            return true;
    }
    protected string ShowProgrammeType(object enrollments)
    {

        return (from st in (System.Data.Objects.DataClasses.EntityCollection<Enrollment>)enrollments
                select st.Xlk_ProgrammeType.Description).FirstOrDefault();

    }
    protected void results_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DeleteStudent")
        {
            using (EnrollmentsEntities entity = new EnrollmentsEntities())
            {
                entity.DeleteStudent(Convert.ToInt32(e.CommandArgument));
                Response.Redirect(Request.RawUrl);

            }
         
        }
    }


    #endregion

    #region JavaScript Enabled Methods

    [WebMethod]
    public static string AddSageInvoiceRef(int studentid, string sageinvoiceref)
    {
        try
        {
            if (!SageInvoiceRefExists(studentid, sageinvoiceref))
            {
                using (EnrollmentsEntities entity = new EnrollmentsEntities())
                {
                    StudentInvoice studentInvoice = new StudentInvoice();
                    studentInvoice.StudentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Student", "StudentId", studentid);
                    studentInvoice.SageInvoiceRef = sageinvoiceref;
                    studentInvoice.DateRecorded = DateTime.Now;

                    entity.AddToStudentInvoice(studentInvoice);

                    if (entity.SaveChanges() > 0)
                        return string.Empty;
                }
                return string.Empty;
            }
            else
                return "Could not save the reference, this reference already exists.";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string LoadSageInvoiceRefs(int studentid)
    {
        StringBuilder sb = new StringBuilder();
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<StudentInvoice> sageRefQuery = from d in entity.StudentInvoice
                                                                  where d.Student.StudentId == studentid
                                                                  select d;
            foreach (StudentInvoice sageref in sageRefQuery.ToList())
            {
                sb.AppendFormat("<option>{0}</option>", sageref.SageInvoiceRef);
            }
        }
        return sb.ToString();
    }

    [WebMethod]
    public static string DeleteSageInvoiceRef(int studentid, string sageinvoiceref)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<StudentInvoice> sageRefObject = from d in entity.StudentInvoice
                                                                   where d.SageInvoiceRef == sageinvoiceref && d.Student.StudentId == studentid
                                                                   select d;
            if (sageRefObject.Count() > 0)
                entity.DeleteObject(sageRefObject.First());
            else
                return "Could not delete reference: reference not found.";
            if (entity.SaveChanges() > 0)
                return string.Empty;
        }
        return "Could not add the new reference. There was a problem.";
    }

    [WebMethod]
    private static bool SageInvoiceRefExists(int studentid, string sageinvoiceref)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var i = from d in entity.StudentInvoice
                    where d.SageInvoiceRef == sageinvoiceref && d.Student.StudentId == studentid
                    select d.StudentInvoiceId;
            return (i.Count() > 0);
        }
    }

    public static string RemoveDiacritics(string text)
    {
        return string.Concat(
      text.Normalize(NormalizationForm.FormD)
      .Where(ch => CharUnicodeInfo.GetUnicodeCategory(ch) !=
                                    UnicodeCategory.NonSpacingMark)
    ).Normalize(NormalizationForm.FormC);

    }

    #endregion



}
