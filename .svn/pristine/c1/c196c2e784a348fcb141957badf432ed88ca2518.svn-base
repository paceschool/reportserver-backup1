﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using System.Data.Objects;

public partial class Transport_AddTransferToBookingControl : BaseTransportControl
{
    protected Int32 _bookingId;
    protected Int32 _grouptransferId;
    protected Lnk_TransferBooking _transferbooking;
    protected Booking _booking;
    protected Int32 GroupId = 0;
    protected Int32 firstGroup = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("TransferBookingId") != null)
        {
            using (TransportEntities entity = new TransportEntities())
            {
                Int32 TransferBookingId = (Int32)GetValue("TransferBookingId");
                Int32 BookingId = (Int32)GetValue("BookingId");
                Int32 GroupTransferId = (Int32)GetValue("GroupTransferId");

                _transferbooking = (from t in entity.Lnk_TransferBooking.Include("Booking")
                                    where t.TransferBookingId == TransferBookingId
                                    select t).FirstOrDefault();
            }
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetTransferBookingId
    {
        get
        {
            if (_transferbooking != null)
                return _transferbooking.TransferBookingId.ToString();

            if (Parameters.ContainsKey("TransferBookingId"))
                return Parameters["TransferBookingId"].ToString();

            return "0";
        }
    }

    protected string GetBookingId
    {
        get
        {
            if (_transferbooking != null)
                return _transferbooking.Booking.BookingId.ToString();

            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }

    protected string GetGroupList()
    {
        StringBuilder sb = new StringBuilder();

        using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
        {
            foreach (Pace.DataAccess.Group.Group item in LoadGroups())
            {
                sb.AppendFormat("<option alt={2} value={0}>{1}</option>", item.GroupId, item.GroupName, item.GroupId);
            }
        }

        return sb.ToString();
    }

    protected string GetTransferList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = _transferbooking != null ? _transferbooking.GroupTransferId : (Int32?)null;

        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            foreach (Pace.DataAccess.Group.GroupTransfer item in LoadTransfers())
            {
                sb.AppendFormat("<option {2} alt={0} value={0}>{1}</option>", item.GroupTransferId, item.Group.GroupName + " - " + item.Xlk_TransferType.Description, (_current.HasValue && item.GroupTransferId == _current) ? "Selected" : string.Empty);
            }
        }

        return sb.ToString();
    }

    protected void Click_ChangeTransfers(object sender, EventArgs e)
    {
        GetTransferList();
    }

    private IList<Pace.DataAccess.Group.Group> LoadGroups()
    {
        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            var lookupQuery = (from g in entity.Group
                               where g.ArrivalDate > DateTime.Now
                               orderby g.ArrivalDate ascending
                               select g).AsEnumerable(); // Get all Groups still to come
            firstGroup = lookupQuery.First().GroupId;

            return lookupQuery.ToList();
        }
    }

    private IList<Pace.DataAccess.Group.GroupTransfer> LoadTransfers()
    {
        if (firstGroup > 0)
        {
            using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
            {
                var lookupQuery = (from t in gentity.GroupTransfer.Include("Group").Include("Xlk_TransferType").Include("Xlk_Location")
                                   where t.Date > DateTime.Now
                                   orderby t.Group.GroupName ascending, t.Xlk_TransferType.Description ascending
                                   select t).AsEnumerable(); // Get all Transfers that are later than today for selected group

                using (TransportEntities tentity = new TransportEntities())
                {
                    var bQuery = (from b in tentity.Lnk_TransferBooking
                                  select b).AsEnumerable(); // Get list of transfers tied to Transport Bookings

                    if (bQuery.Count() > 0)
                    {
                        foreach (var item in bQuery)
                        {
                            lookupQuery = lookupQuery.Where(x => x.GroupTransferId != item.GroupTransferId);
                        }
                    }
                }

                return lookupQuery.ToList();
            }
        }
        else
        {
            using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
            {
                var lookupQuery = (from t in gentity.GroupTransfer.Include("Group").Include("Xlk_TransferType").Include("Xlk_Location")
                                   where t.Date > DateTime.Now
                                   orderby t.Group.GroupName ascending, t.Xlk_TransferType.Description ascending
                                   select t).AsEnumerable(); // Get all Transfers that are later than today

                using (TransportEntities tentity = new TransportEntities())
                {
                    var bQuery = (from b in tentity.Lnk_TransferBooking
                                  select b).AsEnumerable(); // Get list of transfers tied to Transport Bookings

                    if (bQuery.Count() > 0)
                    {
                        foreach (var item in bQuery)
                        {
                            lookupQuery = lookupQuery.Where(x => x.GroupTransferId != item.GroupTransferId);
                        }
                    }
                }

                return lookupQuery.ToList();
            }
        }
    }
}