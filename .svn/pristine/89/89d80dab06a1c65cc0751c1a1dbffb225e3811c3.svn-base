﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Tuition;
using System.Text;
using System.Web.Services;

public partial class Tuition_AddExamEnrollment : BaseTuitionControl
{
    protected Int32 _studentExamId;
    protected Int32 _studentId;
    protected Int32 _examDateId;
    protected StudentExam _exam;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (GetValue("StudentExamId") != null)
            {
                Int32 _studentExamId = (Int32)GetValue("StudentExamId");
                _exam = (from x in entity.StudentExam.Include("Student").Include("ExamDates")
                         where x.StudentExamId == _studentExamId
                         select x).FirstOrDefault();
            }
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetStudentName()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_exam != null && _exam.Student != null) ? _exam.Student.StudentId : (Int32?)null;

        if (_exam.Student.DepartureDate < DateTime.Now)
        {
            foreach (Student item in LoadPastStudents())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StudentId, item.FirstName + " " + item.SurName, (_current.HasValue && item.StudentId == _current) ? "selected" : string.Empty);
            }

            return sb.ToString();
        }
        else
        {
            foreach (Student item in LoadStudents())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StudentId, item.FirstName + " " + item.SurName, (_current.HasValue && item.StudentId == _current) ? "selected" : string.Empty);
            }

            return sb.ToString();
        }
    }

    protected string GetExamDatesList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_exam != null && _exam.Student != null) ? _exam.ExamDates.ExamDateId : (Int32?)null;

        foreach (ExamDates item in LoadExamDates())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExamDateId, item.ExamDate.Value.ToLongDateString(), (_current.HasValue && item.ExamDateId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStudentId
    {
        get
        {
            if (_exam != null)
                return _exam.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetExamDateId
    {
        get
        {
            if (_exam != null)
                return _exam.ExamDates.ExamDateId.ToString();

            if (Parameters.ContainsKey("ExamDateId"))
                return Parameters["ExamDateId"].ToString();

            return "0";
        }
    }

    protected string GetStudentExamId
    {
        get
        {
            if (_exam != null)
                return _exam.StudentExamId.ToString();

            if (Parameters.ContainsKey("StudentExamId"))
                return Parameters["StudentExamId"].ToString();

            return "0";
        }
    }

    protected string GetExamDate
    {
        get
        {
            if (_exam != null)
                return _exam.ExamDates.ExamDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetDateRegistered
    {
        get
        {
            if (_exam != null && _exam.DateRegistered.HasValue)
                return _exam.DateRegistered.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected bool GetPaidValue
    {
        get
        {
            if (_exam != null && _exam.Paid != null)
                return _exam.Paid.Value;

            return false;
        }
    }

    protected string GetPaidAmount
    {
        get
        {
            if (_exam != null)
                return _exam.Amount.ToString();

            return null;
        }
    }

    protected bool GetAsIndividual
    {
        get
        {
            if (_exam != null && _exam.AsIndividual != null)
                return _exam.AsIndividual.Value;

            return false;
        }
    }

    protected string GetResultScore
    {
        get
        {
            if (_exam != null && _exam.ResultScore != null)
                return _exam.ResultScore.ToString();

            return null;
        }
    }
}