﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using Pace.DataAccess.Group;



public partial class Finance_ReceiptInvoiceControl : BaseEnrollmentControl
{
    protected Int32? _groupId;
    protected Int32? _invoiceReceiptId;
    protected InvoiceReceipt _receipt = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("InvoiceReceiptId") != null)
        {
            _invoiceReceiptId = Convert.ToInt32(GetValue("InvoiceReceiptId"));

            using (GroupEntities entity = new GroupEntities())
            {
                _receipt = (from i in entity.InvoiceReceipts.Include("Group").Include("Xlk_PaymentMethod")
                                     where i.InvoiceReceiptId == _invoiceReceiptId
                                     select i).FirstOrDefault();
            }
        }

        if (GetValue("GroupId") != null)
            _groupId = Convert.ToInt32(GetValue("GroupId"));
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }


    protected string IsEditMode
    {
        get
        {
            if (_receipt == null)
                return String.Empty;

            return "disabled";
        }
    }

    protected string GetInvoiceReceiptId
    {
        get
        {
            if (_receipt != null)
                return _receipt.InvoiceReceiptId.ToString();

            if (_invoiceReceiptId.HasValue)
                return _invoiceReceiptId.ToString();

            if (Parameters.ContainsKey("InvoiceReceiptId"))
                return Parameters["InvoiceReceiptId"].ToString();

            return "0";
        }
    }

    protected string GetGroupId
    {
        get
        {
            if (_receipt != null)
                return   _receipt.Group.GroupId.ToString();

            if (Parameters.ContainsKey("GroupId"))
                return Parameters["GroupId"].ToString();

            return "0";
        }
    }

    protected string GetInvoices
    {
        get
        {
            using (Pace.DataAccess.Finance.FinanceEntities entity = new Pace.DataAccess.Finance.FinanceEntities())
            {
                var Groupinvoices = ((from  s in entity.GroupInvoices
                                     where s.GroupId == _groupId.Value
                                       join sd in entity.SageDatas on s.SageInvoiceRef equals sd.SageRef into lg
                                       from l in lg.DefaultIfEmpty()
                select new { s.SageInvoiceRef, l.Amount }).ToList().Select(x => new {value=x.SageInvoiceRef, label = (x.Amount.HasValue) ? string.Format("{0} for {1}", x.SageInvoiceRef, x.Amount.Value.ToString("C")) : x.SageInvoiceRef.ToString(), category = "Group Invoice" })).ToList();

                return new JavaScriptSerializer().Serialize(Groupinvoices);
            }
        }
    }

    protected string GetInvoiceNumber
    {
        get
        {
            if (_receipt != null)
                return _receipt.SageInvoiceRef.ToString();

            return "";
        }
    }

    protected string GetAmountPaid
    {
        get
        {
            if (_receipt != null)
                return _receipt.AmountPaid.ToString();

            return "0.0";
        }
    }

    protected string GetComment
    {
        get
        {
            if (_receipt != null)
                return _receipt.Comment;

            return "";
        }
    }

    protected string GetPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_receipt != null && _receipt.Xlk_PaymentMethod != null) ? _receipt.Xlk_PaymentMethod.PaymentMethodId : (byte?)null;


        foreach (Pace.DataAccess.Enrollment.Xlk_PaymentMethod item in LoadPaymentMethods())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.PaymentMethodId, item.Description, (_current.HasValue && item.PaymentMethodId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}