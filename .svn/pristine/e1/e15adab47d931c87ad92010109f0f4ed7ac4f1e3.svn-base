﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using Pace.DataAccess.Group;
using Accommodation = Pace.DataAccess.Accommodation;

public partial class Groups_FamilyReservationPage : BaseGroupPage
{

    private DateTime _modellingDate;
    private DateTime _maxDate = DateTime.Now.AddMonths(6);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            _modellingDate = DateTime.Now;

            familystatus.DataSource = LoadFamilyStatus();
            familystatus.DataBind();
            roomlist.DataSource = LoadRoomList();
            roomlist.DataBind();
            familytype.DataSource = LoadFamilyType();
            familytype.DataBind();
            BuildGroups();
            monthselect.DataSource = LoadMonths();
            monthselect.DataBind();
            monthselect.SelectedIndex = -1;
        }
    }

    #region Private Methods

    protected string FamilyNumbers(int id)
    {
        {
            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                int[,] sharing = new int[50, 3]; //Number of sharings by gender
                int msing = 0; int mdoub = 0; int mtrip = 0; //Breakdown of male sharing types
                int fsing = 0; int fdoub = 0; int ftrip = 0; //Breakdown of female sharing types
                Int32 groupings = 0;

                var lookupQuery = from s in entity.Student
                                  where s.GroupId == id
                                  select s; //Get students list tied to this group
                groupings = Convert.ToInt32(lookupQuery.Max(x => x.GroupingBlockId)); //Get the last GroupingBlockId in this group

                for (var j = 1; j < 3; j++) //Loop through the genders
                {
                    for (var i = 1; i < groupings + 1; i++) //Loop through each GroupingBlockId
                    {
                        foreach (Pace.DataAccess.Enrollment.Student item in lookupQuery.Where(x => x.Xlk_Gender.GenderId == j)) //Look for students with current GroupingBlockId
                        {
                            if (item.GroupingBlockId == i)
                                sharing[i, j] = sharing[i, j] + 1;
                        }
                    }
                }
                for (var a = 1; a < groupings + 1; a++)
                {
                    if (sharing[a, 1] == 1)
                        msing = msing + 1; //Add up the Male Singles
                    if (sharing[a, 2] == 1)
                        fsing = fsing + 1; //Add up the Female Singles
                    if (sharing[a, 1] == 2)
                        mdoub = mdoub + 1; //Add up the Male Doubles
                    if (sharing[a, 2] == 2)
                        fdoub = fdoub + 1; //Add up the Female Doubles
                    if (sharing[a, 1] == 3)
                        mtrip = mtrip + 1; //Add up the Male Triples
                    if (sharing[a, 2] == 3)
                        ftrip = ftrip + 1; //Add up the Female Triples
                }

                if (lookupQuery != null)
                {
                    return groupings.ToString();
                }
                return string.Empty;
            }
        }
    }

    protected void Click_ReBuild(object sender, EventArgs e)
    {
        BuildGroups();
    }

    protected void BuildGroups()
    {
        _modellingDate = DateTime.Now.Date;
        if (monthselect.SelectedIndex > 0)
            _maxDate = DateTime.Now.AddMonths(monthselect.SelectedIndex-1);
        using (GroupEntities entity = new GroupEntities())
        {
            var groupQuery = from x in
                                 ((from g in entity.Group.Include("ReservedFamilies")
                                   where g.ArrivalDate >= _modellingDate && g.ArrivalDate <= _maxDate && g.Xlk_Status.StatusId != 3 && g.CampusId == GetCampusId
                             orderby g.ArrivalDate select g).ToList())
            select new 
                             {
                                 GroupId = x.GroupId,
                                 GroupName = x.GroupName,
                                 Arrival = x.ArrivalDate,
                                 Departure = x.DepartureDate,
                                 FamilyId = x.ReservedFamilies.Any(y => y.GroupId == x.GroupId),
                                 FamilyNumbers = FamilyNumbers(x.GroupId)
                             };

            results.DataSource = groupQuery;
            results.DataBind();
        }
    }

    protected void Click_ClearFilters(object sender, EventArgs e)
    {
        familystatus.SelectedIndex = 0;
        roomlist.SelectedIndex = 0;
        familysearch.Text = "";
        familysearch.Focus();
    }

    protected Dictionary<int, string> LoadMonths()
    {
        Dictionary<int,string> monthName = new Dictionary<int, string>();

        for (var i = 1; i < 13; i++)
        {
            monthName.Add(i, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames[i-1]);
        }

        return monthName;
    }

    #endregion

    #region ScriptMethods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string FindFamilies(byte statusid,byte familytypeid, string familysearch, byte roomtypeid, string strqty)
    {
        StringBuilder sb = new StringBuilder();
        Dictionary<int, int> familyIds = new Dictionary<int, int>();

        try
        {
            using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {

                var familiesQuery = (from f in entity.Families.Include("Rooms").Include("Xlk_Status").Include("Rooms.Xlk_RoomType")
                                     where (f.Xlk_FamilyType.FamilyTypeId != 2) && f.CampusId == GetCampusId
                                select f);

                familiesQuery = familiesQuery.OrderBy(e => e.FirstName).ThenBy(e => e.SurName);


                if (statusid > 0)
                    familiesQuery = familiesQuery.Where(e => e.Xlk_Status.StatusId == statusid);

                if (familytypeid > 0)
                    familiesQuery = familiesQuery.Where(e => e.Xlk_FamilyType.FamilyTypeId == familytypeid);

                if (!string.IsNullOrEmpty(familysearch))
                    familiesQuery = familiesQuery.Where(e => e.FirstName.Contains(familysearch) || e.SurName.Contains(familysearch));


                if (familiesQuery != null && familiesQuery.ToList().Count() > 0)
                {
                    using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
                    {
                        var familyQuery = gentity.Families.Select(p => new { Key = p.FamilyId, Value = p.ReservedFamilies.Count });
                        
                        if (!string.IsNullOrEmpty(strqty))
                        {
                            var qty = Convert.ToInt32(strqty);
                            familyQuery = familyQuery.Where(x => x.Value <= qty);
                        }
                        familyIds = familyQuery.AsEnumerable().ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

                    }

                    var families = from f in familiesQuery.ToList()
                                   where familyIds.ContainsKey(f.FamilyId)
                                   let qty = familyIds[f.FamilyId]
                                   select new { Family=f, Qty=qty };


                    foreach (var item in families)
                    {
                        sb.AppendFormat("<div id=\"{1}\" name=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}:{4}&nbsp;{2}<a target=\"_blank\" href={3}><img src=\"../Content/img/actions/printer.png\" title=\"Family Reservations\" style=\"float:right\"/></a><a href=\"#\" class=\"highlighter\"  onclick=\"ShowFamily({1})\"><img src=\"../Content/img/actions/orange_button.png\" alt=\"Highlight Family\" style=\"float:right\"/></a></div>", CreateName(item.Family.FirstName, item.Family.SurName), item.Family.FamilyId, RoomLink(item.Family.FamilyId, "~/Groups/FamilyReservationPage.aspx", CreateName(item.Family.FirstName, item.Family.SurName)), BasePage.ReportPath("AccommodationReports", "FamilyAllocationsAndReservations", "HTML4.0", new Dictionary<string, object> { { "FamilyId", item.Family.FamilyId } }), item.Qty);
                    }
                }
                return sb.ToString();
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string BuildGroupedFamilies(object groupid)
    {

        int _groupId = Convert.ToInt32(groupid);

        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            List<Family> groupedfamilieslist = (
                from f in entity.Families
                where f.ReservedFamilies.Any(x => x.GroupId == _groupId)
                orderby f.FirstName, f.SurName
                select f).ToList();

            StringBuilder sb = new StringBuilder();

            if (groupedfamilieslist != null && groupedfamilieslist.Count > 0)
            {
                int i = 1;
                foreach (Family family in groupedfamilieslist)
                {
                    sb.AppendFormat("<div id=\"f_{1}-g_{2}\" name=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{3}. {0}<a href=\"#\" onclick=\"RemoveFamily({1},{2})\"><img src=\"../Content/img/actions/delete.png\" alt=\"Remove from Group\" style=\"float:right\" id=\"DeleteGrouping\"/></a><a href=\"#\" class=\"highlighter\" onclick=\"ShowFamily({1})\"><img src=\"../Content/img/actions/orange_button.png\" alt=\"Highlight Family\" style=\"float:right\" /></a></div>", CreateName(family.FirstName, family.SurName), family.FamilyId, groupid, i++);
                }
            }

            return sb.ToString();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AssignFamilyToGroup(int familyid, int groupid)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                var fam = (from r in entity.ReservedFamilies.Include("Group").Include("Family")
                                     where r.GroupId == groupid
                                     select r).ToList();
                foreach (ReservedFamily item in fam)
                {
                    if (item.FamilyId == familyid)
                        return item.Family.FirstName + " " + item.Family.SurName + " already assigned to the " + item.Group.GroupName + " group.";
                }
                
                Family family = (from f in entity.Families
                                 where f.FamilyId == familyid
                                 select f).First();

                Group group = (from g in entity.Group
                               where g.GroupId == groupid
                               select g).First();

                ReservedFamily newfamily = ReservedFamily.CreateReservedFamily(familyid, groupid);
                newfamily.GroupReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Group", "GroupId", groupid);
                newfamily.FamilyReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Families", "FamilyId", familyid);
                newfamily.AssignedBy = Convert.ToByte(GetCurrentUser().UserId);
                newfamily.AssignedDate = DateTime.Now;
                entity.AddToReservedFamilies(newfamily);

                if (entity.SaveChanges() > 0)
                    return string.Empty;
                else
                    return "Could not save this change";
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteGroupedFamily(int familyid, int groupid)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                Family family = (from f in entity.Families
                                 where f.FamilyId == familyid
                                 select f).First();

                Group group = (from g in entity.Group
                               where g.GroupId == groupid
                               select g).First();

                if (family != null && group != null)
                {
                    var schedule = (from g in entity.ReservedFamilies
                                    where g.GroupId == groupid && g.FamilyId == familyid
                                    select g).First();

                    if (schedule != null)
                        entity.DeleteObject(schedule);
                }

                if (entity.SaveChanges() > 0)
                    return string.Empty;
                else
                    return "Could not remove assignment";
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    #endregion
}