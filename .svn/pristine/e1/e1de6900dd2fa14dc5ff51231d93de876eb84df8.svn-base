﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Tuition;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for BaseTuitionPage
/// </summary>
public partial class BaseTuitionPage : BasePage
{
    public TuitionEntities _entities { get; set; }
    
    public BaseTuitionPage()
	{
    }

    public static IList<Xlk_Building> LoadBuildings()
    {
        List<Xlk_Building> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Building>>("AllBuilding", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Building>>("AllBuilding", GetBuildings().ToList());
    }

    private static IQueryable<Xlk_Building> GetBuildings()
    {
        IQueryable<Xlk_Building> lookupQuery =
            from c in Entities.Xlk_Building
            select c;
        return lookupQuery;
    }

    public IList<ExamDates> LoadExamDates()
    {
        List<ExamDates> values = null;

        if (CacheManager.Instance.GetFromCache<List<ExamDates>>("AllExamDates", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<ExamDates>>("AllExamDates", GetExamDates().ToList());
    }

    public IList<Exam> LoadExams()
    {
        List<Exam> values = null;

        if (CacheManager.Instance.GetFromCache<List<Exam>>("AllEnrollmentExmas", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Exam>>("AllEnrollmentExams", GetExams().ToList());
    }

    private IQueryable<Exam> GetExams()
    {
        IQueryable<Exam> lookupQuery =
            from ex in Entities.Exam
            select ex;
        return lookupQuery;
    }
    private IQueryable<ExamDates> GetExamDates()
    {
        IQueryable<ExamDates> lookupQuery =
            from ed in Entities.ExamDates
            orderby ed.ExamDate ascending
            select ed;
        return lookupQuery;
    }

    #region Events

    public static TuitionEntities Entities
    {
        get
        {
            TuitionEntities value;

            if (CacheManager.Instance.GetFromCache<TuitionEntities>("ClassObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<TuitionEntities>("ClassObject", new TuitionEntities());
        }
    }

    public IList<TuitionLevel> LoadTuitionLevels()
    {
        List<TuitionLevel> values = null;

        if (CacheManager.Instance.GetFromCache<List<TuitionLevel>>("AllTuitionLevel", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<TuitionLevel>>("AllTuitionLevel", GetTuitionLevels().ToList());
    }

    private IQueryable<TuitionLevel> GetTuitionLevels()
    {
        IQueryable<TuitionLevel> lookupQuery =
            from l in Entities.TuitionLevel
            select l;
        return lookupQuery;
    }

    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllTuitionProgrammeType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllTuitionProgrammeType", GetProgrammeTypes().ToList());
    }
    private IQueryable<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        IQueryable<Xlk_ProgrammeType> lookupQuery =
            from p in Entities.Xlk_ProgrammeType
            select p;
        return lookupQuery;
    }

    public IList<Teacher> LoadTeachers()
    {
        List<Teacher> values = null;

        if (CacheManager.Instance.GetFromCache<List<Teacher>>("AllTeacher", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Teacher>>("AllTeacher", GetTeachers().ToList());
    }

    private IQueryable<Teacher> GetTeachers()
    {
        IQueryable<Teacher> lookupQuery =
            from t in Entities.Teacher
            orderby t.Name
            select t;
        return lookupQuery;
    }

    public IList<ClassRoom> LoadClassRooms()
    {
        List<ClassRoom> values = null;

        if (CacheManager.Instance.GetFromCache<List<ClassRoom>>("AllClassRoom", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<ClassRoom>>("AllClassRoom", GetClassRooms().ToList());
    }

    private IQueryable<ClassRoom> GetClassRooms()
    {
        IQueryable<ClassRoom> lookupQuery =
            from c in Entities.ClassRoom
            select c;
        return lookupQuery;
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadControl(string control, int id)
    {
        try
        {
            var page = new BaseEnrollmentPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            userControl.Parameters.Add("ClassId", id);

            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    #endregion
}
