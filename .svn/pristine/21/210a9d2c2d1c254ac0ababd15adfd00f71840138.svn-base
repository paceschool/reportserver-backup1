﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Agent;

public partial class Agent_AddRateCardCostingControl : BaseAgencyControl
{
    protected Int32? _agencyId;
    protected Int32? _rateCardId;
    protected Int32? _rateCardCostingId;
    protected Int32? _unitId;

    protected RateCardCosting _costing;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (AgentEntities entity = new AgentEntities())
        {
            if (GetValue("RateCardId") != null)
            {
                _rateCardId = Convert.ToInt32(GetValue("RateCardId"));
            }

            if (GetValue("UnitId") != null)
            {
                _unitId = (Int32)GetValue("UnitId");
            }

            if (GetValue("RateCardCostingId") != null)
            {
                _rateCardCostingId = (Int32)GetValue("RateCardCostingId");
                _costing = (from cards in entity.RateCardCostings.Include("Xlk_Unit")
                            where cards.RateCardCostingId == _rateCardCostingId
                            select cards).FirstOrDefault();
            }
            
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string IsEditMode
    {
        get
        {
            if ((_unitId.HasValue))
                return "disabled";

            return String.Empty;

        }
    }

    
    protected string GetRateCardCostingId
    {
        get
        {
            if (_costing != null)
                return _costing.RateCardCostingId.ToString();

            if (_rateCardCostingId.HasValue)
                return _rateCardCostingId.Value.ToString();

            return "0";
        }
    }

    protected string GetRateCardId
    {
        get
        {
            if (_costing != null)
                return _costing.RateCardId.ToString();
            if (_rateCardId.HasValue)
                return _rateCardId.Value.ToString();
            return "0";
        }
    }

    protected string GetQtyFrom
    {
        get
        {
            if (_costing != null)
                return _costing.QtyFrom.ToString();
            return "1";
        }
    }

    protected string GetQtyTo
    {
        get
        {
            if (_costing != null)
                return _costing.QtyTo.ToString();

            return string.Empty;
        }
    }


    protected string GetCostPer
    {
        get
        {
            if (_costing != null)
                return _costing.CostPer.ToString();
            return "0.00";
        }
    }


    protected string GetDiscount
    {
        get
        {
            if (_costing != null)
                return _costing.Discount.ToString();

            return "0.00";
        }
    }

    protected string GetAllowProRate
    {

        get
        {
            if (_costing != null)
                if (_costing.AllowProRate == true)
                    return "checked";

            return string.Empty;
        }

    }

    protected string GetSageRef
    {
        get
        {
            if (_costing != null)
                return _costing.SageRef;

            return string.Empty;
        }
    }

    protected string GetBaseCost
    {
        get
        {
            if (_costing != null)
                return _costing.BaseCost.ToString();

            return "0.00";
        }
    }

    protected string GetUnits()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_costing != null && _costing.Xlk_Unit != null) ? _costing.Xlk_Unit.UnitId : (byte?)null;

        if (_current == null)
            _current = GetRateCard();


        foreach (Xlk_Unit item in LoadUnits().OrderBy(x=>x.Description))
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UnitId, item.Description, (_current.HasValue && item.UnitId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    private byte? GetRateCard()
    {
        using (AgentEntities entity = new AgentEntities())
        {
            return (from r in entity.RateCards where r.RateCardId == _rateCardId select r.Xlk_Unit.UnitId).SingleOrDefault();
        }
    }


}