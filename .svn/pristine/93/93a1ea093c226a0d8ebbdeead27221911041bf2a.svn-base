﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using System.Data;
using Pace.DataAccess.Enrollment;
using System.Globalization;
using System.Threading;


public partial class AddStudentPage : BaseEnrollmentPage
{
    private Int32? _studentId;
    protected string strMessage;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["StudentId"]))
            _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!Page.IsPostBack)
        {
            if (Request.UrlReferrer != null)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();

            newnationality.DataSource = LoadNationalities();
            newnationality.DataBind();

            newagent.DataSource = LoadAgents();
            newagent.DataBind();

            newstatus.DataSource = LoadStatus();
            newstatus.DataBind();

            errorcontainer.Visible = false;
            gender.DataSource = LoadGender();
            gender.DataBind();

            newexam.DataSource = this.LoadExams();
            newexam.DataBind();

            stype.DataSource = LoadStudentTypes();
            stype.DataBind();

            listcampus.DataSource = BasePage.LoadCampus;
            listcampus.DataBind();
            listcampus.Items.FindByValue(GetCampusId.ToString()).Selected = true;

            if (_studentId.HasValue)
            {
               DisplayStudent(LoadStudent(new EnrollmentsEntities(),_studentId.Value));
            }
        }
    }

    #region private methods

    protected void Click_ClearFields(object sender, EventArgs e) //Clear all Fields
    {
        newfname.Text = ""; newsname.Text = "";
        newnationality.SelectedIndex = 0; newagent.SelectedIndex = 0; newstatus.SelectedIndex = 0;
        newarrdate.Text = ""; newdepdate.Text = ""; newplacement.Text = ""; newexam.Text = "";
        errorcontainer.Visible = false;
    }

    protected bool StudentExists(string firstName, string surName, DateTime dob)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            return entity.Student.Where(y => y.FirstName == firstName.Trim() && y.SurName == surName.Trim() && y.DateOfBirth == dob).Count() > 0;
        }
    }

    protected void Click_Save(object sender, EventArgs e) //Save edits or Save new Student
    {

        string _message = ""; errorcontainer.Visible = false;
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {

            if (StudentExists(newfname.Text, newsname.Text, Convert.ToDateTime(dob.Text)))
            {
                _message = "A student with this name and date of birth already exists.";
                errorcontainer.Visible = true;
                errormessage.Text = _message;
                return;
            }

            if (!string.IsNullOrEmpty(newfname.Text) && !string.IsNullOrEmpty(newsname.Text) && !string.IsNullOrEmpty(newarrdate.Text) &&
                !string.IsNullOrEmpty(newdepdate.Text) && Convert.ToInt32(newagent.SelectedItem.Value) != 0)
            {
                Student _student = GetStudent(entity);

                _student.FirstName =  newfname.Text.ToTitleCase();
                _student.SurName = newsname.Text.ToTitleCase();
                if (newexam.SelectedValue != "0")
                    _student.ExamReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Exam", "ExamId", Convert.ToInt16(newexam.SelectedValue));
                _student.PrepaidBook = chkbook.Checked;
                _student.PrepaidExam = chkexam.Checked;
                _student.Xlk_GenderReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Gender", "GenderId", Convert.ToByte(gender.SelectedItem.Value));
                if (!string.IsNullOrEmpty(dob.Text))
                    _student.DateOfBirth = Convert.ToDateTime(dob.Text);
                _student.Xlk_NationalityReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
                _student.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
                _student.DepartureDate = Convert.ToDateTime(newdepdate.Text);
                _student.CampusId = Convert.ToInt16(listcampus.SelectedItem.Value);

                if (!string.IsNullOrEmpty(newplacement.Text))
                    _student.PlacementTestResult = Convert.ToDecimal(newplacement.Text);
                if (newagent.SelectedIndex > 0)
                    _student.AgencyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
                _student.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));
                _student.Xlk_StudentTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_StudentType", "StudentTypeId", Convert.ToByte(stype.SelectedItem.Value));

                SaveStudent(entity,_student);

            }
            else
            {
                _message = "Not all fields are complete. Please check.";
                errorcontainer.Visible = true;
                errormessage.Text = _message;
            }
        }

    }

    private Student LoadStudent(EnrollmentsEntities entity, Int32 studentId) //Load Student if passed from Enrollment page
    {

        IQueryable<Student> studentQuery = from s in entity.Student.Include("Agency").Include("Xlk_Nationality")
                                           where s.StudentId == studentId
                                           select s;

        if (studentQuery.ToList().Count() > 0)
            return studentQuery.ToList().First();

        return null;
    }

    private void DisplayStudent(Student student) //Display loaded Student details
    {
        if (student != null)
        {
            newfname.Text = student.FirstName;
            newsname.Text = student.SurName;
            chkbook.Checked = student.PrepaidBook;
            chkexam.Checked = student.PrepaidExam;
            newexam.Items.FindByValue(student.Exam.ExamId.ToString()).Selected = true;
            dob.Text = (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("dd/MM/yyyy") : string.Empty;
            newarrdate.Text = student.ArrivalDate.ToString("dd/MM/yyyy");
            newdepdate.Text = (student.DepartureDate.HasValue) ? student.DepartureDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            newplacement.Text = student.PlacementTestResult.ToString();
            gender.Items.FindByValue(student.Xlk_Gender.GenderId.ToString()).Selected = true;
            newagent.Items.FindByValue(student.Agency.AgencyId.ToString()).Selected = true;
            newstatus.Items.FindByValue(student.Xlk_Status.StatusId.ToString()).Selected = true;
            newnationality.Items.FindByValue(student.Xlk_Nationality.NationalityId.ToString()).Selected = true;
            listcampus.Items.FindByValue(student.CampusId.ToString()).Selected = true;
        }
    }

    private Student GetStudent(EnrollmentsEntities entity) //Load Student or Create new Student
    {
        if (_studentId.HasValue)
            return LoadStudent(entity,_studentId.Value);

        return new Student();
    }

    private void SaveStudent(EnrollmentsEntities entity, Student student) //Final Save stage & Redirect
    {
        if (!_studentId.HasValue)
        {
            entity.AddToStudent(student);
        }

        if (entity.SaveChanges() > 0)
        {
            var getLastQuery = (from f in entity.Student
                               where f.GroupId == null
                               orderby f.StudentId descending
                               select f).First();

            Response.Redirect(string.Format("ViewStudentPage.aspx?StudentId=" + getLastQuery.StudentId));
        }


    }


    protected void CheckArrival(object sender, EventArgs e)
    {
         
        if (Convert.ToDateTime(newarrdate.Text) < DateTime.Now)
        {
             strMessage = "Arrival date is before today.  Is this correct?";
        }
            Response.Write("<script language='javascript'>alert('" + strMessage + ".');</script>");
    }

  #endregion  
}
