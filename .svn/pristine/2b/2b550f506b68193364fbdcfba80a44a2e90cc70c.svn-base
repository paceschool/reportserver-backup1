﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Agent;
using System.Text;
using System.Web.Services;

public partial class Agency_AddQuote : BaseAgencyControl
{
    protected Int32 _agentId;
    protected Quote _quote;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (AgentEntities entity = new AgentEntities())
        {
            if (GetValue("AgencyId") != null)
            {
                _agentId = (Int32)GetValue("AgencyId");
                _quote = (from qt in entity.Quotes.Include("Xlk_Status")
                          where qt.Agency.AgencyId == _agentId
                          select qt).FirstOrDefault();
            }
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetCreatedDate
    {
        get
        {
            if (_quote != null)
                return _quote.DateCreated.ToString("dd/MM/yyyy");

            return null;
        }
    }

    protected string GetArrivalDate
    {
        get
        {
            if (_quote != null)
                return _quote.ArrivalDate.ToString("dd/MM/yyyy");

            return null;
        }
    }
    protected string GetDepartureDate
    {
        get
        {
            if (_quote != null && _quote.DepartureDate.HasValue)
                return _quote.DepartureDate.Value.ToString("dd/MM/yyyy");

            return null;
        }
    }
    protected string GetSageInvoiceRef
    {
        get
        {
            if (_quote != null)
                return _quote.SageInvoiceRef.ToString();

            return null;
        }
    }

    protected string GetDescription
    {
        get
        {
            if (_quote != null)
                return _quote.Description;

            return null;
        }
    }

    protected string GetComment
    {
        get
        {
            if (_quote != null)
                return _quote.Comment;

            return null;
        }
    }

    protected string GetAgencyId
    {
        get
        {
            if (_quote != null)
                return _quote.Agency.AgencyId.ToString();

            if (GetValue("AgencyId") != null)
                return GetValue("AgencyId").ToString();

            return "0";
        }
    }

    protected string GetAgentList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_quote != null && _quote.Agency != null) ? _quote.Agency.AgencyId : (Int32?)null;
        

        foreach (Agency agent in LoadAgents())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", agent.AgencyId, agent.Name, (_current.HasValue && agent.AgencyId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    //protected string GetUserList()
    //{
    //    StringBuilder sb = new StringBuilder();

    //    Int32? _current = (_quote != null && _quote.CreatedByUser != null) ? _quote.CreatedByUser.UserId : (Int32?)null;

    //    foreach (User item in LoadUsers())
    //    {
    //        sb.AppendFormat("<option {2} value={0}>{1}</option>", item.UserId, item.Name, (_current.HasValue && item.UserId == _current) ? "selected" : string.Empty);
    //    }

    //    return sb.ToString();
    //}

    protected string GetStatusList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_quote != null && _quote.Xlk_Status != null) ? _quote.Xlk_Status.StatusId : (short?)null;

        foreach (Xlk_Status item in LoadStatus())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StatusId, item.Description, (_current.HasValue && item.StatusId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}