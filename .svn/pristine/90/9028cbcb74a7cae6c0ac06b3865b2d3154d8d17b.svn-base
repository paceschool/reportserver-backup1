﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using PaceManager.Enrollment;
using System.Data.Objects;
using System.Data;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;

public partial class HomePage : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

 

    protected DateTime FirstDayOfWeek(DateTime date, CalendarWeekRule rule)
    {

        int weeknumber = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, rule, DayOfWeek.Monday);

        DateTime jan1 = new DateTime(date.Year, 1, 1);

        int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        DateTime firstMonday = jan1.AddDays(daysOffset);

        var cal = CultureInfo.CurrentCulture.Calendar;
        int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

        if (firstWeek <= 1)
        {
            weeknumber -= 1;
        }

        DateTime result = firstMonday.AddDays(weeknumber * 7);

        return result;

    }


    private static IEnumerable<object> ShowStudentArrivalEvents(DateTime start, DateTime end)
    {
        var studentQuery = from s in BaseEnrollmentPage.Entities.Enrollment.Include("Student")
                           where ((s.Student.ArrivalDate >= start && s.Student.ArrivalDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                           select new { Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode, ArrivalDate = s.Student.ArrivalDate, DepartureDate = s.Student.DepartureDate, Id = s.Student.StudentId};

        foreach (var item in studentQuery.ToList())
        {
            yield return new { Title = item.Title, ArrivalDate = item.ArrivalDate.ToLongDateString(), URL = string.Format("{0}?StudentId={1}", VirtualPathUtility.ToAbsolute("~/Enrollments/ViewStudentPage.aspx"), item.Id) };
        }
    }
    private static IEnumerable<object> ShowStudentDepartureEvents(DateTime start, DateTime end)
    {
        var studentQuery = from s in BaseEnrollmentPage.Entities.Enrollment.Include("Student")
                           where ((s.Student.DepartureDate >= start && s.Student.DepartureDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                           select new { Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode + (s.Student.PrepaidBook ? "-Book Return!" : ""), ArrivalDate = s.Student.ArrivalDate, DepartureDate = s.Student.DepartureDate, Id = s.Student.StudentId };

        foreach (var item in studentQuery.ToList())
        {
            yield return new { Title = item.Title, ArrivalDate = item.DepartureDate.Value.ToLongDateString(), URL = string.Format("{0}?StudentId={1}", VirtualPathUtility.ToAbsolute("~/Enrollments/ViewStudentPage.aspx"), item.Id) };
        }
    }
    private static IEnumerable<object> ShowGroupEvents(DateTime start, DateTime end)
    {
        var studentQuery = from g in BaseGroupPage.Entities.Group
                           where ((g.ArrivalDate >= start && g.ArrivalDate <= end))
                           select new { Title = g.GroupName, ArrivalDate = g.ArrivalDate, DepartureDate = g.DepartureDate, Id = g.GroupId };

        foreach (var item in studentQuery.ToList())
        {
            yield return new { Title = item.Title, ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString(), URL = string.Format("{0}?GroupId={1}", VirtualPathUtility.ToAbsolute("~/Groups/ViewGroupPage.aspx"), item.Id) };
        }
    }
    private static IEnumerable<object> ShowTaskEvents(DateTime start, DateTime end)
    {
        var taskQuery = from g in BaseSupportPage.Entities.Task
                           where ((g.DateDue >= start && g.DateDue <= end && g.Xlk_Status.StatusId !=3))
                           select new { Title = g.Description,ArrivalDate = g.RaisedDate, DepartureDate = g.DateDue, Id = g.TaskId };

        foreach (var item in taskQuery.ToList())
        {
            yield return new { Title = string.Format("{0}", item.Title), ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString(), URL = string.Format("{0}?TaskId={1}", VirtualPathUtility.ToAbsolute("~/Support/ViewTaskPage.aspx"), item.Id) };
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> ListCalendarEvents(DateTime start, DateTime end, string action)
    {
        switch (action.ToLower())
        {
            case "studentarrivals":
                return ShowStudentArrivalEvents(start, end);
            case "studentdepartures":
                return ShowStudentDepartureEvents(start, end);
            case "group":
                return ShowGroupEvents(start, end);
            case "tasks":
                return ShowTaskEvents(start, end);
            default:
                return null;
        }
    }
}