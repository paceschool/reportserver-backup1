﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Transport;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Collections.Specialized;
using System.Data.Objects;
using System.Data;
using Pace.Common;

/// <summary>
/// Summary description for TransportBasePage
/// </summary>
public class BaseTransportPage : BasePage
{
    public TransportEntities _entities { get; set; }

    public BaseTransportPage()
    {
    }

    public static TransportEntities CreateEntity
    {
        get
        {
            return new TransportEntities();
        }
    }

    public static IList<T> GetFromCache<T>(TransportEntities entity)
    {
        IList<T> values = null;

        if (CacheManager.Instance.GetFromCache<IList<T>>(string.Format("{0}.{1}", entity.DefaultContainerName, typeof(T).ToString()), out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<T>>(string.Format("{0}.{1}", entity.DefaultContainerName, typeof(T).ToString()), GetFromStore<T>(entity));
    }

    public static IList<T> GetFromStore<T>(TransportEntities entity)
    {
        switch (typeof(T).ToString().ToLower())
        {
            case "Pace.DataAccess.transport.xlk_ststus":
                return (IList<T>)(from e in entity.Xlk_Status select e).ToList();
            case "Pace.DataAccess.transport.xlk_triptype":
                return (IList<T>)(from e in entity.Xlk_TripType select e).ToList();
            case "Pace.DataAccess.transport.xlk_vehicletype":
                return (IList<T>)(from e in entity.Xlk_VehicleType select e).ToList();
            case "Pace.DataAccess.transport.xlk_location":
                return (IList<T>)(from e in entity.Xlk_Location select e).ToList();
            default:
                return null;
        }

    }

    public Dictionary<int, string> LoadTransportSuppliers()
    {
        return GetTransportSuppliers();
    }

    private Dictionary<int,string> GetTransportSuppliers()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<TransportSupplier> lookupQuery =
                from d in entity.TransportSuppliers
                orderby d.Supplier.Title
                select d;
            return lookupQuery.ToDictionary(o => o.TransportSupplierId, o => o.Supplier.Title);
        }
    }

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string FindVehicles(int qty, short dropofflocationid, short pickuplocationid, byte triptypeid)
    {
        try
        {
            StringBuilder _reposnse = new StringBuilder();
            using (TransportEntities entity = new TransportEntities())
            {

                var options = (entity.FindTransportVehicles(dropofflocationid, pickuplocationid, triptypeid, qty).ToList().Select(s => s.VehiclePricePlanId));

                var studentQuery = from s in entity.VehiclePricePlans.Include("Vehicles")
                                   where options.Contains(s.VehiclePricePlanId)
                                   select new { s.VehiclePricePlanId, VehicleId = s.Vehicle.VehicleId, Supplier = s.Vehicle.TransportSupplier.Supplier.Title, VehicleType = s.Vehicle.Xlk_VehicleType.Description ,Cost=s.Cost};

                foreach (var vehicle in studentQuery.ToList())
                {
                    _reposnse.AppendFormat("<label style=\"display:block;\"><input name=\"VehicleList\" id=\"VehiclePricePlanId\"  style=\"width:50px;float:left;\" type=\"checkbox\" name=\"VehicleList\" value=\"{0}\" />{1} {2} :cost ({3})</label><br />", vehicle.VehiclePricePlanId, vehicle.Supplier, vehicle.VehicleType, vehicle.Cost);
                }
            }
            return _reposnse.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadTransfers()
    {
        List<byte> transferTypes = new List<byte>() {1,2};
        DateTime _today = DateTime.Now.AddDays(-14).Date;
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Type</th><th scope=\"col\">Name</th><th scope=\"col\">Date / Time</th><th scope=\"col\">Flight Number</th><th scope=\"col\">Comment</th><th scope=\"col\">Transport Type</th><th scope=\"col\">Actions</th></tr></thead><tbody>", BasePage.ReportServerURL));

            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                var query = from n in entity.StudentTransfer.Include("Student").Include("Xlk_TransferType")
                            where (!n.TransportBookingId.HasValue) && (n.Xlk_BusinessEntity.EntityId == 1) && transferTypes.Contains(n.Xlk_TransferType.TransferTypeId) && n.Date >= _today
                            select n;

                if (query.Count() > 0)
                {
                    foreach (Pace.DataAccess.Enrollment.StudentTransfer transfer in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3} / {4}</td> <td style=\"text-align: left\">{5}</td> <td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: left\"><a title=\"Make Booking\" href=\"#\" onclick=\"loadEditControl('#dialog-form','PlanBookingsPage.aspx','Transport/AddTransportBookingControl.ascx','StudentTransferId',{0})\"><img src=\"../Content/img/actions/next.png\"></a></td></tr>", transfer.StudentTransferId, "Individual", CreateName(transfer.Student.FirstName, transfer.Student.SurName), transfer.Date.ToString("D"), transfer.Time, transfer.FlightNumber, transfer.Comment, transfer.Xlk_TransferType.Description);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Enrollments to Show!</td></tr>");
                }
            }


            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                var query = from g in entity.GroupTransfer.Include("Group").Include("Xlk_TransferType")
                            where (!g.TransportBookingId.HasValue) && (g.Xlk_BusinessEntity.EntityId == 1) && g.Date >= _today
                            select g;

                if (query.Count() > 0)
                {
                    foreach (Pace.DataAccess.Group.GroupTransfer transfer in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3} / {4}</td> <td style=\"text-align: left\">{5}</td> <td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: left\"><a title=\"Make Booking\" href=\"#\" onclick=\"loadEditControl('#dialog-form','PlanBookingsPage.aspx','Transport/AddTransportBookingControl.ascx','GroupTransferId',{0})\"><img src=\"../Content/img/actions/next.png\"></a></td></tr>", transfer.GroupTransferId, "Group", CreateName(transfer.Group.GroupName, string.Empty), transfer.Date.ToString("D"), transfer.Time, transfer.FlightNumber, transfer.Comment, transfer.Xlk_TransferType.Description);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Enrollments to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
        
    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadExcursions()
    {
        List<byte> transferTypes = new List<byte>() {1,2};
        DateTime _today = DateTime.Now.AddDays(-14).Date;
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\"> Id </th><th scope=\"col\"> Title </th><th scope=\"col\"> Date - Time </th><th scope=\"col\"> Transport Type </th><th scope=\"col\"> Meet Time </th><th scope=\"col\"> Meet Location </th><th scope=\"col\"> Confirmed Date <th scope=\"col\"> Confirmed By </th><th scope=\"col\"> Voucher Id </th><th scope=\"col\"> Qty Pace Guides </th><th scope=\"col\"> Actions </th></tr></thead><tbody >", BasePage.ReportServerURL));

            using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
            {
                var query = from n in entity.Booking.Include("Xlk_Status").Include("Xlk_TransportType")
                            where n.Xlk_Status.StatusId == 2 && n.Xlk_TransportType.TransportTypeId == 4 && (!n.TransportBookingId.HasValue) && n.ExcursionDate > _today
                            select n;

                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Transport/ViewBookingPage.aspx?BookingId={0}\">{1}</a></td><td>{2} - {3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td></td><td>{9}</td><td style=\"text-align: center\"><a title=\"Create Booking\" href=\"#\" onclick=\"loadEditControl('#dialog-form','PlanBookingsPage.aspx','Transport/AddTransportBookingControl.ascx','TransportBookingId',{0})\"><img src=\"../Content/img/actions/next.png\"></a></td></tr>", booking.BookingId, TrimText(booking.Title, 30), booking.ExcursionDate.ToString("dd/MM/yyyy"), booking.ExcursionTime, booking.Xlk_TransportType.Description, booking.MeetTime, booking.MeetLocation, (booking.ConfirmedDate.HasValue) ? booking.ConfirmedDate.Value.ToString("dd/MM/yyyy") : "Not Confirmed", booking.ConfirmedBy, booking.QtyGuides);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Enrollments to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadVehicles(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewTransportSupplierPage.aspx','Transport/AddNewVehicleControl.ascx','TransportSupplierId',{0})\"><img src=\"../Content/img/actions/add.png\" />Add New Vehicle</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Vehicle Id</th><th scope=\"col\">Max Capacity</th><th scope=\"col\">Min Capacity</th><th scope=\"col\">Type </th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));
            using (TransportEntities entity = new TransportEntities())
            {
                var query = from h in entity.Vehicles.Include("Xlk_VehicleType")
                            where h.TransportSupplier.TransportSupplierId == id
                            select h;


                if (query.Count() > 0)
                {
                    foreach (Vehicle vehicle in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: center\"><a title=\"Edit Vehicle\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewTransportSupplierPage.aspx','Transport/AddNewVehicleControl.ascx','VehicleId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Vehicle\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewTransportSupplierPage.aspx','Vehicle',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", vehicle.VehicleId, vehicle.MaxCapacity, vehicle.MinCapacity, vehicle.Xlk_VehicleType.Description);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadPricePlans(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewTransportSupplierPage.aspx','Transport/AddNewPricePlanControl.ascx','TransportSupplierId',{0})\"><img src=\"../Content/img/actions/add.png\" />Add New Price Plan</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Vehicle PricePlanId</th><th scope=\"col\">Vehicle Type</th><th scope=\"col\">Dropoff Location</th><th scope=\"col\">Collection Location </th><th scope=\"col\">Capacity Max/Min</th><th scope=\"col\">Cost</th><th scope=\"col\">Trip Type</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));
            using (TransportEntities entity = new TransportEntities())
            {
                var query = from h in entity.VehiclePricePlans.Include("Vehicle").Include("Xlk_TripType").Include("PickupLocation").Include("DropoffLocation")
                            where h.Vehicle.TransportSupplier.TransportSupplierId == id
                            select h;

                if (query.Count() > 0)
                {
                    foreach (VehiclePricePlan priceplan in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}/{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: center\"><a title=\"Edit Transfer\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewTransportSupplierPage.aspx','Transport/AddNewPricePlanControl.ascx','VehiclePricePlanId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Transfer\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewTransportSupplierPage.aspx','VehiclePricePlan',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", priceplan.VehiclePricePlanId, priceplan.Vehicle.Xlk_VehicleType.Description, priceplan.DropoffLocation.Description, priceplan.PickupLocation.Description, priceplan.MaxCapacity, priceplan.MinCapacity, priceplan.Cost.Value.ToString("€00.00"), priceplan.Xlk_TripType.Description);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadBookings(int id,string type)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">BookingId</th><th scope=\"col\">Date / Time</th><th scope=\"col\">Passengers</th><th scope=\"col\">Max/Min</th><th scope=\"col\">Vehicle Type</th><th scope=\"col\">Cost</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id, BasePage.ReportServerURL));
            using (TransportEntities entity = new TransportEntities())
            {
                var query = from b in entity.Bookings
                            from vb in b.VehicleBookings
                            where vb.Vehicle.TransportSupplier.TransportSupplierId == id
                            select new {Booking=b,Cost=vb.Cost, Type=vb.Vehicle.Xlk_VehicleType.Description,Vehicle=vb.Vehicle };

                if (type=="current")
                    query = query.Where(s=>s.Booking.RequestedDate >= DateTime.Now);
                else
                    query = query.Where(s => s.Booking.RequestedDate < DateTime.Now);


                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1} @ {2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4} - {5}</td> <td style=\"text-align: left\">{6}</td><td style=\"text-align: center\"><a title=\"Edit Transfer\" href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewTransportSupplierPage.aspx','Transport/AddNewPricePlanControl.ascx','GroupTransferId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Transfer\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewTransportSupplierPage.aspx','GroupTransfer',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", booking.Booking.BookingId, booking.Booking.RequestedDate, booking.Booking.RequestedTime, booking.Booking.RequestedDate, booking.Booking.NumberOfPassengers, booking.Vehicle.MaxCapacity, booking.Vehicle.MinCapacity, booking.Type,booking.Cost);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    
    [WebMethod]
    public static string SaveVehicle(Vehicle newObject)
    {
        try
        {
            Vehicle newplan = new Vehicle();
            using (TransportEntities entity = new TransportEntities())
            {
                if (newObject.VehicleId > 0)
                    newplan = (from c in entity.Vehicles where c.VehicleId == newObject.VehicleId select c).FirstOrDefault();

                newplan.MaxCapacity = newObject.MaxCapacity;
                newplan.MinCapacity = newObject.MinCapacity;

                newplan.Xlk_VehicleTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_VehicleType", newObject.Xlk_VehicleType);
                newplan.TransportSupplierReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".TransportSuppliers", newObject.TransportSupplier);


                if (newObject.VehicleId == 0)
                    entity.AddToVehicles(newplan);

                entity.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SavePricePlan(VehiclePricePlan newObject)
    {
        try
        {
            VehiclePricePlan newplan = new VehiclePricePlan();
            using (TransportEntities entity = new TransportEntities())
            {
                if (newObject.VehiclePricePlanId > 0)
                    newplan = (from c in entity.VehiclePricePlans where c.VehiclePricePlanId == newObject.VehiclePricePlanId select c).FirstOrDefault();

                newplan.Cost = newObject.Cost;
                newplan.MaxCapacity = newObject.MaxCapacity;
                newplan.MinCapacity = newObject.MinCapacity;

                newplan.DropoffLocationReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Location", newObject.DropoffLocation);
                newplan.PickupLocationReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Location", newObject.PickupLocation);
                newplan.VehicleReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Vehicles", newObject.Vehicle);
                newplan.Xlk_TripTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_TripType", newObject.Xlk_TripType);

                if (newObject.VehiclePricePlanId == 0)
                    entity.AddToVehiclePricePlans(newplan);

                entity.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveTransportBooking(_Booking newObject)
    {
        try
        {
            Booking newplan = new Booking();
            using (TransportEntities entity = new TransportEntities())
            {
                if (newObject.VehicleList.Count > 0)
                {

                    if (newObject.BookingId > 0)
                    {
                        newplan = (from c in entity.Bookings where c.BookingId == newObject.BookingId select c).FirstOrDefault();
                        newplan.Xlk_StatusReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Status", newObject.Xlk_Status);
                    }
                    else
                        newplan.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)1);


                    newplan.CollectionInfo = newObject.CollectionInfo;
                    newplan.DropoffInfo = newObject.DropoffInfo;
                    newplan.NumberOfPassengers = newObject.NumberOfPassengers;
                    newplan.RequestedDate = newObject.RequestedDate;
                    newplan.RequestedTime = newObject.RequestedTime;

                    if (newObject.DropoffLocation != null)
                        newplan.DropoffLocationReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Location", newObject.DropoffLocation);
                    if (newObject.PickupLocation != null)
                        newplan.PickupLocationReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Location", newObject.PickupLocation);
                    if (newObject.RepCollectionLocation != null)
                        newplan.RepCollectionLocationReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_Location", newObject.RepCollectionLocation);

                    newplan.Xlk_TripTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_TripType", newObject.Xlk_TripType);

                    if (newObject.BookingId == 0)
                        entity.AddToBookings(newplan);

                    if (entity.SaveChanges() > 0)
                    {
                        foreach (VehiclePricePlan vehiclePricePlan in newObject.VehicleList)
                        {
                            VehiclePricePlan plan = entity.VehiclePricePlans.Include("Vehicle").Single(s => s.VehiclePricePlanId == vehiclePricePlan.VehiclePricePlanId);
                            VehicleBooking vbooking = new VehicleBooking();
                            vbooking.BookingId = newplan.BookingId;
                            vbooking.VehicleId = plan.Vehicle.VehicleId;
                            vbooking.Cost = plan.Cost;
                            entity.AddToVehicleBookings(vbooking);
                        }
                        if (entity.SaveChanges() > 0)
                        {

                            if (newObject.StudentTransferId.HasValue)
                            {
                                using (Pace.DataAccess.Enrollment.EnrollmentsEntities enty = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
                                {
                                    enty.StudentTransfer.Single(x => x.StudentTransferId == newObject.StudentTransferId.Value).TransportBookingId = newplan.BookingId;
                                    if (enty.SaveChanges() > 0)
                                        return string.Empty;
                                }
                            }
                            if (newObject.GroupTransferId.HasValue)
                            {
                                using (Pace.DataAccess.Group.GroupEntities enty = new Pace.DataAccess.Group.GroupEntities())
                                {
                                    enty.GroupTransfer.Single(x => x.GroupTransferId == newObject.GroupTransferId.Value).TransportBookingId = newplan.BookingId;
                                    if (enty.SaveChanges() > 0)
                                        return string.Empty;
                                }
                            }
                            if (newObject.ExcursionBookingId.HasValue)
                            {
                                using (Pace.DataAccess.Excursion.ExcursionEntities enty = new Pace.DataAccess.Excursion.ExcursionEntities())
                                {
                                    enty.Booking.Single(x => x.BookingId == newObject.ExcursionBookingId.Value).TransportBookingId = newplan.BookingId;
                                    if (enty.SaveChanges() > 0)
                                        return string.Empty;
                                }
                            }
                        }

                    }
                }
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadConfirmed()
    {
        DateTime _now = DateTime.Now.Date;

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Trip Type</th><th scope=\"col\"> Date - Time </th><th scope=\"col\">Pickup</th><th scope=\"col\">Actions</th></tr></thead><tbody >", BasePage.ReportServerURL));

            using (TransportEntities entity = new TransportEntities())
            {

                var query = (from n in entity.Bookings
                             from v in n.VehicleBookings
                             where n.Xlk_Status.StatusId == 2
                             orderby n.RequestedDate
                             select new { n.BookingId, n.NumberOfPassengers, Date = n.RequestedDate, Time = n.RequestedTime, TripType = n.Xlk_TripType.Description, PickupLocation = n.PickupLocation.Description, DropoffLocation = n.DropoffLocation.Description, v.VehicleId, VehicleSupplier = v.Vehicle.TransportSupplier.Supplier.Title, VehicleType = v.Vehicle.Xlk_VehicleType.Description }).ToList();

                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Excursions/ViewBookingPage.aspx?BookingId={0}\">{1}:- {2} {3} to {4}</a></td><td>on {5} @{6}</td><td>{7}</td><td style=\"text-align: center\"><a title=\"Delete Booking\" href=\"#\" onclick=\"deleteBooking({0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>&nbsp;<a title=\"Edit Booking\" href=\"#\" onclick=\"loadEditControl('#dialog-form','PlanBookingsPage.aspx','Transport/AddTransportBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Confirm Booking\" href=\"#\" onclick=\"confirmBooking({0},'conmfirmbooked')\"><img src=\"../Content/img/actions/accept.png\"></a></td></tr>", booking.BookingId, booking.VehicleSupplier, booking.VehicleType, booking.TripType, booking.DropoffLocation, booking.Date.ToString("dd MMM yy"), booking.Time.ToString("hh\\:mm"), booking.PickupLocation);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadToBeConfirmed()
    {
        DateTime _now = DateTime.Now.Date;

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Trip Type</th><th scope=\"col\"> Date - Time </th><th scope=\"col\">Pickup</th><th scope=\"col\">Actions</th></tr></thead><tbody >", BasePage.ReportServerURL));

            using (TransportEntities entity = new TransportEntities())
            {

                var query = (from n in entity.Bookings
                                       from v in n.VehicleBookings
                                       where n.Xlk_Status.StatusId == 1
                                       orderby n.RequestedDate
                             select new { n.BookingId, n.NumberOfPassengers,Date=n.RequestedDate, Time=n.RequestedTime, TripType = n.Xlk_TripType.Description, PickupLocation = n.PickupLocation.Description, DropoffLocation = n.DropoffLocation.Description, v.VehicleId, VehicleSupplier = v.Vehicle.TransportSupplier.Supplier.Title, VehicleType=v.Vehicle.Xlk_VehicleType.Description }).ToList();
                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Transport/ViewBookingPage.aspx?BookingId={0}\">{1}:- {2} {3} to {4}</a></td><td>on {5} @{6}</td><td>{7}</td><td style=\"text-align: center\"><a title=\"Delete Booking\" href=\"#\" onclick=\"deleteBooking({0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a>&nbsp;<a title=\"Edit Booking\" href=\"#\" onclick=\"loadEditControl('#dialog-form','PlanBookingsPage.aspx','Transport/AddTransportBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Confirm Booking\" href=\"#\" onclick=\"confirmBooking({0},'conmfirmbooked')\"><img src=\"../Content/img/actions/accept.png\"></a></td></tr>", booking.BookingId, booking.VehicleSupplier, booking.VehicleType, booking.TripType, booking.DropoffLocation, booking.Date.ToString("dd MMM yy"), booking.Time.ToString("hh\\:mm"), booking.PickupLocation);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadDeleted()
    {
        DateTime _now = DateTime.Now.Date;

        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Id</th><th scope=\"col\">Trip Type</th><th scope=\"col\"> Date - Time </th><th scope=\"col\">Pickup</th><th scope=\"col\">Actions</th></tr></thead><tbody >", BasePage.ReportServerURL));

            using (TransportEntities entity = new TransportEntities())
            {

                var query = (from n in entity.Bookings
                             from v in n.VehicleBookings
                             where n.Xlk_Status.StatusId == 3
                             orderby n.RequestedDate
                             select new { n.BookingId, n.NumberOfPassengers, Date = n.RequestedDate, Time = n.RequestedTime, TripType = n.Xlk_TripType.Description, PickupLocation = n.PickupLocation.Description, DropoffLocation = n.DropoffLocation.Description, v.VehicleId, VehicleSupplier = v.Vehicle.TransportSupplier.Supplier.Title, VehicleType = v.Vehicle.Xlk_VehicleType.Description }).ToList();

                if (query.Count() > 0)
                {
                    foreach (var booking in query.ToList())
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td><a href=\"../Transport/ViewBookingPage.aspx?BookingId={0}\">{1}:- {2} {3} to {4}</a></td><td>on {5} @{6}</td><td>{7}</td><td style=\"text-align: center\"><a title=\"Edit Booking\" href=\"#\" onclick=\"loadEditControl('#dialog-form','PlanBookingsPage.aspx','Transport/AddTransportBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Confirm Booking\" href=\"#\" onclick=\"confirmBooking({0},'conmfirmcancelled')\"><img src=\"../Content/img/actions/accept.png\"></a></td></tr>", booking.BookingId, booking.VehicleSupplier, booking.VehicleType, booking.TripType, booking.DropoffLocation, booking.Date.ToString("dd MMM yy"), booking.Time.ToString("hh\\:mm"), booking.PickupLocation);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"11\">No Bookings to Show!</td></tr>");
                }
            }

            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }


    [WebMethod]
    public static string ConfirmBooking(int bookingid, string action)
    {
        Booking booking;
        try
        {
            using (TransportEntities entity = new TransportEntities())
            {
                if (bookingid > 0)
                {
                    booking = (from e in entity.Bookings.Include("Xlk_Status") where e.BookingId == bookingid select e).FirstOrDefault();

                    switch (action)
                    {
                        case "conmfirmcancelled":
                            booking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)4); // confirmed cancelled statusid is 4
                            break;
                        case "conmfirmbooked":
                            booking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)2); // confirmed statusid is 2
                            break;
                        default:
                            break;
                    }

                    booking.ConfirmedDate = DateTime.Now;



                    if (entity.SaveChanges() > 0)
                        return string.Empty;
                }
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }


    [WebMethod]
    public static string DeleteBooking(int bookingid)
    {
        Booking booking;
        try
        {
            using (TransportEntities entity = new TransportEntities())
            {
                if (bookingid > 0)
                {
                    booking = (from e in entity.Bookings.Include("Xlk_Status") where e.BookingId == bookingid select e).FirstOrDefault();


                    booking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", (byte)3); // tobecancelled statusid is 2

                    if (entity.SaveChanges() > 0)
                        return string.Empty;
                }
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    #endregion

    public class _Booking : Booking
    {
        public int? GroupTransferId { get; set; }
        public int? StudentTransferId { get; set; }
        public int? ExcursionBookingId { get; set; }
        public List<VehiclePricePlan> VehicleList { get; set; }
    }
}

