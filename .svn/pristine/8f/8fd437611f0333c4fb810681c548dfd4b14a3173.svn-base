﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Enrollment;
using System.Text;

/// <summary>
/// Summary description for BaseEnrollmentControl
/// </summary>
public class BaseEnrollmentControl : BaseControl
{
    public EnrollmentsEntities _entities { get; set; }

    public BaseEnrollmentControl()
    {
       
    }


    public static EnrollmentsEntities Entities
    {
        get
        {
            EnrollmentsEntities value;

            if (CacheManager.Instance.GetFromCache<EnrollmentsEntities>("EnrollmentsEntitiesObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<EnrollmentsEntities>("EnrollmentsEntitiesObject", new EnrollmentsEntities());

        }
    }

    public IList<Xlk_NoteType> LoadNoteTypes()
    {
        List<Xlk_NoteType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_NoteType>>("AllNoteType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_NoteType>>("AllNoteType", GetNoteType().ToList());
    }
    private IQueryable<Xlk_NoteType> GetNoteType()
    {

        IQueryable<Xlk_NoteType> lookupQuery =
            from p in Entities.Xlk_NoteType
            select p;
        return lookupQuery;

    }
    public IList<Xlk_ComplaintType> LoadComplaintTypes()
    {
        List<Xlk_ComplaintType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ComplaintType>>("AllComplaintType", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ComplaintType>>("AllComplaintType", GetComplaintType().ToList());
    }
    private IQueryable<Xlk_ComplaintType> GetComplaintType()
    {

        IQueryable<Xlk_ComplaintType> lookupQuery =
            from p in Entities.Xlk_ComplaintType
            select p;
        return lookupQuery;

    }
    public IList<Xlk_Severity> LoadSeverity()
    {
        List<Xlk_Severity> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Severity>>("AllSeverity", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Severity>>("AllSeverity", GetSeveity().ToList());
    }
    private IQueryable<Xlk_Severity> GetSeveity()
    {

        IQueryable<Xlk_Severity> lookupQuery =
            from p in Entities.Xlk_Severity
            select p;
        return lookupQuery;

    }
    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        List<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_ProgrammeType>>("AllProgrammeTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_ProgrammeType>>("AllProgrammeTypes", GetProgrammeTypes().ToList());
    }

    private IQueryable<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        IQueryable<Xlk_ProgrammeType> lookupQuery =
            from b in Entities.Xlk_ProgrammeType
            select b;
        return lookupQuery;
    }
    public IList<Xlk_Location> LoadLocations()
    {
        List<Xlk_Location> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_Location>>("AllLocationTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_Location>>("AllLocationTypes", GetLocations().ToList());
    }

    private IQueryable<Xlk_Location> GetLocations()
    {
        IQueryable<Xlk_Location> lookupQuery =
            from b in Entities.Xlk_Location
            select b;
        return lookupQuery;
    }
    public IList<Xlk_TransferType> LoadTransferTypes()
    {
        List<Xlk_TransferType> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_TransferType>>("AllTransferTypes", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_TransferType>>("AllTransferTypes", GetTransferTypes().ToList());
    }

    private IQueryable<Xlk_TransferType> GetTransferTypes()
    {
        IQueryable<Xlk_TransferType> lookupQuery =
            from b in Entities.Xlk_TransferType
            select b;
        return lookupQuery;
    }
    public IList<Xlk_LessonBlock> LoadLessonBlocks()
    {
        List<Xlk_LessonBlock> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_LessonBlock>>("AllEnrollmentLessonBlocks", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_LessonBlock>>("AllEnrollmentLessonBlocks", GetLessonBlocks().ToList());
    }

    private IQueryable<Xlk_LessonBlock> GetLessonBlocks()
    {
        IQueryable<Xlk_LessonBlock> lookupQuery =
            from a in Entities.Xlk_LessonBlock
            select a;
        return lookupQuery;
    }

    public IList<Xlk_BusinessEntity> LoadBusinessEntity()
    {
        List<Xlk_BusinessEntity> values = null;

        if (CacheManager.Instance.GetFromCache<List<Xlk_BusinessEntity>>("AllEnrollmentBusinessEntity", out values))
            return values;

        return CacheManager.Instance.AddToCache<List<Xlk_BusinessEntity>>("AllEnrollmentBusinessEntity", GetBusinessEntity().ToList());
    }

    private IQueryable<Xlk_BusinessEntity> GetBusinessEntity()
    {
        IQueryable<Xlk_BusinessEntity> lookupQuery =
            from a in Entities.Xlk_BusinessEntity
            select a;
        return lookupQuery;
    }

    protected string GetComplaintTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_ComplaintType item in LoadComplaintTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.ComplaintTypeId, item.Description);
        }

        return sb.ToString();

    }
    protected string GetSeverityList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_Severity item in LoadSeverity())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.SeverityId, item.Description);
        }

        return sb.ToString();

    }
    protected string GetNoteTypeList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Xlk_NoteType item in LoadNoteTypes())
        {
            sb.AppendFormat("<option value={0}>{1}</option>", item.NoteTypeId, item.Description);
        }

        return sb.ToString();

    }
}

