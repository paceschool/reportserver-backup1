﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using PaceManager.Security;
using System.Globalization;
using System.Configuration;

/// <summary>
/// Summary description for BasePage
/// </summary>
public partial class BasePage : System.Web.UI.Page
{
	public BasePage()
	{
	}

    protected string ReportPath(string ReportName, string format, Dictionary<string, object> _params)
    {

        return string.Format("{0}?/PaceManagerReports/{1}{2}&rs:Command=Render&rs:Format={3}&rc:Parameters=False", BasePage.ReportServerURL, ReportName, _params.ToQueryString(), format);

    }
    protected string ReportPath(string ReportName, Dictionary<string, object> _params)
    {
        return string.Format("{0}?/PaceManagerReports/{1}{2}&rs:Command=Render&rs:Format=WORD&rc:Parameters=False", BasePage.ReportServerURL, ReportName, _params.ToQueryString());

    }

    protected static string ReportServerURL
    {
        get
        {
            return ConfigurationManager.AppSettings["ReportServerURL"];
        }
    }

    protected static string DateFormatString
    {
        get
        {
            return ConfigurationManager.AppSettings["DateFormatString"];

        }
    }

    protected static string CreateName(object firstName, object secondName)
    {
        if (firstName != null && secondName != null)
        {
            return string.Format("{0} {1}", firstName, secondName);
        }
        else
        {
            return (firstName != null) ? firstName.ToString() : secondName.ToString();
        }
    }

    public static DateTime FirstDayOfWeek(DateTime date, CalendarWeekRule rule)
    {

        int weeknumber = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, rule, DayOfWeek.Monday);

        DateTime jan1 = new DateTime(date.Year, 1, 1);

        int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        DateTime firstMonday = jan1.AddDays(daysOffset);

        var cal = CultureInfo.CurrentCulture.Calendar;
        int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

        if (firstWeek <= 1)
        {
            weeknumber -= 1;
        }

        DateTime result = firstMonday.AddDays((weeknumber-1) * 7);

        return result;
    }

    public static SecurityEntities SecurityEntities
    {
        get
        {
            SecurityEntities value;

            if (CacheManager.Instance.GetFromCache<SecurityEntities>("SecurityObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<SecurityEntities>("SecurityObject", new SecurityEntities());
        }
    }

    protected static string TrimText(object text, int amount)
    {
        if (text != null && !string.IsNullOrEmpty(text.ToString()))
            return string.Format("{0}...", text.ToString().Substring(0, (text.ToString().Length < amount) ? text.ToString().Length : amount));


        return string.Empty;
    }

    protected string CalculateTimePeriod(object startdate, object enddate, char timeperiod)
    {
        int weeks;
        switch (timeperiod.ToString().ToUpper())
        {
            case "D":
                weeks = (int)(Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays;
                break;
            case "W":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 7);
                break;
            case "M":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 31);
                break;
            default:
                return "0";
        }

        return weeks.ToString();
    }

    protected static string CalculateAge(DateTime? dob)
    {
        if (dob.HasValue)
        return Convert.ToInt32(((DateTime.Now - dob.Value).Days)/365).ToString();

        return string.Empty;
    }

    protected static string CalculateTicketExpiry(DateTime FromDate, Int32 ticketType)
    {
        DateTime expiryDate;
        switch (ticketType)
        {
            case 1:
                expiryDate = FromDate.AddDays(7);
                break;
            case 2:
                expiryDate = FromDate.AddDays(14);
                break;
            case 3:
                expiryDate = FromDate.AddDays(21);
                break;
            case 4:
                expiryDate = FromDate.AddDays(28);
                break;
            default:
                return "0";
        }

        return expiryDate.ToString("dd MMMM yyyy");
    }

    protected static string CalculateTimePeriod(DateTime startdate, DateTime? enddate, char timeperiod)
    {
        int weeks;
        DateTime _enddate = (!enddate.HasValue) ? DateTime.Now :enddate.Value;
        switch (timeperiod.ToString().ToUpper())
        {
            case "D":
                weeks = (int)(Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays;
                break;
            case "W":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 7);
                break;
            case "M":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 31);
                break;
            default:
                return "0";
        }

        return weeks.ToString();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteTabItem(DeleteTabAction actionName, object id)
    {
        try
        {
            object x = null;
            System.Data.Objects.ObjectContext context = null ;

            switch (actionName)
            {
                case DeleteTabAction.FamilyMember:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyMember", "FamilyMemberId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyVisit:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyVisit", "FamilyVisitId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyAvailability:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Availability", "AvailabilityId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyComplaint:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyComplaint", "FamilyComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.Room:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Room", "RoomId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyNote:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyNote", "FamilyNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyBank:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyBank", "FamilyBankBankId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.FamilyPayment:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyPayments", "PaymentId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentEnrollment:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Enrollment", "EnrollmentId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentExam:
                    context = BaseTuitionPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentExam", "StudentExamId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.AssignedBook:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AssignedBooks", "AssignedBookId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.StudentHosting:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Hostings", "HostingId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentComplaint:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentComplaint", "StudentComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.AgencyComplaint:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AgencyComplaint", "AgencyComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.AgencyNote:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AgencyNote", "AgencyNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentNote:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentNote", "StudentNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentTransfer:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentTransfer", "StudentTransferId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.PreTests:
                    context = BaseTuitionPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".PreTests", "PreTestId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupNote:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupNote", "GroupNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupTransfer:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupTransfer", "GroupTransferId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupComplaint:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupComplaint", "GroupComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupStudent:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Student", "StudentId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupEnrollment:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Enrollment", "EnrollmentId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.ClassTransfer:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".ClassTransfer", "ClassTransferId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupExcursion:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupExcursion", "GroupExcursionId", Convert.ToInt32(id)));
                    break;
                default:
                    break;
            }
            if (x != null && context != null)
            {
                context.DeleteObject(x);
                context.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteParentChildTabItem(DeleteRelationshipTabItem actionName, object parentid, object childid)
    {
        try
        {
            System.Data.Objects.ObjectContext context = null;

            switch (actionName)
            {
                case DeleteRelationshipTabItem.FamilyTag:
                    context = BaseAccommodationPage.CreateEntity;
                    PaceManager.Accommodation.Family family = (PaceManager.Accommodation.Family)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Family", "FamilyId", Convert.ToInt32(parentid)));
                    PaceManager.Accommodation.Tag tag = (PaceManager.Accommodation.Tag)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Tag", "TagId", Convert.ToInt32(childid)));
                    family.Tag.Attach(tag);
                    family.Tag.Remove(tag);
                    break;
                case DeleteRelationshipTabItem.StudentTag:
                    context = BaseAccommodationPage.CreateEntity;
                    //PaceManager.Enrollment.Student student = (PaceManager.Enrollment.Student)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Student", "StudentId", Convert.ToInt32(parentid)));
                    //PaceManager.Enrollment.Tag tag = (PaceManager.Enrollment.Tag)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Tag", "TagId", Convert.ToInt32(childid)));
                    //student.Tag.Remove(tag);
                    break;
                case DeleteRelationshipTabItem.GroupExcursion:
                    context = BaseExcursionPage.Entities;
                    PaceManager.Excursion.Lnk_GroupExcursion_Booking lnk_Group_Excursion = (PaceManager.Excursion.Lnk_GroupExcursion_Booking)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Lnk_GroupExcursion_Booking", new Dictionary<string, object> { { "GroupExcursionId", parentid }, { "BookingId", childid } }));
                    context.DeleteObject(lnk_Group_Excursion);
                    break;
                default:
                    break;
            }


            if (context != null)
            {
                context.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}

public enum DeleteRelationshipTabItem
{
    FamilyTag,
    StudentTag,
    GroupExcursion,
}
public enum DeleteTabAction
{
    FamilyMember,
    FamilyVisit,
    FamilyComplaint,
    FamilyAvailability,
    FamilyBank,
    Room,
    FamilyNote,
    StudentEnrollment,
    StudentHosting,
    StudentComplaint,
    AgencyComplaint,
    AgencyNote,
    StudentNote,
    StudentTransfer,
    GroupNote,
    GroupTransfer,
    GroupComplaint,
    GroupStudent,
    GroupEnrollment,
    GroupExcursion,
    ClassTransfer,
    StudentExam,
    AssignedBook,
    FamilyPayment,
    PreTests,
}