﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Enrollment;

public partial class ManageAgentPage : BaseEnrollmentPage
{
    private Int32? _agentId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["AgencyId"]))
            _agentId = Convert.ToInt32(Request["AgencyId"]);

        if (!Page.IsPostBack)
        {
            editagentnat.DataSource = LoadNationalities();
            editagentnat.DataBind();
            if (_agentId.HasValue)
            {
                DisplayAgent(LoadAgent(_agentId.Value));
            }
        }
    }

    #region Private Methods

    private Agency LoadAgent(Int32 agentId) //Load Agent if Id passed from Agent page
    {
        IQueryable<Agency> agentQuery = from f in Entities.Agency
                                        where f.AgencyId == agentId
                                        select f;
        if (agentQuery.ToList().Count() > 0)
            return agentQuery.ToList().First();

        return null;
    }

    private void DisplayAgent(Agency agent) //Display Agent details in fields
    {
        if (agent != null)
        {
            editagentname.Text = agent.Name;
            editagentcontact.Text = agent.ContactName;
            editagentemail.Text = agent.Email;
            editagentsage.Text = agent.SageRef;
            editagentnat.Items.FindByValue(agent.Xlk_Nationality.NationalityId.ToString()).Selected = true;
        }
    }

    protected void Toggle_CheckChanged(object sender, EventArgs e) //Enable/Disable SAGERef field
    {
        if (chksage.Checked == true)
            editagentsage.Enabled = true;
        else
            editagentsage.Enabled = false;
    }

    protected void Click_SaveAgent(object sender, EventArgs e) //Save new Agent / Save Agent Edits
    {
        if (!string.IsNullOrEmpty(editagentname.Text) || Convert.ToInt32(editagentnat.SelectedItem.Value) == 0 || !string.IsNullOrEmpty(editagentsage.Text))
        {
            Agency _agent = GetAgent();

            _agent.Name = editagentname.Text;
            _agent.ContactName = editagentcontact.Text;
            _agent.Email = editagentemail.Text;
            _agent.SageRef = editagentsage.Text;
            _agent.Xlk_NationalityReference.EntityKey = new System.Data.EntityKey(Entities.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(editagentnat.SelectedItem.Value));


            SaveAgent(_agent);
        }
    }

    private Agency GetAgent() //Get Agents details or set up new Agent
    {
        if (_agentId.HasValue)
            return LoadAgent(_agentId.Value);

        return new Agency();
    }

    private void SaveAgent(Agency agent) //Final Save and Redirect to Agent page
    {
        if (!_agentId.HasValue)
            Entities.AddToAgency(agent);

        if (Entities.SaveChanges() > 0)
        {
            Response.Redirect(string.Format("~/Agent/AgentPage.aspx"));
        }
    }

    #endregion

}
