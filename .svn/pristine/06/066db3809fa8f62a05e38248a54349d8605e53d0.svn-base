﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using Pace.DataAccess.Security;
using System.Data.Objects;
using Pace.DataAccess.Transport;

public partial class Transport_AddTransportBookingControl : BaseTransportControl
{
    private const short PACELocationId = 6;// the default location
    protected Transfer _transfer;
    protected string _ParentType;
    protected string _ParentValue;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue<Int32?>("BookingId").HasValue)
        {
            _ParentType = "BookingId";
            _ParentValue = GetValue<Int32>("BookingId").ToString();

            using (TransportEntities entity = new TransportEntities())
            {
                Int32 _bookingId = GetValue<Int32>("BookingId");

                _transfer = (from transfer in entity.Bookings
                            where transfer.BookingId == _bookingId
                            select new Transfer
                            {
                                Title=transfer.Title,
                                BookingId = transfer.BookingId,
                                CollectionInfo=transfer.CollectionInfo,
                                DropoffInfo = transfer.DropoffInfo,
                                PickupLocationId =transfer.PickupLocation.LocationId,
                                DropoffLocationId = transfer.DropoffLocation.LocationId,
                                RepPickupLocationId = transfer.RepCollectionLocation.LocationId,
                                NumberOfPassengers = transfer.NumberOfPassengers,
                                TripTypeId = transfer.Xlk_TripType.TripTypeId,
                                RequestedDate = transfer.RequestedDate,
                                RequestedTime = transfer.RequestedTime,
                                DropoffLocationInfo = transfer.DropoffLocationInfo,
                                PickupLocationInfo = transfer.PickupLocationInfo
                            }).First();
            }
        }

        if (GetValue<Int32?>("StudentTransferId").HasValue)
        {
                _ParentType = "StudentTransferId";
                _ParentValue = GetValue<Int32>("StudentTransferId").ToString();

            using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
            {
                Int32 id = GetValue<Int32>("StudentTransferId");

                _transfer = (from transfer in entity.StudentTransfer
                             where transfer.StudentTransferId == id
                             select new Transfer
                             {
                                 Title="Airport Transfer",
                                 BookingId = 0,
                                 PickupLocationId = ((transfer.Xlk_TransferType.TransferTypeId == 1) ? transfer.Xlk_Location.LocationId : PACELocationId),
                                 PickupLocationInfo = ((transfer.Xlk_TransferType.TransferTypeId == 1) ? transfer.Xlk_Location.Description : transfer.FamilyCollectionLocation),
                                 DropoffLocationId = ((transfer.Xlk_TransferType.TransferTypeId == 2) ? transfer.Xlk_Location.LocationId : PACELocationId),
                                 DropoffLocationInfo = ((transfer.Xlk_TransferType.TransferTypeId == 2) ? transfer.Xlk_Location.Description : transfer.FamilyCollectionLocation),
                                 NumberOfPassengers = 1,
                                 TripTypeId = 1,// one way trip type
                                 RequestedDate = transfer.Date,
                                 RequestedTime = transfer.Time,
                             }).First();
            
                      
            }
        }

        if (GetValue<Int32?>("GroupTransferId").HasValue)
        {
            _ParentType = "GroupTransferId";
            _ParentValue = GetValue<Int32>("GroupTransferId").ToString();

            using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
            {
                Int32 id = GetValue<Int32>("GroupTransferId");

                _transfer = (from transfer in entity.GroupTransfer.Include("Group")
                             where transfer.GroupTransferId == id
                             select new Transfer
                             {
                                 Title = transfer.Group.GroupName + " Airport Transfer", 
                                 BookingId = 0,
                                 PickupLocationId = ((transfer.Xlk_TransferType.TransferTypeId == 1) ? transfer.Xlk_Location.LocationId : PACELocationId),
                                 PickupLocationInfo = ((transfer.Xlk_TransferType.TransferTypeId == 1) ? transfer.Xlk_Location.Description : transfer.FamilyCollectionLocation),
                                 DropoffLocationId = ((transfer.Xlk_TransferType.TransferTypeId == 2) ? transfer.Xlk_Location.LocationId : PACELocationId),
                                 DropoffLocationInfo = ((transfer.Xlk_TransferType.TransferTypeId == 2) ? transfer.Xlk_Location.Description : transfer.FamilyCollectionLocation),
                                 NumberOfPassengers = transfer.NumberOfPassengers,
                                 TripTypeId = 1,// one way trip type
                                 RequestedDate = transfer.Date,
                                 RequestedTime = transfer.Time
                             }).First();
            }
        }
        if (GetValue<Int32?>("ExcursionBookingId").HasValue)
        {
            _ParentType = "ExcursionBookingId";
            _ParentValue = GetValue<Int32>("ExcursionBookingId").ToString();

            using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
            {
                Int32 id = GetValue<Int32>("ExcursionBookingId");

                _transfer = (from excursionbooking in entity.Bookings.Include("Excursion")
                             where excursionbooking.BookingId == id
                             select new Transfer
                             {
                                 Title =  excursionbooking.Title + " Transfer",
                                 BookingId = 0,
                                 PickupLocationId = PACELocationId,
                                 DropoffLocationId = excursionbooking.Excursion.Xlk_Location.LocationId,
                                 NumberOfPassengers = excursionbooking.QtyLeaders + excursionbooking.QtyStudents + excursionbooking.QtyGuides,
                                 TripTypeId = 1,// one way trip type
                                 RequestedDate = excursionbooking.ExcursionDate,
                                 RequestedTime = excursionbooking.MeetTime,
                                 DropoffLocationInfo = excursionbooking.MeetLocation,
                                 PickupLocationInfo = excursionbooking.MeetLocation
                             }).First();
            }
        }
    }


    protected string GetTripTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_transfer != null) ? _transfer.TripTypeId : (Int32?)null;


        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_TripType> triptypeQuery = (from s in entity.Xlk_TripType select s);
            foreach (Xlk_TripType item in triptypeQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TripTypeId, item.Description, (_current.HasValue && item.TripTypeId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetDropOffLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_transfer != null) ? _transfer.DropoffLocationId : (Int32?)null;


        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_Location> Xlk_LocationQuery = (from s in entity.Xlk_Location select s);
            foreach (Xlk_Location item in Xlk_LocationQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current.HasValue && item.LocationId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetPickUpLocationList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_transfer != null) ? _transfer.PickupLocationId : (Int32?)null;


        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_Location> Xlk_LocationQuery = (from s in entity.Xlk_Location select s);
            foreach (Xlk_Location item in Xlk_LocationQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current.HasValue && item.LocationId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetRepCollectionLocationList()
    {
        StringBuilder sb = new StringBuilder("<option value=-1>No Requested</option>");

        Int32? _current = (_transfer != null) ? _transfer.RepPickupLocationId : (Int32?)null;


        using (TransportEntities entity = new TransportEntities())
        {
            IQueryable<Xlk_Location> Xlk_LocationQuery = (from s in entity.Xlk_Location select s);
            foreach (Xlk_Location item in Xlk_LocationQuery.ToList())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LocationId, item.Description, (_current.HasValue && item.LocationId == _current) ? "selected" : string.Empty);
            }

        }

        return sb.ToString();
    }

    protected string GetTitle
    {
        get
        {
            if (_transfer != null)
                return _transfer.Title;

            return string.Empty;
        }
    }


    protected string GetCollectionInfo
    {
        get
        {
            if (_transfer != null)
                return _transfer.CollectionInfo;

            return string.Empty;
        }
    }

    protected string GetPickupLocationInfo
    {
        get
        {
            if (_transfer != null)
                return _transfer.PickupLocationInfo;

            return string.Empty;
        }
    }


    protected string GetDropoffLocationInfo
    {
        get
        {
            if (_transfer != null)
                return _transfer.DropoffLocationInfo;

            return string.Empty;
        }
    }


    protected string GetDropoffInfo
    {
        get
        {
            if (_transfer != null)
                return _transfer.DropoffInfo;

            return string.Empty;
        }
    }

    protected string GetNumberOfPassengers
    {
        get
        {
            if (_transfer != null)
                return _transfer.NumberOfPassengers.ToString();

            return "0";
        }
    }

    protected string GetRequestedDate
    {
        get
        {
            if (_transfer != null && _transfer.RequestedDate.HasValue)
                return _transfer.RequestedDate.Value.ToString("dd/MM/yy");

            return DateTime.Now.ToString("dd/MM/yy");
        }
    }

    protected string GetRequestedTime
    {
        get
        {
            if (_transfer != null)
                return _transfer.RequestedTime.ToString();

            return "";
        }
    }

    protected string GetBookingId
    {
        get
        {
            if (_transfer != null)
                return _transfer.BookingId.ToString();

            if (Parameters.ContainsKey("BookingId"))
                return Parameters["BookingId"].ToString();

            return "0";
        }
    }

    protected AjaxResponse<string> GetVehicleCheckboxList()
    {
        return BaseTransportPage.FindVehicles(_transfer.NumberOfPassengers.Value, _transfer.DropoffLocationId.Value, _transfer.PickupLocationId.Value, _transfer.TripTypeId.Value);
    }
}

public class Transfer
{
    public string Title, CollectionInfo, DropoffInfo;
    public Int16? PickupLocationId, DropoffLocationId, RepPickupLocationId;
    public int? BookingId, NumberOfPassengers;
    public byte? TripTypeId;
    public DateTime? RequestedDate;
    public TimeSpan? RequestedTime;
    public string PickupLocationInfo { get; set; }
    public string DropoffLocationInfo { get; set; }
}


