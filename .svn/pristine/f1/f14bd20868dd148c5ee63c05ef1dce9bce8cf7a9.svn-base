﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Enrollment;
using System.Data;

public partial class Enrollments_ViewStudentPage : BaseEnrollmentPage
{
    protected Int32 _studentId;


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["StudentId"]))
            _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!Page.IsPostBack)
        {
            newnationality.DataSource = LoadNationalities();
            newnationality.DataBind();
            newagent.DataSource = LoadAgents();
            newagent.DataBind();
            newstatus.DataSource = LoadStatus();
            newstatus.DataBind();
            errorcontainer.Visible = false;
            newgender.DataSource = LoadGender();
            newgender.DataBind();
            newexam.DataSource = this.LoadExams();
            newexam.DataBind();
            DisplayStudent(LoadStudent(_studentId));
            LoadClassHistory(_studentId);

        }
    }


    protected void Exams_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 _exam = Convert.ToInt32(newexam.SelectedIndex);

        if (_exam > 0)
        {
            examinfo.Visible = true;
            var examQuery = from ex in Entities.Exam
                            where ex.ExamId == _exam
                            select ex.ExamDescription;
            examinfo.Text = examQuery.First().ToString();
        }
        else examinfo.Visible = false;
    }


    protected void Click_Save(object sender, EventArgs e) //Save edits or Save new Student
    {
        string _message = ""; errorcontainer.Visible = false;

        if (!string.IsNullOrEmpty(newfname.Text) || !string.IsNullOrEmpty(newsname.Text) || !string.IsNullOrEmpty(newarrdate.Text) ||
            !string.IsNullOrEmpty(newdepdate.Text))
        {
            Student _student = GetStudent();

            _student.FirstName = newfname.Text;
            _student.SurName = newsname.Text;
 
            _student.PrepaidBook = chkbook.Checked;
            _student.PrepaidExam = chkexam.Checked;
            _student.HomeAddress = homeaddress.Text;
            _student.StudentEmail = stemail.Text;
            _student.Xlk_GenderReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Gender", "GenderId", Convert.ToByte(newgender.SelectedItem.Value));
            if (!string.IsNullOrEmpty(newdob.Text))
                _student.DateOfBirth = Convert.ToDateTime(newdob.Text);
            _student.Xlk_NationalityReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
            _student.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
            _student.DepartureDate = Convert.ToDateTime(newdepdate.Text);
            if (!string.IsNullOrEmpty(newplacement.Text))
                _student.PlacementTestResult = Convert.ToDecimal(newplacement.Text);
            _student.AgencyReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
            _student.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));
            if (newexam.SelectedIndex > 0)
                _student.ExamReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Exam", "ExamId", Convert.ToInt16(newexam.SelectedItem.Value));
            else
                _student.ExamReference.EntityKey = null;

            SaveStudent(_student);

        }
        else
        {
            _message = "Not all fields are complete. Please check.";
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }


    }

    private Student LoadStudent(Int32 studentId) //Load Student if passed from Enrollment page
    {

        IQueryable<Student> studentQuery = from s in Entities.Student.Include("Agency").Include("Xlk_Nationality")
                                           where s.StudentId == studentId
                                           select s;

        if (studentQuery.ToList().Count() > 0)
            return studentQuery.ToList().First();

        return null;
    }

    private void DisplayStudent(Student student) //Display loaded Student details
    {
        if (student != null)
        {
            StudentId.Text = student.StudentId.ToString();
            Name.Text = String.Format("{0} {1}", student.FirstName, student.SurName);
            CourseStart.Text = student.ArrivalDate.ToString("d");
            CourseFinish.Text = student.DepartureDate.Value.ToString("d");
            Weeks.Text = CalculateTimePeriod(student.ArrivalDate, student.DepartureDate, 'W');
            //hadd.Text = "Home Address: " + student.HomeAddress;
            //email.Text = "Email : " + student.StudentEmail;

            //if (student.DepartureDate.HasValue)
            //{
            //    if (student.DepartureDate.Value.Date < DateTime.Now.Date)
            //    {
            //        studentstatus.ImageUrl = "~/Content/img/flag.png";
            //        studentstatus.ToolTip = "Student has left";
            //    }
            //    else if (student.DepartureDate.Value.Date < DateTime.Now.AddDays(7).Date)
            //    {
            //        studentstatus.ImageUrl = "~/Content/img/flag-yellow.png";
            //        studentstatus.ToolTip = "Student will be leaving soon";
            //    }
            //    else if (student.ArrivalDate > DateTime.Now)
            //    {
            //        studentstatus.ImageUrl = "~/Content/img/flag-blue.png";
            //        studentstatus.ToolTip = "Student has not arrived yet";
            //    }
            //}

            if (student.Xlk_Gender.Description == "Female")
            {
                gender.ImageUrl = "~/Content/img/female.png";
                gender.ToolTip = "Female";
            }
            else
            {
                gender.ImageUrl = "~/Content/img/male.png";
                gender.ToolTip = "Male";
            }
            if (student.DateOfBirth.HasValue)
                DOB.Text = student.DateOfBirth.Value.ToString("dd/MM/yyyy");
            Nationality.Text = student.Xlk_Nationality.Description;
            //Agent.Text = "Agent: " + student.Agency.Name;
            if (student.PlacementTestResult.HasValue)
                Placement.Text = (student.PlacementTestResult.Value/100).ToString("0.0%");
            if (student.PrepaidBook == true)
                ppbook.Text = "YES";
            else
                ppbook.Text = "NO";
            if (student.Exam != null)
                exam.Text = "YES";
            else
                exam.Text = "";
            //if (student.PrepaidExam == true)
            //    ppexam.Text = " Prepaid Exam: YES";
            //else if (student.Exam == null && student.PrepaidExam == false)
            //    ppexam.Text = "";

            newfname.Text = student.FirstName;
            newsname.Text = student.SurName;
            chkbook.Checked = student.PrepaidBook;
            chkexam.Checked = student.PrepaidExam;
            newdob.Text = (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("dd/MM/yyyy") : string.Empty;
            newarrdate.Text = student.ArrivalDate.ToString("dd/MM/yyyy");
            newdepdate.Text = (student.DepartureDate.HasValue) ? student.DepartureDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            newplacement.Text = student.PlacementTestResult.ToString();
            newgender.Items.FindByValue(student.Xlk_Gender.GenderId.ToString()).Selected = true;
            newagent.Items.FindByValue(student.Agency.AgencyId.ToString()).Selected = true;
            newstatus.Items.FindByValue(student.Xlk_Status.StatusId.ToString()).Selected = true;
            newnationality.Items.FindByValue(student.Xlk_Nationality.NationalityId.ToString()).Selected = true;
            if (student.Exam != null)
            newexam.Items.FindByValue(student.Exam.ExamId.ToString()).Selected = true;
            homeaddress.Text = student.HomeAddress;
            stemail.Text = student.StudentEmail;

            string TextToTrim = "Home Address: " + student.HomeAddress + "<br/><br/>Email: " + student.StudentEmail + "<br/><br/>Agency: " + student.Agency.Name + "<br/><br/>Prepaid Exam: " + student.PrepaidExam;
            //trimmedtext.Text = TrimText(TextToTrim, 1350);
            moreinfo.Title = "Home Address: " + student.HomeAddress + "<br/><br/>Email: " + student.StudentEmail + "<br/><br/>Agency: " + student.Agency.Name + "<br/><br/>Prepaid Exam: " + student.PrepaidExam;
        }
    }

    private Student GetStudent() //Load Student or Create new Student
    {
        return LoadStudent(_studentId);
    }

    private void SaveStudent(Student student) //Final Save stage & Redirect
    {
        if (Entities.SaveChanges() > 0)
        {
            DisplayStudent(LoadStudent(_studentId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }

    private void LoadClassHistory(Int32 studentId) //Display Class History for Student
    {
        var studentClassHistoryQuery = from e in BaseTuitionPage.Entities.Enrollment.Include("Xlk_CourseType").Include("Teacher").Include("ClassRoom")
                                           from s in e.ClassSchedule
                                           where e.Student.StudentId == studentId
                                           select new { s.StartDate, TuitionLevel=s.Class.TuitionLevel.Description,CEFR=s.Class.TuitionLevel.CEFR,Teacher=s.Class.Teacher.Name,CourseType=s.Class.Xlk_CourseType.Description, ClassId=s.Class.ClassId };


        classHistory.DataSource = studentClassHistoryQuery.ToList();
        classHistory.DataBind();

    }
}
