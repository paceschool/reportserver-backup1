﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Enrollment;
using System.Data;
using System.Text;
using System.Globalization;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core;


public partial class EnrollmentPage : BaseEnrollmentPage
{
    private int[] statusFilter = new[] { 5 };
    protected string lookupText = string.Empty;
    protected Int32 agencyid = 0;
    protected Int32 nationalityid = 0;

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listnationality.DataSource = LoadNationalities();
            listnationality.DataBind();

            listagent.DataSource = LoadAgents();
            listagent.DataBind();

            listprogrammes.DataSource = LoadProgrammeTypes();
            listprogrammes.DataBind();

        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["Action"]))
            ShowSelected(Request["Action"]);
    }



    #endregion

    #region private Methods

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadStudents();
                break;
            case "current":
                Click_LoadCurrentStudents();
                break;
            case "arriving":
                Click_Arriving();
                break;
            case "departing":
                Click_Departing();
                break;
            case "cancelled":
                Click_Cancelled();
                break;
            case "noenrollments":
                Click_LoadNoEnrollmentsStudents();
                break;
            case "nohostings":
                Click_LoadNoHostingStudents();
                break;
            case "allnotgroups":
                Click_LoadAllNotGroups();
                break;
            case "birthdays":
                Click_Birthdays();
                break;
            case "lastentered":
                Click_Last(20);
                break;
            case "junior":
                Click_Juniors();
                break;
            default:
                Click_LoadAllStudents();
                break;
        }
    }

    protected void Click_LoadCurrentStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.GroupId == null && f.ArrivalDate <= DateTime.Now && f.DepartureDate >= DateTime.Now && f.CampusId == GetCampusId
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }

    }
    protected void Click_Juniors()
    {
        DateTime _checkDate = DateTime.Now.AddMonths(-3);

        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.GroupId == null && f.ArrivalDate >= _checkDate && f.Enrollment.Any(r => r.Xlk_ProgrammeType.ProgrammeTypeId == 7) && f.CampusId == GetCampusId
                                     orderby f.ArrivalDate ascending, f.DepartureDate ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }

    }

    protected void Click_LoadAllStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.CampusId == GetCampusId
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected void Click_Last(int qty)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.GroupId == null && f.CampusId == GetCampusId
                                     orderby f.StudentId descending, f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.Take(qty).ToList());
        }
    }

    protected void Click_Birthdays()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from b in entity.Student.Include("Xlk_Status")
                                     where !statusFilter.Contains(b.Xlk_Status.StatusId) && b.DateOfBirth.Value.Month == DateTime.Now.Month && (b.ArrivalDate < DateTime.Now && b.DepartureDate >= DateTime.Now) && b.CampusId == GetCampusId
                                     select b;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected void Click_Arriving()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime endarrivingDate = startarrivingDate.AddDays(31);
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.GroupId == null && (f.Enrollment.Any(g => g.StartDate >= startarrivingDate && g.StartDate <= endarrivingDate)) && f.CampusId == GetCampusId
                                     orderby f.ArrivalDate, f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());

        }
    }

    protected void Click_Departing()
    {
        DateTime startleavingDate = (FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek));
        DateTime endleavingDate = startleavingDate.AddDays(7);
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from g in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(g.Xlk_Status.StatusId) && g.GroupId == null && (g.Enrollment.Any(h => h.EndDate >= startleavingDate && h.EndDate <= endleavingDate)) && g.CampusId == GetCampusId
                                     orderby g.FirstName ascending, g.SurName ascending
                                     select g;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected void Click_Cancelled()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where f.Xlk_Status.StatusId == 5 && f.CampusId == GetCampusId
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected List<Pace.DataAccess.Enrollment.Student> Get_Cancelled()
    {
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Status")
                                     where f.Xlk_Status.StatusId == 5 && f.CampusId == GetCampusId
                                     select f;
            return StudentSearchQuery.ToList();
        }
    }

    protected void Click_LoadAllNotGroups()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.GroupId == null && f.CampusId == GetCampusId
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }

    protected void Click_LoadNoHostingStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.GroupId == null && f.Hostings.Count == 0 && f.CampusId == GetCampusId
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }
    protected void Click_LoadNoEnrollmentsStudents()
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where !statusFilter.Contains(f.Xlk_Status.StatusId) && f.GroupId == null && f.Enrollment.Count == 0 && f.CampusId == GetCampusId
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }
    }


    protected void lookupStudent(object sender, EventArgs e)
    {
        Click_LoadStudents();
        //listagent.SelectedIndex = 0; 
        //listnationality.SelectedIndex = 0; 
        lookupString.Attributes.Add("onfocus", "this.select();");
    }

    protected void Click_LoadStudents()
    {
        int agencyId = Convert.ToInt32(listagent.SelectedItem.Value);
        int nationalityId = Convert.ToInt32(listnationality.SelectedItem.Value);
        int programmeTypeId = Convert.ToInt32(listprogrammes.SelectedItem.Value);

        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            string search = lookupString.Text.Trim();
            string search1 = RemoveDiacritics(lookupString.Text.Trim());

            List<Int32?> ids = entity.SearchStudents((agencyId > 0) ? agencyId : (Int32?)null, (nationalityId > 0) ? nationalityId : (Int32?)null,  (programmeTypeId > 0) ? programmeTypeId : (Int32?)null, search1).Take(300).ToList();

            var studentSearchQuery = from s in entity.Student.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("StudentInvoice").Include("Exam").Include("Enrollment").Include("Enrollment.Xlk_ProgrammeType")
                                     where ids.Contains(s.StudentId) && s.CampusId == GetCampusId
                                     select s;

            LoadResults(studentSearchQuery.OrderByDescending(s => s.ArrivalDate).ToList());
        }
    } 

    private bool SearchUnicodeString(string first, string second)
    {
        return true;
    }

    private void LoadResults(List<Student> students)
    {
        //Set the datasource of the repeater
        results.DataSource = students;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", (students != null) ? students.Count().ToString() : "0");
    }

    //Create list of Sage references
    protected string CreateSageRefsList(object sagerefs)
    {
        System.Text.StringBuilder sagerefstring = new System.Text.StringBuilder();
        var s = from st in (EntityCollection<Agency>)sagerefs
                select st.SageRef;
        return String.Join("/", s.ToArray());
    }

    protected string CreateStudentInvoiceList(object studentinvoices)
    {

        System.Text.StringBuilder invoiceString = new System.Text.StringBuilder();

        foreach (StudentInvoice gi in (EntityCollection<StudentInvoice>)studentinvoices)
        {
            invoiceString.AppendFormat("<a href=\"{0}?InvoiceNumber={1}\">{1}</a>&nbsp;", VirtualPathUtility.ToAbsolute("~/Finance/ViewSageInvoicePage.aspx"), gi.SageInvoiceRef);
        }

        return invoiceString.ToString();

    }
    protected bool IsCancelled(object statusid,string value)
    {
        if (statusid.ToString() != value)
            return false;
        else
            return true;
    }
    protected string ShowProgrammeType(object enrollments)
    {

        return (from st in (EntityCollection<Enrollment>)enrollments
                select st.Xlk_ProgrammeType.Description).FirstOrDefault();

    }
    protected void results_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DeleteStudent")
        {
            using (EnrollmentsEntities entity = new EnrollmentsEntities())
            {
                entity.DeleteStudent(Convert.ToInt32(e.CommandArgument));
                Response.Redirect(Request.RawUrl);

            }
         
        }
    }


    #endregion

    #region JavaScript Enabled Methods

    [WebMethod]
    public static string AddSageInvoiceRef(int studentid, int sageinvoiceref)
    {
        try
        {
            if (!SageInvoiceRefExists(studentid, sageinvoiceref))
            {
                using (EnrollmentsEntities entity = new EnrollmentsEntities())
                {
                    StudentInvoice studentInvoice = new StudentInvoice();
                    studentInvoice.StudentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Student", "StudentId", studentid);
                    studentInvoice.SageInvoiceRef = sageinvoiceref;
                    studentInvoice.DateRecorded = DateTime.Now;

                    entity.AddToStudentInvoice(studentInvoice);

                    if (entity.SaveChanges() > 0)
                        return string.Empty;
                }
                return string.Empty;
            }
            else
                return "Could not save the reference, this reference already exists.";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string LoadSageInvoiceRefs(int studentid)
    {
        StringBuilder sb = new StringBuilder();
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<StudentInvoice> sageRefQuery = from d in entity.StudentInvoice
                                                                  where d.Student.StudentId == studentid
                                                                  select d;
            foreach (StudentInvoice sageref in sageRefQuery.ToList())
            {
                sb.AppendFormat("<option>{0}</option>", sageref.SageInvoiceRef);
            }
        }
        return sb.ToString();
    }

    [WebMethod]
    public static string DeleteSageInvoiceRef(int studentid, int sageinvoiceref)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            IQueryable<StudentInvoice> sageRefObject = from d in entity.StudentInvoice
                                                                   where d.SageInvoiceRef == sageinvoiceref && d.Student.StudentId == studentid
                                                                   select d;
            if (sageRefObject.Count() > 0)
                entity.DeleteObject(sageRefObject.First());
            else
                return "Could not delete reference: reference not found.";
            if (entity.SaveChanges() > 0)
                return string.Empty;
        }
        return "Could not add the new reference. There was a problem.";
    }

    [WebMethod]
    private static bool SageInvoiceRefExists(int studentid, int sageinvoiceref)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var i = from d in entity.StudentInvoice
                    where d.SageInvoiceRef == sageinvoiceref && d.Student.StudentId == studentid
                    select d.StudentInvoiceId;
            return (i.Count() > 0);
        }
    }


    #endregion



}
