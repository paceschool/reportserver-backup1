﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Web.Services;

public partial class Student_AddEnrollment : BaseEnrollmentControl
{
    protected Int32 _familyId;
    protected Enrollment _enrollment;
    protected Student _student;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            if (GetValue<Int32?>("EnrollmentId").HasValue)
            {
                Int32 enrollmentId = GetValue<Int32>("EnrollmentId");
                _enrollment = (from enroll in entity.Enrollment.Include("Xlk_ProgrammeType").Include("Xlk_CourseType").Include("Xlk_LessonBlock").Include("Student")
                               where enroll.EnrollmentId == enrollmentId
                               select enroll).FirstOrDefault();

            }
            else if (GetValue<Int32?>("StudentId").HasValue)
            {
                Int32 studentId = GetValue<Int32>("StudentId");
                _student = (from host in entity.Student
                            where host.StudentId == studentId
                            select host).FirstOrDefault();

            }
        }
    }


    protected string GetProgrammeTypeList()
    {
        StringBuilder sb = new StringBuilder();
        
        byte? _current = (_enrollment != null && _enrollment.Xlk_ProgrammeType != null) ? _enrollment.Xlk_ProgrammeType.ProgrammeTypeId: (byte?)null;

        foreach (Xlk_ProgrammeType item in LoadProgrammeTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ProgrammeTypeId, item.Description, (_current.HasValue && item.ProgrammeTypeId == _current) ? "selected" :string.Empty);
        }

        return sb.ToString();
    }

    protected string GetCourseTypeList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_enrollment != null && _enrollment.Xlk_CourseType != null) ? _enrollment.Xlk_CourseType.CourseTypeId : (short?)null;

        if (_enrollment != null && _enrollment.Xlk_ProgrammeType != null)
        {
            foreach (Xlk_CourseType item in LoadCourseTypes().Where(x => x.Xlk_ProgrammeType.Any(p => p.ProgrammeTypeId == _enrollment.Xlk_ProgrammeType.ProgrammeTypeId)))
            {
                sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.CourseTypeId, item.Description, (_current.HasValue && item.CourseTypeId == _current) ? "selected" : string.Empty);

            }
        }
        else
        {
            foreach (Xlk_CourseType item in LoadCourseTypes())
            {
                sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.CourseTypeId, item.Description, (_current.HasValue && item.CourseTypeId == _current) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();
    }

    protected string GetLessonBlockTypeList()
    {
        StringBuilder sb = new StringBuilder();
        byte? _current = (_enrollment != null && _enrollment.Xlk_LessonBlock != null) ? _enrollment.Xlk_LessonBlock.LessonBlockId : (byte?)2;

        if (_enrollment != null && _enrollment.Xlk_CourseType != null)
        {
            foreach (Xlk_LessonBlock item in LoadLessonBlocks().Where(x => x.Xlk_CourseType.Any(p => p.CourseTypeId == _enrollment.Xlk_CourseType.CourseTypeId)))
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LessonBlockId, item.Description, (_current.HasValue && item.LessonBlockId == _current) ? "selected" : string.Empty);
            }
        }
        else
        {
            foreach (Xlk_LessonBlock item in LoadLessonBlocks())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.LessonBlockId, item.Description, (_current.HasValue && item.LessonBlockId == _current) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();

    }
    protected string GetStudentId
    {
        get
        {
            if (_enrollment != null)
                return _enrollment.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }


    protected string GetEnrollmentId
    {
        get
        {
            if (_enrollment != null)
                return _enrollment.EnrollmentId.ToString();
            return "0";
        }
    }

    protected string GetStartDate
    {
        get
        {
            if (_enrollment != null)
                return _enrollment.StartDate.ToString("dd/MM/yy");

            if (_student != null)
                return Pace.Common.DateHelper.Instance.GetDayOfWeekDate(_student.ArrivalDate, DayOfWeek.Monday, true).ToString("dd/MM/yy");

            return null;
        }
    }
    protected string GetEndDate
    {
        get
        {
            if (_enrollment != null && _enrollment.EndDate.HasValue)
                return _enrollment.EndDate.Value.ToString("dd/MM/yy");


            if (_student != null && _student.DepartureDate.HasValue)
                return Pace.Common.DateHelper.Instance.GetDayOfWeekDate(_student.DepartureDate.Value, DayOfWeek.Friday, false).ToString("dd/MM/yy");

            return null;

        }
    }
}
