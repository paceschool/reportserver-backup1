﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaceManager.Enrollment;
using System.Data.Objects;
using System.Data;

public partial class AddStudentPage : BaseEnrollmentPage
{
    private Int32? _studentId;
    protected string strMessage;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request["StudentId"]))
            _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!Page.IsPostBack)
        {
            if (Request.UrlReferrer != null)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();

            newnationality.DataSource = LoadNationalities();
            newnationality.DataBind();
            newagent.DataSource = LoadAgents();
            newagent.DataBind();
            newstatus.DataSource = LoadStatus();
            newstatus.DataBind();
            errorcontainer.Visible = false;
            gender.DataSource = LoadGender();
            gender.DataBind();
            newexam.DataSource = this.LoadExams();
            newexam.DataBind();
            stype.DataSource = LoadStudentTypes();
            stype.DataBind();
            if (_studentId.HasValue)
            {
               DisplayStudent(LoadStudent(_studentId.Value));
            }
        }
    }

    #region private methods

    protected void Click_ClearFields(object sender, EventArgs e) //Clear all Fields
    {
        newfname.Text = ""; newsname.Text = "";
        newnationality.SelectedIndex = 0; newagent.SelectedIndex = 0; newstatus.SelectedIndex = 0;
        newarrdate.Text = ""; newdepdate.Text = ""; newplacement.Text = ""; newexam.Text = "";
        errorcontainer.Visible = false;
    }

    protected void Click_Save(object sender, EventArgs e) //Save edits or Save new Student
    {
        string _message = ""; errorcontainer.Visible = false;

        if (!string.IsNullOrEmpty(newfname.Text) || !string.IsNullOrEmpty(newsname.Text) || !string.IsNullOrEmpty(newarrdate.Text) ||
            !string.IsNullOrEmpty(newdepdate.Text))
        {
            Student _student = GetStudent();

            _student.FirstName = newfname.Text;
            _student.SurName = newsname.Text;
            if (newexam.SelectedValue != "0")
                _student.ExamReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Exam", "ExamId", Convert.ToInt16(newexam.SelectedValue));
            _student.PrepaidBook = chkbook.Checked;
            _student.PrepaidExam = chkexam.Checked;
            _student.Xlk_GenderReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Gender", "GenderId", Convert.ToByte(gender.SelectedItem.Value));
            if (!string.IsNullOrEmpty(dob.Text))
            _student.DateOfBirth = Convert.ToDateTime(dob.Text);
            _student.Xlk_NationalityReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Nationality", "NationalityId", Convert.ToInt16(newnationality.SelectedItem.Value));
            _student.ArrivalDate = Convert.ToDateTime(newarrdate.Text);
            _student.DepartureDate = Convert.ToDateTime(newdepdate.Text);
            if (!string.IsNullOrEmpty(newplacement.Text))
                _student.PlacementTestResult = Convert.ToDecimal(newplacement.Text);
            _student.AgencyReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Agency", "AgencyId", Convert.ToInt32(newagent.SelectedItem.Value));
            _student.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(newstatus.SelectedItem.Value));
            _student.Xlk_StudentTypeReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_StudentType", "StudentTypeId", Convert.ToByte(stype.SelectedItem.Value));

            SaveStudent(_student);

        }
        else
        {
            _message = "Not all fields are complete. Please check.";
            errorcontainer.Visible = true;
            errormessage.Text = _message;
        }


    }

    private Student LoadStudent(Int32 studentId) //Load Student if passed from Enrollment page
    {

        IQueryable<Student> studentQuery = from s in Entities.Student.Include("Agency").Include("Xlk_Nationality")
                                           where s.StudentId == studentId
                                           select s;

        if (studentQuery.ToList().Count() > 0)
            return studentQuery.ToList().First();

        return null;
    }

    private void DisplayStudent(Student student) //Display loaded Student details
    {
        if (student != null)
        {
            newfname.Text = student.FirstName;
            newsname.Text = student.SurName;
            chkbook.Checked = student.PrepaidBook;
            chkexam.Checked = student.PrepaidExam;
            newexam.Items.FindByValue(student.Exam.ExamId.ToString()).Selected = true;
            dob.Text = (student.DateOfBirth.HasValue) ? student.DateOfBirth.Value.ToString("dd/MM/yyyy") : string.Empty;
            newarrdate.Text = student.ArrivalDate.ToString("dd/MM/yyyy");
            newdepdate.Text = (student.DepartureDate.HasValue) ? student.DepartureDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            newplacement.Text = student.PlacementTestResult.ToString();
            gender.Items.FindByValue(student.Xlk_Gender.GenderId.ToString()).Selected = true;
            newagent.Items.FindByValue(student.Agency.AgencyId.ToString()).Selected = true;
            newstatus.Items.FindByValue(student.Xlk_Status.StatusId.ToString()).Selected = true;
            newnationality.Items.FindByValue(student.Xlk_Nationality.NationalityId.ToString()).Selected = true;
        }
    }

    private Student GetStudent() //Load Student or Create new Student
    {
        if (_studentId.HasValue)
            return LoadStudent(_studentId.Value);

        return new Student();
    }

    private void SaveStudent(Student student) //Final Save stage & Redirect
    {
        if (!_studentId.HasValue)
            Entities.AddToStudent(student);

        if (Entities.SaveChanges() > 0)
            Response.Redirect(string.Format("ViewStudentPage.aspx?StudentId=" + student.StudentId));


    }

    protected void Exams_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 _exam = Convert.ToInt32(newexam.SelectedIndex);

        if (_exam > 0)
        {
            examinfo.Visible = true;
            var examQuery = from ex in Entities.Exam
                            where ex.ExamId == _exam
                            select ex.ExamDescription;
            examinfo.Text = examQuery.First().ToString();
        }
        else examinfo.Visible = false;
    }

    protected void CheckArrival(object sender, EventArgs e)
    {
         
        if (Convert.ToDateTime(newarrdate.Text) < DateTime.Now)
        {
             strMessage = "Arrival date is before today.  Is this correct?";
        }
            Response.Write("<script language='javascript'>alert('" + strMessage + ".');</script>");
    }

  #endregion  
}
