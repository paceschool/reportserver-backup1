﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Documents;
using System.Data.Objects;
using System.Web.SessionState;
using System.Web.Services;
using System.Data;
using System.Text;


public partial class DocumentsPage : BaseDocumentPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            category.DataSource = this.LoadCategories();
            category.DataBind();
        }
    }

    protected void category_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }


    protected void type_SelectedIndexChanged(object sender, EventArgs e)
    {
        TypeChanged();
    }


    protected void search_Click(object sender, EventArgs e)
    {
        string searchString = searchstring.Text;

        var documentSearchQuery = ((from d in Entities.Documents
                                    join t in Entities.Tag on d.DocId equals t.DocId
                                    where  d.Xlk_Status.StatusId == 1 && (d.Title.Contains(searchString) | d.Description.Contains(searchString)) | d.Tag.Any(f => f.Tag1.Contains(searchString))
                                    select d).Distinct() as ObjectQuery<Documents>).Include("Xlk_FileType").Include("Xlk_Type").Include("Server").Include("Tag");

        LoadResults(documentSearchQuery.ToList());
    }

    protected void type_DataBound(object sender, EventArgs e)
    {
        TypeChanged();
    }

    protected void category_DataBound(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }

    #endregion

    #region Private Methods

    private void LoadTypesToDropDown()
    {
        //Set datasource of the Type DropDown
        ListItem _category = category.SelectedItem;
        type.DataSource = this.LoadTypes(Convert.ToInt32(_category.Value));
        type.DataBind();
    }

    private void TypeChanged()
    {
        if (type.SelectedItem != null)
        {
            Int32 _typeId = Convert.ToInt32(type.SelectedItem.Value);

            var documentSearchQuery = from d in Entities.Documents.Include("Xlk_FileType").Include("Xlk_Type").Include("Server").Include("Tag")
                                      where d.Xlk_Type.TypeId == _typeId && d.Xlk_Status.StatusId == 1
                                      select d;

            LoadResults(documentSearchQuery.ToList());
        }
    }

    private void LoadResults(List<Documents> documents)
    {
        //Set datasource of the repeater
        results.DataSource = documents;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", documents.Count().ToString());
    }

    private void LineClicked(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "error", "alert('oops');", true);
    }


    protected string CreateUri(object docId)
    {
        string URI = string.Format("~/ViewerPage.aspx?documentid={0}",docId.ToString());
        return URI;
    }

    protected string CreateDirectUri(object server,object path , object filename)
    {
        return string.Format("file:///{0}{1}{2}", server.ToString().Replace("\\", "/"), path.ToString().Replace("\\", "/"), filename.ToString());
    }

    protected string CreateTagList(object tags)
    {
        System.Text.StringBuilder tagsString = new System.Text.StringBuilder();

        var s = from st in (System.Data.Objects.DataClasses.EntityCollection<Tag>)tags
                select st.Tag1;

        return String.Join("/", s.ToArray());
    }

    private bool GetDocument(Int32 docId)
    {
        IQueryable<Documents> documentSearchQuery = from d in Entities.Documents.Include("Xlk_FileType").Include("Xlk_Type").Include("Server")
                                                    where d.DocId == docId
                                                    select d;

        if (documentSearchQuery.Count() == 0)
            return false;

        Documents doc = documentSearchQuery.First();

        if (doc == null)
            return false;

        if (doc.Xlk_FileType == null)
            return false;

        if (doc.Server == null)
            return false;

        //filePath = string.Format("{0}\\{1}\\{2}", doc.Server.UncPath, doc.Path, doc.FileName);

        //contentType = doc.Xlk_FileType.MimeType;

        return true;
    }


    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    public static string AddDocumentTag(int id, string tag)
    {
        if (!TagExists(id, tag))
        {
            Tag newtag = new Tag();
            newtag.DocumentsReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Documents", "DocId", id);
            newtag.DocId = id;
            newtag.TagId = NextTagId(id, 1);
            newtag.Tag1 = tag;

            Entities.AddToTag(newtag);

            if (Entities.SaveChanges() > 0)
                return string.Empty;

            return "Could not add the new tag; there was a problem";

        }
        else
            return "Could not add the new tag; tag already exists";
    }

    [WebMethod]
    public static string LoadTags(int id)
    {
        StringBuilder sb = new StringBuilder();

        IQueryable<Tag> tagQuery = from d in Entities.Tag
                                   where d.DocId == id
                                   select d;

        foreach (Tag tag in tagQuery.ToList())
        {
            sb.AppendFormat("<option>{0}</option>", tag.Tag1);
        }
        return sb.ToString();

    }

    [WebMethod]
    public static string DeleteTag(int id, string tag)
    {
        IQueryable<Tag> tagObject = from d in Entities.Tag
                                    where d.Tag1 == tag && d.DocId == id
                                    select d;

        if (tagObject.Count() > 0)
            Entities.DeleteObject(tagObject.First());
        else
            return "Could not delete tag; tag not found";

        if (Entities.SaveChanges() > 0)
            return string.Empty;

        return "Could not add the new tag; there was a problem";

    }

    [WebMethod]
    public static string ToggleStatus(int id)
    {

        IQueryable<Documents> documentQuery = from d in Entities.Documents.Include("Xlk_Status")
                                              where d.DocId == id
                                              select d;

        if (documentQuery.Count() > 0)
        {
            Documents doc = documentQuery.First();
            doc.Xlk_StatusReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte((doc.Xlk_Status.StatusId == 1) ? 2 : 1));
        }
        else
            return "Could not toggle the status, document not found";

        if (Entities.SaveChanges() > 0)
            return string.Empty;

        return "Could not add the new tag; there was a problem";

    }

    private static bool TagExists(int id, string tag)
    {

        var i = from d in Entities.Tag
                where d.Tag1 == tag && d.DocId == id
                select d.DocId;

        return (i.Count() > 0);

    }

    private static byte NextTagId(int id, byte step)
    {

        IQueryable<Tag> tagQuery = from t in Entities.Tag
                                   where t.DocId == id
                                   select t;
        if (tagQuery.Count() > 0)
            return (byte)(tagQuery.Max(t => t.TagId) + step);

        return step;

    }

    #endregion
}
