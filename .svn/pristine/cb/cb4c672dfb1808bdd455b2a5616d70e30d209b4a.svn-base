﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Enrollment;
using System.Text;
using System.Web.Services;

public partial class Enrollments_AddStudentAssignedBookControl : BaseEnrollmentControl
{
    protected Int32 _studentId;
    protected AssignedBook _assignedBook;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("AssignedBookId") != null)
        {
            Int32 assignedBookId = (Int32)GetValue("AssignedBookId");
                _assignedBook = (from book in Entities.AssignedBooks.Include("Xlk_CourseBooks").Include("Xlk_CourseBookPaymentType").Include("Student")
                                 where book.AssignedBookId == assignedBookId
                                 select book).FirstOrDefault();
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    #region GetIds

    protected string GetStudentId
    {
        get
        {
            if (_assignedBook != null)
                return _assignedBook.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetAssignedBookId
    {
        get
        {
            if (_assignedBook != null)
                return _assignedBook.AssignedBookId.ToString();

            if (Parameters.ContainsKey("AssignedBookId"))
                return Parameters["AssignedBookId"].ToString();

            return "0";
        }
    }

    #endregion

    #region Get Dropdown lists & other data

    protected string GetCourseBookList()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_assignedBook != null && _assignedBook.Xlk_CourseBooks != null) ? _assignedBook.Xlk_CourseBooks.CourseBookId : (Int16?)null;

        foreach (Xlk_CourseBooks item in LoadCourseBooks())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.CourseBookId, item.CourseBookName, (_current.HasValue && item.CourseBookId == _current) ? "Selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetCourseBookPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_assignedBook != null && _assignedBook.Xlk_CourseBookPaymentType != null) ? _assignedBook.Xlk_CourseBookPaymentType.CourseBookPaymentTypeId : (Int16?)null;

        foreach (Xlk_CourseBookPaymentType item in LoadCourseBookPaymentTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.CourseBookPaymentTypeId, item.CourseBookPaymentType, (_current.HasValue && item.CourseBookPaymentTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetFromDate
    {
        get
        {
            if (_assignedBook != null)
                return _assignedBook.FromDate.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetReturnedDate
    {
        get
        {
            if (_assignedBook != null && _assignedBook.ReturnedDate.HasValue)
                return _assignedBook.ReturnedDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetMoneyIn
    {
        get
        {
            if (_assignedBook != null)
                return _assignedBook.MoneyIn.Value.ToString();

            return null;
        }
    }

    protected string GetRefunded
    {
        get
        {
            if (_assignedBook != null && _assignedBook.Refunded.HasValue)
                return _assignedBook.Refunded.Value.ToString();

            return null;
        }
    }

    protected string GetOtherBook
    {
        get
        {
            if (_assignedBook != null && _assignedBook.OtherBook != null)
            {
                return _assignedBook.OtherBook.ToString();
            }

            return "";
        }
    }

    #endregion
}