﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using Pace.DataAccess.Security;
using System.Globalization;
using System.Configuration;
using Accommodation = Pace.DataAccess.Accommodation;
using System.Text;
using System.Diagnostics;
using log4net;
using Pace.Common;

/// <summary>
/// Summary description for BasePage
/// </summary>
public partial class BasePage : System.Web.UI.Page
{
    private static readonly ILog log = LogManager.GetLogger(typeof(BasePage).Name);

    public BasePage()
	{
	}

    public static string PopUpLink(string Id,object value, string target, string labelText, string methodName)
    {
        if (string.IsNullOrEmpty(labelText))
            return string.Format("<a class=\"popupTrigger\" rel=\"{0},{1}/{2}\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", value.ToString(), System.Web.VirtualPathUtility.ToAbsolute(target), methodName);
        else
            return string.Format("<a href=\"{3}?{0}={1}\" class=\"popupTrigger\" rel=\"{1},{3}/{4}\">{2}</a>", Id.ToString(), value.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target), methodName);
    }   
    public static string PopUpLink(object Id, string target, string labelText, string internalLink,string methodName)
    {
        return string.Format("<a href=\"{2}?StudentId={0}{3}\">{1}</a><a class=\"popupTrigger\" rel=\"{0},{2}/{4}\"></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target), internalLink, methodName);
    }   
    public static string TuitionDaysLink(object Id, string target, string labelText)
    {
        return string.Format("<a href=\"{2}?LessonDays={0}\">{1}</a><a class=\"popupTrigger\" rel=\"{0},{2}/PopupTuitionDays\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target));
    }  
    public static string StudentLink(object Id, string target, string labelText)
    {
        return string.Format("<a href=\"{2}?StudentId={0}\">{1}</a><a class=\"popupTrigger\" rel=\"{0},{2}/PopupStudent\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target));
    }
    public static string StudentLink(object Id, string target, string labelText, string internalLink)
    {
        return string.Format("<a href=\"{2}?StudentId={0}{3}\">{1}</a><a class=\"popupTrigger\" rel=\"{0},{2}/PopupStudent\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target), internalLink);
    }    
    public static string FamilyLink(object Id, string target, string labelText)
    {
        return string.Format("<a href=\"{2}?FamilyId={0}\">{1}</a><a  class=\"popupTrigger\" rel=\"{0},{2}/PopupFamily\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target));
    }
    public static string FamilyLink(object Id, string target, string labelText, string internalLink)
    {
        return string.Format("<a href=\"{2}?FamilyId={0}{3}\">{1}</a><a class=\"popupTrigger\" rel=\"{0},{2}/PopupFamily\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target), internalLink);
    }
    public static string AgencyLink(object Id, string target, string labelText)
    {
        return string.Format("<a href=\"{2}?AgencyId={0}\">{1}</a><a class=\"popupTrigger\" rel=\"{0},{2}/PopupAgent\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target));
    }
    public static string GroupLink(object Id, string target, string labelText)
    {
        if (string.IsNullOrEmpty(labelText))
            return string.Format("<a class=\"popupTrigger\" rel=\"{0},{2}/PopupGroup\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target));
        else
            return string.Format("<a href=\"{2}?GroupId={0}\">{1}</a><a class=\"popupTrigger\" rel=\"{0},{2}/PopupGroup\"><img alt=\"More Info\" src=\"/Content/img/actions/info.png\" /></a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target));
    }
    public static string ExcursionLink(object Id, string target, string labelText)
    {
        return string.Format("<a href=\"{2}?ExcursionId={0}\" class=\"popupTrigger\" rel=\"{0},{2}/PopupExcursion\">{1}</a>", Id.ToString(), labelText, System.Web.VirtualPathUtility.ToAbsolute(target));
    }
    public static string MapLink(string target, string loadfromurl,string labelText)
    {
        return string.Format("<a href=\"http://maps.google.com/maps?q={0}&output=embed\">{3}</a><a class=\"popupTrigger\" rel=\"'{1}',{2}/PopupMap\"><img alt=\"More Info\" src=\"/Content/img/actions/globe.png\" /></a>", target, HttpUtility.UrlEncode(target.Replace(',', ' ')), loadfromurl, labelText);
    }

    protected static bool IsBetween(DateTime startDate, DateTime endDate, DateTime checkDate)
    {
       return startDate.CompareTo(checkDate) * checkDate.CompareTo(endDate) > 0;
    }
    protected static bool DoRangesOverLap(DateTime startDate, DateTime endDate,DateTime compareStartDate,DateTime compareEndDate)
    {
      return ( startDate <= compareEndDate && compareStartDate <= endDate );
    }
   
    protected bool IsFuture(DateTime startDate, DateTime endDate,DateTime compareStartDate,DateTime compareEndDate)
    {
        return endDate < compareStartDate;
    }

    protected string ReportPath(string ReportName, string format, Dictionary<string, object> _params)
    {

        return string.Format("{0}?/PaceManagerReports/{1}{2}&rs:Command=Render&rs:Format={3}&rc:Parameters=False", BasePage.ReportServerURL, ReportName, _params.ToQueryString(), format);

    }
    protected string ReportPath(string ReportName, Dictionary<string, object> _params)
    {
        return string.Format("{0}?/PaceManagerReports/{1}{2}&rs:Command=Render&rs:Format=WORD&rc:Parameters=False", BasePage.ReportServerURL, ReportName, _params.ToQueryString());

    }
    protected string ReportPath(string ReportName,string format)
    {
        return string.Format("{0}?/PaceManagerReports/{1}&rs:Command=Render&rs:Format={2}&rc:Parameters=False", BasePage.ReportServerURL, ReportName, format);

    }
    protected string ReportPath(string ReportName)
    {
        return string.Format("{0}?/PaceManagerReports/{1}&rs:Command=Render&rs:Format=WORD&rc:Parameters=False", BasePage.ReportServerURL, ReportName);

    }

    protected static string ReportServerURL
    {
        get
        {
            return ConfigurationManager.AppSettings["ReportServerURL"];
        }
    }

    protected static string DateFormatString
    {
        get
        {
            return ConfigurationManager.AppSettings["DateFormatString"];
        }
    }

    protected static void WriteToLog(string message)
    {
        if (log.IsDebugEnabled)
        {
            log.Info(message);
        }
    }

    public static string TuitionDays(int bitstring)
    {
        StringBuilder sb = new StringBuilder();

        foreach (var item in  Enum.GetValues(typeof(TuitionDays)).Cast<TuitionDays>())
        {
            sb.AppendFormat("<label style=\"display:block;\"><input id=\"HostingId\" type=\"checkbox\" value=\"1516\" style=\"width:50px;float:left;\" name=\"Hostingids\" {0}>{1}</label>", ((TuitionDays)Enum.ToObject(typeof(TuitionDays), bitstring)).HasFlag(item) ? "checked" : string.Empty, Enum.GetName(typeof(TuitionDays), item));
        }
        return sb.ToString();
    }

    public static IEnumerable<T> GetValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

    protected static string CreateName(object firstName, object secondName)
    {
        if (firstName != null && secondName != null)
        {
            return string.Format("{0} {1}", firstName, secondName);
        }
        else
        {
            return (firstName != null) ? firstName.ToString() : secondName.ToString();
        }
    }

    public static DateTime FirstDayOfWeek(DateTime date, CalendarWeekRule rule)
    {

        int weeknumber = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, rule, DayOfWeek.Monday);

        DateTime jan1 = new DateTime(date.Year, 1, 1);

        int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        DateTime firstMonday = jan1.AddDays(daysOffset);

        var cal = CultureInfo.CurrentCulture.Calendar;
        int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

        if (firstWeek <= 1)
        {
            weeknumber -= 1;
        }

        DateTime result = firstMonday.AddDays((weeknumber-1) * 7);

        return result;
    }

    public static SecurityEntities SecurityEntities
    {
        get
        {
            SecurityEntities value;

            if (CacheManager.Instance.GetFromCache<SecurityEntities>("SecurityObject", out value))
                return value;

            return CacheManager.Instance.AddToCache<SecurityEntities>("SecurityObject", new SecurityEntities());
        }
    }

    protected static string TrimText(object text, int amount)
    {
        if (text != null && !string.IsNullOrEmpty(text.ToString()))
        {
            if (text.ToString().Length > amount)
                return string.Format("{0}...", text.ToString().Substring(0, (text.ToString().Length < amount) ? text.ToString().Length : amount),text);
            else
                return text.ToString();
        }
        return string.Empty;
    }

    protected static string TrimTextA(object text, int amount)
    {
        if (text != null && !string.IsNullOrEmpty(text.ToString()))
        {
            if (text.ToString().Length > amount)
                return string.Format("<a title=\"{1}\">{0}...</a>", text.ToString().Substring(0, (text.ToString().Length < amount) ? text.ToString().Length : amount), text);
            else
                return text.ToString();
        }
        return string.Empty;
    }

    protected string CalculateTimePeriod(object startdate, object enddate, char timeperiod)
    {
        int weeks;
        switch (timeperiod.ToString().ToUpper())
        {
            case "D":
                weeks = (int)(Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays;
                break;
            case "W":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 7);
                break;
            case "M":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 31);
                break;
            default:
                return "0";
        }

        return weeks.ToString();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, int> entitykeys)
    {
        try
        {
            var page = new BasePage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    protected static string CalculateAge(DateTime? dob)
    {
        if (dob.HasValue)
        return Convert.ToInt32(((DateTime.Now - dob.Value).Days)/365).ToString();

        return string.Empty;
    }

    protected static string CalculateTicketExpiry(DateTime FromDate, Int32 ticketType)
    {
        DateTime expiryDate;
        switch (ticketType)
        {
            case 1:
                expiryDate = FromDate.AddDays(7);
                break;
            case 2:
                expiryDate = FromDate.AddDays(14);
                break;
            case 3:
                expiryDate = FromDate.AddDays(21);
                break;
            case 4:
                expiryDate = FromDate.AddDays(28);
                break;
            default:
                return "0";
        }

        return expiryDate.ToString("dd MMMM yyyy");
    }

    protected static string CalculateTimePeriod(DateTime startdate, DateTime? enddate, char timeperiod)
    {
        int weeks;
        DateTime _enddate = (!enddate.HasValue) ? DateTime.Now :enddate.Value;
        switch (timeperiod.ToString().ToUpper())
        {
            case "D":
                weeks = (int)(Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays;
                break;
            case "W":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 7);
                break;
            case "M":
                weeks = (int)Math.Ceiling((Convert.ToDateTime(enddate) - Convert.ToDateTime(startdate)).TotalDays / 31);
                break;
            default:
                return "0";
        }

        return weeks.ToString();
    }

    protected static bool CheckNoConflict(Int32 studentId, DateTime startDate, DateTime endDate)
    {
        if (startDate > endDate)
        {
            return false;
        }
        //using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
        //{
        //    var chechQuery = from h in entity.Hostings
        //                     where h.Student.StudentId == studentId && ((h.ArrivalDate < startDate && h.DepartureDate > startDate) || ((h.ArrivalDate < endDate && h.DepartureDate > endDate)))
        //                     select h;

        //    return chechQuery.Count() == 0;
        //}
        return true;
    }

    protected static Users IsAuthenticatedCurrentUser()
    {
        System.Security.Principal.WindowsIdentity p = System.Security.Principal.WindowsIdentity.GetCurrent();

        if (p != null)
        {
            using (Pace.DataAccess.Security.SecurityEntities entity = new SecurityEntities())
            {
                return (from s in entity.Users where s.DomainUserName == p.Name select s).FirstOrDefault();
            }
        }
        return null;
    }

    protected static Users GetCurrentUser()
    {
        System.Security.Principal.WindowsIdentity p = System.Security.Principal.WindowsIdentity.GetCurrent();
        WriteToLog(p.IsAuthenticated.ToString());
        WriteToLog(p.Name.ToString());

        if (p != null)
        {
            using (Pace.DataAccess.Security.SecurityEntities entity = new SecurityEntities())
            {
                Users user =  (from s in entity.Users where s.DomainUserName == p.Name select s).FirstOrDefault();
                WriteToLog(user.ToString());
                return user;
            }
        }

        return null;
    }

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupTuitionDays(int id)
    {  
        StringBuilder sb = new StringBuilder();
        try
        {
            foreach (var item in Enum.GetValues(typeof(TuitionDays)).Cast<TuitionDays>())
            {
                if (((TuitionDays)Enum.ToObject(typeof(TuitionDays), id)).HasFlag(item))
                    sb.AppendFormat("<label style=\"display:block;\">{0}</label>", item);
            }

            return sb.ToString();

        }


        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopupMap(string id)
    {
        StringBuilder sb = new StringBuilder("<div data-role=\"popup\" id=\"popupMap\" data-overlay-theme=\"a\" data-theme=\"a\" data-corners=\"false\" data-tolerance=\"15,15\">");
        try
        {
            sb.AppendFormat("<iframe src=\"http://maps.google.com/maps?q={0}&output=embed\" width=\"480\" height=\"320\" seamless></iframe></div>",id);

            return sb.ToString();

        }


        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteTabItem(DeleteTabAction actionName, object id)
    {
        try
        {
            object x = null;
            System.Data.Objects.ObjectContext context = null ;

            switch (actionName)
            {
                case DeleteTabAction.FamilyMember:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyMember", "FamilyMemberId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyVisit:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyVisit", "FamilyVisitId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyAvailability:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Availability", "AvailabilityId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyComplaint:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyComplaint", "FamilyComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.Room:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Room", "RoomId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyNote:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyNote", "FamilyNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.FamilyBank:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyBank", "FamilyBankBankId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.FamilyPayment:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".FamilyPayments", "PaymentId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentEnrollment:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Enrollment", "EnrollmentId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentExam:
                    context = BaseTuitionPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentExam", "StudentExamId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.AssignedBook:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AssignedBooks", "AssignedBookId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.StudentHosting:
                    context = BaseAccommodationPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Hostings", "HostingId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentComplaint:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentComplaint", "StudentComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.AgencyComplaint:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AgencyComplaint", "AgencyComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.AgencyNote:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AgencyNote", "AgencyNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.StudentNote:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentNote", "StudentNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.AssignedBusTicket:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".AssignedTickets", "AssignedTicketId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.StudentTransfer:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentTransfer", "StudentTransferId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.PreTests:
                    context = BaseTuitionPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".PreTests", "PreTestId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupNote:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupNote", "GroupNoteId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupTransfer:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupTransfer", "GroupTransferId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupComplaint:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupComplaint", "GroupComplaintId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupInvoice:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupInvoice", "GroupInvoiceId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.StudentInvoice:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".StudentInvoice", "StudentInvoiceId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupStudent:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Student", "StudentId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupOrderedItem:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".OrderedItems", "OrderedItemId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.GroupEnrollment:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Enrollment", "EnrollmentId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.ClassTransfer:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".ClassTransfer", "ClassTransferId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.GroupExcursion:
                    context = BaseGroupPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".GroupExcursion", "GroupExcursionId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.ExcursionStudentLink:
                    context = BaseExcursionPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Lnk_Student_Booking", "StudentBookingId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.VehiclePricePlan:
                    context = BaseTransportPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".VehiclePricePlans", "VehiclePricePlanId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.Vehicle:
                    context = BaseTransportPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Vehicles", "VehicleId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.OrderedItem:
                    context = BaseEnrollmentPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".OrderedItems", "OrderedItemId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.Contact:
                    context = BaseContactPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".ContactDetails", "ContactId", Convert.ToInt32(id)));
                    break;
                case DeleteTabAction.SupportSchedule:
                    context = BaseSupportPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Schedulers", "ScheduleId", Convert.ToInt16(id)));
                    break;
                case DeleteTabAction.Task:
                    context = BaseSupportPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Task", "TaskId", Convert.ToInt64(id)));
                    break;
                case DeleteTabAction.TemplateTask:
                    context = BaseSupportPage.CreateEntity;
                    x = context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".TemplateTasks", "TemplateTaskId", Convert.ToInt16(id)));
                    break;
                default:
                    break;
            }
            if (x != null && context != null)
            {
                context.DeleteObject(x);
                context.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteParentChildTabItem(DeleteRelationshipTabItem actionName, object parentid, object childid)
    {
        try
        {
            System.Data.Objects.ObjectContext context = null;

            switch (actionName)
            {
                case DeleteRelationshipTabItem.FamilyTag:
                    context = BaseAccommodationPage.CreateEntity;
                    Pace.DataAccess.Accommodation.Family family = (Pace.DataAccess.Accommodation.Family)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Family", "FamilyId", Convert.ToInt32(parentid)));
                    Pace.DataAccess.Accommodation.Tag tag = (Pace.DataAccess.Accommodation.Tag)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Tag", "TagId", Convert.ToInt32(childid)));
                    family.Tag.Attach(tag);
                    family.Tag.Remove(tag);
                    break;
                case DeleteRelationshipTabItem.StudentTag:
                    context = BaseAccommodationPage.CreateEntity;
                    //Pace.DataAccess.Enrollment.Student student = (Pace.DataAccess.Enrollment.Student)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Student", "StudentId", Convert.ToInt32(parentid)));
                    //Pace.DataAccess.Enrollment.Tag tag = (Pace.DataAccess.Enrollment.Tag)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Tag", "TagId", Convert.ToInt32(childid)));
                    //student.Tag.Remove(tag);
                    break;
                case DeleteRelationshipTabItem.ClosedGroupExcursionBooking:
                    context = BaseExcursionPage.CreateEntity;
                    Pace.DataAccess.Excursion.Lnk_GroupExcursion_Booking lnk_GroupExcursion_Booking = (Pace.DataAccess.Excursion.Lnk_GroupExcursion_Booking)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Lnk_GroupExcursion_Booking", new Dictionary<string, object> { { "GroupId", parentid }, { "BookingId", childid } }));
                    context.DeleteObject(lnk_GroupExcursion_Booking);
                    break;
                case DeleteRelationshipTabItem.OpenGroupExcursionBooking:
                    context = BaseExcursionPage.CreateEntity;
                    Pace.DataAccess.Excursion.Lnk_Group_Booking lnk_Group_Booking = (Pace.DataAccess.Excursion.Lnk_Group_Booking)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".Lnk_Group_Booking", new Dictionary<string, object> { { "GroupId", parentid }, { "BookingId", childid } }));
                    context.DeleteObject(lnk_Group_Booking);
                    break;
                case DeleteRelationshipTabItem.ExcursionCost:
                    context = BaseExcursionPage.CreateEntity;
                    Pace.DataAccess.Excursion.ExcursionCost excursionCost = (Pace.DataAccess.Excursion.ExcursionCost)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".ExcursionCosts", new Dictionary<string, object> { { "ExcursionId", parentid }, { "AttendeeTypeId", Convert.ToByte(childid) } }));
                    context.DeleteObject(excursionCost);
                    break;
                case DeleteRelationshipTabItem.ExcursionDocument:
                    context = BaseExcursionPage.CreateEntity;
                    Pace.DataAccess.Excursion.ExcursionDocument excursionDocument = (Pace.DataAccess.Excursion.ExcursionDocument)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".ExcursionDocuments", new Dictionary<string, object> { { "ExcursionId", parentid }, { "DocId", childid } }));
                    context.DeleteObject(excursionDocument);
                    break;
                case DeleteRelationshipTabItem.QuoteItem:
                    context = BaseAgentPage.CreateEntity;
                    Pace.DataAccess.Agent.QuoteItem quoteItem = (Pace.DataAccess.Agent.QuoteItem)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".QuoteItems", new Dictionary<string, object> { { "QuoteId", parentid }, { "LineId", Convert.ToInt16(childid) } }));
                    context.DeleteObject(quoteItem);
                    break;
                case DeleteRelationshipTabItem.BookingStaff:
                    context = BaseExcursionPage.CreateEntity;
                    Pace.DataAccess.Excursion.BookingStaff staff = (Pace.DataAccess.Excursion.BookingStaff)context.GetObjectByKey(new System.Data.EntityKey(context.DefaultContainerName + ".BookingStaffs", new Dictionary<string, object> { { "BookingId", parentid }, { "ContactId", Convert.ToInt32(childid) } }));
                    context.DeleteObject(staff);
                    break;
                    
                default:
                    break;
            }


            if (context != null)
            {
                context.SaveChanges();
            }
            return string.Empty;

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveHosting(Pace.DataAccess.Accommodation.Hosting newObject)
    {
        Pace.DataAccess.Accommodation.Hosting hosting = new Pace.DataAccess.Accommodation.Hosting();

        try
        {
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                if (newObject.HostingId > 0)
                    hosting = (from e in entity.Hostings where e.HostingId == newObject.HostingId select e).FirstOrDefault();

                if (CheckNoConflict(newObject.Student.StudentId, newObject.ArrivalDate, newObject.DepartureDate))
                {
                    hosting.ArrivalDate = newObject.ArrivalDate;
                    hosting.DepartureDate = newObject.DepartureDate;
                    hosting.WeeklyRate = newObject.WeeklyRate;

                    hosting.StudentReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Student", newObject.Student);
                    hosting.FamilyReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Family", newObject.Family);

                    if (hosting.HostingId == 0)
                        entity.AddToHostings(hosting);

                    entity.SaveChanges();

                    return string.Empty;
                }
                else
                    return "There was a problem with the dates, please try again!";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    #endregion

}


public enum DeleteRelationshipTabItem
{
    FamilyTag,
    StudentTag,
    ClosedGroupExcursionBooking,
    OpenGroupExcursionBooking,
    ExcursionCost,
    ExcursionDocument,
    QuoteItem,
    BookingStaff,
}

public enum DeleteTabAction
{
    FamilyMember,
    FamilyVisit,
    FamilyComplaint,
    FamilyAvailability,
    FamilyBank,
    Room,
    FamilyNote,
    StudentEnrollment,
    StudentHosting,
    StudentComplaint,
    AgencyComplaint,
    AgencyNote,
    StudentNote,
    StudentTransfer,
    GroupNote,
    GroupTransfer,
    GroupComplaint,
    GroupStudent,
    GroupEnrollment,
    GroupExcursion,
    ClassTransfer,
    StudentExam,
    AssignedBook,
    FamilyPayment,
    PreTests,
    AssignedBusTicket,
    ExcursionStudentLink,
    VehiclePricePlan,
    Vehicle,
    OrderedItem,
    GroupOrderedItem,
    GroupInvoice,
    StudentInvoice,
    Contact,
    SupportSchedule,
    Task,
    TemplateTask,
}

[Flags]
public enum TuitionDays
{
    Mon = 0x1,
    Tue = 0x2,
    Wed = 0x4,
    Thur = 0x8,
    Fri = 0x10,
    Sat = 0x20,
    Sun = 0x40,
}