﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Contacts;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using System.Text;

public partial class ContactsPage : BaseContactPage
{

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Set datasource of the Category DropDown
            Category.DataSource = this.LoadCategories();
            Category.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);
        }

    }

    protected void ShowSelected(string action)
    {
        Click_LoadByCategory(action.ToLower());

        //switch (action.ToLower())
        //{
        //    case "administration":
        //        Click_LoadByCategory(action.ToLower());
        //        break;
        //    case "current":
        //        Click_LoadCurrentStudents();
        //        break;
        //    case "arriving":
        //        Click_Arriving();
        //        break;
        //    case "departing":
        //        Click_Departing();
        //        break;
        //    case "cancelled":
        //        Click_Cancelled();
        //        break;
        //    case "noenrollments":
        //        Click_LoadNoEnrollmentsStudents();
        //        break;
        //    case "nohostings":
        //        Click_LoadNoHostingStudents();
        //        break;
        //    case "allnotgroups":
        //        Click_LoadAllNotGroups();
        //        break;
        //    case "birthdays":
        //        Click_Birthdays();
        //        break;
        //    case "lastentered":
        //        Click_Last(20);
        //        break;
        //    default:
        //        Click_LoadAllStudents();
        //        break;
        //}
    }

    protected void Click_LoadByCategory(string category)
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            var StudentSearchQuery = from f in entity.ContactDetails.Include("Xlk_Status").Include("Xlk_Type")
                                     where f.Xlk_Type.Xlk_Category.Description == category
                                     orderby f.FirstName ascending, f.SurName ascending
                                     select f;
            LoadResults(StudentSearchQuery.ToList());
        }

    }

    protected void category_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }


    protected void type_SelectedIndexChanged(object sender, EventArgs e)
    {
        TypeChanged();
    }


    protected void search_Click(object sender, EventArgs e)
    {
        string searchString = lookupString.Text;

        using (ContactsEntities entity = new ContactsEntities())
        {
            var ContactSearchQuery = ((from d in entity.ContactDetails
                                       where (d.CompanyName.Contains(searchString) | d.SurName.Contains(searchString) | d.FirstName.Contains(searchString)) | d.Tag.Any(f => f.Tag1.Contains(searchString))
                                       orderby d.FirstName ascending, d.SurName ascending
                                       select d).Distinct() as ObjectQuery<ContactDetails>).Include("Xlk_Type").Include("Tag");

            LoadResults(ContactSearchQuery.ToList());
        }
    }

    protected void type_DataBound(object sender, EventArgs e)
    {
        TypeChanged();
    }
    protected void category_DataBound(object sender, EventArgs e)
    {
        LoadTypesToDropDown();
    }

    #endregion

    #region Private Methods

    private void LoadTypesToDropDown()
    {
        //Set datasource of the Type DropDown
        ListItem _category = Category.SelectedItem;
        type.DataSource = this.LoadTypes(Convert.ToInt32(_category.Value));
        type.DataBind();
        lookupString.Text = "";
    }

    private void TypeChanged()
    {
        if (type.SelectedItem != null)
        {
            Int32 _typeId = Convert.ToInt32(type.SelectedItem.Value);

            using (ContactsEntities entity = new ContactsEntities())
            {
                var ContactSearchQuery = from d in entity.ContactDetails
                                         where d.Xlk_Type.TypeId == _typeId && d.Xlk_Status.StatusId == 1
                                         orderby d.FirstName ascending, d.SurName ascending
                                         select d;

                LoadResults(ContactSearchQuery.ToList());
            }
            
            
        }
    }

    private void LoadResults(List<ContactDetails> contacts)
    {
        //Set datasource of the repeater
        results.DataSource = contacts;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}",contacts.Count().ToString());  
    }

    protected string CreateTagList(object tags)
    {
        System.Text.StringBuilder tagsString = new System.Text.StringBuilder();

        var s = from st in (System.Data.Objects.DataClasses.EntityCollection<Tag>)tags
                select st.Tag1;

        return String.Join("/", s.ToArray());
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    public static string AddDocumentTag(int id, string tag)
    {
        if (!TagExists(id, tag))
        {
            using (ContactsEntities entity = new ContactsEntities())
            {
                Tag newtag = new Tag();
                newtag.ContactDetailsReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ContactDetails", "ContactId", id);
                newtag.ContactId = id;
                newtag.TagId = NextTagId(id, 1);
                newtag.Tag1 = tag;

                entity.AddToTag(newtag);

                if (entity.SaveChanges() > 0)
                    return string.Empty;
            }

            return "Could not add the new tag; there was a problem";

        }
        else
            return "Could not add the new tag; tag already exists";
    }

    [WebMethod]
    public static string LoadTags(int id)
    {
        StringBuilder sb = new StringBuilder();

        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Tag> tagQuery = from d in entity.Tag
                                       where d.ContactId == id
                                       select d;

            foreach (Tag tag in tagQuery.ToList())
            {
                sb.AppendFormat("<option>{0}</option>", tag.Tag1);
            }
            return sb.ToString();
        }
    }

    [WebMethod]
    public static string DeleteTag(int id, string tag)
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Tag> tagObject = from d in entity.Tag
                                        where d.Tag1 == tag && d.ContactId == id
                                        select d;

            if (tagObject.Count() > 0)
                entity.DeleteObject(tagObject.First());
            else
                return "Could not delete tag; tag not found";

            if (entity.SaveChanges() > 0)
                return string.Empty;
        }

        return "Could not add the new tag; there was a problem";

    }


    private static bool TagExists(int id, string tag)
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            var i = from d in entity.Tag
                    where d.Tag1 == tag && d.ContactId == id
                    select d.ContactId;

            return (i.Count() > 0);
        }
    }

    private static byte NextTagId(int id, byte step)
    {
        using (ContactsEntities entity = new ContactsEntities())
        {
            IQueryable<Tag> tagQuery = from t in entity.Tag
                                       where t.ContactId == id
                                       select t;
            if (tagQuery.Count() > 0)
                return (byte)(tagQuery.Max(t => t.TagId) + step);

            return step;
        }

    }

    #endregion

}

