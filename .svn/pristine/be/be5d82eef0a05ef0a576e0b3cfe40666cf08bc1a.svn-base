﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using PaceManager.Tuition;
using System.Data.Objects;
using System.Data;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;

public partial class Tuition_ClassPage : BaseTuitionPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listclasses.DataSource = this.LoadTuitionLevels();
            listclasses.DataBind();
            listprogrammetype.DataSource = this.LoadProgrammeTypes();
            listprogrammetype.DataBind();
            listprogrammetype.DataSource = this.LoadProgrammeTypes();
            listprogrammetype.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

            lookupString.Focus();
        }
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_SearchClasses();
                break;
            case "current":
                Click_LoadCurrentClasses();
                break;
            case "future":
                Click_Future();
                break;
            case "previous":
                Click_Previous();
                break;
            default:
                Click_LoadAllClasses();
                break;
        }
    }
    protected void Click_SearchClasses()
    {


        IQueryable<Class> classSearchQuery = from c in Entities.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                                             select c;

        int classId = Convert.ToInt32(listclasses.SelectedItem.Value);
        int programmeTypeId = Convert.ToInt32(listprogrammetype.SelectedItem.Value);

        if (!string.IsNullOrEmpty(lookupString.Text) || classId > 0 || programmeTypeId > 0)
        {
            if (!string.IsNullOrEmpty(lookupString.Text))
                classSearchQuery = classSearchQuery.Where(s => s.Teacher.Name.Contains(lookupString.Text) || s.ClassRoom.Label.Contains(lookupString.Text) || (s.TuitionLevel.Description.Contains(lookupString.Text) || s.TuitionLevel.CEFR.Contains(lookupString.Text) || s.TuitionLevel.Label.Contains(lookupString.Text)));

            if (classId > 0)
                classSearchQuery = classSearchQuery.Where(s => s.ClassId == classId);

            if (programmeTypeId > 0)
                classSearchQuery = classSearchQuery.Where(s => s.Xlk_ProgrammeType.ProgrammeTypeId == programmeTypeId);
        }

        LoadResults(classSearchQuery.ToList()); lookupString.Text = ""; lookupString.Focus();

    }

    protected void Click_LoadCurrentClasses()
    {
        var classSearchQuery = from c in Entities.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                               where (c.StartDate <= DateTime.Now && c.EndDate >= DateTime.Now)
                               orderby c.Label
                               select c;
        LoadResults(classSearchQuery.ToList());
    }
    protected void Click_Future()
    {
        var classSearchQuery = from c in Entities.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                               where c.StartDate > DateTime.Now
                               orderby c.Label
                               select c;
        LoadResults(classSearchQuery.ToList());
    }

    protected void Click_Previous()
    {
        var classSearchQuery = from c in Entities.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                               where c.EndDate < DateTime.Now
                               orderby c.Label
                               select c;
        LoadResults(classSearchQuery.ToList());
    }

    protected void Click_LoadAllClasses()
    {
        var classSearchQuery = from c in Entities.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                               orderby c.Label
                               select c;
        LoadResults(classSearchQuery.ToList());
    }

    private void LoadResults(List<Class> classes)
    {
        //Set the datasource of the repeater
        results.DataSource = classes;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", classes.Count().ToString());
    }

    protected void lookupclass(object sender, EventArgs e)
    {
        Click_SearchClasses();
    }

    protected void Click_LoadClasses()
    {
        int classLevelId = Convert.ToInt32(listclasses.SelectedItem.Value);

        if (classLevelId > 0)
        {
            var classSearchQuery = from c in Entities.Class.Include("Student").Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType")
                                   select c;

            if (classLevelId > 0)
                classSearchQuery = classSearchQuery.Where(c => c.TuitionLevel.TuitionLevelId == classLevelId);

            results.DataSource = classSearchQuery.ToList();
        }
        else
            results.DataSource = null;

        results.DataBind();
        listclasses.SelectedIndex = 0;
    }
    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string CloneAndScheduleClass(int classid)
    {
        try
        {
            var cloneQuery = from c in Entities.CloneAndScheduleNewClass(classid)
                             select c;

            Class _class = cloneQuery.FirstOrDefault();
            if (_class != null)
                return string.Format("ViewClassPage.aspx?ClassId={0}", _class.ClassId);

            return "";

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    #endregion
}
