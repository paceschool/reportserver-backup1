﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Transport;
using System.Data;
using System.IO;
using System.Web.Services;
using Pace.DataAccess.Security;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script.Serialization;


public partial class Transport_ViewBookingPage : BaseTransportPage
{
    #region Events
    protected Int32 _bookingId;
    protected Int32[] BookId = new Int32[100];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["BookingId"]))
            _bookingId = Convert.ToInt32(Request["BookingId"]);

        if (!Page.IsPostBack)
        {

            using (TransportEntities entity = new TransportEntities())
            {

                //TripType.DataSource = GetFromCache<Xlk_TripType>(entity);
                //TripType.DataBind();
                LoadTypes();
                LoadUsers();
                //Dropoff.DataSource = GetFromCache<Xlk_Location>(entity);
                //Dropoff.DataBind();
                //Pickup.DataSource = GetFromCache<Xlk_Location>(entity);
                //Pickup.DataBind();
                //RepPickup.DataSource = GetFromCache<Xlk_Location>(entity);
                //RepPickup.DataBind();
                DisplayTransport(LoadTransport(entity, _bookingId));
            }
        }
    }

    protected void LoadTypes()
    {
        using (TransportEntities entity = new TransportEntities())
        {
            var ttypes = from t in entity.Xlk_TripType
                         select t;
            TripType.DataSource = ttypes.ToList();
            TripType.DataBind();

            var locations = from d in entity.Xlk_Location
                         select d;
            Dropoff.DataSource = locations.ToList();
            Dropoff.DataBind();
            Pickup.DataSource = locations.ToList();
            Pickup.DataBind();
            RepPickup.DataSource = locations.ToList();
            RepPickup.DataBind();

            var vehicles = from v in entity.Xlk_VehicleType
                           select v;
            vehicletype.DataSource = vehicles.ToList();
            vehicletype.DataBind();

            var stat = from s in entity.Xlk_Status
                       select s;
            status.DataSource = stat.ToList();
            status.DataBind();
        }
    }

    protected void LoadUsers()
    {
        using (SecurityEntities entity = new SecurityEntities())
        {
            var users = from u in entity.Users
                        select u;
            userlist.DataSource = users.ToList();
            userlist.DataBind();
        }
    }

    protected Dictionary<TimeSpan, string> GetTransportTimeList()
    {
        Dictionary<TimeSpan, string> returnlist = new Dictionary<TimeSpan, string>();

        TimeSpan _span = new TimeSpan(0, 0, 0);
        TimeSpan _endtimespan = new TimeSpan(23, 30, 0);

        while (_span < _endtimespan)
        {
            _span = _span.Add(new TimeSpan(0, 30, 0));
            returnlist.Add(_span, _span.ToString("hh\\:mm"));
        }

        return returnlist;

    }

    protected void Click_SaveBooking(object sender, EventArgs e)
    {
        using (TransportEntities entity = new TransportEntities())
        {
            Booking newbooking = LoadTransport(entity, _bookingId);

            newbooking.CollectionInfo = CollectionInfo.Text;
            
            if (userlist.SelectedIndex > 0)
                newbooking.UserReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Users", "UserId", Convert.ToInt32(userlist.SelectedItem.Value));
            
            if (!string.IsNullOrEmpty(confdate.Text))
                newbooking.ConfirmedDate = Convert.ToDateTime(confdate.Text);

            newbooking.Title = Title.Text;
            newbooking.DropoffInfo = DropoffInfo.Text;
            newbooking.PickupLocationReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Location", "LocationId", Convert.ToInt16(Pickup.SelectedItem.Value));
            newbooking.RepCollectionLocationReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Location", "LocationId", Convert.ToInt16(RepPickup.SelectedItem.Value));
            newbooking.RequestedDate = Convert.ToDateTime(RequestedDate.Text);
            newbooking.RequestedTime = TimeSpan.Parse(RequestedTime.Text);
            newbooking.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(status.SelectedItem.Value));
            newbooking.Xlk_TripTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_TripType", "TripTypeId", Convert.ToByte(TripType.SelectedItem.Value));
            newbooking.NumberOfPassengers = Convert.ToInt32(NumberOfPassengers.Text);
            newbooking.DropoffLocationReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Location", "LocationId", Convert.ToInt16(Dropoff.SelectedItem.Value));
            newbooking.DropoffLocationInfo = DropoffLocationInfo.Text;
            newbooking.PickupLocationInfo = PickupLocationInfo.Text;


            SaveBooking(entity, newbooking);

        }
    }


    #endregion

    #region Private Methods   

    private Booking LoadTransport(TransportEntities entity, Int32 bookingId)
    {
        IQueryable<Booking> excursionQuery = from e in entity.Bookings.Include("Xlk_TripType").Include("Xlk_Status")
                                             where e.BookingId == bookingId
                                             select e;
        if (excursionQuery.ToList().Count() > 0)
            return excursionQuery.ToList().First();

        return null;
    }

    private void DisplayTransport(Booking booking)
    {
        if (booking != null)
        {
            curId.Text = booking.BookingId.ToString();
            curTitle.Text = booking.Title;
            curDate.Text = booking.RequestedDate.ToString("dd MMM yyyy");
            curTime.Text = booking.RequestedTime.ToString("hh\\:mm");
            curTripType.Text = booking.Xlk_TripType.Description;
            curNoOfPasengers.Text = ( booking.NumberOfPassengers.HasValue) ? booking.NumberOfPassengers.Value.ToString() : string.Empty;
            curPickupLocation.Text = booking.PickupLocation.Description;
            curRepPickupLocation.Text = (booking.RepCollectionLocation != null) ? booking.RepCollectionLocation.Description : string.Empty;
            curDropoffLocation.Text = booking.DropoffLocation.Description;

            Title.Text = booking.Title;
            RequestedDate.Text = booking.RequestedDate.ToString("dd MMM yy");
            NumberOfPassengers.Text = (booking.NumberOfPassengers.HasValue) ? booking.NumberOfPassengers.Value.ToString() : "";
            RequestedTime.Text = booking.RequestedTime.ToString("hh\\:mm");
            CollectionInfo.Text = booking.CollectionInfo;
            DropoffInfo.Text = booking.DropoffInfo;
            TripType.Items.FindByValue(booking.Xlk_TripType.TripTypeId.ToString()).Selected = true;
            Pickup.Items.FindByValue(booking.PickupLocation.LocationId.ToString()).Selected = true;
            Dropoff.Items.FindByValue(booking.DropoffLocation.LocationId.ToString()).Selected = true;
            if (booking.RepCollectionLocation != null)
                RepPickup.Items.FindByValue(booking.RepCollectionLocation.LocationId.ToString()).Selected = true;
            if (booking.User != null)
                userlist.Items.FindByValue(booking.User.UserId.ToString()).Selected = true;
            if (booking.ConfirmedDate.HasValue)
            {
                confdate.Text = booking.ConfirmedDate.ToString();
            }
            DropoffLocationInfo.Text = booking.DropoffLocationInfo;
            PickupLocationInfo.Text = booking.PickupLocationInfo;


        }
    }

  private void SaveBooking(TransportEntities entity ,Booking excursion)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayTransport(LoadTransport(entity, _bookingId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }

  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookingGroupTransfers(int id)
  {
      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Transport/AddGroupTransferToBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Attach a Transfer</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">No.</th><th scope=\"col\">Transfer Booking Id</th><th scope=\"col\">Group</th><th scope=\"col\">No. Passengers</th><th scope=\"col\">Transfer Type</th><th scope=\"col\">Date</th><th scope=\"col\">Time</th><th scope=\"col\">Actions</th></thead><tbody>", id));

          using (Pace.DataAccess.Group.GroupEntities gentity = new Pace.DataAccess.Group.GroupEntities())
          {
              var gquery = (from g in gentity.GroupTransfer.Include("Xlk_TransferType").Include("Group")
                            where g.TransportBookingId == id
                            select g).AsEnumerable();


              if (gquery.Count() > 0)
              {
                  var counter = 1;
                  foreach (var gitem in gquery)
                  {
                      sb.AppendFormat("<tr><td style=\"text-align:left\">{0}</td><td style=\"text-align:left\">{1}</td><td style=\"text-align:left\">{7}<a href=\"../Groups/ViewGroupPage.aspx?GroupId={6}\" title=\"Go to Group\"><img src=\"../Content/img/actions/next.png\"/></a></td><td style=\"text-align:left\">{2}</td><td style=\"text-align:left\">{3}</td><td style=\"text-align:left\">{4}</td><td style=\"text-align:left\">{5}</td><td><a title=\"Remove Vehicle from Booking\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewBookingPage.aspx','GroupTransferTransportBooking',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td>", gitem.GroupTransferId, gitem.TransportBookingId, gitem.NumberOfPassengers, gitem.Xlk_TransferType.Description, gitem.Date.ToLongDateString(), gitem.Time.HasValue ? gitem.Time.Value.ToString() : string.Empty, gitem.Group != null ? gitem.Group.GroupId : 0, gitem.Group != null ? gitem.Group.GroupName : string.Empty, gitem.TransportBookingId, gitem.GroupTransferId);
                      counter++;
                  }

              }
              else
              {
                  sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
              }
          }



          sb.Append("</tbody></table>");

          return sb.ToString();
      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }
  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookingStudentTransfers(int id)
  {
      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Transport/AddStudentTransferToBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Attach a Transfer</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">No.</th><th scope=\"col\">Transfer Booking Id</th><th scope=\"col\">Group</th><th scope=\"col\">No. Passengers</th><th scope=\"col\">Transfer Type</th><th scope=\"col\">Date</th><th scope=\"col\">Time</th><th scope=\"col\">Actions</th></thead><tbody>", id));

          using (Pace.DataAccess.Enrollment.EnrollmentsEntities gentity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
          {
              var gquery = (from g in gentity.StudentTransfer.Include("Xlk_TransferType").Include("Student")
                            where g.TransportBookingId == id
                            select g).AsEnumerable();


              if (gquery.Count() > 0)
              {
                  var counter = 1;
                  foreach (var gitem in gquery)
                  {
                      sb.AppendFormat("<tr><td style=\"text-align:left\">{0}</td><td style=\"text-align:left\">{1}</td><td style=\"text-align:left\">{7}<a href=\"../Groups/ViewGroupPage.aspx?GroupId={6}\" title=\"Go to Group\"><img src=\"../Content/img/actions/next.png\"/></a></td><td style=\"text-align:left\">{2}</td><td style=\"text-align:left\">{3}</td><td style=\"text-align:left\">{4}</td><td style=\"text-align:left\">{5}</td><td><a title=\"Remove Vehicle from Booking\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewBookingPage.aspx','StudentTransferTransportBooking',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td>", gitem.StudentTransferId, gitem.TransportBookingId, "1", gitem.Xlk_TransferType.Description, gitem.Date.ToLongDateString(), gitem.Time.HasValue ? gitem.Time.Value.ToString() : string.Empty, gitem.Student != null ? gitem.Student.StudentId : 0, gitem.Student != null ? gitem.Student.SurName : string.Empty, gitem.TransportBookingId, gitem.StudentTransferId);
                      counter++;
                  }

              }
              else
              {
                  sb.Append("<tr><td colspan=\"8\">No Transfers to Show!</td></tr>");
              }
          }



          sb.Append("</tbody></table>");

          return sb.ToString();
      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }

  [WebMethod]
  [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookingExcursions(int id)
  {
      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Transport/AddExcursionToBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Attach an Excursion</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">No.</th><th scope=\"col\">Excursion Booking Id</th><th scope=\"col\">Transport Booking Id</th><th scope=\"col\">Excursion Booking</th><th scope=\"col\">Actions</th></thead><tbody>", id));

          using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
          {
              var query = (from e in entity.Bookings.Include("Excursion")
                           where e.TransportBookingId == id
                           select e).ToList();

              if (query.Count() > 0)
              {
                  var counter = 1;
                  foreach (var venue in query)
                  {
                      sb.AppendFormat("<tr><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td><a href=\"#\" title=\"Print Options\" onclick=\"showPrintOptions();\"><img src=\"../Content/img/actions/printer.png\" title=\"Print Options\" /></a>&nbsp;<a title=\"Remove Excursion from Booking\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewBookingPage.aspx','ExcursionTransportBooking',{0},{1})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td>", venue.BookingId, venue.TransportBookingId, venue.Excursion.Title, counter, venue.BookingId, venue.TransportBookingId);
                      counter = counter + 1;
                  }
              }
              else
              {
                  sb.Append("<tr><td colspan=\"5\">No Attached Excursions to Show!</td></tr>");
              }
          }

          sb.Append("</tbody></table>");

          return sb.ToString();
      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }

  [WebMethod]
  [ScriptMethod(UseHttpGet=true,ResponseFormat = ResponseFormat.Json)]
  public static string LoadBookingVehicles(int id)
  {
      try
      {
          StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditControl('#dialog-form','ViewBookingPage.aspx','Transport/AddVehicleBookingControl.ascx','BookingId',{0})\"><img src=\"../Content/img/actions/add.png\" />Add a Vehicle</a></span><table id=\"box-table-a\" class=\"tablesorter\"> <thead><tr><th scope=\"col\">No.</th><th scope=\"col\">Veh.Booking Id</th><th scope=\"col\">Ref</th><th scope=\"col\">Type</th><th scope=\"col\">Cost</th><th scope=\"col\">Status</th><th scope=\"col\">Confirmed By</th><th scope=\"col\">Confirmed Date</th><th scope=\"col\">Driver Name</th><th scope=\"col\">Driver Contact No.</th><th scope=\"col\">Vehicle Price Plan</th><th scope=\"col\">Capacity</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));

          using (TransportEntities entity = new TransportEntities())
          {
                  var query = (from n in entity.VehicleBookings.Include("VehiclePricePlan").Include("VehiclePricePlan.Vehicle")
                                                where n.Booking.BookingId == id
                                                select n).ToList();

                  if (query.Count() > 0)
                  {
                      var counter = 1;
                      foreach (var vehicle in query)
                      {
                          sb.AppendFormat("<tr><td style=\"text-align: left\">{9}</td><td style=\"text-align: left\">{9}</td><td style=\"text-align: left\">{13}</td><td style=\"text-align: left\">Type {8} = {0}</td><td style=\"text-align: left\">€{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">€{7}</td><td style=\"text-align:left\">{12}</td><td><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewBookingPage.aspx','Transport/AddVehicleBookingControl.ascx',{{ VehicleBookingId: {11}, BookingId: {10}, VehicleId: {8} }})\"><img src=\"../Content/img/actions/edit.png\" /></a>&nbsp;<a title=\"Remove Vehicle from Booking\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewBookingPage.aspx','VehicleBooking',{11})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", vehicle.VehiclePricePlan.Vehicle.Xlk_VehicleType.Description, vehicle.Cost.HasValue ? vehicle.Cost.Value.ToString("00.00") : string.Empty, vehicle.Xlk_Status.Description, vehicle.User != null ? vehicle.User.Name : null, vehicle.ConfirmedDate.HasValue ? vehicle.ConfirmedDate.Value.ToString("dd/MM/yy") : string.Empty, vehicle.DriverName, vehicle.DriverContactNumber, vehicle.VehiclePricePlan != null ? vehicle.VehiclePricePlan.Cost.HasValue ? vehicle.VehiclePricePlan.Cost.Value.ToString("00.00") : string.Empty : string.Empty, vehicle.VehiclePricePlan.Vehicle.VehicleId, counter, vehicle.Booking.BookingId, vehicle.VehicleBookingId, vehicle.Booking.NumberOfPassengers - vehicle.VehiclePricePlan.MaxCapacity > 0 ? vehicle.Booking.NumberOfPassengers - vehicle.VehiclePricePlan.MaxCapacity + " not seated" : "Capacity OK", vehicle.Reference);
                          counter = counter + 1;
                      }
                  }
                  else
                  {
                      sb.Append("<tr><td colspan=\"12\">No Vehicles to Show!</td></tr>");
                  }
          }

          sb.Append("</tbody></table>");

          return sb.ToString();

      }
      catch (Exception ex)
      {
          return ex.Message;
      }
  }
    
    #endregion
}
