﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Group;
using System.Globalization;
using System.Web.Services;
using System.Text;

public partial class Group_GroupsPage : BaseGroupPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listnationality.DataSource = this.LoadNationalities();
            listnationality.DataBind();

            listagent.DataSource = this.LoadAgents();
            listagent.DataBind();

            Click_LoadCurrentGroups();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

            lookupString.Focus();
        }    
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_LoadGroups();
                break;
            case "current":
                Click_LoadCurrentGroups();
                break;
            case "next":
                Click_NextArrivingGroups();
                break;
            case "arriving":
                Click_Arriving();
                break;
            case "departing":
                Click_Departing();
                break;
            default:
                Click_LoadAllGroups();
                break;
        }
    }

    protected void Click_LoadCurrentGroups()
    {
        DateTime today = DateTime.Now.Date;

        var GroupSearchQuery = from f in Entities.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                 where f.ArrivalDate <= DateTime.Now && f.DepartureDate >= today
                                 orderby f.GroupName ascending
                                 select f;
        LoadResults(GroupSearchQuery.ToList());

    }

    protected void Click_LoadAllGroups()
    {
        var GroupSearchQuery = from f in Entities.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                               orderby f.ArrivalDate ascending
                                 select f;
        LoadResults(GroupSearchQuery.ToList());
    }

    protected void Click_Arriving()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now.AddDays(7), CalendarWeekRule.FirstFullWeek);
        DateTime endarrivingDate = startarrivingDate.AddDays(7);

        var GroupSearchQuery = (from f in Entities.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                where f.ArrivalDate > DateTime.Now
                                orderby f.ArrivalDate ascending
                                select f).ToList().Take(1);
        LoadResults(GroupSearchQuery.ToList());
        listnationality.SelectedIndex = 0;
    }

    protected void Click_Departing()
    {
        DateTime startleavingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime endleavingDate = startleavingDate.AddDays(7);

        var GroupSearchQuery = from g in Entities.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                 where (g.DepartureDate >= startleavingDate && g.DepartureDate <= endleavingDate && g.DepartureDate > DateTime.Now)
                                 orderby g.GroupName ascending
                                 select g;
        LoadResults(GroupSearchQuery.ToList());
        listnationality.SelectedIndex = 0;
    }

    protected void Click_NextArrivingGroups()
    {
        var GroupSearchQuery = (from g in Entities.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                               where g.ArrivalDate > DateTime.Now
                               orderby g.ArrivalDate ascending
                               select g).ToList().Take(4);
        LoadResults(GroupSearchQuery.ToList());
    }

    protected void lookupGroup(object sender, EventArgs e)
    {
        Click_LoadGroups();
    }

    private void LoadResults(List<Group> groups)
    {
        //Set the datasource of the repeater
        results.DataSource = groups;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", groups.Count().ToString());
    }

    protected void Click_LoadGroups()
    {
        int agencyId = Convert.ToInt32(listagent.SelectedItem.Value);
        int nationalityId = Convert.ToInt32(listnationality.SelectedItem.Value);

        if (!string.IsNullOrEmpty(lookupString.Text) || agencyId > 0 || nationalityId > 0)
        {

            var groupSearchQuery = from s in Entities.Group.Include("Xlk_Nationality").Include("Agency").Include("Xlk_Status").Include("GroupInvoice")
                                     select s;

            if (!string.IsNullOrEmpty(lookupString.Text))
                groupSearchQuery = groupSearchQuery.Where(s => s.GroupName.StartsWith(lookupString.Text) || s.GroupInvoice.Count(x => x.SageInvoiceRef.StartsWith(lookupString.Text)) > 0);

            if (agencyId > 0)
                groupSearchQuery = groupSearchQuery.Where(s => s.Agency.AgencyId == agencyId);

            if (nationalityId > 0)
                groupSearchQuery = groupSearchQuery.Where(s => s.Xlk_Nationality.NationalityId == nationalityId);

            results.DataSource = groupSearchQuery.ToList();
        }
        else
            results.DataSource = null;

        results.DataBind();
        listagent.SelectedIndex = 0; listnationality.SelectedIndex = 0; lookupString.Text = ""; lookupString.Focus();
    }

    protected string CreateGroupInvoiceList(object groupinvoices)
    {
        System.Text.StringBuilder invoiceString = new System.Text.StringBuilder();

        var s = from st in (System.Data.Objects.DataClasses.EntityCollection<GroupInvoice>)groupinvoices
                select st.SageInvoiceRef;

        return String.Join("/", s.ToArray());
    }

    [WebMethod]
    public static string LoadSageInvoiceRefs(int groupId)
    {
        StringBuilder sb = new StringBuilder();

        IQueryable<GroupInvoice> sageRefQuery = from d in Entities.GroupInvoice
                                                  where d.Group.GroupId == groupId
                                                  select d;
        foreach (GroupInvoice sageref in sageRefQuery.ToList())
        {
            sb.AppendFormat("<option>{0}</option>", sageref.SageInvoiceRef);
        }
        return sb.ToString();
    }

    [WebMethod]
    public static string DeleteSageInvoiceRef(int groupId, string sageinvoiceref)
    {
        IQueryable<GroupInvoice> sageRefObject = from d in Entities.GroupInvoice
                                                 where d.SageInvoiceRef == sageinvoiceref && d.Group.GroupId == groupId
                                                 select d;
        if (sageRefObject.Count() > 0)
            Entities.DeleteObject(sageRefObject.First());
        else
            return "Could not delete reference: reference not found.";
        if (Entities.SaveChanges() > 0)
            return string.Empty;

        return "Could not add the new reference. There was a problem.";
    }

    [WebMethod]
    public static string AddSageInvoiceRef(int groupId, string sageinvoiceref)
    {
        try
        {
            if (!SageInvoiceRefExists(groupId, sageinvoiceref))
            {
                GroupInvoice groupInvoice = new GroupInvoice();
                groupInvoice.GroupReference.EntityKey = new System.Data.EntityKey(Entities.DefaultContainerName + ".Group", "GroupId", groupId);
                groupInvoice.SageInvoiceRef = sageinvoiceref;
                groupInvoice.DateRecorded = DateTime.Now;

                //Entities.AddToGroupInvoice(groupInvoice);

                //if (Entities.SaveChanges() > 0)
                //    return string.Empty;

                return "Could not save reference, there was a problem.";
            }
            else
                return "Could not save the reference, this reference already exists.";
        }

        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    public static string SaveSageReference(GroupInvoice groupInvoice)
    {
        Entities.AddToGroupInvoice(groupInvoice);

        if (Entities.SaveChanges() > 0)
            return string.Empty;
        else
            return "Could not save reference, there was a problem.";
    }

    [WebMethod]
    private static bool SageInvoiceRefExists(int groupid, string sageinvoiceref)
    {
        var i = from d in Entities.GroupInvoice
                where d.SageInvoiceRef == sageinvoiceref && d.Group.GroupId == groupid
                select d.GroupInvoiceId;
        return (i.Count() > 0);
    }
}
