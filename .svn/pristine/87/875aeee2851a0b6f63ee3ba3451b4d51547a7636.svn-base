﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaceManager.Enrollment;
using System.Text;
using System.Web.Services;
using PaceManager.Security;

public partial class Enrollments_AddStudentBusTicketControl : BaseEnrollmentControl
{
    protected Int32 _studentId;
    protected AssignedTicket _assignedTicket;
    protected Int32 assginedTicketId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetValue("AssignedTicketId") != null)
        {
            Int32 assignedTicketId = (Int32)GetValue("AssignedTicketId");
            _assignedTicket = (from tickets in Entities.AssignedTickets
                          where tickets.AssignedTicketId == assignedTicketId
                          select tickets).FirstOrDefault();
        }
    }

    protected object GetValue(string key)
    {
        if (Parameters.ContainsKey(key))
            return Parameters[key];

        return null;
    }

    protected string GetBusTicketTypeList()
    {
        StringBuilder sb = new StringBuilder();

        Int16? _current = (_assignedTicket != null && _assignedTicket.BusTicket.Xlk_BusTicketType != null) ? _assignedTicket.BusTicket.Xlk_BusTicketType.BusTicketTypeId : (Int16?)null;

        foreach (Xlk_BusTicketType item in LoadBusTicketType())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.BusTicketTypeId, item.BusTicketType, (_current.HasValue && item.BusTicketTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetBusTicketPaymentTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_assignedTicket != null && _assignedTicket.Xlk_BusTicketPaymentType != null) ? _assignedTicket.Xlk_BusTicketPaymentType.BusTicketPaymentTypeId : (byte?)null;

        foreach (Xlk_BusTicketPaymentType item in LoadBusTicketPaymentType())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.BusTicketPaymentTypeId, item.PaymentType, (_current.HasValue && item.BusTicketPaymentTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetBusTicketsList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_assignedTicket != null && _assignedTicket.BusTicket != null) ? _assignedTicket.BusTicket.BusTicketId : (Int32?)null;

        foreach (BusTicket item in LoadBusTickets())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.BusTicketId, item.SerialNumber, (_current.HasValue & item.BusTicketId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetAssignedTicketId
    {
        get
        {
            if (_assignedTicket != null)
                return _assignedTicket.AssignedTicketId.ToString();

            if (Parameters.ContainsKey("AssignedTicketId"))
                return Parameters["AssignedTicketId"].ToString();

            return "0";
        }
    }

    protected string GetStudentId
    {
        get
        {
            if (_assignedTicket != null)
                return _assignedTicket.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetFromDate
    {
        get
        {
            if (_assignedTicket != null)
                return _assignedTicket.FromDate.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetAmountPaid
    {
        get
        {
            if (_assignedTicket != null)
                return _assignedTicket.AmountPaid.ToString();

            return null;
        }
    }

    protected string GetComment
    {
        get
        {
            if (_assignedTicket != null)
                return _assignedTicket.Comment.ToString();

            return null;
        }
    }
}