﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using Pace.DataAccess.Accommodation;
using System.Text;
using System.Data;
using System.Data.Objects;

public partial class Accommodation_ViewAccomodationPage : BaseAccommodationPage
{
    protected Int32 _familyId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["FamilyId"]))
            _familyId = Convert.ToInt32(Request["FamilyId"]);

        if (!Page.IsPostBack)
        {
            listtype.DataSource = this.LoadFamilyTypes();
            listtype.DataBind();

            newzone.DataSource = this.LoadZones();
            newzone.DataBind();

            famstatus.DataSource = this.LoadStatus();
            famstatus.DataBind();

            fampaymenttype.DataSource = this.LoadPaymentMethods();
            fampaymenttype.DataBind();

            DisplayFamily(LoadFamily(new AccomodationEntities(),_familyId));

            if (Request.UrlReferrer != null)
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
        }
    }
    protected void Save_Click(object sender, EventArgs e)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            Family _family = LoadFamily(entity,_familyId);

            _family.FirstName = famfirstname.Text;
            _family.SurName = famsurname.Text;
            _family.Address = famaddress.Text;
            _family.LandLine = famtel.Text;
            _family.Mobile = fammobile.Text;
            _family.EmailAddress = famemail.Text;
            _family.SageRef = sageref.Text;
            _family.Description = famdescription.Text;
            _family.Comments = famcomment.Text;
            _family.GeoCode = famgeocode.Text;
            _family.Xlk_StatusReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Status", "StatusId", Convert.ToByte(famstatus.SelectedItem.Value));
            _family.Xlk_FamilyTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_FamilyType", "FamilyTypeId", Convert.ToByte(listtype.SelectedItem.Value));
            _family.Xlk_PaymentMethodReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_PaymentMethod", "PaymentMethodId", Convert.ToByte(fampaymenttype.SelectedItem.Value));
           
            if (newzone.SelectedIndex > 0)
                _family.Xlk_ZoneReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_Zone", "ZoneId", Convert.ToInt32(newzone.SelectedItem.Value));


            SaveFamily(entity,_family);
        }
    }

    private void SaveFamily(AccomodationEntities entity,Family family)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayFamily(LoadFamily(entity,_familyId));
        }
    }

    private void DisplayFamily(Family family)
    {
        if (family != null)
        {

            famfirstname.Text = family.FirstName;
            famsurname.Text = family.SurName;
            famaddress.Text = family.Address;
            famtel.Text = family.LandLine;
            fammobile.Text = family.Mobile;
            famemail.Text = family.EmailAddress;
            sageref.Text = family.SageRef;
            famdescription.Text = family.Description;
            famstatus.Items.FindByValue(family.Xlk_Status.StatusId.ToString()).Selected = true;
            famgeocode.Text = family.GeoCode;
            
            if (family.Xlk_PaymentMethod != null)
                fampaymenttype.Items.FindByValue(family.Xlk_PaymentMethod.PaymentMethodId.ToString()).Selected = true;

            if (family.Xlk_Zone != null)
                newzone.Items.FindByValue(family.Xlk_Zone.ZoneId.ToString()).Selected = true;

            listtype.Items.FindByValue(family.Xlk_FamilyType.FamilyTypeId.ToString()).Selected = true;

            curFamId.Text = family.FamilyId.ToString();
            curFamName.Text = family.FirstName + " " + family.SurName;
            curFamAdd.Text = TrimText(family.Address, 20);
            curFamAdd.ToolTip = family.Address;
            curFamAdd.NavigateUrl = String.Format("http://maps.google.com/maps?q=" + family.Address + "&output=embed");
            if (family.Xlk_Zone != null)
                curZone.Text = family.Xlk_Zone.Description;
            curPhone.Text = family.LandLine;
            curMob.Text = family.Mobile;
            if (family.EmailAddress != null && family.EmailAddress != "")
            {
                curEmail.NavigateUrl = String.Format("MailTo:" + family.EmailAddress);
                curEmail.ImageUrl = "~/Content/img/actions/email_go.png";
                curEmail.Target = "_blank";
                curEmail.ToolTip = "Click to open email message window";
            }
            else
            {
                curEmail.ImageUrl = "~/Content/img/email_not_available.jpg";
                curEmail.ToolTip = "None Available";
            }
            curSage.Text = family.SageRef;
            curStatus.Text = family.Xlk_Status.Description;
            
        }
    }

    private Family LoadFamily(AccomodationEntities entity,Int32 familyId)
    {
        IQueryable<Family> familyQuery = from f in entity.Family.Include("FamilyMember").Include("FamilyMember.Xlk_FamilyMemberType").Include("Xlk_FamilyType").Include("Xlk_Zone").Include("Xlk_Status").Include("Xlk_PaymentMethod")
                                         where f.FamilyId == familyId
                                         select f;

        if (familyQuery.ToList().Count() > 0)
            return familyQuery.ToList().First();

        return null;


    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> ListCalendarEvents(DateTime start, DateTime end, int id)
    {
        using (AccomodationEntities entity = new AccomodationEntities())
        {
            var studentQuery = from h in entity.Hostings
                               where h.Family.FamilyId == id && ((h.ArrivalDate <= start && h.DepartureDate >= end) || (h.ArrivalDate >= start && h.DepartureDate <= end) || (h.ArrivalDate >= start && h.ArrivalDate <= end) || (h.DepartureDate >= start && h.DepartureDate <= end))
                               select new { Title = h.Student.FirstName + " " + h.Student.SurName, ArrivalDate = h.ArrivalDate, DepartureDate = h.DepartureDate };

            foreach (var item in studentQuery.ToList())
            {
                yield return new { Title = item.Title, ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString() };
            }
        }
    }

  

}
