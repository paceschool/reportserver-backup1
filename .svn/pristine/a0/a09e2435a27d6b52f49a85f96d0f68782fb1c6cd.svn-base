﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Tuition;
using System.Data.Objects;
using System.Data;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;

public partial class Tuition_ViewClassPage : BaseTuitionPage
{
    protected Int32 _classId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["ClassId"]))
            _classId = Convert.ToInt32(Request["ClassId"]);

        if (!Page.IsPostBack)
        {
            newclasslevel.DataSource = this.LoadTuitionLevels();
            newclasslevel.DataBind();
            newclassteacher.DataSource = this.LoadTeachers();
            newclassteacher.DataBind();
            newclassroom.DataSource = this.LoadClassRooms();
            newclassroom.DataBind();
            newclassprogramme.DataSource = this.LoadProgrammeTypes();
            newclassprogramme.DataBind();
            newclasscoursetype.DataSource = BaseEnrollmentControl.LoadCourseTypes();
            newclasscoursetype.DataBind();

            DisplayClass(LoadClass(new TuitionEntities(),_classId));
            LoadStudentList(_classId);
        }
    }

    protected void Click_SaveClass(object sender, EventArgs e)
    {
        string _message = ""; errorcontainer.Visible = false;
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (newclasslevel.SelectedIndex > 0 && newclassteacher.SelectedIndex > 0 && newclassroom.SelectedIndex > 0
                && newclassprogramme.SelectedIndex > 0 && !string.IsNullOrEmpty(newstartdate.Text) && !string.IsNullOrEmpty(newenddate.Text)
                && !string.IsNullOrEmpty(DailyStartTime1.Text) && !string.IsNullOrEmpty(DailyEndTime1.Text))
            {
                Class _newclass = GetClass(entity);

                _newclass.Label = newLabel.Text;
                _newclass.TeacherReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Teacher", "TeacherId", Convert.ToInt16(newclassteacher.SelectedItem.Value));
                _newclass.ClassRoomReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ClassRoom", "ClassroomId", Convert.ToInt16(newclassroom.SelectedItem.Value));
                _newclass.TuitionLevelReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".TuitionLevel", "TuitionLevelId", Convert.ToByte(newclasslevel.SelectedItem.Value));
                _newclass.Xlk_ProgrammeTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_ProgrammeType", "ProgrammeTypeId", Convert.ToByte(newclassprogramme.SelectedItem.Value));
                _newclass.Xlk_CourseTypeReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_CourseType", "CourseTypeId", Convert.ToInt16(newclasscoursetype.SelectedItem.Value));
                _newclass.StartDate = Convert.ToDateTime(newstartdate.Text);
                _newclass.EndDate = Convert.ToDateTime(newenddate.Text);
                _newclass.DailyStartTime = TimeSpan.Parse(newstarttime.Text);
                _newclass.DailyEndTime = TimeSpan.Parse(newendtime.Text);
                _newclass.IsClosed = newclosed.Checked;

                SaveClass(entity,_newclass);
            }
            else
            {
                _message = "Not all fields are complete. Please check.";
                errorcontainer.Visible = true;
                errormessage.Text = _message;
            }
        }
    }

    private void LoadStudentList(Int32 classId) //Display Class History for Student
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var studentsQuery = from e in entity.Enrollment
                                from s in e.ClassSchedule
                                where e.Class.ClassId == classId && s.Class.ClassId == classId && (e.StartDate <= DateTime.Now && e.EndDate >= DateTime.Now)
                                select new { StartDate = s.StartDate, e.Student.FirstName, e.Student.SurName };


            currentStudents.DataSource = studentsQuery.ToList();
            currentStudents.DataBind();
        }

    }

    private Class LoadClass(TuitionEntities entity,Int32 classId)
    {
        IQueryable<Class> classQuery = from c in entity.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType").Include("TuitionLevel")
                                       where c.ClassId == classId
                                       select c;

        if (classQuery.ToList().Count() > 0)
            return classQuery.ToList().First();

        return null;
    }

    private void DisplayClass(Class classinstance)
    {
        if (classinstance != null)
        {
            ClassId.Text = classinstance.ClassId.ToString();
            Label.Text = classinstance.Label;
            if (classinstance.TuitionLevel != null)
                Level.Text = classinstance.TuitionLevel.Description;
            DailyStartTime1.Text = classinstance.DailyStartTime.ToString();
            DailyEndTime1.Text = classinstance.DailyEndTime.ToString();
            if (classinstance.Teacher != null)
                Teacher.Text = classinstance.Teacher.Name;
            if (classinstance.ClassRoom != null)
                Room.Text = classinstance.ClassRoom.Label;
            Dates.Text = string.Format("{0}&nbsp;-&nbsp;{1}", classinstance.StartDate.ToString("dd MMM yy"), classinstance.EndDate.ToString("dd MMM yy"));

            if (classinstance.TuitionLevel != null)
                newclasslevel.Items.FindByValue(classinstance.TuitionLevel.TuitionLevelId.ToString()).Selected = true;
            if (classinstance.Teacher != null)
                newclassteacher.Items.FindByValue(classinstance.Teacher.TeacherId.ToString()).Selected = true;
            if (classinstance.ClassRoom != null)
                newclassroom.Items.FindByValue(classinstance.ClassRoom.ClassroomId.ToString()).Selected = true;
            newclassprogramme.Items.FindByValue(classinstance.Xlk_ProgrammeType.ProgrammeTypeId.ToString()).Selected = true;
            if (classinstance.Xlk_CourseType != null)
                newclasscoursetype.Items.FindByValue(classinstance.Xlk_CourseType.CourseTypeId.ToString()).Selected = true;
            newstartdate.Text = classinstance.StartDate.ToString();
            newenddate.Text = classinstance.EndDate.ToString();
            newstarttime.Text = classinstance.DailyStartTime.ToString();
            newendtime.Text = classinstance.DailyEndTime.ToString();
            newclosed.Checked = classinstance.IsClosed;
            newLabel.Text = classinstance.Label;
        }
    }

    private Class GetClass(TuitionEntities entity)
    {
        return LoadClass(entity,_classId);
    }

    private void SaveClass(TuitionEntities entity,Class classinstance)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayClass(LoadClass(entity, _classId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }


    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadClassTransfers(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewClassPage.aspx','Tuition/AddClassTransferControl.ascx',{ ClassId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Transfer</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Class Transfer Id</th><th scope=\"col\">From Class</th><th scope=\"col\">To Class</th><th scope=\"col\">Action Date</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.ClassTransfer.Include("FromClass").Include("ToClass")
                            where n.FromClass.ClassId == id || n.ToClass.ClassId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (ClassTransfer classTransfer in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\"><a href=\"ViewClassPage.aspx?ClassId={4}\" >{1} {4}</a></td> <td style=\"text-align: left\"><a href=\"ViewClassPage.aspx?ClassId={5}\" >{2} {5}</a></td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: center\"><a title=\"Delete Class Transfer\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewClassPage.aspx','ClassTransfer',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", classTransfer.ClassTransferId, classTransfer.FromClass.Label, classTransfer.ToClass.Label, classTransfer.ActionDate.ToString("D"), classTransfer.FromClass.ClassId, classTransfer.ToClass.ClassId);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"7\">No Transfer to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadStudentSchedules(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Enrollment Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">Action Date</th><th scope=\"col\">From Class</th><th scope=\"col\">To Class</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.ClassSchedule.Include("Class").Include("Enrollment.Student").Include("Enrollment.Class")
                            where n.Class.ClassId == id && !n.DateProcessed.HasValue
                            select n;

                if (query.Count() > 0)
                {
                    foreach (ClassSchedule classSchedule in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: center\"><a title=\"Delete Enrollment\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupEnrollment',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", classSchedule.EnrollmentId, classSchedule.Enrollment.Student.StudentId, BasePage.StudentLink(classSchedule.Enrollment.Student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(classSchedule.Enrollment.Student.FirstName, classSchedule.Enrollment.Student.SurName)), classSchedule.StartDate.ToString("D"), (classSchedule.Class.ClassId == id) ? (classSchedule.Enrollment.Class != null) ? classSchedule.Enrollment.Class.Label : "New Enrollment" : classSchedule.Class.Label, (classSchedule.Class.ClassId == id) ? classSchedule.Class.Label : classSchedule.Enrollment.Class.Label);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"7\">No Schedules to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    
    [WebMethod]
    public static string SaveClassTransfer(ClassTransfer newObject)
    {
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                ClassTransfer newclassTransfer = new ClassTransfer();

                newclassTransfer.ActionDate = newObject.ActionDate;
                newclassTransfer.FromClassReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".FromClass", newObject.FromClass);
                newclassTransfer.ToClassReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".ToClass", newObject.ToClass);
                newclassTransfer.ClassTransferId = newObject.ClassTransferId;

                if (newclassTransfer.ClassTransferId == 0)
                    entity.AddToClassTransfer(newclassTransfer);

                entity.SaveChanges();
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    #endregion

}
