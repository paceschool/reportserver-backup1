﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using PaceManager.Group;
using Accommodation = PaceManager.Accommodation;

public partial class Accommodation_PlanAccommodationPage : BaseGroupPage
{
    protected string _action = "assign";

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["Action"]))
            _action = Request["Action"];

        if (!Page.IsPostBack)
        {
            startDate.Text = DateTime.Now.ToString("dd/MM/yy");
            endDate.Text = DateTime.Now.AddDays(14).ToString("dd/MM/yy");
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                familyStatusList.DataSource = (from s in entity.Xlk_Status
                                               select s).ToList();
            }
            familyStatusList.DataBind();
            familyStatusList.Items[0].Selected = true;

        }


        foreach (ListItem item in familyStatusList.Items)
        {
            item.Attributes.Add("statusid", item.Value);// = usersInRole.Find(u => u.UserName == item.Text).InRole;
        }


    }


    protected void filterDate_TextChanged(object sender, EventArgs e)
    {

    }

    #endregion
    
    #region Private Methods

    protected string BuildGroupList()
    {
        string familiesOptionList = string.Empty;
        List<PaceManager.Accommodation.Family> families = null;
        List<Xlk_ConfirmationAction> actions = null;
        DateTime _startDate = DateTime.Parse(startDate.Text);
        DateTime _endDate = DateTime.Parse(endDate.Text);

        try
        {
            StringBuilder sb = new StringBuilder();

            int[] statusList =  (from item in familyStatusList.Items.Cast<ListItem>() 
                               where item.Selected 
                               select int.Parse(item.Value)).ToArray();

            families = LoadFamilies(new int[] { 1, 5 },statusList);

            familiesOptionList = BuildFamilyDropDownOptions(families);

            using (GroupEntities entity = new GroupEntities())
            {
                actions = (from a in entity.Xlk_ConfirmationAction select a).ToList();


                var studentQuery = (from g in entity.Group
                                    where g.ArrivalDate >= _startDate && g.ArrivalDate <= _endDate
                                    orderby g.ArrivalDate
                                    select new { g.GroupId, g.GroupName, g.ArrivalDate, g.DepartureDate, g.NoOfLeaders, g.NoOfStudents, AssignedStudents = (from s in g.Student where s.Hostings.Count > 0 select s).Count(), Student = (from s in g.Student orderby s.Xlk_StudentType.StudentTypeId descending, s.FirstName, s.SurName select new { s.StudentId, s.FirstName, s.SurName, StudentType = s.Xlk_StudentType.Description, HostingDetails = (from h in s.Hostings select new { Hosting = h, h.HostingConfirmations, h.Family }).FirstOrDefault() }) });

                foreach (var group in studentQuery.ToList())
                {
                    sb.AppendFormat("<div><h2 id=\"{0}\">{1}</h2><div><table id=\"box-table-a\"><thead><tr><th>StudentId</th><th>Type</th><th>First Name</th><th>Sur Name</th><th>Family</th><th>Action</th></tr></thead><tbody>", group.GroupId, string.Format("<table class=\"navtdst\"><tr><td>{0}</td><td>Dates: {1} - {2}</td><td>Leaders: {3}</td><td>Student: {4}</td><td>Unassigned {5}</td></tr></table>", group.GroupName, group.ArrivalDate.ToString("dd/MMM/yyyy"), group.DepartureDate.ToString("dd/MMM/yyyy"), (group.NoOfLeaders.HasValue) ? group.NoOfLeaders.Value.ToString() : "0", (group.NoOfStudents.HasValue) ? group.NoOfStudents.Value.ToString() : "0", (group.NoOfLeaders + group.NoOfStudents) - group.AssignedStudents));

                    foreach (var student in group.Student)
                    {
                        sb.AppendFormat("<tr id=\"{0}\"><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>", student.StudentId, student.StudentType, student.FirstName, student.SurName, (student.HostingDetails == null) ? BuildFamilyDropDown(student.StudentId, familiesOptionList) : string.Format("<a class=\"DeleteHosting\" onClick=\"deleteHostingAction({0},{2})\"></a>{1}", student.StudentId, BaseEnrollmentPage.CreateName(student.HostingDetails.Family.FirstName, student.HostingDetails.Family.SurName), student.HostingDetails.Hosting.HostingId), (student.HostingDetails == null) ? string.Empty : BuildActions(student.StudentId, student.HostingDetails.Hosting.HostingId, actions, student.HostingDetails.HostingConfirmations.ToList()));
                    }

                    sb.Append("</table></div></div>");
                }
            }
            return sb.ToString();
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    private static List<PaceManager.Accommodation.Family> LoadFamilies(int[] familyTypeId, int[] statusId)
    {
        using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
        {
            var familyQuery = from f in entity.Family
                              where familyTypeId.Contains(f.Xlk_FamilyType.FamilyTypeId) && statusId.Contains(f.Xlk_Status.StatusId)
                              orderby f.SurName, f.FirstName
                              select f;

            return familyQuery.ToList();
        }

    }

    private static string BuildFamilyDropDown(int studentId, string families)
    {
        StringBuilder sb = new StringBuilder(string.Format("<select id=\"{0}\" onChange=\"assignFamily(this,{0})\">", studentId));
        sb.Append(families);
        sb.Append("</select>");
        return sb.ToString();
    }

    private static string BuildFamilyDropDownOptions(List<PaceManager.Accommodation.Family> families)
    {

        StringBuilder sb = new StringBuilder();
        sb.Append("<option \"selected\">Please Select</option>");

        foreach (PaceManager.Accommodation.Family family in families)
        {
            sb.AppendFormat("<option value={0}>{1} {2}</option>", family.FamilyId, family.SurName, family.FirstName);
        }
        return sb.ToString();

    }

    private static string BuildActions(int studentid,long hostingId, List<Xlk_ConfirmationAction> actions, List<HostingConfirmation> confirmations)
    {
        StringBuilder sb = new StringBuilder().AppendFormat("<span id=\"{0}actions\">", hostingId);

        foreach (var action in actions)
        {
            if ((from c in confirmations where c.ConfirmationActionId == action.ConfirmationActionId && c.ConfirmedDate.HasValue && c.ConfirmedBy.HasValue select c).Count() == 0)
                sb.AppendFormat("<a class=\"{3}\" title=\"{4}\" onClick=\"confirmHostingAction({0},{1},{2})\"></a>",studentid, hostingId, action.ConfirmationActionId,action.Description.Replace(" ", string.Empty),action.Description);
        }

        return sb.ToString();
    }

    protected static string BuildStudentRow(int studentId, int[] statuslist)
    {
        string familiesOptionList = string.Empty;
        List<PaceManager.Accommodation.Family> families = null;
        List<Xlk_ConfirmationAction> actions = null;



        families = LoadFamilies(new int[] { 1, 5 }, statuslist);

        familiesOptionList = BuildFamilyDropDownOptions(families);
        using (GroupEntities entity = new GroupEntities())
        {
            actions = (from a in entity.Xlk_ConfirmationAction select a).ToList();

            var student = (from s in entity.Student.Include("Hosting")
                           where s.StudentId == studentId
                           select new { s.StudentId, s.FirstName, s.SurName, StudentType = s.Xlk_StudentType.Description, HostingDetails = (from h in s.Hostings select new { Hosting = h, h.HostingConfirmations, h.Family }).FirstOrDefault() }).FirstOrDefault();


            return string.Format("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td>", student.StudentId, student.StudentType, student.FirstName, student.SurName, (student.HostingDetails == null) ? BuildFamilyDropDown(student.StudentId, familiesOptionList) : string.Format("<a class=\"DeleteHosting\" onClick=\"deleteHostingAction({0},{2})\"></a>{1}", student.StudentId, BaseEnrollmentPage.CreateName(student.HostingDetails.Family.FirstName, student.HostingDetails.Family.SurName), student.HostingDetails.Hosting.HostingId), (student.HostingDetails == null) ? string.Empty : BuildActions(studentId, student.HostingDetails.Hosting.HostingId, actions, student.HostingDetails.HostingConfirmations.ToList()));
        }

    }
    #endregion

    #region Javascript Enabled Methods


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AssignStudentToFamily(int familyid, int studentid, int[] statuslist)
    {
       
        try
        {
            using (Accommodation.AccomodationEntities entity = new Accommodation.AccomodationEntities())
            {
                PaceManager.Accommodation.Hosting currenthosting = (from h in entity.Hostings
                                                                    where h.Student.StudentId == studentid && h.ArrivalDate > DateTime.Now
                                                                    select h).FirstOrDefault();

                if (currenthosting != null)
                {
                    currenthosting.FamilyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Family", "FamilyId", familyid);
                    //set the confirmed date and details to null

                    if (entity.SaveChanges() > 0)
                        return BuildStudentRow(studentid, statuslist);
                }
                else
                { 
                    PaceManager.Accommodation.Hosting hosting = new PaceManager.Accommodation.Hosting();
                    using (GroupEntities gentity = new GroupEntities())
                    {
                        Group group = (from s in gentity.Student
                                       where s.StudentId == studentid
                                       select s.Group).FirstOrDefault();


                       
                        hosting.FamilyReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Family", "FamilyId", familyid);
                        hosting.StudentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Student", "StudentId", studentid);
                        hosting.ArrivalDate = group.ArrivalDate;
                        hosting.DepartureDate = group.DepartureDate;
                    }
                    entity.AddToHostings(hosting);

                    if (entity.SaveChanges() > 0)
                        return BuildStudentRow(studentid, statuslist);
                }
            }
           return string.Empty;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ConfirmHostingAction(int studentid,int[] statuslist,long hostingid, byte actionid)
    {
        try
        {
            using (GroupEntities entity = new GroupEntities())
            {
                List<HostingConfirmation> confirmations = (from c in entity.HostingConfirmation where c.HostingId == hostingid select c).ToList();

                HostingConfirmation current = (from h in confirmations where h.Xlk_ConfirmationAction.ConfirmationActionId == actionid select h).FirstOrDefault();

                if (current == null)
                {
                    current = HostingConfirmation.CreateHostingConfirmation(hostingid, actionid);
                    current.HostingReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Hostings", "HostingId", hostingid);
                    current.Xlk_ConfirmationActionReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Xlk_ConfirmationAction", "ConfirmationActionId", actionid);
                    entity.AddToHostingConfirmation(current);
                    if (entity.SaveChanges() > 0)
                        confirmations.Add(current);
                }

                current.ConfirmedDate = DateTime.Now;
                current.ConfirmedBy = 1; // we dont have security yet so defalut to 1

                if (entity.SaveChanges() > 0)
                {
                    //return BuildActions(hostingid, (from a in Entities.Xlk_ConfirmationAction select a).ToList(), confirmations);
                    return BuildStudentRow(studentid, statuslist);
                }
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteHosting(int studentid, int hostingid,int[] statuslist)
    {
        using (GroupEntities entity = new GroupEntities())
        {
            if (entity.DeleteHosting(hostingid).Count() == 0)
                return BuildStudentRow(studentid, statuslist);
        }
        return string.Empty;
    }


    #endregion
        
}
