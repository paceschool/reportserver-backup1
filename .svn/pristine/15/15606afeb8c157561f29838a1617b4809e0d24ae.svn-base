﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Tuition;
using System.Text;
using System.Data.Objects.DataClasses;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Objects;

public partial class Tuition_ClassPlanPage : BaseTuitionPage 
{

    private DateTime _modellingDate;

    #region Events    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {           
            _modellingDate = DateTime.Now.NextWeekDay(DayOfWeek.Monday);

            listprogrammetype.DataSource = BaseEnrollmentControl.LoadProgrammeTypes();
            listprogrammetype.DataBind();

            listcoursetype.DataSource = BaseEnrollmentControl.LoadCourseTypes();
            listcoursetype.DataBind();

            stuprogrammetype.DataSource = BaseEnrollmentControl.LoadProgrammeTypes();
            stuprogrammetype.DataBind();

            stucoursetype.DataSource = BaseEnrollmentControl.LoadCourseTypes();
            stucoursetype.DataBind();

            listbuilding.DataSource = LoadBuildings().ToList();
            listbuilding.DataBind();

            datepicker.Text = _modellingDate.ToString("dd/MM/yy");
            BuildClasses();
        }
    }

    protected void datepicker_TextChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    protected void listbuilding_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    protected void listcoursetype_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    protected void isClosed_CheckedChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }   
    protected void listprogrammetype_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
    #endregion

    #region Private Methods

    private void BuildClasses()
    {

        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        byte programmeTypeId = Convert.ToByte(listprogrammetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classQuery = from c in entity.Class
                             where (c.IsClosed == _isClosed) && (c.StartDate <= _modellingDate.Date && c.EndDate >= _modellingDate.Date) && (c.ClassRoom.Xlk_Building.BuildingId == buildingId) && (c.Xlk_CourseType.CourseTypeId == courseTypeId)
                             orderby c.TuitionLevel.MinPlancementScore
                             select new
                             {
                                 ClassId = c.ClassId,
                                 ClassRoom = c.ClassRoom.Label,
                                 Teacher = c.Teacher.Name,
                                 TuitionLevel = c.TuitionLevel.Description,
                                 CEFR = c.TuitionLevel.CEFR,
                                 ProgrammeTypeId = c.Xlk_ProgrammeType.ProgrammeTypeId,
                                 xx = c.Enrollment.Where(e => (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date)).Count(),
                                 Quantity = c.Enrollment.Where(e=>(e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date && c.ClassSchedule.Any(x => x.Class.ClassId ==  c.ClassId && x.StartDate <= _modellingDate.Date && !x.DateProcessed.HasValue))).Count()
                                 // || (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date && e.Class.ClassId == c.ClassId && (e.ClassSchedule.Count == 0 || !e.ClassSchedule.Any(x => x.StartDate <= _modellingDate.Date && !x.DateProcessed.HasValue)))).Count()
                             };

            if (programmeTypeId > 0)
                classQuery = classQuery.Where(x => x.ProgrammeTypeId == programmeTypeId);

            results.DataSource = classQuery;
            results.DataBind();
        }
    }

    protected string GetReportURL(string reportname)
    {
        if (string.IsNullOrEmpty(datepicker.Text))
            return "#";
        
        _modellingDate = Convert.ToDateTime(datepicker.Text);
        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        return ReportPath("TuitionReports", reportname, "HTML4.0", new Dictionary<string, object> { { "Date", _modellingDate.Date.ToString("dd/MMM/yyyy") }, { "CourseTypeId", courseTypeId }, { "BuildingId", buildingId } });
     }

    protected List<Enrollment> BuildLooseGroupStudents(DateTime modellingDate)
    {
        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        byte programmeTypeId = Convert.ToByte(listprogrammetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);
        using (TuitionEntities entity = new TuitionEntities())
        {
            List<byte> programmetypes = null;

            if (programmeTypeId > 0)
                programmetypes = new List<byte> { programmeTypeId };
            else
                programmetypes = (from e in entity.Xlk_ProgrammeType
                                  where e.Xlk_Building.Any(b => b.BuildingId == buildingId)
                                  select e.ProgrammeTypeId).ToList();

            if (programmetypes.Count > 0)
                return (from n in entity.Enrollment.Include("Student").Include("Student.Group")
                        where programmetypes.Contains(n.Xlk_ProgrammeType.ProgrammeTypeId) && (n.Student.Group.IsClosed == _isClosed) && (n.StartDate <= modellingDate && n.EndDate >= modellingDate) && n.Class.ClassId == null && n.ClassSchedule.Where(d => !d.DateProcessed.HasValue).Count() == 0 && n.Xlk_CourseType.CourseTypeId == courseTypeId && n.Student.StudentTypeId == 1
                        orderby n.Student.SurName descending
                        select n).ToList();
            else

                return (from n in entity.Enrollment.Include("Student").Include("Student.Group")
                        where (n.Student.Group.IsClosed == _isClosed) && (n.StartDate <= modellingDate && n.EndDate >= modellingDate) && n.Class.ClassId == null && n.ClassSchedule.Where(d => !d.DateProcessed.HasValue).Count() == 0 && n.Xlk_CourseType.CourseTypeId == courseTypeId && n.Student.StudentTypeId == 1
                        orderby n.Student.SurName descending
                        select n).ToList();
        }

    }

    protected List<Enrollment> BuildLooseStudents(DateTime modellingDate)
    {
        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        byte programmeTypeId = Convert.ToByte(listprogrammetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);


        using (TuitionEntities entity = new TuitionEntities())
        {
            List<byte> programmetypes = null;

            if (programmeTypeId > 0)
                programmetypes = new List<byte> { programmeTypeId };
            else programmetypes = (from e in entity.Xlk_ProgrammeType
                                   where e.Xlk_Building.Any(b => b.BuildingId == buildingId)
                                   select e.ProgrammeTypeId).ToList();

            if (programmetypes.Count > 0)
                return (
                    from n in entity.Enrollment.Include("Student")
                    where programmetypes.Contains(n.Xlk_ProgrammeType.ProgrammeTypeId) && (!n.Student.GroupId.HasValue) && (n.StartDate <= modellingDate && n.EndDate >= modellingDate) && n.Class.ClassId == null && n.ClassSchedule.Where(d => !d.DateProcessed.HasValue).Count() == 0 && n.Xlk_CourseType.CourseTypeId == courseTypeId && n.Student.StudentTypeId == 1
                    orderby n.Student.SurName descending
                    select n).ToList();
            else
                return (
                    from n in entity.Enrollment.Include("Student")
                    where (!n.Student.GroupId.HasValue) && (n.StartDate <= modellingDate && n.EndDate >= modellingDate) && n.Class.ClassId == null && n.ClassSchedule.Where(d=>!d.DateProcessed.HasValue).Count() == 0 && n.Xlk_CourseType.CourseTypeId == courseTypeId && n.Student.StudentTypeId == 1
                    orderby n.Student.SurName descending
                    select n).ToList();
        }
    }

    protected string BuildStudents(List<Enrollment> enrollments)
    {
        StringBuilder sb = new StringBuilder();
        if (enrollments != null && enrollments.Count > 0)
        {
            foreach (Enrollment enrollment in enrollments)
            {
                sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0} {2}% <i> - {3}</i></div>", CreateName(enrollment.Student.FirstName, enrollment.Student.SurName), enrollment.EnrollmentId, enrollment.Student.PlacementTestResult, enrollment.Student.GroupId != null ? enrollment.Student.Group.GroupName : string.Empty);
            }
        }
        return sb.ToString();
    }

    protected string BuildStudents(object classId)
    {
        int _classId = Convert.ToInt32(classId);
        using (TuitionEntities entity = new TuitionEntities())
        {
            List<Enrollment> enrollmentswithschedulelist = (
                from e in entity.Enrollment.Include("Student")
                where (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date) && e.ClassSchedule.Any(x => x.Class.ClassId == _classId && x.StartDate <= _modellingDate.Date && !x.DateProcessed.HasValue)
                select e)
                .Union(
                 from e in entity.Enrollment.Include("Student")
                 where (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date) && e.Class.ClassId == _classId && (e.ClassSchedule.Count == 0 || !e.ClassSchedule.Any(x => x.StartDate <= _modellingDate.Date && !x.DateProcessed.HasValue))
                 select e).OrderBy(s => s.Student.FirstName).ToList();

            StringBuilder sb = new StringBuilder();
            if (enrollmentswithschedulelist != null && enrollmentswithschedulelist.Count > 0)
            {
                foreach (Enrollment enrollment in enrollmentswithschedulelist)
                {
                    sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}</div>", CreateName(enrollment.Student.FirstName, enrollment.Student.SurName), enrollment.EnrollmentId);
                }
            }

            return sb.ToString();
        }
        
    }

    private void RebuildPage()
    {
        if (!string.IsNullOrEmpty(datepicker.Text))
        {
            _modellingDate = Convert.ToDateTime(datepicker.Text);

            BuildClasses();
        }
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AssignStudentToClassSchedule(int classid, long enrollmentid,DateTime assignmentdate) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
         try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                Enrollment enrollment = (from e in entity.Enrollment.Include("Class").Include("Student").Include("ClassSchedule")
                                         where e.EnrollmentId == enrollmentid
                                         select e).First();


                if (enrollment != null && classid == -1)
                {
                    var schedule = (from e in entity.ClassSchedule
                                    where e.EnrollmentId == enrollmentid && !e.DateProcessed.HasValue
                                    select e).First();

                    if (schedule != null)
                        entity.DeleteObject(schedule);
                    if (entity.SaveChanges() > 0)
                        return string.Empty;
                    else
                        return "Could not remove schedule";
                }

                if (enrollment.Class != null && enrollment.Class.ClassId == classid)
                    return "Could not find the class or the enrollment!";


                if (enrollment.ClassSchedule.Any(x => !x.DateProcessed.HasValue))
                {
                    List<ClassSchedule> schedules = enrollment.ClassSchedule.Where(x => !x.DateProcessed.HasValue).ToList();
                    foreach (ClassSchedule schedule in schedules)
                    {
                        entity.DeleteObject(schedule);
                    }
                    entity.SaveChanges();
                }

                ClassSchedule newSchedule = ClassSchedule.CreateClassSchedule(enrollmentid, assignmentdate.Date);
                newSchedule.ClassReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Class", "ClassId", classid);
                newSchedule.EnrollmentReference.EntityKey = new System.Data.EntityKey(entity.DefaultContainerName + ".Enrollment", "EnrollmentId", enrollmentid);
                entity.AddToClassSchedule(newSchedule);

                if (entity.SaveChanges() > 0)
                    return string.Empty;
                else
                    return "Could not save this move";
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string FindLooseStudents(DateTime modellingdate, byte programmetypeid, byte coursetypeid, bool isclosed) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        StringBuilder sb = new StringBuilder();

        try
        {

            using (TuitionEntities entity = new TuitionEntities())
            {


                var enrollments = (from n in entity.Enrollment.Include("Student").Include("Student.Group")
                                   where (n.StartDate <= modellingdate && n.EndDate >= modellingdate) && n.Class.ClassId == null && n.ClassSchedule.Where(d => !d.DateProcessed.HasValue).Count() == 0 && n.Student.StudentTypeId == 1
                                   select n);

                if (programmetypeid > 0)
                    enrollments = enrollments.Where(e=>e.Xlk_ProgrammeType.ProgrammeTypeId == programmetypeid);
               
                if (coursetypeid > 0)
                    enrollments = enrollments.Where(e=>e.Xlk_CourseType.CourseTypeId == coursetypeid);

                if (isclosed)
                    enrollments = enrollments.Where(e => e.Student.Group.IsClosed);

                if (enrollments != null && enrollments.ToList().Count > 0)
                {
                    foreach (Enrollment enrollment in enrollments)
                    {
                        sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0} {2}% <i> - {3}</i></div>", CreateName(enrollment.Student.FirstName, enrollment.Student.SurName), enrollment.EnrollmentId, enrollment.Student.PlacementTestResult, enrollment.Student.GroupId != null ? enrollment.Student.Group.GroupName : string.Empty);
                    }
                }
                return sb.ToString();
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    

    #endregion


}
