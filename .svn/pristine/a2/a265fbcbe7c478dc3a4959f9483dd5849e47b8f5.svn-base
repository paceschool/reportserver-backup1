﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Tuition;
using System.Data.Objects;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Text;
using Pace.Common;

/// <summary>
/// Summary description for BaseTuitionPage
/// </summary>
public partial class BaseTuitionPage : BasePage
{
    public TuitionEntities _entities { get; set; }
    
    public BaseTuitionPage()
	{
    }

    public static IList<Xlk_Building> LoadBuildings()
    {
        IList<Xlk_Building> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_Building>>("AllBuilding", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_Building>>("AllBuilding", GetBuildings().ToList());
    }

    private static IList<Xlk_Building> GetBuildings()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Xlk_Building> lookupQuery =
                from c in entity.Xlk_Building
                select c;
            return lookupQuery.ToList();
        }
    }

    public IList<ExamDates> LoadExamDates()
    {
        IList<ExamDates> values = null;

        if (CacheManager.Instance.GetFromCache<IList<ExamDates>>("AllExamDates", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<ExamDates>>("AllExamDates", GetExamDates());
    }

    public IList<Xlk_ExamType> LoadExamTypes()
    {
        IList<Xlk_ExamType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_ExamType>>("AllExamType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_ExamType>>("AllExamType", GetExamTypes());
    }

    private IList<Xlk_ExamType> GetExamTypes()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Xlk_ExamType> lookupQuery =
                from et in entity.Xlk_ExamType
                select et;
            return lookupQuery.ToList();
        }
    }

    public IList<Exam> LoadExams()
    {
        IList<Exam> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Exam>>("AllEnrollmentExmas", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Exam>>("AllEnrollmentExams", GetExams());
    }

    private IList<Exam> GetExams()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Exam> lookupQuery =
                from ex in entity.Exam
                select ex;
            return lookupQuery.ToList();
        }
    }
    private IList<ExamDates> GetExamDates()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<ExamDates> lookupQuery =
                from ed in entity.ExamDates
                orderby ed.ExamDate ascending
                select ed;
            return lookupQuery.ToList();
        }
    }

    #region Events

    public static TuitionEntities CreateEntity
    {
        get
        {
            return new TuitionEntities();
        }
    }

    public IList<TuitionLevel> LoadTuitionLevels()
    {
        IList<TuitionLevel> values = null;

        if (CacheManager.Instance.GetFromCache<IList<TuitionLevel>>("AllTuitionLevel", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<TuitionLevel>>("AllTuitionLevel", GetTuitionLevels());
    }

    private IList<TuitionLevel> GetTuitionLevels()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<TuitionLevel> lookupQuery =
                from l in entity.TuitionLevel
                select l;
            return lookupQuery.ToList();
        }
    }

    public IList<Xlk_ProgrammeType> LoadProgrammeTypes()
    {
        IList<Xlk_ProgrammeType> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Xlk_ProgrammeType>>("AllTuitionProgrammeType", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Xlk_ProgrammeType>>("AllTuitionProgrammeType", GetProgrammeTypes());
    }
    private IList<Xlk_ProgrammeType> GetProgrammeTypes()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Xlk_ProgrammeType> lookupQuery =
                from p in entity.Xlk_ProgrammeType
                select p;
            return lookupQuery.ToList();
        }
    }

    public IList<Teacher> LoadTeachers()
    {
        IList<Teacher> values = null;

        if (CacheManager.Instance.GetFromCache<IList<Teacher>>("AllTeacher", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<Teacher>>("AllTeacher", GetTeachers());
    }

    private IList<Teacher> GetTeachers()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Teacher> lookupQuery =
                from t in entity.Teacher
                where t.Active.Value == true
                orderby t.Name
                select t;
            return lookupQuery.ToList();
        }
    }

    public IList<ClassRoom> LoadClassRooms()
    {
        IList<ClassRoom> values = null;

        if (CacheManager.Instance.GetFromCache<IList<ClassRoom>>("AllClassRoom", out values))
            return values;

        return CacheManager.Instance.AddToCache<IList<ClassRoom>>("AllClassRoom", GetClassRooms());
    }

    private IList<ClassRoom> GetClassRooms()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<ClassRoom> lookupQuery =
                from c in entity.ClassRoom.Include("Xlk_Building")
                select c;
            return lookupQuery.ToList();
        }
    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopUpTuition(int id)
    {
        StringBuilder sb = new StringBuilder();

        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {


                var enrollment = (from e in entity.Enrollment
                                  where e.EnrollmentId == id
                                  select new { e.EnrollmentId, e.StartDate, e.EndDate, ProgrammeType = e.Xlk_ProgrammeType.Description, CourseType = e.Xlk_CourseType.Description,LessonBlock = e.Xlk_LessonBlock.Description,CurrentClass = e.Class.Label }).SingleOrDefault();


                sb.AppendFormat("<table class=\"popup-contents\"><tbody><tr><th>EnrollmentId</th><td>{0}</td></tr><tr><th>StartDate</th><td>{1}</td></tr><tr><th>EndDate</th><td>{2}</td></tr><tr><th>ProgrammeType</th><td>{3}</td></tr><tr><th>CourseType:</th><td>{4}</td></tr><tr><th>LessonBlock:</th><td>{5}</td></tr></tbody></table>", enrollment.EnrollmentId, enrollment.StartDate.ToString("dd/MMM/yyyy"), enrollment.EndDate.Value.ToString("dd/MMM/yyyy"), enrollment.ProgrammeType, enrollment.CourseType, enrollment.LessonBlock);

                var schedules = ((from s in entity.ClassSchedule
                                  where s.EnrollmentId == id
                                  select new { Label = s.Class.Label, Name = s.Class.Teacher.Name, StartDate = s.StartDate, ProcessedDate = s.DateProcessed }).Union(
                                 from t in entity.ClassTransfer
                                 join e in entity.Enrollment on t.FromClass.ClassId equals e.Class.ClassId
                                 where e.EnrollmentId == id && !t.ProcessedDate.HasValue
                                 select new { Label = t.ToClass.Label, Name = t.ToClass.Teacher.Name, StartDate = t.ActionDate, ProcessedDate = t.ProcessedDate })).OrderBy(y=>y.ProcessedDate).ToList();

                if (schedules != null && schedules.Count > 0)
                {
                    sb.Append("<table  id=\"box-table-a\"  class=\"popup-contents\"><thead><tr><th>Class</th><th>Teacher</th><th>For Date</th><th>Processed Date</th></tr></thead><tbody>");
                    
                    foreach (var schedule in schedules)
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", schedule.Label, schedule.Name, schedule.StartDate.ToString("dd/MMM/yy"), (schedule.ProcessedDate.HasValue) ? schedule.ProcessedDate.Value.ToString("dd/MMM/yy") : string.Empty);
                    }
                    sb.Append("</tbody></table>");
                }
            }

            return sb.ToString();
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string PopUpClass(int id)
    {
        StringBuilder sb = new StringBuilder();

        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                var enrollment = (from e in entity.Class
                                  where e.ClassId == id
                                  select new { e.ClassId,e.Label, Building=e.ClassRoom.Xlk_Building.Description ,ClassRoom = e.ClassRoom.Label, e.StartDate, e.Teacher.Name, e.EndDate, ProgrammeType = e.Xlk_ProgrammeType.Description, CourseType = e.Xlk_CourseType.Description, CurrentClass = e.Label }).SingleOrDefault();

                sb.AppendFormat("<table class=\"popup-contents\"><tbody><tr><th>Label:</th><td>{0}</td></tr><tr><th>StartDate</th><td>{1}</td></tr><tr><th>EndDate</th><td>{2}</td></tr><tr><th>ProgrammeType</th><td>{3}</td></tr><tr><th>CourseType:</th><td>{4}</td></tr><tr><th>Teacher:</th><td>{5}</td></tr><tr><th>Building</th><td>{6}</td></tr><tr><th>Room</th><td>{7}</td></tr></tbody></table>", enrollment.Label, enrollment.StartDate.ToString("dd/MMM/yyyy"), enrollment.EndDate.ToString("dd/MMM/yyyy"), enrollment.ProgrammeType, enrollment.CourseType, enrollment.Name,enrollment.Building,enrollment.ClassRoom);
            }

            return sb.ToString();
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LoadEditControl(string control, Dictionary<string, object> entitykeys)
    {
        try
        {
            var page = new BaseTuitionPage();

            BaseControl userControl = (BaseControl)page.LoadControl(control);
            foreach (var entitykey in entitykeys)
            {
                userControl.Parameters.Add(entitykey.Key, entitykey.Value);
            }


            page.Controls.Add(userControl);

            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }

    #endregion

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadPreTestStudents(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewExamsPage.aspx','Tuition/AddPreTestStudentControl.ascx',{{'ExamDateId': {0}}})\"><img src=\"../Content/img/actions/add.png\" />Add Pre Test Result</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">PreTest Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">Exam</th><th scope=\"col\">Exam Date</th><th scope=\"col\">Class Start</th><th scope=\"col\">Score</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from p in entity.PreTests
                                .Include("Student").Include("ExamDate").Include("ExamDate.Exam").Include("Student.StudentExam")
                            where p.ExamDate.StartDate >= DateTime.Now && p.ExamDateId == id && p.Student.StudentExam.Count == 0
                             select p;


                if (query.Count() > 0)
                {
                    foreach (PreTest test in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: left\">{8}</td><td style=\"text-align: left\">{9}</td><td style=\"text-align: center\"><a title=\"Edit Enrollment\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewExamsPage.aspx','Tuition/AddPreTestStudentControl.ascx',{{'PreTestId': {0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a href=\"#\" title=\"Enroll Student for Exam\" onclick=\"createExamBooking({0})\" ><img src=\"../Content/img/actions/next.png\"></a>&nbsp;<a title=\"Delete Exam Pretest\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewExamsPage.aspx','PreTests',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", test.PreTestId, test.Student.StudentId, test.ExamDateId, test.ExamDate.Exam.ExamId, test.Score, CreateName(test.Student.FirstName, test.Student.SurName), test.ExamDate.Exam.ExamName, test.ExamDate.ExamDate.Value.ToString("dd/MM/yyyy"), test.ExamDate.StartDate.Value.ToString("dd/MM/yyyy"), (test.Score.HasValue ? test.Score.Value.ToString() + "%" : string.Empty));
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"10\">No PreTests Found for this Class!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadPreTestByStudent(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewStudentPage.aspx','Enrollments/AddStudentResultControl.ascx',{{'ExamDateId': {0}}})\"><img src=\"../Content/img/actions/add.png\" />Add Test Result</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">PreTest Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">Exam</th><th scope=\"col\">Exam Date</th><th scope=\"col\">Class Start</th><th scope=\"col\">Score</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from p in entity.PreTests.Include("Student").Include("ExamDate").Include("ExamDate.Exam")
                            where p.Student.StudentId == id
                            orderby p.Score
                            select p;

                if (query.Count() > 0)
                {
                    foreach (PreTest test in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: left\">{8}</td><td style=\"text-align: left\">{9}</td><td style=\"text-align: center\"><a title=\"Edit Enrollment\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewExamsPage.aspx','Tuition/AddPreTestStudentControl.ascx',{{'PreTestId': {0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a href=\"#\" title=\"Move to Current Students\"><img src=\"../Content/img/actions/arrow_right.png\"></a></td></tr>", test.PreTestId, test.Student.StudentId, test.ExamDateId, test.ExamDate.Exam.ExamId, test.Score, CreateName(test.Student.FirstName, test.Student.SurName), test.ExamDate.Exam.ExamName, test.ExamDate.ExamDate.Value.ToString("dd/MM/yyyy"), test.ExamDate.StartDate.Value.ToString("dd/MM/yyyy"), (test.Score.HasValue ? test.Score.Value.ToString() + "%" : string.Empty));
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"10\">No Results Found for this Student!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadStudents(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewExamsPage.aspx','Tuition/AddExamEnrollmentControl.ascx',{{'ExamDateId': {0}}})\"><img src=\"../Content/img/actions/add.png\" />Add New Student</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Student Exam Id</th><th scope=\"col\">Student Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">Date Registered</th><th scope=\"col\">Paid</th><th scope=\"col\">Amount</th><th scope=\"col\">Date Paid</th><th scope=\"col\">As Individual</th><th scope=\"col\">Result Score</th><th scope=\"col\">Actions</th></tr></thead><tbody>", id));
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.StudentExam.Include("ExamDates").Include("Student")
                            where n.ExamDates.ExamDateId == id //&& n.Student.ArrivalDate < DateTime.Now && n.Student.DepartureDate > DateTime.Now
                            select n;

                if (query.Count() > 0)
                {
                    foreach (StudentExam exam in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td> <td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: left\">{7}</td><td style=\"text-align: left\">{8}</td><td style=\"text-align: left\"><a title=\"Edit Exam Enrollment\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewExamsPage.aspx','Tuition/AddExamEnrollmentControl.ascx',{{'StudentExamId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Exam Enrollment\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewExamsPage.aspx','StudentExam',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", exam.StudentExamId, exam.Student.StudentId, BasePage.StudentLink(exam.Student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(exam.Student.FirstName, exam.Student.SurName)), (exam.DateRegistered.HasValue) ? exam.DateRegistered.Value.ToString("D") : String.Empty, (exam.Paid.HasValue && exam.Paid.Value) ? "<b>Yes</b>" : String.Empty, exam.Amount.HasValue ? exam.Amount.Value.ToString("€0.00") : "", (exam.DatePaid.HasValue) ? exam.DatePaid.Value.ToString("D") : String.Empty, (exam.AsIndividual.HasValue && exam.AsIndividual.Value) ? "<b>Yes</b>" : String.Empty, exam.ResultScore);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"10\">No Students Found for this Exam!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadPreviousStudents(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder(string.Format("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Student Exam Id</th><th scope=\"col\">Student Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">As Individual</th><th scope=\"col\">Result</th></tr></thead><tbody>", id));
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.StudentExam.Include("ExamDates").Include("Student")
                            where n.ExamDates.ExamDateId == id && n.Student.DepartureDate < DateTime.Now
                            select n;

                if (query.Count() > 0)
                {
                    foreach (StudentExam exam in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\">{1}</td> <td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td> <td style=\"text-align: left\">{4}</td></tr>", exam.StudentExamId, exam.Student.StudentId, CreateName(exam.Student.FirstName, exam.Student.SurName), (exam.AsIndividual.Value == true) ? "<b>Yes</b>" : String.Empty, exam.ResultScore);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"5\">No Students Found for this Exam!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse RegisterStudentForExam(int pretestid)
    {

        StudentExam newExam = new StudentExam();

        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {

                PreTest test = entity.PreTests.Include("Student").Include("ExamDate").Single(t => t.PreTestId == pretestid);

                newExam.StudentReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Student", test.Student); ;
                newExam.DateRegistered = DateTime.Now;
                newExam.ExamDatesReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".ExamDates", test.ExamDate);
                newExam.AsIndividual = false;
                newExam.Amount = 0;
                newExam.Paid = test.Student.PrepaidExam;



                entity.AddToStudentExam(newExam);

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();

            }
            return new AjaxResponse(new Exception( "There was a problem"));
        }
        catch (Exception ex)
        {
            return  new AjaxResponse(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveStudentPreTest(PreTest newObject)
    {
        PreTest newtest = new PreTest();

        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                if (newObject.PreTestId > 0)
                    newtest = (from t in entity.PreTests where t.PreTestId == newObject.PreTestId select t).FirstOrDefault();

                newtest.StudentReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Student", newObject.Student);
                newtest.ExamDateId = newObject.ExamDateId;
                newtest.Score = newObject.Score;

                if (newtest.PreTestId == 0)
                    entity.AddToPreTests(newtest);

                entity.SaveChanges();
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveScheduleClass(ScheduleClass newObject)
    {
        ScheduleClass _newschedule = new ScheduleClass();
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                if (newObject.ScheduleId > 0)
                    _newschedule = (from e in entity.ScheduleClasses where e.ScheduleId == newObject.ScheduleId select e).FirstOrDefault();
                else
                    entity.AddToScheduleClasses(_newschedule);

                _newschedule.BreakMinutes = newObject.BreakMinutes;
                _newschedule.DailyEndTime = newObject.DailyEndTime;
                _newschedule.DailyStartTime = newObject.DailyStartTime;
                _newschedule.EndDate = newObject.EndDate;
                _newschedule.StartDate = newObject.StartDate;
                _newschedule.TuitionMinutes = (int)(newObject.DailyEndTime - newObject.DailyStartTime).TotalMinutes;


                _newschedule.ClassReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Class", newObject.Class);


                entity.SaveChanges();

            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveClass(Class newObject)
    {
        Class _newclass = new Class();
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                if (newObject.ClassId > 0)
                    _newclass = (from e in entity.Class where e.ClassId == newObject.ClassId select e).FirstOrDefault();
                else
                    entity.AddToClass(_newclass);

                _newclass.Label = newObject.Label;
                _newclass.StartDate = newObject.StartDate;
                _newclass.EndDate = newObject.EndDate;
                _newclass.DailyStartTime = newObject.DailyStartTime;
                _newclass.DailyEndTime = newObject.DailyEndTime;
                _newclass.IsClosed = newObject.IsClosed;

                _newclass.TeacherReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Teacher", newObject.Teacher);

                _newclass.ClassRoomReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".ClassRoom", newObject.ClassRoom);
                _newclass.TuitionLevelReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".TuitionLevel", newObject.TuitionLevel);
                _newclass.Xlk_ProgrammeTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_ProgrammeType", newObject.Xlk_ProgrammeType);
                _newclass.Xlk_CourseTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_CourseType", newObject.Xlk_CourseType);


                if (newObject.ClassId == 0)
                {
                    if (_newclass.ClassId > 0)
                        return new AjaxResponse(string.Format("{0}?ClassId={1}", VirtualPathUtility.ToAbsolute("~/Tuition/ViewClassPage.aspx"), _newclass.ClassId));
                    else
                        return new AjaxResponse(new Exception("Could not find the new group Identifier, please search for this group before trying to add again!"));
                }


                entity.SaveChanges();
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveExamDate(ExamDates newObject)
    {
        ExamDates newExamDate = new ExamDates();
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                if (newObject.ExamDateId > 0)
                    newExamDate = (from ex in entity.ExamDates.Include("Xlk_ExamType") where ex.ExamDateId == newObject.ExamDateId select ex).FirstOrDefault();

                if (newObject.EndDate.HasValue)
                    newExamDate.EndDate = Convert.ToDateTime(newObject.EndDate.Value);

                if (newObject.ExamDate.HasValue)
                    newExamDate.ExamDate = Convert.ToDateTime(newObject.ExamDate.Value);

                if (newObject.ExpecetedStudentDepartureDate.HasValue)
                    newExamDate.ExpecetedStudentDepartureDate = Convert.ToDateTime(newObject.ExpecetedStudentDepartureDate.Value);

                if (newObject.RegistrationDate.HasValue)
                    newExamDate.RegistrationDate = Convert.ToDateTime(newObject.RegistrationDate.Value);

                if (newObject.StartDate.HasValue)
                    newExamDate.StartDate = Convert.ToDateTime(newObject.StartDate.Value);

                newExamDate.ExamReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Exam", newObject.Exam);
                newExamDate.Xlk_ExamTypeReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Xlk_ExamType", newObject.Xlk_ExamType);
                newExamDate.ExamDateConfirmed = newObject.ExamDateConfirmed;

                if (newExamDate.ExamDateId == 0)
                    entity.AddToExamDates(newExamDate);

                entity.SaveChanges();
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveExamEnrollment(StudentExam newObject)
    {
        StudentExam newexam = new StudentExam();
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                if (newObject.StudentExamId > 0)
                    newexam = (from e in entity.StudentExam where e.StudentExamId == newObject.StudentExamId select e).FirstOrDefault();


                newexam.StudentReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".Student", newObject.Student);
                newexam.ExamDatesReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".ExamDates", newObject.ExamDates);
                if (newObject.DateRegistered.HasValue)
                    newexam.DateRegistered = Convert.ToDateTime(newObject.DateRegistered.Value);

                if (newObject.Paid != null)
                    newexam.Paid = true;
                else
                    newexam.Paid = false;

                if (newObject.Amount.HasValue)
                    newexam.Amount = Convert.ToDecimal(newObject.Amount.Value);

                if (newObject.AsIndividual != null)
                    newexam.AsIndividual = true;
                else
                    newexam.AsIndividual = false;

                newexam.ResultScore = newObject.ResultScore;

                if (newexam.StudentExamId == 0)
                    entity.AddToStudentExam(newexam);

                entity.SaveChanges();
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }
}
