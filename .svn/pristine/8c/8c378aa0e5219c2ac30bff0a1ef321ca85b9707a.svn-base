﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Enrollment;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;



public partial class HomePage : BasePage
{
    protected string pageName = "Home Page";

    protected void Page_Load(object sender, EventArgs e)
    {
        //SupportService service = new SupportService();
        //service.SendTaskEmails();
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected DateTime FirstDayOfWeek(DateTime date, CalendarWeekRule rule)
    {

        int weeknumber = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, rule, DayOfWeek.Monday);

        DateTime jan1 = new DateTime(date.Year, 1, 1);

        int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        DateTime firstMonday = jan1.AddDays(daysOffset);

        var cal = CultureInfo.CurrentCulture.Calendar;
        int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Monday);

        if (firstWeek <= 1)
        {
            weeknumber -= 1;
        }

        DateTime result = firstMonday.AddDays(weeknumber * 7);

        return result;
    }

    private static IEnumerable<object> ShowStudentArrivalEvents(DateTime start, DateTime end)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var studentQuery = from s in entity.Enrollment.Include("Student")
                               where s.Student.CampusId == GetCampusId && ((s.Student.ArrivalDate >= start && s.Student.ArrivalDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                               select new { Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode, ArrivalDate = s.Student.ArrivalDate, DepartureDate = s.Student.DepartureDate, Id = s.Student.StudentId };

            foreach (var item in studentQuery.ToList())
            {
                yield return new { Title = item.Title, ArrivalDate = item.ArrivalDate.ToLongDateString(), URL = string.Format("{0}?StudentId={1}", VirtualPathUtility.ToAbsolute("~/Enrollments/ViewStudentPage.aspx"), item.Id),Rel = string.Format("{0},Enrollments/ViewStudentPage.aspx/PopupStudent", item.Id)};
            }
        }
    }

    private static IEnumerable<object> ShowStudentDepartureEvents(DateTime start, DateTime end)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var studentQuery = from s in entity.Enrollment
                               where s.Student.CampusId == GetCampusId &&  ((s.Student.DepartureDate >= start && s.Student.DepartureDate <= end)) && ((s.EndDate >= start && s.EndDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                               select new { Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode + (s.Student.PrepaidBook ? "-Book Return!" : ""), ArrivalDate = s.Student.ArrivalDate, DepartureDate = s.Student.DepartureDate, Id = s.Student.StudentId };

            foreach (var item in studentQuery.ToList())
            {
                yield return new { Title = item.Title, ArrivalDate = item.DepartureDate.Value.ToLongDateString(), URL = string.Format("{0}?StudentId={1}", VirtualPathUtility.ToAbsolute("~/Enrollments/ViewStudentPage.aspx"), item.Id), Rel = string.Format("{0},Enrollments/ViewStudentPage.aspx/PopupStudent", item.Id) };
            }
        }
    }

    private static IEnumerable<object> ShowStudentEvents(DateTime start, DateTime end)
    {
        using (EnrollmentsEntities entity = new EnrollmentsEntities())
        {
            var studentQuery = (from s in entity.Enrollment
                                where s.Student.CampusId == GetCampusId && ((s.Student.DepartureDate >= start && s.Student.DepartureDate <= end)) && ((s.EndDate >= start && s.EndDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                                select new { ClassName = "studentdeparture", Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode + (s.Student.PrepaidBook ? "-Book Return!" : ""), Date = s.Student.DepartureDate.Value, Id = s.Student.StudentId }).Union(
                               from s in entity.Enrollment.Include("Student")
                               where s.Student.CampusId == GetCampusId && ((s.Student.ArrivalDate >= start && s.Student.ArrivalDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                               select new { ClassName = "studentarrival", Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode, Date = s.Student.ArrivalDate, Id = s.Student.StudentId });

            foreach (var item in studentQuery.ToList())
            {
                yield return new { Title = item.Title,ClassName=item.ClassName, ArrivalDate = item.Date.ToLongDateString(), URL = string.Format("{0}?StudentId={1}", VirtualPathUtility.ToAbsolute("~/Enrollments/ViewStudentPage.aspx"), item.Id), Rel = string.Format("{0},Enrollments/ViewStudentPage.aspx/PopupStudent", item.Id) };
            }
        }
    }

    private static IEnumerable<object> ShowGroupEvents(DateTime start, DateTime end)
    {
        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            var studentQuery = from g in entity.Group
                               where g.CampusId == GetCampusId &&  g.Xlk_Status.StatusId != 3 && ((g.ArrivalDate >= start && g.ArrivalDate <= end) || (g.ArrivalDate < start && g.DepartureDate >= end) || (g.DepartureDate >= start && g.DepartureDate <= end))
                               select new { Title = g.GroupName, ArrivalDate = g.ArrivalDate, DepartureDate = g.DepartureDate, Id = g.GroupId };

            foreach (var item in studentQuery.ToList())
            {
                yield return new { Title = item.Title, ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString(), URL = string.Format("{0}?GroupId={1}", VirtualPathUtility.ToAbsolute("~/Groups/ViewGroupPage.aspx"), item.Id), Rel = string.Format("{0},Groups/ViewGroupPage.aspx/PopupGroup", item.Id) };
            }
        }
    }

    private static IEnumerable<object> ShowTaskEvents(DateTime start, DateTime end)
    {
        int _currentUserId = GetCurrentUser().UserId;
        using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
        {
            var taskQuery = from g in entity.Task
                            where ((g.AssignedToUser.UserId == _currentUserId) && (g.DateDue >= start && g.DateDue <= end && g.Xlk_Status.StatusId != 3))
                            select new { ClassName = g.Xlk_Priority.Description.ToLower(), Title = g.Title, ArrivalDate = g.DateDue, DepartureDate = g.DateDue, Id = g.TaskId };

            foreach (var item in taskQuery.ToList())
            {
                yield return new { Title = string.Format("{0}", item.Title), ClassName = String.Format("task_priority_{0}", item.ClassName), ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString(), URL = string.Format("{0}?TaskId={1}", VirtualPathUtility.ToAbsolute("~/Support/ViewTaskPage.aspx"), item.Id), Rel = string.Format("{0},Support/ViewTaskPage.aspx/PopupTask", item.Id) };
            }
        }
    }

    private static IEnumerable<object> ShowExcursionEvents(DateTime start, DateTime end)
    {

        using (Pace.DataAccess.Excursion.ExcursionEntities entity = new Pace.DataAccess.Excursion.ExcursionEntities())
        {
            var taskQuery = from g in entity.Bookings
                            where g.CampusId == GetCampusId && ((g.ExcursionDate >= start && g.ExcursionDate <= end))
                            select new { Title = g.Title, ArrivalDate = g.ExcursionDate, DepartureDate = g.ExcursionDate, Id = g.BookingId };

            foreach (var item in taskQuery.ToList())
            {
                yield return new { Title = string.Format("{0}", item.Title), ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString(), URL = string.Format("{0}?BookingId={1}", VirtualPathUtility.ToAbsolute("~/Excursions/ViewBookingPage.aspx"), item.Id), Rel = string.Format("{0},Excursions/ViewBookingPage.aspx/PopupBooking", item.Id) };
            }
        }
    }

    private static IEnumerable<object> ShowTransportEvents(DateTime start, DateTime end)
    {

        using (Pace.DataAccess.Transport.TransportEntities entity = new Pace.DataAccess.Transport.TransportEntities())
        {
            var taskQuery = from g in entity.VehicleBookings
                            where ((g.Booking.RequestedDate >= start && g.Booking.RequestedDate <= end ))
                            select new { Title = g.Booking.Title,Supplier = g.VehiclePricePlan.Vehicle.TransportSupplier.Supplier.Title, ArrivalDate = g.Booking.RequestedDate, DepartureDate = g.Booking.RequestedDate, VehicleId = g.VehicleBookingId, BookId = g.Booking.BookingId };

            foreach (var item in taskQuery.ToList())
            {
                yield return new { Title = string.Format("{0}:{1}", item.Supplier, item.Title), ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString(), URL = string.Format("{0}?BookingId={1}", VirtualPathUtility.ToAbsolute("~/Transport/ViewBookingPage.aspx"), item.BookId), Rel = string.Format("{0},Transport/ViewBookingPage.aspx/PopupVehicleBooking", item.BookId) };
            }
        }
    }

    private static IEnumerable<object> ShowClassEvents(DateTime start, DateTime end)
    {

        using (Pace.DataAccess.Tuition.TuitionEntities entity = new Pace.DataAccess.Tuition.TuitionEntities())
        {
            var taskQuery = from g in entity.Class
                            where ((g.StartDate >= start && g.StartDate <= end) || (g.StartDate < start && g.EndDate >= end) || (g.EndDate >= start && g.EndDate <= end)) && g.ClassRoom.Xlk_Building.CampusId == GetCampusId
                            select new { Title = g.Label,Teacher=g.Teacher.Name,Classroom= g.ClassRoom.Label, g.IsClosed , Course = g.Xlk_CourseType.Description,Building = g.ClassRoom.Xlk_Building.Title, ArrivalDate = g.StartDate, DepartureDate = g.EndDate, Id = g.ClassId };

            foreach (var item in taskQuery.ToList())
            {
                yield return new { Title = string.Format("{0}:{1} - {2} - {3} - {4}", item.Building, item.Classroom, item.Title, item.Course,item.Teacher), ClassName = (item.IsClosed) ? "closedclass" : "openclass", ArrivalDate = item.ArrivalDate.ToLongDateString(), DepartureDate = item.DepartureDate.ToLongDateString(), URL = string.Format("{0}?ClassId={1}", VirtualPathUtility.ToAbsolute("~/Tuition/ViewClassPage.aspx"), item.Id), Rel = string.Format("{0},Tuition/ViewClassPage.aspx/PopupClass", item.Id)};
            }
        }
    }

    private static IEnumerable<object> ShowAcademicEvents(DateTime start, DateTime end)
    {

        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var studentQuery = (from s in entity.Enrollment
                                where s.Student.CampusId == GetCampusId && ((s.Student.DepartureDate >= start && s.Student.DepartureDate <= end)) && ((s.EndDate >= start && s.EndDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                                select new { ClassName = "studentdeparture", Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode + (s.Student.PrepaidBook ? "-Book Return!" : ""), Date = s.EndDate.Value, Id = s.Student.StudentId }).Union(
                               from s in entity.Enrollment.Include("Student")
                               where s.Student.CampusId == GetCampusId && ((s.StartDate >= start && s.StartDate <= end)) && s.Student.GroupId == null && s.Student.Xlk_Status.StatusId != 5
                               select new { ClassName = "studentarrival", Title = s.Student.FirstName + " " + s.Student.SurName + " - " + s.Xlk_ProgrammeType.ShortCode, Date = s.StartDate, Id = s.Student.StudentId });

            foreach (var item in studentQuery.ToList())
            {
                yield return new { Title = item.Title, ClassName = item.ClassName, ArrivalDate = item.Date.ToLongDateString(), URL = string.Format("{0}?StudentId={1}", VirtualPathUtility.ToAbsolute("~/Enrollments/ViewStudentPage.aspx"), item.Id), Rel = string.Format("{0},Enrollments/ViewStudentPage.aspx/PopupStudent", item.Id) };
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static IEnumerable<object> ListCalendarEvents(DateTime start, DateTime end, string action)
    {
        switch (action.ToLower())
        {
            case "students":
                return ShowStudentEvents(start, end);
            case "group":
                return ShowGroupEvents(start, end);
            case "tasks":
                return ShowTaskEvents(start, end);
            case "excursion":
                return ShowExcursionEvents(start, end);
            case "transport":
                return ShowTransportEvents(start, end);
            case "classes":
                return ShowClassEvents(start, end);
            case "academic":
                return ShowAcademicEvents(start, end);
            default:
                return null;
        }
    }
}