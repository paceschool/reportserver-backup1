﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using Pace.DataAccess.Agent;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Agent_ViewAgentPage : BaseAgentPage
{
    protected Int32 _agentId;

    #region Event Methods

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["AgencyId"]))
            _agentId = Convert.ToInt32(Request["AgencyId"]);

        if (!Page.IsPostBack)
        {
            DisplayAgent(LoadAgent(new AgentEntities(), _agentId));
            LoadStudentsAndGroups();
        }
    }


    #endregion

    #region Private Methods

    private void LoadStudentsAndGroups()
    {
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            IQueryable<Pace.DataAccess.Enrollment.Student> studentQuery = from s in entity.Student.Include("Enrollment")
                                                                          where s.Agency.AgencyId == _agentId && !s.GroupId.HasValue
                                                                          select s;

            currentStudents.DataSource = studentQuery.AsQueryable().Where(s => s.DepartureDate >= DateTime.Now);
            currentStudents.DataBind();

            pastStudents.DataSource = studentQuery.AsQueryable().Where(s => s.DepartureDate <= DateTime.Now);
            pastStudents.DataBind();
        }
    }

    private Agency LoadAgent(AgentEntities entity, Int32 agentId) //Load Student if passed from Enrollment page
    {
        IQueryable<Agency> agentQuery = from s in entity.Agencies.Include("Xlk_Nationality")
                                        where s.AgencyId == agentId
                                        select s;
        if (agentQuery.ToList().Count() > 0)
            return agentQuery.ToList().First();

        return null;
    }

    private void DisplayAgent(Agency agent) //Display loaded Student details
    {
        if (agent != null)
        {
            curAddress.Text = agent.Address;
            curAgencyId.Text = agent.AgencyId.ToString();
            curContactName.Text = agent.ContactName;
            curEmail.Text = agent.Email;
            curIsCommissionable.Text = (agent.IsCommissionable.HasValue && agent.IsCommissionable.Value) ? "Yes" : "No";
            curName.Text = agent.Name;
            curSageRef.Text = agent.SageRef;
            curNationality.Text = agent.Xlk_Nationality.Description;
            curMonth.Text = (agent.MonthToDateTurnover.HasValue) ? agent.MonthToDateTurnover.Value.ToString("c"): "&eur;0.00";
            curYear.Text = (agent.YearToDateTurnover.HasValue) ? agent.YearToDateTurnover.Value.ToString("c") : "&eur;0.00";
            curPreviousYear.Text = (agent.PriorYearTurnover.HasValue) ? agent.PriorYearTurnover.Value.ToString("c") : "&eur;0.00";
        }
    }

    private void SaveAgent(AgentEntities entity, Agency agent) //Final Save and Redirect to Agent page
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayAgent(LoadAgent(entity, _agentId));
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);  
        }

    }

    #endregion


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadRateCards(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div id=\"navarea\"><ul id=\"pricesbrowser\" class=\"filetree treeview-famfamfam\">");

            using (AgentEntities entity = new AgentEntities())
            {
                var itemQuery = (from f in entity.OrderableItems
                                 group f by f.Xlk_ProductType.Description into Group_items
                                 select Group_items).ToList();

                foreach (var key in itemQuery)
                {
                    sb.AppendFormat("<li><span class=\"folder\">{0}</span><ul>", string.IsNullOrEmpty(key.Key) ? "UnClassified" : key.Key);
                    foreach (var item in key)
                    {
                        sb.AppendFormat("<li><span class=\"folder\">{0}</span><ul>", item.Description);
                        foreach (var titem in item.RateCards)
                        {
                            //sb.AppendFormat("<li><span class=\"folder\">Prices</span><ul>", titem.DateActiveFrom.ToString("dd/MM/yyyy"), titem.DateActiveTo.ToString("dd/MM/yyyy"));
                            foreach (var ritem in titem.RateCardCostings)
                            {

                                sb.AppendFormat("<li><span class=\"file\"><b>€{0} per {1}.</b>  <i>Valid from {2} to {3}</i></span></li>", ritem.Cost.ToString(), ritem.Xlk_Unit.Description, titem.DateActiveFrom.ToString("dd/MM/yy"), titem.DateActiveTo.ToString("dd/MM/yy"));

                                sb.AppendFormat("<li><span class=\"file\"><b>€{0} per {1}.</b>  <i>Valid from {2} to {3}</i></span></li>", ritem.Cost.ToString("00.00"), ritem.Xlk_Unit.Description, titem.DateActiveFrom.ToString("dd/MM/yy"), titem.DateActiveTo.ToString("dd/MM/yy"));

                            }
                           // sb.AppendFormat("</ul></li>");
                        }
                        sb.AppendFormat("</ul><//li>");
                    }
                    sb.AppendFormat("</ul></li>");
                }
                sb.Append("</ul></div>");
            }
            return sb.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
