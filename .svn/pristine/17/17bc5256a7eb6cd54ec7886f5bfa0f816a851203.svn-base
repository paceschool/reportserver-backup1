﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using PaceManager.Enrollment;

[PartialCaching(3600, "none", "none", "none",true)]
public partial class NavigationControl : BaseControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected string ReportPath(string ReportName, string format,Dictionary<string, object> _params)
    {
        return string.Format("{0}?/PaceManagerReports/{1}{2}&rs:Command=Render&rs:Format={3}&rc:Parameters=False", BaseControl.ReportServerURL, ReportName, _params.ToQueryString(), format);

    }

    protected string ReportPath(string ReportName, Dictionary<string,object> _params)
    {
      return string.Format("{0}?/PaceManagerReports/{1}{2}&rs:Command=Render&rs:Format=pdf&rc:Parameters=False", BaseControl.ReportServerURL, ReportName, _params.ToQueryString());
        
    }

    protected List<PaceManager.Group.Group> GetNextGroups(int NoOfGroups)
    {
        DateTime _now = DateTime.Now.Date;
        using (PaceManager.Group.GroupEntities entity = new PaceManager.Group.GroupEntities())
        {
            var GroupSearchQuery = (from g in entity.Group
                                    where g.ArrivalDate >= _now
                                    orderby g.ArrivalDate ascending
                                    select g).ToList().Take(NoOfGroups);

            return GroupSearchQuery.ToList();
        }
    }

    protected Dictionary<Xlk_ProgrammeType, List<Student>> Click_Arriving_ThisWeek()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime endarrivingDate = startarrivingDate.AddDays(5);
        using (PaceManager.Enrollment.EnrollmentsEntities entity = new PaceManager.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = (from f in entity.Student
                                      from g in f.Enrollment
                                      where f.GroupId == null && ((g.StartDate >= startarrivingDate && g.StartDate <= endarrivingDate))
                                      orderby f.ArrivalDate, f.FirstName ascending, f.SurName ascending
                                      group f by g.Xlk_ProgrammeType into proggroup
                                      select proggroup).ToDictionary(gdc => gdc.Key, gdc => gdc.Distinct().ToList());
            return StudentSearchQuery;
        }
    }

    protected Dictionary<Xlk_ProgrammeType, List<Student>> Click_Arriving_NextWeek()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek).AddDays(7);
        DateTime endarrivingDate = startarrivingDate.AddDays(5);
        using (PaceManager.Enrollment.EnrollmentsEntities entity = new PaceManager.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = (from f in entity.Student
                                      from g in f.Enrollment
                                      where f.GroupId == null && ((g.StartDate >= startarrivingDate && g.StartDate <= endarrivingDate))
                                      orderby f.ArrivalDate, f.FirstName ascending, f.SurName ascending
                                      group f by g.Xlk_ProgrammeType into proggroup
                                      select proggroup).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

            return StudentSearchQuery;
        }
    }

    protected Dictionary<Xlk_ProgrammeType,List<Student>> Click_Departing()
    {
        DateTime startdepartingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime enddepartingDate = startdepartingDate.AddDays(7);
        using (PaceManager.Enrollment.EnrollmentsEntities entity = new PaceManager.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = (from f in entity.Student
                                     from g in f.Enrollment
                                     where f.GroupId == null && (g.EndDate >= startdepartingDate && g.EndDate <= enddepartingDate)
                                     orderby f.DepartureDate, f.FirstName ascending, f.SurName ascending
                                     group f by g.Xlk_ProgrammeType into proggroup
                                     select proggroup).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());
            return StudentSearchQuery;
        }
    }

    protected List<PaceManager.Enrollment.Student> Click_Cancelled()
    {
        using (PaceManager.Enrollment.EnrollmentsEntities entity = new PaceManager.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student
                                     where f.Xlk_Status.StatusId == 5
                                     select f;
            return StudentSearchQuery.ToList();
        }
    }

    protected List<PaceManager.Group.Group> Click_GroupDeparting()
    {
        DateTime startdepartingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime enddepartingDate = startdepartingDate.AddDays(7);
        using (PaceManager.Group.GroupEntities entity = new PaceManager.Group.GroupEntities())
        {
            var GroupSearchQuery = (from f in entity.Group
                                    from g in f.Enrollment
                                    where f.GroupId != null && (g.EndDate >= startdepartingDate && g.EndDate <= enddepartingDate)
                                    orderby f.DepartureDate, f.GroupName ascending
                                    select f).Distinct();
            return GroupSearchQuery.ToList();
        }
    }

    protected List<PaceManager.Contacts.Xlk_Category> ContactCategories()
    {
        {
            using (PaceManager.Contacts.ContactsEntities entity = new PaceManager.Contacts.ContactsEntities())
            {
                IQueryable<PaceManager.Contacts.Xlk_Category> lookupQuery =
                     from p in entity.Xlk_Category
                     select p;
                return lookupQuery.ToList();
            }
        }
    }
    protected List<PaceManager.Documents.Xlk_Category> DocumentCategories()
    {
        {
            using (PaceManager.Documents.DocumentsEntities entity = new PaceManager.Documents.DocumentsEntities())
            {
                IQueryable<PaceManager.Documents.Xlk_Category> lookupQuery =
                     from p in entity.Xlk_Category
                     select p;
                return lookupQuery.ToList();
            }
        }
    }
}
