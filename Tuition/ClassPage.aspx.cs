﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Tuition;

using System.Data;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;

public partial class Tuition_ClassPage : BaseTuitionPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            listclasses.DataSource = this.LoadTuitionLevels();
            listclasses.DataBind();
            listprogrammetype.DataSource = this.LoadProgrammeTypes();
            listprogrammetype.DataBind();
            listprogrammetype.DataSource = this.LoadProgrammeTypes();
            listprogrammetype.DataBind();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "search":
                Click_SearchClasses();
                break;
            case "current":
                Click_LoadCurrentClasses();
                break;
            case "future":
                Click_Future();
                break;
            case "previous":
                Click_Previous();
                break;
            default:
                Click_LoadAllClasses();
                break;
        }
    }
    protected void Click_SearchClasses()
    {

        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<Class> classSearchQuery = from c in entity.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                                                 where c.ClassRoom.Xlk_Building.CampusId == GetCampusId
                                                 select c;

            int classId = Convert.ToInt32(listclasses.SelectedItem.Value);
            int programmeTypeId = Convert.ToInt32(listprogrammetype.SelectedItem.Value);

            if (!string.IsNullOrEmpty(lookupString.Text) || classId > 0 || programmeTypeId > 0)
            {
                if (!string.IsNullOrEmpty(lookupString.Text))
                    classSearchQuery = classSearchQuery.Where(s => s.Teacher.Name.Contains(lookupString.Text) || s.ClassRoom.Label.Contains(lookupString.Text) || (s.TuitionLevel.Description.Contains(lookupString.Text) || s.TuitionLevel.CEFR.Contains(lookupString.Text) || s.TuitionLevel.Label.Contains(lookupString.Text)));

                if (classId > 0)
                    classSearchQuery = classSearchQuery.Where(s => s.ClassId == classId);

                if (programmeTypeId > 0)
                    classSearchQuery = classSearchQuery.Where(s => s.Xlk_ProgrammeType.ProgrammeTypeId == programmeTypeId);
            }

            LoadResults(classSearchQuery.ToList());
        }
    }

    protected void Click_LoadCurrentClasses()
    {
        DateTime _checkDate = DateTime.Now.Date;

        using (TuitionEntities entity = new TuitionEntities())
        {
            var classSearchQuery = from c in entity.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                                   where (c.StartDate <= _checkDate && c.EndDate >= _checkDate) && c.ClassRoom.Xlk_Building.CampusId == GetCampusId
                                   orderby c.Label
                                   select c;
            LoadResults(classSearchQuery.ToList());
        }
    }
    protected void Click_Future()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classSearchQuery = from c in entity.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                                   where c.StartDate > DateTime.Now && c.ClassRoom.Xlk_Building.CampusId == GetCampusId
                                   orderby c.Label
                                   select c;
            LoadResults(classSearchQuery.ToList());
        }
    }

    protected void Click_Previous()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classSearchQuery = from c in entity.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                                   where c.EndDate < DateTime.Now && c.ClassRoom.Xlk_Building.CampusId == GetCampusId
                                   orderby c.Label
                                   select c;
            LoadResults(classSearchQuery.ToList());
        }
    }

    protected void Click_LoadAllClasses()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classSearchQuery = from c in entity.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType")
                                   where c.ClassRoom.Xlk_Building.CampusId == GetCampusId
                                   orderby c.Label
                                   select c;
            LoadResults(classSearchQuery.ToList());
        }
    }

    private void LoadResults(List<Class> classes)
    {
        //Set the datasource of the repeater
        results.DataSource = classes;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", classes.Count().ToString());
    }

    protected void lookupclass(object sender, EventArgs e)
    {
        Click_SearchClasses();
    }

    protected void Click_LoadClasses()
    {
        int classLevelId = Convert.ToInt32(listclasses.SelectedItem.Value);
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (classLevelId > 0)
            {
                var classSearchQuery = from c in entity.Class.Include("Student").Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType")
                                       where c.ClassRoom.Xlk_Building.CampusId == GetCampusId
                                       select c;

                if (classLevelId > 0)
                    classSearchQuery = classSearchQuery.Where(c => c.TuitionLevel.TuitionLevelId == classLevelId);

                results.DataSource = classSearchQuery.ToList();
            }
            else
                results.DataSource = null;

            results.DataBind();
            listclasses.SelectedIndex = 0;
        }
    }

    #region Javascript Enabled Methods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse CloneAndScheduleClass(int classid)
    {
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                var cloneQuery = from c in entity.CloneAndScheduleNewClass(classid)
                                 select c;

                Class _class = cloneQuery.FirstOrDefault();
                if (_class != null)
                    return new AjaxResponse(string.Format("ViewClassPage.aspx?ClassId={0}", _class.ClassId));

                return new AjaxResponse(new Exception("Could not clode the class, please try again"));
            }

        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    #endregion
}
