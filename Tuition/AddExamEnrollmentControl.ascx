﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddExamEnrollmentControl.ascx.cs" 
    Inherits="Tuition_AddExamEnrollment" %>

<script type="text/javascript">
    $(function () {
        getForm = function() {
            return $("#addexamenrollment");
        }

        getTargetUrl = function() {
            return '<%= ToVirtual("~/Tuition/ViewExamsPage.aspx","SaveExamEnrollment") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $("#ExamDateId").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#DateRegistered").datepicker({ dateFormat: 'dd/mm/yy' });
    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addexamenrollment" name="addexamenrollment" method="post" action="ViewExamsPage.aspx/SaveExamEnrollment">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="StudentExamId" value="<%= GetStudentExamId %>" />

            <label>
                Student Name
            </label>
            <select name="Student" id="StudentId">
                <%=GetStudentName() %>
            </select>
            <label for="ExamDates">
                Exam Date
            </label>
            <select name="ExamDates" id="ExamDateId">
                <%=GetExamDatesList() %>
            </select>
            <label for="DateRegistered">
                Date Registered
            </label>
            <input type="datetext" name="DateRegistered" id="DateRegistered" value="<%= GetDateRegistered %>" />
            <label>
                Paid
            </label>
            <input type="checkbox" id="Paid" <%=GetPaidValue %>  value="true"/>
            <label>
                Amount Paid
            </label>
            <input type="text" name="Amount" id="Amount" value="<%=GetPaidAmount %>" />
            <label>
                As Individual
            </label>
            <input type="checkbox" id="AsIndividual" <%=GetAsIndividual %> value="true" />
            <label>
                Result Score
            </label>
            <input type="text" id="ResultScore" value="<%=GetResultScore %>" />
        </fieldset>
    </div>
</form>