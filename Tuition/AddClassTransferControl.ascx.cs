﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Pace.DataAccess.Tuition;

public partial class Tuition_AddClassTransferControl : BaseTuitionControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string GetClassList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Pace.DataAccess.Tuition.Class _class in LoadActiveAndFutureClassList())
        {
            sb.AppendFormat("<option value={0}>{1} StartDate:{2} EndDate:{3}</option>", _class.ClassId,_class.Label,_class.StartDate.ToString("dd MMM yy"),_class.EndDate.ToString("dd MMM yy"));
        }

        return sb.ToString();
    }

    private List<Class> LoadActiveAndFutureClassList()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classQuery = from c in entity.Class
                             where (c.StartDate <= DateTime.Now && c.EndDate >= DateTime.Now) || (c.StartDate >= DateTime.Now)
                             select c;

            return classQuery.ToList();
        }
    }
}
