﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Tuition;
using System.Text;

using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity.Core;


public partial class Tuition_ClassPlanPage : BaseTuitionPage 
{

    private DateTime _modellingDate;

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {           
            _modellingDate = DateTime.Now.NextWeekDay(DayOfWeek.Monday);

            listprogrammetype.DataSource = BaseEnrollmentControl.LoadProgrammeTypes();
            listprogrammetype.DataBind();

            listcoursetype.DataSource = BaseEnrollmentControl.LoadCourseTypes();
            listcoursetype.DataBind();

            stuprogrammetype.DataSource = BaseEnrollmentControl.LoadProgrammeTypes();
            stuprogrammetype.DataBind();

            stucoursetype.DataSource = BaseEnrollmentControl.LoadCourseTypes();
            stucoursetype.DataBind();

            listbuilding.DataSource = LoadBuildings().Where(b=>b.CampusId == GetCampusId).ToList();
            listbuilding.DataBind();

            datepicker.Text = _modellingDate.ToString("dd/MM/yy");
            BuildClasses();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void datepicker_TextChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }

    protected void listbuilding_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }

    protected void listcoursetype_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }

    protected void isClosed_CheckedChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }
  
    protected void listprogrammetype_SelectedIndexChanged(object sender, EventArgs e)
    {
        RebuildPage();
    }

    protected void classroom_SelectedIndexChanged(object sender, EventArgs e)
    {
        var classId = Convert.ToInt32(((System.Web.UI.WebControls.WebControl)(sender)).CssClass);
        short classroomId = Convert.ToInt16(((System.Web.UI.WebControls.ListControl)(sender)).SelectedValue);
        var date = DateTime.Parse(datepicker.Text);
        ChangeClassInfo(classId, date, null , classroomId);
    }

    protected void teacher_SelectedIndexChanged(object sender, EventArgs e)
    {
        var classId = Convert.ToInt32(((System.Web.UI.WebControls.WebControl)(sender)).CssClass);
        short teacherId = Convert.ToInt16(((System.Web.UI.WebControls.ListControl)(sender)).SelectedValue);
        var date = DateTime.Parse(datepicker.Text);
        ChangeClassInfo(classId, date, teacherId, null);
    }

    #endregion

    #region Private Methods

    protected void ChangeClassInfo(int classId,DateTime date,short? teacherid,short? classroomid)
    {

        using (TuitionEntities entity = new TuitionEntities())
        {

            var _currentClass = (from c in entity.Class.Include("Teacher").Include("ClassRoom")
                                where c.ClassId == classId
                                select c).FirstOrDefault();


            var _info = (from ci in entity.ClassInfoes
                         where ci.ClassId == classId && ci.ActiveFrom == date
                         select ci).FirstOrDefault();

            if (date == DateTime.Now.Date)
            {
                _currentClass.TeacherReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Teacher", "TeacherId", (teacherid.HasValue) ? teacherid.Value : _currentClass.Teacher.TeacherId);
                _currentClass.ClassRoomReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ClassRoom", "ClassroomId", (classroomid.HasValue) ? classroomid.Value : _currentClass.ClassRoom.ClassroomId);
            }

            if (_info == null)
            {
                _info = new ClassInfo();
                _info.ClassId = classId;
                _info.ActiveFrom = date;
                _info.TeacherReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Teacher", "TeacherId", (teacherid.HasValue) ? teacherid.Value : _currentClass.Teacher.TeacherId);
                _info.ClassRoomReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ClassRoom", "ClassroomId", (classroomid.HasValue) ? classroomid.Value : _currentClass.ClassRoom.ClassroomId);
                entity.AddToClassInfoes(_info);

            }
            else
            {
                if (teacherid.HasValue)
                _info.TeacherReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Teacher", "TeacherId", teacherid.Value);
                if (classroomid.HasValue)
                _info.ClassRoomReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ClassRoom", "ClassroomId",classroomid.Value);

            }

            entity.SaveChanges();
        }

    }

    protected List<ClassRoom> GetClassrooms()
    {
        byte buildingId = Convert.ToByte(listbuilding.SelectedValue);

        return LoadClassRooms().Where(x => x.Xlk_Building.BuildingId == buildingId).ToList();

    }

    protected List<Teacher> GetTeachers()
    {
        return LoadTeachers().ToList();

    }

    private void BuildClasses()
    {

        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        byte programmeTypeId = Convert.ToByte(listprogrammetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classQuery = from c in entity.Class
                             join info in (from ci in entity.ClassInfoes where ci.ActiveFrom <= _modellingDate.Date group ci by ci.ClassId into g select g.OrderByDescending(x => x.ActiveFrom).FirstOrDefault()) on c.ClassId equals info.ClassId into left
                             where (c.IsClosed == _isClosed) && (c.StartDate <= _modellingDate.Date && c.EndDate >= _modellingDate.Date) && (c.ClassRoom.Xlk_Building.BuildingId == buildingId) && (c.Xlk_CourseType.CourseTypeId == courseTypeId)
                             orderby c.TuitionLevel.MinPlancementScore
                             select new
                             {
                                 ClassId = c.ClassId,
                                 ClassroomId = (left.FirstOrDefault() == null) ? c.ClassRoom.ClassroomId : left.FirstOrDefault().ClassRoom.ClassroomId,
                                 ClassRoom = (left.FirstOrDefault() == null) ?c.ClassRoom.Label : left.FirstOrDefault().ClassRoom.Label,
                                 Teacher = (left.FirstOrDefault() == null) ? c.Teacher.Name : left.FirstOrDefault().Teacher.Name,
                                 TeacherId = (left.FirstOrDefault() == null) ? c.Teacher.TeacherId : left.FirstOrDefault().Teacher.TeacherId,
                                 TuitionLevel = c.TuitionLevel.Description,
                                 CEFR = c.TuitionLevel.CEFR,
                                 ProgrammeTypeId = c.Xlk_ProgrammeType.ProgrammeTypeId,
                                 xx = c.Enrollment.Where(e => (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date)).Count(),
                                 Quantity = c.Enrollment.Where(e => (e.StartDate <= _modellingDate.Date && e.EndDate >= _modellingDate.Date && c.ClassSchedule.Any(x => x.Class.ClassId == c.ClassId && x.StartDate <= _modellingDate.Date && !x.DateProcessed.HasValue))).Count()
                             };

            if (programmeTypeId > 0)
                classQuery = classQuery.Where(x => x.ProgrammeTypeId == programmeTypeId);

            results.DataSource = classQuery;
            results.DataBind();
        }
    }

    protected string GetReportURL(string reportname)
    {
        if (string.IsNullOrEmpty(datepicker.Text))
            return "#";
        
        _modellingDate = Convert.ToDateTime(datepicker.Text);
        byte buildingId = Convert.ToByte(listbuilding.SelectedItem.Value);
        byte courseTypeId = Convert.ToByte(listcoursetype.SelectedItem.Value);
        bool _isClosed = Convert.ToBoolean(isClosed.Checked);
        if (reportname == "Multi_Room_Labels_Today")
            return ReportPath("AcademicReports", reportname, "HTML4.0", new Dictionary<string, object> { { "ModellingDate", _modellingDate.Date.ToString("dd/MMM/yyyy") }, { "CourseTypeId", courseTypeId }, { "BuildingId", buildingId }, { "IsClosed", _isClosed } });
        else
            return ReportPath("TuitionReports", reportname, "HTML4.0", new Dictionary<string, object> { { "Date", _modellingDate.Date.ToString("dd/MMM/yyyy") }, { "CourseTypeId", courseTypeId }, { "BuildingId", buildingId }, { "IsClosed", _isClosed } });
     }


    private void RebuildPage()
    {
        if (!string.IsNullOrEmpty(datepicker.Text))
        {
            _modellingDate = Convert.ToDateTime(datepicker.Text);

            BuildClasses();
        }
    }

    #endregion

    #region Javascript Enabled Methods


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> BuildStudents(DateTime modellingdate, object classid)
    {
        int _classId = Convert.ToInt32(classid);
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                var ids = entity.FindClassStudents(modellingdate, _classId).ToList();

                List<Enrollment> enrollmentswithschedulelist = (from e in entity.Enrollment.Include("Student")
                                                                where ids.Contains(e.EnrollmentId) && e.Student.CampusId == GetCampusId
                                                                select e).OrderBy(s => s.Student.FirstName).ToList();

                StringBuilder sb = new StringBuilder();
                if (enrollmentswithschedulelist != null && enrollmentswithschedulelist.Count > 0)
                {
                    int i = 1;
                    foreach (Enrollment enrollment in enrollmentswithschedulelist)
                    {
                        sb.AppendFormat("<div id=\"{2}\" class=\"draggable ui-state-default ui-draggable {3}\">{0}:&nbsp;{1}</div>", i++, TuitionLink(enrollment.EnrollmentId, "~/Tuition/ViewClassPage.aspx", CreateName(enrollment.Student.FirstName, enrollment.Student.SurName)), enrollment.EnrollmentId, (enrollment.Student.StudentTypeId == 2) ? "leader" : string.Empty);
                    }
                }
                return new AjaxResponse<string>(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> AssignStudentToClassSchedule(int classid, long enrollmentid,DateTime assignmentdate)
    {
         try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                Enrollment enrollment = (from e in entity.Enrollment.Include("Class").Include("Student").Include("ClassSchedule")
                                         where e.EnrollmentId == enrollmentid
                                         select e).First();


                if (enrollment != null && classid == -1)
                {
                    var schedule = (from e in entity.ClassSchedule
                                    where e.EnrollmentId == enrollmentid && !e.DateProcessed.HasValue
                                    select e).FirstOrDefault();

                    if (schedule == null)
                        enrollment.Class = null; 

                    if (schedule != null)
                        entity.DeleteObject(schedule);


                    if (entity.SaveChanges() > 0)
                        return new AjaxResponse<string>(string.Empty);
                    else
                         return new AjaxResponse<string>(new Exception( "Could not remove schedule"));
                }

                if (enrollment.Class != null && enrollment.Class.ClassId == classid)
                     return new AjaxResponse<string>(new Exception( "Could not find the class or the enrollment!"));


                if (enrollment.ClassSchedule.Any(x => !x.DateProcessed.HasValue))
                {
                    List<ClassSchedule> schedules = enrollment.ClassSchedule.Where(x => !x.DateProcessed.HasValue).ToList();
                    foreach (ClassSchedule schedule in schedules)
                    {
                        entity.DeleteObject(schedule);
                    }
                    entity.SaveChanges();
                }

                ClassSchedule newSchedule = ClassSchedule.CreateClassSchedule(enrollmentid, assignmentdate.Date);
                newSchedule.ClassReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Class", "ClassId", classid);
                newSchedule.EnrollmentReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Enrollment", "EnrollmentId", enrollmentid);
                entity.AddToClassSchedule(newSchedule);

                if (entity.SaveChanges() > 0)
                    return new AjaxResponse<string>(string.Empty);
                else
                    return new AjaxResponse<string>(new Exception( "Could not save this move"));
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> FindLooseStudents(DateTime modellingdate, byte programmetypeid, byte coursetypeid, bool isclosed,bool showleaders) //int id,int onlyfreefamilies, int excludenationality, string familyname, int statusid, int zoneid, int roomtypeid, int ensuite)
    {
        StringBuilder sb = new StringBuilder();

        try
        {

            using (TuitionEntities entity = new TuitionEntities())
            {

                var ids = entity.FindLooseStudents(modellingdate, programmetypeid, coursetypeid, isclosed, showleaders).ToList();
                var enrollments = (from n in entity.Enrollment.Include("Student").Include("Student.Group")
                                   where ids.Contains(n.EnrollmentId) && n.Student.CampusId == GetCampusId
                                   select n);

                if (enrollments != null && enrollments.ToList().Count > 0)
                {
                    int i = 1;
                    foreach (Enrollment enrollment in enrollments)
                    {
                        sb.AppendFormat("<div id=\"{1}\" class=\"draggable ui-state-default ui-draggable {5}\">{4}:&nbsp;{0} {2}% <i> - {3}</i></div>", TuitionLink(enrollment.EnrollmentId, "~/Tuition/ViewClassPage.aspx", CreateName(enrollment.Student.FirstName, enrollment.Student.SurName)), enrollment.EnrollmentId, enrollment.Student.PlacementTestResult, enrollment.Student.GroupId != null ? enrollment.Student.Group.GroupName : string.Empty, i++, (enrollment.Student.StudentTypeId == 2) ? "leader" : string.Empty);
                    }
                }
                return new AjaxResponse<string>(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    #endregion

    protected void results_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var classDropDownList = e.Item.FindControl("ddlClassRooms") as DropDownList;
            classDropDownList.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "ClassroomId").ToString()).Selected = true;
            classDropDownList.CssClass = DataBinder.Eval(e.Item.DataItem, "ClassId").ToString();

            var teacherDropDownList = e.Item.FindControl("ddlTeachers") as DropDownList;
            teacherDropDownList.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "TeacherId").ToString()).Selected = true;
            teacherDropDownList.CssClass = DataBinder.Eval(e.Item.DataItem, "ClassId").ToString();
        }
    }
}
