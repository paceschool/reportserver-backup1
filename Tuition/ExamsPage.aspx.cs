﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Text;
using System.Globalization;
using Pace.DataAccess.Tuition;
using System.Drawing.Drawing2D;
using System.Data.Entity.Core.Objects;

public partial class Tuition_ExamsPage : BaseTuitionPage
{
    protected Int32 examType = 0;
    protected Int32 DateExam = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Click_UpcomingExams();

            if (!string.IsNullOrEmpty(Request["Action"]))
                ShowSelected(Request["Action"]);

            LoadChart();

        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void ShowSelected(string action)
    {
        switch (action.ToLower())
        {
            case "upcoming":
                Click_UpcomingExams();
                break;
            case "firstcert":
                Click_FirstCertExams();
                break;
            case "advanced":
                Click_AdvancedExams();
                break;
            case "proficiency":
                Click_ProficiencyExams();
                break;
            default:
                Click_LoadAllExams();
                break;

        }
    }

    protected void Click_UpcomingExams()
    {
        //using (TuitionEntities entity = new TuitionEntities())
        //{
        //    var lookupQuery = ((from u in entity.ExamDates
        //                        join ee in
        //                            (from e in entity.Exam
        //                             from ed in e.ExamDates
        //                             where ed.ExamDate > DateTime.Now
        //                             group new { ed.Exam.ExamId, ed.ExamDate } by e.ExamId into group_exam
        //                             select new { ExamId = group_exam.Key, ExamDate = group_exam.Min(x => x.ExamDate) }) on new { u.Exam.ExamId, u.ExamDate } equals new { ee.ExamId, ee.ExamDate }
        //                        select u) as ObjectQuery<ExamDates>).Include("Exam").Include("Xlk_ExamType");

        //    results.DataSource = lookupQuery.ToList();
        //    results.DataBind();
        //    resultsreturned.Text = string.Format("Records Found: {0}", lookupQuery.ToList().Count().ToString());
        //}

        using (TuitionEntities eentity = new TuitionEntities())
        {
            DateTime limit = DateTime.Now.AddMonths(4);
            var lookupQuery = from ex in eentity.ExamDates.Include("Exam").Include("Xlk_ExamType")
                              where ex.ExamDate >= DateTime.Now && ex.ExamDate < limit
                              orderby ex.ExamDate, ex.ExamDateId
                              select ex;
            results.DataSource = lookupQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", lookupQuery.ToList().Count().ToString());
        }
    }

    protected void Click_CurrentExams()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var lookupQuery = from c in entity.ExamDates.Include("Exam").Include("Xlk_ExamType")
                              where c.ExamDate >= DateTime.Today
                              orderby c.ExamDate ascending
                              select c;

            results.DataSource = lookupQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", lookupQuery.ToList().Count().ToString());
        }
    }
    
    protected void Click_FirstCertExams()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var lookupQuery = from f in entity.ExamDates.Include("Exam").Include("Xlk_ExamType")
                              where f.Exam.ExamId == 3
                              orderby f.ExamDate ascending
                              select f;

            results.DataSource = lookupQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", lookupQuery.ToList().Count().ToString());
        }
    }

    protected void Click_AdvancedExams()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var lookupQuery = from f in entity.ExamDates.Include("Exam")
                              where f.Exam.ExamId == 4
                              orderby f.ExamDate ascending
                              select f;

            results.DataSource = lookupQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", lookupQuery.ToList().Count().ToString());
        }
    }

    protected void Click_ProficiencyExams()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var lookupQuery = from f in entity.ExamDates.Include("Exam")
                              where f.Exam.ExamId == 5
                              orderby f.ExamDate ascending
                              select f;

            results.DataSource = lookupQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", lookupQuery.ToList().Count().ToString());
        }
    }

    protected void Click_LoadAllExams()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var lookupQuery = from f in entity.ExamDates.Include("Exam")
                              orderby f.ExamDate ascending
                              select f;

            results.DataSource = lookupQuery.ToList();
            results.DataBind();
            resultsreturned.Text = string.Format("Records Found: {0}", lookupQuery.ToList().Count().ToString());
        }
    }

    protected void LoadChart()
    {
        

    }

    protected void lookupExam(object sender, EventArgs e)
    {
        Click_CurrentExams();
    }

    private void LoadResults(List<Student> students)
    {
        results.DataSource = students;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", students.Count().ToString());
    }

    protected DateTime ToDate(object date, int days)
    {
        if (date != null)
            return Convert.ToDateTime(date).AddDays(days);

        return DateTime.Now.AddDays(days);
    }

    protected string ReportPath(string ReportName, Dictionary<string, object> _params)
    {

        //string.Format("{0}?/PaceManagerReports/{1}&{2}&rs:Command=Render&rs:Format=HTML4.0&rc:Parameters=False", BasePage.ReportServerURL1, FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek), ReportName);

        return string.Format("{0}?/PaceManagerReports/{1}{2}&rs:Command=Render&rs:Format=pdf&rc:Parameters=False", ReportServerURL, ReportName, _params.ToQueryString());
    }
}
