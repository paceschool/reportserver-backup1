﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Text;
using System.Globalization;
using Pace.DataAccess.Tuition;

public partial class Tuition_ViewExamsPage : BaseTuitionPage
{
    protected Int32 _examDateId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["ExamDateId"]))
            _examDateId = Convert.ToInt32(Request["ExamDateId"]);

        if (!Page.IsPostBack)
        {
            DisplayExams(LoadExams(new TuitionEntities(),_examDateId));
        }
    }

    public override string TextValue() // Page Name
    {
        newPageName.Text = "View Exam Page - " + ExamName1.Text + " " + ExamDate.Text;
        return newPageName.Text;
    }

    private ExamDates LoadExams(TuitionEntities entity,Int32 examDateId)
    {
        IQueryable<ExamDates> examQuery = from e in entity.ExamDates.Include("Xlk_ExamType").Include("Exam")
                                          where e.ExamDateId == examDateId
                                          select e;

        if (examQuery.ToList().Count() > 0)
            return examQuery.ToList().First();

        return null;
    }

    private void DisplayExams(ExamDates exam)
    {
        if (exam != null)
        {
            ExamDateId.Text = exam.ExamDateId.ToString();
            ExamDate.Text = string.Format("{0:D}", exam.ExamDate);
            RegistrationDate.Text = string.Format("{0:D}", exam.RegistrationDate);
            RegistrationDate1.Text = string.Format("{0:D}", exam.RegistrationDate);
            ExamTypeId.Text = exam.Xlk_ExamType.Description;
            ExamName.Text = exam.Exam.ExamName;
            ExamName1.Text = exam.Exam.ExamName;
            ExamDateConfirmed.Text = exam.ExamDateConfirmed.ToString();

            newexamdate.Text = exam.ExamDate.Value.ToString("dd/MM/yyyy");
            if (exam.RegistrationDate.HasValue)
                newregdate.Text = exam.RegistrationDate.Value.ToString("dd/MM/yyyy");
            newspeakdate.Text = exam.StartDate.Value.ToString("dd/MM/yyyy");
            newspeakend.Text = exam.EndDate.Value.ToString("dd/MM/yyyy");
            newstudentdepart.Text = exam.ExpecetedStudentDepartureDate.Value.ToString("dd/MM/yyyy");

            newexamconfirmed.Checked = exam.ExamDateConfirmed;

            string TextToTrim = "<br/>Exam Date: <b>" + exam.ExamDate.Value.ToString("dd/MM/yyyy") + "</b><br/><br/>Registration Date: <b>" + ((!exam.RegistrationDate.HasValue) ? "Not Defined": exam.RegistrationDate.Value.ToString("dd/MM/yyyy")) + "</b><br/><br/>Speaking Test Window:<br/>From: <b>" + exam.StartDate.Value.ToString("dd/MM/yyyy") + "</b><br/>To: <b>" + exam.EndDate.Value.ToString("dd/MM/yyyy") + "</b><br/><br/>Earliest Student Departure: <b>" + exam.ExpecetedStudentDepartureDate.Value.ToString("dd/MM/yyyy") + "</b><br/><br/>Exam Date Confirmed: <b>" + (exam.ExamDateConfirmed ? "YES" : "NO") + "</b>";
            //trimmedtext.Text = TrimText(TextToTrim, 1350);
            moreinfo.Title = "<br/>Exam Date: <b>" + exam.ExamDate.Value.ToString("dd/MM/yyyy") + "</b><br/><br/>Registration Date: <b>" + ((!exam.RegistrationDate.HasValue) ? "Not Defined" : exam.RegistrationDate.Value.ToString("dd/MM/yyyy"))  + "</b><br/><br/>Speaking Test Window:<br/>From: <b>" + exam.StartDate.Value.ToString("dd/MM/yyyy") + "</b><br/>To: <b>" + exam.EndDate.Value.ToString("dd/MM/yyyy") + "</b><br/><br/>Earliest Student Departure: <b>" + exam.ExpecetedStudentDepartureDate.Value.ToString("dd/MM/yyyy") + "</b><br/><br/>Exam Date Confirmed: <b>" + (exam.ExamDateConfirmed ? "YES" : "NO") + "</b>";
        }
    }

    protected void Click_Save(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (!string.IsNullOrEmpty(newexamdate.Text) && !string.IsNullOrEmpty(newregdate.Text) && !string.IsNullOrEmpty(newspeakdate.Text)
                && !string.IsNullOrEmpty(newspeakend.Text) && !string.IsNullOrEmpty(newstudentdepart.Text))
            {
                ExamDates _exam = GetExamDate(entity);

                _exam.ExamDate = Convert.ToDateTime(newexamdate.Text);
                _exam.RegistrationDate = Convert.ToDateTime(newregdate.Text);
                _exam.StartDate = Convert.ToDateTime(newspeakdate.Text);
                _exam.EndDate = Convert.ToDateTime(newspeakend.Text);
                _exam.ExpecetedStudentDepartureDate = Convert.ToDateTime(newstudentdepart.Text);
                _exam.ExamDateConfirmed = (newexamconfirmed.Checked);

                SaveExam(entity, _exam);
            }
        }
    }

    private ExamDates GetExamDate(TuitionEntities entity)
    {
        return LoadExams(entity,_examDateId);
    }

    private void SaveExam(TuitionEntities entity,ExamDates exam)
    {
        if (entity.SaveChanges() > 0)
        {
            DisplayExams(LoadExams(entity,_examDateId));
            object refUrl = ViewState["refUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }
}