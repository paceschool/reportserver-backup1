﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Tuition;
using System.Text;
using System.Web.Services;

public partial class Tuition_AddExamControl : BaseTuitionControl
{
    protected Int32 _examId;
    protected Int32 _examDateId;
    protected ExamDates _examDate;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (GetValue<Int32?>("ExamDateId").HasValue)
            {
                Int32 _examDateId = GetValue<Int32>("ExamDateId");
                _examDate = (from ex in entity.ExamDates.Include("Xlk_ExamType").Include("Exam")
                             where ex.ExamDateId == _examDateId
                             select ex).FirstOrDefault();
            }
        }
    }

    protected string GetExams()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_examDate != null) ? _examDate.Exam.ExamId : (Int32?)null;

        if (_current.HasValue)
            sb.AppendFormat("<option {2} value={0}>{1}</option>", _examDate.Exam.ExamId, _examDate.Exam.ExamName, "selected");
        else
        {
            foreach (Exam item in LoadExams())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExamId, item.ExamName, (_current.HasValue && item.ExamId == _current) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();
    }

    protected string GetExamTypes()
    {
        StringBuilder sb = new StringBuilder();

        Byte? _current = (_examDate != null) ? _examDate.Xlk_ExamType.ExamTypeId : (Byte?)null;

        if (_current.HasValue)
            sb.AppendFormat("<option {2} value={0}>{1}</option>", _examDate.Xlk_ExamType.ExamTypeId, _examDate.Xlk_ExamType.Description, "selected");
        else
        {
            foreach (Xlk_ExamType item in LoadExamTypes())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExamTypeId, item.Description, (_current.HasValue && item.ExamTypeId == _current) ? "selected" : string.Empty);
            }
        }
        return sb.ToString();
    }

    protected string GetExamDateId
    {
        get
        {
            if (_examDate != null)
                return _examDate.ExamDateId.ToString();

            if (Parameters.ContainsKey("ExamDateId"))
                return Parameters["ExamDateId"].ToString();

            return "0";
        }
    }

    protected string GetExamDate
    {
        get
        {
            if (_examDate != null)
                return _examDate.ExamDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetRegistrationDate
    {
        get
        {
            if (_examDate != null)
                return _examDate.RegistrationDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetStartDate
    {
        get
        {
            if (_examDate != null)
                return _examDate.StartDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetEndDate
    {
        get
        {
            if (_examDate != null)
                return _examDate.EndDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetDepartureDate
    {
        get
        {
            if (_examDate != null && _examDate.ExpecetedStudentDepartureDate.HasValue)
                return _examDate.ExpecetedStudentDepartureDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetConfirmed
    {
        get
        {
            if (_examDate != null)
            {
                if (_examDate.ExamDateConfirmed)
                    return "checked";
                else
                    return string.Empty;
            }

            return null;
        }
    }
}