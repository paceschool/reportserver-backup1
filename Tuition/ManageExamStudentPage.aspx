﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="ManageExamStudentPage.aspx.cs" Inherits="Tuition_ManageExamStudentPage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(function() {

            $("#<%= regdate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Manage Exam Student Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="viewTabs">
    <div id="studentdetails">
        <table class="box-table-a">
            <thead>
                <tr>
                    <th scope="col">
                        <label>
                            Student Id
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Name
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Exam Type
                        </label>
                    </th>
                    <th scope="col">
                        <label>
                            Exam Date
                        </label>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align:center">
                        <asp:Label ID="studentid" runat="server">
                        </asp:Label>
                    </td>
                    <td style="text-align:center">
                        <asp:Label ID="studentname" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="studentexamname" runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="studentexamdate" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    <div class="inputArea">
        <fieldset>
            <label>
                Registered
            </label>
            <asp:CheckBox ID="regstudent" runat="server" />
            <label>
                Registration Date
            </label>
            <asp:TextBox ID="regdate" runat="server">
            </asp:TextBox>
            <label>
                Exam Date
            </label>
            <asp:DropDownList ID="exdates" runat="server" DataTextField="ExamDate"
                DataValueField="ExamDateId" DataTextFormatString="{0:D}">
            </asp:DropDownList>
        </fieldset>
        <fieldset>
            <label>
                Paid
            </label>
            <asp:CheckBox ID="chkpaid" runat="server" />
            <label>
                Home Address
            </label>
            <textarea id="homeaddress" cols="20" rows="5" runat="server">
            </textarea>
        </fieldset>
        <fieldset>
            <label>
                email
            </label>
            <asp:TextBox ID="studentemail" runat="server" />
            <asp:LinkButton ID="examsave" runat="server" class="button" OnClick="Click_SaveStudentExam"><span>Save</span></asp:LinkButton>
            <asp:LinkButton ID="examcancel" runat="server" class="button" PostBackUrl="~/Tuition/ExamsPage.aspx"><span>Cancel</span></asp:LinkButton>
        </fieldset>
    </div>
</asp:Content>