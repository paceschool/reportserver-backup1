﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddClassTransferControl.ascx.cs"
    Inherits="Tuition_AddClassTransferControl" %>

<script type="text/javascript">
    $(function() {

        getForm = function() {
            return $("#addclasstransfer");
        }

        $("#ActionDate").datepicker({ dateFormat: 'dd/mm/yy' });


        getTargetUrl = function() {
            return '<%= ToVirtual("~/Tuition/ViewClassPage.aspx","SaveClassTransfer") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>

<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addclasstransfer" name="addclasstransfer" method="post" action="ViewClassPage.aspx/SaveClassTransfer">
<div class="inputArea">
    <fieldset>
    Move the student from the current class to on the selected date!
        <input type="hidden" name="FromClass" id="ClassId" value="<%= GetValue<Int32>("ClassId") %>" />
        
        <label>
            To Class
        </label>
        <select type="text" name="ToClass" id="ClassId">
            <%= GetClassList()%>
        </select>
        <label>
            Action Date
        </label>
        <input type="datetext" name="ActionDate" id="ActionDate" />
    </fieldset>
</div>
</form>
