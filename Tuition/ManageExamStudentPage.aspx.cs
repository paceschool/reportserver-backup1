﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Tuition;
using Pace.DataAccess.Enrollment;

using System.Data;
using System.Text;
using System.Globalization;
using System.Data.Entity.Core;

public partial class Tuition_ManageExamStudentPage : BaseTuitionPage
{
    protected Int32? _studentId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["StudentId"]))
            _studentId = Convert.ToInt32(Request["StudentId"]);

        if (!Page.IsPostBack)
        {
            exdates.DataSource = this.LoadExamDates();
            exdates.DataBind();

            if (_studentId.HasValue)
            {
                DisplayStudent(LoadStudent(_studentId.Value));
            }
        }
    }

    protected void Click_SaveStudentExam(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            StudentExam _student = GetStudentExam();

            if (regdate.Text != "")
                _student.DateRegistered = Convert.ToDateTime(regdate.Text);
            _student.ExamDatesReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".ExamDates", "ExamDateId", Convert.ToInt32(exdates.SelectedItem.Value));
            _student.Paid = chkpaid.Checked;
            _student.Student.HomeAddress = homeaddress.Value;
            _student.Student.StudentEmail = studentemail.Text;
            _student.Student.Exam.ExamName = studentexamname.Text;
            _student.ExamDates.ExamDate = Convert.ToDateTime(studentexamdate.Text);

            SaveStudentExam(entity,_student);
        }
    }

    private void DisplayStudent(StudentExam student)
    {
        if (student != null)
        {
            studentname.Text = student.Student.FirstName + " " + student.Student.SurName;
            studentid.Text = Convert.ToString(student.Student.StudentId);
            if (student.Student.Exam != null)
                regstudent.Checked = true;
            else regstudent.Checked = false;
            regdate.Text = Convert.ToString(student.DateRegistered);
            exdates.Items.FindByValue(student.ExamDates.ExamDateId.ToString()).Selected = true;
            if (student.Paid == true)
                chkpaid.Checked = student.Paid.Value;
            else chkpaid.Checked = false;
            homeaddress.Value = student.Student.HomeAddress;
            studentemail.Text = student.Student.StudentEmail;
            studentexamname.Text = student.Student.Exam.ExamName;
            studentexamdate.Text = student.ExamDates.ExamDate.Value.ToString("dddd dd MMMM yyyy");
        }
    }

    private StudentExam LoadStudent(Int32 studentId)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            IQueryable<StudentExam> lookupQuery = from s in entity.StudentExam
                                                  where s.Student.StudentId == studentId
                                                  select s;

            if (lookupQuery.Count() > 0)
                return lookupQuery.FirstOrDefault();
        }
        return null;
    }

    private StudentExam GetStudentExam()
    {
        if (_studentId.HasValue)
            return LoadStudent(_studentId.Value);

        return new StudentExam();
    }

    private void SaveStudentExam(TuitionEntities entity ,StudentExam student)
    {
        if (!_studentId.HasValue)
            entity.AddToStudentExam(student);

        if (entity.SaveChanges() > 0)
            Response.Redirect(string.Format("../Tuition/ExamsPage.aspx", student.Student.Exam.ExamId));
    }
}