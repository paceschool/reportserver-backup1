﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ClassPlanPage.aspx.cs" Inherits="Tuition_ClassPlanPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {

            $("#<%= datepicker.ClientID %>").datepicker({ minDate: "-90D", maxDate: "+3M +60D", dateFormat: 'dd/mm/yy' });

            displayOption = function () {
                $(".draggable").draggable({
                    helper: "clone",
                    revert: "invalid",
                    containment: ".resultscontainer",
                    scroll: false
                });

                $(".droppable").droppable({
                    accept: ".draggable",
                    activeClass: "ui-state-hover",
                    hoverClass: "ui-state-active",
                    drop: function (event, ui) {
                        $(this).addClass("ui-state-highlight");
                        assignStudentToClassSchedule(this.id, ui.draggable.attr('id'), $("#<%= datepicker.ClientID %>").datepicker("getDate"))
                    }
                });

                createPopUp();
            }

            displayOption();

            assignStudentToClassSchedule = function (classid, enrollmentid, assignmentdate) {
                $.ajax({
                    type: "POST",
                    url: "ClassPlanPage.aspx/AssignStudentToClassSchedule",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ classid: classid, enrollmentid: enrollmentid, assignmentdate: assignmentdate }),
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                getStudents($("#<%= datepicker.ClientID %>").datepicker("getDate"), $('#<%= stuprogrammetype.ClientID %> option:selected').val(), $('#<%=stucoursetype.ClientID %> option:selected').val(), $('#<%=stuisClosed.ClientID %>').is(':checked'), $('#<%=leaders.ClientID %>').is(':checked'));
                                getClassStudents($("#<%= datepicker.ClientID %>").datepicker("getDate"), classid);
                                displayOption();
                            }
                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            };

            $('.watcher').change(function () {
                getStudents($("#<%= datepicker.ClientID %>").datepicker("getDate"), $('#<%= stuprogrammetype.ClientID %> option:selected').val(), $('#<%=stucoursetype.ClientID %> option:selected').val(), $('#<%=stuisClosed.ClientID %>').is(':checked'), $('#<%=leaders.ClientID %>').is(':checked'));
            });


            $("#assigned").jScroll();

            getStudents = function (date, programmetypeid, coursetypeid, isclosed, showleaders) {
                $.ajax({
                    type: "POST",
                    url: "ClassPlanPage.aspx/FindLooseStudents",
                    data: JSON.stringify({ modellingdate: date, programmetypeid: programmetypeid, coursetypeid: coursetypeid, isclosed: isclosed, showleaders: showleaders }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                $('#filteredstudents').html(msg.d.Payload);
                                displayOption();
                            }

                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            }

            getClassStudents = function (date, classid) {
                $.ajax({
                    type: "POST",
                    url: "ClassPlanPage.aspx/BuildStudents",
                    data: JSON.stringify({ modellingdate: date, classid: classid }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                $('#students_' + classid).html(msg.d.Payload);
                                displayOption();
                            }

                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            }

        });
    </script>
    <style type="text/css">
        #disconnect
        {
            float: left;
        }
        #notassigned .classtd div
        {
            background-color: Red;
        }
        .classtd
        {
            border: 1px solid #AED0EA;
            vertical-align: top;
        }
        .classlist
        {
            float: left;
        }
        .classlist div
        {
            background-color: Fuchsia;
            font-weight: bold;
        }
        #notassigned
        {
            float: left;
        }
        #assigned
        {
            float: left;
        }
        .leader
        {
            background-image:none;
            background-color:yellow !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Class Planner Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Date:
                    </td>
                    <td>
                        <asp:TextBox ID="datepicker" runat="server" OnTextChanged="datepicker_TextChanged"
                            AutoPostBack="true">
                        </asp:TextBox>
                    </td>
                    <td>
                        For Programme Type:
                    </td>
                    <td>
                        <asp:DropDownList ID="listprogrammetype" runat="server" AppendDataBoundItems="true"
                            DataValueField="ProgrammeTypeId" DataTextField="Description" OnSelectedIndexChanged="listprogrammetype_SelectedIndexChanged"
                            AutoPostBack="true">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Is Closed:
                    </td>
                    <td>
                        <asp:CheckBox ID="isClosed" runat="server" OnCheckedChanged="isClosed_CheckedChanged"
                            AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        For Course Type:
                    </td>
                    <td>
                        <asp:DropDownList ID="listcoursetype" runat="server" DataValueField="CourseTypeId"
                            DataTextField="Description" OnSelectedIndexChanged="listcoursetype_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        For Building:
                    </td>
                    <td>
                        <asp:DropDownList ID="listbuilding" runat="server" DataValueField="BuildingId" DataTextField="Description"
                            OnSelectedIndexChanged="listbuilding_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    <td>
                        <a href="<%= GetReportURL("ClassList") %>">
                            <img alt="Class List" src="../Content/img/actions/printer.png" />Print Class List</a>
                        <br />
                        <a href="<%= GetReportURL("Multi_Room_Labels_Today") %>">
                            <img alt="Door List" src="../Content/img/actions/printer.png" />Print Door List</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer" style="width: 3000px;">
        <div id="notassigned" style="width: 300px;">
            <table id="ctl00_maincontent_results" cellspacing="0" border="0" style="border-collapse: collapse;">
                <tbody>
                    <tr>
                        <td class="classtd">
                            <ul class="classlist" style="float: left; list-style-type: none; font-style: italic;">
                                <div id="-1" class="droppable">
                                    <li>Students not assigned</li>
                                    <li>
                                        <asp:DropDownList ID="stuprogrammetype" CssClass="watcher" runat="server" AppendDataBoundItems="true"
                                            DataValueField="ProgrammeTypeId" DataTextField="Description">
                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                        </asp:DropDownList>
                                    </li>
                                    <li>
                                        <asp:DropDownList ID="stucoursetype" CssClass="watcher" runat="server" AppendDataBoundItems="true"
                                            DataValueField="CourseTypeId" DataTextField="Description">
                                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                        </asp:DropDownList>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="stuisClosed" CssClass="watcher" runat="server" Text="IsClosed" /></li>
                                    <li>
                                        <asp:CheckBox ID="leaders" CssClass="watcher" runat="server" Text="Show Leaders" /></li>

                                </div>
                                <li id="filteredstudents">
                                    <%= FindLooseStudents(Convert.ToDateTime(datepicker.Text), Convert.ToByte(stuprogrammetype.SelectedItem.Value),  Convert.ToByte(stucoursetype.SelectedItem.Value), stuisClosed.Checked, leaders.Checked).Payload%>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="assigned" style="width: 90%;">
            <asp:DataList ID="results" runat="server" RepeatDirection="Horizontal" 
                OnItemDataBound="results_ItemDataBound" DataKeyField="ClassId">
                <HeaderStyle Font-Names="Verdana" Font-Size="12pt" HorizontalAlign="center" Font-Bold="True" />
                <ItemStyle CssClass="classtd" />
                <ItemTemplate>
                    <ul class="classlist" style="float: left; list-style-type: none;">
                        <div id="<%#DataBinder.Eval(Container.DataItem, "ClassId")%>" class="droppable">
                            <li><asp:DropDownList ID="ddlClassRooms" Runat="Server" DataTextField="Label" DataValueField="ClassRoomId"
                                    DataSource='<%# GetClassrooms() %>' OnSelectedIndexChanged="classroom_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist>
                                <a href="ViewClassPage.aspx?ClassId=<%#DataBinder.Eval(Container.DataItem, "ClassId")%>">
                                <img src="../Content/img/actions/arrow_right.png" /></a>
                                </li>
                            <li>
                                <%#DataBinder.Eval(Container.DataItem, "TuitionLevel").ToString().Trim()%></li>
                            <li>
                                <%#DataBinder.Eval(Container.DataItem, "CEFR").ToString().Trim()%></li>
                            <li><asp:Dropdownlist ID="ddlTeachers" Runat="Server" DataTextField="Name" DataValueField="TeacherId"
                                    DataSource='<%# GetTeachers() %>' OnSelectedIndexChanged="teacher_SelectedIndexChanged" AutoPostBack="true"></asp:Dropdownlist>
                                </li>
                            <li  id="students_<%#DataBinder.Eval(Container.DataItem, "ClassId")%>">
                                <%# BuildStudents(Convert.ToDateTime(datepicker.Text),DataBinder.Eval(Container.DataItem, "ClassId")).Payload%>
                            </li>
                        </div> 
                    </ul>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:DataList>
        </div>
    </div>
</asp:Content>
