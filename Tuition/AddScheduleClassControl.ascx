﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddScheduleClassControl.ascx.cs"
    Inherits="Tuition_AddClassTransferControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addclasstransfer");
        }

        $("#ActionDate").datepicker({ dateFormat: 'dd/mm/yy' });


        getTargetUrl = function () {
            return '<%= ToVirtual("~/Tuition/ViewClassPage.aspx","SaveScheduleClass") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }


        $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy', minDate:new Date(<%= GetMinDate %>),maxDate:new Date(<%= GetMaxDate %> ) });
        $("#EndDate").datepicker({ dateFormat: 'dd/mm/yy',minDate: new Date(<%= GetMinDate %>), maxDate: new Date(<%= GetMaxDate %> )  });

        
        $(".watcher").change(function (e) {

            var start_time = $('#DailyStartTime').val();
            var end_time = $('#DailyEndTime').val();

            var diff = (new Date("1970/1/1 " + end_time) -new Date("1970/1/1 " + start_time));
            $('#TuitionMinutes').val(diff/1000/60);

        });

    });
</script>
<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addclasstransfer" name="addclasstransfer" method="post" action="ViewClassPage.aspx/SaveClassTransfer">
<div class="inputArea">
    <fieldset>
     <input type="hidden" id="ScheduleId" value="<%= GetScheduleId %>" />
        <input type="hidden" name="Class" id="ClassId" value="<%= GetClassId %>" />
        <label>
            Daily Start Time
        </label>
        <input type="text" class="watcher" id="DailyStartTime" value="<%= GetDailyStartTime %>" />
        <label>
            Daily End Time
        </label>
        <input type="text" class="watcher" id="DailyEndTime" value="<%= GetDailyEndTime %>" />
        <label>
            Start Date
        </label>
        <input type="datetext" id="StartDate" value="<%= GetStartDate %>" />
        <label>
            End Date
        </label>
        <input type="datetext" id="EndDate" value="<%= GetEndDate %>" />
        <label>
            Tuition Minutes
        </label>
        <input type="text" disabled id="TuitionMinutes" value="<%= GetTuitionMinutes %>" />
        <label>
            Break Minutes
        </label>
        <input type="text" id="BreakMinutes" value="<%= GetBreakMinutes %>" />
    </fieldset>
</div>
</form>
