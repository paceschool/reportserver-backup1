﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddClassInfoControl.ascx.cs"
    Inherits="Tuition_AddClassInfoControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addclassinfo");
        }

        $("#ActiveFrom").datepicker({ dateFormat: 'dd/mm/yy' });



        getTargetUrl = function () {
            return '<%= ToVirtual("~/Tuition/ViewClassPage.aspx","SaveClassInfo") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>
<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addclassinfo" name="addclassinfo" method="post" action="ViewClassPage.aspx/SaveClassInfo">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ClassId" value="<%= GetClassId %>" />
        <input type="hidden" id="ClassId" name="Class" value="<%= GetClassId %>" />
        <label for="StartDate">
            Start Date</label>
        <input type="datetext" <%= IsDisabled %> name="ActiveFrom" id="ActiveFrom" value="<%= GetActiveFrom %>" />
         <label>
            Class Room
        </label>
        <select type="text" name="ClassRoom" id="ClassRoomId">
            <%= GetClassroomList()%>
        </select>
        <label>
            Teacher
        </label>
        <select type="text" name="Teacher" id="TeacherId">
            <%= GetTeacherList()%>
        </select>
    </fieldset>
</div>
</form>
