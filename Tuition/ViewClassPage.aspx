﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewClassPage.aspx.cs" Inherits="Tuition_ViewClassPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {

            refreshCurrentTab = function () {
                var selected = $tabs.tabs('option', 'selected');
                $tabs.tabs("load", selected);
            }


            refreshPage = function () {
                location.reload();
            }

            $(function () {
                $("#dvAccordion").accordion();
            });

            var $tabs = $("#tabs").tabs({
                spinner: 'Retrieving data...',
                load: function (event, ui) { createPopUp(); $("table").tablesorter(); },
                ajaxOptions: {
                    contentType: "application/json; charset=utf-8",
                    error: function (xhr, status, index, anchor) {
                        $(anchor.hash).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo.");
                    },
                    dataFilter: function (result) {
                        this.dataTypes = ['html']
                        var data = $.parseJSON(result);
                        return data.d;
                    }
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Class View Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div class="resultscontainer">
        <table id="matrix-table-a">
            <tbody>
                <tr>
                    <th scope="col">
                        <label>
                            Class Id
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="ClassId" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Label
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Label" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Level
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Level" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Teacher
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Teacher" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Dates
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Dates" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Room
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="Room" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <th scope="col">
                        <label>
                            Tuition Times
                        </label>
                    </th>
                    <td style="text-align: left">
                        <asp:Label ID="DailyStartTime1" runat="server">
                        </asp:Label>
                        to
                        <asp:Label ID="DailyEndTime1" runat="server">
                        </asp:Label>
                    </td>
                    <th>
                        <label>
                            Is Closed
                        </label>
                    </th>
                    <td>
                        <asp:Label ID="isClosed" runat="server">
                        </asp:Label>
                    </td>
                    <th scope="col">
                        <label>
                            Actions
                        </label>
                    </th>
                    <td style="text-align: center">
                        <a title="Edit Class" href="#" onclick="loadEditArrayControlWithCallback('#dialog-form','ViewClassPage.aspx','Tuition/AddClassControl.ascx',{'ClassId':<%=_classId %>},refreshPage)">
                            <img src="../Content/img/actions/edit.png"></a>
                        &nbsp;
                        <a title="Print Door Info" href="<%= ReportPath("AcademicReports","Academic_RoomTeacherLabel","HTML4.0", new Dictionary<string, object> { {"ClassId",_classId}, {"ModellingDate", _modelDate}, {"CourseTypeId", 1}, {"BuildingId", 1}, {"IsClosed", false}}) %>">
                                <img src="../Content/img/actions/printer.png" />
                        </a>
                        &nbsp;
                        <a title="Print Certificates" href="<%= ReportPath("AcademicReports", "Academic_ClassStudentCert", "PDF", new Dictionary<string, object> { {"ClassId", _classId} }) %>">
                            <img src="../Content/img/actions/report-paper.png" />
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Current Students</a></li>
                <li><a href="ViewClassPage.aspx/LoadClassInfos?id=<%= _classId %>">Class Info</a></li>
                <li><a href="ViewClassPage.aspx/LoadClassTransfers?id=<%= _classId %>">Class Transfers</a></li>
                <li><a href="ViewClassPage.aspx/LoadStudentSchedules?id=<%= _classId %>">Student Transfer</a></li>
                <li><a href="ViewClassPage.aspx/LoadClassSchedules?id=<%= _classId %>">Class Schedules</a></li>
                <li><a href="ViewClassPage.aspx/LoadMonthLessonPlans?id=<%= _classId %>">Lesson Plans this Month</a></li>
                <li><a href="ViewClassPage.aspx/LoadNewLessonPlans?id=<%= _classId %>">All Lesson Plans</a></li>
            </ul>
            <div id="tabs-1">
                <table id="box-table-a" class="tablesorter">
                    <thead>
                        <tr>
                            <th scope="col">
                                Name
                            </th>
                            <th scope="col">
                                Date Started
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <asp:Repeater ID="currentStudents" runat="server" EnableViewState="True">
                                <ItemTemplate>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "FirstName")%>
                                        <%# DataBinder.Eval(Container.DataItem, "SurName")%>
                                    </td>
                                    <td style="text-align: left">
                                        <%# DataBinder.Eval(Container.DataItem, "StartDate","{0:D}")%>
                                    </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
