﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using Pace.DataAccess.Tuition;
using System.Data.Entity.Core;

public partial class Tuition_TeacherReservationPage : BaseTuitionPage
{

    private DateTime _modellingDate;
    private DateTime _maxDate = DateTime.Now.AddMonths(6);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            _modellingDate = DateTime.Now;
            BuildBookings();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    #region Private Methods

    private object GetTypes()
    {
        using (Pace.DataAccess.Contacts.ContactsEntities entity = new Pace.DataAccess.Contacts.ContactsEntities())
        {
           return entity.Xlk_Type.Select(p=> new { p.TypeId, Description =p.Xlk_Category.Description + ":- " + p.Description }).ToList();
        }
    }

    protected void Click_ReBuild(object sender, EventArgs e)
    {
        BuildBookings();
    }

    protected void BuildBookings()
    {
        _modellingDate = DateTime.Now;

        using (Pace.DataAccess.Tuition.TuitionEntities entity = new Pace.DataAccess.Tuition.TuitionEntities())
        {
            var bookingQuery = from g in entity.Class
                               where g.StartDate > _modellingDate && g.Groups.Count > 0 && g.ClassRoom.Xlk_Building.CampusId == GetCampusId
                               group g by g.Groups.FirstOrDefault() into groupClass
                               orderby groupClass.Key.ArrivalDate
                               select new
                               {
                                   groupClass.Key.GroupId,
                                   groupClass.Key.GroupName,
                                   GroupClass = (from x in groupClass
                                                 select new
                                                     {

                                                         ClassId = x.ClassId,
                                                         CourseType = x.Xlk_CourseType.Description,
                                                         ProgrammeType = x.Xlk_ProgrammeType.Description,
                                                         TuitionLevel = x.TuitionLevel.Description,
                                                         Classroom = x.ClassRoom.Label,
                                                         FromDate = x.StartDate,
                                                         ToDate = x.EndDate
                                                     })
                               };

            groupresult.DataSource = bookingQuery;
            groupresult.DataBind();
        }
    }

    #endregion

    #region ScriptMethods

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> FindTeachers(string contactsearch)
    {
        StringBuilder sb = new StringBuilder();
        Dictionary<int, int> familyIds = new Dictionary<int, int>();

        try
        {
            using (Pace.DataAccess.Tuition.TuitionEntities entity = new Pace.DataAccess.Tuition.TuitionEntities())
            {

                var contactsQuery = (from f in entity.Teacher
                                     where f.Active.Value == true
                                     select f);

                contactsQuery = contactsQuery.OrderBy(e => e.Name);


                if (!string.IsNullOrEmpty(contactsearch))
                    contactsQuery = contactsQuery.Where(e => e.Name.Contains(contactsearch));

                if (contactsQuery != null && contactsQuery.ToList().Count() > 0)
                {
                    foreach (var item in contactsQuery)
                    {
                        sb.AppendFormat("<div id=\"{1}\" name=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{0}<a href=\"#\" class=\"highlighter\"  onclick=\"ShowTeacher({1})\"><img src=\"../Content/img/actions/orange_button.png\" alt=\"Highlight Teacher\" style=\"float:right\"/></a></div>", item.Name, item.TeacherId);
                    }
                }
                return new AjaxResponse<string>(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse<string> BuildTeachers(object classid)
    {
        int _classId = Convert.ToInt32(classid);
        try
        {
            using (Pace.DataAccess.Tuition.TuitionEntities entity = new Pace.DataAccess.Tuition.TuitionEntities())
            {
                var staff = (from f in entity.Class
                             where f.ClassId == _classId
                             select f.Teacher).FirstOrDefault();
                int i = 1;

                if (staff != null)
                    return new AjaxResponse<string>(string.Format("<div id=\"f_{1}-g_{2}\" name=\"{1}\" class=\"draggable ui-state-default ui-draggable\">{3}. {0}<a href=\"#\" onclick=\"DeleteTeacher({1},{2})\"><img src=\"../Content/img/actions/delete.png\" alt=\"Remove from Group\" style=\"float:right\" id=\"DeleteGrouping\"/></a><a href=\"#\" class=\"highlighter\" onclick=\"ShowTeacher({1})\"><img src=\"../Content/img/actions/orange_button.png\" alt=\"Highlight Family\" style=\"float:right\" /></a></div>", staff.Name, staff.TeacherId, _classId, i++));
            }
            return new AjaxResponse<string>(string.Empty);
        }
        catch (Exception ex)
        {
            return new AjaxResponse<string>(ex);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AssignTeacher(short teacherid, int classid)
    {
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                var _class = (from g in entity.Class
                              where g.ClassId == classid
                              select g).FirstOrDefault();

                _class.TeacherReference.EntityKey = new EntityKey(entity.DefaultContainerName + ".Teacher", "TeacherId", teacherid);


                if (entity.SaveChanges() > 0)
                    return string.Empty;
                else
                    return "Could not save this change";
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse DeleteTeacher(short teacherid, int classid)
    {
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                var _class = (from g in entity.Class
                                    where g.Teacher.TeacherId == teacherid && g.ClassId == classid
                                select g).FirstOrDefault();

                if (_class != null)
                    _class.TeacherReference.Value = null;


                if (entity.SaveChanges() > 0)
                    return new AjaxResponse();
                else
                    return new AjaxResponse(new Exception( "Could not remove teacher"));
            }
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    #endregion

}