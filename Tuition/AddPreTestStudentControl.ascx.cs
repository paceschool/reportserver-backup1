﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Tuition;
using System.Text;
using System.Web.Services;

public partial class Tuition_AddPreTestStudentControl : BaseTuitionControl
{
    protected Int32 StudentId;
    protected Int32 PreTestId;
    protected PreTest _test;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (GetValue<Int32?>("PreTestId").HasValue)
            {
                Int32 PreTestId = GetValue<Int32>("PreTestId");
                _test = (from p in entity.PreTests.Include("Student").Include("ExamDate").Include("ExamDate.Exam")
                         where p.PreTestId == PreTestId
                         select p).FirstOrDefault();
            }
        }
    }

    protected string GetPreTestId
    {
        get
        {
            if (_test != null)
                return _test.PreTestId.ToString();

            if (Parameters.ContainsKey("PreTestId"))
                return Parameters["PreTestId"].ToString();

            return "0";
        }
    }

    protected string GetScore
    {
        get
        {
            if (_test != null && _test.Score.HasValue)
                return _test.Score.Value.ToString();

            return null;
        }
    }

    protected string GetStudent()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_test != null && _test.Student != null) ? _test.Student.StudentId : (Int32?)null;

        foreach (Student item in LoadStudents())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StudentId, item.FirstName + " " + item.SurName, (_current.HasValue && item.StudentId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetExamDates()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_test != null && _test.ExamDate != null) ? _test.ExamDateId : (GetValue<Int32?>("ExamDateId").HasValue) ? GetValue<Int32>("ExamDateId") : (Int32?)null;

        foreach (ExamDates item in LoadExamDates())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExamDateId, item.ExamDate, (_current.HasValue && item.ExamDateId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }
}