﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TeacherReservationPage.aspx.cs" Inherits="Tuition_TeacherReservationPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {

            getTeachers = function (classid) {
                $.ajax({
                    type: "POST",
                    url: "TeacherReservationPage.aspx/BuildTeachers",
                    data: JSON.stringify({ classid: classid }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                $('#Li_' + classid).html(msg.d.Payload);
                            }

                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            }

            displayActions = function () {

                $('.draggable').mouseover(function () {
                    $(this).find('a:.highlighter').show();
                });

                $('.draggable').mouseout(function () {
                    $(this).find('a:.highlighter').hide();
                });

                createPopUp();
            };

            ShowTeacher = function (id) {
                $('div.orange').removeClass('orange');
                $('div[name=' + id + ']').addClass('orange');
            }

            displayActions();

            FindTeachers = function (contactsearch) {
                $.ajax({
                    type: "POST",
                    url: "TeacherReservationPage.aspx/FindTeachers",
                    data: JSON.stringify({ contactsearch: contactsearch }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                $('#filteredteachers').html(msg.d.Payload);
                                displayActions();
                            }

                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            }

            $(".draggable").draggable({
                helper: "clone",
                revert: "invalid",
                containment: ".resultscontainer",
                highlight: ('div', 'highlight-selected', 'mousedown'),
                scroll: false
            });

            $(".droppable").droppable({
                accept: ".draggable",
                activeClass: "ui-state-hover",
                hoverClass: "ui-state-active",
                drop: function (event, ui) {
                    $(this).addClass("ui-state-highlight");
                    AssignTeacher(ui.draggable.attr('id'), this.id)
                }
            });

            $(function () {
                $("#assigned").jScroll();
            });

            DeleteTeacher = function (teacherid, classid) {
                $("#dialog-message").html('<p><span class="ui-icon ui-icon-alert"></span>The reservation will be permanently deleted and cannot be recovered. Are you sure?</p>')
                    .dialog({
                        autoOpen: true,
                        resizable: true,
                        width: '400px',
                        height: 'auto',
                        modal: true,
                        buttons: {
                            "Delete item": function () {
                                $.ajax({
                                    type: "POST",
                                    url: "TeacherReservationPage.aspx/DeleteTeacher",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({ teacherid: teacherid, classid: classid }),
                                    success: function (msg) {
                                        if (msg.d != null) {
                                            if (msg.d.Success) {
                                                getTeachers(classid);
                                            }
                                            else
                                                alert(msg.d.ErrorMessage);
                                        }
                                        else
                                            alert("No Response, please contact admin to check!");
                                    }
                                });
                                $(this).dialog("close");
                            },
                            Cancel: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
            }


            $('.watcher').change(function () {
                FindTeachers($('#<%= teachersearch.ClientID %>').val());
            });

            showPrintOptions = function () {
                $("#dialog-print").dialog("open");
            };

            $("#dialog-print").css({ 'background-color': '#E0FFFF' }).dialog({
                autoOpen: false,
                modal: true
            });

            AssignTeacher = function (teacherid, classid) {
                $.ajax({
                    type: "POST",
                    url: "TeacherReservationPage.aspx/AssignTeacher",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ teacherid: teacherid, classid: classid }),
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                getTeachers(classid);
                            }
                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            };

        });
    </script>
    <style type="text/css">
        .highlighter
        {
            display: none;
        }
        .filterinput
        {
            width: 80%;
        }
        .orange
        {
            color: White;
            background: Orange;
        }
        #disconnect
        {
            float: left;
        }
        .classtd
        {
            border: 1px solid #AED0EA;
            vertical-align: top;
        }
        .classlist
        {
            float: left;
        }
        .classlist div
        {
            font-weight: bold;
        }
        #notassigned
        {
            float: left;
        }
        .highlight-selected
        {
            background-color: Blue;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Class Teacher Planner Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontianer">
    </div>
    <div class="resultscontainer" style="width: 3000px;">
        <div id="notassigned" style="width: 300px;">
            <table id="ctl00_maincontent_results" cellspacing="0" border="0" style="border-collapse: collapse;">
                <tbody>
                    <tr>
                        <td class="classtd">
                            <ul class="classlist" style="float: left; list-style-type: none; font-style: italic;"
                                id="familyhighlight">
                                <div id="header">
                                    <li>
                                        <asp:Label runat="server" Text="Filter 3:">
                                        </asp:Label>
                                        <asp:TextBox ID="teachersearch" runat="server" CssClass="watcher filterinput" ToolTip="Filter by text">
                                        </asp:TextBox>
                                    </li>
                                    <li>
                                        <asp:Label runat="server" Text="Clear">
                                        </asp:Label>
                                        <asp:ImageButton ImageUrl="~/Content/img/actions/user_delete.png" ID="clearfilter"
                                            runat="server" Text="Clear Filters" />
                                    </li>
                                </div>
                                <li id="filteredteachers">
                                    <%= FindTeachers(teachersearch.Text).Payload%>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="assigned" style="width: 80%; float: inherit">
            <asp:DataList ID="groupresult" runat="server" RepeatDirection="Horizontal" CellSpacing="2"
                CellPadding="2">
                <HeaderStyle Font-Names="Verdana" Font-Size="10pt" HorizontalAlign="center" Font-Bold="True" />
                 <ItemStyle CssClass="classtd" />
                <ItemTemplate>
                    <div>
                     <%# GroupLink(DataBinder.Eval(Container.DataItem, "GroupId"), "~/Groups/ViewGroupPage.aspx", CreateName(DataBinder.Eval(Container.DataItem, "GroupName"), "")) %>
                        <asp:DataList ID="results" runat="server" RepeatDirection="Vertical" CellSpacing="2"
                            DataSource='<%#DataBinder.Eval(Container.DataItem, "GroupClass")%>' CellPadding="2">
                            <HeaderStyle Font-Names="Verdana" Font-Size="10pt" HorizontalAlign="center" Font-Bold="True" />
                            <ItemStyle CssClass="classtd" />
                            <ItemTemplate>
                                <ul class="classlist" style="float: left; list-style-type: none; width: 210px;">
                                    <div id="<%#DataBinder.Eval(Container.DataItem, "ClassId")%>" class="droppable">
                                        <li style="text-align: center">
                                            <%#DataBinder.Eval(Container.DataItem, "CourseType")%>
                                            /<%# DataBinder.Eval(Container.DataItem, "ProgrammeType")%></li>
                                        <li style="text-align: center">
                                            <%#DataBinder.Eval(Container.DataItem, "FromDate", "{0:dd MMM yyyy}")%>/<%#DataBinder.Eval(Container.DataItem, "ToDate", "{0:dd MMM yyyy}")%></li>
                                        <li style="text-align: center">
                                            <%#DataBinder.Eval(Container.DataItem, "TuitionLevel")%>
                                        </li>
                                        <li style="text-align: center">
                                            <%#DataBinder.Eval(Container.DataItem, "Classroom")%>
                                        </li>
                                        <li id="Li_<%#DataBinder.Eval(Container.DataItem, "ClassId")%>">
                                            <%# BuildTeachers(DataBinder.Eval(Container.DataItem, "ClassId")).Payload%>
                                        </li>
                                    </div>
                                </ul>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:DataList>
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
    </div>
</asp:Content>
