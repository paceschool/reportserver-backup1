﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddExamControl.ascx.cs" 
Inherits="Tuition_AddExamControl" %>

<script type="text/javascript">
    $(function () {
        getForm = function () {
            return $("#addExamDate");
        }

        getTargetUrl = function () {
            return '<%= ToVirtual("~/Tuition/ViewClassPage.aspx","SaveExamDate") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }

        $(".datetext").datepicker({ dateFormat: 'dd/mm/yy' });

    });
    
</script>

<p id="message" style="display: none;">
    All fields must be completed</p>
<form id="addExamDate" name="addExamDate" method="post" action="ExamsPage.aspx/SaveExamDate">
    <div class="inputArea">
        <fieldset>
            <input type="hidden" id="ExamDateId" value="<%= GetExamDateId %>" />
            <label>
                Exam
            </label>
            <select name="Exam" id="ExamId">
                <%= GetExams() %>
            </select>
            <label>
                Exam Type
            </label>
            <select name="Xlk_ExamType" id="ExamTypeId">
                <%= GetExamTypes() %>
            </select>
            <label for="ExamDate">
                Exam Date
            </label>
            <input class="datetext" type="datetext" name="ExamDate" id="ExamDate" value="<%= GetExamDate %>" />
            <label for="RegistrationDate">
                Registration Date
            </label>
            <input class="datetext" type="datetext" name="RegistrationDate" id="RegistrationDate" value="<%= GetRegistrationDate %>" />
            <label for="WindowStart">
                Speaking Test Window Start
            </label>
            <input class="datetext" type="datetext" name="StartDate" id="StartDate" value="<%= GetStartDate %>" />
            <label for="WindowEnd">
                Speaking Test Window End
            </label>
            <input class="datetext" type="datetext" name="EndDate" id="EndDate" value="<%= GetEndDate %>" />
            <label for="StudentDeparture">
                Earliest Student Departure Date
            </label>
            <input class="datetext" type="datetext" name="ExpecetedStudentDepartureDate" id="ExpecetedStudentDepartureDate" value="<%= GetDepartureDate %>" />
            <label for="Confirmed">
                Exam Date Confirmed
            </label>
            <input type="checkbox" id="ExamDateConfirmed" value="true" "<%= GetConfirmed %>" />
        </fieldset>
    </div>
</form>