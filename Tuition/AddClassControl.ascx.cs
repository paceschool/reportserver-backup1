﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Pace.DataAccess.Tuition;

public partial class Tuition_AddClassTransferControl : BaseTuitionControl
{
    protected Int32? _classId;
    protected Class _class;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (GetValue<Int32?>("ClassId").HasValue)
            {
                _classId = GetValue<Int32>("ClassId");

                _class = (from n in entity.Class.Include("Xlk_CourseType").Include("Xlk_ProgrammeType").Include("TuitionLevel").Include("ClassRoom").Include("Teacher")
                          where n.ClassId == _classId
                                   select n).FirstOrDefault();
            }
        }
    }

    protected string GetCourseTypeList()
    {

        StringBuilder sb = new StringBuilder();

        short? _current = (_class != null && _class.Xlk_CourseType != null) ? _class.Xlk_CourseType.CourseTypeId : (short?)null;

        if (_class != null && _class.Xlk_ProgrammeType != null)
        {
            foreach (Xlk_CourseType item in LoadCourseTypes().Where(x => x.Xlk_ProgrammeType.Any(p => p.ProgrammeTypeId == _class.Xlk_ProgrammeType.ProgrammeTypeId)))
            {
                sb.AppendFormat("<option  {2} value={0}>{1}</option>", item.CourseTypeId, item.Description, (_current.HasValue && item.CourseTypeId == _current) ? "selected" : string.Empty);

            }
        }
        else
        {
            foreach (Xlk_CourseType item in this.LoadCourseTypes())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.CourseTypeId, item.Description, (_current.HasValue && item.CourseTypeId == _current) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();

    }

    protected string GetClassId
    {
        get
        {
            if (_class != null)
                return _class.ClassId.ToString();

            if (_classId.HasValue)
                return _classId.Value.ToString();


            return "0";
        }
    }

    protected string GetProgrammeTypeList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_class != null && _class.Xlk_ProgrammeType != null) ? _class.Xlk_ProgrammeType.ProgrammeTypeId : (byte?)null;


        foreach (Xlk_ProgrammeType item in this.LoadProgrammeTypes())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ProgrammeTypeId, item.Description, (_current.HasValue && item.ProgrammeTypeId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetTuitionLevelList()
    {
        StringBuilder sb = new StringBuilder();

        byte? _current = (_class != null && _class.TuitionLevel != null) ? _class.TuitionLevel.TuitionLevelId : (byte?)null;


        foreach (TuitionLevel item in this.LoadTuitionLevels())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TuitionLevelId, item.Label, (_current.HasValue && item.TuitionLevelId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetClassroomList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_class != null && _class.ClassRoom != null) ? _class.ClassRoom.ClassroomId : (short?)null;


        foreach (ClassRoom item in this.LoadClassRooms())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ClassroomId, item.Label, (_current.HasValue && item.ClassroomId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetTeacherList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_class != null && _class.Teacher != null) ? _class.Teacher.TeacherId : (short?)null;


        foreach (Teacher item in this.LoadTeachers().OrderBy(x=>x.Name))
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TeacherId, item.Name, (_current.HasValue && item.TeacherId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetIsClosed
    {
        get
        {
            if (_class != null && _class.IsClosed)
                return "checked";


            return null;
        }
    }

    protected string GetDailyEndTime
    {
        get
        {
            if (_class != null && _class.DailyEndTime.HasValue)
                return _class.DailyEndTime.Value.ToString(@"hh\:mm");


            return null;
        }
    }

    protected string GetDailyStartTime
    {
        get
        {

            if (_class != null && _class.DailyStartTime.HasValue)
                return _class.DailyStartTime.Value.ToString(@"hh\:mm");

            return null;
        }
    }

    protected string GetEndDate
    {
        get
        {
            if (_class != null)
                return _class.EndDate.ToString("dd/MM/yy");


            return null;
        }
    }

    protected string GetStartDate
    {
        get
        {
            if (_class != null)
                return _class.StartDate.ToString("dd/MM/yy");


            return null;
        }
    }

    protected string GetLabel
    {
        get
        {
            if (_class != null)
                return _class.Label;


            return null;
        }
    }

    protected string GetClassList()
    {
        StringBuilder sb = new StringBuilder();

        foreach (Pace.DataAccess.Tuition.Class _class in LoadActiveAndFutureClassList())
        {
            sb.AppendFormat("<option value={0}>{1} StartDate:{2} EndDate:{3}</option>", _class.ClassId,_class.Label,_class.StartDate.ToString("dd MMM yy"),_class.EndDate.ToString("dd MMM yy"));
        }

        return sb.ToString();
    }

    private List<Class> LoadActiveAndFutureClassList()
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            var classQuery = from c in entity.Class
                             where (c.StartDate <= DateTime.Now && c.EndDate >= DateTime.Now) || (c.StartDate >= DateTime.Now)
                             select c;

            return classQuery.ToList();
        }
    }
}
