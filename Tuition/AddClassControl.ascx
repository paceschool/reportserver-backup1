﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddClassControl.ascx.cs"
    Inherits="Tuition_AddClassTransferControl" %>
<script type="text/javascript">
    $(function () {

        getForm = function () {
            return $("#addclasstransfer");
        }

        $("#StartDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#EndDate").datepicker({ dateFormat: 'dd/mm/yy' });


        getTargetUrl = function () {
            return '<%= ToVirtual("~/Tuition/ViewClassPage.aspx","SaveClass") %>';
        }

        getLocalRules = function () {
            return {};
        }

        getLocalMessages = function () {
            return {};
        }
    });
</script>
<p id="message" style="display: none;">
    All fields must be completed
</p>
<form id="addclasstransfer" name="addclasstransfer" method="post" action="ViewClassPage.aspx/SaveClass">
<div class="inputArea">
    <fieldset>
        <input type="hidden" id="ClassId" value="<%= GetClassId %>" />
        <label>
            Label
        </label>
        <input type="text" id="Label" value="<%= GetLabel %>" />
        <label for="StartDate">
            Start Date</label>
        <input type="datetext" name="StartDate" id="StartDate" value="<%= GetStartDate %>" />
        <label for="EndDate">
            End Date</label>
        <input type="datetext" name="EndDate" id="EndDate" value="<%= GetEndDate %>" />
        <label>
            DailyStartTime
        </label>
        <input type="text" id="DailyStartTime" value="<%= GetDailyStartTime %>" />
        <label>
            DailyEndTime
        </label>
        <input type="text" id="DailyEndTime" value="<%= GetDailyEndTime %>" />
        <label>
            IsClosed
        </label>  
        <input type="checkbox" id="IsClosed" <%= GetIsClosed %> value="true" />
    </fieldset>
    <fieldset>
        <label>
            Programme Type
        </label>
        <select type="text" name="Xlk_ProgrammeType" id="ProgrammeTypeId">
            <%= GetProgrammeTypeList()%>
        </select>
        <label>
            Course
        </label>
        <select type="text" name="Xlk_CourseType" id="CourseTypeId">
            <%= GetCourseTypeList()%>
        </select>
        <label>
            Tuition Level
        </label>
        <select type="text" name="TuitionLevel" id="TuitionLevelId">
            <%= GetTuitionLevelList()%>
        </select>
        <label>
            Class Room
        </label>
        <select type="text" name="ClassRoom" id="ClassRoomId">
            <%= GetClassroomList()%>
        </select>
        <label>
            Teacher
        </label>
        <select type="text" name="Teacher" id="TeacherId">
            <%= GetTeacherList()%>
        </select>
    </fieldset>
</div>
</form>
