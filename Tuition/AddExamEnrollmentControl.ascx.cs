﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Tuition;
using System.Text;
using System.Web.Services;

public partial class Tuition_AddExamEnrollment : BaseTuitionControl
{
    protected Int32 _studentExamId;
    protected Int32 _studentId;
    protected Int32 _examDateId;
    protected StudentExam _exam;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (GetValue<Int32?>("StudentExamId").HasValue)
            {
                Int32 _studentExamId = GetValue<Int32>("StudentExamId");
                _exam = (from x in entity.StudentExam.Include("Student").Include("ExamDates")
                         where x.StudentExamId == _studentExamId
                         select x).FirstOrDefault();
            }

        }
    }

    protected string GetStudentName()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_exam != null && _exam.Student != null) ? _exam.Student.StudentId : (Int32?)null;

        if (_current.HasValue)
            sb.AppendFormat("<option {2} value={0}>{1}</option>", _exam.Student.StudentId, _exam.Student.FirstName + " " + _exam.Student.SurName, "selected");
        else
        {

            foreach (Student item in LoadStudents())
            {
                sb.AppendFormat("<option {2} value={0}>{1}</option>", item.StudentId, item.FirstName + " " + item.SurName, (_current.HasValue && item.StudentId == _current) ? "selected" : string.Empty);
            }
        }

        return sb.ToString();
    }

    protected string GetExamDatesList()
    {
        StringBuilder sb = new StringBuilder();

        Int32? _current = (_exam != null && _exam.ExamDates != null) ? _exam.ExamDates.ExamDateId : (GetValue<Int32?>("ExamDateId").HasValue) ?GetValue<Int32>("ExamDateId") : (Int32?)null;

        foreach (ExamDates item in LoadExamDates())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ExamDateId, item.ExamDate.Value.ToLongDateString(), (_current.HasValue && item.ExamDateId == _current) ? "selected" : string.Empty);
        }
        return sb.ToString();
    }

    protected string GetStudentId
    {
        get
        {
            if (_exam != null)
                return _exam.Student.StudentId.ToString();

            if (Parameters.ContainsKey("StudentId"))
                return Parameters["StudentId"].ToString();

            return "0";
        }
    }

    protected string GetExamDateId
    {
        get
        {
            if (_exam != null)
                return _exam.ExamDates.ExamDateId.ToString();

            if (Parameters.ContainsKey("ExamDateId"))
                return Parameters["ExamDateId"].ToString();

            return "0";
        }
    }

    protected string GetStudentExamId
    {
        get
        {
            if (_exam != null)
                return _exam.StudentExamId.ToString();

            if (Parameters.ContainsKey("StudentExamId"))
                return Parameters["StudentExamId"].ToString();

            return "0";
        }
    }

    protected string GetExamDate
    {
        get
        {
            if (_exam != null)
                return _exam.ExamDates.ExamDate.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetDateRegistered
    {
        get
        {
            if (_exam != null && _exam.DateRegistered.HasValue)
                return _exam.DateRegistered.Value.ToString("dd/MM/yy");

            return null;
        }
    }

    protected string GetPaidValue
    {
        get
        {
            if (_exam != null && (_exam.Paid.HasValue && _exam.Paid.Value || _exam.Student.PrepaidExam))
                return "checked";

            return string.Empty;
        }
    }

    protected string GetPaidAmount
    {
        get
        {
            if (_exam != null)
                return _exam.Amount.ToString();

            return null;
        }
    }

    protected string GetAsIndividual
    {
        get
        {
            if (_exam != null && _exam.AsIndividual.HasValue && _exam.AsIndividual.Value)
                return "checked";

            return string.Empty;
        }
    }

    protected string GetResultScore
    {
        get
        {
            if (_exam != null && _exam.ResultScore != null)
                return _exam.ResultScore.ToString();

            return null;
        }
    }
}