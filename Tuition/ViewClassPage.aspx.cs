﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Pace.DataAccess.Tuition;

using System.Data;
using System.Text;
using System.Globalization;
using System.Web.Script.Services;

public partial class Tuition_ViewClassPage : BaseTuitionPage
{
    protected Int32 _classId;
    protected DateTime _modelDate = DateTime.Parse(DateTime.Now.ToString("dd MMM yyyy"));

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["ClassId"]))
            _classId = Convert.ToInt32(Request["ClassId"]);

        if (!Page.IsPostBack)
        {

            DisplayClass(LoadClass(new TuitionEntities(), _classId));
            LoadStudentList(_classId);
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    private void LoadStudentList(Int32 classId) //Display Class History for Student
    {
        Class _class;
        DateTime modellingdate = DateTime.Now;

        using (TuitionEntities entity = new TuitionEntities())
        {

            _class = (from c in entity.Class
                      where c.ClassId == classId
                      select c).FirstOrDefault();

            if (_class.IsClosed)
                modellingdate = _class.StartDate;

            var ids = entity.FindClassStudents(modellingdate, _classId).ToList();

            currentStudents.DataSource = (from e in entity.Enrollment.Include("Student")
                                          where ids.Contains(e.EnrollmentId) && e.Student.CampusId == GetCampusId
                                          select new { StartDate = e.StartDate, e.Student.FirstName, e.Student.SurName }).OrderBy(s => s.FirstName).ToList();


            currentStudents.DataBind();
        }

    }

    private Class LoadClass(TuitionEntities entity, Int32 classId)
    {
        IQueryable<Class> classQuery = from c in entity.Class.Include("Teacher").Include("ClassRoom").Include("Xlk_ProgrammeType").Include("Xlk_CourseType").Include("TuitionLevel")
                                       where c.ClassId == classId
                                       select c;

        if (classQuery.ToList().Count() > 0)
            return classQuery.ToList().First();

        return null;
    }

    private void DisplayClass(Class classinstance)
    {
        if (classinstance != null)
        {
            ClassId.Text = classinstance.ClassId.ToString();
            Label.Text = classinstance.Label;
            if (classinstance.TuitionLevel != null)
                Level.Text = classinstance.TuitionLevel.Description;
            DailyStartTime1.Text = classinstance.DailyStartTime.ToString();
            DailyEndTime1.Text = classinstance.DailyEndTime.ToString();
            if (classinstance.Teacher != null)
                Teacher.Text = classinstance.Teacher.Name;
            if (classinstance.ClassRoom != null)
                Room.Text = classinstance.ClassRoom.Label;
            isClosed.Text = (classinstance.IsClosed) ? "Yes" : "no";
            Dates.Text = string.Format("{0}&nbsp;-&nbsp;{1}", classinstance.StartDate.ToString("dd MMM yy"), classinstance.EndDate.ToString("dd MMM yy"));
        }
    }

    private Class GetClass(TuitionEntities entity)
    {
        return LoadClass(entity, _classId);
    }

    private void SaveClass(TuitionEntities entity, Class classinstance)
    {
        if (entity.SaveChanges() > 0)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
    }


    #region Javascript Enabled Methods

    

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadClassInfos(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewClassPage.aspx','Tuition/AddClassInfoControl.ascx',{ ClassId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Info</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">ClassId</th><th scope=\"col\">Active Date</th><th scope=\"col\">Teacher</th><th scope=\"col\">Classroom</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.ClassInfoes.Include("Class").Include("Teacher").Include("ClassRoom")
                            where n.ClassId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (ClassInfo classInfo in query.ToList())
                    {
                        if (classInfo.ActiveFrom <= DateTime.Now || classInfo.ActiveFrom == classInfo.Class.StartDate)
                            sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: center\"></td></tr>", classInfo.ClassId, classInfo.ActiveFrom.ToString("dd/MM/yyyy"), classInfo.Teacher.Name, classInfo.ClassRoom.Label);
                        else
                            sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: center\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewClassPage.aspx','Tuition/AddClassInfoControl.ascx',{{ ClassId: {0}, ActiveFrom: '{1}' }} )\"><img src=\"../Content/img/actions/edit.png\" /></a>&nbsp;<a title=\"Delete Class Info\" href=\"#\" onclick=\"deleteParentChildObjectFromAjaxTab('ViewClassPage.aspx','ClassInfo',{0}, '{1}')\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", classInfo.ClassId, classInfo.ActiveFrom.ToString("dd/MM/yyyy"), classInfo.Teacher.Name, classInfo.ClassRoom.Label);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"7\">No Transfer to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadClassTransfers(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewClassPage.aspx','Tuition/AddClassTransferControl.ascx',{ ClassId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Transfer</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Class Transfer Id</th><th scope=\"col\">From Class</th><th scope=\"col\">To Class</th><th scope=\"col\">Action Date</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.ClassTransfer.Include("FromClass").Include("ToClass")
                            where n.FromClass.ClassId == id || n.ToClass.ClassId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (ClassTransfer classTransfer in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td> <td style=\"text-align: left\"><a href=\"ViewClassPage.aspx?ClassId={4}\" >{1} {4}</a></td> <td style=\"text-align: left\"><a href=\"ViewClassPage.aspx?ClassId={5}\" >{2} {5}</a></td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: center\"><a title=\"Delete Class Transfer\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewClassPage.aspx','ClassTransfer',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", classTransfer.ClassTransferId, classTransfer.FromClass.Label, classTransfer.ToClass.Label, classTransfer.ActionDate.ToString("D"), classTransfer.FromClass.ClassId, classTransfer.ToClass.ClassId);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"7\">No Transfer to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadStudentSchedules(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Enrollment Id</th><th scope=\"col\">Student Name</th><th scope=\"col\">Action Date</th><th scope=\"col\">From Class</th><th scope=\"col\">To Class</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.ClassSchedule.Include("Class").Include("Enrollment.Student").Include("Enrollment.Class")
                            where n.Class.ClassId == id && !n.DateProcessed.HasValue
                            select n;

                if (query.Count() > 0)
                {
                    foreach (ClassSchedule classSchedule in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{2}</td> <td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: center\"><a title=\"Delete Enrollment\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewGroupPage.aspx','GroupEnrollment',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", classSchedule.EnrollmentId, classSchedule.Enrollment.Student.StudentId, BasePage.StudentLink(classSchedule.Enrollment.Student.StudentId, "~/Enrollments/ViewStudentPage.aspx", BasePage.CreateName(classSchedule.Enrollment.Student.FirstName, classSchedule.Enrollment.Student.SurName)), classSchedule.StartDate.ToString("D"), (classSchedule.Class.ClassId == id) ? (classSchedule.Enrollment.Class != null) ? classSchedule.Enrollment.Class.Label : "New Enrollment" : classSchedule.Class.Label, (classSchedule.Class.ClassId == id) ? classSchedule.Class.Label : classSchedule.Enrollment.Class.Label);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"7\">No Schedules to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string LoadClassSchedules(int id)
    {
        try
        {
            StringBuilder sb = new StringBuilder("<span style=\"float:right\"><a href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewClassPage.aspx','Tuition/AddScheduleClassControl.ascx',{ ClassId:" + id + "})\"><img src=\"../Content/img/actions/add.png\" />Add New Schedule</a></span><table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Class Schedule Id</th><th scope=\"col\">From Date</th><th scope=\"col\">To Date</th><th scope=\"col\">Start Time</th><th scope=\"col\">End Time</th><th scope=\"col\">Tuition Minutes</th><th scope=\"col\">Break Minutes</th><th scope=\"col\">Actions</th></tr></thead><tbody>");
            using (TuitionEntities entity = new TuitionEntities())
            {
                var query = from n in entity.ScheduleClasses.Include("Class")
                            where n.Class.ClassId == id
                            select n;

                if (query.Count() > 0)
                {
                    foreach (ScheduleClass scheduleClass in query.ToList())
                    {
                        sb.AppendFormat("<tr><td style=\"text-align: left\">{0}</td><td style=\"text-align: left\">{1}</td><td style=\"text-align: left\">{2}</td><td style=\"text-align: left\">{3}</td><td style=\"text-align: left\">{4}</td><td style=\"text-align: left\">{5}</td><td style=\"text-align: left\">{6}</td><td style=\"text-align: center\"><a title=\"Edit Note\" href=\"#\" onclick=\"loadEditArrayControl('#dialog-form','ViewClassPage.aspx','Tuition/AddScheduleClassControl.ascx',{{'ScheduleId':{0}}})\"><img src=\"../Content/img/actions/edit.png\"></a>&nbsp;<a title=\"Delete Class Schedule\" href=\"#\" onclick=\"deleteObjectFromAjaxTab('ViewClassPage.aspx','ScheduleClass',{0})\"><img src=\"../Content/img/actions/bin_closed.png\"></a></td></tr>", scheduleClass.ScheduleId, scheduleClass.StartDate.ToString("dd/MM/yy"), scheduleClass.EndDate.ToString("dd/MM/yy"), scheduleClass.DailyStartTime.ToString(@"hh\:mm"), scheduleClass.DailyEndTime.ToString(@"hh\:mm"), scheduleClass.TuitionMinutes, scheduleClass.BreakMinutes);
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"8\">No Transfer to Show!</td></tr>");
                }
            }
            sb.Append("</tbody></table>");

            return sb.ToString();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AjaxResponse SaveClassTransfer(ClassTransfer newObject)
    {
        try
        {
            using (TuitionEntities entity = new TuitionEntities())
            {
                ClassTransfer newclassTransfer = new ClassTransfer();

                newclassTransfer.ActionDate = newObject.ActionDate;
                newclassTransfer.FromClassReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".FromClass", newObject.FromClass);
                newclassTransfer.ToClassReference.EntityKey = entity.CreateEntityKey(entity.DefaultContainerName + ".ToClass", newObject.ToClass);
                newclassTransfer.ClassTransferId = newObject.ClassTransferId;

                if (newclassTransfer.ClassTransferId == 0)
                    entity.AddToClassTransfer(newclassTransfer);

                entity.SaveChanges();
            }
            return new AjaxResponse();
        }
        catch (Exception ex)
        {
            return new AjaxResponse(ex);
        }
    }

    #endregion

}
