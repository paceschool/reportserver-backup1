﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Pace.DataAccess.Tuition;

public partial class Tuition_AddClassTransferControl : BaseTuitionControl
{
    protected ScheduleClass _schedule;
    protected Class _class;
    protected Int32? _classId;
    protected Int32? _scheduleId;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (GetValue<Int32?>("ScheduleId").HasValue)
            {
                _scheduleId = GetValue<Int32>("ScheduleId");
                _schedule = (from x in entity.ScheduleClasses.Include("Class")
                             where x.ScheduleId == _scheduleId
                         select x).FirstOrDefault();
            }
            if (GetValue<Int32?>("ClassId").HasValue)
            {
                _classId = GetValue<Int32?>("ClassId").Value;
                _class = entity.Class.Where(c => c.ClassId == _classId).FirstOrDefault();
            }

        }
    }

    protected string GetMinDate
    {
        get
        {
            DateTime _date = DateTime.MinValue;

            if (_schedule != null)
                _date = _schedule.Class.StartDate;

            if (_class!= null)
                _date =_class.StartDate;

            return string.Format("{0},{1},{2}",_date.Year,_date.Month-1, _date.Day);
        }
    }
    protected string GetMaxDate
    {
        get
        {
            DateTime _date = DateTime.MaxValue;

            if (_schedule != null)
                _date = _schedule.Class.EndDate;

            if (_class != null)
                _date = _class.EndDate;

            return string.Format("{0},{1},{2}", _date.Year, _date.Month - 1, _date.Day);
        }
    }

    protected string GetScheduleId
    {
        get
        {
            if (_schedule != null)
                return _schedule.ScheduleId.ToString();

            if (_scheduleId.HasValue)
                return _scheduleId.Value.ToString();

            return "0";
        }
    }


    protected string GetClassId
    {
        get
        {
            if (_schedule != null)
                return _schedule.Class.ClassId.ToString();

            if (_classId.HasValue)
                return _classId.Value.ToString();

            return "0";
        }
    }


    protected string GetStartDate
    {
        get
        {
            if (_schedule != null)
                return _schedule.StartDate.ToString("dd/MM/yy");

            if (_class != null)
                return _class.StartDate.ToString("dd/MM/yy");

            return "";
        }
    }

    protected string GetEndDate
    {
        get
        {
            if (_schedule != null)
                return _schedule.EndDate.ToString("dd/MM/yy"); ;

            if (_class != null)
                return _class.EndDate.ToString("dd/MM/yy");

            return "";
        }
    }


    protected string GetDailyStartTime
    {
        get
        {
            if (_schedule != null)
                return _schedule.DailyStartTime.ToString(@"hh\:mm"); ;


            return "09:00";
        }
    }

    protected string GetDailyEndTime
    {
        get
        {
            if (_schedule != null)
                return _schedule.DailyEndTime.ToString(@"hh\:mm"); ;


            return "13:00";
        }
    }

    protected string GetTuitionMinutes
    {
        get
        {
            if (_schedule != null)
                return _schedule.TuitionMinutes.ToString(); ;


            return "0";
        }
    }

    protected string GetBreakMinutes
    {
        get
        {
            if (_schedule != null && _schedule.BreakMinutes.HasValue)
                return _schedule.BreakMinutes.Value.ToString(); ;


            return "15";
        }
    }
}
