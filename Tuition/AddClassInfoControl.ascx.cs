﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Pace.DataAccess.Tuition;

public partial class Tuition_AddClassInfoControl : BaseTuitionControl
{
    protected Int32? _classId;
    protected DateTime? _activeFrom;

    protected ClassInfo _classInfo;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        using (TuitionEntities entity = new TuitionEntities())
        {
            if (GetValue<Int32?>("ClassId").HasValue)
            {
                _classId = GetValue<Int32>("ClassId");
            }

            if (GetValue<Int32?>("ClassId").HasValue && GetValue<DateTime?>("ActiveFrom").HasValue)
            {
                _classId = GetValue<Int32>("ClassId");
                _activeFrom = GetValue<DateTime>("ActiveFrom");

                _classInfo = (from n in entity.ClassInfoes.Include("ClassRoom").Include("Teacher").Include("Class")
                              where n.ClassId == _classId & n.ActiveFrom == _activeFrom
                                   select n).FirstOrDefault();
            }
        }
    }


    protected string IsDisabled
    {
        get
        {

            return (_activeFrom.HasValue) ? "disabled=disabled" : string.Empty;
        }

    }
    protected string GetClassId
    {
        get
        {
            if (_classInfo != null)
                return _classInfo.ClassId.ToString();

            if (_classId.HasValue)
                return _classId.Value.ToString();


            return "0";
        }
    }

    protected string GetClassroomList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_classInfo != null && _classInfo.ClassRoom != null) ? _classInfo.ClassRoom.ClassroomId : (short?)null;


        foreach (ClassRoom item in this.LoadClassRooms())
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.ClassroomId, item.Label, (_current.HasValue && item.ClassroomId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }

    protected string GetTeacherList()
    {
        StringBuilder sb = new StringBuilder();

        short? _current = (_classInfo != null && _classInfo.Teacher != null) ? _classInfo.Teacher.TeacherId : (short?)null;


        foreach (Teacher item in this.LoadTeachers().OrderBy(x=>x.Name))
        {
            sb.AppendFormat("<option {2} value={0}>{1}</option>", item.TeacherId, item.Name, (_current.HasValue && item.TeacherId == _current) ? "selected" : string.Empty);
        }

        return sb.ToString();
    }



    protected string GetActiveFrom
    {
        get
        {
            if (_classInfo != null)
                return _classInfo.ActiveFrom.ToString("dd/MM/yy");


            return null;
        }
    }

}
