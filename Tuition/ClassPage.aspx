﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ClassPage.aspx.cs" Inherits="Tuition_ClassPage" %>

<asp:Content ID="script" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("table").tablesorter()

            $("#<%= repeat.ClientID %>").buttonset();

            refreshCurrentTab = function () {
                location.reload();
            }


            cloneandschedule = function (classid) {
                alert('asdd');
                $.ajax({
                    type: "POST",
                    url: '<%= ToVirtual("~/Tuition/ClassPage.aspx","CloneAndScheduleClass") %>',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ classid: classid }),
                    success: function (msg) {
                        if (msg.d != null) {
                            if (msg.d.Success) {
                                // similar behavior as clicking on a link
                                window.location.href = msg.d.RedirectUrl;
                            }

                            else
                                alert(msg.d.ErrorMessage);
                        }
                        else
                            alert("No Response, please contact admin to check!");
                    }
                });
            }

            $('#<%= lookupString.ClientID %>').bind('keypress', function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) { //Enter keycode
                    eval($("#<%=searchButton.ClientID %>").attr('href'));
                }
            });

        });
    </script>
    <style type="text/css">
        #toolbar
        {
            padding: 10px 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <asp:Label ID="newPageName" runat="server" Text="Classes Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="searchcontainer">
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        Search By
                    </td>
                    <td>
                        <asp:DropDownList ID="listclasses" runat="server" DataTextField="Description" DataValueField="TuitionLevelId"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="All Class Levels" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="listprogrammetype" runat="server" DataTextField="Description"
                            DataValueField="ProgrammeTypeId" AppendDataBoundItems="true">
                            <asp:ListItem Text="All Programme Types" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        .
                    </td>
                </tr>
                <tr>
                    <td>
                        Find By: Teacher Name/Class/Level
                    </td>
                    <td>
                        <asp:TextBox ID="lookupString" runat="server" class="text ui-widget-content ui-corner-all"
                            EnableViewState="true" OnTextChanged="lookupclass" Font-Size="X-Large"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="resultscontainer">
        <span id="repeat" runat="server" repeatdirection="Horizontal">
            <asp:LinkButton ID="searchButton" runat="server" OnClick="lookupclass"><span>Search Results</span>
            </asp:LinkButton>
            <a href="?Action=current">Current</a> <a href="?Action=future">Future</a> <a href="?Action=previous">
                Previous</a> <a href="?Action=all">All</a> </span><span style="float: right;">
                     <a title="Add Class" href="#" onclick="loadEditArrayControl('#dialog-form','ClassPage.aspx','Tuition/AddClassControl.ascx',{})"><img src="../Content/img/actions/add.png">Add Class</a>&nbsp;<asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
                    </asp:Label>
                </span>
        <table id="box-table-a" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Class Id
                    </th>
                    <th scope="col">
                        Class Level
                    </th>
                    <th scope="col">
                        Teacher
                    </th>
                    <th scope="col">
                        Room Number
                    </th>
                    <th scope="col">
                        Dates
                    </th>
                    <th scope="col">
                        Tuition Times
                    </th>
                    <th scope="col">
                        Duration
                    </th>
                    <th scope="col">
                        Course Type
                    </th>
                    <th scope="col">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="results" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center">
                                <%# DataBinder.Eval(Container.DataItem, "ClassId") %>
                            </td>
                            <td style="text-align: left">
                                <a href="ViewClassPage.aspx?ClassId=<%# DataBinder.Eval(Container.DataItem, "ClassId") %>">
                                    <%# DataBinder.Eval(Container.DataItem, "Label") %>
                                </a>
                            </td>
                            <td style="text-align: left">
                                <a href="../Contacts/ManageContactPage.aspx?ContactId=<%#DataBinder.Eval(Container.DataItem, "Teacher.ContactId") %>&Category=1&Type=0">
                                    <%# DataBinder.Eval(Container.DataItem, "Teacher.Name") %>
                                </a>
                            </td>
                            <td style="text-align: left">
                                <%# DataBinder.Eval(Container.DataItem, "ClassRoom.Label") %>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:D}")%>
                                to
                                <%#DataBinder.Eval(Container.DataItem, "EndDate", "{0:D}")%>
                            </td>
                            <td style="text-align: left">
                                <%#DataBinder.Eval(Container.DataItem, "DailyStartTime") %>
                                -
                                <%#DataBinder.Eval(Container.DataItem, "DailyEndTime") %>
                            </td>
                            <td style="text-align: left">
                                <%# CalculateTimePeriod(DataBinder.Eval(Container.DataItem, "StartDate", "{0:D}"), DataBinder.Eval(Container.DataItem, "EndDate", "{0:D}"), 'M') %>
                                months
                            </td>
                            <td style="text-align: left">
                                <%# DataBinder.Eval(Container.DataItem, "Xlk_CourseType.Description") %>
                            </td>
                            <td style="text-align: left">
                                <a onclick="cloneandschedule(<%# DataBinder.Eval(Container.DataItem, "ClassId") %>)"
                                    href="#">
                                    <img alt="Clone Class" src="../Content/img/actions/add.png" />Clone</a><a onclick="loadEditArrayControl('#dialog-form','ClassPage.aspx','Tuition/AddClassControl.ascx',{ ClassId: <%#DataBinder.Eval(Container.DataItem, "ClassId")%>, Action: 0},<%#DataBinder.Eval(Container.DataItem, "ClassId")%>)"
                                        href="#" title="Edit Class">
                                        <img src="../Content/img/actions/edit.png"></a>&nbsp;<a href="<%# ReportPath("AcademicReports","Academic_RoomTeacherLabel", new Dictionary<string, object> { {"ClassId", DataBinder.Eval(Container.DataItem, "ClassId")}}) %>">
                                <img src="../Content/img/actions/printer.png" />Door Info</a>
                                                                                          
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
