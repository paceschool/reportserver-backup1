﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManageClassPage.aspx.cs" Inherits="Tuition_ManageClassPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function() {
            $("#<%= newstartdate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#<%= newenddate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Add / Manage Class Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <div id="errorcontainer" name="errorcontainer" class="error" runat="server" visible="false">
        <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
    </div>
           <div class="inputArea">
                    <fieldset>
                        <label>
                            Class Level
                        </label>
                        <asp:DropDownList ID="newclasslevel" runat="server" DataTextField="Description" DataValueField="TuitionLevelId"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="Choose a Level" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                        <label>
                            Teacher
                        </label>
                        <asp:DropDownList ID="newclassteacher" runat="server" DataTextField="Name" DataValueField="TeacherId"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="Choose a Teacher" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                        <label>
                            Classroom
                        </label>
                        <asp:DropDownList ID="newclassroom" runat="server" DataTextField="Label" DataValueField="ClassroomId"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="Choose a Room" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                        <label>
                            Programme Type
                        </label>
                        <asp:DropDownList ID="newclassprogramme" runat="server" DataTextField="Description"
                            DataValueField="ProgrammeTypeId" AppendDataBoundItems="true">
                            <asp:ListItem Text="Choose a Type" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                        <label>
                            Course Type
                        </label>
                        <asp:DropDownList ID="newclasscoursetype" runat="server" DataTextField="Description"
                            DataValueField="CourseTypeId" AppendDataBoundItems="true">
                            <asp:ListItem Text="Choose a Type" Value="-1">
                            </asp:ListItem>
                            <asp:ListItem Text="-------" Value="0">
                            </asp:ListItem>
                        </asp:DropDownList>
                    </fieldset>
                    <fieldset>
                        <label>
                            Start Date
                        </label>
                        <asp:TextBox ID="newstartdate" runat="server">
                        </asp:TextBox>
                        <label>
                            End Date
                        </label>
                        <asp:TextBox ID="newenddate" runat="server">
                        </asp:TextBox>
                        <label>
                            Class Start Time
                        </label>
                        <asp:TextBox ID="newstarttime" runat="server">
                        </asp:TextBox>
                        <label>
                            Class End Time
                        </label>
                        <asp:TextBox ID="newendtime" runat="server">
                        </asp:TextBox>
                    </fieldset>
                    <fieldset>
                        <label>
                            Closed Group?
                        </label>
                        <asp:CheckBox ID="newclosedclass" runat="server" />
                        <asp:LinkButton ID="newsave" runat="server" class="button" OnClick="Click_SaveClass"><span>Save</span></asp:LinkButton>
                        <asp:LinkButton ID="cancel" runat="server" class="button" OnClientClick=""><span>Cancel</span></asp:LinkButton>
                    </fieldset>
                </div>

</asp:Content>
