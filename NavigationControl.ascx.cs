﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Pace.DataAccess.Enrollment;
using System.Text;

[PartialCaching(60, null, null, "CampusId", true)]
public partial class NavigationControl : BaseControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (campuslist.Items == null || campuslist.Items.Count == 0)
        {
            campuslist.DataSource = BasePage.LoadCampus;
            campuslist.DataBind();
        }
        if (campuslist.Items != null && campuslist.Items.Count > 0)
            campuslist.Items.FindByValue(BasePage.GetCampusId.ToString()).Selected = true;
    }

    protected DateTime firstDay = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);

    public string CampusIdCacheString
    {
        get { return BasePage.GetCampusId.ToString(); }
    }

    protected List<Pace.DataAccess.Security.Company> GetCompanies()
    {
        return BasePage.GetCompanies();
    }

    protected List<Pace.DataAccess.Group.Group> GeCurrentGroups(int MaxNoOfGroups)
    {
        DateTime _now = DateTime.Now.Date;
        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            var GroupSearchQuery = (from g in entity.Group
                                    where g.ArrivalDate <= _now && g.Xlk_Status.StatusId != 3 && g.DepartureDate >= _now && g.CampusId == BasePage.GetCampusId
                                    orderby g.ArrivalDate ascending
                                    select g).ToList().Take(MaxNoOfGroups);

            return GroupSearchQuery.ToList();
        }
    }

    protected List<Pace.DataAccess.Group.Group> GetNextGroups(int NoOfGroups)
    {
        DateTime _now = DateTime.Now.Date;
        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            var GroupSearchQuery = (from g in entity.Group
                                    where g.ArrivalDate >= _now && g.Xlk_Status.StatusId != 3 && g.CampusId == BasePage.GetCampusId
                                    orderby g.ArrivalDate ascending
                                    select g).ToList().Take(NoOfGroups);

            return GroupSearchQuery.ToList();
        }
    }

    protected Dictionary<Xlk_ProgrammeType, List<Student>> Click_Arriving_ThisWeek()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime endarrivingDate = startarrivingDate.AddDays(5);
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = (from f in entity.Student
                                      from g in f.Enrollment
                                      where f.GroupId == null && ((g.StartDate >= startarrivingDate && g.StartDate <= endarrivingDate)) && f.CampusId == BasePage.GetCampusId
                                      orderby f.ArrivalDate, f.FirstName ascending, f.SurName ascending
                                      group f by g.Xlk_ProgrammeType into proggroup
                                      select proggroup).ToDictionary(gdc => gdc.Key, gdc => gdc.Distinct().ToList());
            return StudentSearchQuery;
        }
    }

    protected Dictionary<Xlk_ProgrammeType, List<Student>> Click_Arriving_NextWeek()
    {
        DateTime startarrivingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek).AddDays(7);
        DateTime endarrivingDate = startarrivingDate.AddDays(5);
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = (from f in entity.Student
                                      from g in f.Enrollment
                                      where f.GroupId == null && ((g.StartDate >= startarrivingDate && g.StartDate <= endarrivingDate)) && f.CampusId == BasePage.GetCampusId
                                      orderby f.ArrivalDate, f.FirstName ascending, f.SurName ascending
                                      group f by g.Xlk_ProgrammeType into proggroup
                                      select proggroup).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

            return StudentSearchQuery;
        }
    }

    protected Dictionary<Xlk_ProgrammeType,List<Student>> Click_Departing()
    {
        DateTime startdepartingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime enddepartingDate = startdepartingDate.AddDays(7);
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = (from f in entity.Student
                                     from g in f.Enrollment
                                      where f.GroupId == null && (g.EndDate >= startdepartingDate && g.EndDate <= enddepartingDate) && f.CampusId == BasePage.GetCampusId
                                     orderby f.DepartureDate, f.FirstName ascending, f.SurName ascending
                                     group f by g.Xlk_ProgrammeType into proggroup
                                     select proggroup).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());
            return StudentSearchQuery;
        }
    }

    protected List<Pace.DataAccess.Enrollment.Student> Click_Cancelled()
    {
        DateTime checkDate = DateTime.Now.AddMonths(-12);

        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var StudentSearchQuery = from f in entity.Student
                                     where f.Xlk_Status.StatusId == 5 && f.CampusId == BasePage.GetCampusId && f.ArrivalDate > checkDate
                                     select f;
            return StudentSearchQuery.ToList();
        }
    }

    protected List<Pace.DataAccess.Group.Group> Click_GroupDeparting()
    {
        DateTime startdepartingDate = FirstDayOfWeek(DateTime.Now, CalendarWeekRule.FirstFullWeek);
        DateTime enddepartingDate = startdepartingDate.AddDays(7);
        using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
        {
            var GroupSearchQuery = (from f in entity.Group
                                    from g in f.Enrollment
                                    where f.GroupId != null && (g.EndDate >= startdepartingDate && g.EndDate <= enddepartingDate) && f.CampusId == BasePage.GetCampusId
                                    orderby f.DepartureDate, f.GroupName ascending
                                    select f).Distinct();
            return GroupSearchQuery.ToList();
        }
    }

    protected List<Pace.DataAccess.Contacts.Xlk_Category> ContactCategories()
    {
        {
            using (Pace.DataAccess.Contacts.ContactsEntities entity = new Pace.DataAccess.Contacts.ContactsEntities())
            {
                IQueryable<Pace.DataAccess.Contacts.Xlk_Category> lookupQuery =
                     from p in entity.Xlk_Category
                     select p;
                return lookupQuery.ToList();
            }
        }
    }
    protected List<Pace.DataAccess.Documents.Xlk_Category> DocumentCategories()
    {
        {
            using (Pace.DataAccess.Documents.DocumentsEntities entity = new Pace.DataAccess.Documents.DocumentsEntities())
            {
                IQueryable<Pace.DataAccess.Documents.Xlk_Category> lookupQuery =
                     from p in entity.Xlk_Category
                     select p;
                return lookupQuery.ToList();
            }
        }
    }

}
