﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManageInvoicePage.aspx.cs" Inherits="Payments_RecordInvoicePage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="New Supplier Invoice Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
<script type="text/javascript">
    $(function() {
        $("#<%= invoicedate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });

        $('[type=checkbox]').change(function() {
            checkSaveEnabled();
        });

        $('input:text').keyup(function(event) {
            checkSaveEnabled();
        });

        $('input:text').change(function() {
            checkSaveEnabled();
        });

        checkSaveEnabled = function() {

            $("#<%= save.ClientID %>").hide();

            var n = $("input:checked").length;
            var j = $('input:text[value=""]').length;


            if (n > 0 && j == 0)
                $("#<%= save.ClientID %>").show();
        }
    });
    </script>

    <div class="resultscontainer" style="float: left;">
        <div id="errorcontainer" name="errorcontainer" class="error" visible="false" runat="server">
            <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
        </div>
        <table id="search-table">
            <tr>
                <td>
                    <label>
                        Supplier</label>
                </td>
                <td>
                    <asp:DropDownList ID="supplier" runat="server" DataTextField="Title" DataValueField="SupplierId"
                        AutoPostBack="True" OnDataBound="supplier_DataBound" OnSelectedIndexChanged="supplier_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <label>
                        Invoice Ref</label>
                </td>
                <td>
                    <asp:TextBox ID="invoiceref" runat="server"></asp:TextBox>
                </td>
                <td>
                    <label>
                        Invoice Date</label>
                </td>
                <td>
                    <asp:TextBox ID="invoicedate" runat="server"></asp:TextBox>
                </td>
                                <td>
                    <label>
                        Total Amount</label>
                </td>
                <td>
                    <asp:TextBox ID="invoiceamount" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="viewconatiner">
        <div class="resultscontainer">
            <table id="box-table-a" class="tablesorter">
                <thead>
                    <tr>
                        <th scope="col">
                            Id
                        </th>
                        <th scope="col">
                            OurRef
                        </th>
                        <th scope="col">
                            Title
                        </th>
                        <th scope="col">
                            DateCreated
                        </th>
                        <th scope="col">
                            ExpectedCost
                        </th>
                        <th scope="col">
                            Description
                        </th>
                        <th scope="col">
                            Assign To Invoice
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="results" runat="server" EnableViewState="True">
                        <ItemTemplate>
                            <tr>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "VoucherId")%>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "OurRef")%>
                                </td>
                                <td style="font-weight: bold; text-align:center" >
                                    <%#DataBinder.Eval(Container.DataItem, "Title")%>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "DateCreated", "{0:D}")%>
                                </td>
                                <td style="text-align:center">
                                    €<%#DataBinder.Eval(Container.DataItem, "ExpectedCost", "{0:0.00}")%>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "Description")%>
                                </td>
                                <td style="text-align:center">
                                <asp:CheckBox ID="checkbox" runat="server" Checked="false" />
                                <asp:HiddenField ID="voucherId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "VoucherId")%>' />
                                <asp:HiddenField ID="expectedCost"  runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ExpectedCost")%>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="7" align="right">
                            <asp:Button ID="save" Style="display: none" runat="server" Text="Save" 
                                onclick="save_Click" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
