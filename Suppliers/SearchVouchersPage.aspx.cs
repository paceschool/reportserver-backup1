﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Supplier;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;

public partial class SearchVouchersPage : BaseSupplierPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Set the DataSource of the Supplier Type dropdown
            SupplierType.DataSource = this.LoadSupplierTypes();
            SupplierType.DataBind();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void supplierType_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSuppliersToDropDown();
    }

    protected void supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        SupplierChanged();
    }

    private void SupplierChanged()
    {
        if (supplier.SelectedItem != null)
        {
            Int32 _supplierId = Convert.ToInt32(supplier.SelectedItem.Value);

            if (_supplierId == 0)
            {
                var SupplierSearchQuery = from d in Entities.Voucher.Include("Supplier")
                                          where d.Supplier.Xlk_SupplierType.SupplierTypeId == SupplierType.SelectedIndex
                                          orderby d.Supplier.Title ascending
                                          select d;
                LoadResults(SupplierSearchQuery.ToList());
            }
            else
            {
                var SupplierSearchQuery1 = from d in Entities.Voucher.Include("Supplier")
                                           where d.Supplier.SupplierId == _supplierId 
                                          orderby d.Supplier.Title ascending
                                          select d;
                LoadResults(SupplierSearchQuery1.ToList());
            }
        }
    }

    private void LoadResults(List<Voucher> supplier)
    {
        //Set datasource of the repeater
        results.DataSource = supplier;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", supplier.Count().ToString());
    }

    protected void supplier_DataBound(object sender, EventArgs e)
    {
        SupplierChanged();
    }
    protected void supplierType_DataBound(object sender, EventArgs e)
    {
        LoadSuppliersToDropDown();
    }

    #endregion 

    #region Private Methods

    private void LoadSuppliersToDropDown()
    {
        //Set datasource of the Type DropDown
        ListItem _supplier = SupplierType.SelectedItem;
        supplier.DataSource = this.LoadSuppliers(Convert.ToInt32(_supplier.Value));
        supplier.DataBind();
        lookupString.Text = "";
    }
    protected void PrintClick(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)(sender);
        string printId = lb.CommandArgument;

        Response.Redirect(string.Format("PrintPage.aspx?VoucherId={0}", printId));
    }

    protected void Click_SearchVouchers(object sender, EventArgs e)
    {
        var voucherSearchQuery = from v in Entities.Voucher.Include("Supplier")
                                 where v.Title.Contains(lookupString.Text) || v.OurRef.Contains(lookupString.Text) || v.SupplierRef.Contains(lookupString.Text)
                                 select v;
        results.DataSource = voucherSearchQuery.ToList();
        results.DataBind();
        lookupString.Text = "";
    }

    protected void Click_ClearFilter(object sender, EventArgs e)
    {
        lookupString.Text = "";
    }


    #endregion

}