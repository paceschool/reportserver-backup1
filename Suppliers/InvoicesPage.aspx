﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InvoicesPage.aspx.cs" Inherits="Payments_InvoicesPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <asp:Label ID="newPageName" runat="server" Text="Supplier Invoices Page" BorderStyle="None" Visible="false"></asp:Label> <!-- Page Name -->
    <script type="text/javascript" language="javascript">
        $(function() {

            var Id

            // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
            $("#dialog:ui-dialog").dialog("destroy");

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 'auto',
                width: 'auto',
                modal: true,
                title: "Vouchers for " + $("#<%= supplier.ClientID %> option:selected").text(),
                open: function() {
                    //display correct dialog content
                    $.ajax({
                        type: "POST",
                        url: "InvoicesPage.aspx/LoadVouchers",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: "{'id':" + Id + "}",
                        success: function(msg) {
                            $("#dialog-form").html(msg.d);
                        }
                    });
                },
                buttons: {
                    Cancel: function() {
                        $(this).dialog("close");
                    }
                },
                close: function() {

                }
            });


            showDialog = function(id) {
                Id = id;
                $("#dialog-form").dialog("open");
            }
        });

    </script>

    <div class="resultscontainer" style="float: left;">
        <div id="errorcontainer" name="errorcontainer" class="error" visible="false" runat="server">
            <asp:Label ID="errormessage" runat="server" Text=""></asp:Label>
        </div>
        <table id="search-table">
            <tbody>
                <tr>
                    <td>
                        <label>
                            Supplier</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="supplier" runat="server" DataTextField="Title" DataValueField="SupplierId"
                            AutoPostBack="True" OnDataBound="supplier_DataBound" OnSelectedIndexChanged="supplier_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <label>
                            Ref (Invoice/Voucher)</label>
                    </td>
                    <td>
                        <asp:TextBox ID="reference" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="search" Text="Search" runat="server" OnClick="search_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="viewconatiner">
        <div class="resultscontainer">
            <table id="box-table-a" class="tablesorter">
                <thead>
                    <tr>
                        <th scope="col">
                            Invoice Id
                        </th>
                        <th scope="col">
                            Ref
                        </th>
                        <th scope="col">
                            Invoice Date
                        </th>
                        <th scope="col">
                            Amount
                        </th>
                        <th scope="col">
                            Date Created
                        </th>
                        <th scope="col">
                            Vouchers
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="results" runat="server" EnableViewState="True">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "InvoiceId")%>
                                </td>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "InvoiceRef")%>
                                </td>
                                <td style="font-weight: bold">
                                    <%#DataBinder.Eval(Container.DataItem, "InvoiceDate")%>
                                </td>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "TotalAmount")%>
                                </td>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "ReceivedDate")%>
                                </td>
                                <td>
                                    <a href="#" onclick="showDialog('<%#DataBinder.Eval(Container.DataItem, "InvoiceId")%>');">
                                        View</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Areas for showing in the dialog -->
    <div id="dialog-form" title="View Vouchers">
        <table id="Table1" class="tablesorter">
            <thead>
                <tr>
                    <th scope="col">
                        Invoice Id
                    </th>
                    <th scope="col">
                        Ref
                    </th>
                    <th scope="col">
                        Invoice Date
                    </th>
                    <th scope="col">
                        Amount
                    </th>
                    <th scope="col">
                        Date Created
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5">
                        Loading Results
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
