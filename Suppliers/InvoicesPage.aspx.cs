﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Supplier;
using System.Web.Services;
using System.Text;

public partial class Payments_InvoicesPage : BaseSupplierPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            supplier.DataSource = this.LoadSuppliers();
            supplier.DataBind();
            
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void supplier_DataBound(object sender, EventArgs e)
    {
        SearchRecords();
    }

    protected void supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        SearchRecords();
    }

    protected void search_Click(object sender, EventArgs e)
    {
        SearchRecords();
    }

    #endregion

    #region Private


    private void SearchRecords()
    {
        short _supplierId = Convert.ToInt16(supplier.SelectedItem.Value);
        string _ref = reference.Text;


        var invoiceQuery = from i in Entities.Invoice
                           where i.Supplier.SupplierId == _supplierId
                           select i;

        if (!string.IsNullOrEmpty(_ref))
            invoiceQuery = invoiceQuery.Where(i => i.InvoiceRef.StartsWith(_ref) || i.Lnk_InvoiceVoucher.Where(v => v.Voucher.SupplierRef.StartsWith(_ref) || v.Voucher.OurRef.StartsWith(_ref)).Count() >0);

        results.DataSource = invoiceQuery.ToList();
        results.DataBind();

    }

    #endregion

    #region Javascript Enabled Methods

    [WebMethod]
    public static string LoadVouchers(int id)
    {
        StringBuilder sb = new StringBuilder("<table id=\"box-table-a\" class=\"tablesorter\"><thead><tr><th scope=\"col\">Voucher Id</th><th scope=\"col\">Title</th><th scope=\"col\">Date Created</th><th scope=\"col\">Time</th><th scope=\"col\">Our Ref</th><th scope=\"col\">No Leaders/Students</th></tr></thead><tbody>");

        IQueryable<Voucher> voucherQuery = from i in Entities.Voucher
                           from l in i.Lnk_InvoiceVoucher
                           where l.InvoiceId == id
                           select i;

        if (voucherQuery.Count() > 0)
        {

            foreach (Voucher item in voucherQuery.ToList())
            {
                sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}/{6}</td></tr>", item.VoucherId, item.Title.Trim(), item.VoucherDate.Value.ToString("dd MM yyyy"), item.VoucherTime.Value.ToString(), item.OurRef, item.NumberOfLeaders.Value.ToString(), item.NumberOfStudents.Value.ToString());

            }
        }

        sb.Append("</tbody></table>");
        return sb.ToString();
    }

    #endregion
}
