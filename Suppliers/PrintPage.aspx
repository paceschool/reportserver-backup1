﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="PrintPage.aspx.cs"
    Inherits="PrintPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" type="text/css" href="<%= Page.ResolveUrl("~/Content/css/print.css") %>" />
    <title>Print Voucher</title>
</head>
<body>
    <div id="myPrintArea" style="text-align: center">
        <fieldset>
            <table class="voucherprint">
                <colgroup>
                    <col class="oce-first" />
                </colgroup>
                <thead>
                    <tr>
                        <th colspan="2" style="background-color:#ffffff;border-top: 1px solid #E8EDFF;">
                            <asp:Image ID="printpic" Style="border-width: 0px; float: left; padding-left: 25px"
                                runat="server" src="../Content/img/pacelogoprint.jpg" />
                        </th>
                    </tr>
                    <tr>
                        <th scope="col">
                            Type
                        </th>
                        <th scope="col">
                            Excursion Voucher
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            This voucher is the property of PACE Language Institute.
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>
                            <label for="SupplierTitle">
                                Venue:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="SupplierTitle" name="SupplierTitle" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="VoucherTitle">
                                Group:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="VoucherTitle" name="VoucherTitle" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="OurRef">
                                Our Reference:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="OurRef" name="OurRef" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="SupplierRef">
                                Supplier Reference:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="SupplierRef" name="SupplierRef" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="VoucherDate">
                                Voucher Date:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="VoucherDate" name="VoucherDate" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="VoucherTime">
                                Voucher Time:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="VoucherTime" name="VoucherTime" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="NumberOfStudents">
                                Number Of Students:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="NumberOfStudents" name="NumberOfStudents" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="NumberOfLeaders">
                                Number Of Leaders:
                            </label>
                        </td>
                        <td>
                            <asp:Label ID="NumberOfLeaders" name="NumberOfLeaders" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
    </div>
</body>
</html>
