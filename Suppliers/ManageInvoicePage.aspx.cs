﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Supplier;
using System.Data.Entity.Core;

public partial class Payments_RecordInvoicePage : BaseSupplierPage
{

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            supplier.DataSource = this.LoadSuppliers();
            supplier.DataBind();
        }
    }

    public override string TextValue() // Page Name
    {
        return newPageName.Text;
    }

    protected void supplier_DataBound(object sender, EventArgs e)
    {
        LoadTable();
    }
    protected void supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTable();
    }

    protected void save_Click(object sender, EventArgs e)
    {
        Dictionary<int, decimal> vouchers = new Dictionary<int, decimal>();

        if (DoChecks())
        {

            for (int cnt = 0; cnt < results.Items.Count; cnt++)
            {
                // your checkbox ; type casting
                CheckBox cbId = ((CheckBox)results.Items[cnt].FindControl("checkbox"));

                // or the other option; value from hidden field

                if (cbId.Checked)
                {
                    HiddenField voucher = ((HiddenField)results.Items[cnt].FindControl("voucherId"));
                    HiddenField expectedCost = ((HiddenField)results.Items[cnt].FindControl("expectedCost"));
                    vouchers.Add(Convert.ToInt32(voucher.Value), Convert.ToDecimal(expectedCost.Value));
                }
            }

            if (vouchers.Count > 0)
            {
                Invoice _invoice = new Invoice();
                _invoice.ReceivedDate = DateTime.Now;
                _invoice.TotalAmount = decimal.Parse(invoiceamount.Text);
                _invoice.InvoiceRef = invoiceref.Text;
                _invoice.InvoiceDate = Convert.ToDateTime(invoicedate.Text);
                _invoice.ReceivedDate = DateTime.Now;
                _invoice.SupplierReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Supplier", "SupplierId", Convert.ToInt16(supplier.SelectedItem.Value));

                Entities.AddToInvoice(_invoice);

                if (Entities.SaveChanges() > 0)
                {
                    foreach (var item in vouchers)
                    {
                        Lnk_InvoiceVoucher inv = new Lnk_InvoiceVoucher();
                        inv.InvoiceReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Invoice", "InvoiceId", _invoice.InvoiceId);
                        inv.VoucherReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Voucher", "VoucherId", item.Key);
                        inv.AmountAllocated = item.Value;

                        Entities.AddToLnk_InvoiceVoucher(inv);
                    }
                    if (Entities.SaveChanges() > 0)
                    {
                        ClearFields();

                    }
                }


            }
        }
    }

    #endregion

    #region Methods

    private void ClearFields()
    {
        invoiceamount.Text = string.Empty;
        invoiceref.Text = string.Empty;
        invoicedate.Text = string.Empty;
        LoadTable();
    }

    private bool DoChecks()
    {
        decimal d;
        DateTime dd;

        if (!decimal.TryParse(invoiceamount.Text, out d))
            return false;

        if (invoiceref.Text.Length == 0)
            return false;

        if (!DateTime.TryParse(invoicedate.Text, out dd))
            return false;

        if (supplier.SelectedItem == null)
            return false;

        return true;
    }

    private void LoadTable()
    {
        ListItem _supplier = supplier.SelectedItem;
        results.DataSource = this.LoadVouchers(Convert.ToInt32(_supplier.Value));
        results.DataBind();
    }

    private List<Voucher> LoadVouchers(int supplierId)
    {

        IQueryable<Voucher> voucherQuery = from v in Entities.Voucher
                                           where v.Lnk_InvoiceVoucher.Count() == 0 && v.Supplier.SupplierId == supplierId
                                           select v;

        return voucherQuery.ToList();
    }

    #endregion




}
