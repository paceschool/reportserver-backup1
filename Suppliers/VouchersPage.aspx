<%@ Page Title="Pace Voucher Management" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VouchersPage.aspx.cs" Inherits="VouchersPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript">
    $(function() {
    $("#<%= voucherDate.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' });
	});
	</script>
	<div id="pagename">
        <span style="font-style:italic; font-weight:bolder; font-size:larger">
            Add Voucher Page
        </span>
    </div>
    <div id="resultsbox">
         <asp:Label ID="resultsreturned" runat="server" Text='Records Found: 0'>
        </asp:Label>
    </div>
    <div class="inputArea">
        <fieldset>
            <label>
                Supplier Type - 
            </label>
            <asp:DropDownList ID="suppliertype" runat="server" DataTextField="Description" DataValueField="SupplierTypeId"
                 AutoPostBack="true" 
                OnSelectedIndexChanged="suppliertype_SelectedIndexChanged" 
                ondatabound="suppliertype_DataBound">
            </asp:DropDownList>
            <label>
                Supplier - 
            </label>
            <asp:DropDownList ID="supplier" runat="server" DataTextField="Title" DataValueField="SupplierId"
                AutoPostBack="true" OnSelectedIndexChanged="supplier_SelectedIndexChanged" 
                ondatabound="supplier_DataBound">
            </asp:DropDownList>
            <label>
                PACE Ref (Auto Generated) - 
            </label>
            <asp:TextBox ID="paceref" runat="server" Enabled="false" Font-Bold="true">
            </asp:TextBox>
            <label>
                Supplier Ref - 
            </label>
            <asp:TextBox ID="supplierref" runat="server">
            </asp:TextBox>
            <label>
                Title - 
            </label>
            <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" 
                ontextchanged="TextBox1_TextChanged">
            </asp:TextBox>
        </fieldset>
        <fieldset>
            <label>
                Description - 
            </label>
            <asp:TextBox ID="TextBox2" runat="server">
            </asp:TextBox>
            <label>
                Number of Students - 
            </label>
            <asp:TextBox ID="TextBox3" runat="server" AutoPostBack="true">
            </asp:TextBox>
            <label>
                Number of Leaders - 
            </label>
            <asp:TextBox ID="TextBox4" runat="server">
            </asp:TextBox>
            <label>
                Prepaid Method - 
            </label>
            <asp:DropDownList ID="paymenttype" runat="server" DataTextField="Description" DataValueField="PaymentMethodId"
            AutoPostBack="true">
            </asp:DropDownList>
            <label>
                Pre-payment Reference - 
            </label>
            <asp:TextBox ID="TextBox7" runat="server">
            </asp:TextBox>
        </fieldset>
        <fieldset>
            <label>
                Date - 
            </label>
            <asp:TextBox ID="voucherDate" runat="server" AutoPostBack="true" ontextchanged="voucherDate_TextChanged">
            </asp:TextBox>
            <label>
                Time - 
            </label>
            <asp:TextBox ID="voucherTime" runat="server" />
            <label>
                Expected Cost - 
            </label>
            <asp:TextBox ID="expectedcost" runat="server">
            </asp:TextBox>
            <asp:CheckBox ID="CheckBox1" runat="server" Text="Print" />
            <asp:Button ID="reset" runat="server" Text="Clear" OnClick="clear_Click"/>
            <asp:Button ID="save" runat="server" Text="Save" OnClick="Save_Click"/>
        </fieldset>
    </div>
    <div id="viewconatiner">
        <div class="resultscontainer">
            <table id="box-table-a" class="tablesorter">
                <thead>
                    <tr>
                        <th scope="col">
                            Id
                        </th>
                        <th scope="col">
                            Supplier Type
                        </th>
                        <th scope="col">
                            Supplier
                        </th>
                        <th scope="col">
                            Pace Ref
                        </th>
                        <th scope="col">
                            Title
                        </th>
                        <th scope="col">
                            No. of students
                        </th>
                        <th scope="col">
                            No. of leaders
                        </th>
                        <th scope="col">
                            Supplier Ref
                        </th>
                        <th scope="col">
                            Description
                        </th>
                        <th scope="col">
                            Pre-payment reference
                        </th>
                        <th scope="col">
                            Expected Cost
                        </th>
                        <th scope="col">
                            Print
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="results" runat="server" EnableViewState="True">
                        <ItemTemplate>
                            <tr>
                                <td style="text-align:center">  
                                    <%#DataBinder.Eval(Container.DataItem, "VoucherId")%>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "Supplier.Xlk_SupplierType.Description") %>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "Supplier.Title") %>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "OurRef") %>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "Title") %>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "NumberOfStudents") %>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "NumberOfLeaders") %>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "SupplierRef") %>
                                </td>
                                <td style="text-align:center">
                                <asp:Image runat="server" src="../Content/img/house.jpg" 
                                ToolTip='<%#DataBinder.Eval(Container.DataItem, "Description") %>' />
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "PrePaidReference") %>
                                </td>
                                <td style="text-align:center">
                                    <%#DataBinder.Eval(Container.DataItem, "ExpectedCost", "{0:c}")%>
                                </td>
                                <td style="text-align:center">
                                    <asp:LinkButton ID="printlink" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "VoucherId") %>'
                                    OnClick="PrintClick" Text="" ToolTip="Click to Print">
                                    <asp:Image ID="imgFolder" runat="server" ImageUrl="../Content/img/printer.jpg" />
                                    </asp:LinkButton>
                                </td>
                                    
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
