﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Supplier;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;

public partial class PrintPage : BaseSupplierPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            if (!String.IsNullOrEmpty(Request["VoucherId"]))
            {
                var Vid = Convert.ToByte(Request["VoucherId"]);
                var printquery = from d in Entities.Voucher.Include("Supplier").Include("Supplier.Xlk_SupplierType")
                                 where d.VoucherId == Vid
                                 select d;
                LoadDetails(printquery.ToList());
                
            }
        }
    }

    private void LoadDetails(List<Voucher> details)
    {
        foreach (var detail in details)
        {
            if (detail.Supplier.Xlk_SupplierType.Description == "Excursion")
                SupplierTitle.Text = detail.Supplier.Title;
            else SupplierTitle.Text = detail.Supplier.Title;

            VoucherTitle.Text = detail.Title;
            OurRef.Text =  detail.OurRef;
            if (detail.SupplierRef != "")
            {
                SupplierRef.Text = detail.SupplierRef;
            }
            else SupplierRef.Text = "";
            VoucherDate.Text = String.Format("{0:dddd, d MMMM yyyy}", detail.VoucherDate);
            VoucherTime.Text = String.Format("{0:t}", detail.VoucherTime);
            NumberOfStudents.Text =  (detail.NumberOfStudents.HasValue) ? detail.NumberOfStudents.Value.ToString(): "n/a";
            NumberOfLeaders.Text = (detail.NumberOfLeaders.HasValue) ? detail.NumberOfLeaders.Value.ToString() : "n/a";

        }
    }
}
