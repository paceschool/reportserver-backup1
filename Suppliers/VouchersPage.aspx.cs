﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pace.DataAccess.Supplier;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Text;
using System.Data.Entity.Core;

public partial class VouchersPage : BaseSupplierPage
{
    #region Events

    private Int32? _voucherId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["VoucherId"]))
            _voucherId = Convert.ToInt32(Request["VoucherId"]);

        if (!Page.IsPostBack)
        {
            
            //Set datasource of the Category DropDown
            suppliertype.DataSource = this.LoadSupplierTypes();
            suppliertype.DataBind();
            //Set datasource of PaymentMethod DropDown
            paymenttype.DataSource = this.LoadPaymentMethods();
            paymenttype.DataBind();
            //Populate Suppliers DropDown on load based on initial SupplierType value

            if (_voucherId.HasValue)
            {
                DisplayVoucher(LoadVoucher(_voucherId.Value));
            }
        }
    }

    //Update Suppliers DropDown
    protected void suppliertype_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSuppliersToDropDown();
    }
    protected void suppliertype_DataBound(object sender, EventArgs e)
    {
        LoadSuppliersToDropDown();
    }
    //Clear Fields
    protected void clear_Click(object sender, EventArgs e)
    {
        paceref.Text = ""; supplierref.Text = ""; TextBox1.Text = "";
        TextBox2.Text = ""; TextBox3.Text = ""; TextBox4.Text = "";
        TextBox7.Text = ""; expectedcost.Text = ""; suppliertype.SelectedIndex = 0;
        LoadSuppliersToDropDown();
        
    }

    //Save record and Print if required
    protected void Save_Click(object sender, EventArgs e)
    {
        
        string _message = String.Empty;
        bool IsAcceptable = true;

        if (String.IsNullOrEmpty(paceref.Text))
            { _message = "No Title entered."; IsAcceptable = false; }
        else if (String.IsNullOrEmpty(TextBox3.Text))
        { _message = "A number of Students must be entered"; IsAcceptable = false; }
        else
        {
            IsAcceptable = true;
        }
        if (IsAcceptable == true)
        {
            Voucher _voucher = new Voucher();

            _voucher.OurRef = paceref.Text;
            _voucher.SupplierRef = supplierref.Text;
            _voucher.Title = TextBox1.Text;
            _voucher.Description = TextBox2.Text;
            _voucher.DateCreated = DateTime.Now.Date;
            _voucher.VoucherDate = Convert.ToDateTime(voucherDate.Text);

            int vhour = Convert.ToInt32((voucherTime.Text).Substring(0, 2));
            int vmin = Convert.ToInt32((voucherTime.Text).Substring(3, 2));
            _voucher.VoucherTime = new TimeSpan(vhour, vmin, 0);

            _voucher.NumberOfStudents = Convert.ToInt32(TextBox3.Text);
            _voucher.NumberOfLeaders = Convert.ToInt32(TextBox4.Text);
            _voucher.ExpectedCost = Convert.ToDecimal(expectedcost.Text);
            _voucher.PrepaidReference = TextBox7.Text;
            _voucher.SupplierReference.EntityKey = new EntityKey(Entities.DefaultContainerName + ".Supplier", "SupplierId", Convert.ToInt16(supplier.SelectedItem.Value));
            
            SaveVoucher(_voucher);
        }
        
        else _message = "Record not saved.";

        MessageBoxShow(this, _message);
        
    }

    protected void PrintClick(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)(sender);
        string printId = lb.CommandArgument;

        Response.Redirect(string.Format("PrintPage.aspx?VoucherId={0}", printId));
    }

    protected void supplier_DataBound(object sender, EventArgs e)
    {
        SupplierChanged();

    }
    protected void supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        SupplierChanged();
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        CreateRef();
    }
    protected void voucherDate_TextChanged(object sender, EventArgs e)
    {
        CreateRef();
    }
    #endregion

    #region Private Methods

    private void CreateRef()
    {
        DateTime _useDate = (!string.IsNullOrEmpty(voucherDate.Text)) ? Convert.ToDateTime(voucherDate.Text) :DateTime.Now;
        Supplier _supplier = LoadSupplier(Convert.ToInt32(supplier.SelectedItem.Value));
        if (_supplier != null)
        {
            if (TextBox1.Text == "" || TextBox1.Text.Length < 3)
                paceref.Text = string.Format("PACE-{0}-{1}-{2}", _supplier.VoucherRef, _useDate.Year
                    + _useDate.DayOfYear + "0", supplier.SelectedValue);
            else
                paceref.Text = string.Format("PACE-{0}-{1}-{2}-{3}", _supplier.VoucherRef, TextBox1.Text.Substring(0, 3).ToUpper(),_useDate.Year
                    + _useDate.DayOfYear , supplier.SelectedValue);
        }
    }
    //Populate Suppliers DropDown based on value of SupplierTypes DropDown
    private void LoadSuppliersToDropDown()
    {
        ListItem _supplier = suppliertype.SelectedItem;
        supplier.DataSource = this.LoadSuppliers(Convert.ToInt32(_supplier.Value));
        supplier.DataBind();
    }

    //Error MessageBox
    private void MessageBoxShow(Page page, string message)
    {
        Literal ltr = new Literal();
        ltr.Text = @"<script type='text/javascript'> alert('" + message + "') </script>";
        page.Controls.Add(ltr);
    }

    private void SupplierChanged()
    {
        if (supplier.SelectedItem != null)
        {
            Int32 _typeId = Convert.ToInt32(supplier.SelectedItem.Value);
            Int32 _suppTypeId = Convert.ToInt32(suppliertype.SelectedItem.Value);

            var contactSearchQuery = from d in Entities.Voucher.Include("Supplier.Xlk_SupplierType")
                                     where d.Supplier.SupplierId == _typeId
                                     orderby d.Title ascending
                                     select d;

            LoadResults(contactSearchQuery.ToList());
            CreateRef();
        }

    }

    private void LoadResults(List<Voucher> details)
    {
        //Set datasource of the repeater
        results.DataSource = details;
        results.DataBind();
        resultsreturned.Text = string.Format("Records Found: {0}", details.Count().ToString());
    }

    private Voucher LoadVoucher(Int32 voucherId)
    {
        IQueryable<Voucher> voucherQuery = from v in Entities.Voucher
                                           where v.VoucherId == voucherId
                                           select v;
        if (voucherQuery.ToList().Count() > 0)
            return voucherQuery.ToList().First();

        return null;
    }

    private void DisplayVoucher(Voucher voucher)
    {
        if (voucher != null)
        {
            supplier.Items.FindByValue(voucher.Supplier.SupplierId.ToString()).Selected = true;
            paceref.Text = voucher.OurRef;
            supplierref.Text = voucher.SupplierRef;
            TextBox1.Text = voucher.Title;
            TextBox2.Text = voucher.Description;
            TextBox3.Text = Convert.ToString(voucher.NumberOfStudents);
            TextBox4.Text = Convert.ToString(voucher.NumberOfLeaders);
            paymenttype.Items.FindByValue(voucher.Supplier.Xlk_SupplierType.SupplierTypeId.ToString()).Selected = true;
            TextBox7.Text = voucher.PrepaidReference;
            voucherDate.Text = Convert.ToString(voucher.VoucherDate);
            voucherTime.Text = Convert.ToString(voucher.VoucherTime);
            expectedcost.Text = Convert.ToString(voucher.ExpectedCost);

        }
    }

    private Voucher GetVoucher()
    {
        if (_voucherId.HasValue)
            return LoadVoucher(_voucherId.Value);

            return new Voucher();
    }

    private void SaveVoucher(Voucher voucher)
    {
        if (!_voucherId.HasValue)
            Entities.AddToVoucher(voucher);

        if (Entities.SaveChanges() > 0)
            Response.Redirect(string.Format("SearchVouchersPage.aspx"));
    }
    

    #endregion
}
