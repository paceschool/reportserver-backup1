﻿<%@ WebHandler Language="C#" Class="Thumbnail" %>

using System;
using System.Linq;
using System.Web;

using System.Data;

using System.Drawing;

public class Thumbnail : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string id = context.Request.QueryString["id"];
        string type = context.Request.QueryString["type"];
        bool isthumbnail = (context.Request.QueryString["thumbnail"] != null);

        if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(type))
        {
            throw new HttpException(404, "Invalid parameters.");
        }
        else
        {
            LoadImage(context, Convert.ToInt64(id), type, isthumbnail);
        }
 
    }

    private void LoadImage(HttpContext context, long id, string type, bool isthumbnail)
    {

        switch (type)
        {
            case "student":
                LoadStudentImage(context, id, isthumbnail);
                    break;
            default:
                break;
        }
    }

    private void LoadStudentImage(HttpContext context, long id, bool isthumbnail)
    {
        using (Pace.DataAccess.Enrollment.EnrollmentsEntities entity = new Pace.DataAccess.Enrollment.EnrollmentsEntities())
        {
            var img = (from ix in entity.StudentImages.Include("Xlk_ImageType").Include("Xlk_MimeType")
                       where ix.Student.StudentId == id && ix.Xlk_ImageType.ImageTypeId == 1 //profile pic
                       select ix).FirstOrDefault();


            if (img != null && img.ImageBinary.Length > 0)
            {

                using (System.IO.MemoryStream stream = new System.IO.MemoryStream(img.ImageBinary))
                {
                    if (isthumbnail)
                    {
                        using (System.Drawing.Image image = Image.FromStream(stream))
                        {
                            Image _img = image.GetThumbnailImage(50, 50, null, IntPtr.Zero);
                            _img.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                            context.Response.ContentType = "image/jpeg";
                        }
                    }
                    else
                    {
                        context.Response.ContentType = img.Xlk_MimeType.MediaType;
                        stream.CopyTo(context.Response.OutputStream);
                    }

                }
            }
            else
            {
                using (System.Drawing.Image image = Image.FromFile(context.Server.MapPath(VirtualPathUtility.ToAbsolute("~/Content/img/blank-profile.jpg"))))
                {
                    Image _img = image.GetThumbnailImage(50, 50, null, IntPtr.Zero);
                    _img.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    context.Response.ContentType = "image/jpeg";
                }

            }
            context.Response.End();
        }

    }
    

    
    public bool IsReusable
    {
        get { return true; }
    }
}