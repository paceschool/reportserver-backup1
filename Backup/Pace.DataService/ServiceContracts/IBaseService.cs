﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace Pace.DataService
{
    /// <summary>
    /// Summary description for IBaseService
    /// </summary>
    [ServiceContract]
    public interface IBaseService
    {
        [OperationContract]
        ResultMessage Ping();
    }
}