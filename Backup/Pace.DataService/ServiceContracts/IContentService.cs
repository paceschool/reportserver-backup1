﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Pace.DataService.DataContracts;
using Pace.DataAccess.Content;

namespace Pace.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITransportService" in both code and config file together.
    [ServiceContract]
    public interface IContentService : IBaseService
    {
        [OperationContract]
        string DoWork();

        [OperationContract]
        ContentResultMessage<DataContracts.ServiceType> GetServiceTypes(ServiceTypeRequest request);

        [OperationContract]
        ContentResultMessage<DataContracts.Module> FindContent(ContentRequest request);

        [OperationContract]
        ContentResultMessage<DataContracts.Module> GetModules(ModuleRequest request);

        [OperationContract]
        ContentResultMessage<DataContracts.Element> GetElements(ElementRequest request);

        [OperationContract]
        ContentResultMessage<DataContracts.LanguageCode> GetLanguages(LanguageRequest request);
    }

}