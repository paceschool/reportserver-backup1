﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Pace.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISupportService" in both code and config file together.
    [ServiceContract]
    public interface ISupportService : IBaseService
    {
        [OperationContract]
        ResultMessage SendTaskEmails();
        [OperationContract]
        ResultMessage SendArrivalsEmails();
        [OperationContract]
        ResultMessage SendTransportWeeklyEmails(Dictionary<string, string> paramaters);
        [OperationContract]
        ResultMessage SendExcursionWeeklyEmails(Dictionary<string, string> paramaters);
        [OperationContract]
        ResultMessage SendExcursionDailyEmails(Dictionary<string, string> paramaters);
        [OperationContract]
        ResultMessage SendTransportDailyEmails(Dictionary<string, string> paramaters);
    }
}