﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pace.DataService.Translations
{
    public class ElementTranslation
    {
        public static ElementTranslation Instance = new ElementTranslation();

        public List<Pace.DataService.DataContracts.Element> Translate(List<Pace.DataAccess.Content.Element> items)
        {

            List<Pace.DataService.DataContracts.Element> _items = new List<DataContracts.Element>(items.Count);

            foreach (Pace.DataAccess.Content.Element item in items)
            {
                _items.Add(Translate(item));
            }

         return _items;
        }

        public Pace.DataService.DataContracts.Element Translate(Pace.DataAccess.Content.Element item)
        {

            Pace.DataService.DataContracts.Element _item = new DataContracts.Element();
            _item.Attributes = AttributeTranslation.Instance.Translate(item.Attributes.ToList());
            _item.ElementType = item.Xlk_ElementType.Description;
            _item.Value = item.ElementValue;

            using (Pace.BusinessLogic.Content.ContentLogic logic = new BusinessLogic.Content.ContentLogic())
            {
                _item.ChildElements = ElementTranslation.Instance.Translate(logic.GetElements((from i in item.ChildElements select i.ChildElementId).ToList()));
            }


            return _item;
        }


    }
}