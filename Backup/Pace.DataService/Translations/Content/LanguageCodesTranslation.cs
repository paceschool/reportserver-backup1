﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Content;

namespace Pace.DataService.Translations
{
    public class LanguageCodeTranslation
    {

        public static LanguageCodeTranslation Instance = new LanguageCodeTranslation();

        public List<Pace.DataService.DataContracts.LanguageCode> Translate(List<Pace.DataAccess.Content.Xlk_LanguageCode> items)
        {

            List<Pace.DataService.DataContracts.LanguageCode> _items = new List<DataContracts.LanguageCode>(items.Count);

            foreach (Pace.DataAccess.Content.Xlk_LanguageCode item in items)
            {
                _items.Add(Translate(item));
            }

            return _items;
        }


        public Pace.DataService.DataContracts.LanguageCode Translate(Pace.DataAccess.Content.Xlk_LanguageCode item)
        {

            Pace.DataService.DataContracts.LanguageCode _item = new DataContracts.LanguageCode();
            _item.LanguageId = item.LanguageId;
            _item.Code = item.LanguageCode;
            _item.Description = item.Description;


            return _item;
        }



    }
}