﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Content;

namespace Pace.DataService.Translations
{
    public class ModuleTranslation
    {

        public static ModuleTranslation Instance = new ModuleTranslation();

        public List<Pace.DataService.DataContracts.Module> Translate(List<Pace.DataAccess.Content.Module> items)
        {

            List<Pace.DataService.DataContracts.Module> _items = new List<DataContracts.Module>(items.Count);

            foreach (Pace.DataAccess.Content.Module item in items)
            {
                _items.Add(Translate(item));
            }

            return _items;
        }


        public Pace.DataService.DataContracts.Module Translate(Pace.DataAccess.Content.Module item)
        {

            Pace.DataService.DataContracts.Module _item = new DataContracts.Module();
            _item.Title = item.Title;
            _item.Description = item.Description;
            _item.PageTarget = item.ElementTarget;

            using (Pace.BusinessLogic.Content.ContentLogic logic = new BusinessLogic.Content.ContentLogic())
            {
                _item.Elements = ElementTranslation.Instance.Translate(logic.GetElements((from i in item.Lnk_Module_Element
                                                                                         orderby i.Sequence
                                                                                         select i.ElementId).ToList()));
            }

            return _item;
        }



    }
}