﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pace.DataAccess.Content;

namespace Pace.DataService.Translations
{
    public class AttributeTranslation
    {

        public static AttributeTranslation Instance = new AttributeTranslation();

        public Dictionary<string, string> Translate(List<Pace.DataAccess.Content.Attribute> items)
        {

            Dictionary<string, string> _items = new Dictionary<string,string>(items.Count);

            foreach (Pace.DataAccess.Content.Attribute item in items)
            {
                _items.Add(item.Xlk_AttributeType.Description,item.AttributeValue);
            }

            return _items;
        }


    }
}