﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Pace.DataService
{
    /// <summary>
    /// Summary description for ResultMessage
    /// </summary>
    public interface IMessage
    {
        [DataMember]
        ResultCodes ResultCode { get; }
        [DataMember]
        string Message { get; }
        [DataMember]
        List<IMessage> InnerMessages { get; }
    }

    [DataContract]
    public class ResultMessage : IMessage
    {

        public ResultMessage()
            : this(ResultCodes.Success, string.Empty)
        {

        }

        public ResultMessage(ResultCodes resultCode, string message)
        {
            ResultCode = resultCode;
            Message = message;
        }
        public ResultMessage(Exception ex)
            : this(ResultCodes.Error, ex.ToString())
        {
        }

        #region IResultMessage Members

        [DataMember]
        public ResultCodes ResultCode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<IMessage> InnerMessages { get; private set; }

        public void AddMessage(IMessage message)
        {
            if (InnerMessages == null)
                InnerMessages = new List<IMessage>();

            if (message.ResultCode != ResultCodes.Success)
                ResultCode = ResultCodes.Error;

            InnerMessages.Add(message);


        }

        #endregion
    }


    [DataContract]
    public class ResultMessage<t> : ResultMessage
    {

        public ResultMessage()
            : base(ResultCodes.Success, string.Empty)
        {
        }

        public ResultMessage(Exception ex)
            : base(ex)
        {
        }

        public ResultMessage(ResultCodes resultCode, string message)
            : base(ResultCodes.Success, string.Empty)
        {
        }

        public ResultMessage(List<t> results)
            : base(ResultCodes.Success, string.Empty)
        {
            Results = results;
        }

        public ResultMessage(ResultCodes resultCode, string message, List<t> results)
            : base(ResultCodes.Success, string.Empty)
        {
            Results = results;
        }

        [DataMember]
        public List<t> Results { get; set; }
    }
}