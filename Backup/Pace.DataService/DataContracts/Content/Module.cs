﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pace.DataService.DataContracts
{
    public class Module
    {

        public string Title { get; set; }
        public string Description { get; set; }
        public string PageTarget { get; set; }
        public List<Element> Elements { get; set; }

        public Module()
        {
            Elements = new List<Element>();
        }
    }
}