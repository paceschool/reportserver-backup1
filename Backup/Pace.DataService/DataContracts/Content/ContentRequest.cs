﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pace.DataService.DataContracts
{
    public class ContentRequest
    {

        public string LanguageCode { get; set; } // usually en for english or es for spanish
        public List<string> Filters{ get; set; }
        public string ServiceType { get; set; } //Production or Test
    }
}