﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pace.DataService.DataContracts
{
    public class LanguageCode
    {

        public int LanguageId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public LanguageCode()
        {

        }
    }

}