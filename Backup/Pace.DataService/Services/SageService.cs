﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Mail;
using System.IO;
using log4net;
using Pace.Common;
using System.Data.Odbc;
using System.Configuration;
using Pace.DataAccess.Sage;

namespace Pace.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GroupService" in code, svc and config file together.
    public class SageService : BaseService, ISageService
    {

        #region ISageService Members

        public ResultMessage CheckConsistency()
        {
            throw new NotImplementedException();
        }

        public ResultMessage UpdateSageReceipts(SageEntities entity, Agency agency,List<SageReceipt> _sagereceipts)
        {

            ResultMessage message = new ResultMessage();

            try
            {
                if (_sagereceipts == null)
                     _sagereceipts = Pace.SageAccess.Receipt.Instance.GetSageReceiptList();

                foreach (SageReceipt sagereceipt in _sagereceipts)
                {
                    Receipt receipt = (from i in entity.Receipts where i.ReceiptNumber == sagereceipt.ReceiptNumber select i).SingleOrDefault();

                    if (receipt == null)
                    {
                        receipt = new Receipt();
                        receipt.ReceiptNumber = sagereceipt.ReceiptNumber;
                        entity.AddToReceipts(receipt);
                    }

                    receipt.Account = sagereceipt.Account;
                    receipt.Agency = agency;
                    receipt.Description = sagereceipt.Description;
                    receipt.ReceiptDate = sagereceipt.ReceiptDate;
                    receipt.Reference = sagereceipt.Reference;
                    receipt.Total = sagereceipt.Total;
                    receipt.Type = sagereceipt.Type;


                }
                return message;
            }
            catch (Exception ex)
            {
                message.AddMessage(new ResultMessage(ResultCodes.Error, ex.ToString()));
                return message;

            }

        }

        private ResultMessage UpdateSageInvoices(SageEntities entity, Agency agency,List<SageInvoice> _sageinvoices)
        {

            ResultMessage message = new ResultMessage();

            try
            {

                if (_sageinvoices == null)
                    _sageinvoices = Pace.SageAccess.SalesInvoice.Instance.GetSageInvoiceList();

                foreach (SageInvoice sageinvoice in _sageinvoices)
                {
                    Invoice invoice = (from i in entity.Invoices where i.InvoiceNumber == sageinvoice.InvoiceNumber select i).SingleOrDefault();

                    if (invoice == null)
                    {
                        invoice = new Invoice();
                        invoice.InvoiceNumber = sageinvoice.InvoiceNumber;
                        entity.AddToInvoices(invoice);
                    }

                    invoice.AccountRef = sageinvoice.AccountRef;
                    invoice.Address1 = sageinvoice.Address1;
                    invoice.Address2 = sageinvoice.Address2;
                    invoice.Address3 = sageinvoice.Address3;
                    invoice.Address4 = sageinvoice.Address4;
                    invoice.Address5 = sageinvoice.Address5;
                    invoice.Agency = agency;
                    invoice.Xlk_InvoiceType = (entity.Xlk_InvoiceType.SingleOrDefault(x => x.InvoiceTypeId == sageinvoice.InvoiceType));

                    invoice.GrossAmount = sageinvoice.GrossAmount;
                    invoice.InvoiceDate = sageinvoice.InvoiceDate;
                    invoice.Name = sageinvoice.Name;
                    invoice.NetAmount = sageinvoice.NetAmount;
                    invoice.Notes1 = sageinvoice.Notes1;
                    invoice.Notes2 = sageinvoice.Notes2;
                    invoice.Status = sageinvoice.Status;
                    invoice.Type = sageinvoice.Type;
                    entity.DeleteInvoiceItems(sageinvoice.InvoiceNumber);

                    foreach (SageInvoiceItem sageline in sageinvoice.SageInvoiceItems)
                    {

                        InvoiceItem invoiceitem = new InvoiceItem();
                        entity.AddToInvoiceItems(invoiceitem);

                        invoiceitem.Invoice = invoice;
                        invoiceitem.Comment1 = sageline.Comment1;
                        invoiceitem.Comment2 = sageline.Comment2;
                        invoiceitem.Description = sageline.Description;
                        invoiceitem.GrossAmount = sageline.GrossAmount;
                        invoiceitem.OrderableItem = entity.OrderableItems.SingleOrDefault(x => x.SageRef == sageline.Item);

                        invoiceitem.ItemNumber = sageline.ItemNumber;
                        invoiceitem.NetAmount = sageline.NetAmount;
                        invoiceitem.Qty = sageline.Qty;
                        invoiceitem.Unit = sageline.Unit;
                        invoiceitem.UnitPrice = sageline.UnitPrice;
                    }
                }

                return message;
            }
            catch (Exception ex)
            {
                message.AddMessage(new ResultMessage(ResultCodes.Error, ex.ToString()));
                return message;
            }
        }
        
        private ResultMessage UpdateSageQuotations(SageEntities entity, Agency agency, List<SageInvoice> _sageinvoices)
        {

            ResultMessage message = new ResultMessage();

            try
            {

                if (_sageinvoices == null)
                    _sageinvoices = Pace.SageAccess.SalesQuotation.Instance.GetSageInvoiceList();

                foreach (SageInvoice sageinvoice in _sageinvoices)
                {
                    Invoice invoice = (from i in entity.Invoices where i.InvoiceNumber == sageinvoice.InvoiceNumber select i).SingleOrDefault();

                    if (invoice == null)
                    {
                        invoice = new Invoice();
                        invoice.InvoiceNumber = sageinvoice.InvoiceNumber;
                        entity.AddToInvoices(invoice);
                    }

                    invoice.AccountRef = sageinvoice.AccountRef;
                    invoice.Address1 = sageinvoice.Address1;
                    invoice.Address2 = sageinvoice.Address2;
                    invoice.Address3 = sageinvoice.Address3;
                    invoice.Address4 = sageinvoice.Address4;
                    invoice.Address5 = sageinvoice.Address5;
                    invoice.Agency = agency;
                    invoice.Xlk_InvoiceType = (entity.Xlk_InvoiceType.SingleOrDefault(x => x.InvoiceTypeId == sageinvoice.InvoiceType));

                    invoice.GrossAmount = sageinvoice.GrossAmount;
                    invoice.InvoiceDate = sageinvoice.InvoiceDate;
                    invoice.Name = sageinvoice.Name;
                    invoice.NetAmount = sageinvoice.NetAmount;
                    invoice.Notes1 = sageinvoice.Notes1;
                    invoice.Notes2 = sageinvoice.Notes2;
                    invoice.Status = sageinvoice.Status;
                    invoice.Type = sageinvoice.Type;

                    foreach (SageInvoiceItem sageline in sageinvoice.SageInvoiceItems)
                    {

                        InvoiceItem invoiceitem = (from i in entity.InvoiceItems where i.InvoiceNumber == sageinvoice.InvoiceNumber && i.ItemNumber == sageline.ItemNumber select i).SingleOrDefault();

                        if (invoiceitem == null)
                        {
                            invoiceitem = new InvoiceItem();
                            entity.AddToInvoiceItems(invoiceitem);
                        }
                        invoiceitem.Invoice = invoice;
                        invoiceitem.Comment1 = sageline.Comment1;
                        invoiceitem.Comment2 = sageline.Comment2;
                        invoiceitem.Description = sageline.Description;
                        invoiceitem.GrossAmount = sageline.GrossAmount;
                        invoiceitem.OrderableItem = entity.OrderableItems.SingleOrDefault(x => x.SageRef == sageline.Item);

                        invoiceitem.ItemNumber = sageline.ItemNumber;
                        invoiceitem.NetAmount = sageline.NetAmount;
                        invoiceitem.Qty = sageline.Qty;
                        invoiceitem.Unit = sageline.Unit;
                        invoiceitem.UnitPrice = sageline.UnitPrice;
                    }


                }

                return message;

            }
            catch (Exception ex)
            {
                message.AddMessage(new ResultMessage(ResultCodes.Error, ex.ToString()));
                return message;
            }
        }

        private ResultMessage UpdateSageCredits(SageEntities entity, Agency agency,List<SageInvoice> _sagecredits)
        {

            ResultMessage message = new ResultMessage();

            try
            {

                if (_sagecredits == null)
                    _sagecredits = Pace.SageAccess.SalesCredit.Instance.GetSageInvoiceList();

                foreach (SageInvoice sageinvoice in _sagecredits)
                {
                    Invoice invoice = (from i in entity.Invoices where i.InvoiceNumber == sageinvoice.InvoiceNumber select i).SingleOrDefault();

                    if (invoice == null)
                    {
                        invoice = new Invoice();
                        invoice.InvoiceNumber = sageinvoice.InvoiceNumber;
                        entity.AddToInvoices(invoice);
                    }
                    invoice.AccountRef = sageinvoice.AccountRef;
                    invoice.Address1 = sageinvoice.Address1;
                    invoice.Address2 = sageinvoice.Address2;
                    invoice.Address3 = sageinvoice.Address3;
                    invoice.Address4 = sageinvoice.Address4;
                    invoice.Address5 = sageinvoice.Address5;
                    invoice.Agency = agency;
                    invoice.Xlk_InvoiceType = (entity.Xlk_InvoiceType.SingleOrDefault(x => x.InvoiceTypeId == sageinvoice.InvoiceType));

                    invoice.GrossAmount = sageinvoice.GrossAmount;
                    invoice.InvoiceDate = sageinvoice.InvoiceDate;
                    invoice.Name = sageinvoice.Name;
                    invoice.NetAmount = sageinvoice.NetAmount;
                    invoice.Notes1 = sageinvoice.Notes1;
                    invoice.Notes2 = sageinvoice.Notes2;
                    invoice.Status = sageinvoice.Status;
                    invoice.Type = sageinvoice.Type;

                    foreach (SageInvoiceItem sageline in sageinvoice.SageInvoiceItems)
                    {

                        InvoiceItem invoiceitem = (from i in entity.InvoiceItems where i.InvoiceNumber == sageinvoice.InvoiceNumber && i.ItemNumber == sageline.ItemNumber select i).SingleOrDefault();

                        if (invoiceitem == null)
                        {
                            invoiceitem = new InvoiceItem();
                            entity.AddToInvoiceItems(invoiceitem);
                        }
                        invoiceitem.Invoice = invoice;
                        invoiceitem.Comment1 = sageline.Comment1;
                        invoiceitem.Comment2 = sageline.Comment2;
                        invoiceitem.Description = sageline.Description;
                        invoiceitem.GrossAmount = sageline.GrossAmount;
                        invoiceitem.OrderableItem = entity.OrderableItems.SingleOrDefault(x => x.SageRef == sageline.Item);

                        invoiceitem.ItemNumber = sageline.ItemNumber;
                        invoiceitem.NetAmount = sageline.NetAmount;
                        invoiceitem.Qty = sageline.Qty;
                        invoiceitem.Unit = sageline.Unit;
                        invoiceitem.UnitPrice = sageline.UnitPrice;
                    }

                }



                return message;

            }
            catch (Exception ex)
            {
                message.AddMessage(new ResultMessage(ResultCodes.Error, ex.ToString()));
                return message;
            }
        }

        public ResultMessage UpdateSageAccount()
        {

            ResultMessage message = new ResultMessage();
            int startIndex = 0;
            int count = 2;

           try
            {
                List<SageAccount> _sageaccounts = Pace.SageAccess.TradingAccount.Instance.GetSageAccountList(startIndex,count);

                while (_sageaccounts != null && _sageaccounts.Count > 0)
                {
                    using (SageEntities entity = new SageEntities())
                    {

                        foreach (SageAccount account in _sageaccounts)
                        {
                            Agency agency = (from i in entity.Agencies where i.SageRef == account.SageRef select i).Take(1).SingleOrDefault();

                            if (agency == null)
                            {
                                agency = new Agency();
                                agency.Name = account.Name;
                                agency.SageRef = account.SageRef;
                                
                                entity.AddToAgencies(agency);
                            }

                            agency.AccountBalance = account.AccountBalance;
                            agency.MonthToDateTurnover = account.MonthToDateTurnover;
                            agency.PriorYearTurnover = account.PriorYearTurnover;
                            agency.YearToDateTurnover = account.YearToDateTurnover;
                            agency.Address = account.Address1 + Environment.NewLine + account.Address2 + Environment.NewLine + account.Address3 + Environment.NewLine + account.Address4 + Environment.NewLine + account.Address5;
                            agency.ContactName = account.ContactName;

                            message.AddMessage(this.UpdateSageCredits(entity, agency, account.SageCredits));
                            message.AddMessage(this.UpdateSageInvoices(entity, agency, account.SageInvoices));
                            message.AddMessage(this.UpdateSageReceipts(entity, agency, account.SageReceipts));
                            message.AddMessage(this.UpdateSageQuotations(entity, agency, account.SageQuotations));

                            entity.SaveChanges();
                        }

                    }
                    startIndex += count;
                    _sageaccounts = Pace.SageAccess.TradingAccount.Instance.GetSageAccountList(startIndex, count);
                }
                return message;

            }
            catch (Exception ex)
            {
                message.AddMessage(new ResultMessage(ResultCodes.Error, ex.ToString()));
                return message;
            }
        }

        public ResultMessage UpdateSageData()
        {

            ResultMessage message = new ResultMessage();

            try
            {
                message.AddMessage(UpdateSageAccount());


                return message;

            }
            catch (Exception ex)
            {
                message.AddMessage(new ResultMessage(ResultCodes.Error, ex.ToString()));
                return message;
            }
        }

        public ResultMessage UpdateSageMetaData()
        {

            ResultMessage message = new ResultMessage();

            try
            {

                List<SageInvoice> _sageinvoices = Pace.SageAccess.SalesInvoice.Instance.GetSageInvoiceList();

                using (SageEntities entity = new SageEntities())
                {

                    foreach (SageInvoice sageinvoice in _sageinvoices)
                    {
                        Invoice invoice = (from i in entity.Invoices where i.InvoiceNumber == sageinvoice.InvoiceNumber select i).SingleOrDefault();

                        if (invoice == null)
                        {
                            invoice = new Invoice();
                            entity.AddToInvoices(invoice);
                        }
                        invoice.AccountRef = sageinvoice.AccountRef;
                        invoice.Address1 = sageinvoice.Address1;
                        invoice.Address2 = sageinvoice.Address2;
                        invoice.Address3 = sageinvoice.Address3;
                        invoice.Address4 = sageinvoice.Address4;
                        invoice.Address5 = sageinvoice.Address5;
                        invoice.Agency = (entity.Agencies.Where(x => x.SageRef == sageinvoice.AccountRef).Take(1).SingleOrDefault());

                        invoice.GrossAmount = sageinvoice.GrossAmount;
                        invoice.InvoiceDate = sageinvoice.InvoiceDate;
                        invoice.InvoiceNumber = sageinvoice.InvoiceNumber;
                        invoice.Name = sageinvoice.Name;
                        invoice.NetAmount = sageinvoice.NetAmount;
                        invoice.Notes1 = sageinvoice.Notes1;
                        invoice.Notes2 = sageinvoice.Notes2;
                        invoice.Status = sageinvoice.Status;
                        invoice.Type = sageinvoice.Type;

                        foreach (SageInvoiceItem sageline in sageinvoice.SageInvoiceItems)
                        {

                            InvoiceItem invoiceitem = (from i in entity.InvoiceItems where i.InvoiceNumber == sageinvoice.InvoiceNumber && i.ItemNumber == sageline.ItemNumber select i).SingleOrDefault();

                            if (invoiceitem == null)
                            {
                                invoiceitem = new InvoiceItem();
                                entity.AddToInvoiceItems(invoiceitem);
                            }
                            invoiceitem.Invoice = invoice;
                            invoiceitem.Comment1 = sageline.Comment1;
                            invoiceitem.Comment2 = sageline.Comment2;
                            invoiceitem.Description = sageline.Description;
                            invoiceitem.GrossAmount = sageline.GrossAmount;
                            invoiceitem.OrderableItem = entity.OrderableItems.SingleOrDefault(x => x.SageRef == sageline.Item);

                            invoiceitem.ItemNumber = sageline.ItemNumber;
                            invoiceitem.NetAmount = sageline.NetAmount;
                            invoiceitem.Qty = sageline.Qty;
                            invoiceitem.Unit = sageline.Unit;
                            invoiceitem.UnitPrice = sageline.UnitPrice;
                        }

                        entity.SaveChanges();
                    }

                }

                return message;

            }
            catch (Exception ex)
            {
                message.AddMessage(new ResultMessage(ResultCodes.Error, ex.ToString()));
                return message;
            }
        }

    }
        #endregion
}