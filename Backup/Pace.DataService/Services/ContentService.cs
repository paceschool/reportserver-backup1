﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Channels;
using Pace.DataService.DataContracts;

namespace Pace.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TransportService" in code, svc and config file together.
    public class ContentService : BaseService, IContentService
    {
        public string DoWork()
        {

            StringBuilder sb = new StringBuilder();

            OperationContext context = OperationContext.Current;
            MessageProperties messageProperties = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpointProperty =
                messageProperties[RemoteEndpointMessageProperty.Name]
                as RemoteEndpointMessageProperty;

            sb.AppendFormat("Hello! Your IP address is {0} and your port is {1}"             , endpointProperty.Address, endpointProperty.Port);

            sb.AppendFormat("Content Service @ {0}", DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"));


            var xx = FindContent(new ContentRequest() { LanguageCode = "en", ServiceTypeId = 1, Filters = new List<string> { "home" } });


            return sb.ToString();

        }

        #region IContentService Members


        public ContentResultMessage<DataContracts.ServiceType> GetServiceTypes(ServiceTypeRequest request)
        {
            using (Pace.BusinessLogic.Content.ContentLogic logic = new BusinessLogic.Content.ContentLogic())
            {
                return new ContentResultMessage<DataContracts.ServiceType>(Translations.ServiceTypeTranslation.Instance.Translate(logic.GetServiceTypes(request.serviceTypeIds)));
            }
        }

        public ContentResultMessage<DataContracts.Module> FindContent(ContentRequest request)
        {

            List<string> _filters = new List<string>();

            try
            {
                _filters.AddRange(request.Filters);

                using (Pace.BusinessLogic.Content.ContentLogic logic = new BusinessLogic.Content.ContentLogic())
                {
                    if (String.IsNullOrEmpty(request.LanguageCode) || request.LanguageCode == "en")
                        return new ContentResultMessage<DataContracts.Module>(Translations.ModuleTranslation.Instance.Translate(logic.FindContent(request.Filters)));
                    else
                        return new ContentResultMessage<DataContracts.Module>(Translations.ModuleTranslation.Instance.Translate(logic.FindContent(request.Filters, request.LanguageCode)));

                }

            }
            catch (Exception ex)
            {
                return new ContentResultMessage<DataContracts.Module>(ex);
            }
        }

        public ContentResultMessage<DataContracts.LanguageCode> GetLanguages(LanguageRequest request)
        {

            using (Pace.BusinessLogic.Content.ContentLogic logic = new BusinessLogic.Content.ContentLogic())
            {
                return new ContentResultMessage<DataContracts.LanguageCode>(Translations.LanguageCodeTranslation.Instance.Translate(logic.GetLanguageCodes(request.ServiceTypeId)));
            }
        }

        public ContentResultMessage<DataContracts.Module> GetModules(ModuleRequest request)
        {
            using (Pace.BusinessLogic.Content.ContentLogic logic = new BusinessLogic.Content.ContentLogic())
            {
                return new ContentResultMessage<DataContracts.Module>(Translations.ModuleTranslation.Instance.Translate(logic.GetContents(request.ModuleIds)));
            }
        }

        public ContentResultMessage<DataContracts.Element> GetElements(ElementRequest request)
        {
            using (Pace.BusinessLogic.Content.ContentLogic logic = new BusinessLogic.Content.ContentLogic())
            {
                return new ContentResultMessage<DataContracts.Element>(Translations.ElementTranslation.Instance.Translate(logic.GetElements(request.ElementIds)));
            }
        }

        #endregion

        #region IBaseService Members

        ResultMessage IBaseService.Ping()
        {
            return base.Ping();
        }

        #endregion

    }
}