﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Net;
using Pace.DataAccess.Support;
using log4net;
using Pace.Common;

namespace Pace.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SupportService" in code, svc and config file together.
    public class SupportService : BaseService, ISupportService
    {

        #region ISupportService Members

        public ResultMessage SendTaskEmails()
        {
            List<Pace.DataAccess.Security.Users> users;
            string reportName = "SupportReports/Support_TasksDue";
            string emailSubject = "Tasks for Today";
            ResultMessage _message = new ResultMessage();

            try
            {
                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        users = (from s in entity.Users
                                 where s.Email != null
                                 select s).ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} users found", users.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    foreach (Pace.DataAccess.Security.Users user in users)
                    {
                        byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/{0}", reportName), new Dictionary<string, string>() { { "UserId", user.UserId.ToString() } }, ReportServiceHelper.ExportFormat.PDF);
                        EmailHelper.SendEmail(user.Email, user.Name, emailSubject, bytes, reportName, "application/pdf");

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sent task Email to {0} @ email addess {1}", user.Name, user.Email)));
                    }
                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }

        public ResultMessage SendArrivalsEmails()
        {
            List<Pace.DataAccess.Security.Users> users;
            string reportName = "SupportReports/Support_Arrivals";
            string emailSubject = "Arrivals for Next 4 Days";
            ResultMessage _message = new ResultMessage();

            try
            {
                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        users = (from s in entity.Users
                                 select s).ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} users found", users.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/{0}", reportName), new Dictionary<string, string>() { { "StartDate", DateTime.Now.ToString("dd MMM yyyy") } }, ReportServiceHelper.ExportFormat.PDF);
                    List<MailAddress> emails = new List<MailAddress>();

                    foreach (Pace.DataAccess.Security.Users user in users)
                    {
                        emails.Add(new MailAddress(user.Email, user.Name));
                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sending arrivals Email to {0} @ email addess {1}", user.Name, user.Email)));
                    }

                    EmailHelper.SendEmail(emails, emailSubject, bytes, reportName, "application/pdf");

                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }

        public ResultMessage SendDepartureEmails()
        {
            List<Pace.DataAccess.Security.Users> users;
            string reportName = "SupportReports/Support_TasksDue";
            string emailSubject = "Tasks for Today";
            ResultMessage _message = new ResultMessage();

            try
            {
                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        users = (from s in entity.Users
                                 select s).ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} users found", users.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    foreach (Pace.DataAccess.Security.Users user in users)
                    {
                        byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/{0}", reportName), new Dictionary<string, string>() { { "StudentId", "5407".ToString() } }, ReportServiceHelper.ExportFormat.PDF);
                        EmailHelper.SendEmail(user.Email, user.Name, emailSubject, bytes, reportName, "application/pdf");

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sent task Email to {0} @ email addess {1}", user.Name, user.Email)));
                    }
                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }

        public ResultMessage<Task> GetTasksByRef(string reference)
        {
            ResultMessage<Task> _message = new ResultMessage<Task>();

            try
            {

                using (SupportEntities entity = new SupportEntities())
                {

                    var tasks = (from t in entity.Task.Include("Xlk_Status").Include("Xlk_Priority").Include("RaisedByUser").Include("AssignedToUser")
                                 where t.Ref == reference
                                 orderby t.DateDue
                                 select t).ToList();

                    _message.Results = tasks;
                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage<Task>(ResultCodes.Error, ex.ToString());
            }
        }

        public ResultMessage SendTransportWeeklyEmails(Dictionary<string, string> paramaters)
        {
            List<Pace.DataAccess.Security.Users> users;
            string reportName = "SupportReports/Support_Transport";
            string emailSubject = "Transport for Next 7 Days";
            ResultMessage _message = new ResultMessage();

            try
            {
                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        users = (from s in entity.Users
                                 select s).ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} users found", users.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/{0}", reportName), paramaters, ReportServiceHelper.ExportFormat.PDF);
                    List<MailAddress> emails = new List<MailAddress>();

                    foreach (Pace.DataAccess.Security.Users user in users)
                    {
                        emails.Add(new MailAddress(user.Email, user.Name));
                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sending transport Email to {0} @ email addess {1}", user.Name, user.Email)));
                    }

                    EmailHelper.SendEmail(emails, emailSubject, bytes, reportName, "application/pdf");

                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }

        public ResultMessage SendExcursionWeeklyEmails(Dictionary<string, string> paramaters)
        {
            List<Pace.DataAccess.Security.Users> users;
            string reportName = "SupportReports/Support_Excursions";
            string emailSubject = "Excursions for Next 7 Days";
            ResultMessage _message = new ResultMessage();

            try
            {
                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        users = (from s in entity.Users
                                 select s).ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} users found", users.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/{0}", reportName), paramaters, ReportServiceHelper.ExportFormat.PDF);
                    List<MailAddress> emails = new List<MailAddress>();

                    foreach (Pace.DataAccess.Security.Users user in users)
                    {
                        emails.Add(new MailAddress(user.Email, user.Name));
                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sending excursion Email to {0} @ email addess {1}", user.Name, user.Email)));
                    }

                    EmailHelper.SendEmail(emails, emailSubject, bytes, reportName, "application/pdf");
                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }

        public ResultMessage SendExcursionDailyEmails(Dictionary<string, string> paramaters)
        {
            List<Pace.DataAccess.Security.Users> users;
            string reportName = "SupportReports/Support_Excursions";
            string emailSubject = "Excursions for Today";
            ResultMessage _message = new ResultMessage();

            try
            {
                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        users = (from s in entity.Users
                                 select s).ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} users found", users.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/{0}", reportName), paramaters, ReportServiceHelper.ExportFormat.PDF);
                    List<MailAddress> emails = new List<MailAddress>();

                    foreach (Pace.DataAccess.Security.Users user in users)
                    {
                        emails.Add(new MailAddress(user.Email, user.Name));
                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sending excursion Email to {0} @ email addess {1}", user.Name, user.Email)));
                    }

                    EmailHelper.SendEmail(emails, emailSubject, bytes, reportName, "application/pdf");

                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }

        public ResultMessage SendTransportDailyEmails(Dictionary<string, string> paramaters)
        {
            List<Pace.DataAccess.Security.Users> users;
            string reportName = "SupportReports/Support_Transport";
            string emailSubject = "Transport for Today";
            ResultMessage _message = new ResultMessage();

            try
            {
                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        users = (from s in entity.Users
                                 select s).ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} users found", users.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/{0}", reportName), paramaters, ReportServiceHelper.ExportFormat.PDF);
                    List<MailAddress> emails = new List<MailAddress>();

                    foreach (Pace.DataAccess.Security.Users user in users)
                    {
                        emails.Add(new MailAddress(user.Email, user.Name));
                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sending transport Email to {0} @ email addess {1}", user.Name, user.Email)));
                    }

                    EmailHelper.SendEmail(emails, emailSubject, bytes, reportName, "application/pdf");

                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }

        #endregion
    }

}