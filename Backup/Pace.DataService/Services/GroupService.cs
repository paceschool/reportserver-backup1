﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Mail;
using System.IO;
using log4net;
using Pace.Common;

namespace Pace.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GroupService" in code, svc and config file together.
    public class GroupService : BaseService, IGroupService
    {

        #region IGroupService Members

        public ResultMessage CheckConsistency()
        {
            throw new NotImplementedException();
        }

        public ResultMessage SendEmailToFamilies(int groupId, int userId, int messageId, string subject, string body)
        {

            Pace.DataAccess.Security.Users user;
            Pace.DataAccess.Group.Group group;
            List<Pace.DataAccess.Accommodation.Family> families;
            ResultMessage _message = new ResultMessage();
            Dictionary<string, string> replacements = new Dictionary<string, string>();

            try
            {
                CommunicationItemRecord record = (from f in CommunicationItems.Instance.GetCommunicationItemsDetails("message") where f.Id == messageId.ToString() select f).FirstOrDefault();

                using (Pace.DataAccess.Security.SecurityEntities entity = new Pace.DataAccess.Security.SecurityEntities())
                {
                    try
                    {
                        user = (from s in entity.Users
                                where s.UserId == userId
                                select s).FirstOrDefault();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sending message from {0}: user found!", user.Email)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Group.GroupEntities entity = new Pace.DataAccess.Group.GroupEntities())
                {
                    try
                    {
                        group = (from s in entity.Group
                                 where s.GroupId == groupId
                                 select s).FirstOrDefault();

                        replacements.Add("groupname", group.GroupName);


                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sending message from {0}: user found!", user.Email)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }
                using (Pace.DataAccess.Accommodation.AccomodationEntities entity = new Pace.DataAccess.Accommodation.AccomodationEntities())
                {
                    try
                    {
                        families = (from s in entity.Families
                                    from h in s.Hostings
                                    where h.Student.GroupId == groupId
                                    select s).Distinct().ToList();

                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("{0} families found!", families.Count)));
                    }
                    catch (Exception ex)
                    {
                        return new ResultMessage(ResultCodes.Error, ex.ToString());
                    }
                }

                using (Pace.DataAccess.Support.SupportEntities entity = new Pace.DataAccess.Support.SupportEntities())
                {
                    foreach (Pace.DataAccess.Accommodation.Family family in families)
                    {
                        replacements.Add("firstname", family.FirstName);
                        replacements.Add("surname", family.SurName);

                        MailMessage message = EmailHelper.CreateEmail(user.Email, user.Name, "hugh@paceinstitute.ie", string.Format("{0} {1}", family.FirstName, family.SurName), TextReplacement.Instance.Replace(subject, replacements), TextReplacement.Instance.Replace(body, replacements));

                        foreach (string templateReportName in record.ReportAttachments)
                        {
                            byte[] bytes = ReportHelper.ReportASBtyeStream(string.Format("/PaceManagerReports/{0}", templateReportName), new Dictionary<string, string>() { { "FamilyId", family.FamilyId.ToString() }, { "GroupId", groupId.ToString() } }, ReportServiceHelper.ExportFormat.PDF);
                            message.Attachments.Add(new Attachment(new MemoryStream(bytes), templateReportName, "application/pdf"));
                        }
                        EmailHelper.SendEmail(message);
                        _message.AddMessage(new ResultMessage(ResultCodes.Success, string.Format("Sent {0} Email to {1} {2} @ email addess {3}", record.Title, family.FirstName, family.SurName, family.EmailAddress)));
                    }
                }

                return _message;
            }
            catch (Exception ex)
            {
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }


        }


        #endregion
    }
}