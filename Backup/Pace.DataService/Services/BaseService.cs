﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

namespace Pace.DataService
{
    /// <summary>
    /// Summary description for BaseService
    /// </summary>
    public abstract class BaseService : IBaseService
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public virtual ResultMessage Ping()
        {
            try
            {
                return new ResultMessage(ResultCodes.Success, string.Format("The service {0} is alive @ {1}", typeof(BaseService).Name, DateTime.Now.ToString()));
            }
            catch (Exception ex)
            {
                WriteToLog(ex.ToString());
                return new ResultMessage(ResultCodes.Error, ex.ToString());
            }
        }


        public BaseService()
        {
            WriteToLog("Service Called");
        }
        protected static void WriteToLog(string message)
        {
            if (log.IsDebugEnabled)
            {
                log.Info(message);
            }
        }

    }
}