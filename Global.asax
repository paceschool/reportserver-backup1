﻿<%@ Application Language="C#" %>


<script runat="server">
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(BasePage).Name);
    
    void Application_Start(object sender, EventArgs e) 
    {
        log4net.Config.XmlConfigurator.Configure();
          //   RouteTable.Routes.MapHttpRoute(
          //     name: "DefaultApi",
          //     routeTemplate: "WebAPI/{controller}/{action}/{id}",
          //     defaults: new { id = System.Web.Http.RouteParameter.Optional }
          //);
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown
    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        Exception exc = Server.GetLastError();
        
        if (log.IsDebugEnabled)
        {
            log.Info(exc.ToString());
        }

        Server.ClearError();
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
    public override string GetVaryByCustomString(HttpContext context, string arg)
    {
        if (arg == "CampusId")
        {
            return string.Format("{0} - {1}",context.Session.SessionID, BasePage.GetCampusId.ToString());
        }

        return base.GetVaryByCustomString(context, arg);
    }
    
</script>
